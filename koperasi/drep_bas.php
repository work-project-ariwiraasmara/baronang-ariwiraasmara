<?php
session_start();
error_reporting(0);

include('lib/phpExcel/Classes/PHPExcel.php');
include "connect.php";


    $filePath = "uploads/excel/";

    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getProperties()->setCreator("baronang");
    $objPHPExcel->getProperties()->setTitle("Saldo Simpanan Pokok");

// set autowidth
    for($col = 'A'; $col !== 'Z'; $col++) {
        $objPHPExcel->getActiveSheet()
            ->getColumnDimension($col)
            ->setAutoSize(true);
    }

    $listCombo = array();

    //sheet 2
    $objWorkSheet = $objPHPExcel->createSheet(0);
// baris judul
    $objWorkSheet->SetCellValue('A1', 'No');
    $objWorkSheet->SetCellValue('B1', 'Member ID');
    $objWorkSheet->SetCellValue('C1', 'Nama');
    $objWorkSheet->SetCellValue('D1', 'Simpanan Pokok');
    $objWorkSheet->SetCellValue('E1', 'Simpanan Wajib');
    $objWorkSheet->SetCellValue('F1', 'Simpanan Sukarela');


    $no = 1;
    $row = 2;
    $x =  "SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY MemberID asc) as row FROM [dbo].[MemberListView]) a WHERE KID='$_SESSION[KID]'";
    $y = sqlsrv_query($conn, $x);
    while($z = sqlsrv_fetch_array( $y, SQLSRV_FETCH_NUMERIC)){
         //push untuk drop down list
        array_push($listCombo, $z[1]);

        $x1   = "select * from dbo.BasicSavingBalance where KodeBasicSavingType='".$_SESSION['KID']."101' and MemberID = '$z[1]'";
        $y1  = sqlsrv_query($conn, $x1);
        $z1  = sqlsrv_fetch_array($y1, SQLSRV_FETCH_NUMERIC);
        
        

        $x2   = "select * from dbo.BasicSavingBalance where KodeBasicSavingType='".$_SESSION['KID']."102' and MemberID = '$z[1]'";
        $y2  = sqlsrv_query($conn, $x2);
        $z2  = sqlsrv_fetch_array($y2, SQLSRV_FETCH_NUMERIC);

      

        $x3   = "select * from dbo.BasicSavingBalance where KodeBasicSavingType='".$_SESSION['KID']."103' and MemberID = '$z[1]'";
        $y3  = sqlsrv_query($conn, $x3);
        $z3   = sqlsrv_fetch_array($y3, SQLSRV_FETCH_NUMERIC);
       

        $objWorkSheet->SetCellValue("A".$row, $no);
        $objWorkSheet->SetCellValueExplicit("B".$row, $z[1], PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet->SetCellValue("C".$row, $z[2]);
        $objWorkSheet->SetCellValueExplicit("D".$row, number_format($z1[5]), PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet->SetCellValueExplicit("E".$row, number_format($z2[5]), PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet->SetCellValueExplicit("F".$row, number_format($z3[5]), PHPExcel_Cell_DataType::TYPE_STRING);
        
        $no++;
        $row++;
    }

    $objWorkSheet->setTitle('Saldo Simpanan Pokok');

    $fileName = 'Saldosimpananpokok'.'_'.strtotime(date('Y-m-d H:i:s')).'.xls';
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');

// download ke client
    header('Content-type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename="'.$fileName.'"');
    $objWriter->save('php://output');

    return $filePath.'/'.$fileName;

?>
