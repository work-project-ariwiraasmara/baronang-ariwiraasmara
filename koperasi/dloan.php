<?php
session_start();
error_reporting(0);

include('lib/phpExcel/Classes/PHPExcel.php');
include "connect.php";

$a = "select * from [dbo].[MemberList] where StatusMember = 1";
$b = sqlsrv_query($conn, $a);
$c = sqlsrv_fetch_array( $b, SQLSRV_FETCH_NUMERIC);

$aaa = "select * from [dbo].[LoanType] where Status = 1";
$bbb = sqlsrv_query($conn, $aaa);
$ccc = sqlsrv_fetch_array( $bbb, SQLSRV_FETCH_NUMERIC);

if($c == null){
    messageAlert('Member belum di set. Tidak dapat mendownload template.','warning');
    header('Location: mloan.php');
}
else if($ccc == null){
    messageAlert('Loan type belum di set/tidak ada yang aktif. Tidak dapat mendownload template.','warning');
    header('Location: mloan.php');
}
else{

    $filePath = "uploads/excel/";

    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getProperties()->setCreator("baronang");
    $objPHPExcel->getProperties()->setTitle("Data Upload Loan Balance");

// set autowidth
    for($col = 'A'; $col !== 'Z'; $col++) {
        $objPHPExcel->getActiveSheet()
            ->getColumnDimension($col)
            ->setAutoSize(true);
    }

    $listCombo = array();

    //sheet 3
    $objWorkSheet3 = $objPHPExcel->createSheet(1);
// baris judul
    $objWorkSheet3->SetCellValue('A1', 'No');
    $objWorkSheet3->SetCellValue('B1', 'Product Name');
    $objWorkSheet3->SetCellValue('C1', 'Interest Type');
    $objWorkSheet3->SetCellValue('D1', 'Interest Rate %');
    $objWorkSheet3->SetCellValue('E1', 'Maturity');
    $objWorkSheet3->SetCellValue('F1', 'Contract Period');
    $objWorkSheet3->SetCellValue('G1', 'Principal Pinalty');
    $objWorkSheet3->SetCellValue('H1', 'Interest Pinalty');
    $objWorkSheet3->SetCellValue('I1', 'Minimum Amount');
    $objWorkSheet3->SetCellValue('J1', 'Maximum Amount');
    $objWorkSheet3->SetCellValue('K1', 'Tempo Hari Pengajuan');
    $objWorkSheet3->SetCellValue('L1', 'Over Limit');
    $objWorkSheet3->SetCellValue('M1', 'Interest+Principal');

    $no = 1;
    $row = 2;

    $x = "select * from [dbo].[LoanType] where Status = 1";
    $y = sqlsrv_query($conn, $x);
    $type = '';
    while($z = sqlsrv_fetch_array( $y, SQLSRV_FETCH_NUMERIC)){
        array_push($listCombo, $z[1]);

        $itype = '';
        if($z[2] == 01){
            $itype = 'Flat';
        }
        else if($z[2] == 02){
            $itype = 'Effective';
        }
        else if($z[2] == 03){
            $itype = 'Annuitas';
        }

        $maturity = '';
        if($z[6] == 01){
            $maturity = 'Monthly';
        }
        else if($z[6] == 02){
            $maturity = 'Weekly';
        }
        else if($z[6] == 03){
            $maturity = 'Daily';
        }

        $over = '';
        if($z[17] == 1){
            $over = 'Yes';
        }
        else{
            $over = 'No';
        }

        $ip = '';
        if($z[20] == 1){
            $ip = 'Yes';
        }
        else{
            $ip = 'No';
        }

        $objWorkSheet3->SetCellValue("A".$row, $no);
        $objWorkSheet3->SetCellValue("B".$row, $z[1]);
        $objWorkSheet3->SetCellValue("C".$row, $itype);
        $objWorkSheet3->SetCellValue("D".$row, $z[3]);
        $objWorkSheet3->SetCellValue("E".$row, $maturity);
        $objWorkSheet3->SetCellValue("F".$row, $z[7]);
        $objWorkSheet3->SetCellValue("G".$row, $z[8]);
        $objWorkSheet3->SetCellValue("H".$row, $z[9]);
        $objWorkSheet3->SetCellValue("I".$row, number_format($z[4]));
        $objWorkSheet3->SetCellValue("J".$row, number_format($z[5]));
        $objWorkSheet3->SetCellValue("K".$row, $z[11]);
        $objWorkSheet3->SetCellValue("L".$row, $over);
        $objWorkSheet3->SetCellValue("M".$row, $ip);

        $no++;
        $row++;
    }
    $objWorkSheet3->setTitle('Data Master Produk Loan');

    $objWorkSheet = $objPHPExcel->createSheet(0);
// baris judul
    $objWorkSheet->SetCellValue('A1', 'Nama');
    $objWorkSheet->SetCellValue('B1', 'NIP');
    $objWorkSheet->SetCellValue('C1', 'KTP');
    $objWorkSheet->SetCellValue('D1', 'Email');
    $objWorkSheet->SetCellValue('E1', 'Telepon');
    $objWorkSheet->SetCellValue('F1', 'Akun Regular Saving');
    $objWorkSheet->SetCellValue('G1', 'Nama Produk');
    $objWorkSheet->SetCellValue('H1', 'Balance');
    $objWorkSheet->SetCellValue('I1', 'Contract Period');
    $objWorkSheet->SetCellValue('J1', 'Interest Rate %');

    //looping isi combo box
    $row = 2;
    for($no=1;$no<=100;$no++){
        $combo = $objWorkSheet->GetCell("G".$row)->getDataValidation();
        $combo->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
        $combo->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
        $combo->setAllowBlank(false);
        $combo->setShowInputMessage(true);
        $combo->setShowErrorMessage(true);
        $combo->setShowDropDown(true);
        $combo->setErrorTitle('Input error');
        $combo->setError('Value is not in list.');
        $combo->setPromptTitle('Pick from list');
        $combo->setPrompt('Please pick a value from the drop-down list.');
        $combo->setFormula1('"'.implode(',',$listCombo).'"');

        $row++;
    }
    $objWorkSheet->setTitle('Data Upload Loan Balance');

    //sheet 2
    $objWorkSheet2 = $objPHPExcel->createSheet(2);
// baris judul
    $objWorkSheet2->SetCellValue('A1', 'Member ID');
    $objWorkSheet2->SetCellValue('B1', 'No. KTP');
    $objWorkSheet2->SetCellValue('C1', 'NIP');
    $objWorkSheet2->SetCellValue('D1', 'Nama');
    $objWorkSheet2->SetCellValue('E1', 'Alamat');
    $objWorkSheet2->SetCellValue('F1', 'Telepon');
    $objWorkSheet2->SetCellValue('G1', 'Email');

    $no = 1;
    $row = 2;

    $x = "select * from [dbo].[MemberList] where StatusMember = 1 order by MemberID asc";
    $y = sqlsrv_query($conn, $x);
    while($z = sqlsrv_fetch_array( $y, SQLSRV_FETCH_NUMERIC)){
        $objWorkSheet2->SetCellValueExplicit("A".$row, $z[1], PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet2->SetCellValueExplicit("B".$row, $z[7], PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet2->SetCellValue("C".$row, $z[8]);
        $objWorkSheet2->SetCellValue("D".$row, $z[2]);
        $objWorkSheet2->SetCellValue("E".$row, $z[3]);
        $objWorkSheet2->SetCellValueExplicit("F".$row, $z[4], PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet2->SetCellValue("G".$row, $z[5]);

        $no++;
        $row++;
    }
    $objWorkSheet2->setTitle('Data Master Member');

    //sheet 3
    $objWorkSheet3 = $objPHPExcel->createSheet(3);
    // baris judul
    $objWorkSheet3->SetCellValue('A1', 'Member ID');
    $objWorkSheet3->SetCellValue('B1', 'No. KTP');
    $objWorkSheet3->SetCellValue('C1', 'NIP');
    $objWorkSheet3->SetCellValue('D1', 'Nama');
    $objWorkSheet3->SetCellValue('E1', 'Telepon');
    $objWorkSheet3->SetCellValue('F1', 'Email');
    $objWorkSheet3->SetCellValue('G1', 'Akun Regular Saving');
    $objWorkSheet3->SetCellValue('H1', 'Nama Produk Regular Saving');

    $no = 1;
    $row = 2;

    $x = "select * from [dbo].[RegSavingAccView] order by MemberID, AccountNo asc";
    $y = sqlsrv_query($conn, $x);
    while($z = sqlsrv_fetch_array( $y, SQLSRV_FETCH_NUMERIC)){
        $objWorkSheet3->SetCellValueExplicit("A".$row, $z[1], PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet3->SetCellValueExplicit("B".$row, $z[9], PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet3->SetCellValue("C".$row, $z[8]);
        $objWorkSheet3->SetCellValue("D".$row, $z[7]);
        $objWorkSheet3->SetCellValueExplicit("E".$row, $z[11], PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet3->SetCellValue("F".$row, $z[10]);
        $objWorkSheet3->SetCellValueExplicit("G".$row, $z[2], PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet3->SetCellValue("H".$row, $z[4]);

        $no++;
        $row++;
    }
    $objWorkSheet3->setTitle('Data Akun Regular Saving Member');

    $fileName = 'templateLoanBalance'.'_'.strtotime(date('Y-m-d H:i:s')).'.xls';
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');

    // download ke client
    header('Content-type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename="'.$fileName.'"');
    $objWriter->save('php://output');

    return $filePath.'/'.$fileName;
}
?>