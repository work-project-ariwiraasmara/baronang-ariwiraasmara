<?php
session_start();
error_reporting(0);

include('lib/phpExcel/Classes/PHPExcel.php');
include "connect.php";


    $filePath = "uploads/excel/";

    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getProperties()->setCreator("baronang");
    $objPHPExcel->getProperties()->setTitle("Saldo Tabungan");

// set autowidth
    for($col = 'A'; $col !== 'Z'; $col++) {
        $objPHPExcel->getActiveSheet()
            ->getColumnDimension($col)
            ->setAutoSize(true);
    }

    $listCombo = array();

    //sheet 2
    $objWorkSheet = $objPHPExcel->createSheet(0);
// baris judul
    $objWorkSheet->SetCellValue('A1', 'No');
    $objWorkSheet->SetCellValue('B1', 'No. Akun');
    $objWorkSheet->SetCellValue('C1', 'Nama');
    $objWorkSheet->SetCellValue('D1', 'Tanggal Buka');
    $objWorkSheet->SetCellValue('E1', 'Tanggal Tutup');
    $objWorkSheet->SetCellValue('F1', 'Produk');
    $objWorkSheet->SetCellValue('G1', 'Saldo');
    $objWorkSheet->SetCellValue('H1', 'Status');


    $no = 1;
    $row = 2;
    $x =  "SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY MemberID asc) as row FROM [dbo].[RegSavingAccView]) a WHERE KID='$_SESSION[KID]'";
    $y = sqlsrv_query($conn, $x);
    while($z = sqlsrv_fetch_array( $y, SQLSRV_FETCH_NUMERIC)){
        //var_dump($z).die;
        //push untuk drop down list
        array_push($listCombo, $z[1]);
        $open = '';
        $close = '';
        $sql   = "select * from dbo.RegularSavingReportList where AccNo = '$z[2]'";
        $stmt  = sqlsrv_query($conn, $sql);
        $rw   = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_NUMERIC);

            if($rw != null){
               $open = $rw[1]->format('Y-m-d H:i:s');
            if($rw[2] != ''){
                $close = $rw[2]->format('Y-m-d H:i:s');
                //var_dump($close).die;
            }
        }


          

        $objWorkSheet->SetCellValue("A".$row, $no);
        $objWorkSheet->SetCellValueExplicit("B".$row, $z[2], PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet->SetCellValue("C".$row, $z[7]);
        $objWorkSheet->SetCellValue("D".$row, $open);
        $objWorkSheet->SetCellValue("E".$row, $close);
        $objWorkSheet->SetCellValue("F".$row, $z[4]);
        $objWorkSheet->SetCellValueExplicit("G".$row, number_format($z[5]), PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet->SetCellValueExplicit("H".$row, $rw[5]);
        
        $no++;
        $row++;
    }

    $objWorkSheet->setTitle('Saldo Tabungan');

    $fileName = 'Saldotabungan'.'_'.strtotime(date('Y-m-d H:i:s')).'.xls';
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');

// download ke client
    header('Content-type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename="'.$fileName.'"');
    $objWriter->save('php://output');

    return $filePath.'/'.$fileName;

?>
