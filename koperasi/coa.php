<?php require('header.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
    <div class="alert alert-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-warning ?>"></i> <?php echo ucfirst($_SESSION['error-type']); ?></h4>
        <?php echo $_SESSION['error-message']; ?>
    </div>
<?php } ?>

<?php
$sql = "select * from dbo.TypeAcc order by KodeGroup asc";
$stmt = sqlsrv_query($conn, $sql);
$row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_NUMERIC);
?>
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><?php echo lang('Chart of Accounts'); ?></h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="form">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-example"><i class="fa fa-tv"></i> <?php echo lang('Contoh Upload Data'); ?></button>
            <button type="button" class="btn btn-info download" data-toggle="modal" data-target="#modal-confirm"><i class="fa fa-download"></i> <?php echo lang('Download Template'); ?></button>
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-upload"><i class="fa fa-upload"></i> <?php echo lang('Upload Data'); ?></button>

            <button type="button" id="delete" class="btn btn-danger pull-right"><i class="fa fa-trash"></i> <?php echo lang('Hapus Semua Data'); ?></button>

            <div class="modal fade" id="modal-example" style="display: none;">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span></button>
                            <h4 class="modal-title"><?php echo lang('Contoh Upload Data'); ?></h4>
                        </div>
                        <div class="modal-body">
                            <div class="form">
                                <div class="callout callout-info">
                                    <h4><?php echo lang('Informasi'); ?></h4>

                                    <p><?php echo lang('Anda harus mengikuti contoh ini untuk sukses meng-upload data. <b> Jangan ubah judul header, row, dan kolum pada data excel tersebut! </b>'); ?></p>
                                </div>
                            </div>
                            <div class="form">
                                <table class="table table-bordered">
                                    <tr>
                                        <th colspan="5" class="text-center"><?php echo lang('Kode Akun'); ?></th>
                                        <th rowspan="2" class="text-center"><?php echo lang('Nama Akun'); ?></th>
                                        <th rowspan="2" class="text-center"><?php echo lang('Kategori Akun'); ?></th>
                                    </tr>
                                    <tr>
                                        <th>Header 1</th>
                                        <th>Header 2</th>
                                        <th>Header 3</th>
                                        <th>Header 4</th>
                                        <th>Header 5</th>
                                    </tr>
                                    <tr>
                                        <td>1.0.00.00.000</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td><?php echo lang('Aktiva'); ?></td>
                                        <td><?php echo lang('Aktiva'); ?></td>
                                    </tr>
                                    <tr>
                                        <td>1.0.00.00.000</td>
                                        <td>1.1.00.00.000</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td><?php echo lang('Aktiva Lancar'); ?></td>
                                        <td><?php echo lang('Aktiva Lancar'); ?></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>1.1.00.00.000</td>
                                        <td>1.1.01.00.000</td>
                                        <td></td>
                                        <td></td>
                                        <td><?php echo lang('Kas & Bank'); ?></td>
                                        <td><?php echo lang('Aktiva Lancar'); ?></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>1.1.01.00.000</td>
                                        <td>1.1.01.01.000</td>
                                        <td></td>
                                        <td><?php echo lang('Kas'); ?></td>
                                        <td><?php echo lang('Aktiva Lancar'); ?></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>1.1.01.01.000</td>
                                        <td>1.1.01.01.001</td>
                                        <td><?php echo lang('Kas'); ?></td>
                                        <td><?php echo lang('Aktiva Lancar'); ?></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>1.1.01.00.000</td>
                                        <td>1.1.01.02.000</td>
                                        <td></td>
                                        <td><?php echo lang('Bank'); ?></td>
                                        <td><?php echo lang('Aktiva Lancar'); ?></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>1.1.01.02.000</td>
                                        <td>1.1.01.02.001</td>
                                        <td>Bank Mandiri</td>
                                        <td><?php echo lang('Aktiva Lancar'); ?></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><?php echo lang('Tutup'); ?></button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>

            <div class="modal fade" id="modal-upload" style="display: none;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form action="ucoa.php" method="post" enctype="multipart/form-data">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span></button>
                                <h4 class="modal-title"><?php echo lang('Upload Data'); ?></h4>
                            </div>
                            <div class="modal-body">
                                <?php if($row == null){ ?>
                                <div class="callout callout-info">
                                    <h4><?php echo lang('Informasi'); ?></h4>

                                    <p><?php echo lang('Anda harus mengikuti contoh ini untuk sukses meng-upload data. <b> Jangan ubah judul header, row, dan kolum pada data excel tersebut! </b>'); ?></p>
                                </div>

                                <input type="file" name="filename" class="form-control" accept=".xls"  required="" placeholder="File Excel">
                                <?php } else { ?>
                                    <div class="callout callout-info">
                                        <h4><?php echo lang('Informasi!'); ?></h4>

                                        <p><?php echo lang('COA sudah di Upload'); ?></p>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="modal-footer">
                                <?php if($row == null){ ?>
                                    <button type="submit" id="btn-save" class="btn btn-success pull-left"><?php echo lang('Simpan'); ?></button>
                                    <button type="button" id="tree-loading" class="btn btn-default pull-left hide"><i class="fa fa-rotate-right fa-spin"></i></button>
                                <?php } ?>
                                <button type="button" class="btn btn-default pull-right" data-dismiss="modal"><?php echo lang('Tutup'); ?></button>
                            </div>
                        </form>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>

            <div class="modal fade" id="modal-confirm" style="display: none;">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span></button>
                            <h4 class="modal-title"><?php echo lang('Konfirmasi'); ?></h4>
                        </div>
                        <div class="modal-body">
                            <?php echo lang('Download Template dengan contoh data?'); ?>
                        </div>
                        <div class="modal-footer">
                            <a href="dcoa.php?example"><button type="button" class="btn btn-primary pull-left"><?php echo lang('Ya'); ?></button></a>
                            <a href="dcoa.php"><button type="button" class="btn btn-warning pull-left"><?php echo lang('Tidak'); ?></button></a>
                            <button type="button" class="btn btn-default pull-right" data-dismiss="modal"><?php echo lang('Tutup'); ?></button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>

        </div>
        <hr>
        <div class="form">
            <ul id="treeview">
                <?php
                $index = 1;
                $x = "select* from dbo.GroupAcc order by substring (ltrim([KodeGroup]),1,1) asc";
                $y = sqlsrv_query($conn, $x);
                while($z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC)){
                    ?>
                    <li data-icon-cls="fa fa-folder"><?php echo $z[0].' - '.$z[1]; ?>
                        <button type="button" data-toggle="modal" data-target="#modal-default<?php echo $index; ?>" class="btn btn-primary btn-xs pull-right"><i class="fa fa-plus"></i> Add Header</button>
                        <ul id="treeview">
                            <?php
                            $index2 = 0;
                            $xx = "select* from dbo.TypeAcc where KodeGroup = '$z[0]'";
                            $yy = sqlsrv_query($conn, $xx);
                            while($zz = sqlsrv_fetch_array($yy, SQLSRV_FETCH_NUMERIC)){
                                ?>
                                <li data-icon-cls="fa fa-folder"><?php echo $zz[0].' - '.$zz[1]; ?>
                                    <span class="pull-right"><button type="button" data-toggle="modal" data-target="#modal-detail<?php echo $index.$index2; ?>" class="btn btn-info btn-xs btn-adc1"><i class="fa fa-plus"></i> Add Account</button></span>
                                    <ul id="treeview">
                                        <?php
                                        $xxx = "select* from dbo.AccountView where KodeTipe = '$zz[0]' and Header = 0";
                                        $yyy = sqlsrv_query($conn, $xxx);
                                        while($zzz = sqlsrv_fetch_array($yyy, SQLSRV_FETCH_NUMERIC)){
                                            ?>
                                            <li data-icon-cls="fa fa-folder"><?php echo $zzz[0].' - '.$zzz[1]; ?>
                                                <span class="pull-right"><button type="button" data-toggle="modal" data-target="#modal-detail-form_acclvl1_<?php echo str_replace('.','', $zzz[0]); ?>" id="btnopen_acclvl1_<?php echo str_replace(".","", $zzz[0]); ?>" class="btn btn-info btn-xs"><i class="fa fa-plus"></i> Add Account</button></span>
                                                <ul id="treeview">
													<!--<li id="">
                                                        <div id="">
                                                            <form name ="formx" action ="prosescoa.php" method="post" id="">
                                                                <div class ="col-sm-4">
                                                                    <label name="lblnm" class="badge">Input Account Di Bawah <?php echo $zzz[1]; ?></label>
                                                                </div>
                                                                <div class ="col-sm-4">
                                                                    <input type="text" class="form-control" name="sacc" placeholder="Nama Account">
                                                                </div>
                                                                <input type="submit" class="btn btn-default btn-sm" name = "btnsave" value="<?php echo lang('Simpan'); ?>">
                                                                <input type ="hidden" name ="tipex" value ="<?php echo $zzz[2]; ?>">
                                                                <input type ="hidden" name ="accx" value ="<?php echo $zzz[0]; ?>">
                                                                <input type ="hidden" name ="headerx" value ="<?php echo $zzz[4]; ?>">
                                                                <span class="pull-right"><button type="button" id="btnclose_acclvl1_<?php echo str_replace(".","", $zzz[0]); ?>" class="btn btn-info btn-xs"><i class="fa fa-minus"></i> Close Account</button></span>
                                                            </form>
                                                        </div>
													</li>
                                                    <script type="text/javascript">
                                                        $('#btnopen_acclvl1_<?php echo str_replace('.','', $zzz[0]); ?>').click(function() {
                                                            $('#form_acclvl1_<?php echo str_replace('.','', $zzz[0]); ?>').removeClass('hidden');
                                                            alert('This Button Open Has Responded!');
                                                        });

                                                        $('#btnclose_acclvl1_<?php echo str_replace('.','', $zzz[0]); ?>').click(function() {
                                                            $('#form_acclvl1_<?php echo str_replace('.','', $zzz[0]); ?>').addClass('hidden');
                                                            alert('This Button Close Has Responded!');
                                                        });
                                                    </script>-->
													<?php
                                                    $xxxx = "select* from dbo.AccountView where KodeTipe = '$zz[0]' and Header = 1 and KodeAccount like '%".substr($zzz[0],0,6)."%'";
                                                    $yyyy = sqlsrv_query($conn, $xxxx);
                                                    while($zzzz = sqlsrv_fetch_array($yyyy, SQLSRV_FETCH_NUMERIC)){
                                                        ?>														
                                                        <li data-icon-cls="fa fa-folder"><?php echo $zzzz[0].' - '.$zzzz[1]; ?>
                                                            <span class="pull-right"><button type="button" data-toggle="modal" data-target="#modal-detail-form_acclvl2_<?php echo str_replace('.','', $zzzz[0]); ?>" id="btnopen_acclvl2_<?php echo str_replace(".","", $zzzz[0]); ?>" class="btn btn-info btn-xs btn-adc btn-adc3" id="btn-adc"><i class="fa fa-plus"></i> Add Account</button></span>
                                                            <ul class="treesh">
																<!--<li>
                                                                    <div id="" class="">
                                                                        <form name ="formx" action ="prosescoa.php" method="post" id="">
                                                                            <div class ="col-sm-4">
                                                                                <label name="lblnm" class="badge">Input Account Di Bawah <?php echo $zzzz[1]; ?></label>
                                                                            </div>
                                                                            <div class ="col-sm-4">
                                                                                <input type="text" class="form-control" style="border-radius: 10px" name="sacc" placeholder="Nama Account">
                                                                            </div>
                                                                            <input type="submit" class="btn btn-danger" name = "btnsave" value="<?php echo lang('Simpan'); ?>">
                                                                            <input type ="hidden" name ="tipex" value ="<?php echo $zzzz[2]; ?>">
                                                                            <input type ="hidden" name ="accx" value ="<?php echo $zzzz[0]; ?>">
                                                                            <input type ="hidden" name ="headerx" value ="<?php echo $zzzz[4]; ?>">
                                                                        </form>
                                                                    </div>
                                                                </li>-->
                                                                <?php
                                                                $xxxxx = "select* from dbo.AccountView where KodeTipe = '$zz[0]' and Header = 2 and KodeAccount like '%".substr($zzzz[0],0,9)."%'";
                                                                $yyyyy = sqlsrv_query($conn, $xxxxx);
                                                                while($zzzzz = sqlsrv_fetch_array($yyyyy, SQLSRV_FETCH_NUMERIC)){
                                                                    ?>
                                                                    <li>
                                                                        <?php echo $zzzzz[0].' - '.$zzzzz[1]; ?>
                                                                    </li>
                                                                <?php } ?>
                                                            </ul>
                                                        </li>
                                                    <?php } ?>
                                                </ul>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </li>
                            <?php $index2++; } ?>
                        </ul>
                    </li>
                <?php $index++; } ?>
            </ul>
        </div>

        <div class="form">
            <?php
            $index=1;
            $sql = "select * from dbo.GroupAcc order by KodeGroup asc";
            $stmt = sqlsrv_query($conn, $sql);
            while($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_NUMERIC)){
                ?>
                <div class="modal fade" id="modal-default<?php echo $index; ?>">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span></button>
                                <h4 class="modal-title"><?php echo lang('Tambah Sub Akun'); ?><?php echo $row[1]; ?></h4>
                            </div>
                            <div class="modal-body">
                                <input type="text" class="form-control" id="name2<?php echo $index; ?>" placeholder="Account Name">
                                <input type="text" class="form-control" id="ket2<?php echo $index; ?>" placeholder="Description">
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><?php echo lang('Tutup'); ?></button>
                                <button type="button" id="btn-add<?php echo $index; ?>" class="btn btn-primary"><?php echo lang('Simpan'); ?></button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <script type="text/javascript">
                    $('#btn-add<?php echo $index; ?>').click(function(){
                        var name = $('#name2<?php echo $index; ?>').val();
                        var ket = $('#ket2<?php echo $index; ?>').val();
                        $.ajax({
                            url : "ajax_coa.php",
                            type : 'POST',
                            dataType: 'json',
                            data: { index: '<?php echo $index; ?>', id: '<?php echo $row[0]; ?>', name: name, ket: ket, type: 'type'},
                            success : function(responseJSON) {
                                if(responseJSON.status == 1){
                                    $('#name2<?php echo $index; ?>').val('');
                                    $('#ket2<?php echo $index; ?>').val('');
                                    alert('Success save data.');
                                    window.location.href='coa.php';
                                }
                                else{
                                    alert('Failed save data.');
                                    return false;
                                }
                            },
                            error : function(){
                                alert('Silahkan coba lagi.');
                            }
                        });
                    });
                </script>

                <?php
                $index2 = 0;
                $sql2 = "select * from dbo.TypeAcc where KodeGroup='$row[0]' order by KodeTipe asc";
                $stmt2 = sqlsrv_query($conn, $sql2);
                while($row2 = sqlsrv_fetch_array($stmt2, SQLSRV_FETCH_NUMERIC)){
                    ?>
                    <div class="modal fade" id="modal-detail<?php echo $index.$index2; ?>">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span></button>
                                    <h4 class="modal-title"><?php echo lang('Tambah Sub Akun'); ?> <?php echo $row2[0]; ?> <?php echo $row2[1]; ?></h4>
                                </div>
                                <div class="modal-body">
                                    <select id="type<?php echo $index.$index2; ?>" class="form-control">
                                        <option value=""><?php echo lang('Pangkat'); ?></option>
                                        <?php for($q=0;$q<=2;$q++){ ?>
                                            <option value="<?php echo $q; ?>"><?php echo $q+3; ?></option>
                                        <?php } ?>
                                    </select>
                                    <input type="text" class="form-control" id="name<?php echo $index.$index2; ?>" placeholder="Account Name">
                                    <input type="text" class="form-control" id="ket<?php echo $index.$index2; ?>" placeholder="Description">
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><?php echo lang('Tutup'); ?></button>
                                    <button type="button" id="btn-det<?php echo $index.$index2; ?>" class="btn btn-primary"><?php echo lang('Simpan'); ?></button>
                                </div>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    <script type="text/javascript">
                        $('#btn-det<?php echo $index.$index2; ?>').click(function(){
                            var type = $('#type<?php echo $index.$index2; ?> :selected').val();
                            var name = $('#name<?php echo $index.$index2; ?>').val();
                            var ket = $('#ket<?php echo $index.$index2; ?>').val();
                            $.ajax({
                                url : "ajax_coa.php",
                                type : 'POST',
                                dataType: 'json',
                                data: { index: type,id: '<?php echo $row2[0]; ?>', name: name, ket: ket, type: 'detail'},
                                success : function(responseJSON) {
                                    if(responseJSON.status == 1){
                                        $('#type<?php echo $index2; ?>').val('');
                                        $('#name<?php echo $index2; ?>').val('');
                                        $('#ket<?php echo $index2; ?>').val('');
                                        alert('Success save data.');
                                        window.location.href='coa.php';
                                    }
                                    else{
                                        alert('Failed save data.');
                                        return false;
                                    }
                                },
                                error : function(){
                                    alert('Silahkan coba lagi.');
                                }
                            });
                        });
                    </script>
                <?php $index2++; } ?>
            <?php $index++; } ?>

            <?php
            $x = "select* from dbo.GroupAcc order by substring (ltrim([KodeGroup]),1,1) asc";
            $y = sqlsrv_query($conn, $x);
            while($z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC)){
                $xx = "select* from dbo.TypeAcc where KodeGroup = '$z[0]'";
                $yy = sqlsrv_query($conn, $xx);
                while($zz = sqlsrv_fetch_array($yy, SQLSRV_FETCH_NUMERIC)){
                    $xxx = "select* from dbo.AccountView where KodeTipe = '$zz[0]' and Header = 0";
                    $yyy = sqlsrv_query($conn, $xxx);
                    while($zzz = sqlsrv_fetch_array($yyy, SQLSRV_FETCH_NUMERIC)){
                        $xxxx = "select* from dbo.AccountView where KodeTipe = '$zz[0]' and Header = 1 and KodeAccount like '%".substr($zzz[0],0,6)."%'";
                        $yyyy = sqlsrv_query($conn, $xxxx);
                        ?>

                        <div class="modal fade" id="modal-detail-form_acclvl1_<?php echo str_replace(".","", $zzz[0]); ?>">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <form name ="formx" action ="prosescoa.php" method="post" id="form_acclvl1_<?php echo str_replace(".","", $zzz[0]); ?>">
                                        <div class ="modal-header">
                                            <label name="lblnm" class="badge">Input Account Di Bawah <?php echo $zzz[1]; ?></label>
                                        </div>
                                        <div class ="modal-body">
                                            <input type="text" class="form-control" name="sacc" placeholder="Nama Account">
                                        </div>
                                        <div class="modal-footer">
                                            <span class="pull-left">
                                                <input type="submit" class="btn btn-default btn-sm" name = "btnsave" value="<?php echo lang('Simpan'); ?>">
                                            </span>
                                            <span class="pull-right">
                                                <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><?php echo lang('Tutup'); ?></button>
                                            </span>

                                            <input type ="hidden" name ="tipex" value ="<?php echo $zzz[2]; ?>">
                                            <input type ="hidden" name ="accx" value ="<?php echo $zzz[0]; ?>">
                                            <input type ="hidden" name ="headerx" value ="<?php echo $zzz[4]; ?>">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <?php
                        while($zzzz = sqlsrv_fetch_array($yyyy, SQLSRV_FETCH_NUMERIC)){
                            ?>

                            <div class="modal fade" id="modal-detail-form_acclvl2_<?php echo str_replace(".","", $zzzz[0]); ?>">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <form name ="formx" action ="prosescoa.php" method="post" id="form_acclvl2_<?php echo str_replace(".","", $zzzz[0]); ?>">
                                            <div class ="modal-header">
                                                <label name="lblnm" class="badge">Input Account Di Bawah <?php echo $zzzz[1]; ?></label>
                                            </div>
                                            <div class ="modal-body">
                                                <input type="text" class="form-control" style="border-radius: 10px" name="sacc" placeholder="Nama Account">
                                            </div>
                                            <div class="modal-footer">
                                            <span class="pull-left">
                                                <input type="submit" class="btn btn-danger" name = "btnsave" value="<?php echo lang('Simpan'); ?>">
                                            </span>
                                                <span class="pull-right">
                                                <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><?php echo lang('Tutup'); ?></button>
                                            </span>

                                                <input type ="hidden" name ="tipex" value ="<?php echo $zzzz[2]; ?>">
                                                <input type ="hidden" name ="accx" value ="<?php echo $zzzz[0]; ?>">
                                                <input type ="hidden" name ="headerx" value ="<?php echo $zzzz[4]; ?>">
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <?php
                        }
                    }
                }
            }
            ?>

        </div>
    </div>
</div>

    <!-- you need to include the ShieldUI CSS and JS assets in order for the TreeView widget to work -->
    <link rel="stylesheet" type="text/css" href="static/plugins/tree/tree.css" />
    <script type="text/javascript" src="static/plugins/tree/tree.js"></script>

    <script type="text/javascript">
        jQuery(function ($) {
            $("#treeview").shieldTreeView();
        });
    </script>

<script type="text/javascript">
    $('#delete').click(function(){
        if(confirm('Are you sure delete all data COA ? data cannot be restore after delete.') == true){
            window.location.href='ucoa.php?delete';
        }
        return false;
    });

    $('#btn-save').click(function(){
        //$('#btn-save').prop('disabled',true);
        $('#tree-loading').removeClass('hide');
    });
</script>

<?php require('content-footer.php');?>

<?php require('footer.php');?>