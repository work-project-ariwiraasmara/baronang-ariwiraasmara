<?php require('header.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<?php
session_start();
include "connect.php";

$qq   = "select * from dbo.UserApprovalLoan where KodeKaryawan = '".$_SESSION['UserID']."' and KID = '".$_SESSION['KID']."'";
$ww  = sqlsrv_query($conn, $qq);
$ee   = sqlsrv_fetch_array( $ww, SQLSRV_FETCH_NUMERIC);
if($ee != null){
?>
    <div class="row">
        <div class="col-md-12">
            <h3>Loan Approval</h3>

            <?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
                <div class="alert alert-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-warning ?>"></i> <?php echo ucfirst($_SESSION['error-type']); ?></h4>
                    <?php echo $_SESSION['error-message']; ?>
                </div>
            <?php } ?>

            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Loan Application List</h3>
                </div>
                <div class="box-body">
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Loan App Number</th>
                            <th>Member ID</th>
                            <th>Name</th>
                            <th>Product</th>
                            <th>Amount</th>
                            <th>Date</th>
                            <th>Amount Approve</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $no = 1;
                        $aa   = "select * from [dbo].[LoanApplicationListView] where StatusComplete = 3 order by TimeStamp desc";
                        $bb  = sqlsrv_query($conn, $aa);
                        while($cc = sqlsrv_fetch_array( $bb, SQLSRV_FETCH_NUMERIC)){
                            $aaa   = "select * from [dbo].[ChatApprovalView] where KodeAppNum = '".$cc[2]."' and Status = 1";
                            $bbb  = sqlsrv_query($conn, $aaa);
                            $ccc = sqlsrv_fetch_array( $bbb, SQLSRV_FETCH_NUMERIC);
                            ?>
                            <tr>
                                <td><?= $no; ?></td>
                                <td><?= $cc[2] ?></td>
                                <td><?= $cc[0] ?></td>
                                <td><?= $cc[1] ?></td>
                                <td><?= $cc[6] ?></td>
                                <td><?= number_format($cc[7]) ?></td>
                                <td><?= date_format($cc[3],"Y/m/d H:i:s"); ?></td>
                                <td>
                                    <b><?php echo number_format($ccc[3]); ?></b>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-info btn-sm btn-user<?php echo $cc[0] ?>" title="Show detail user"><i class="fa fa-user"></i> </button>
                                    <?php if($ccc != null){ ?>
                                        <a href="proccsoaloanapp2.php?t=<?= md5('y') ?>&lan=<?= $cc[2] ?>" title="Approve"><button class="btn btn-success btn-sm" type="button"><i class="fa fa-check"></i></button></a>
                                        <a href="proccsoaloanapp2.php?t=<?= md5('n') ?>&lan=<?= $cc[2] ?>" title="Reject"><button class="btn btn-danger btn-sm" type="button"><i class="fa fa-remove"></i></button></a>
                                    <?php } ?>
                                </td>
                            </tr>

                            <script type="text/javascript">
                                $('.btn-user<?php echo $cc[0] ?>').click(function(){
                                    $('#modal').click();
                                    $.ajax({
                                        url : "ajax_getmember.php",
                                        type : 'POST',
                                        data: { member: '<?php echo $cc[0] ?>'},
                                        success : function(data) {
                                            $(".modal-body").html(data);
                                        },
                                        error : function(){
                                            alert('Try again.');
                                        }
                                    });
                                });
                            </script>
                            <?php $no++; } ?>
                        </tbody>
                    </table>
                </div>
            </div>

            <button type="button" class="btn btn-primary hide" id="modal" data-toggle="modal" data-target="#myModal"></button>

            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Detail</h4>
                        </div>
                        <div class="modal-body">

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } else { ?>
    <div class="alert alert-warning alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-warning"></i> Alert!</h4>
        You are not user approval loan application
    </div>
<?php } ?>
<?php require('content-footer.php');?>

<?php require('footer.php');?>