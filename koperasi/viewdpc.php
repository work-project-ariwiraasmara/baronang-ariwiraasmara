<?php require('header.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<?php
if(isset($_GET['lan']) and isset($_GET['tn']) and isset($_GET['lt'])){

    $LoanApp = $_GET['lan'];
    $TransNum = $_GET['tn'];
    $LoanType = $_GET['lt'];

    $a = "select * from [dbo].[LoanApplicationListView] where LoanAppNum='$LoanApp' and TransactionNumber='$TransNum' and KodeLoanType='$LoanType'";
    $b = sqlsrv_query($conn, $a);
    $c = sqlsrv_fetch_array( $b, SQLSRV_FETCH_NUMERIC);
    if($c == null){
        echo "<script language='javascript'>alert('Not found!');document.location='loan_chat.php';</script>";
    }
?>
    <div class="row">
        <h1 class="col-md-12">Detail Pengajuan Pinjaman</h1>
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Pengajuan Pinjaman</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <div class="box-body">
                    <table class="table table-bordered">
                        <tr>
                            <th>Member ID</th>
                            <td><?php echo $c[0]; ?></td>
                        </tr>
                        <tr>
                            <th>Nama</th>
                            <td><?php echo $c[1]; ?></td>

                        </tr>
                        <tr>
                            <th>Produk Pinjaman</th>
                            <td><?php echo $c[5].' '.$c[6]; ?></td>
                        </tr>
                        <tr>
                            <th>Kode Loan App Number</th>
                            <td><?php echo $c[2]; ?></td>
                        </tr>
                        <tr>
                            <th>Besar Pinjaman</th>
                            <td><?php echo number_format($c[7]); ?></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        Data Penjamin Loan
                    </h3>
                    <div class="box-body">
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Member ID</th>
                                <th>Nama</th>
                                <th>Jabatan</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $aa = "select * from [dbo].[LoanPenjaminView] where LoanAppNum='$LoanApp'";
                            $bb = sqlsrv_query($conn, $aa);
                            while($cc = sqlsrv_fetch_array( $bb, SQLSRV_FETCH_NUMERIC)){
                            ?>
                            <tr>
                                <td><?php echo $cc[1] ?></td>
                                <td><?php echo $cc[2] ?></td>
                                <td><?php echo $cc[6] ?></td>
                                <td>
                                    <?php if($cc[7] == 0){ ?>
                                    <b>Menunggu Persetujuan</b>
                                    <?php } else if($cc[7] == 1){ ?>
                                    <b>Disetujui</b>
                                    <?php } else if($cc[7] == 2){ ?>
                                    <b>Ditolak</b>
                                    <?php } ?>
                                </td>
                            </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Upload Dokumen</h3>
                    <div class="box-body">
                        <table class="table table-bordered table-hover table-striped">
                            <thead>
                            <tr>
                                <th>Nama</th>
                                <th>File</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $sql   = "select * from [dbo].[LoanDocUpload] where LoanAppNum='$LoanApp'";
                            $st  = sqlsrv_query($conn, $sql);
                            while($row = sqlsrv_fetch_array( $st, SQLSRV_FETCH_NUMERIC)){ ?>
                                <tr>
                                    <td><?= $row[1]; ?></td>
                                    <td><a href="<?= $row[2]; ?>" target="_blank" title="Klik untuk melihat dokumen">Klik untuk melihat dokumen</a></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="box-footer">
                <a href="loan_chat.php"><button type="button" class="btn btn-flat btn-default">Kembali Ke Loan Chat</button></a>
            </div>
        </div>
    </div>
<?php } else {
    echo "<script language='javascript'>alert('Invalid require data!');document.location='loan_chat.php';</script>";
}?>
<?php require('content-footer.php');?>

<?php require('footer.php');?>