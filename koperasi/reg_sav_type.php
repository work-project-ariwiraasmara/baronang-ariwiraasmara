<?php require('header.php');?>
<?php require('sidebar-left.php');?>
<?php require('content-header.php');?>

<?php
//edit
$uledit = $_GET['edit'];
$ul0    = "";
$ul1    = "";
$ul2    = "";
$ul3    = "";
$ul4    = "";
$ul5    = "";
$ul6    = "";
$ul7    = "";
$ul8    = "";
$ul9    = "";
$ulprocedit = "";
$uldisabled = "";
$uldisabled2 = "";
if(!empty($uledit)){
    $eulsql   = "select * from dbo.RegularSavingType where RegularSavingType='$uledit'";
    $eulstmt  = sqlsrv_query($conn, $eulsql);
    $eulrow   = sqlsrv_fetch_array( $eulstmt, SQLSRV_FETCH_NUMERIC);
    if(count($eulrow[0]) > 0){
        $ul0    = $eulrow[0];
        $ul1    = $eulrow[1];
        $ul2    = $eulrow[2];
        $ul3    = $eulrow[3];
        $ul4    = substr($eulrow[4], 0, -5);
        $ul5    = substr($eulrow[5],0,-5);
        $ul6    = substr($eulrow[6], 0, -5);
        $ul7    = $eulrow[7];
        $ul8    = $eulrow[8];
        $ul9    = substr($eulrow[9],0, -5);
        $ulprocedit = "?page=".$_GET['page']."&edit=".$ul0;
        $uldisabled = "disabled";
        $uldisabled2 = "readonly";
    }
    else{
        if(empty($_GET['page'])){
            echo "<script language='javascript'>document.location='reg_sav_type.php';</script>";
        }
        else{
            echo "<script language='javascript'>document.location='reg_sav_type.php?page=".$_GET['page']."';</script>";
        }
    }
}

?>

<?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
    <div class="alert alert-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-warning ?>"></i> <?php echo ucfirst($_SESSION['error-type']); ?></h4>
        <?php echo $_SESSION['error-message']; ?>
    </div>
<?php } ?>

<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><?php echo lang('Tipe tabungan'); ?></h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <form class="form-horizontal" action="procreg_sav_type.php<?=$ulprocedit?>" method = "POST">
        <div class="box-body">

            <?php
            $sql   = "select * from dbo.RegularSavingType";
            $stmt  = sqlsrv_query($conn, $sql);
            $row   = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_NUMERIC);
            if($row == null){ ?>
                <div class="row">
                    <div class="col-sm-12">
                        <input type="hidden" name="wiz" value="<?php echo $step; ?>" readonly>
                        <div class="callout">
                            <h4><?php echo lang('Info'); ?>!</h4>

                            <p>
                                <b><?php echo lang('Anda harus membuat satu tipe tabungan'); ?></b>(<?php echo lang('Satu kali input'); ?>)<br>
                                <?php echo lang('Pilih kba'); ?>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="form-group">
                            <label for="regularsavingdescription" class="col-sm-3 control-label" style="text-align: left;"><?php echo lang('Kba'); ?></label>
                            <div class="col-sm-6">
                                <select name="kba" class="form-control" <?=$uldisabled2?>>
                                    <option value="">- <?php echo lang('Pilih salah satu'); ?> -</option>
                                    <?php
                                    $julsql   = "select * from [dbo].[BankAccountView]";
                                    $julstmt = sqlsrv_query($conn, $julsql);
                                    while($rjulrow  = sqlsrv_fetch_array( $julstmt, SQLSRV_FETCH_NUMERIC)){
                                        ?>
                                        <option value="<?=$rjulrow[1];?>" <?php if($rjulrow[1]==$ul5){echo "selected";} ?>>&nbsp;<?=$rjulrow[4];?>&nbsp;<?=$rjulrow[3];?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-sm-2">
                                <button type="submit" class="btn btn-flat btn-block btn-primary pull-right"><?php echo lang('Simpan'); ?></button>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } else { ?>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="regularsavingdescription" class="col-sm-4 control-label" style="text-align: left;"><?php echo lang('Deskripsi'); ?></label>
                            <div class="col-sm-6">
                                <input type="text" name="reg" class="form-control" id="regularsavingdescription" placeholder="" value="<?=$ul1?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="regularsavingdescription" class="col-sm-4 control-label" style="text-align: left;"><?php echo lang('Tipe bunga'); ?></label>
                            <div class="col-sm-6">
                                <?php
                                $a = '';
                                $b = '';
                                $c = '';
                                if($ul2 == 0){
                                    $a = 'selected';
                                }
                                else if($ul2 == 1){
                                    $b = 'selected';
                                }
                                else{
                                    $c = 'selected';
                                }
                                ?>
                                <select name="intt" class="form-control">
                                    <option>- <?php echo lang('Pilih salah satu'); ?> -</option>
                                    <option value="0" <?php echo $a; ?>><?php echo lang('Saldo harian'); ?></option>
                                    <option value="1" <?php echo $b; ?>><?php echo lang('Saldo rata-rata'); ?></option>
                                    <option value="2" <?php echo $c; ?>><?php echo lang('Saldo terendah'); ?></option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="regularsavingdescription" class="col-sm-4 control-label" style="text-align: left;"><?php echo lang('Minimum saldo'); ?></label>
                            <div class="col-sm-6">
                                <input type="text" name="min" class="form-control price" id="regularsavingdescription" placeholder="" value="<?=$ul4?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="regularsavingdescription" class="col-sm-4 control-label" style="text-align: left;"><?php echo lang('Maximum saldo'); ?></label>
                            <div class="col-sm-6">
                                <input type="text" name="max" class="form-control price" id="regularsavingdescription" placeholder="" value="<?=$ul5?>">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="regularsavingdescription" class="col-sm-4 control-label" style="text-align: left;"><?php echo lang('Biaya admin'); ?></label>
                            <div class="col-sm-6">
                                <input type="text" name="adm" class="form-control price" id="regularsavingdescription" placeholder="" value="<?=$ul6?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="regularsavingdescription" class="col-sm-4 control-label" style="text-align: left;"><?php echo lang('Biaya penutupan'); ?></label>
                            <div class="col-sm-6">
                                <input type="text" name="clos" class="form-control price" id="regularsavingdescription" placeholder="" value="<?=$ul9?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="regularsavingdescription" class="col-sm-4 control-label" style="text-align: left;"><?php echo lang('Kba'); ?></label>
                            <div class="col-sm-6">
                                <select name="kba" class="form-control" <?=$uldisabled2?>>
                                    <option value="">- <?php echo lang('Pilih salah satu'); ?> -</option>
                                    <?php
                                    $julsql   = "select * from [dbo].[BankAccountView]";
                                    $julstmt = sqlsrv_query($conn, $julsql);
                                    while($rjulrow  = sqlsrv_fetch_array( $julstmt, SQLSRV_FETCH_NUMERIC)){
                                        ?>
                                        <option value="<?=$rjulrow[1];?>" <?php if($rjulrow[1]==$ul5){echo "selected";} ?>>&nbsp;<?=$rjulrow[4];?>&nbsp;<?=$rjulrow[3];?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <b><?php echo lang('Skema bunga'); ?></b>
                                <table class="table table-bordered table-striped" id="tskema">
                                    <thead>
                                    <tr>
                                        <th><?php echo lang('No'); ?></th>
                                        <th><?php echo lang('Saldo awal'); ?></th>
                                        <th><?php echo lang('Saldo akhir'); ?></th>
                                        <th><?php echo lang('Persentase'); ?></th>
                                        <th></th>
                                    </tr>
                                    <?php
                                    $jum = 0;
                                    $max = 0;
                                    $qwe = "select * from [dbo].[SkemaBunga] where RegularSavingType = '$uledit' order by Urutan asc";
                                    $asd = sqlsrv_query($conn, $qwe);
                                    while($zxc = sqlsrv_fetch_array($asd, SQLSRV_FETCH_NUMERIC)){
                                        $jum+=1;

                                        ?>
                                        <tr>
                                            <td><?php echo $zxc[1]; ?></td>
                                            <td><?php echo number_format($zxc[2]); ?></td>
                                            <td><?php echo number_format($zxc[3]); ?></td>
                                            <td><?php echo $zxc[4]; ?></td>
                                            <td>
                                                <a href="procreg_sav_type.php?del=<?php echo $zxc[0]; ?>&u=<?php echo $zxc[1]; ?>"><button type="button" class="btn btn-sm btn-danger btn-delete"><i class="fa fa-times"></i> </button></a>
                                            </td>
                                        </tr>
                                        <?php
                                        $max = $zxc[3]+1;
                                    }
                                    ?>
                                    <input type="hidden" id="last" value="<?php echo $jum; ?>" readonly>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <td></td>
                                        <td><input type="text" id="start" class="form-control price" value="<?php echo $max; ?>" readonly></td>
                                        <td><input type="text" id="end" class="form-control price" value="0"></td>
                                        <td><input type="text" id="percentage" class="form-control" value="0"></td>
                                        <td><button type="button" class="btn btn-sm btn-success btn-add"><?php echo lang('Tambah'); ?></button></td>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="row">
                        <div class="col-sm-4">
                            <?php if(count($eulrow[0]) > 0){ ?>
                                <button type="submit" class="btn btn-flat btn-block btn-success pull-left"><?php echo lang('Perbaharui'); ?></button>
                            <?php } else { ?>
                                <button type="submit" class="btn btn-flat btn-block btn-primary pull-right"><?php echo lang('Simpan'); ?></button>
                            <?php } ?>
                        </div>
                        <div class="col-sm-4">
                        </div>
                        <div class="col-sm-4">
                            <?php if(count($eulrow[0]) > 0){ ?>
                                <a href="reg_sav_type.php" class="btn btn-flat btn-block btn-default"><?php echo lang('Batal'); ?></a>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </form>
</div>
<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title"><?php echo lang('Daftar tipe tabungan'); ?></h3>
    </div>
    <div class="box-body">
        <div class="col-xs-12 table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th><?php echo lang('No'); ?></th>
                    <th><?php echo lang('Kode tipe tabungan'); ?></th>
                    <th><?php echo lang('Deskripsi'); ?></th>
                    <th><?php echo lang('Tipe bunga'); ?></th>
                    <th><?php echo lang('Minimum saldo'); ?></th>
                    <th><?php echo lang('Maximum saldo'); ?></th>
                    <th><?php echo lang('Biaya admin'); ?></th>
                    <th><?php echo lang('Kba'); ?></th>
                    <th><?php echo lang('Biaya penutupan'); ?></th>
                    <th><?php echo lang('Aksi'); ?></th>
                </tr>
                </thead>
                <tbody>
                <?php
                //count
                $jmlulsql   = "select count(*) from dbo.RegularSavingTypeView";
                $jmlulstmt  = sqlsrv_query($conn, $jmlulsql);
                $jmlulrow   = sqlsrv_fetch_array( $jmlulstmt, SQLSRV_FETCH_NUMERIC);

                //pagging
                $perpages   = 10;
                $halaman    = $_GET['page'];
                if(empty($halaman)){
                    $posisi  = 0;
                    $batas   = $perpages;
                    $halaman = 1;
                }
                else{
                    $posisi  = (($perpages * $halaman) - 10) + 1;
                    $batas   = ($perpages * $halaman);
                }

                $ulsql = "SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY RegularSavingType asc) as row FROM [dbo].[RegularSavingTypeView]) a WHERE row between '$posisi' and '$batas'";
                $ulstmt = sqlsrv_query($conn, $ulsql);
                while($ulrow = sqlsrv_fetch_array( $ulstmt, SQLSRV_FETCH_NUMERIC)){
                    ?>
                    <tr>
                        <td><?=$ulrow[14];?></td>
                        <td><?=$ulrow[0];?>
                        <td><?=$ulrow[1];?></td>
                        <td>
                            <?php
                            $saldo = '';
                            if($ulrow[2] == 0){
                                $saldo = lang('Saldo Harian');
                            }
                            else if($ulrow[2] == 1){
                                $saldo = lang('Saldo Rata-rata');
                            }
                            else{
                                $saldo = lang('Saldo Terendah');
                            }

                            echo $saldo;
                            ?>
                        </td>
                        <td><?=$ulrow[5];?></td>
                        <td><?=$ulrow[7];?></td>
                        <td><?=$ulrow[9];?></td>
                        <td><?=$ulrow[10];?></td>
                        <td><?=$ulrow[13];?></td>
                        <td>
                            <a href="reg_sav_type.php?page=<?=$halaman?>&edit=<?=$ulrow[0]?>" class="btn btn-default btn-flat btn-sm text-green" title="edit"><i class="fa fa-pencil-square-o"></i></a>
                        </td>
                    </tr>
                <?php
                }
                $jmlhalaman = ceil($jmlulrow[0]/$perpages);
                ?>
                </tbody>
            </table>
        </div>
        <div class="box-footer clearfix">
            <label><?php echo lang('Menampilkan'); ?> <?=$halaman?> to <?=$jmlhalaman?> of <?=$jmlulrow[0]?> <?php echo lang('Masukan'); ?></label>
            <?=$posisi." - ".$batas?>
            <ul class="pagination pagination-sm no-margin pull-right">
                <li class="paginate_button"><a href="reg_sav_type.php">&laquo;</a></li>
                <?php
                for($ul = 1; $ul <= $jmlhalaman; $ul++){
                    if($ul != $halaman){ $ulpageactive = ""; $ulpagedisabled = "";}else{$ulpageactive = "active"; $ulpagedisabled = "disabled";}
                    echo '<li class="paginate_button '.$ulpageactive.'"><a href="reg_sav_type.php?page='.$ul.'" '.$ulpagedisabled.'>'.$ul.'</a></li>';
                }
                ?>
                <li class="paginate_button"><a href="reg_sav_type.php?page=<?=$jmlhalaman?>">&raquo;</a></li>
            </ul>
        </div>
    </div>
</div>

<script type="text/javascript">
    var tampung = [];

    $('.btn-delete').click(function(){
        if(confirm('<?php echo lang('Urutan dibawah kolom ini akan dihapus juga. Apakah anda yakin'); ?> ?') == false){
            return false;
        }
    });

    $('.btn-add').click(function(){
        var start = $('#start').val();
        var end = $('#end').val();
        var percent = $('#percentage').val();
        var last = $('#last').val();

        if(start == '' || end == '' || percent == ''){
            alert('<?php echo lang('Harap isi inputan dengan benar'); ?>');
            return false;
        }
        else if(jQuery.inArray(start, tampung) !== -1 || jQuery.inArray(end, tampung) !== -1){
            alert('<?php echo lang('Jumlah saldo harus berbeda dengan yang sudah ditambahkan sebelumnya'); ?>');
            return false;
        }
        else if(parseInt(start) > parseInt(end)){
            alert('<?php echo lang('Saldo akhir harus lebih besar'); ?>');
            return false;
        }
        else{
            $('#start').val(parseInt(end)+1);

            $.ajax({
                url : "ajax_addskema.php",
                type : 'POST',
                dataType : 'json',
                data: { start: start, end: end, percent: percent},   // data POST yang akan dikirim
                success : function(data) {
                    tampung.push(data.start);
                    tampung.push(data.end);

                    var urutan = parseInt(last)+1;

                    $('#last').val(urutan);

                    $("#tskema tbody").append("<tr>" +
                        "<td><input type='hidden' name='urutan[]' class='urutan' value="+ urutan +">" + urutan + "</td>" +
                        "<td><input type='hidden' name='start[]' value="+ data.start +">" + data.start + "</td>" +
                        "<td><input type='hidden' name='end[]' value="+ data.end +">" + data.end + "</td>" +
                        "<td><input type='hidden' name='percentage[]' value="+ data.percent +">" + data.percent + "</td>" +
                        "<td><a class='btn btn-default btn-flat btn-sm text-red remove' onclick='$(this).parent().parent().remove();'><i class='fa fa-trash-o'></i></a></td>" +
                        "</tr>");
                    $('#end').val('0');
                    $('#percentage').val('0');
                },
                error : function() {
                    alert('<?php echo lang('Telah terjadi error'); ?>');
                    return false;
                }
            });
        }
    });
</script>

<?php require('content-footer.php');?>

<?php require('footer.php');?>
