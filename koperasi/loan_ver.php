<?php require('header.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<?php
$qq = "select * from dbo.UserApprovalLoan where KodeKaryawan = '".$_SESSION['UserID']."' and KID = '".$_SESSION['KID']."'";
$ww = sqlsrv_query($conn, $qq);
$ee = sqlsrv_fetch_array( $ww, SQLSRV_FETCH_NUMERIC);
if($ee != null){
?>
<div class="row">
    <div class="col-md-12">
        <h3><?php echo lang('Permohonan Pinjaman'); ?></h3>

        <?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
            <div class="alert alert-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-warning ?>"></i> <?php echo ucfirst($_SESSION['error-type']); ?></h4>
                <?php echo $_SESSION['error-message']; ?>
            </div>
        <?php } ?>

        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title"><?php echo lang('Daftar Permohonan Pinjaman'); ?></h3>
            </div>
            <div class="box-body">
                <?php if(isset($_GET['id'])){ ?>
                    <a href="loan_ver.php"><button type="button" class="btn btn-primary btn-sm">Reset</button> </a>
                <?php } ?>
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th><?php echo lang('No App Pinjaman'); ?></th>
                        <th>Member ID</th>
                        <th><?php echo lang('Nama'); ?></th>
                        <th><?php echo lang('Produk'); ?></th>
                        <th><?php echo lang('Jumlah'); ?></th>
                        <th><?php echo lang('Tanggal'); ?></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $no = 1;
                    if(isset($_GET['id'])){
                        $aa   = "select * from [dbo].[LoanApplicationListView] where LoanAppNum = $_GET[id] and StatusComplete = 1 order by TimeStamp desc";
                    }
                    else{
                        $aa   = "select * from [dbo].[LoanApplicationListView] where StatusComplete = 1 order by TimeStamp desc";
                    }
                    $bb  = sqlsrv_query($conn, $aa);
                    while($cc = sqlsrv_fetch_array( $bb, SQLSRV_FETCH_NUMERIC)){
                    ?>
                        <tr>
                            <td><?= $no; ?></td>
                            <td><?= $cc[2] ?></td>
                            <td><?= $cc[0] ?></td>
                            <td><?= $cc[1] ?></td>
                            <td><?= $cc[6] ?></td>
                            <td><?= number_format($cc[7]) ?></td>
                            <td><?= date_format($cc[3],"Y/m/d H:i:s"); ?></td>
                            <td>
                                <button type="button" class="btn btn-info btn-sm btn-user<?php echo $cc[0] ?>" title="Show detail user"><i class="fa fa-user"></i> </button>
                                <?php if(isset($_GET['id'])){ ?>
                                    <a href="procloanver.php?app=<?php echo $cc[2]; ?>"><button type="button" class="btn btn-success btn-sm" title="Approve"><i class="fa fa-check"></i> Verify</button></a>
                                <?php } else { ?>
                                    <a href="loan_ver.php?id=<?php echo $cc[2]; ?>"><button type="button" class="btn btn-info btn-sm" title="Show detail"><i class="fa fa-book"></i> Show</button></a>
                                <?php } ?>
                            </td>
                        </tr>
                        <?php if(isset($_GET['id'])){ ?>
                            <tr>
                                <td colspan="8">
                                    <div class="col-sm-6">
                                        <b><?php echo lang('Penjamin'); ?></b>
                                        <table class="table">
                                            <?php
                                            $aaa   = "select * from [dbo].[LoanPenjaminView] where LoanAppNum = '".$cc[2]."'";
                                            $bbb  = sqlsrv_query($conn, $aaa);
                                            while($ccc = sqlsrv_fetch_array( $bbb, SQLSRV_FETCH_NUMERIC)){
                                                $status = '';
                                                if($ccc[7] == 0){
                                                    $status = 'Not Approve';
                                                }
                                                else if($ccc[7] == 1){
                                                    $status = 'Approved';
                                                }
                                                else{
                                                    $status = 'Reject';
                                                }
                                                ?>
                                            <tr>
                                                <td><?php echo $ccc[1]; ?></td>
                                                <td><?php echo $ccc[2]; ?></td>
                                                <td><?php echo $status; ?></td>
                                                <td>
                                                    <button type="button" class="btn btn-info btn-sm btn-duser<?php echo $ccc[1] ?>" title="Show detail user"><i class="fa fa-user"></i> </button>
                                                </td>
                                            </tr>

                                                <script type="text/javascript">
                                                    $('.btn-duser<?php echo $ccc[1] ?>').click(function(){
                                                        $('#modal').click();
                                                        $.ajax({
                                                            url : "ajax_getmember.php",
                                                            type : 'POST',
                                                            data: { member: '<?php echo $ccc[1] ?>'},
                                                            success : function(data) {
                                                                $(".modal-body").html(data);
                                                            },
                                                            error : function(){
                                                                alert('Try again.');
                                                            }
                                                        });
                                                    });
                                                </script>
                                            <?php } ?>
                                        </table>
                                    </div>
                                    <div class="col-sm-6">
                                        <b><?php echo lang('Upload Dokumen'); ?></b>
                                        <table class="table">
                                            <?php
                                            $aaa   = "select * from [dbo].[LoanDocUpload] where LoanAppNum = '".$cc[2]."'";
                                            $bbb  = sqlsrv_query($conn, $aaa);
                                            while($ccc = sqlsrv_fetch_array( $bbb, SQLSRV_FETCH_NUMERIC)){
                                                ?>
                                                <tr>
                                                    <td><?php echo $ccc[1]; ?></td>
                                                    <td><a href="appmobile/<?php echo $ccc[2]; ?>"><?php echo lang('Tampilkan Dokumen'); ?></a></td>
                                                    <td>
                                                        <a href="procloanver.php?del=<?php echo $ccc[0]; ?>&doc=<?php echo $ccc[1]; ?>"><button type="button" class="btn btn-danger btn-sm" title="Delete doc"><i class="fa fa-times"></i> </button>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        <?php } ?>

                        <script type="text/javascript">
                            $('.btn-user<?php echo $cc[0] ?>').click(function(){
                                $('#modal').click();
                                $.ajax({
                                    url : "ajax_getmember.php",
                                    type : 'POST',
                                    data: { member: '<?php echo $cc[0] ?>'},
                                    success : function(data) {
                                        $(".modal-body").html(data);
                                    },
                                    error : function(){
                                        alert('Try again.');
                                    }
                                });
                            });
                        </script>
                    <?php $no++; } ?>
                    </tbody>
                </table>
            </div>
        </div>

        <button type="button" class="btn btn-primary hide" id="modal" data-toggle="modal" data-target="#myModal"></button>

        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel"><?php echo lang('Detail'); ?></h4>
                    </div>
                    <div class="modal-body">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang('Tutup'); ?></button>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<?php } else { ?>
    <div class="alert alert-warning alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-warning"></i> Alert!</h4>
        You are not user approval loan application
    </div>
<?php } ?>

<?php require('content-footer.php');?>

<?php require('footer.php');?>