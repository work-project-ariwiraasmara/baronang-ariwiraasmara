<?php require('header.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
    <div class="alert alert-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-warning ?>"></i> <?php echo ucfirst($_SESSION['error-type']); ?></h4>
        <?php echo $_SESSION['error-message']; ?>
    </div>
<?php } ?>

<div class="row">
    <!-- left column -->
    <div class="col-md-6">
        <!-- Horizontal Form -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Change Password</h3>
            </div><!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" action="acchange_pass.php" method="POST">
                <div class="box-body">
                    <div class="form-group">
                        <label for="oldpass" class="col-sm-4 control-label">Old Password</label>
                        <div class="col-sm-8">
                            <input type="password" class="form-control" name="oldpass" id="oldpass" placeholder="Old Password">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="pass" class="col-sm-4 control-label">New Password</label>
                        <div class="col-sm-8">
                            <input type="password" class="form-control" name="pass" id="pass" placeholder="Password">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="repass" class="col-sm-4 control-label">Retype Password</label>
                        <div class="col-sm-8">
                            <input type="password" class="form-control" name="repass" id="repass" placeholder="Retype Password">
                        </div>
                    </div>
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-flat btn-primary">Change</button>
                </div><!-- /.box-footer -->
            </form>
        </div><!-- /.box -->
    </div><!--/.col (right) -->
</div>   <!-- /.row -->

<?php require('content-footer.php');?>

<?php require('footer.php');?>