<?php
include "connectinti.php";

$a = "select * from [dbo].[WizSeting] where Link = '$_SESSION[FileAkses]'";
$b = sqlsrv_query($conns, $a);
$c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC);
$s = 1;
$st = 0;
if($c != null){
    $s = $c[0];
    $st = $c[2];

    if($_SESSION['error-type'] == 'success'){
        $sql = "exec [dbo].[ProsesWiz] '$_SESSION[KID]', '$c[0]','1'";
        $stmt = sqlsrv_query($conn, $sql);
    }
}

$step = '';
$name = '';
$link = '';
$hint = '';
$status = '';
$skip = 0;
$aa = "select TOP 1 * from [dbo].[Wiz] where KID = '$_SESSION[KID]' order by Step desc";
$bb = sqlsrv_query($conn, $aa);
$cc = sqlsrv_fetch_array( $bb, SQLSRV_FETCH_NUMERIC);
if($cc == null){
    $a = "select * from [dbo].[WizSeting] where Step = 1";
    $b = sqlsrv_query($conns, $a);
    $c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC);
    if($c != null){
        $step = $c[0];
        $name = $c[1];
        $link = $c[3];
        $hint = $c[4];
        $status = $c[2];
        $skip = $c[5];
    }
}
else{
    $urut = $cc[1]+1;
    $a = "select * from [dbo].[WizSeting] where Step = '$urut'";
    $b = sqlsrv_query($conns, $a);
    $c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC);
    if($c != null){
        $step = $c[0];
        $name = $c[1];
        $link = $c[3];
        $hint = $c[4];
        $status = $c[2];
        $skip = $c[5];
    }
}
?>

<?php if($step != ''){

    $linknext = '';
    $stepnext = '';
    $s = $step+1;
    $aa = "select * from [dbo].[WizSeting] where Step = '$s'";
    $bb = sqlsrv_query($conns, $aa);
    $cc = sqlsrv_fetch_array($bb, SQLSRV_FETCH_NUMERIC);
    if($cc != null){
        $stepnext = $cc[0];
        $linknext = $cc[3];
    }
    ?>
    <div class="box box-primary">
        <div class="box-header with-border">
            <div class="wizard">
                <div class="wizard-inner">
                    <div class="connecting-line"></div>
                    <ul class="nav nav-tabs">
                        <?php
                        $p = "select * from [dbo].[WizSeting] order by Step asc";
                        $l = sqlsrv_query($conns, $p);
                        while($m = sqlsrv_fetch_array($l, SQLSRV_FETCH_NUMERIC)){
                            $stat = 'disabled';
                            $bg='';
                            if($step == $m[0]){
                                $stat = 'active';
                                $bg = 'bg-light-blue';
                            }

                            $ss = $m[0];
                            if($step > $m[0]){
                                $ss = '<i class="fa fa-check"></i>';
                            }
                            ?>
                            <li class="<?php echo $stat; ?>">
                                <a href="<?php echo $m[3]; ?>" title="<?php echo 'Step '.$m[0].' - '.$m[1]; ?>">
                                    <span class="round-tab <?php echo $bg; ?>"><?php echo $ss; ?></span>
                                </a>
                            </li>
                        <?php } ?>
                        <li class="pull-right">
                            <a href="#" title="Complete">
                                <span class="round-tab bg-gray"><i class="fa fa-flag-checkered"></i> </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <h3 class="box-title">Complete Step <?php echo $step; ?> | <?php echo $name; ?></h3>

            <?php if($stepnext != ''){ ?>
                <a href="<?php echo $linknext; ?>"><button type="button" class="btn btn-sm btn-success pull-right btn-wiz"><i class="fa fa-mail-forward"></i> Next Step <?php echo $stepnext; ?></button></a>
            <?php } ?>

            <?php if($skip == 1){ ?>
                <button id="btn-skip" type="button" class="btn btn-sm btn-warning pull-right btn-wiz"><i class="fa fa-times"></i> Skip Step <?php echo $step; ?></button>
            <?php } ?>

            <?php
            $back = $step-1;
            if($back > 0){
                $a = "select * from [dbo].[WizSeting] where Step = '$back'";
                $b = sqlsrv_query($conns, $a);
                $c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC);
                if($c != null){
                ?>
                    <a href="<?php echo $c[3]; ?>"><button type="button" class="btn btn-sm btn-default btn-wiz pull-right"><i class="fa fa-mail-reply"></i> Back to Step <?php echo $back; ?></button></a>
            <?php }
            } ?>

            <button type="button" id="btn-wiz" class="btn btn-primary btn-wiz hide" data-toggle="modal" data-target=".bs-example-modal-lg" data-backdrop="static">Large modal</button>

            <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close btn-wiz" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Complete The Step <?php echo $step; ?> | <?php echo $name; ?></h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <?php echo $hint; ?>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left btn-wiz" data-dismiss="modal">Close</button>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php
    if($link != $_SESSION['FileAkses'] and $s > $step and $_SESSION['FileAkses'] != 'bas_sav_type.php' and $_SESSION['FileAkses'] != 'mprofile.php' and $_SESSION['FileAkses'] != 'mposition.php' or $st == 0 and $link != $_SESSION['FileAkses'] and $_SESSION['FileAkses'] != 'bas_sav_type.php' and $_SESSION['FileAkses'] != 'mprofile.php' and $_SESSION['FileAkses'] != 'mposition.php'){ ?>
        <script type="text/javascript">
            window.onload = function(){
                $('#btn-wiz').click();

                $("input[type=submit]").attr('disabled',true);
                $("input[type=button]").attr('disabled',true);

                $("button[type=submit]").attr('disabled',true);
                $("button[type=button]").attr('disabled',true);

                $('.btn-wiz').attr('disabled',false);
            };
        </script>
    <?php } else { ?>
        <script type="text/javascript">
            window.onload = function(){
                $('#btn-wiz').click();
            };
        </script>
    <?php } ?>

    <script type="text/javascript">
        $('#btn-skip').click(function(){
            if(confirm('Apakah anda yakin melewati step ini ?')){
                window.location.href='procwizard.php?step=<?php echo $step; ?>';
            }

            return false;
        });
    </script>
<?php } ?>

<?php
//filter hak akses
$fgh = "select * from [dbo].[UserLevelMenu] where KodeUserLevel='$_SESSION[KodeJabatan]' and MenuName='$_SESSION[FileAkses]'";
$cvb = sqlsrv_query($conn, $fgh);
$wer = sqlsrv_fetch_array($cvb, SQLSRV_FETCH_NUMERIC);
if($wer == null and $_SESSION['FileAkses'] != 'index.php' and $_SESSION['FileAkses'] != 'dashboard.php' and $_SESSION['FileAkses'] != 'authorize.php' and $_SESSION['FileAkses'] != 'change_pass.php' and $_SESSION['FileAkses'] != 'baronang' and $_SESSION['FileAkses'] != 'site'){
    //echo "<script language='javascript'>document.location='authorize.php';</script>";
}
?>

<!-- Main content -->
<section class="content body-idcs2">