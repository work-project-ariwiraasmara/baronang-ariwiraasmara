<?php require('header.php');?>      

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

    <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Basic Saving Withdrawal</h3>
        </div>
        <form action="#" method="POST" class="form-horizontal">
			<div class="box-body">
				<div class="row">
					<div class="col-md-12">
						<div class="col-sm-6">
							<div class="form-group">
								<label for="csoaregsavmember" class="col-sm-3 control-label" style="text-align: left;">Member ID</label>
								<div class="col-sm-6">
								<input type="text" name="member" class="form-control" id="csoaregsavmember" placeholder="" value="<?=$ul0?>" <?=$uldisabled;?>>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
						  <div class="form-group">
							<label for="nama" class="col-sm-2 control-label" style="text-align: left;">Name</label>
							<div class="col-sm-6">
							  <input type="text" name="nama" class="form-control" id="nama" placeholder="" value="<?=$ul1?>" disabled>
							</div>
						  </div>
						</div>
				</div>
					<div class="col-sm-12" style="height:20px;">
					</div>
					<div class="col-sm-12">
						<div class="col-sm-4" style="border:1px solid #d2d6de; border-radius:15px;">
							<div class="col-sm-12" style="margin-bottom:10px;">
								<label class="control-label text-center" style="width:100%;text-align: center;">Simpanan Pokok</label>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<small for="name" class="col-sm-3" style="text-align: left;">Payment Amount</small>
									<div class="col-sm-9">
										<input type="text" name="name1" class="form-control" id="name" placeholder="">
									</div>
								</div>
								<div class="form-group">
									<small for="name" class="col-sm-3" style="text-align: left;">Outstanding Amount</small>
									<div class="col-sm-9">
										<input type="text" name="name2" class="form-control" id="name" placeholder="">
									</div>
								</div>
								<div class="form-group">
									<small for="name" class="col-sm-3" style="text-align: left;">Account Number</small>
									<div class="col-sm-9">
										<input type="text" name="name3" class="form-control" id="name" placeholder="">
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-4" style="border:1px solid #d2d6de; border-radius:15px;">
							<div class="col-sm-12" style="margin-bottom:10px;">
								<label class="control-label" style="width:100%;text-align: center;">Simpanan Wajib</label>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<small for="name" class="col-sm-3" style="text-align: left;">Payment Amount</small>
									<div class="col-sm-9">
										<input type="text" name="name" class="form-control" id="name" placeholder="">
									</div>
								</div>
								<div class="form-group">
									<small for="name" class="col-sm-3" style="text-align: left;">Outstanding Amount</small>
									<div class="col-sm-9">
										<input type="text" name="name" class="form-control" id="name" placeholder="">
									</div>
								</div>
								<div class="form-group">
									<small for="name" class="col-sm-3" style="text-align: left;">Account Number</small>
									<div class="col-sm-9">
										<input type="text" name="name" class="form-control" id="name" placeholder="">
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-4" style="border:1px solid #d2d6de; border-radius:15px;">
							<div class="col-sm-12" style="margin-bottom:10px;">
								<label class="control-label" style="width:100%;text-align: center;">Simpanan Sukarela</label>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<small for="name" class="col-sm-3" style="text-align: left;">Payment Amount</small>
									<div class="col-sm-9">
										<input type="text" name="name" class="form-control" id="name" placeholder="">
									</div>
								</div>
								<div class="form-group">
									<small for="name" class="col-sm-3" style="text-align: left;">Outstanding Amount</small>
									<div class="col-sm-9">
										<input type="text" name="name" class="form-control" id="name" placeholder="">
									</div>
								</div>
								<div class="form-group">
									<small for="name" class="col-sm-3" style="text-align: left;">Account Number</small>
									<div class="col-sm-9">
										<input type="text" name="name" class="form-control" id="name" placeholder="">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>	
			</div>
		    <div class="box-footer">
				<div class="row">
					<div class="col-sm-4">
					</div>
					<div class="col-sm-4">
						<button type="submit" class="btn btn-flat btn-block btn-primary pull-right">Save</button>
					</div>
					<div class="col-sm-4">
					</div>
				</div>
			</div>
        </form>
    </div>

<?php  require('content-footer.php');?>
<?php  require('footer.php');?>