<?php
session_start();
error_reporting(0);

include('lib/phpExcel/Classes/PHPExcel.php');
include "connect.php";

$a = "select * from [dbo].[MemberList]";
$b = sqlsrv_query($conn, $a);
$c = sqlsrv_fetch_array( $b, SQLSRV_FETCH_NUMERIC);

$aa = "select * from [dbo].[TimeDepositType]";
$bb = sqlsrv_query($conn, $aa);
$cc = sqlsrv_fetch_array( $bb, SQLSRV_FETCH_NUMERIC);

$aaa = "select * from [dbo].[RegularSavingType]";
$bbb = sqlsrv_query($conn, $aaa);
$ccc = sqlsrv_fetch_array( $bbb, SQLSRV_FETCH_NUMERIC);

if($c == null){
    messageAlert('Member belum di set. Tidak dapat menndownload template.','warning');
    header('Location: mtime_balance.php');
}
else if($cc == null){
    messageAlert('Time deposit type belum di set. Tidak dapat menndownload template.','warning');
    header('Location: mtime_balance.php');
}
else if($ccc == null){
    messageAlert('Regular saving type belum di set. Tidak dapat menndownload template.','warning');
    header('Location: mtime_balance.php');
}
else{

    $filePath = "uploads/excel/";

    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getProperties()->setCreator("baronang");
    $objPHPExcel->getProperties()->setTitle("Data Upload TD Balance");

// set autowidth
    for($col = 'A'; $col !== 'Z'; $col++) {
        $objPHPExcel->getActiveSheet()
            ->getColumnDimension($col)
            ->setAutoSize(true);
    }

    $listCombo = array();

    //sheet 3
    $objWorkSheet3 = $objPHPExcel->createSheet(1);
// baris judul
    $objWorkSheet3->SetCellValue('A1', 'No');
    $objWorkSheet3->SetCellValue('B1', 'Product Name');
    $objWorkSheet3->SetCellValue('C1', 'Interest Rate %');
    $objWorkSheet3->SetCellValue('D1', 'Contract Period');
    $objWorkSheet3->SetCellValue('E1', 'Interest Pinalty');
    $objWorkSheet3->SetCellValue('F1', 'Minimum Amount');
    $objWorkSheet3->SetCellValue('G1', 'Pinalty Fee');

    $no = 1;
    $row = 2;

    $x = "select * from [dbo].[TimeDepositType]";
    $y = sqlsrv_query($conn, $x);
    $type = '';
    while($z = sqlsrv_fetch_array( $y, SQLSRV_FETCH_NUMERIC)){
        array_push($listCombo, $z[1]);

        $objWorkSheet3->SetCellValue("A".$row, $no);
        $objWorkSheet3->SetCellValue("B".$row, $z[1]);
        $objWorkSheet3->SetCellValue("C".$row, $z[2]);
        $objWorkSheet3->SetCellValue("D".$row, $z[3]);
        $objWorkSheet3->SetCellValue("E".$row, $z[6]);
        $objWorkSheet3->SetCellValue("F".$row, number_format($z[4]));
        $objWorkSheet3->SetCellValue("G".$row, number_format($z[5]));


        $no++;
        $row++;
    }
    $objWorkSheet3->setTitle('Data Master Produk TD');

    $objWorkSheet = $objPHPExcel->createSheet(0);
// baris judul
    $objWorkSheet->SetCellValue('A1', 'Nama');
    $objWorkSheet->SetCellValue('B1', 'NIP');
    $objWorkSheet->SetCellValue('C1', 'KTP');
    $objWorkSheet->SetCellValue('D1', 'Email');
    $objWorkSheet->SetCellValue('E1', 'Telepon');
    $objWorkSheet->SetCellValue('F1', 'Regular Saving Acc');
    $objWorkSheet->SetCellValue('G1', 'Nama Produk');
    $objWorkSheet->SetCellValue('H1', 'Nomor Rekening Lama');
    $objWorkSheet->SetCellValue('I1', 'Saldo Awal');
    $objWorkSheet->SetCellValue('J1', 'Tanggal Awal');
    $objWorkSheet->SetCellValue('K1', 'Contract Period');
    $objWorkSheet->SetCellValue('L1', 'Bunga %');
    $objWorkSheet->SetCellValue('M1', 'ARO');
    $objWorkSheet->SetCellValue('N1', 'Bunga Ditransfer Bulanan');
    $objWorkSheet->SetCellValue('O1', 'Bunga Transfer Saat ARO');

    //looping isi combo box
    $row = 2;
    for($no=1;$no<=100;$no++){
        $combo = $objWorkSheet->GetCell("G".$row)->getDataValidation();
        $combo->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
        $combo->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
        $combo->setAllowBlank(false);
        $combo->setShowInputMessage(true);
        $combo->setShowErrorMessage(true);
        $combo->setShowDropDown(true);
        $combo->setErrorTitle('Input error');
        $combo->setError('Value is not in list.');
        $combo->setPromptTitle('Pick from list');
        $combo->setPrompt('Please pick a value from the drop-down list.');
        $combo->setFormula1('"'.implode(',',$listCombo).'"');

        $tanggal = $objWorkSheet->GetCell("J".$row)->getDataValidation();
        $tanggal->setShowInputMessage(true);
        $tanggal->setPromptTitle('Format date example');
        $tanggal->setPrompt('2000/01/01 or 2000-01-01');

        $row++;
    }

    $objWorkSheet->setTitle('Data Upload TD Balance');

//sheet 2
    $objWorkSheet2 = $objPHPExcel->createSheet(2);
// baris judul
    $objWorkSheet2->SetCellValue('A1', 'Member ID');
    $objWorkSheet2->SetCellValue('B1', 'No. KTP');
    $objWorkSheet2->SetCellValue('C1', 'NIP');
    $objWorkSheet2->SetCellValue('D1', 'Nama');
    $objWorkSheet2->SetCellValue('E1', 'Telepon');
    $objWorkSheet2->SetCellValue('F1', 'Email');
    $objWorkSheet2->SetCellValue('G1', 'Regular Saving Acc');
    $objWorkSheet2->SetCellValue('H1', 'Regular Saving Name');

    $no = 1;
    $row = 2;

    $x = "select * from [dbo].[RegSavingAccView]";
    $y = sqlsrv_query($conn, $x);
    while($z = sqlsrv_fetch_array( $y, SQLSRV_FETCH_NUMERIC)){
        $objWorkSheet2->SetCellValueExplicit("A".$row, $z[1], PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet2->SetCellValueExplicit("B".$row, $z[9], PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet2->SetCellValue("C".$row, $z[8]);
        $objWorkSheet2->SetCellValue("D".$row, $z[7]);
        $objWorkSheet2->SetCellValueExplicit("E".$row, $z[11], PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet2->SetCellValue("F".$row, $z[10]);
        $objWorkSheet2->SetCellValueExplicit("G".$row, $z[2], PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet2->SetCellValue("H".$row, $z[4]);

        $no++;
        $row++;
    }
    $objWorkSheet2->setTitle('Data Master Member');

    $fileName = 'templateTimeDepositBalance'.'_'.strtotime(date('Y-m-d H:i:s')).'.xls';
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');

    // download ke client
    header('Content-type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename="'.$fileName.'"');
    $objWriter->save('php://output');

    return $filePath.'/'.$fileName;
}
?>