<?php require('header.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<?php
if($_SESSION['RequestExpired'] < date('Y-m-d H:i:s')){
    unset($_SESSION['RequestID']);
    unset($_SESSION['RequestLink']);
    unset($_SESSION['RequestExpired']);
    unset($_SESSION['RequestMember']);
    unset($_SESSION['RequestMemberName']);

    $_SESSION['error-type'] = 'info';
    $_SESSION['error-message'] = 'Your Session Has Expired. Retry input User ID Baronang App.';
    $_SESSION['error-time'] = time() + 5;
    echo "<script language='javascript'>document.location='identity_cs.php';</script>";
}
?>

<?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
    <div class="alert alert-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-warning ?>"></i> <?php echo ucfirst($_SESSION['error-type']); ?></h4>
        <?php echo $_SESSION['error-message']; ?>
    </div>
<?php } ?>

<div class="row">
    <div class="col-sm-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Time Deposit Open</h3>
            </div>
            <form action="proccsoatimedeposit.php" method="POST" class="form-horizontal">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="csoaregsavmember" class="col-sm-4 control-label" style="text-align: left;">Member ID</label>
                                    <div class="col-sm-8">
                                        <input type="number" name="member" class="form-control" id="csoaregsavmember" placeholder="" value="<?php echo $_SESSION['RequestMember']; ?>" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="nama" class="col-sm-5 control-label" style="text-align: left;">Name</label>
                                    <div class="col-sm-7">
                                        <input type="text" name="nama" class="form-control" id="nama" placeholder="" value="<?php echo $_SESSION['RequestMemberName']; ?>" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" style="text-align: left;">Time Deposit Type</label>
                                    <div class="col-sm-8">
                                        <select name="timedep" class="form-control">
                                            <option>--- Select One ---</option>
                                            <?php
                                            $amcsoabssql         = "select * from [dbo].[TimeDepositType] order by KodeTimeDepositType";
                                            $amcsoabsstmt        = sqlsrv_query($conn, $amcsoabssql);
                                            while($amcsoabsrow   = sqlsrv_fetch_array( $amcsoabsstmt, SQLSRV_FETCH_NUMERIC)){
                                                ?>
                                                <option value="<?=$amcsoabsrow[0];?>"><?=$amcsoabsrow[1];?></option>
                                            <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" style="text-align: left;">Regular Saving Account</label>
                                    <div class="col-sm-8">
                                        <select name="reg" class="form-control">
                                            <option>--- Select One ---</option>
                                            <?php
                                            $ssql = "SELECT a.[KID]
                                                  ,a.[MemberID]
                                                  ,a.[AccountNo]
                                                  ,a.[RegularSavingType]
                                                  ,a.[Balance]
                                                  ,b.[RegularSavingTypeDesc]
                                              FROM [dbo].[RegularSavingAcc] as a, [dbo].[RegularSavingType] as b where a.[RegularSavingType] = b.[RegularSavingType]
                                              and a.[MemberID] = '".$_SESSION['RequestMember']."' and a.Status in(1,9)";
                                            $sstmt = sqlsrv_query($conn, $ssql);
                                            while($srow = sqlsrv_fetch_array( $sstmt, SQLSRV_FETCH_NUMERIC)){
                                                ?>
                                                <option value="<?php echo $srow[2]; ?>"><?php echo $srow[2].' - '.$srow[5];?></option>
                                            <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="amount" class="col-sm-4 control-label" style="text-align: left;">Amount</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="amount" class="form-control price" id="amount" placeholder="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="aro" class="col-sm-5 control-label" style="text-align: left;">ARO</label>
                                    <div class="col-sm-4">
                                        <label style="padding-left: 10px">
                                            <input type="radio" name="aro" class="minimal" id="aro" value="1">
                                            Yes
                                        </label>
                                        <label>
                                            <input type="radio" name="aro" class="minimal" id="aro" value="0" checked>
                                            No
                                        </label>
                                    </div>
                                    <div class="col-sm-3">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="itm" class="col-sm-5 control-label" style="text-align: left;">Interest Transfer Monthly</label>
                                    <div class="col-sm-4">
                                        <label style="padding-left: 10px">
                                            <input type="radio" name="itm" class="minimal" id="itm" value="1">
                                            Yes
                                        </label>
                                        <label>
                                            <input type="radio" name="itm" class="minimal" id="itm" value="0" checked>
                                            No
                                        </label>
                                    </div>
                                    <div class="col-sm-3">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="itaa" class="col-sm-5 control-label" style="text-align: left;">Interest Transfer At ARO</label>
                                    <div class="col-sm-4">
                                        <label style="padding-left: 10px">
                                            <input type="radio" name="itaa" class="minimal" id="itaa" value="1">
                                            Yes
                                        </label>
                                        <label>
                                            <input type="radio" name="itaa" class="minimal" id="itaa" value="0" checked>
                                            No
                                        </label>
                                    </div>
                                    <div class="col-sm-3">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="row">
                        <div class="col-sm-4">
                            <button type="submit" class="btn btn-flat btn-block btn-primary pull-right">Save</button>
                        </div>
                        <div class="col-sm-4">
                        </div>
                        <div class="col-sm-4">
                            <a href="identity_cs.php"><button type="button" class="btn btn-success btn-flat btn-block">Back</button></a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<?php require('content-footer.php');?>

<?php require('sidebar-right.php');?>

<?php require('footer.php');?>
