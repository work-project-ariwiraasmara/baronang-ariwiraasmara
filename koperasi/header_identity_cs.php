<?php
error_reporting(0);
include "connect.php";

$KID = $_SESSION['KID'];
$NamaKop = $_SESSION['NamaKoperasi'];

$UserID = $_SESSION['UserID'];
$NameUser = $_SESSION['Name'];
$JabatanUser = $_SESSION['KodeJabatan'];

$filepath = basename($_SERVER['REQUEST_URI'], '?' . $_SERVER['QUERY_STRING']);
$_SESSION['FileAkses'] = $filepath;

$FileAkses = $_SESSION['FileAkses'];

$y = "select top 1 * from [dbo].[Profil]";
$n = sqlsrv_query($conn, $y);
$m = sqlsrv_fetch_array( $n, SQLSRV_FETCH_NUMERIC);
if($m != null){
    $_SESSION['Logo'] = $m[6];
    if($m[1] != ''){
        $_SESSION['NamaKoperasi'] = $m[1];
    }
}

if($UserID == "" || $NameUser == "" || $JabatanUser == ""){
    echo "<script language='javascript'>document.location='login.php';</script>";
}
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Baronang</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- ICON -->
    <link href="static/images/Logo-fish-icon.png" rel="shortcut icon">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="static/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="static/plugins/datepicker/datepicker3.css">
    <!-- daterange picker -->
    <link rel="stylesheet" href="static/plugins/daterangepicker/daterangepicker.css">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="static/plugins/iCheck/all.css">
    <!-- Bootstrap time Picker -->
    <link rel="stylesheet" href="static/plugins/timepicker/bootstrap-timepicker.min.css">
    <!-- Bootstrap Color Picker -->
    <link rel="stylesheet" href="static/plugins/colorpicker/bootstrap-colorpicker.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="static/plugins/select2/select2.min.css">
    <!-- Wizard -->
    <link rel="stylesheet" href="static/plugins/wizard/wizard-css.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="static/plugins/datatables/dataTables.bootstrap.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="static/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="static/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="static/dist/css/skins/_all-skins.min.css">
    <link rel="stylesheet" href="custom_css.css">
    <!-- jQuery 2.1.4 -->
    <script src="static/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <style>
        .form_center {
            width: 65%;
            text-align: center;
        }
        .main-footer2 {
            bottom: 0;
            color:#444;
            width: 100%;
            font-size: 16px;
            padding: 15px;
            position: fixed;
            background: #fff;
            margin-top: 30px;
            border-top:1px solid #d2d6de;
        }
        .body-idcs2 {
            background: #ecf0f5;
        }
        #copyright {
            text-align: center;
        }
        .spacel {
            margin-right: 5px;
            margin-left: 5px;
        }
    </style>

</head>
<body class="hold-transition skin-blue sidebar-mini body-idcs2">
<?php
if(isset($_SESSION['error-time'])){
    if($_SESSION['error-time'] <= time()){
        unset($_SESSION['error-time']);
        unset($_SESSION['error-type']);
        unset($_SESSION['error-message']);
    }
}
?>
<div class="wrapper">
    <header class="main-header">
        <!-- Logo -->
        <?php
        $Logo = $_SESSION['Logo'];
        if($Logo == ''){
            $Logo = 'static/images/yourlogohere.png';
        }
        ?>
        <a href="#" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><img src="<?php echo $Logo; ?>" width="45" height="45"></span>
        </a>

        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="static/images/No-Image-Icon.png" class="user-image" alt="User Image">
                            <span class="hidden-xs"><?php echo $NameUser; ?></span>
                        </a>
                        <ul class="dropdown-menu" style="border-radius: 0;border: 1px solid #375ce4;padding-top: 0;">
                            <!-- User image -->
                            <li class="user-header" style="background: #375ce4;">
                                <img src="static/images/No-Image-Icon.png" class="img-circle" alt="User Image">
                                <p>
                                    <?php echo $NameUser; ?>
                                </p>
                            </li>
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="change_pass.php" class="btn btn-warning btn-flat" style="background-color: #f39c12;border-color: #e08e0b; color: #fff"><?php echo lang('Ubah password'); ?></a>
                                </div>
                                <div class="pull-right">
                                    <a href="logout.php" class="btn btn-flat btn-danger" style="background-color: #d73925;color: #fff"><?php echo lang('Keluar'); ?></a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
