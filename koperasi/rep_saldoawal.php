<?php require('header.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><?php echo lang('Saldo Awal'); ?></h3>
    </div>
    <div class="box-body">

        <?php if($_GET['acc']){ ?>
            <div class="table-responsive">
                <table class="table table-bordered">
                    <?php
                    $a = "exec dbo.TampilDetailReportCOA '0','$_GET[acc]'";
                    $b = sqlsrv_query($conn, $a);
                    while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                    ?>
                        <tr>
                            <td><?php echo $c[1]; ?></td>
                            <td colspan="7"></td>
                        </tr>
                        <?php
                        $aa = "exec dbo.TampilDetailReportCOA '1','$c[0]'";
                        $bb = sqlsrv_query($conn, $aa);
                        while($cc = sqlsrv_fetch_array($bb, SQLSRV_FETCH_NUMERIC)){
                        ?>
                        <tr>
                            <td></td>
                            <td><?php echo $cc[0]; ?></td>
                            <td><?php echo $cc[1]; ?></td>
                            <td><?php echo $cc[2]; ?></td>
                            <td colspan="4"></td>
                        </tr>
                            <?php
                            $aaa = "exec dbo.TampilDetailReportCOA '2','$cc[0]'";
                            $bbb = sqlsrv_query($conn, $aaa);
                            while($ccc = sqlsrv_fetch_array($bbb, SQLSRV_FETCH_NUMERIC)){
                            ?>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td><?php echo $ccc[0]; ?></td>
                                    <td><?php echo $ccc[1]; ?></td>
                                    <td><?php echo $ccc[2]->format('Y-m-d H:i:s'); ?></td>
                                    <td>
                                        <?php
                                        $aaaa = "select* from dbo.TransactionType where KodeTransactionType='".$ccc[3]."'";
                                        $bbbb = sqlsrv_query($conn, $aaaa);
                                        $cccc = sqlsrv_fetch_array($bbbb, SQLSRV_FETCH_NUMERIC);
                                        if($cccc != null){
                                            echo $cccc[1];
                                        }
                                        else{
                                            echo $ccc[3];
                                        }
                                        ?>
                                    </td>
                                    <td><?php echo $ccc[6]; ?></td>
                                    <td><?php echo $ccc[7]; ?></td>
                                </tr>
                            <?php } ?>
                        <?php } ?>
                    <?php } ?>
                </table>
            </div>

        <?php } ?>

        <?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
            <div class="alert alert-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-warning ?>"></i> <?php echo ucfirst($_SESSION['error-type']); ?></h4>
                <?php echo $_SESSION['error-message']; ?>
            </div>
        <?php } ?>

        <form action="procsaldoawal.php" method="post">
        <div class="table-responsive">
            <table class="table table-bordered">
                <tr>
                    <th colspan="2"><?php echo lang('Aktiva'); ?></th>
                    <th colspan="2"><?php echo lang('Pasiva'); ?></th>
                </tr>
                <tr>
                    <td colspan="2">
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th><?php echo lang('Kode Akun'); ?></th>
                                <th><?php echo lang('Nama Akun'); ?></th>
                                <th><?php echo lang('Nilai'); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $atotal = 0;
							$pp = 1;
                            $x = "select* from dbo.SaldoAwalViewBaru where KodeGroup in('1.0.00.00.000') order by SUBSTRING(KodeGroup,1,1),SUBSTRING(KodeAccount,3,1),SUBSTRING(KodeAccount,6,1),SUBSTRING(KodeAccount,9,1),SUBSTRING(KodeAccount,8,2),SUBSTRING(KodeAccount,11,2) asc";
                            $y = sqlsrv_query($conn, $x);
                            while($z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC)){
								$tipex = $z[8];
                                if($z[1] == 0){
                                    $tab = '20px;';
                                }
                                else if($z[1] == 1){
                                    $tab = '40px;';
                                }
                                else{
                                    $tab = '60px;';
                                }

                                if($z[7] == ''){
                                    $acc = "<span style='padding-left: ".$tab.";'><b>".$z[0]."</b></span>";
                                    $accname = "<span style='padding-left: ".$tab.";'><b>".$z[2]."</b></span>";
                                    $total = "<span class='pull-right'><b>".number_format($z[7])."</b></span>";
                                }
                                else{
                                    $acc = "<span style='padding-left: ".$tab.";'>".$z[0]."</span>";
                                    $accname = "<span style='padding-left: ".$tab.";'>".$z[2]."</span>";
                                    $total = "<span class='pull-right'>".number_format($z[7])."</span>";
                                    $atotal+=$z[7];
                                }
                                ?>
                                <tr>
                                    <td><a href="?acc=<?php echo $z[0]; ?>&st=2"><?php echo $acc; ?></a></td>
                                    <td><?php echo $accname; ?></td>
                                    <td><?php
									$namex = str_replace(' ', '', $accname);

									if ($tipex >= 1) {
									echo $total;
									} else {
										if ($z[7] != '') {
										?>
										<input type ='number' class="input<?php echo $z[1].$pp; ?>" name ="Amount[<?php echo $z[1].$pp; ?>]" value = "<?php echo $z[7]; ?>" class='form-control'>
										<?php }}
									?></td>
									</tr>
								<script>
									$(".input<?php echo $z[1].$pp; ?>").change(function(){
										var amount = $(".input<?php echo $z[1].$pp; ?>").val();
										var total = $("#totalA").val();

										hasil = parseFloat(total) + parseFloat(amount);
										$("#resultA").html(hasil);
										$(".resultA").val(hasil);
									});
								</script>
                            <?php $pp++; } ?>
                            </tbody>
                        </table>
                    </td>
                    <td colspan="2">
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th><?php echo lang('Kode Akun'); ?></th>
                                <th><?php echo lang('Nama Akun'); ?></th>
                                <th><?php echo lang('Nilai'); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $btotal = 0;
							$pn = 1;
                            $x = "select* from dbo.SaldoAwalViewBaru where KodeGroup in('2.0.00.00.000','3.0.00.00.000') order by SUBSTRING(KodeGroup,1,1),SUBSTRING(KodeAccount,3,1),SUBSTRING(KodeAccount,6,1),SUBSTRING(KodeAccount,9,1),SUBSTRING(KodeAccount,8,2),SUBSTRING(KodeAccount,11,2) asc";
                            $y = sqlsrv_query($conn, $x);
                            while($z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC)){
								$tipex = $z[8];
                                if($z[1] == 0){
                                    $tab = '20px;';
                                }
                                else if($z[1] == 1){
                                    $tab = '40px;';
                                }
                                else{
                                    $tab = '60px;';
                                }

                                if($z[7] == ''){
                                    $acc = "<span style='padding-left: ".$tab.";'><b>".$z[0]."</b></span>";
                                    $accname = "<span style='padding-left: ".$tab.";'><b>".$z[2]."</b></span>";
                                    $total = "<span class='pull-right'><b>".number_format($z[7])."</b></span>";
                                }
                                else{
                                    $acc = "<span style='padding-left: ".$tab.";'>".$z[0]."</span>";
                                    $accname = "<span style='padding-left: ".$tab.";'>".$z[2]."</span>";
                                    $total = "<span class='pull-right'>".number_format($z[7])."</span>";
                                    $btotal+=$z[7];
                                }
                                ?>
                                <tr>
                                    <td><a href="?acc=<?php echo $z[0]; ?>&st=2"><?php echo $acc; ?></a></td>
                                    <td><?php echo $accname; ?></td>
                                    <td><?php
									$namex = str_replace(' ', '', $accname);

									if ($tipex >= 1) {
									echo $total;
									} else {
										if ($z[7] != '') {
										?>
										<input type ='number' class="input<?php echo $z[1].$pn; ?>" name ="Amount[<?php echo $z[1].$pn; ?>]" value = "<?php echo $z[7]; ?>" class='form-control'>
										<?php }}
									?></td>
                                </tr>

								<script>
									$(".input<?php echo $z[1].$pn; ?>").change(function(){
										var amount = $(".input<?php echo $z[1].$pn; ?>").val();
										var total = $("#totalB").val();

										hasil = parseFloat(total) + parseFloat(amount);
										$("#resultB").html(hasil);
										$(".resultB").val(hasil);
									});
								</script>
                            <?php $pn++; } ?>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <th><?php echo lang('Total Aktiva'); ?></th>
                    <th>
						<input type="hidden" id="totalA" value="<?php echo $atotal; ?>">
						<input type="hidden" name="resultA" class="resultA" value="<?php echo $atotal; ?>">
						<span id="resultA" class="pull-right"><?php echo $atotal; ?></span>
					</th>
                    <th><?php echo lang('Total Pasiva'); ?></th>
                    <th>
						<input type="hidden" id="totalB" value="<?php echo $btotal; ?>">
						<input type="hidden" name="resultB" class="resultB" value="<?php echo $btotal; ?>">
						<span id="resultB" class="pull-right"><?php echo $btotal; ?></span>
					</th>
                </tr>
            </table>
            <div class="box-footer">
                <button type="submit" class="btn btn-sm btn-primary">Save</button>
            </div>
          </form>
        </div>
    </div>
</div>

<?php require('content-footer.php');?>

<?php require('footer.php');?>
