<?php
session_start();
error_reporting(0);

include "connect.php";

$page			= $_GET['page'];
$edit			= $_GET['edit'];
$delete			= $_GET['delete'];

$reg		= $_POST['reg'];
$intt		= $_POST['intt'];
$intr		= 0;
$min		= $_POST['min'];
$max		= $_POST['max'];
$adm		= $_POST['adm'];
$kba		= $_POST['kba'];
$clos		= $_POST['clos'];

$a = "select [dbo].[getKodeRegularSavingType]('$_SESSION[KID]')";
$b = sqlsrv_query($conn, $a);
$c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC);
$reg_sav = $c[0];

if(isset($_POST['wiz']) and $kba != ""){
    $a = "exec [dbo].[ProsesSkemaBunga] '$reg_sav','1','0','10000000000','0'";
    $b = sqlsrv_query($conn, $a);

    $spbltsql = "exec [dbo].[ProsesRegularSavingType] '$reg_sav','Tabungan','0','0','0','10000000000','0','$kba','0','0'";
    $spblstmt = sqlsrv_query($conn, $spbltsql);
    if($spblstmt){
        messageAlert(lang('Berhasil menyimpan ke database'),'success');
        header('Location: reg_sav_type.php');
    }
    else{
        messageAlert(lang('Gagal menyimpan ke database'),'danger');
        header('Location: reg_sav_type.php');
    }
}
else if(isset($_GET['del']) and isset($_GET['u'])){

	$dpultsql = "delete from [dbo].[SkemaBunga] where RegularSavingType='$_GET[del]' and Urutan >= '$_GET[u]'";
	$dpulstmt = sqlsrv_query($conn, $dpultsql);

    if($dpulstmt){
        messageAlert(lang('Berhasil menghapus data dari database'),'success');
        header('Location: reg_sav_type.php?edit='.$_GET['del']);
    }
	else{
        messageAlert(lang('Gagal menghapus data dari database'),'danger');
        header('Location: reg_sav_type.php?edit='.$_GET['del']);
	}
}
else{
	if($reg == "" || $intt == "" || $min == "" || $max == "" || $adm == "" || $kba == "" || $clos == ""){
        messageAlert(lang('Harap isi seluruh kolom'),'info');
        header('Location: reg_sav_type.php');
	}
	else{
		$uasql = "select * from [dbo].[RegularSavingType] where RegularSavingType='$reg_sav'";
		$uastmt = sqlsrv_query($conn, $uasql);
		$uarow = sqlsrv_fetch_array( $uastmt, SQLSRV_FETCH_NUMERIC);

		if(!empty($edit)){
			if(count($ulrow[0] > 0)){
				$upultsql = "update [dbo].[RegularSavingType] set RegularSavingTypeDesc ='$reg', KodeInterestType='$intt', InterestRate='$intr', MinBalance='$min', MaxWithDraw='$max', AdminFee='$adm', KBA='$kba', ClosingFee='$clos' where RegularSavingType='$edit'";
				$upulstmt = sqlsrv_query($conn, $upultsql);

                foreach ($_POST["start"] as $key => $error) {
                    $urutan = $_POST['urutan'][$key];
                    $start = $_POST['start'][$key];
                    $end = $_POST['end'][$key];
                    $percentage = $_POST['percentage'][$key];

                    $a = "exec [dbo].[ProsesSkemaBunga] '$edit','$urutan','$start','$end','$percentage'";
                    $b = sqlsrv_query($conn, $a);
                }

				if($upulstmt){
                    messageAlert(lang('Berhasil memperbaharui data ke database'),'success');
                    header('Location: reg_sav_type.php');
				}
				else{
                    messageAlert(lang('Gagal memperbaharui data ke database'),'danger');
                    header('Location: reg_sav_type.php');
				}
			}
			else{
                header('Location: reg_sav_type.php');
			}
		}
		else{
            $blsql = "select * from [dbo].[RegularSavingType]  where RegularSavingType='$reg_sav'";
            $blstmt = sqlsrv_query($conn, $blsql);
            $blrow = sqlsrv_fetch_array( $blstmt, SQLSRV_FETCH_NUMERIC);

            if(empty($blrow[0])){
                $spbltsql = "exec [dbo].[ProsesRegularSavingType] '$reg_sav','$reg','$intt','$intr','$min','$max','$adm','$kba','0','$clos'";
                $spblstmt = sqlsrv_query($conn, $spbltsql);

                foreach ($_POST["start"] as $key => $error) {
                    $urutan = $_POST['urutan'][$key];
                    $start = $_POST['start'][$key];
                    $end = $_POST['end'][$key];
                    $percentage = $_POST['percentage'][$key];

                    $a = "exec [dbo].[ProsesSkemaBunga] '$reg_sav','$urutan','$start','$end','$percentage'";
                    $b = sqlsrv_query($conn, $a);
                }

                if($spblstmt){
                    messageAlert(lang('Berhasil menyimpan ke database'),'success');
                    header('Location: reg_sav_type.php');
                }
                else{
                    messageAlert(lang('Gagal menyimpan ke database'),'danger');
                    header('Location: reg_sav_type.php');
                }
            }
            else{
                messageAlert(lang('Kode Bank sudah ada'),'warning');
                header('Location: reg_sav_type.php');
            }
		}
	}
}
?>