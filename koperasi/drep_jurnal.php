<?php
session_start();
error_reporting(0);

include('lib/phpExcel/Classes/PHPExcel.php');
include "connect.php";



    $filePath = "uploads/excel/";

    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getProperties()->setCreator("baronang");
    $objPHPExcel->getProperties()->setTitle("Jurnal");




// set autowidth
    for($col = 'A'; $col !== 'Z'; $col++) {
        $objPHPExcel->getActiveSheet()
            ->getColumnDimension($col)
            ->setAutoSize(true);
    }

    $listCombo = array();

    //sheet 2
    $objWorkSheet = $objPHPExcel->createSheet(0);
// baris judul
    $objWorkSheet->SetCellValue('A1', 'No');
    $objWorkSheet->SetCellValue('B1', 'Kode Jurnal');
    $objWorkSheet->SetCellValue('C1', 'Kode Akun');
    $objWorkSheet->SetCellValue('D1', 'Nama Akun');
    $objWorkSheet->SetCellValue('E1', 'Debit');
    $objWorkSheet->SetCellValue('F1', 'Credit');
    $objWorkSheet->SetCellValue('G1', 'Deskripsi');




    $no = 1;
    $row = 2;


    $tglawal    = $_GET['from'];
    $tglakhir   = $_GET['to'];

    $x = "select * from dbo.JurnalDetail a inner join dbo.JurnalHeader b on a.KodeJurnal = b.KodeJurnal where a.KodeJurnal is not null and b.Tanggal between '$tglawal' and '$tglakhir' ORDER BY b.Tanggal Asc";
    //var_dump($x).die;
    $y = sqlsrv_query($conn, $x);
    while($z = sqlsrv_fetch_array( $y, SQLSRV_FETCH_NUMERIC)){
    
         //push untuk drop down list
        array_push($listCombo, $z[1]);

        $noacc = '';
        $acc = '';
        $sql   = "select * from dbo.Account where KodeAccount='$z[1]'";
        $stmt  = sqlsrv_query($conn, $sql);
        $row1   = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_NUMERIC);
      
        if($row1 != null){
            $noacc = $row1[0];
            $acc = $row1[1];
        }

        $objWorkSheet->SetCellValue("A".$row, $no);
        $objWorkSheet->SetCellValueExplicit("B".$row, $z[0], PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet->SetCellValue("C".$row, $noacc);
        $objWorkSheet->SetCellValue("D".$row, $acc);
        $objWorkSheet->SetCellValueExplicit("E".$row, number_format($z[2]), PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet->SetCellValueExplicit("F".$row, number_format($z[3]), PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet->SetCellValueExplicit("G".$row, $z[4]);


        
        $no++;
        $row++;
    }

    $objWorkSheet->setTitle('Jurnal');

    $fileName = 'Jurnal'.'_'.strtotime(date('Y-m-d H:i:s')).'.xls';
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');

// download ke client
    header('Content-type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename="'.$fileName.'"');
    $objWriter->save('php://output');

    return $filePath.'/'.$fileName;

?>
