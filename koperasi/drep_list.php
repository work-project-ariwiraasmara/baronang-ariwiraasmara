<?php
session_start();
error_reporting(0);

include('lib/phpExcel/Classes/PHPExcel.php');
include "connect.php";


    $filePath = "uploads/excel/";

    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getProperties()->setCreator("baronang");
    $objPHPExcel->getProperties()->setTitle("Data Member");

// set autowidth
    for($col = 'A'; $col !== 'Z'; $col++) {
        $objPHPExcel->getActiveSheet()
            ->getColumnDimension($col)
            ->setAutoSize(true);
    }

    $listCombo = array();

    //sheet 2
    $objWorkSheet = $objPHPExcel->createSheet(0);
// baris judul
    $objWorkSheet->SetCellValue('A1', 'No');
    $objWorkSheet->SetCellValue('B1', 'Member ID');
    $objWorkSheet->SetCellValue('C1', 'Nama');
    $objWorkSheet->SetCellValue('D1', 'Alamat');
    $objWorkSheet->SetCellValue('E1', 'No. Telp');
    $objWorkSheet->SetCellValue('F1', 'Email');
    $objWorkSheet->SetCellValue('G1', 'KTP');
    $objWorkSheet->SetCellValue('H1', 'NIP');

    $no = 1;
    $row = 2;
    $x = "select * from [dbo].[MemberListView] ORDER BY MemberID Asc";
    //var_dump($x).die;
    $y = sqlsrv_query($conn, $x);
    while($z = sqlsrv_fetch_array( $y, SQLSRV_FETCH_NUMERIC)){
        //push untuk drop down list
        array_push($listCombo, $z[1]);

        $objWorkSheet->SetCellValue("A".$row, $no);
        $objWorkSheet->SetCellValueExplicit("B".$row, $z[1], PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet->SetCellValue("C".$row, $z[2]);
        $objWorkSheet->SetCellValue("D".$row, $z[3]);
        $objWorkSheet->SetCellValueExplicit("E".$row, $z[4], PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet->SetCellValue("F".$row, $z[5]);
        $objWorkSheet->SetCellValueExplicit("G".$row, $z[6], PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet->SetCellValueExplicit("H".$row, $z[7], PHPExcel_Cell_DataType::TYPE_STRING);

        $no++;
        $row++;
    }

    $objWorkSheet->setTitle('Data Member');

    $fileName = 'dataMember'.'_'.strtotime(date('Y-m-d H:i:s')).'.xls';
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');

// download ke client
    header('Content-type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename="'.$fileName.'"');
    $objWriter->save('php://output');

    return $filePath.'/'.$fileName;

?>
