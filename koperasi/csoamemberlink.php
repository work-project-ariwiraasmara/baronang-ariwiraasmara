<?php require('header.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<?php include 'connectuser.php';?>

<?php
//edit
$uledit  = $_GET['edit'];
$ulnew   = $_SESSION['UID'];

$ul0        = "";
$ul1        = "";
$ul2        = "";
$ul3        = "";
$ul4        = "";
$ul5        = "";
$ul6        = "";
$ul7        = "";
$ul8        = "";
$ul9        = "";
$ul10        = "";
$ul11 = "";
$ul12 = "";
$ulbelanja  = "";
$ulpinjam   = "";
if(!empty($uledit)){
    $eulsql       = "select * from dbo.MemberList where MemberID='$uledit' and StatusMember=1";
    $eulstmt      = sqlsrv_query($conn, $eulsql);
    $eulrow       = sqlsrv_fetch_array( $eulstmt, SQLSRV_FETCH_NUMERIC);
    if(count($eulrow[0]) > 0){
        $ul0        = $eulrow[0];
        $ul1        = $eulrow[1];
        $ul2        = $eulrow[2];
        $ul3        = $eulrow[3];
        $ul4        = $eulrow[4];
        $ul5        = $eulrow[5];
        $ul6        = $eulrow[6];
        $ul7        = $eulrow[7];
        $ul8        = $eulrow[8];
        $ul9        = $eulrow[9];
        $ul11        = $eulrow[10];

        //limit belanja
        $bjeulsql    = "select * from dbo.MemberLoanCredit where KodeMember='$ul1' and Status='1'";
        $bjeulstmt   = sqlsrv_query($conn, $bjeulsql);
        $bjeulrow    = sqlsrv_fetch_array( $bjeulstmt, SQLSRV_FETCH_NUMERIC);

        //limit pinjam
        $pjeulsql    = "select * from dbo.MemberLoanCredit where KodeMember='$ul1' and Status='2'";
        $pjeulstmt   = sqlsrv_query($conn, $pjeulsql);
        $pjeulrow    = sqlsrv_fetch_array( $pjeulstmt, SQLSRV_FETCH_NUMERIC);

        $ulbelanja  = number_format(substr($bjeulrow[2],0,-5),2,'.',',');
        $ulpinjam   = number_format(substr($pjeulrow[2],0,-5),2,'.',',');
    }
    else{
        $_SESSION['error-message'] = 'Member ID Not Found!';
        $_SESSION['error-type'] = 'warning';
        $_SESSION['error-time'] = time()+5;
    }
}

//cek
if(!empty($ulnew)){
    $eulsql       = "select * from dbo.UserPaymentGateway where KodeUser='$ulnew'";
    $eulstmt      = sqlsrv_query($connuser, $eulsql);
    $eulrow       = sqlsrv_fetch_array( $eulstmt, SQLSRV_FETCH_NUMERIC);
    if(count($eulrow[0]) > 0){
        $ul12 = $eulrow[0];
    }
    else{
        $_SESSION['error-message'] = 'User ID Baronang Pay Not Found!';
        $_SESSION['error-type'] = 'warning';
        $_SESSION['error-time'] = time()+5;
        echo "<script>window.location.href='identity_cs.php'</script>";
    }
}
else{
    $_SESSION['error-message'] = 'Invalid Request Data!';
    $_SESSION['error-type'] = 'warning';
    $_SESSION['error-time'] = time()+5;
    echo "<script>window.location.href='identity_cs.php'</script>";
}
?>

<?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
    <div class="alert alert-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-warning ?>"></i> <?php echo ucfirst($_SESSION['error-type']); ?></h4>
        <?php echo $_SESSION['error-message']; ?>
    </div>
<?php } ?>

<div class="box box-solid">
    <div class="box-header with-border">
        <h3 class="box-title">Member List</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <form action="proc_csoamemberlink.php" method="POST" class="form-horizontal">
        <div class="box-body">
            <div class="row">
                    <input type="hidden" class="form-control" id="mid" value="<?=$ul1?>" readonly>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="ktp" class="col-sm-3 control-label" style="text-align: left;">User ID Baronang Pay</label>
                            <div class="col-sm-9">
                                <input type="number" name="uid" class="form-control" id="uid" placeholder="" value="<?=$ul12?>" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-3 control-label" style="text-align: left;">No. KTP</label>
                            <div class="col-sm-9">
                                <input type="text" name="ktp" class="form-control" id="ktp" placeholder="" value="<?=$ul7?>" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-3 control-label" style="text-align: left;">Name</label>
                            <div class="col-sm-9">
                                <input type="text" name="name" class="form-control" id="name" placeholder="" value="<?=$ul2?>" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="nip" class="col-sm-3 control-label" style="text-align: left;">NIP</label>
                            <div class="col-sm-9">
                                <input type="text" name="nip" class="form-control" id="nip" placeholder="" value="<?=$ul8?>" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="nip" class="col-sm-3 control-label" style="text-align: left;">Member ID</label>
                            <div class="col-sm-9">
                                <input type="text" name="mid" class="form-control" id="mid" placeholder="" value="<?=$ul11?>" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="address" class="col-sm-3 control-label" style="text-align: left;">Address</label>
                            <div class="col-sm-9">
                                <textarea name="address" class="form-control" id="address" rows="3" placeholder="" readonly><?=$ul3?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="telp" class="col-sm-3 control-label" style="text-align: left;">Telp</label>
                            <div class="col-sm-9">
                                <input type="text" name="telp" class="form-control" id="telp" placeholder="" value="<?=$ul4?>" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-sm-3 control-label" style="text-align: left;">Email</label>
                            <div class="col-sm-9">
                                <input type="email" name="email" class="form-control" id="email" placeholder="" value="<?=$ul5?>" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" style="text-align: left;">Position</label>
                            <div class="col-sm-9">
                                <select name="position" class="form-control" disabled>
                                    <option>--- Select One ---</option>
                                    <?php
                                    $julsql   = "select * from [dbo].[JabatanView] order by KodeJabatan";
                                    $julstmt = sqlsrv_query($conn, $julsql);
                                    $jsArrayPositionLimit = "var prdName = new Array();\n";
                                    while($julrow  = sqlsrv_fetch_array( $julstmt, SQLSRV_FETCH_NUMERIC)){
                                        ?>
                                        <option value="<?=$julrow[0];?>" <?php if($julrow[0] == $ul9){echo "selected";}?>><?=$julrow[1];?></option>
                                        <?php
                                        $jsArrayPositionLimit .= "prdName['" . $julrow[0] . "'] = {belanja:'" . addslashes($julrow[4]) . "',pinjam:'".addslashes($julrow[6])."'};\n";
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="belanja" class="col-sm-4 control-label" style="text-align: left;">Limit Belanja</label>
                            <div class="col-sm-8">
                                <input type="text" name="belanja" class="form-control price" id="belanja" placeholder="" value="<?=$ulbelanja?>" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="pinjam" class="col-sm-4 control-label" style="text-align: left;">Limit Pinjam</label>
                            <div class="col-sm-8">
                                <input type="text" name="pinjam" class="form-control price" id="pinjam" placeholder="" value="<?=$ulpinjam?>" disabled>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
        <div class="box-footer">
            <div class="row">
                <div class="col-sm-4">
                    <?php if(!empty($uledit)){ ?>
                        <button type="button" class="btn btn-primary btn-cek btn-flat btn-block" data-toggle="modal" data-target="#modal-default">Connect</button>
                    <?php } ?>
                </div>
                <div class="col-sm-4">
                    <?php if(!empty($uledit)){ ?>
                        <a href="csoamemberlink.php?uid=<?php echo $ulnew; ?>"><button type="button" class="btn btn-default btn-flat btn-block">Reset</button></a>
                    <?php } ?>
                </div>
                <div class="col-sm-4">
                    <a href="identity_cs.php"><button type="button" class="btn btn-success btn-flat btn-block">Back</button></a>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="modal-default" style="display: none;">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">Compare Data</h4>
                    </div>
                    <div class="modal-body">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    </form>
</div>

<div class="box box-solid">
    <div class="box-header">
        <h3 class="box-title">Member List Data </h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="col-sm-12">
            <div class="alert alert-info alert-dismissible">
                <h4><i class="icon fa fa-warning ?>"></i> Info</h4>
                Pilih data member dibawah ini yang akan di connect
            </div>
            <table class="table">
                <thead>
                <tr>
                    <th>Member ID</th>
                    <th>Name</th>
                    <th>KTP</th>
                    <th>NIP</th>
                    <th>Phone</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td><input type="text" class="form-control input-sm member"></td>
                    <td><input type="text" class="form-control input-sm name"></td>
                    <td><input type="text" class="form-control input-sm ktp"></td>
                    <td><input type="text" class="form-control input-sm nip"></td>
                    <td><input type="text" class="form-control input-sm phone"></td>
                    <td><button type="button" class="btn btn-sm btn-success btn-search"><i class="fa fa-search"></i> Search</button></td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="col-sm-12">
            <table id="table1" class="table table-striped">
                <thead>
                <tr>
                    <th>No</th>
                    <th>Member ID</th>
                    <th>Name</th>
                    <th>KTP</th>
                    <th>NIP</th>
                    <th>Phone</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody id="lmember">
                <?php
                //count
                $jmlulsql   = "select count(*) from dbo.MemberList where StatusMember = 1";
                $jmlulstmt  = sqlsrv_query($conn, $jmlulsql);
                $jmlulrow   = sqlsrv_fetch_array( $jmlulstmt, SQLSRV_FETCH_NUMERIC);

                //pagging
                $perpages   = 10;
                $halaman    = $_GET['page'];
                if(empty($halaman)){
                    $posisi  = 0;
                    $batas   = $perpages;
                    $halaman = 1;
                }
                else{
                    $posisi  = (($perpages * $halaman) - 10) + 1;
                    $batas   = ($perpages * $halaman);
                }

                $ulsql = "SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY MemberID asc) as row FROM [dbo].[MemberList]) a WHERE StatusMember=1 and  KID='$_SESSION[KID]' and row between '$posisi' and '$batas'";
                $ulstmt = sqlsrv_query($conn, $ulsql);
                while($ulrow = sqlsrv_fetch_array( $ulstmt, SQLSRV_FETCH_NUMERIC)){
                    //limit belanja
                    $lbjulsql   = "select * from dbo.MemberLoanCredit where KodeMember='$ulrow[1]' and Status='1'";
                    $lbjulstmt  = sqlsrv_query($conn, $lbjulsql);
                    $lbjulrow   = sqlsrv_fetch_array( $lbjulstmt, SQLSRV_FETCH_NUMERIC);

                    //limit pinjaman
                    $lpjulsql   = "select * from dbo.MemberLoanCredit where KodeMember='$ulrow[1]' and Status='2'";
                    $lpjulstmt  = sqlsrv_query($conn, $lpjulsql);
                    $lpjulrow   = sqlsrv_fetch_array( $lpjulstmt, SQLSRV_FETCH_NUMERIC);
                    ?>
                    <tr>
                        <td><?=$ulrow[11];?></td>
                        <td><?=$ulrow[1];?></td>
                        <td><?=$ulrow[2];?></td>
                        <td><?=$ulrow[7];?></td>
                        <td><?=$ulrow[8];?></td>
                        <td><?=$ulrow[4];?></td>
                        <td>
                            <div class="btn-group">
                                <a href="csoamemberlink.php?edit=<?=$ulrow[1]?>&uid=<?php echo $ulnew; ?>" class="btn btn-default btn-flat btn-sm text-green" title="Pilih"><i class="fa fa-check"></i></a>
                            </div>
                        </td>
                    </tr>
                <?php
                }

                $jmlhalaman = ceil($jmlulrow[0]/$perpages);
                ?>
                </tbody>
            </table>
        </div>
        <div class="col-sm-12">
            <label>Showing <?=$halaman?> to <?=$jmlhalaman?> of <?=$jmlulrow[0]?> entries</label>
            <?=$posisi." - ".$batas?>
            <ul class="pagination pagination-sm no-margin pull-right">
                <li class="paginate_button"><a href="csoamemberlink.php">&laquo;</a></li>
                <?php
                for($ul = 1; $ul <= $jmlhalaman; $ul++){
                    if($ul != $halaman){ $ulpageactive = ""; $ulpagedisabled = "";}else{$ulpageactive = "active"; $ulpagedisabled = "disabled";}
                    echo '<li class="paginate_button '.$ulpageactive.'"><a href="csoamemberlink.php?page='.$ul.'" '.$ulpagedisabled.'>'.$ul.'</a></li>';
                }
                ?>
                <li class="paginate_button"><a href="csoamemberlink.php?uid=<?php echo $ulnew; ?>&page=<?=$jmlhalaman?>">&raquo;</a></li>
            </ul>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('.btn-search').click(function(){
        var member = $('.member').val();
        var name = $('.name').val();
        var ktp = $('.ktp').val();
        var nip = $('.nip').val();
        var phone = $('.phone').val();

        if(member == '' && name == '' && ktp == '' && nip == '' && phone == ''){
            window.location.href='csoamemberlink.php';
        }
        else{
            $.ajax({
                url : "ajax_member.php",
                type : 'POST',
                data: { member: member, name: name, ktp: ktp, nip: nip, phone: phone},
                success : function(data) {
                    $("#lmember").html(data);
                },
                error : function(){
                    alert('Silahkan coba lagi.');
                }
            });
        }
    });

    $('.btn-cek').click(function(){
        var uid = $('#uid').val();
        var mid = $('#mid').val();

        if(uid == ''){
            alert('Harap isi user id baronang pay');
            return false;
        }
        else{
            $.ajax({
                url : "ajax_cekmember.php",
                type : 'POST',
                data: { uid: uid, mid: mid},
                success : function(data) {
                    $(".modal-body").html(data);
                },
                error : function(){
                    alert('Silahkan coba lagi.');
                }
            });
        }
    });
</script>

<?php require('content-footer.php');?>

<?php require('footer.php');?>
