<?php require('header.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<?php
if($_SESSION['RequestExpired'] < date('Y-m-d H:i:s')){
    unset($_SESSION['RequestID']);
    unset($_SESSION['RequestLink']);
    unset($_SESSION['RequestExpired']);
    unset($_SESSION['RequestMember']);
    unset($_SESSION['RequestMemberName']);

    $_SESSION['error-type'] = 'info';
    $_SESSION['error-message'] = 'Your Session Has Expired. Retry input User ID Baronang App.';
    $_SESSION['error-time'] = time() + 5;
    echo "<script language='javascript'>document.location='identity_cs.php';</script>";
}
?>

<?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
    <div class="alert alert-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-warning ?>"></i> <?php echo ucfirst($_SESSION['error-type']); ?></h4>
        <?php echo $_SESSION['error-message']; ?>
    </div>
<?php } ?>

<div class="row">
    <div class="col-sm-8">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Regular Saving Close</h3>
            </div>
            <form action="proccscaregsaving.php" method="POST" class="form-horizontal">
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="csoaregsavmember" class="col-sm-5 control-label" style="text-align: left;">Member ID</label>
                                    <div class="col-sm-7">
                                        <input type="number" name="member" class="form-control" id="csoaregsavmember" placeholder="" value="<?php echo $_SESSION['RequestMember']; ?>" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="nama" class="col-sm-5 control-label" style="text-align: left;">Name</label>
                                    <div class="col-sm-7">
                                        <input type="text" name="nama" class="form-control" id="nama" placeholder="" value="<?php echo $_SESSION['RequestMemberName']; ?>" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-sm-8">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" style="text-align: left;">Account Number</label>
                                    <div class="col-sm-8">
                                        <input type="number" name="acc" class="form-control" id="belanja" placeholder="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-sm-8">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <input type="radio" name="type" class="cash" value="cash" checked> Cash
                                        <input type="radio" name="type" class="transfer" value="transfer"> Transfer to account
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 tf hide">
                            <div class="col-sm-8">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" style="text-align: left;">Account Number Transfer</label>
                                    <div class="col-sm-8">
                                        <input type="number" name="regacc" class="form-control" id="regacc" placeholder="" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="row">
                        <div class="col-sm-4">
                            <button type="submit" class="btn btn-flat btn-block btn-danger pull-right">Close</button>
                        </div>
                        <div class="col-sm-4">
                        </div>
                        <div class="col-sm-4">
                            <a href="identity_cs.php"><button type="button" class="btn btn-success btn-flat btn-block">Back</button></a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('.transfer').click(function(){
        $('.tf').removeClass('hide');
        $('#regacc').prop('disabled', false);
    });

    $('.cash').click(function(){
        $('.tf').addClass('hide');
        $('#regacc').prop('disabled', true);
    });
</script>

<?php require('content-footer.php');?>

<?php require('footer.php');?>