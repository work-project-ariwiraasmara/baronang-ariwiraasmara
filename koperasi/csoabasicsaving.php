      <?php require('header.php');?>      

      <?php require('sidebar-left.php');?>

      <?php require('content-header.php');?>

      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Basic Saving Account</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form action="#" method="POST" class="form-horizontal">
          <div class="box-body">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="col-sm-3 control-label" style="text-align: left;">Member ID</label>
                  <div class="col-sm-9">
                    <select name="id" class="form-control">
                      <option>--- Select One ---</option>
                      <?php
                        $mcsoabssql         = "select * from [dbo].[MemberList] order by MemberID";  
                        $mcsoabsstmt        = sqlsrv_query($conn, $mcsoabssql);
                        while($mcsoabsrow   = sqlsrv_fetch_array( $mcsoabsstmt, SQLSRV_FETCH_NUMERIC)){
                      ?>
                      <option value="<?=$mcsoabsrow[1];?>"><?=$mcsoabsrow[2];?></option>
                      <?php
                        }
                      ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label" style="text-align: left;">Loan Type</label>
                  <div class="col-sm-9">
                    <select name="loantype" class="form-control">
                      <option>--- Select One ---</option>
                      <?php
                        $kltcsoabssql         = "select * from [dbo].[LoanType] order by KodeLoanType";  
                        $kltcsoabsstmt        = sqlsrv_query($conn, $kltcsoabssql);
                        while($kltcsoabsrow   = sqlsrv_fetch_array( $kltcsoabsstmt, SQLSRV_FETCH_NUMERIC)){
                      ?>
                      <option value="<?=$kltcsoabsrow[0];?>"><?=$kltcsoabsrow[1];?></option>
                      <?php
                        }
                      ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="amount" class="col-sm-3 control-label" style="text-align: left;">Amount</label>
                  <div class="col-sm-9">
                    <input type="text" name="amount" class="form-control" id="amount" placeholder="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="cashier" class="col-sm-3 control-label" style="text-align: left;">Cashier</label>
                  <div class="col-sm-9">
                    <input type="text" name="cashier" class="form-control" id="cashier" placeholder="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="date" class="col-sm-3 control-label" style="text-align: left;">Date</label>
                  <div class="col-sm-9">
                    <input type="text" name="date" class="form-control" id="date" placeholder="">
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="name" class="col-sm-5 control-label" style="text-align: left;">Name</label>
                  <div class="col-sm-7">
                    <input type="text" name="name" class="form-control" id="name" placeholder="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="loanapp" class="col-sm-5 control-label" style="text-align: left;">Loan Application Number</label>
                  <div class="col-sm-7">
                    <input type="text" name="loanapp" class="form-control" id="loanapp" placeholder="">
                  </div>
                </div>
              </div>
            </div>
          </div>  
          <div class="box-footer">
            <button type="reset" class="btn btn-flat btn-default">Erase</button>
            <button type="submit" class="btn btn-flat btn-primary pull-right">Save</button>
          </div>
        </form>
      </div>

      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title">Drag a column header here to group by that column</h3>
        </div>
        <div class="box-body">
          <div class="col-xs-12 table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                    <th>Data 1</th>
                    <th>Data 2</th>
                    <th>Data 3</th>
                    <th>Data 4</th>
                    <th>Data 5</th>
                    <th>Data 6</th>
                  </tr>
              </thead>
              <tbody>
              <tr>
                    <td>Trident</td>
                    <td>Internet
                      Explorer 4.0
                    </td>
                    <td>Win 95+</td>
                    <td> 4</td>
                    <td>X</td>
                    <td>X</td>
                  </tr>
                  <tr>
                    <td>Trident</td>
                    <td>Internet
                      Explorer 5.0
                    </td>
                    <td>Win 95+</td>
                    <td>5</td>
                    <td>C</td>
                    <td>X</td>
                  </tr>
                  <tr>
                    <td>Trident</td>
                    <td>Internet
                      Explorer 5.5
                    </td>
                    <td>Win 95+</td>
                    <td>5.5</td>
                    <td>A</td>
                    <td>X</td>
                  </tr>
              </tbody>
            </table>
          </div>
          <!-- /.col -->
        </div>
        <div class="box-footer clearfix">
          <label>Showing 1 to 10 of <?=$tblrow?> entries</label>
          <ul class="pagination pagination-sm no-margin pull-right">
            <li class="paginate_button"><a href="#">&laquo;</a></li>
            <li class="paginate_button active"><a href="#">1</a></li>
            <li class="paginate_button"><a href="#">2</a></li>
            <li class="paginate_button"><a href="#">3</a></li>
            <li class="paginate_button"><a href="#">&raquo;</a></li>
          </ul>
        </div>
      </div>

      <?php require('content-footer.php');?>

      <?php require('footer.php');?>