<?php require('header.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<?php include 'connectuser.php';?>

<?php
if(date('Y-m-d H:i:s') >= $_SESSION['RequestExpired']){
    $_SESSION['error-message'] = 'Your session expired!';
    $_SESSION['error-type'] = 'warning';
    $_SESSION['error-time'] = time()+5;
    echo "<script>window.location.href='identity_cs.php'</script>";
}

$ulnew   = $_SESSION['UID'];

$ul0        = "";
$ul1        = "";
$ul2        = "";
$ul3        = "";
$ul4        = "";
$ul5        = "";
$ul6        = "";
$ul7        = "";
$ul8        = "";
$ul9        = "";
$ul10        = "";
$ul11 = "";
$ul12 = "";

//cek
if(!empty($ulnew)){
    $eulsql       = "select * from dbo.UserPaymentGateway where KodeUser='$ulnew'";
    $eulstmt      = sqlsrv_query($connuser, $eulsql);
    $eulrow       = sqlsrv_fetch_array( $eulstmt, SQLSRV_FETCH_NUMERIC);
    if(count($eulrow[0]) > 0){
        $ul12        = $eulrow[0];
        $ul7        = $eulrow[9];
        $ul2        = $eulrow[1];
        $ul3        = $eulrow[4];
        $ul4        = $eulrow[3];
        $ul5        = $eulrow[2];

        $disable = 'readonly';

        //cek sudah member atau belum
        $qwe       = "select * from dbo.MemberList where Name='$eulrow[1]' or email='$eulrow[2]' or phone='$eulrow[3]' or KTP='$eulrow[9]'";
        $asd      = sqlsrv_query($conn, $qwe);
        $zxc       = sqlsrv_fetch_array( $asd, SQLSRV_FETCH_NUMERIC);
        $dis = '';
        if($zxc != null){
            //cek sudah konek atau belum
            $zzz       = "select * from [dbo].[UserMemberKoperasi] where KodeMember='$zxc[1]'";
            $xxx      = sqlsrv_query($connuser, $zzz);
            $ccc       = sqlsrv_fetch_array( $xxx, SQLSRV_FETCH_NUMERIC);

            //limit belanja
            $lbjulsql   = "select * from dbo.MemberLoanCredit where KodeMember='$zxc[1]' and Status='1'";
            $lbjulstmt  = sqlsrv_query($conn, $lbjulsql);
            $lbjulrow   = sqlsrv_fetch_array( $lbjulstmt, SQLSRV_FETCH_NUMERIC);

            //limit pinjaman
            $lpjulsql   = "select * from dbo.MemberLoanCredit where KodeMember='$zxc[1]' and Status='2'";
            $lpjulstmt  = sqlsrv_query($conn, $lpjulsql);
            $lpjulrow   = sqlsrv_fetch_array( $lpjulstmt, SQLSRV_FETCH_NUMERIC);

            if($lbjulrow != '' and $lpjulrow != ''){
                $ulbelanja  = substr($lbjulrow[2],0,-5);
                $ulpinjam   = substr($lpjulrow[2],0,-5);
            }

            $ul9        = $zxc[9];
            $ul10        = $zxc[6]; //status
            $u11        = $zxc[1]; //kode member

            $_SESSION['error-message'] = 'Pengguna sudah terdaftar sebagai member';
            $_SESSION['error-type'] = 'info';
            $_SESSION['error-time'] = time()+5;
        }
    }
    else{
        $_SESSION['error-message'] = 'User ID Baronang Pay Not Found';
        $_SESSION['error-type'] = 'warning';
        $_SESSION['error-time'] = time()+5;
        echo "<script>window.location.href='identity_cs.php'</script>";
    }
}
else{
    $_SESSION['error-message'] = 'Invalid Request Data!';
    $_SESSION['error-type'] = 'warning';
    $_SESSION['error-time'] = time()+5;
    echo "<script>window.location.href='identity_cs.php'</script>";
}
?>

<?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
    <div class="alert alert-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-warning ?>"></i> <?php echo ucfirst($_SESSION['error-type']); ?></h4>
        <?php echo $_SESSION['error-message']; ?>
    </div>
<?php } ?>

<div class="box box-solid">
    <div class="box-header with-border">
        <h3 class="box-title">Member List</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <form action="proc_csoamemberlist.php" method="POST" class="form-horizontal">
        <div class="box-body">
            <div class="row">
                <input type="hidden" class="form-control" id="mid2" value="<?=$ul1?>" readonly>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="ktp" class="col-sm-3 control-label" style="text-align: left;">User ID Baronang Pay</label>
                        <div class="col-sm-9">
                            <input type="number" name="uid" class="form-control" id="uid" placeholder="" value="<?=$ul12?>" <?php echo $disable; ?>>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name" class="col-sm-3 control-label" style="text-align: left;">No. KTP</label>
                        <div class="col-sm-9">
                            <input type="text" name="ktp" class="form-control" id="ktp" placeholder="" value="<?=$ul7?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name" class="col-sm-3 control-label" style="text-align: left;">Name</label>
                        <div class="col-sm-9">
                            <input type="text" name="name" class="form-control" id="name" placeholder="" value="<?=$ul2?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nip" class="col-sm-3 control-label" style="text-align: left;">NIP</label>
                        <div class="col-sm-9">
                            <input type="text" name="nip" class="form-control" id="nip" placeholder="" value="<?=$ul8?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nip" class="col-sm-3 control-label" style="text-align: left;">Member ID</label>
                        <div class="col-sm-9">
                            <input type="text" name="mid" class="form-control" id="mid" placeholder="" value="<?=$ul11?>">
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="address" class="col-sm-3 control-label" style="text-align: left;">Address</label>
                        <div class="col-sm-9">
                            <textarea name="address" class="form-control" id="address" rows="3" placeholder=""><?=$ul3?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="telp" class="col-sm-3 control-label" style="text-align: left;">Telp</label>
                        <div class="col-sm-9">
                            <input type="text" name="telp" class="form-control" id="telp" placeholder="" value="<?=$ul4?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-sm-3 control-label" style="text-align: left;">Email</label>
                        <div class="col-sm-9">
                            <input type="email" name="email" class="form-control" id="email" placeholder="" value="<?=$ul5?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" style="text-align: left;">Position</label>
                        <div class="col-sm-9">
                            <select name="position" class="form-control" onChange="changeValuePositionLimit(this.value)">
                                <option>--- Select One ---</option>
                                <?php
                                $julsql   = "select * from [dbo].[JabatanView] order by KodeJabatan";
                                $julstmt = sqlsrv_query($conn, $julsql);
                                $jsArrayPositionLimit = "var prdName = new Array();\n";
                                while($julrow  = sqlsrv_fetch_array( $julstmt, SQLSRV_FETCH_NUMERIC)){
                                    ?>
                                    <option value="<?=$julrow[0];?>" <?php if($julrow[0] == $ul9){echo "selected";}?>><?=$julrow[1];?></option>
                                    <?php
                                    $jsArrayPositionLimit .= "prdName['" . $julrow[0] . "'] = {belanja:'" . addslashes($julrow[4]) . "',pinjam:'".addslashes($julrow[6])."'};\n";
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="belanja" class="col-sm-4 control-label" style="text-align: left;">Limit Belanja</label>
                        <div class="col-sm-8">
                            <input type="text" name="belanja" class="form-control price" id="belanja" placeholder="" value="<?php echo $ulbelanja; ?>" disabled>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="pinjam" class="col-sm-4 control-label" style="text-align: left;">Limit Pinjam</label>
                        <div class="col-sm-8">
                            <input type="text" name="pinjam" class="form-control price" id="pinjam" placeholder="" value="<?php echo $ulpinjam; ?>" disabled>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="belanja" class="col-sm-4 control-label" style="text-align: left;">KTP</label>
                        <div class="col-sm-8">
                            <input type="file" name="filename" class="form-control" placeholder="KTP">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <div class="row">
                <div class="col-sm-4">
                    <?php if($zxc == null){ ?>
                        <button type="submit" class="btn btn-flat btn-block btn-success pull-right">Save</button>
                    <?php } ?>
                </div>
                <div class="col-sm-4">

                </div>
                <div class="col-sm-4">
                    <a href="identity_cs.php"><button type="button" class="btn btn-success btn-flat btn-block">Back</button></a>
                </div>
            </div>
        </div>
    </form>
</div>

<div class="box box-solid">
    <div class="box-header">
        <h3 class="box-title">Member List Data </h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="col-sm-12">
            <table class="table">
                <thead>
                <tr>
                    <th>Member ID</th>
                    <th>Name</th>
                    <th>KTP</th>
                    <th>NIP</th>
                    <th>Phone</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td><input type="text" class="form-control input-sm member"></td>
                    <td><input type="text" class="form-control input-sm name"></td>
                    <td><input type="text" class="form-control input-sm ktp"></td>
                    <td><input type="text" class="form-control input-sm nip"></td>
                    <td><input type="text" class="form-control input-sm phone"></td>
                    <td><button type="button" class="btn btn-sm btn-success btn-search"><i class="fa fa-search"></i> Search</button></td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="col-sm-12">
            <table id="table1" class="table table-striped">
                <thead>
                <tr>
                    <th>No</th>
                    <th>Member ID</th>
                    <th>Name</th>
                    <th>KTP</th>
                    <th>NIP</th>
                    <th>Phone</th>
                </tr>
                </thead>
                <tbody id="lmember">
                <?php
                //count
                $jmlulsql   = "select count(*) from dbo.MemberList where StatusMember = 1";
                $jmlulstmt  = sqlsrv_query($conn, $jmlulsql);
                $jmlulrow   = sqlsrv_fetch_array( $jmlulstmt, SQLSRV_FETCH_NUMERIC);

                //pagging
                $perpages   = 10;
                $halaman    = $_GET['page'];
                if(empty($halaman)){
                    $posisi  = 0;
                    $batas   = $perpages;
                    $halaman = 1;
                }
                else{
                    $posisi  = (($perpages * $halaman) - 10) + 1;
                    $batas   = ($perpages * $halaman);
                }

                $ulsql = "SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY MemberID asc) as row FROM [dbo].[MemberList]) a WHERE StatusMember=1 and  KID='$_SESSION[KID]' and row between '$posisi' and '$batas'";
                $ulstmt = sqlsrv_query($conn, $ulsql);
                while($ulrow = sqlsrv_fetch_array( $ulstmt, SQLSRV_FETCH_NUMERIC)){
                    //limit belanja
                    $lbjulsql   = "select * from dbo.MemberLoanCredit where KodeMember='$ulrow[1]' and Status='1'";
                    $lbjulstmt  = sqlsrv_query($conn, $lbjulsql);
                    $lbjulrow   = sqlsrv_fetch_array( $lbjulstmt, SQLSRV_FETCH_NUMERIC);

                    //limit pinjaman
                    $lpjulsql   = "select * from dbo.MemberLoanCredit where KodeMember='$ulrow[1]' and Status='2'";
                    $lpjulstmt  = sqlsrv_query($conn, $lpjulsql);
                    $lpjulrow   = sqlsrv_fetch_array( $lpjulstmt, SQLSRV_FETCH_NUMERIC);
                    ?>
                    <tr>
                        <td><?=$ulrow[11];?></td>
                        <td><?=$ulrow[1];?></td>
                        <td><?=$ulrow[2];?></td>
                        <td><?=$ulrow[7];?></td>
                        <td><?=$ulrow[8];?></td>
                        <td><?=$ulrow[4];?></td>
                    </tr>
                <?php
                }

                $jmlhalaman = ceil($jmlulrow[0]/$perpages);
                ?>
                </tbody>
            </table>
        </div>
        <div class="col-sm-12">
            <label>Showing <?=$halaman?> to <?=$jmlhalaman?> of <?=$jmlulrow[0]?> entries</label>
            <?=$posisi." - ".$batas?>
            <ul class="pagination pagination-sm no-margin pull-right">
                <li class="paginate_button"><a href="csoamemberlist.php">&laquo;</a></li>
                <?php
                for($ul = 1; $ul <= $jmlhalaman; $ul++){
                    if($ul != $halaman){ $ulpageactive = ""; $ulpagedisabled = "";}else{$ulpageactive = "active"; $ulpagedisabled = "disabled";}
                    echo '<li class="paginate_button '.$ulpageactive.'"><a href="csoamemberlist.php?page='.$ul.'" '.$ulpagedisabled.'>'.$ul.'</a></li>';
                }
                ?>
                <li class="paginate_button"><a href="csoamemberlist.php?uid=<?php echo $ulnew; ?>&page=<?=$jmlhalaman?>">&raquo;</a></li>
            </ul>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('.btn-search').click(function(){
        var member = $('.member').val();
        var name = $('.name').val();
        var ktp = $('.ktp').val();
        var nip = $('.nip').val();
        var phone = $('.phone').val();

        if(member == '' && name == '' && ktp == '' && nip == '' && phone == ''){
            window.location.href='csoamemberlist.php';
        }
        else{
            $.ajax({
                url : "ajax_membernew.php",
                type : 'POST',
                data: { member: member, name: name, ktp: ktp, nip: nip, phone: phone},
                success : function(data) {
                    $("#lmember").html(data);
                },
                error : function(){
                    alert('Silahkan coba lagi.');
                }
            });
        }
    });

    $('.btn-cek').click(function(){
        cek();
    });

    $('.btn-cek2').click(function(){
        var uid = $('#uid').val();
        var mid = $('#mid2').val();

        if(uid == ''){
            alert('Harap isi user id baronang pay');
            return false;
        }
        else{
            $('#uid').prop('disabled', true);

            $.ajax({
                url : "ajax_cekmember.php",
                type : 'POST',
                data: { uid: uid, mid: mid},
                success : function(data) {
                    $(".modal-body").html(data);
                },
                error : function(){
                    alert('Silahkan coba lagi.');
                }
            });
        }
    });
</script>
<?php require('content-footer.php');?>

<?php require('footer.php');?>
