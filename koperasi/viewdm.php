<?php require('header.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<?php
if(isset($_GET['m'])){
    $member = $_GET['m'];

    $a = "select * from [dbo].[MemberListView] where MemberID='$member' and KID='$_SESSION[KID]'";
    $b = sqlsrv_query($conn, $a);
    $c = sqlsrv_fetch_array( $b, SQLSRV_FETCH_NUMERIC);
    if($c == null){
        echo "<script language='javascript'>alert('Not found!');document.location='loan_chat.php';</script>";
    }
?>
<div class="row">
    <h1 class="col-md-12">Detail Member</h1>
    <div class="col-md-8">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Profil Member</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <div class="box-body">
                <table class="table table-bordered table-striped">
                    <tr>
                        <th>Member ID</th>
                        <td><?php echo $c[1]; ?></td>
                    </tr>
                    <tr>
                        <th>No KTP</th>
                        <td><?php echo $c[6]; ?></td>
                    </tr>
                    <tr>
                        <th>NIP</th>
                        <td><?php echo $c[7]; ?></td>
                    </tr>
                    <tr>
                        <th>Nama</th>
                        <td><?php echo $c[2]; ?></td>
                    </tr>
                    <tr>
                        <th>Alamat</th>
                        <td><?php echo $c[3]; ?></td>
                    </tr>
                    <tr>
                        <th>Telepon</th>
                        <td><?php echo $c[4]; ?></td>
                    </tr>
                    <tr>
                        <th>Email</th>
                        <td><?php echo $c[5]; ?></td>
                    </tr>
                    <tr>
                        <th>Jabatan</th>
                        <td><?php echo $c[8]; ?></td>
                    </tr>
                    <tr>
                        <th>Limit Belanja</th>
                        <td><?php echo number_format($c[9]); ?></td>
                    </tr>
                    <tr>
                        <th>Limit Pinjaman</th>
                        <td><?php echo number_format($c[10]); ?></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="box-footer">
            <a href="loan_chat.php"><button type="button" class="btn btn-flat btn-default">Kembali Ke Loan Chat</button></a>
        </div>
    </div>
</div>
<?php } else {
    echo "<script language='javascript'>alert('Invalid required data!');document.location='loan_chat.php';</script>";
} ?>
<?php require('content-footer.php');?>

<?php require('footer.php');?>