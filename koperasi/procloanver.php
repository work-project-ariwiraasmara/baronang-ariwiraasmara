<?php
session_start();
include "connect.php";

if(isset($_GET['app'])){
    $a = "select * from [dbo].[LoanApplicationListView] where LoanAppNum = '$_GET[app]' and StatusComplete = 1";
    $b = sqlsrv_query($conn, $a);
    $c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC);
    if($c != null){
        $x = "update dbo.LoanApplicationList set StatusComplete = '2' where LoanAppNum = '$_GET[app]'";
        $y = sqlsrv_query($conn, $x);
        if($y){
            messageAlert('Success verified loan','success');
            header('Location: loan_ver.php');
        }
        else{
            messageAlert('Failed verify loan','danger');
            header('Location: loan_ver.php');
        }
    }
    else{
        messageAlert('Failed get loan data','warning');
        header('Location: loan_ver.php');
    }
}
else if(isset($_GET['del']) and isset($_GET['doc'])){
    $a = "select * from [dbo].[LoanApplicationListView] where LoanAppNum = '$_GET[del]' and StatusComplete = 1";
    $b = sqlsrv_query($conn, $a);
    $c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC);
    if($c != null){
        $x = "delete from dbo.LoanDocUpload where LoanAppNum = '$_GET[del]' and NameDoc = '$_GET[doc]'";
        $y = sqlsrv_query($conn, $x);

        $exp = date($c[8], strtotime("+1 day"));
        $xx = "update dbo.LoanApplicationList set StatusComplete = '0', TanggalTempo='$exp' where LoanAppNum = '$_GET[doc]'";
        $yy = sqlsrv_query($conn, $xx);

        if($yy){
            messageAlert('Success delete document','success');
            header('Location: loan_ver.php?id='.$_GET['del']);
        }
        else{
            messageAlert('Failed delete document','danger');
            header('Location: loan_ver.php?id='.$_GET['del']);
        }
    }
    else{
        messageAlert('Failed get loan data','warning');
        header('Location: loan_ver.php');
    }
}
else{
    messageAlert('Failed required data','warning');
    header('Location: loan_ver.php');
}
?>