<?php require('header.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<?php
$qq = "select * from dbo.UserApprovalLoan where KodeKaryawan = '".$_SESSION['UserID']."' and KID = '".$_SESSION['KID']."'";
$ww = sqlsrv_query($conn, $qq);
$ee = sqlsrv_fetch_array( $ww, SQLSRV_FETCH_NUMERIC);
if($ee != null){
?>
<div class="row">
    <div class="col-md-12">
        <h3><?php echo lang('Persetujuan Pinjaman'); ?></h3>

        <?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
            <div class="alert alert-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-warning ?>"></i> <?php echo ucfirst($_SESSION['error-type']); ?></h4>
                <?php echo $_SESSION['error-message']; ?>
            </div>
        <?php } ?>

        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title"><?php echo lang('Daftar Permohonan Pinjaman'); ?></h3>
            </div>
            <div class="box-body">
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th><?php echo lang('No App Pinjaman'); ?></th>
                        <th>Member ID</th>
                        <th><?php echo lang('Nama'); ?></th>
                        <th><?php echo lang('Produk'); ?></th>
                        <th><?php echo lang('Tanggal Permintaan'); ?></th>
                        <th><?php echo lang('Jumlah'); ?></th>
                        <th><?php echo lang('Jumlah yang disetujui'); ?></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $no = 1;
                    $aa   = "select * from [dbo].[LoanApplicationListView] where StatusComplete in(2,3) order by TimeStamp desc";
                    $bb  = sqlsrv_query($conn, $aa);
                    while($cc = sqlsrv_fetch_array( $bb, SQLSRV_FETCH_NUMERIC)){
                        $amountapp = 0;
                        $x = "select MIN(Amount) from [dbo].[HistoriAppLoan] where KodeLoanAppNum = '".$cc[2]."'";
                        $y = sqlsrv_query($conn, $x);
                        $z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC);
                        if($z != null){
                            $amountapp = $z[0];
                        }
                        ?>
                        <tr>
                            <td><?= $no; ?></td>
                            <td><?= $cc[2] ?></td>
                            <td><?= $cc[0] ?></td>
                            <td><?= $cc[1] ?></td>
                            <td><?= $cc[6] ?></td>
                            <td><?= date_format($cc[3],"Y/m/d H:i:s"); ?></td>
                            <td>
                                <?= number_format($cc[7]) ?>
                                <input type="hidden" id="amountreq<?php echo $cc[2]; ?>" value="<?php echo $cc[7]; ?>" readonly>
                            </td>
                            <td>
                                <?php if($cc[9] == 3){ ?>
                                    <b><?php echo number_format($amountapp); ?></b> <?php echo lang('Menunggu persetujuan Peminta'); ?>
                                <?php } else { ?>
                                    <input type="text" id="amount<?php echo $cc[2]; ?>" class="form-control price" value="<?php echo $amountapp; ?>">
                                <?php } ?>
                            </td>
                            <td>
                                <?php if($cc[9] == 2){ ?>
                                    <button type="button" class="btn btn-success btn-sm btn-approve<?php echo $cc[2] ?>" title="Approve"><i class="fa fa-check"></i> <?php echo lang('Menyetujui'); ?></button>
                                <?php } ?>
                                <button type="button" class="btn btn-info btn-sm btn-user<?php echo $cc[0] ?>" title="Show detail user"><i class="fa fa-user"></i> </button>
                                <button type="button" class="btn btn-info btn-sm btn-chat<?php echo $cc[2] ?>" data-toggle="modal" data-target="#myModalChat" title="Chat"><i class="fa fa-book"></i> Chat</button>
                            </td>
                        </tr>

                        <script type="text/javascript">
                            $('.btn-user<?php echo $cc[0] ?>').click(function(){
                                $('#modal').click();
                                $.ajax({
                                    url : "ajax_getmember.php",
                                    type : 'POST',
                                    data: { member: '<?php echo $cc[0] ?>'},
                                    success : function(data) {
                                        $(".modal-body").html(data);
                                    },
                                    error : function(){
                                        alert('Try again.');
                                    }
                                });
                            });

                            $('.btn-chat<?php echo $cc[2] ?>').click(function(){
                                $('#message').val('');

                                $.ajax({
                                    url : "ajax_chatloan.php",
                                    type : 'POST',
                                    data: { loanappnum: '<?php echo $cc[2] ?>'},
                                    success : function(data) {
                                        $(".box-body-chat").html(data);
                                    },
                                    error : function(){
                                        alert('Try again.');
                                    }
                                });
                            });

                            $('.btn-approve<?php echo $cc[2] ?>').click(function(){
                                var amount = $('#amount<?php echo $cc[2] ?>').val();
                                var amountreq = $('#amountreq<?php echo $cc[2] ?>').val();

                                if(amount <= 0){
                                    alert('Amount approve tidak boleh 0');
                                    return false;
                                }
                                else if(amount > parseFloat(amountreq)){
                                    alert('Amount approve tidak boleh melebihi request');
                                    return false;
                                }
                                else{
                                    $.ajax({
                                        url : "procchatapp.php",
                                        type : 'POST',
                                        data: { loanappnum: '<?php echo $cc[2] ?>', amount: amount},
                                        success : function(data) {
                                            window.location.href = 'loan_app.php';
                                        },
                                        error : function(){
                                            alert('Try again.');
                                        }
                                    });
                                }
                            });
                        </script>
                        <?php $no++; } ?>
                    </tbody>
                </table>
            </div>
        </div>

        <button type="button" class="btn btn-primary hide" id="modal" data-toggle="modal" data-target="#myModal"></button>

        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Detail</h4>
                    </div>
                    <div class="modal-body">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang('Tutup'); ?></button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="myModalChat" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="box box-warning direct-chat direct-chat-warning">
                        <div class="box-header with-border">
                            <h3 class="box-title"><?php echo lang('Chat Langsung'); ?></h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i> </button>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body-chat">

                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <div class="input-group">
                                <input type="text" id="message" placeholder="Type Message ..." class="form-control">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-warning btn-flat btn-send"><?php echo lang('Kirim'); ?></button>
                                    <button type="button" class="btn btn-default btn-flat" data-dismiss="modal"><?php echo lang('Tutup'); ?></button>
                                </span>
                            </div>
                        </div>
                        <!-- /.box-footer-->
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript">
            $('.btn-send').click(function(){
                var kode = $('#kode').val();
                var msg = $('#message').val();

                $.ajax({
                    url : "ajax_chatloan.php",
                    type : 'POST',
                    data: { loanappnum: kode, message: msg},
                    success : function(data) {
                        $(".box-body-chat").html(data);
                    },
                    error : function(){
                        alert('Try again.');
                    }
                });
            });
        </script>
    </div>
</div>

<?php } else { ?>
    <div class="alert alert-warning alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-warning"></i> Alert!</h4>
        You are not user approval loan application
    </div>
<?php } ?>

<?php require('content-footer.php');?>

<?php require('footer.php');?>