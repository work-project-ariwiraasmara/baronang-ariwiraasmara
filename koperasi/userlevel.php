      <?php echo require('header.php');?>      

      <?php echo require('sidebar-left.php');?>

      <?php echo require('content-header.php');?>
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-6 connectedSortable">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">User Level</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" action="#" method="POST">
              <div class="box-body">
                <div class="col-md-12">
                  <div class="form-group">
                    <label for="levelid" class="col-sm-4 control-label" style="text-align: left;">Level ID</label>
                    <div class="col-sm-8">
                      <input type="text" name="id" class="form-control" id="cooperativeID" placeholder="">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="BasicSaving" class="col-sm-4 control-label" style="text-align: left;">Level Name</label>
                    <div class="col-sm-8">
                      <input type="text" name="level" class="form-control" id="BasicSaving" placeholder="">
                    </div>
                  </div>
                  <!-- Custom Tabs -->
                  <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                      <li class="active"><a href="#master" data-toggle="tab">Master</a></li>
                      <li><a href="#customer" data-toggle="tab">Customer Service</a></li>
                      <li><a href="#cashier" data-toggle="tab">Cashier</a></li>
                      <li><a href="#loan" data-toggle="tab">Loan</a></li>
                    </ul>
                    <div class="tab-content">
                      <div class="tab-pane active" id="master">
                        <div class="row">
                          <div class="col-md-4">
                            <label style="font-weight: normal;">
                              <input type="checkbox" name="mgs" class="minimal"> General Setting
                            </label>
                            <br/>
                            <label style="font-weight: normal;">
                              <input type="checkbox" name="mua" class="minimal"> User Account
                            </label>
                            <br/>
                            <label style="font-weight: normal;">
                              <input type="checkbox" name="mp" class="minimal"> Position
                            </label>
                            <br/>
                            <label style="font-weight: normal;">
                              <input type="checkbox" name="mba" class="minimal"> Bank Account
                            </label>
                            <br/>
                            <label style="font-weight: normal;">
                              <input type="checkbox" name="mbl" class="minimal"> Bank List
                            </label>
                          </div>
                          <div class="col-md-4">
                            <label style="font-weight: normal;">
                              <input type="checkbox" name="mbs" class="minimal"> Basic Saving Type
                            </label>
                            <br/>
                            <label style="font-weight: normal;">
                              <input type="checkbox" name="mrst" class="minimal"> Regular Saving Type
                            </label>
                            <br/>
                            <label style="font-weight: normal;">
                              <input type="checkbox" name="mtdt" class="minimal"> Time Deposit Type
                            </label>
                          </div>
                          <div class="col-md-4">
                            <label style="font-weight: normal;">
                              <input type="checkbox" name="mls" class="minimal"> Loan Setting
                            </label>
                            <br/>
                            <label style="font-weight: normal;">
                              <input type="checkbox" name="mlt" class="minimal"> Loan Type
                            </label>
                          </div>
                        </div>
                      </div><!-- /.tab-pane -->
                      <div class="tab-pane" id="customer">
                        <div class="row">
                          <div class="col-md-6">
                            <label>Open Account</label>
                            <br/>
                            <label style="font-weight: normal;">
                              <input type="checkbox" name="cm" class="minimal"> Memberlist
                            </label>
                            <br/>
                            <label style="font-weight: normal;">
                              <input type="checkbox" name="csoa" class="minimal"> Regular Saving Open Account
                            </label>
                            <br/>
                            <label style="font-weight: normal;">
                              <input type="checkbox" name="cla" class="minimal"> Loan Application
                            </label>
                            <br/>
                            <label style="font-weight: normal;">
                              <input type="checkbox" name="cbsa" class="minimal"> Basic Saving Account
                            </label>
                            <br/>
                            <label style="font-weight: normal;">
                              <input type="checkbox" name="ctdoa" class="minimal"> Time Deposit Open Account
                            </label>
                          </div>
                          <div class="col-md-6">
                            <label>Close Account</label>
                            <br/>
                            <label style="font-weight: normal;">
                              <input type="checkbox" name="cbs" class="minimal"> Basic Saving
                            </label>
                            <br/>
                            <label style="font-weight: normal;">
                              <input type="checkbox" name="cml" class="minimal"> Memberlist
                            </label>
                            <br/>
                            <label style="font-weight: normal;">
                              <input type="checkbox" name="crs" class="minimal"> Regular Saving
                            </label>
                            <br/>
                            <label style="font-weight: normal;">
                              <input type="checkbox" name="ctd" class="minimal"> Time Deposit
                            </label>
                            <br/>
                            <label style="font-weight: normal;">
                              <input type="checkbox" name="cl" class="minimal"> Loan
                            </label>
                          </div>
                        </div>
                      </div><!-- /.tab-pane -->
                      <div class="tab-pane" id="cashier">
                        <div class="row">
                          <div class="col-md-6">
                            <label>Deposit</label>
                            <br/>
                            <label style="font-weight: normal;">
                              <input type="checkbox" name="hb" class="minimal"> Basic Saving
                            </label>
                            <br/>
                            <label style="font-weight: normal;">
                              <input type="checkbox" name="hr" class="minimal"> Regular Saving
                            </label>
                            <br/>
                            <label style="font-weight: normal;">
                              <input type="checkbox" name="htd" class="minimal"> Time Deposit
                            </label>
                            <br/>
                            <label style="font-weight: normal;">
                              <input type="checkbox" name="hlp" class="minimal"> Loan Payment
                            </label>
                          </div>
                          <div class="col-md-6">
                            <label>Withdrwal</label>
                            <br/>
                            <label style="font-weight: normal;">
                              <input type="checkbox" name="hbs" class="minimal"> Basic Saving
                            </label>
                            <br/>
                            <label style="font-weight: normal;">
                              <input type="checkbox" name="hrs" class="minimal"> Regular Saving
                            </label>
                          </div>
                        </div>
                      </div><!-- /.tab-pane -->
                      <div class="tab-pane" id="loan">
                        <div class="row">
                          <div class="col-md-12">
                            <label>Deposit</label>
                            <br/>
                            <label style="font-weight: normal;">
                              <input type="checkbox" name="dla" class="minimal"> Loan Approval
                            </label>
                            <br/>
                            <label style="font-weight: normal;">
                              <input type="checkbox" name="dlr" class="minimal"> Loan Release
                            </label>
                          </div>
                        </div>
                      </div><!-- /.tab-pane -->
                    </div><!-- /.tab-content -->
                  </div><!-- nav-tabs-custom -->
                </div><!-- /.col -->
                </div>
                <div class="col-md-6">
                  
              </div>
              <div class="box-footer">
                <button type="reset" class="btn btn-flat btn-default">Erase</button>
                <button type="submit" class="btn btn-flat btn-primary pull-right">Save</button>
              </div>
              <!-- /.box-body -->
            </form>
          </div>
        </section>
        <!-- /.Left col -->
      </div>
      <!-- /.row (main row) -->
      

      <?php echo require('content-footer.php');?>

      <?php echo require('sidebar-right.php');?>

      <?php echo require('footer.php');?>