      <?php require('header.php');?>      

      <?php require('sidebar-left.php');?>

      <?php require('content-header.php');?>

      <?php
        //edit
      $euledit  = $_GET['edit'];
      $uledit   = $_GET['loan'];

      if(!empty($euledit)){
        $dataID = $euledit;
      }
      if(!empty($uledit)){
        $dataID = $uledit; 
      }

      if(!empty($euledit) || !empty($uledit)){
        $eulsql       = "select * from dbo.MemberList where KID='$KIDkop' and MemberID='$dataID'";  
        $eulstmt      = sqlsrv_query($conn, $eulsql);
        $eulrow       = sqlsrv_fetch_array( $eulstmt, SQLSRV_FETCH_NUMERIC);
        if(count($eulrow[0]) > 0){
          $ul0        = $eulrow[0];
          $ul1        = $eulrow[1];
          $ul2        = $eulrow[2];
          $ul3        = $eulrow[3];
          $ul4        = $eulrow[4];
          $ul5        = $eulrow[5];
          $ul6        = $eulrow[6];
          $ul7        = $eulrow[7];
          $ul8        = $eulrow[8];
          $ul9        = $eulrow[9];
          
          //limit belanja
          $bjeulsql    = "select * from dbo.MemberLoanCredit where KodeMember='$ul1' and Status='1'";  
          $bjeulstmt   = sqlsrv_query($conn, $bjeulsql);
          $bjeulrow    = sqlsrv_fetch_array( $bjeulstmt, SQLSRV_FETCH_NUMERIC);

          //limit pinjam
          $pjeulsql    = "select * from dbo.MemberLoanCredit where KodeMember='$ul1' and Status='2'";  
          $pjeulstmt   = sqlsrv_query($conn, $pjeulsql);
          $pjeulrow    = sqlsrv_fetch_array( $pjeulstmt, SQLSRV_FETCH_NUMERIC);

          if(!empty($euledit)){
            $ulbelanja  = number_format(substr($bjeulrow[2],0,-5),2,'.',',');
            $ulpinjam   = number_format(substr($pjeulrow[2],0,-5),2,'.',',');

            $ulprocedit = "?page=".$_GET['page']."&edit=".$ul1;
            $uldisabled = "disabled";
          }
          else if(!empty($uledit)){
            $ulbelanja  = substr($bjeulrow[2],0,-5);
            $ulpinjam   = substr($pjeulrow[2],0,-5);

            $ulprocedit = "?page=".$_GET['page']."&loan=".$ul1;
            $uldisabled = "";
          }
          else{
            $uldisabled = "";
          }
        }
        else{
          if(empty($_GET['page'])){
            echo "<script language='javascript'>document.location='csoamemberlist.php';</script>";
          }
          else{
            echo "<script language='javascript'>document.location='csoamemberlist.php?page=".$_GET['page']."';</script>";
          }
        }
      }
      else{
        $ul0        = "";
        $ul1        = "";
        $ul2        = "";
        $ul3        = "";
        $ul4        = "";
        $ul5        = "";
        $ul6        = "";
        $ul7        = "";
        $ul8        = "";
        $ul9        = "";
        $ulbelanja  = "";
        $ulpinjam   = "";
        $ulprocedit = "";
        $uldisabled = "disabled";
      }

      ?>

      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Member List</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <!-- /.box-header -->
        <div class="box-body">
          <div class="col-xs-12 table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Member ID</th>
                  <th>Name</th>
				  <th>Address</th>
				  <th>Phone</th>
				  <th>Email</th>
				  <th>KTP</th>
				  <th>NIP</th>
                  <th>Jabatan</th>
                </tr>
              </thead>
              <tbody>
                <?php
                  //count
                  $jmlulsql   = "select count(*) from dbo.MemberList where KID='$KIDkop'";  
                  $jmlulstmt  = sqlsrv_query($conn, $jmlulsql);
                  $jmlulrow   = sqlsrv_fetch_array( $jmlulstmt, SQLSRV_FETCH_NUMERIC);

                  //pagging
                  $perpages   = 10;
                  $halaman    = $_GET['page']; 
                  if(empty($halaman)){ 
                     $posisi  = 0;
                     $batas   = $perpages; 
                     $halaman = 1; 
                  } 
                  else{ 
                     $posisi  = (($perpages * $halaman) - 10) + 1;
                     $batas   = ($perpages * $halaman);
                  }

                  $ulsql = "SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY MemberID asc) as row FROM [dbo].[MemberList]) a WHERE KID='$KIDkop' and row between '$posisi' and '$batas'";

                  $ulstmt = sqlsrv_query($conn, $ulsql);

                  while($ulrow = sqlsrv_fetch_array( $ulstmt, SQLSRV_FETCH_NUMERIC)){
                    //limit belanja
                    $lbjulsql   = "select * from dbo.MemberLoanCredit where KodeMember='$ulrow[1]' and Status='1'";  
                    $lbjulstmt  = sqlsrv_query($conn, $lbjulsql);
                    $lbjulrow   = sqlsrv_fetch_array( $lbjulstmt, SQLSRV_FETCH_NUMERIC);
                    //limit pinjaman
                    $lpjulsql   = "select * from dbo.MemberLoanCredit where KodeMember='$ulrow[1]' and Status='2'";  
                    $lpjulstmt  = sqlsrv_query($conn, $lpjulsql);
                    $lpjulrow   = sqlsrv_fetch_array( $lpjulstmt, SQLSRV_FETCH_NUMERIC);
                ?> 
                <tr>
                  <td><?=$ulrow[10];?></td>
                  <td><?=$ulrow[1];?></td>
                  <td><?=$ulrow[2];?></td>
				  <td><?=$ulrow[3];?></td>
				  <td><?=$ulrow[4];?></td>
				  <td><?=$ulrow[5];?></td>
				  <td><?=$ulrow[7];?></td>
				  <td><?=$ulrow[8];?></td>
                  <td><?=$ulrow[9];?></td>
                </tr>
                <?php
                  }

                  $jmlhalaman = ceil($jmlulrow[0]/$perpages);
                ?>
              </tbody>
            </table>
          </div>
          <div class="box-footer clearfix">
            <label>Showing <?=$halaman?> to <?=$jmlhalaman?> of <?=$jmlulrow[0]?> entries</label>
            <?=$posisi." - ".$batas?>
            <ul class="pagination pagination-sm no-margin pull-right">
              <li class="paginate_button"><a href="csoamemberlist.php">&laquo;</a></li>
              <?php
                for($ul = 1; $ul <= $jmlhalaman; $ul++){
                  if($ul != $halaman){ $ulpageactive = ""; $ulpagedisabled = "";}else{$ulpageactive = "active"; $ulpagedisabled = "disabled";}
                  echo '<li class="paginate_button '.$ulpageactive.'"><a href="csoamemberlist.php?page='.$ul.'" '.$ulpagedisabled.'>'.$ul.'</a></li>';
                }
              ?>
              <li class="paginate_button"><a href="csoamemberlist.php?page=<?=$jmlhalaman?>">&raquo;</a></li>
            </ul>
          </div>
		  <div class="col-md-12">
				<br>
                <p><a href="export.php"><button type="submit" class="btn btn-info pull-left">Save</button></a></p>
				</div>
        </div>
      </div>

      <?php require('content-footer.php');?>

      <?php require('footer.php');?>
