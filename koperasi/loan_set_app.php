      <?php require('header.php');?>      

      <?php require('sidebar-left.php');?>

      <?php require('content-header.php');?>
	<script language="javascript">
		function load_page(value){
			window.location.href = "loan_set_app.php?loantype=" + value;
		};
	</script>
	  <?php
        //edit
	  $loantyp = $_GET['loantype'];
      $uledit = $_GET['edit'];
      if(!empty($uledit)){
        $eulsql   = "select * from dbo.UserApproavalLoanView where UserID='$uledit' and LoanType='$loantyp'";  
        $eulstmt  = sqlsrv_query($conn, $eulsql);
        $eulrow   = sqlsrv_fetch_array( $eulstmt, SQLSRV_FETCH_NUMERIC);
        if(count($eulrow[0]) > 0){
          $ul0    = $eulrow[0];
          $ul1    = $eulrow[1];
          $ul2    = $eulrow[2];
          $ulprocedit = "?page=".$_GET['page']."&edit=".$ul0;
          $uldisabled = "disabled";
        }
        else{
          if(empty($_GET['page'])){
            echo "<script language='javascript'>document.location='loan_set_app.php';</script>";
          }
          else{
            echo "<script language='javascript'>document.location='loan_set_app.php?page=".$_GET['page']."';</script>";
          }
        }
      }
      else{
        $ul0    = "";
        $ul1    = "";
        $ul2    = "";
        $ulprocedit = "";
        $uldisabled = "";
      }

      ?>
      <div class="row">
        <div class="col-md-6">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Loan Setting Approval</h3>
            </div>
            <form class="form-horizontal" action="procloan_set_app.php<?=$ulprocedit?>"  method="POST">
              <div class="box-body">
                  <div class="form-group">
							<label for="AccountName" class="col-sm-4 control-label" style="text-align: left;">Karyawan Approval</label>
							<div class="col-sm-8">
							<select name="kar" class="form-control" onChange="changeValuePositionLimit(this.value)">
								<option>--- Select One ---</option>
                            <?php
                              $julsql   = "select * from [dbo].[LoginList] order by UserID";  
                              $julstmt = sqlsrv_query($conn, $julsql);
                              $jsArrayPositionLimit = "var prdName = new Array();\n";
                              while($rjulrow  = sqlsrv_fetch_array( $julstmt, SQLSRV_FETCH_NUMERIC)){
                            ?>
                              <option value="<?=$rjulrow[0];?>">&nbsp;<?=$rjulrow[0];?>&nbsp;<?=$rjulrow[1];?></option>
                            <?php
                                $jsArrayPositionLimit .= "prdName['" . $rjulrow[0] . "'] = {belanja:'" . addslashes($rjulrow[1]) . "',pinjam:'".addslashes($rjulrow[12])."'};\n";  
                              }
                            ?>
							
                          </select>
                        </div>
                      </div>
			  <div class="form-group">
                    <label class="col-sm-4 control-label" style="text-align: left;">Produk Pinjaman</label>			  
                <div class="col-sm-8">
                        <select name="produk" class="form-control" id="produk" onchange="load_page(this.value);">
                            <option>--- Select One ---</option>
                            <?php
                            $ltvsql   = "select * from [dbo].[LoanTypeView]";
                            $ltvstmt  = sqlsrv_query($conn, $ltvsql);
                            while($ltvrow   = sqlsrv_fetch_array( $ltvstmt, SQLSRV_FETCH_NUMERIC)){
                                echo "<option value=";
                                echo $ltvrow[0];
                                if ($ltvrow[0]==$loantyp) {
                                    echo " selected='selected' ";
                                }
                                echo ">&nbsp;";
                                echo $ltvrow[0];
                                echo "<br>&nbsp;";
                                echo $ltvrow[1];
                                echo "</option>";
                            }
                            ?>
                        </select>
                </div>
				</div>					  
              </div>
                     <div class="box-footer">
						<div class="row">
							<div class="col-sm-4">
							</div>
								<div class="col-sm-4">
									<?php
										if(count($eulrow[0]) > 0){
										echo '<button type="submit" class="btn btn-flat btn-block btn-success pull-right">Update</button>'; 
										}
										else{
										echo '<button type="submit" class="btn btn-flat btn-block btn-primary pull-right">Save</button>';
										}
									?>
						</div>
						<div class="col-sm-4">
							</div>
						</div>
					</div>
				  </div>
              <!-- /.box-body -->
            </form>
          </div>      
	  <div class="col-md-6">
      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title">Data Karyawan Approval</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body pad table-responsive">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr>
                  <th>No</th>
                  <th>Kode Karyawan</th>
                  <th>Nama</th>
                  <th>Jabatan</th>
				  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php
                    //count
					$loantyp = $_GET['loantype'];
                    $jmlulsql   = "select count(*) from dbo.UserApproavalLoanView where LoanType='$loantyp'";  
                    $jmlulstmt  = sqlsrv_query($conn, $jmlulsql);
                    $jmlulrow   = sqlsrv_fetch_array( $jmlulstmt, SQLSRV_FETCH_NUMERIC);

                    //pagging
                    $perpages   = 10;
                    $halaman    = $_GET['page']; 
                    if(empty($halaman)){ 
                       $posisi  = 0;
                       $batas   = $perpages; 
                       $halaman = 1; 
                    } 
                    else{ 
                       $posisi  = (($perpages * $halaman) - 10) + 1;
                       $batas   = ($perpages * $halaman);
                    }

                    $ulsql = "SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY UserID asc) as row FROM [dbo].[UserApproavalLoanView] where LoanType='$loantyp') a WHERE row between '$posisi' and '$batas'";

                    //$ulsql = "select * from [dbo].[UserApproavalLoanView]";  

                    $ulstmt = sqlsrv_query($conn, $ulsql);

                    while($ulrow = sqlsrv_fetch_array( $ulstmt, SQLSRV_FETCH_NUMERIC)){
                  ?>
                <tr>
                  <td><?=$ulrow[3];?></td>
                  <td><?=$ulrow[0];?>
                  <td><?=$ulrow[1];?></td>
                  <td><?=$ulrow[2];?></td>
				   <td width="20%" style="padding: 3px">
                      <div class="btn-group">
                        <a href="procloan_set_app.php?page=<?=$halaman?>&delete=<?=$ulrow[0]?>&prod=<?=$loantyp ?>" class="btn btn-default btn-flat btn-sm text-red" title="delete" onClick="return confirm('Hapus data ini ?')"><i class="fa fa-trash-o"></i></a>
                      </div>
                    </td>
                  </tr>
                <?php
				}
				$jmlhalaman = ceil($jmlulrow[0]/$perpages);
				?>
              </tbody>
            </table>
          </div>
			<div class="box-footer clearfix">
              <label>Showing <?=$halaman?> to <?=$jmlhalaman?> of <?=$jmlulrow[0]?> entries</label>
              <?=$posisi." - ".$batas?>
              <ul class="pagination pagination-sm no-margin pull-right">
                <li class="paginate_button"><a href="loan_set_app.php">&laquo;</a></li>
                <?php
                  for($ul = 1; $ul <= $jmlhalaman; $ul++){
                    if($ul != $halaman){ $ulpageactive = ""; $ulpagedisabled = "";}else{$ulpageactive = "active"; $ulpagedisabled = "disabled";}
                    echo '<li class="paginate_button '.$ulpageactive.'"><a href="loan_set_app.php?page='.$ul.'" '.$ulpagedisabled.'>'.$ul.'</a></li>';
                  }
                ?>
                <li class="paginate_button"><a href="loan_set_app.php?page=<?=$jmlhalaman?>">&raquo;</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>

      <?php require('content-footer.php');?>

      <?php require('footer.php');?>