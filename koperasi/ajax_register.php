<html>
<head>
    <!-- Select2 -->
    <link rel="stylesheet" href="static/plugins/select2/select2.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="static/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="static/dist/css/skins/_all-skins.min.css">
</head>
<body>
    <select name="city" class="form-control select2" required="">
        <option value="">- Select -</option>
        <?php
        include 'connectinti.php';
        $a = "select * from [dbo].[Kota] where KodeProvinsi = '$_POST[province]'";
        $b = sqlsrv_query($conns, $a);
        while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){ ?>
            <option value="<?php echo $c[0]; ?>"><?php echo $c[1]; ?></option>
        <?php } ?>
    </select>

    <!-- Select2 -->
    <script src="static/plugins/select2/select2.full.min.js"></script>
    <!-- AdminLTE App -->
    <script src="static/dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="static/dist/js/demo.js"></script>

    <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2();
        });
    </script>
</body>
</html>