<button type="button" class="btn btn-primary hide" id="btn-app" data-toggle="modal" data-target=".bs-example-modal-sm">Small modal</button>

<!-- Small modal -->
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btn-exec-app">Save changes</button>
            </div>
        </div>
    </div>
</div>

</div>
<!-- ./wrapper -->
<?php
//hak akses
$w = array();
$aaa = "select * from [dbo].[UserLevelMenu] where KodeUserLevel = '$JabatanUser' and isApproval = 1";
$bbb = sqlsrv_query($conn, $aaa);
while($ccc = sqlsrv_fetch_array($bbb, SQLSRV_FETCH_NUMERIC)){
    array_push($w, $ccc[2]);
}
?>

<?php
if(in_array($FileAkses, $w)){
?>
    <!-- Untuk approval spv -->
<?php } ?>

<!-- Bootstrap 3.3.5 -->
<script src="static/bootstrap/js/bootstrap.min.js"></script>
<!-- Auto number format -->
<script src="static/plugins/jquery-number/jquery.number.js"></script>
<!-- Select2 -->
<script src="static/plugins/select2/select2.full.min.js"></script>
<!-- DataTables -->
<script src="static/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="static/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- date-range-picker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src="static/plugins/daterangepicker/daterangepicker.js"></script>
<!-- bootstrap date picker -->
<script src="static/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- bootstrap color picker -->
<script src="static/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="static/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="static/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="static/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="static/plugins/fastclick/fastclick.min.js"></script>
<!-- AdminLTE App -->
<script src="static/dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="static/dist/js/demo.js"></script>
<!-- QR Barcode Reader -->
<script type="text/javascript" src="static/plugins/webCodeCamJs/js/qrcodelib.js"></script>
<script type="text/javascript" src="static/plugins/webCodeCamJs/js/webcodecamjquery.js"></script>
<script type="text/javascript" src="static/plugins/webCodeCamJs/js/mainjquery.js"></script>

<!-- Page script -->
<?php include "js-footer.php"; ?>

<script type="text/javascript">
    <?php echo $jsArrayPositionLimit; ?>
    function changeValuePositionLimit(id){
        document.getElementById('belanja').value = prdName[id].belanja;
        document.getElementById('pinjam').value = prdName[id].pinjam;
    }

    $(document).ready(function(){
        $('#csoaregsavmember').change(function(){    // KETIKA ISI DARI FIEL 'member' BERUBAH MAKA ......
            var memberfromfield = $('#csoaregsavmember').val();  // AMBIL isi dari fiel member masukkan variabel 'memberfromfield'
            $.ajax({        // Memulai ajax
                method: "POST",
                url: "ajax_csoaregsav.php",    // file PHP yang akan merespon ajax
                data: { member: memberfromfield}   // data POST yang akan dikirim
            })
                .done(function( hasilajax ) {   // KETIKA PROSES Ajax Request Selesai
                    $('#nama').val(hasilajax);  // Isikan hasil dari ajak ke field 'nama'
                });
        });
    });
</script>
</body>
</html>
