<?php
session_start();
error_reporting(0);

include "connect.php";

$page = $_GET['page'];
$edit = $_GET['edit'];
$delete = $_GET['delete'];

$kode	= $_POST['kode'];
$acc	= $_POST['acc'];
$accn	= $_POST['accn'];
$bal	= $_POST['bal'];

if(!empty($delete)){
		
	$dpultsql = "delete from [dbo].[BankAccount] where KodeKoperasiBankAccount='$delete'";
	$dpulstmt = sqlsrv_query($conn, $dpultsql);
	if($dpulstmt){
        messageAlert('Berhasil menghapus data dari kedatabase','success');
        header('Location: bank_acc.php');
	}
	else{
        messageAlert('Gagal menghapus data dari kedatabase','danger');
        header('Location: bank_acc.php');
    }
}
else{
	if($kode == ""|| $acc == ""|| $accn == ""|| $bal == ""){
        messageAlert('Harap isi seluruh kolom','info');
        header('Location: bank_acc.php');
	}
	else{
		if(!empty($edit)){
            $ulsql 	= "select * from [dbo].[BankAccount] where KodeKoperasiBankAccount='$edit'";
            $ulstmt = sqlsrv_query($conn, $ulsql);
            $ulrow 	= sqlsrv_fetch_array( $ulstmt, SQLSRV_FETCH_NUMERIC);
            if(count($ulrow[0] > 0)){
                $upultsql = "exec [dbo].[prosesBankAccount] '$_SESSION[KID]', '$edit', '$kode','$acc','$accn','$bal'";
				$upulstmt = sqlsrv_query($conn, $upultsql);
				if($upulstmt){
                    messageAlert('Berhasil memperbaharui data kedatabase','success');
                    header('Location: bank_acc.php');
				}
				else{
                    messageAlert('Gagal memperbaharui data kedatabase','danger');
                    header('Location: bank_acc.php');
				}
			}
			else{
                messageAlert('Data tidak ditemukan','warning');
                header('Location: bank_acc.php');
			}
		}
		else{
            $a = "select [dbo].[getKodeBankAcc]('$_SESSION[KID]','$kode')";
            $b = sqlsrv_query($conn, $a);
            $c = sqlsrv_fetch_array( $b, SQLSRV_FETCH_NUMERIC);
            $code = $c[0];

            $pbltsql = "exec [dbo].[prosesBankAccount] '$_SESSION[KID]', '$code', '$kode','$acc','$accn','$bal'";
            $pblstmt = sqlsrv_query($conn, $pbltsql);
            if($pblstmt){
                messageAlert('Berhasil menyimpan kedatabase','success');
                header('Location: bank_acc.php');
            }
            else{
                messageAlert('Gagal menghapus data dari kedatabase','success');
                header('Location: bank_acc.php');
            }
		}
	}
}

?>
