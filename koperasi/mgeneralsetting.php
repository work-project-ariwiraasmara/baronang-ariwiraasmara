<?php require('header.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
    <div class="alert alert-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-warning ?>"></i> <?php echo ucfirst($_SESSION['error-type']); ?></h4>
        <?php echo $_SESSION['error-message']; ?>
    </div>
<?php } ?>

<form action="proc_mgeneralsetting.php" class="form-horizontal" method="POST">
<div class="box box-primary">
<div class="box-header with-border">
    <h3 class="box-title"><?php echo lang('Pengaturan umum'); ?></h3>
    <?php
    $mgssql    = "select * from [dbo].[GeneralSetting] where KID='$_SESSION[KID]'";
    $mgsstmt    = sqlsrv_query($conn, $mgssql);
    $mgsrow     = sqlsrv_fetch_array( $mgsstmt, SQLSRV_FETCH_NUMERIC);
    if(count($mgsrow[0]) > 0){
        $mgs0  = $mgsrow[0];
        $mgs1  = $mgsrow[1];
        $mgs2  = $mgsrow[2];
        $mgs3  = $mgsrow[3];
        $mgs4  = $mgsrow[4];
        $mgs5  = $mgsrow[5];
        $mgs6  = $mgsrow[6];
        $mgs7  = $mgsrow[7];
        $mgs8  = $mgsrow[8];
        $mgs9  = $mgsrow[9];
        $mgs10 = $mgsrow[10];
        $mgs11 = $mgsrow[11];
        $mgs12 = $mgsrow[12];
        $mgs13 = $mgsrow[13];
        $mgs14 = $mgsrow[14];
        $mgs15 = substr($mgsrow[15], 0 ,-5);
        $mgs16 = $mgsrow[16];
        $mgs17 = $mgsrow[17];
    }
    ?>
</div>
<!-- /.box-header -->
<div class="box-body">
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="PaymentPriority" class="col-sm-6 control-label" style="text-align: left;"><?php echo lang('Prioritas pembayaran'); ?></label>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <select name="template" class="form-control">
                        <option value="">- <?php echo lang('Pilih salah satu'); ?> -</option>
                        <?php
                        $mpbssksql = "select * from [dbo].[TemplateGeneralSeting]";  //simpanan sukarela
                        $mpbsstmt = sqlsrv_query($conn, $mpbssksql);
                        while ($mpbsrow = sqlsrv_fetch_array( $mpbsstmt, SQLSRV_FETCH_NUMERIC)) {
                            if($mpbsrow[1] == $mgs1 and $mpbsrow[2] == $mgs2 and $mpbsrow[3] == $mgs3){
                                ?>
                                <option value="<?=$mpbsrow[0];?>" selected><?=$mpbsrow[0];?></option>
                            <?php } else { ?>
                                <option value="<?=$mpbsrow[0];?>"><?=$mpbsrow[0];?></option>
                            <?php } ?>
                        <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="MonthlyCutOfDate" class="col-sm-6 control-label" style="text-align: left;"><?php echo lang('Tanggal cut off bulanan') ?></label>
            </div>
            <div class="form-group">
                <div class="col-sm-1"></div>
                <label for="BasicSaving" class="col-sm-6 control-label" style="text-align: left;"><?php echo lang('Simpanan pokok'); ?></label>
                <div class="col-sm-5">
                    <input type="number" name="BS" class="form-control" id="BasicSaving" placeholder="" value="<?=$mgs4?>">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-1"></div>
                <label for="RegulerSavingsInterest" class="col-sm-6 control-label" style="text-align: left;"><?php echo lang('Bunga tabungan'); ?></label>
                <div class="col-sm-5">
                    <input type="number" name="RSI" class="form-control" id="RegulerSavingsInterest" placeholder="" value="<?=$mgs5?>">
                </div>
            </div>
            <div class="form-group">
                <label for="Accounting" class="col-sm-6 control-label" style="text-align: left;"><?php echo lang('Aktifkan akuntansi'); ?></label>
                <div class="col-sm-6">
                    <div class="radio" style="border: 1px solid #d2d6de;padding: 7px;border-radius:15px;">
                        <label style="padding-left: 10px">
                            <input type="radio" name="isAkun" class="minimal" id="isAkun" value="1" <?php if($mgs16 == 1){ echo "checked";} ?>>
                            Ya
                        </label>
                        <label>
                            <input type="radio" name="isAkun" class="minimal" value="0" <?php if($mgs16 ==  0){ echo "checked";} ?>>
                            Tidak
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="Accounting" class="col-sm-6 control-label" style="text-align: left;"><?php echo lang('Limit Gabungan (belanja+pinjaman)'); ?></label>
                <div class="col-sm-6">
                    <div class="radio" style="border: 1px solid #d2d6de;padding: 7px;border-radius:15px;">
                        <label style="padding-left: 10px">
                            <input type="radio" name="isLimit" class="minimal" value="2" <?php if($mgs17 == 2){ echo "checked";} ?>>
                            Ya
                        </label>
                        <label>
                            <input type="radio" name="isLimit" class="minimal" value="1" <?php if($mgs17 ==  1){ echo "checked";} ?>>
                            Tidak
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="form-group">
                <label for="cooperativeID" class="col-sm-3 control-label" style="text-align: left;"><?php echo lang('Jam cut off harian'); ?></label>
                <div class="col-sm-6">
                    <input type="text" name="DCT" class="form-control" id="cooperativeID" placeholder="00:00:00" value="00:00:00" readonly>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="PaymentPriority" class="col-sm-6 control-label" style="text-align: left;"><?php echo lang('Hari kredit'); ?>(<?php echo lang('Tahunan'); ?>)</label>
                        <div class="col-sm-4">
                            <input type="number" name="CRD" class="form-control" id="Days" placeholder=""  value="<?=$mgs7?>">
                        </div>
                        <div class="col-sm-2">
                            <label class="control-label" style="text-align: left;font-weight: normal;"><?php echo lang('Hari'); ?></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="BasicSaving" class="col-sm-6 control-label"></label>
                        <div class="col-sm-4">
                            <input type="number" name="CRW" class="form-control" id="Weeks" placeholder="" value="<?=$mgs8?>">
                        </div>
                        <div class="col-sm-2">
                            <label class="control-label" style="text-align: left;font-weight: normal;"><?php echo lang('Minggu'); ?></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Loan" class="col-sm-6 control-label"></label>
                        <div class="col-sm-4">
                            <input type="number" name="CRM" class="form-control" id="Months" placeholder="" value="12" readonly>
                        </div>
                        <div class="col-sm-2">
                            <label class="control-label" style="text-align: left;font-weight: normal;"><?php echo lang('Bulan'); ?></label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="col-sm-4">
                        <label for="PaymentPriority" class="control-label" style="text-align: left;"><?php echo lang('Aktual hari'); ?></label>
                    </div>
                    <div class="col-sm-6">
                        <div class="radio" style="border: 1px solid #d2d6de;padding: 7px;border-radius:15px;">
                            <label style="padding-left: 10px">
                                <input type="radio" name="isRealTime" class="minimal" id="optionsRadios1" value="1" <?php if($mgs10 == 1){ echo "checked";} ?>>
                                <?php echo lang('Ya'); ?>
                            </label>
                            <label>
                                <input type="radio" name="isRealTime" class="minimal" id="optionsRadios2" value="0" <?php if($mgs10 == ""){ echo "";}else{if($mgs10 == 0){ echo "checked";}} ?>>
                                <?php echo lang('Tidak'); ?>
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-2">
                    </div>
                </div>
            </div>



            <div class="form-group">
                <label for="StoreCredit" class="col-sm-3 control-label" style="text-align: left;"><?php echo lang('Hari kredit'); ?>(<?php echo lang('Bulanan'); ?>)</label>
                <div class="col-sm-2">
                    <input type="number" name="CDM" class="form-control" id="CreditDays" placeholder="" value="<?=$mgs11?>">
                </div>
                <div class="col-sm-1"></div>
                <label for="PaymentPriority" class="col-sm-2 control-label" style="text-align: left;"><?php echo lang('Aktual hari'); ?></label>
                <div class="col-sm-3">
                    <div class="radio" style="border: 1px solid #d2d6de;padding: 7px;border-radius:15px;">
                        <label style="padding-left: 10px">
                            <input type="radio" name="isRealDay" class="minimal" id="optionsRadios1" value="1" <?php if($mgs12 == 1){ echo "checked";} ?>>
                            <?php echo lang('Ya'); ?>
                        </label>
                        <label>
                            <input type="radio" name="isRealDay" class="minimal" id="optionsRadios2" value="0" <?php if($mgs12 == ""){ echo "";}else{if($mgs12 == 0){ echo "checked";}} ?>>
                            <?php echo lang('Tidak'); ?>
                        </label>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="Interest Text" class="col-sm-3 control-label" style="text-align: left;"><?php echo lang('Pajak bunga'); ?></label>
                <div class="col-sm-3">
                    <div class="radio" style="border: 1px solid #d2d6de;padding: 7px;border-radius:15px;">
                        <label style="padding-left: 10px">
                            <input type="radio" name="isInterestTax" class="minimal" id="optionsRadios1" value="1" <?php if($mgs13 == 1){ echo "checked";} ?>>
                            <?php echo lang('Ya'); ?>
                        </label>
                        <label>
                            <input type="radio" name="isInterestTax" class="minimal" id="optionsRadios2" value="0" <?php if($mgs13 ==  0){ echo "checked";} ?>>
                            <?php echo lang('Tidak'); ?>
                        </label>
                    </div>
                </div>
                <label for="Interest Text" class="col-sm-2 control-label" style="text-align: center;padding-top:0;"><?php echo lang('Persentase pajak'); ?>(%)</label>
                <div class="col-sm-3">
                    <input type="number" name="TaxRate" class="form-control" id="CreditDays" placeholder=""  value="<?=$mgs14?>">
                </div>
            </div>
            <div class="form-group">
                <label for="MinimumInterest" class="col-sm-3 control-label" style="text-align: left;padding-top:0;"><?php echo lang('Minimum jumlah bunga untuk dikenakan pajak'); ?></label>
                <div class="col-sm-4">
                    <input type="text" name="MinimumTax" class="form-control price" id="MinimumInterest" placeholder=""  value="<?=$mgs15?>">
                </div>
            </div>
        </div>
    </div>
</div>
<div class="box-footer">
    <div class="row">
        <div class="col-sm-4">
        </div>
        <div class="col-sm-4">
            <?php if(count($mgsrow[0]) > 0){ ?>
                <button type="submit" class="btn btn-flat btn-block btn-success pull-right"><?php echo lang('Perbaharui'); ?></button>
            <?php }else{ ?>
                <button type="submit" class="btn btn-flat btn-block btn-primary pull-right"><?php echo lang('Simpan'); ?></button>
            <?php } ?>
        </div>
        <div class="col-sm-4">
        </div>
    </div>
</div>
</form>
<?php require('content-footer.php');?>

<?php require('footer.php');?>
