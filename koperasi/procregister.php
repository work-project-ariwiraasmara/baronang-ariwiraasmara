<?php
session_start();
require("lib/PHPMailer_5.2.0/class.phpmailer.php");

include("connect.php");

$uid = $_POST['uid'];
$name = $_POST['name'];
$email = $_POST['email'];
$telp = $_POST['telp'];
$address = $_POST['address'];
$province = $_POST['province'];
$city = $_POST['city'];

if($uid == "" || $name == "" || $email == "" || $telp == "" || $address == "" || $province == "" || $city == ""){
    messageAlert('Harap isi seluruh kolom','info');
    header('Location: register.php');
}
else{
    $a = "select* from KoneksiKoperasiBaronang.[dbo].[UserRegister] where UserIDBaronang = '$uid' or emailKoperasi = '$email'";
    $b = sqlsrv_query($conn, $a);
    $c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC);

    $aa = "select* from PaymentGateway.[dbo].[UserPaymentGateway] where KodeUser = '$uid'";
    $bb = sqlsrv_query($conn, $aa);
    $cc = sqlsrv_fetch_array($bb, SQLSRV_FETCH_NUMERIC);

    if($c != null){
        messageAlert('Tidak dapat membuat koperasi baru. User ID Baronang App / Email sudah terdaftar.','warning');
        header('Location: register.php');
    }
    else if($cc == null){
        messageAlert('Tidak dapat melakukan pendaftaran koperasi. User ID Baronang App tidak terdaftar. Harap melakukan pendaftaran melalui Baronang App','warning');
        header('Location: register.php');
    }
    else{
        $ppsql = "exec KoneksiKoperasiBaronang.[dbo].[ProsesRegister] '$uid', '$email','$telp','$name','$address','$province','$city'";
        $ppstmt = sqlsrv_query($conn, $ppsql);

        //send email
        //send ke user
        sendEmail($cc[2], $cc[11], $cc[1]);

        if($cc[2] != $email){
            //send ke koperasi
            sendEmail($email, $cc[11], $name);
        }

        if($ppstmt){
            messageAlert('Berhasil melakukan pendaftaran koperasi. Harap periksa email anda untuk melakukan konfirmasi.','success');
            header('Location: register.php');
        }
        else{
            messageAlert('Gagal membuat koperasi','danger');
            header('Location: register.php');
        }
    }
}

function sendEmail($email,$kode,$nama){
    $root = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/';
    $link = $root."koperasi/confregister.php?u=".$kode;

    $mail = new PHPMailer();
    $mail->IsSMTP();
    $mail->Host = "baronang.com";  // specify main and backup server
    $mail->SMTPAuth = true;     // turn on SMTP authentication
    $mail->Username = "konfirmasi@baronang.com";  // SMTP username
    $mail->Password = "alva7000"; // SMTP password
    $mail->Port = 587;
    $mail->SMTPDebug = 1;

    $mail->From = "konfirmasi@baronang.com";
    $mail->FromName = "Baronang Mail Service";
    $mail->AddAddress($email);
    $mail->AddReplyTo("konfirmasi@baronang.com", "Baronang Konfirmasi");
    $mail->IsHTML(true);// set email format to HTML

    $mail->Subject = "Email Verifikasi Koperasi";
    $mail->Body =
        "Hi! ".$nama.",<br><br>
            Terima kasih telah bergabung dengan Baronang.<br>
            Kami perlu memastikan bahwa email anda valid. Harap mengkonfirmasi email anda dengan mengklik link dibawah ini.<br>
            <a href=".$link.">Konfirmasi</a>
            <br>
            <br>
            <br>
            <br>
            Terima kasih.<br>
            Salam,<br>
            <br>
            <br>
            Baronang";
    if(!$mail->Send())
    {
        messageAlert('Message could not be sent. Mailer Error:'.$mail->ErrorInfo,'warning');
        header('Location: register.php');
    }
}
?>