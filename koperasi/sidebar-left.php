<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="static/images/No-Image-Icon.png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p><?php echo $NameUser; ?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <div></div>
        <?php
        //hak akses
        $w = array();
        $aaa = "select * from [dbo].[UserLevelMenu] where KodeUserLevel = '$JabatanUser'";
        $bbb = sqlsrv_query($conn, $aaa);
        while($ccc = sqlsrv_fetch_array($bbb, SQLSRV_FETCH_NUMERIC)){
            array_push($w, $ccc[2]);
        }
        ?>
        <ul class="sidebar-menu">
            <li class="header"><?php echo lang('Menu utama'); ?></li>

            <?php
            //table menu header
            $x = "select * from [dbo].[MenuHeader] where Status = 1 order by HeaderNo ASC";
            $y = sqlsrv_query($conn, $x);
            while($z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC)){
            ?>
                <li class="treeview">
                    <a href="#">
                        <i class="<?php echo $z[3]; ?>"></i> <span><?php echo $z[1]; ?></span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <?php
                        //table menu detail
                        $xx = "select * from [dbo].[MenuDetail] where HeaderNo = '$z[0]' and Status = 1 and isVisible = 1 order by DetailNo ASC";
                        $yy = sqlsrv_query($conn, $xx);
                        while($zz = sqlsrv_fetch_array( $yy, SQLSRV_FETCH_NUMERIC)){
                            //cek hak akses
                            if(in_array($zz[3], $w) or $zz[5] == 1){
                            ?>
                        <li class="">
                            <a href="<?php echo $zz[3]; ?>"><i class="<?php echo $zz[6]; ?>"></i> <?php echo lang($zz[2]); ?>
                                <?php if($zz[5] == 1){ ?>
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                <?php } ?>
                            </a>
                            <?php if($zz[5] == 1){ ?>
                                <ul class="treeview-menu">
                                    <?php
                                    //table menu child
                                    $xxx = "select * from [dbo].[MenuChild] where HeaderNo = '$z[0]' and DetailNo = '$zz[1]' and Status = 1 order by ChildNo ASC";
                                    $yyy = sqlsrv_query($conn, $xxx);
                                    while($zzz = sqlsrv_fetch_array( $yyy, SQLSRV_FETCH_NUMERIC)){
                                        if(in_array($zzz[4], $w)){
                                    ?>
                                        <li class=""><a href="<?php echo $zzz[4]; ?>"><i class="<?php echo $zzz[6]; ?>"></i> <?php echo lang($zzz[3]); ?></a></li>
                                    <?php }
                                    } ?>
                                </ul>
                            <?php } ?>
                        </li>
                        <?php }
                        } ?>
                    </ul>
                </li>
            <?php } ?>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
