<?php require('header.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
    <div class="alert alert-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-warning ?>"></i> <?php echo ucfirst($_SESSION['error-type']); ?></h4>
        <?php echo $_SESSION['error-message']; ?>
    </div>
<?php } ?>

<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title"><?php echo lang('Upload Data Saldo Simpanan Pokok'); ?></h3>
    </div>
    <!-- /.box-header -->
    <form action="ubas_balance.php" method="post" enctype="multipart/form-data">
    <div class="box-body">
        <div class="col-xs-12 table-responsive">
            <div class="row">
                <div class="callout callout-info">
                    <h4><?php echo lang('Informasi'); ?></h4>

                    <p><?php echo lang('Formulir ini hanya menerima 1 (satu) kali upload'); ?></p>
                </div>

                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="ktp" class="col-sm-3 control-label" style="text-align: left;"><?php echo lang('Data Excel'); ?></label>
                        <div class="col-sm-5">
                            <input type="file" name="filename" class="form-control" accept=".xls"  required="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="box-footer">
        <div class="row">
            <div class="col-sm-4">
                <input type="submit" class="btn btn-flat btn-block btn-success btn-sm">
            </div>
            <div class="col-sm-4">

            </div>
            <div class="col-sm-4">
                <a href="dbas_balance.php" class="btn btn-flat btn-block btn-info pull-right">Download Template</a>
            </div>
        </div>
    </div>
    </form>
</div>

<?php require('content-footer.php');?>

<?php require('footer.php');?>
