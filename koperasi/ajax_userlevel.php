<html>
<head>
    <!-- Select2 -->
    <link rel="stylesheet" href="static/plugins/select2/select2.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="static/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="static/dist/css/skins/_all-skins.min.css">
</head>
<body>
<table class="table table-bordered table-striped">
    <?php
    include "connect.php";
    $a = "select * from dbo.MenuHeader where Status=1";
    $b = sqlsrv_query($conn, $a);
    while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
        ?>
        <tr>
            <th><label><?php echo $c[1]; ?></label></th>
            <td>
                <table class="table table-bordered">
                    <tr>
                        <?php
                        $aa = "select * from dbo.MenuDetail where HeaderNo='$c[0]' and Status=1";
                        $bb = sqlsrv_query($conn, $aa);
                        while($cc = sqlsrv_fetch_array($bb, SQLSRV_FETCH_NUMERIC)){
                        ?>
                        <?php if($cc[5] == 0){ ?>
                            <?php
                            $x = "select * from dbo.UserLevelMenu where KodeUserLevel='$_POST[param]' and MenuName='$cc[3]'";
                            $y = sqlsrv_query($conn, $x);
                            $z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC);
                            $le = '';
                            $ap = '';
                            if($z != NULL){
                                $le = 'checked';

                                if($z[1] == 1){
                                    $ap = 'checked';
                                }
                            }
                            ?>
                            <td>
                                <input type="checkbox" class="minimal" value="1" <?php echo $ap; ?> disabled> <i class="fa fa-user"></i>
                                <input type="checkbox" class="minimal" value="<?php echo $cc[3]; ?>" <?php echo $le; ?> disabled> <?php echo $cc[2]; ?>
                            </td>
                        <?php } else { ?>
                    <tr>
                        <td><label><?php echo $cc[2]; ?></label></td>
                        <td>
                            <table class="table table-bordered">
                                <tr>
                                    <?php
                                    $aaa = "select * from dbo.MenuChild where HeaderNo='$c[0]' and DetailNo='$cc[1]' and Status=1";
                                    $bbb = sqlsrv_query($conn, $aaa);
                                    while($ccc = sqlsrv_fetch_array($bbb, SQLSRV_FETCH_NUMERIC)){
                                        ?>
                                        <?php
                                        $xx = "select * from dbo.UserLevelMenu where KodeUserLevel='$_POST[param]' and MenuName='$ccc[4]'";
                                        $yy = sqlsrv_query($conn, $xx);
                                        $zz = sqlsrv_fetch_array($yy, SQLSRV_FETCH_NUMERIC);
                                        $lle = '';
                                        $aap = '';
                                        if($zz != NULL){
                                            $lle = 'checked';

                                            if($zz[1] == 1){
                                                $aap = 'checked';
                                            }
                                        }
                                        ?>
                                        <td>
                                            <input type="checkbox" class="minimal" value="1" <?php echo $aap; ?> disabled> <i class="fa fa-user"></i>
                                            <input type="checkbox" class="minimal" value="<?php echo $ccc[4]; ?>" <?php echo $lle; ?> disabled> <?php echo $ccc[3]; ?>
                                        </td>
                                    <?php } ?>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <?php } ?>
                    <?php } ?>
                    </tr>
                </table>
            </td>
        </tr>
    <?php } ?>
</table>

    <!-- Select2 -->
    <script src="static/plugins/select2/select2.full.min.js"></script>
    <!-- AdminLTE App -->
    <script src="static/dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="static/dist/js/demo.js"></script>

    <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2();
        });
    </script>
</body>
</html>