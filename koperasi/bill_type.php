<?php require('header.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<?php
$new = 'hide';
?>

<?php
//edit
$uledit = $_GET['edit'];
$ul0    = "";
$ul1    = "";
$ul2    = "";
$ul3    = "";
$ul4    = "";
$ulprocedit = "";
$uldisabled = "";
if(!empty($uledit)){
    $eulsql   = "select * from dbo.BillType where KodeTipe='$uledit'";
    $eulstmt  = sqlsrv_query($conn, $eulsql);
    $eulrow   = sqlsrv_fetch_array( $eulstmt, SQLSRV_FETCH_NUMERIC);
    if(count($eulrow[0]) > 0){
        $ul0    = $eulrow[0];
        $ul1    = $eulrow[1];
        $ul2    = $eulrow[4];
        $ul3    = $eulrow[7];
        $ul4    = $eulrow[2];
        $ul5    = $eulrow[6];
        $ul6    = $eulrow[5];
        $ul7    = $eulrow[3];
        $ul8    = $eulrow[8];
        $ulprocedit = "?page=".$_GET['page']."&edit=".$ul0;
        $uldisabled = "disabled";
        $uldisabled2 = "readonly";
    }
    else{
        if(empty($_GET['page'])){
            echo "<script language='javascript'>document.location='bill_type.php';</script>";
        }
        else{
            echo "<script language='javascript'>document.location='bill_type.php?page=".$_GET['page']."';</script>";
        }
    }
}
?>

<?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
    <div class="alert alert-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-warning ?>"></i> <?php echo ucfirst($_SESSION['error-type']); ?></h4>
        <?php echo $_SESSION['error-message']; ?>
    </div>
<?php } ?>
<script type="text/javascript">
function demoDisplay() {
    $("#mytext").removeClass('hide');
}

function demoVisibility() {
    $("#mytext").addClass('hide');
}
</script>

<div class="box box-solid">
    <div class="box-header with-border">
        <h3 class="box-title"><?php echo lang('Tipe Billing'); ?></h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <form class="form-horizontal" action="procbill_type.php<?=$ulprocedit?>" method="POST">
        <div class="box-body">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="AccountNumber" class="col-sm-4 control-label" style="text-align: left;"><?php echo lang('Deskripsi'); ?></label>
                        <div class="col-sm-6">
                            <input type="text" name="des" class="form-control" id="AccountNumber" placeholder="" value="<?=$ul1?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="AccountNumber" class="col-sm-4 control-label" style="text-align: left;"><?php echo lang('Tanggal Cut Off'); ?></label>
                        <div class="col-sm-6">
                            <input type="number" name="cutoff" class="form-control price" id="cutoff" placeholder="" value="<?=$ul3?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="AccountNumber" class="col-sm-4 control-label" style="text-align: left;"><?php echo lang('Tempo Hari Bill'); ?></label>
                        <div class="col-sm-6">
                            <input type="number" name="tempoharibill" class="form-control price" id="tempoharibill" placeholder="" value="<?=$ul3?>">
                        </div>
                    </div>

                    <div class="form-group">
                            <div class="col-sm-12">
                                <b><?php echo lang('Skema Diskon'); ?></b>
                                <table class="table table-bordered table-striped" id="tskema">
                                    <thead>
                                    <tr>
                                        <th><?php echo lang('No'); ?></th>
                                        <th><?php echo lang('Jumlah Pelunasan'); ?></th>
                                        <th><?php echo lang('Nominal Diskon'); ?></th>
                                        <th></th>
                                    </tr>
                                    <?php
                                    $jum = 1;
                                    $qwe = "select * from [dbo].[DiskonSkemaBill] where KodeTipe = '$uledit' order by JumlahPelunasan asc";
                                    $asd = sqlsrv_query($conn, $qwe);
                                    while($zxc = sqlsrv_fetch_array($asd, SQLSRV_FETCH_NUMERIC)){
                                        ?>
                                        <tr>
                                            <td><?php echo $jum; ?></td>
                                            <td><?php echo $zxc[1]; ?></td>
                                            <td><?php echo number_format($zxc[2]); ?></td>
                                            <td>
                                                <a href="procbill_type.php?del=<?php echo $zxc[0]; ?>&u=<?php echo $zxc[1]; ?>"><button type="button" class="btn btn-sm btn-danger btn-delete"><i class="fa fa-times"></i> </button></a>
                                            </td>
                                        </tr>
                                        <?php

                                            $jum++;

                                            }
                                    ?>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <td><?php echo $jum; ?></td>
                                        <td><input type="number" id="jumlah" class="form-control" value="0"></td>
                                        <td><input type="text" id="nominal" class="form-control price" value="0"></td>
                                        <td><button type="button" class="btn btn-sm btn-success btn-add"><?php echo lang('Tambah'); ?></button></td>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                    </div>

                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="AccountName" class="col-sm-2 control-label" style="text-align: left;"><?php echo lang('Nominal'); ?></label>
                        <div class="col-sm-6">
                            <input type="text" name="amo" class="form-control price" id="AccountName" placeholder="" value="<?=$ul4?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="AccountName" class="col-sm-2 control-label" style="text-align: left;"><?php echo lang('Denda Bill'); ?></label>
                        <div class="col-sm-6">
                            <input type="text" name="dendabill" class="form-control price" id="dendabill" placeholder="" value="<?=$ul5?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="AccountName" class="col-sm-2 control-label" style="text-align: left;"><?php echo lang('Admin Bill'); ?></label>
                        <div class="col-sm-6">
                            <input type="text" name="adminbill" class="form-control price" id="adminbill" placeholder="" value="<?=$ul6?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-2"></div>
                        <div class="col-sm-6">
                            <div class="radio" style="border: 1px solid #d2d6de;padding: 7px;border-radius:15px;">
                                <?php
                                $s1 = '';
                                $s0 = '';
                                $s2 = '';
                                if($ul7 == 1){
                                    $s1 = 'checked';
                                }
                                else if($ul7 == 0){
                                    $s0 = 'checked';
                                }
                                else{
                                    $s2 = 'checked';
                                }
                                ?>
                                <label>
                                    <input type="radio" onChange="demoDisplay();" checked name="bank" id="optionsRadios1" value="1" <?php echo $s1; ?>>
                                    <?php echo lang('Period'); ?>
                                </label>
                                <label>
                                    <input type="radio" onClick="demoVisibility();" name="bank" id="optionsRadios2" value="0" <?php echo $s0; ?>>
                                    <?php echo lang('Sekali Bill'); ?>
                                </label>
                                <label>
                                    <input type="radio" onClick="demoVisibility();" name="bank"  id="optionsRadios3" value="2" <?php echo $s2; ?>>
                                    <?php echo lang('Unlimited'); ?>
                                </label>
                            </div>
                        </div>
				   </div>
				   <?php
				   $h = 'hide';
				   if(isset($_GET['edit']) and $ul7 == 1){
					   $h = '';
				   }
				   ?>
				   <div id="mytext" class="<?php echo $h; ?>">
                    <div class="form-group">
                        <label for="AccountNumber" class="col-sm-2 control-label" style="text-align: left;"><?php echo lang('Periode'); ?></label>
                        <div class="col-sm-6">
                            <input type="number" name="period" class="form-control" id="period" placeholder="" value="<?=$ul2?>">
                        </div>
                    </div>
				   </div>
                </div>
            </div>
        </div>
</div>
<!-- /.box-body -->

<?php if($ul0 != '') { ?>
<div class="box box-solid">
    <div class="box-header with-border">
        <h3 class="box-title"><?php echo lang('Billing'); ?></h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
      <div class="box-body">
          <div class="col-sm-12">
                <ul class="nav nav-tabs">
                    <?php
                    $no = 1;
                    $qwe = "select * from [dbo].[JabatanView] order by KodeJabatan asc";
                    $asd = sqlsrv_query($conn, $qwe);
                    while($zxc = sqlsrv_fetch_array($asd, SQLSRV_FETCH_NUMERIC)){
                        if($no == 1){
                            $stat = 'active';
                        }
                        else{
                            $stat = '';
                        }
                    ?>
                        <li class="<?php echo $stat; ?>"><a href="#tab_<?php echo $no; ?>" data-toggle="tab"><?php echo ucfirst($zxc[1]); ?></a></li>
                    <?php $no++; } ?>
                </ul>
            </div>
                <div class="tab-content">
                    <?php
                    $n = 1;
                    $qw = "select * from [dbo].[JabatanView] order by KodeJabatan asc";
                    $as = sqlsrv_query($conn, $qw);
                    while($zx = sqlsrv_fetch_array($as, SQLSRV_FETCH_NUMERIC)){
                        if($n == 1){
                            $status = 'active';
                        }
                        else{
                            $status = '';
                        }
                    ?>
                    <div class="tab-pane <?php echo $status; ?>" id="tab_<?php echo $n; ?>">
                            <div class="col-sm-6">
                                <b>Belum ditambahkan</b>
                                <table class="table table-hover table-striped table-bordered">
                                    <tr>
                                        <th><?php echo lang('Member'); ?></th>
                                    </tr>
                                    <?php
                                    $julsql   = "select * from [dbo].[MemberList] where KodeJabatan = '".$zx[0]."'";
                                    $julstmt = sqlsrv_query($conn, $julsql);
                                    while($rjulrow  = sqlsrv_fetch_array( $julstmt, SQLSRV_FETCH_NUMERIC)){
                                    ?>
                                    <tr>
                                        <td>
                                            <input type="checkbox" name="KodeMember[]" value="<?php echo $rjulrow[1]; ?>">
                                            <?php echo ' '.$rjulrow[1].' - '.$rjulrow[2]; ?>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </table>
                            </div>
                            <div class="col-sm-6">
                                <b>Sudah ditambahkan</b>
                                <table class="table table-hover table-striped table-bordered">
                                    <tr>
                                        <th><?php echo lang('Member'); ?></th>
                                        <th><?php echo lang('Jumlah Billing'); ?></th>
                                    </tr>
                                    <?php
                                    $julsql   = "select
                                      a.MemberID,
                                      a.Name,
                                      (Select count(*) from dbo.PersonBill b where b.KodeMember = a.MemberID and KodeTipe = '".$_GET[edit]."') as Jumlah
                                    from [dbo].[MemberList] a where a.KodeJabatan = '".$zx[0]."' and a.MemberID in(select KodeMember from dbo.PersonBill where KodeTipe = '".$_GET[edit]."')";
                                    $julstmt = sqlsrv_query($conn, $julsql);
                                    while($rjulrow  = sqlsrv_fetch_array( $julstmt, SQLSRV_FETCH_NUMERIC)){
                                    ?>
                                    <tr>
                                        <td>
                                            <?php echo $rjulrow[0].' - '.$rjulrow[1]; ?>
                                        </td>
                                        <td>
                                              <?php echo $rjulrow[2]; ?>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </table>
                            </div>
                    </div>
                  <?php $n++; } ?>
                </div>
      </div>
</div>
<!-- /.box-body -->
<?php } ?>

<div class="box-footer">
    <div class="row">
        <div class="col-sm-4">
            <?php if(count($eulrow[0]) > 0){ ?>
                <button type="submit" class="btn btn-flat btn-block btn-success pull-left"><?php echo lang('Perbaharui'); ?></button>
            <?php } else { ?>
                <button type="submit" class="btn btn-flat btn-block btn-primary pull-right"><?php echo lang('Simpan'); ?></button>
            <?php } ?>
        </div>
        <div class="col-sm-4">
        </div>
        <div class="col-sm-4">
            <?php if(count($eulrow[0]) > 0){ ?>
                <a href="bill_type.php" class="btn btn-flat btn-block btn-default"><?php echo lang('Batal'); ?></a>
            <?php } ?>
        </div>
    </div>
</div>

</form>

<hr>

<!-- /.Left col -->
<div class="box box-solid">
    <div class="box-header">
        <h3 class="box-title"><?php echo lang('Daftar Tipe Bill'); ?></h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="col-xs-12 table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th><?php echo lang('No'); ?></th>
                    <th><?php echo lang('Kode Tipe Bill'); ?></th>
                    <th><?php echo lang('Deskripsi'); ?></th>
                    <th><?php echo lang('Nominal'); ?></th>
                    <th><?php echo lang('Jenis'); ?></th>
                    <th><?php echo lang('Tanggal Cut Off'); ?></th>
                    <th><?php echo lang('Aksi'); ?></th>
                </tr>
                </thead>
                <tbody>
                <?php
                //count
                $jmlulsql   = "select count(*) from dbo.BillType";
                $jmlulstmt  = sqlsrv_query($conn, $jmlulsql);
                $jmlulrow   = sqlsrv_fetch_array( $jmlulstmt, SQLSRV_FETCH_NUMERIC);

                //pagging
                $perpages   = 10;
                $halaman    = $_GET['page'];
                if(empty($halaman)){
                    $posisi  = 0;
                    $batas   = $perpages;
                    $halaman = 1;
                }
                else{
                    $posisi  = (($perpages * $halaman) - 10) + 1;
                    $batas   = ($perpages * $halaman);
                }

                $ulsql = "SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY KodeTipe asc) as row FROM [dbo].[BillType]) a WHERE row between '$posisi' and '$batas'";

                //$ulsql = "select * from [dbo].[BasicSavingType]";

                $ulstmt = sqlsrv_query($conn, $ulsql);

                while($ulrow = sqlsrv_fetch_array( $ulstmt, SQLSRV_FETCH_NUMERIC)){
                    $status = '';
                    if($ulrow[3] == 1){
                        $status = lang('Period');
                    }
                    else if($ulrow[3] == 0){
                        $status = lang('Sekali Bill');
                    }
                    else{
                        $status = lang('Unlimited');
                    }
                    ?>
                    <tr>
                        <td><?=$ulrow[9];?></td>
                        <td><?=$ulrow[0];?></td>
                        <td><?=$ulrow[1];?></td>
                        <td><?=number_format($ulrow[2],0,',','.');?></td>
                        <td><?=$status;?></td>
                        <td><?=$ulrow[7];?></td>
                        <td width="20%" style="padding: 3px">
                            <div class="btn-group" style="padding-right: 15px">
                                <a href="bill_type.php?page=<?=$halaman?>&edit=<?=$ulrow[0]?>" class="btn btn-default btn-flat btn-sm text-green" title="edit"><i class="fa fa-pencil-square-o"></i></a>
                            </div>
                        </td>
                    </tr>
                <?php
                }

                $jmlhalaman = ceil($jmlulrow[0]/$perpages);
                ?>
                </tbody>
            </table>
        </div>
        <div class="box-footer clearfix">
            <label><?php echo lang('Menampilkan'); ?> <?=$halaman?> to <?=$jmlhalaman?> of <?=$jmlulrow[0]?> <?php echo lang('Masukan'); ?></label>
            <?=$posisi." - ".$batas?>
            <ul class="pagination pagination-sm no-margin pull-right">
                <li class="paginate_button"><a href="bill_type.php">&laquo;</a></li>
                <?php
                for($ul = 1; $ul <= $jmlhalaman; $ul++){
                    if($ul != $halaman){ $ulpageactive = ""; $ulpagedisabled = "";}else{$ulpageactive = "active"; $ulpagedisabled = "disabled";}
                    echo '<li class="paginate_button '.$ulpageactive.'"><a href="bill_type.php?page='.$ul.'" '.$ulpagedisabled.'>'.$ul.'</a></li>';
                }
                ?>
                <li class="paginate_button"><a href="bill_type.php?page=<?=$jmlhalaman?>">&raquo;</a></li>
            </ul>
        </div>
    </div>
</div>

<script type="text/javascript">
var tampung = [];

$('.btn-delete').click(function(){
   if(confirm('<?php echo lang('Urutan dibawah kolom ini akan dihapus juga. Apakah anda yakin'); ?> ?') == false){
   return false;
   }
});

$('.btn-add').click(function(){
    var jumlah = $('#jumlah').val();
    var nominal = $('#nominal').val();

    if(jumlah == '' || nominal == ''){
    alert('<?php echo lang('Harap isi inputan dengan benar'); ?>');
    return false;
    }
    else{
    $.ajax({
           url : "ajax_addskemabill.php",
           type : 'POST',
           dataType : 'json',
           data: { jumlah: jumlah, nominal: nominal},   // data POST yang akan dikirim
           success : function(data) {



           $("#tskema tbody").append("<tr>" +
                                     "<td></td>" +
                                     "<td><input type='hidden' name='jumlah[]' value="+ data.jumlah +">" + data.jumlah + "</td>" +
                                     "<td><input type='hidden' name='nominal[]' value="+ data.nominal +">" + data.nominal + "</td>" +
                                     "<td><a class='btn btn-default btn-flat btn-sm text-red remove' onclick='$(this).parent().parent().remove();'><i class='fa fa-trash-o'></i></a></td>" +
                                     "</tr>");
           },
           error : function() {
           alert('<?php echo lang('Telah terjadi error'); ?>');
           return false;
           }
           });
    }
});
</script>

<?php require('content-footer.php');?>

<?php require('footer.php');?>
