<?php require('header.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

 <div class="table-responsive">
            <table id="example1" class="table table-bordered table-striped DataTable" role="grid" aria-describedby="example1_info">
               
                <tbody>

                    <?php if($_GET['acc']){ ?>

                        <div class="table-responsive">
                            <table class="table table-bordered">

                            <thead>
                                <tr>
                                    <th>No Transaksi</th>
                                    <th>Tanggal Transaksi</th>
                                    <th><?php echo lang('Tipe Transaksi'); ?></th>
                                    <th><?php echo lang('Debit'); ?></th>
                                    <th><?php echo lang('Kredit'); ?></th>
                                </tr>
                            </thead>
                
                            <?php
                            /*<!-- //count
                            $jmlulsql   = "select count($_GET[acc]) from dbo.TampilDetailReportCOA";
                            $jmlulstmt  = sqlsrv_query($conn, $jmlulsql);
                            $jmlulrow   = sqlsrv_fetch_array($jmlulstmt, SQLSRV_FETCH_NUMERIC);
                            var_dump($jmlulrow).die; --> */

                            //pagging
                            $perpages   = 10;
                            $halaman    = $_GET['page'];
                            if(empty($halaman)){
                                $posisi  = 0;
                                $batas   = $perpages;
                                $halaman = 1;
                                    }
                            else{
                                $posisi  = (($perpages * $halaman) - 10) + 1;
                                $batas   = ($perpages * $halaman);
                            }

                            if($_GET['st'] == 1){
                                $aaa = "SELECT [TransactionNumber],[AccNo],[TimeStam],[KodeBasicSavingType],[KodeTransactionType],[Descriptions],[BillingAmmount],[PaymentAmmount],[UserID]FROM [dbo].[BasicSavingTransaction] where AccNo ='$_GET[acc]' ";
                            }
                            else if ($_GET['st'] == 2){
                                $aaa = "SELECT [TransactionNumber],[AccNumber],[TimeStam],[KodeTransactionType],[Descriptions],[Note],[DebetHide],[KreditHide],[Debet] FROM [dbo].[RegularSavingTransView] where AccNumber='$_GET[acc]'";
                            }
                            else if ($_GET['st'] == 3) {
                                $aaa = "SELECT [TransactionNumber],[AccountNumber],[TimeStamp],[KodeTransactionType],[Description],[Message],[Debit],[Kredit],[UserID] FROM [dbo].[TimeDepositTrans]where AccountNumber='$_GET[acc]'";
                            }else{
                                $aaa= "SELECT [TransactionNumber],[LoanNumber],[TimeStamp],[KodeTransactionType],[Description],'',[PrincipalPayment],[InterestPayment],[UserID]FROM [dbo].[LoanTransView]  where LoanNumber='$_GET[acc]'";
                            }
                            $bbb = sqlsrv_query($conn,$aaa );
                            $jmlpage=0;
                            while($ccc = sqlsrv_fetch_array($bbb, SQLSRV_FETCH_NUMERIC)){
        
                            ?>
                                <tr>
                                    <td><?php echo $ccc[0]; ?></td>
                                    <td><?php echo $ccc[2]->format('Y-m-d H:i:s'); ?></td>
                                    <td>
                                        <?php
                                        $aaaa = "select* from dbo.TransactionType where KodeTransactionType='".$ccc[3]."'";
                                        $bbbb = sqlsrv_query($conn, $aaaa);
                                        $cccc = sqlsrv_fetch_array($bbbb, SQLSRV_FETCH_NUMERIC);
                                        if($cccc != null){
                                            echo $cccc[1];
                                        }
                                        else{
                                            echo $ccc[3];
                                        }
                                        ?>
                                    </td>
                                    <td align="right"><?php echo number_format($ccc[6]); ?></td>
                                    <td align="right"><?php echo number_format($ccc[7]) ; ?></td>
                                </tr>
                                <?php
                                $jmlpage++;
                                }
                                $jmlhalaman = ceil($jmlpage/$perpages);
                                ?>
                            <?php } ?>
                            </table>
                            <div class="box-footer clearfix right">
                                <div style="text-align: center;"><label>Showing <?=$halaman?> to <?=$jmlhalaman?> of <?=$jmlpage?> entries <?=$posisi." - ".$batas?></label></div>
                                <?php
                                    $reload = "rep_reg.php?acc=$_GET[acc];&st=2";
                                    $page = intval($_GET["page"]);
                                    $adjacents = isset($_GET['adjacents']) ? intval($_GET['adjacents']) : 2; // khusus style pagination 2 dan 3
                                    if( $page == 0 ) $page = 1;
                                    $tpages = ($jmlhalaman) ? ceil($jmlhalaman) : 0; // total pages, last page number
                                    echo "<div>".paginate($reload, $page, $tpages, $adjacents)."</div>";
                            ?>
                            </div>
</div>
                            


<?php
function paginate($reload, $page, $tpages, $adjacents) {

    $prevlabel = "&lsaquo; Prev";
    $nextlabel = "Next &rsaquo;";

    $out = "<nav><ul class=\"pagination\">\n";

    // previous
    if($page==1) {
        $out.= "<li class=\"disabled\"><a href=\"#\">" . $prevlabel . "</a></li>\n";
    }
    elseif($page==2) {
        $out.= "<li><a href=\"" . $reload . "\">" . $prevlabel . "</a></li>\n";
    }
    else {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" . ($page-1) . "\">" . $prevlabel . "</a></li>\n";
    }

    // first
    if($page>($adjacents+1)) {
        $out.= "<li><a href=\"" . $reload . "\">1</a></li>\n";
    }

    // interval
    if($page>($adjacents+2)) {
        $out.= "<li class=\"disabled\"><a href=\"#\">...</a></li>\n";
    }

    // pages
    $pmin = ($page>$adjacents) ? ($page-$adjacents) : 1;
    $pmax = ($page<($tpages-$adjacents)) ? ($page+$adjacents) : $tpages;
    for($i=$pmin; $i<=$pmax; $i++) {
        if($i==$page) {
            //$out.= "<li class=\"active\"><a href=\"#\">" . $i . "</a></li>\n";
            $out.= "<li class=\"active\"><span class=\"current\">Page " . $page . " of " . $tpages ."</span></li>";
        }
        elseif($i==1) {
            $out.= "<li><a href=\"" . $reload . "\">" . $i . "</a></li>\n";
        }
        else {
            $out.= "<li><a href=\"" . $reload . "&amp;page=" . $i . "\">" . $i . "</a></li>\n";
        }
    }

    // interval
    if($page<($tpages-$adjacents-1)) {
        $out.= "<li class=\"disabled\"><a href=\"#\">...</a></li>\n";
    }

    // last
    if($page<($tpages-$adjacents)) {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" . $tpages . "\">" . $tpages . "</a></li>\n";
    }

    // next
    if($page<$tpages) {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" . ($page+1) . "\">" . $nextlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabled\"><a href=\"#\">" . $nextlabel . "</a></li>\n";
    }

    $out.= "</ul></nav>";

    return $out;
}
?>
<?php require('content-footer.php');?>

<?php require('footer.php');?>
