<?php require('header.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>
<script language="javascript">
    function load_page(){
        var e = document.getElementById("produk");
        var a = document.getElementById("amount").value;
        var value = e.options[e.selectedIndex].value;
        var text = e.options[e.selectedIndex].text;

        var e2 = document.getElementById('csoaregsavmember').value;


        window.location.href = "csoaloanapp.php?memberid=" + e2 + "&loantype=" + value + "&amount=" + a;
    };
</script>

<?php
$memberid= $_GET['memberid'];
$loantype= $_GET['loantype'];
$amount= $_GET['amount'];

$xxxs  = "select * from [dbo].[BasicSavReport] where MemberID='$memberid'";
$xxxxs  = sqlsrv_query($conn, $xxxs);
while($xxs   = sqlsrv_fetch_array( $xxxxs, SQLSRV_FETCH_NUMERIC)){
    $names = $xxs[1];
    $_SESSION ['Namex']	= $names;
}

$_SESSION['selectdata']= $loantype;

$xc = '';
$max = 0;
$kata = '';
if (($memberid != '') and ($loantype !='')) {
    //config
    $mgssql    = "select * from [dbo].[GeneralSetingConfig] where KID='$_SESSION[KID]' and LoanType='$loantype'";
    $mgsstmt    = sqlsrv_query($conn, $mgssql);
    $mgsrow     = sqlsrv_fetch_array( $mgsstmt, SQLSRV_FETCH_NUMERIC);

    //kode loan app
    $sq  = "select dbo.getKodeLoanAppNumber('$memberid','$loantype')";
    $sqmt  = sqlsrv_query($conn, $sq);
    $row = sqlsrv_fetch_array( $sqmt, SQLSRV_FETCH_NUMERIC);
    $xc= $row[0];

    //cek max
    $aaa   = "select * from [dbo].[MemberListView] where MemberID='$memberid' and KID='$_SESSION[KID]'";
    $bbb  = sqlsrv_query($conn, $aaa);
    $ccc = sqlsrv_fetch_array( $bbb, SQLSRV_FETCH_NUMERIC);
    $max = $ccc[10];

    //cari history lama
    $x = "select * from [dbo].[LoanApplicationList] where MemberId='$memberid' and KodeLoanType='$loantype' and StatusComplete = 0";
    $y = sqlsrv_query($conn, $x);
    $z = sqlsrv_fetch_array( $y, SQLSRV_FETCH_NUMERIC);
    if($z != null){
        $amount = doubleval($z[5]);
        $xc = $z[1];

        $kata = 'Ditemukan pinjaman yang belum komplit. Harap segera lengkapi data.';
    }

    //cek doc
    $sql = "select * from [dbo].[MemberListDocUpload] where MemberID='$memberid'";
    $st = sqlsrv_query($conn, $sql);
    $cc = sqlsrv_fetch_array( $st, SQLSRV_FETCH_NUMERIC);

}

//batasi jumlah pinjam
if($amount != ''){
    //proses loan simulasi
    $sql = "exec dbo.ProsesLoadSimulasi '$xc','$loantype','$amount'";
    $stmt = sqlsrv_query($conn, $sql);
    sqlsrv_execute($stmt);

    $qwer = "select * from [dbo].[LoanType] where KodeLoanType='$loantype'";
    $asd = sqlsrv_query($conn, $qwer);
    $zxc = sqlsrv_fetch_array( $asd, SQLSRV_FETCH_NUMERIC);
    if($zxc != null){
        if($amount > $zxc[5] and $amount > $zxc[6]){
            echo "<script>alert('Melebihi batas limit pinjaman.')</script>";
            $amount  = 0;
        }
    }

    if($mgsrow != null){
        if($amount > $max and $mgsrow[6] ==0){
            echo "<script>alert('Jumlah pinjaman tidak boleh lebih dari maximum.')</script>";
            $amount  = 0;
        }
    }
}

?>
<div class="row">
    <form action="proccsoaloanapp.php" method="POST" class="form-horizontal" enctype="multipart/form-data">
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Pengajuan Pinjaman</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <div class="box-body">
                <?php if($kata != ''){ ?>
                <div class="alert alert-info alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <b><i class="icon fa fa-info"></i> Info!</b>
                    <?php echo $kata; ?>
                </div>
                <?php } ?>

                <div class="form-group">
                    <label for="csoaregsavmember" class="col-sm-4 control-label" style="text-align: left;">Member ID</label>
                    <div class="col-sm-8">
                        <input type="text" name="member" class="form-control" onblur="load_page();" id="csoaregsavmember" placeholder="" value="<?php echo $memberid; ?>" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="nama" class="col-sm-4 control-label" style="text-align: left;">Nama</label>
                    <div class="col-sm-8">
                        <input type="text" name="nama" class="form-control" id="nama" placeholder="" value="<?php echo"$names"; ?>" disabled>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label" style="text-align: left;">Produk Pinjaman</label>
                    <div class="col-sm-8">
                        <select name="produk" class="form-control" id="produk" onchange="load_page();">
                            <option>--- Select One ---</option>
                            <?php
                            $ltvsql   = "select * from [dbo].[LoanTypeView]";
                            $ltvstmt  = sqlsrv_query($conn, $ltvsql);
                            while($ltvrow   = sqlsrv_fetch_array( $ltvstmt, SQLSRV_FETCH_NUMERIC)){
                                echo "<option value=";
                                echo $ltvrow[0];
                                if ($ltvrow[0]==$_SESSION['selectdata']) {
                                    echo " selected='selected' ";
                                }
                                echo ">&nbsp;";
                                echo $ltvrow[0];
                                echo "<br>&nbsp;";
                                echo $ltvrow[1];
                                echo "</option>";
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label" style="text-align: left;">Kode Loan App Number</label>
                    <div class="col-sm-8">
                        <input type="text" name="kod" class="form-control" id="kod" placeholder="" value="<?php echo $xc; ?>"  readonly required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label" style="text-align: left;">Maximum Pinjaman</label>
                    <div class="col-sm-8">
                        <input type="text" id="max" class="form-control" placeholder="" value="<?php echo number_format($max); ?>" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label" style="text-align: left;">Besar Pinjaman</label>
                    <div class="col-sm-8">
                        <input type="text" name="amount" id="amount" class="form-control" placeholder="" onblur="load_page();" value="<?php echo $amount; ?>" required>
                    </div>
                </div>
                <div class="box-footer">
                    <?php if($loantype != null and $amount != null and $xc != null){ ?>
                        <a href="#"><button type="button" class="btn btn-default btn-simulasi">Lihat Simulasi</button></a>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Data Penjamin Loan</h3>
                <div class="box-body">
                <?php if($mgsrow == null){ ?>
                    <div class="alert alert-info alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <b><i class="icon fa fa-info"></i> Info!</b>
                        Setting belum ditambahkan.
                    </div>
                <?php } else if($mgsrow[2] == 0){ ?>
                    <div class="alert alert-info alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <b><i class="icon fa fa-info"></i> Info!</b>
                        Tidak diperlukan penjamin.
                    </div>
                <?php } else { ?>
                    <div class="form-group">
                        <?php if($memberid != null){ ?>
                            <label for="csoaregsavmember" class="col-sm-4 control-label" style="text-align: left;">Member ID</label>
                            <div class="col-sm-4">
                                <input type="text" id="member" class="form-control" placeholder="" value="">
                            </div>
                            <button type="button" class="btn btn-flat btn-primary btn-member">Add</button>
                        <?php } ?>
                    </div>

                    <?php if($z == null){ ?>
                        <table id="membertable" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Member ID</th>
                                <th>Nama</th>
                                <th>Alamat</th>
                                <th>Telepon</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    <?php } else { ?>
                        <table class="table table-bordered">
                            <tr>
                                <th>Member ID</th>
                                <th>Nama</th>
                                <th>Alamat</th>
                                <th>Status</th>
                            </tr>
                            <?php
                            $l = "select * from [dbo].[LoanPenjaminView] where LoanAppNum='$xc' and Status == 2";
                            $t = sqlsrv_query($conn, $l);
                            while($f = sqlsrv_fetch_array($t, SQLSRV_FETCH_NUMERIC)){
                                ?>
                                <tr>
                                    <td><?php echo $f[1]; ?></td>
                                    <td><?php echo $f[2]; ?></td>
                                    <td><?php echo $f[9]; ?></td>
                                    <td>
                                        <?php if($f[7] == 0){ ?>
                                            <b>Menunggu Persetujuan</b>
                                        <?php } else if($f[7] == 1){ ?>
                                            <b>Disetujui</b>
                                        <?php } else if($f[7] == 2){ ?>
                                            <b>Ditolak</b>
                                        <?php } ?>
                                    </td>
                                </tr>
                            <?php } ?>
                        </table>
                        <table id="membertable" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Member ID</th>
                                <th>Nama</th>
                                <th>Alamat</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $l = "select * from [dbo].[LoanPenjaminView] where LoanAppNum='$xc' and Status != 2";
                            $t = sqlsrv_query($conn, $l);
                            while($f = sqlsrv_fetch_array($t, SQLSRV_FETCH_NUMERIC)){
                            ?>
                            <tr>
                                <td><?php echo $f[1]; ?></td>
                                <td><?php echo $f[2]; ?></td>
                                <td><?php echo $f[9]; ?></td>
                                <td>
                                    <?php if($f[7] == 0){ ?>
                                        <b>Menunggu Persetujuan</b>
                                    <?php } else if($f[7] == 1){ ?>
                                        <b>Disetujui</b>
                                    <?php } else if($f[7] == 2){ ?>
                                        <b>Ditolak</b>
                                    <?php } ?>
                                </td>
                                <td>
                                    <?php if($f[10] == 0){ ?>
                                        <a href="proccsoaloanapp.php?resend=<?= $f[3]; ?>&kod=<?= $f[0] ?>&member=<?= $memberid ?>&produk=<?= $loantype ?>"><button class="btn btn-sm btn-primary" type="button"><i class="fa fa-envelope-o"></i> Resend Email</button></a>
                                    <?php } ?>
                                </td>
                            </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    <?php } ?>
                <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Upload Dokumen</h3>
                <div class="box-body">
                <?php if($mgsrow == null){ ?>
                    <div class="alert alert-info alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <b><i class="icon fa fa-info"></i> Info!</b>
                        Setting belum ditambahkan
                    </div>
                <?php } else if($mgsrow[4] == 0){ ?>
                    <div class="alert alert-info alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <b><i class="icon fa fa-info"></i> Info!</b>
                        Tidak diperlukan dokumen.
                    </div>
                <?php } else { ?>
                    <div class="form-group">
                        <label for="csoaregsavmember" class="col-sm-4 control-label" style="text-align: left;">Dokumen</label>
                        <div class="col-sm-4">
                            <select class="form-control" id="doc">
                                <option>--- Select One ---</option>
                                <?php
                                $l   = "select * from [dbo].[SetingDocGeneralSeting] where KID='$_SESSION[KID]' and LoanType='$loantype'";
                                $lt  = sqlsrv_query($conn, $l);
                                while($ltv   = sqlsrv_fetch_array( $lt, SQLSRV_FETCH_NUMERIC)){
                                    echo "<option value='$ltv[1]'>$ltv[1]</option>";
                                }
                                ?>
                            </select>
                        </div>
                        <button type="button" class="btn btn-flat btn-primary btn-doc">Add</button>
                    </div>
                    <table class="table table-bordered table-hover table-striped" id="tabledoc">
                        <thead>
                        <tr>
                            <th>Nama</th>
                            <th>File</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $sql   = "select * from [dbo].[MemberListDocUpload] where MemberID='$memberid'";
                        $st  = sqlsrv_query($conn, $sql);
                        while($row = sqlsrv_fetch_array( $st, SQLSRV_FETCH_NUMERIC)){ ?>
                        <tr>
                            <td><?= $row[1]; ?></td>
                            <td><a href="<?= $row[2]; ?>" target="_blank" title="Klik untuk melihat dokumen">Klik untuk melihat dokumen</a></td>
                            <td>
                                <a href="proccsoaloanapp.php?memberid=<?php echo $memberid ?>&loantype=<?php echo $loantype ?>&amount=<?php echo $amount ?>&delete=<?=$row[1]?>" class="btn btn-default btn-flat btn-sm text-red" title="delete" onClick="return confirm('Hapus data ini ?')"><i class="fa fa-trash-o"></i></a>
                            </td>
                        </tr>
                        <?php } ?>
                        </tbody>
                        <tfoot>

                        </tfoot>
                    </table>
                <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="box box-primary hide" id="simulasi">
            <div class="box-header">
                <h3 class="box-title">Simulasi Pinjaman</h3>
                <div class="box-body">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>Simulasi No</th>
                            <th>Saldo</th>
                            <th>Pokok</th>
                            <th>Bunga</th>
                            <th>Total</th>
                        </tr>
                        <?php
                        $ls   = "select * from [dbo].[SimulasiLoan] where KodeTransaksi='$xc' order by Nomor ASC";
                        $lsq = sqlsrv_query($conn, $ls);
                        sqlsrv_execute($lsq);
                        while($row = sqlsrv_fetch_array($lsq, SQLSRV_FETCH_NUMERIC)){
                        ?>
                        <tr>
                            <td><?= $row[2]; ?></td>
                            <td><?= number_format($row[3]); ?></td>
                            <td><?= number_format($row[4]); ?></td>
                            <td><?= number_format($row[5]); ?></td>
                            <td><?= number_format($row[6]); ?></td>
                        </tr>
                        <?php } ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="box-footer">
            <?php if($mgsrow == null){ ?>
                <div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <b><i class="icon fa fa-warning"></i> Warning!</b>
                    Tidak dapat menyimpan form.
                </div>
            <?php } else { ?>
                <button type="submit" class="btn btn-flat btn-primary btn-ajukan">Ajukan</button>
            <?php } ?>
        </div>
    </div>
    </form>
</div>

<?php require('content-footer.php');?>

<?php require('footer.php');?>