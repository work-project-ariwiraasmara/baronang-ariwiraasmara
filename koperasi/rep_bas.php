<?php require('header.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>




<div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">Daftar Anggota</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Member ID</th>
                            <th><?php echo lang('Nama'); ?></th>
                            <th><?php echo lang('Alamat'); ?></th>
                            <th><?php echo lang('No. Telp'); ?></th>
                            <th>Email</th>
                            <th>KTP</th>
                            <th>NIP</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    //count
                    if(isset($_GET['acc'])){
                        $sql = "select count(*) from dbo.MemberListView where KID='$_SESSION[KID]' AND MemberID='$_GET[acc]'";
                        }else{
                            $jmlulsql   = "select count(*) from dbo.MemberListView where KID='$_SESSION[KID] '";
                        }
                        $jmlulstmt  = sqlsrv_query($conn,$sql, $jmlulsql);
                        $jmlulrow   = sqlsrv_fetch_array($jmlulstmt, SQLSRV_FETCH_NUMERIC);
                        //var_dump($jmlulrow).die;

                        //pagging
                        $perpages   = 10;
                        $halaman    = $_GET['page'];
                        if(empty($halaman)){
                            $posisi  = 0;
                            $batas   = $perpages;
                            $halaman = 1;
                        }else{
                            $posisi  = (($perpages * $halaman) - 10) + 1;
                            $batas   = ($perpages * $halaman);
                        }


                        $acc = $_GET['acc'];
                        $ulsql = "SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY MemberID asc) as row FROM [dbo].[MemberListView]) a WHERE KID='$_SESSION[KID]' and row between '$posisi' and '$batas' and a.MemberID='$acc'";              
                
                        $ulstmt = sqlsrv_query($conn, $ulsql);
                        while($ulrow = sqlsrv_fetch_array( $ulstmt, SQLSRV_FETCH_NUMERIC)){
                        ?>
                            <tr>
                                <td><?php echo $ulrow[11]; ?></td>
                                <td align="right"><a href="rep_bas.php?acc=<?php echo $ulrow[1]; ?>"><?php echo $ulrow[1]; ?></td>
                                <td><?php echo $ulrow[2]; ?></td>
                                <td><?php echo $ulrow[3]; ?></td>
                                <td align="right"><?php echo $ulrow[4]; ?></td>
                                <td><?php echo $ulrow[5]; ?></td>
                                <td align="right"><?php echo $ulrow[6]; ?></td>
                                <td align="right"><?php echo $ulrow[7]; ?></td>
                            </tr>
                        <?php
                        }
                        $jmlhalaman = ceil($jmlulrow[0]/$perpages);
                        ?>
                        </tbody>
                        </table>

            </div>
            <!-- /.box-header -->
            <div class="box-body">
              Saldo Simpanan Pokok

              <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>No</th>
                    <th>Member ID</th>
                    <th><?php echo lang('Nama'); ?></th>
                    <th><?php echo lang('Simpanan pokok'); ?></th>
                    <th><?php echo lang('Simpanan Wajib'); ?></th>
                    <th><?php echo lang('Simpanan Sukarela'); ?></th>
                </tr>
                </thead>
                <tbody>
                <?php
                //count

                if(isset($_GET['acc'])){
                    $sql   = "select count(*) from dbo.MemberListView where MemberID='$_GET[acc]'";
                   
                }else{
                    $jmlulsql   = "select count(*) from dbo.MemberListView";

                }
                
                $jmlulstmt  = sqlsrv_query($conn, $sql, $jmlulsql);
                $jmlulrow   = sqlsrv_fetch_array($jmlulstmt, SQLSRV_FETCH_NUMERIC);

                //pagging
                $perpages   = 10;
                $halaman    = $_GET['page'];
                if(empty($halaman)){
                    $posisi  = 0;
                    $batas   = $perpages;
                    $halaman = 1;
                }
                else{
                    $posisi  = (($perpages * $halaman) - 10) + 1;
                    $batas   = ($perpages * $halaman);
                }

                $acc = $_GET['acc'];
                $ulsql = "SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY MemberID asc) as row FROM [dbo].[MemberListView]) a WHERE KID='$_SESSION[KID]' and row between '$posisi' and '$batas'and a.MemberID='$acc'";
                $ulstmt = sqlsrv_query($conn, $ulsql);
                while($ulrow = sqlsrv_fetch_array( $ulstmt, SQLSRV_FETCH_NUMERIC)){
                    $sql1   = "select * from dbo.BasicSavingBalance where KodeBasicSavingType='".$_SESSION['KID']."101' and MemberID = '$ulrow[1]'";
                    $stmt1  = sqlsrv_query($conn, $sql1);
                    $row1  = sqlsrv_fetch_array($stmt1, SQLSRV_FETCH_NUMERIC);                    

                    $sql2   = "select * from dbo.BasicSavingBalance where KodeBasicSavingType='".$_SESSION['KID']."102' and MemberID = '$ulrow[1]'";
                    $stmt2  = sqlsrv_query($conn, $sql2);
                    $row2  = sqlsrv_fetch_array($stmt2, SQLSRV_FETCH_NUMERIC);


                    $sql   = "select * from dbo.BasicSavingBalance where KodeBasicSavingType='".$_SESSION['KID']."103' and MemberID = '$ulrow[1]'";
                    $stmt  = sqlsrv_query($conn, $sql);
                    $row   = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_NUMERIC);
                    ?>
                    <tr>
                        <td><?php echo $ulrow[11]; ?></td>
                        <td align="right"><a href="rep_bas.php?acc=<?php echo $ulrow[1]; ?>"><?php echo $ulrow[1]; ?></td>
                        <td><?php echo $ulrow[2]; ?></td>
                        <td align="right"><?php echo number_format($row1[5]); ?></td>
                        <td align="right"><?php echo number_format($row2[5]); ?></td>
                        <td align="right"><?php echo number_format($row[5]); ?></td>
                    </tr>
                <?php
                }
                $jmlhalaman = ceil($jmlulrow[0]/$perpages);
                ?>
                </tbody>
            </table>


            </div>

            <!-- /.box-body -->

            <div class="box-body">
              Saldo Tabungan

              <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>No</th>
                    <th><?php echo lang('No. Akun'); ?></th>
                    <th><?php echo lang('Nama'); ?></th>
                    <th><?php echo lang('Tanggal Buka'); ?></th>
                    <th><?php echo lang('Tanggal Tutup'); ?></th>
                    <th><?php echo lang('Produk'); ?></th>
                    <th><?php echo lang('Saldo'); ?></th>
                    <th><?php echo lang('Status'); ?></th>
                </tr>
                </thead>
                <tbody>
                <?php
                //count

                if(isset($_GET['acc'])){
                    $sql   = "select count(*) from dbo.RegSavingAccView where KID='$_SESSION[KID]' and MemberID='$_GET[acc]'";
                   
                }else{
                    $jmlulsql   = "select count(*) from dbo.RegSavingAccView where KID='$_SESSION[KID]'";

                }
                
                $jmlulstmt  = sqlsrv_query($conn,$sql, $jmlulsql);
                $jmlulrow   = sqlsrv_fetch_array($jmlulstmt, SQLSRV_FETCH_NUMERIC);

                //pagging
                $perpages   = 10;
                $halaman    = $_GET['page'];
                if(empty($halaman)){
                    $posisi  = 0;
                    $batas   = $perpages;
                    $halaman = 1;
                }
                else{
                    $posisi  = (($perpages * $halaman) - 10) + 1;
                    $batas   = ($perpages * $halaman);
                }
                $acc = $_GET['acc'];
                $ulsql = "SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY MemberID asc) as row FROM [dbo].[RegSavingAccView]) a WHERE KID='$_SESSION[KID]' and row between '$posisi' and '$batas' and a.MemberID='$acc'";
                $ulstmt = sqlsrv_query($conn, $ulsql);
                while($ulrow = sqlsrv_fetch_array( $ulstmt, SQLSRV_FETCH_NUMERIC)){
                    $open = '';
                    $close = '';
                    $sql   = "select * from dbo.RegularSavingReportList where AccNo = '$ulrow[2]'";
                    $stmt  = sqlsrv_query($conn, $sql);
                    $row   = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_NUMERIC);
                    if($row != null){
                        $open = $row[1]->format('Y-m-d H:i:s');
                        if($row[2] != ''){
                            $close = $row[2]->format('Y-m-d H:i:s');
                        }
                    }
                ?>
                
                    <tr>
                        <td><?php echo $ulrow[12]; ?></td>
                        <td align="right"><a href="rep_reg.php?acc=<?php echo $ulrow[2]; ?>&st=2"><?php echo $ulrow[2]; ?></td>
                        <td><?php echo $ulrow[7]; ?></td>
                        <td><?php echo $open; ?></td>
                        <td><?php echo $close; ?></td>
                        <td><?php echo $ulrow[4]; ?></td>
                        <td align="right"><?php echo number_format($ulrow[5]); ?></td>
                        <td><?php echo $row[5]; ?></td>
                    </tr>
                <?php
                }
                $jmlhalaman = ceil($jmlulrow[0]/$perpages);
                ?>
                </tbody>
            </table>


            </div>

            <!-- /.box-body -->

            <div class="box-body">
              Saldo Deposito

              <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>No</th>
                    <th><?php echo lang('Nomor Akun'); ?></th>
                    <th><?php echo lang('Nama'); ?></th>
                    <th><?php echo lang('Tanggal Buka'); ?></th>
                    <th><?php echo lang('Tanggal Tutup'); ?></th>
                    <th><?php echo lang('Produk'); ?></th>
                    <th><?php echo lang('Suku Bunga%'); ?></th>
                    <th><?php echo lang('Saldo'); ?></th>
                    <th><?php echo lang('Satus'); ?></th>
                </tr>
                </thead>
                <tbody>
                <?php
                //count
                if(isset($_GET['acc'])){
                    $sql   = "select count(*) from dbo.TimeDepositReport where KID='$_SESSION[KID]' and MemberID='$_GET[acc]'";
                   
                }else{
                    $jmlulsql   = "select count(*) from dbo.TimeDepositReport";

                }
                $jmlulstmt  = sqlsrv_query($conn,$sql, $jmlulsql);
                $jmlulrow   = sqlsrv_fetch_array($jmlulstmt, SQLSRV_FETCH_NUMERIC);

                //pagging
                $perpages   = 10;
                $halaman    = $_GET['page'];
                if(empty($halaman)){
                    $posisi  = 0;
                    $batas   = $perpages;
                    $halaman = 1;
                }
                else{
                    $posisi  = (($perpages * $halaman) - 10) + 1;
                    $batas   = ($perpages * $halaman);
                }

                $acc = $_GET['acc'];
                $ulsql = "SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY MemberID asc) as row FROM [dbo].[TimeDepositReport]) a WHERE row between '$posisi' and '$batas' and a.MemberID='$acc'";
                $ulstmt = sqlsrv_query($conn, $ulsql);
                while($ulrow = sqlsrv_fetch_array( $ulstmt, SQLSRV_FETCH_NUMERIC)){
                    $open = $ulrow[7]->format('Y-m-d H:i:s');
                    $close = '';
                    if($ulrow[9] != ''){
                        $close = $ulrow[9]->format('Y-m-d H:i:s');
                    }
                ?>
                    <tr>
                        <td><?php echo $ulrow[11]; ?></td>
                        <td align="right"><a href="rep_reg.php?acc=<?php echo $ulrow[2]; ?>&st=3"><?php echo $ulrow[2]; ?></td>
                        <td><?php echo $ulrow[1]; ?></td>
                        <td><?php echo $open; ?></td>
                        <td><?php echo $close; ?></td>
                        <td><?php echo $ulrow[4]; ?></td>
                        <td align="right"><?php echo $ulrow[5]; ?>%</td>
                        <td align="right"><?php echo $ulrow[6]; ?></td>
                        <td><?php echo $ulrow[10]; ?></td>
                    </tr>
                <?php
                }
                $jmlhalaman = ceil($jmlulrow[0]/$perpages);
                ?>
                </tbody>
            </table>

            </div>

            <!-- /.box-body -->

            <div class="box-body">
              Daftar Pinjaman

              <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>No</th>
                    <th><?php echo lang('No. Pinjaman'); ?></th>
                    <th><?php echo lang('Jumlah'); ?></th>
                    <th><?php echo lang('Tanggal Buka'); ?></th>
                    <th><?php echo lang('Tanggal tutup'); ?></th>
                    <th><?php echo lang('Produk'); ?></th>
                    <th><?php echo lang('Saldo'); ?></th>
                    <th><?php echo lang('Status'); ?></th>
                </tr>
                </thead>
                <tbody>
                <?php
                //count
                if(isset($_GET['acc'])){  
                $sql   = "select count(*) from dbo.LoanReportBalance where MemberID='$_GET[acc]'";
                }else {
                    $jmlulsql   = "select count(*) from dbo.LoanReportBalance";
                }
                $jmlulstmt  = sqlsrv_query($conn,$sql, $jmlulsql);
                $jmlulrow   = sqlsrv_fetch_array($jmlulstmt, SQLSRV_FETCH_NUMERIC);

                //pagging
                $perpages   = 10;
                $halaman    = $_GET['page'];
                if(empty($halaman)){
                    $posisi  = 0;
                    $batas   = $perpages;
                    $halaman = 1;
                }
                else{
                    $posisi  = (($perpages * $halaman) - 10) + 1;
                    $batas   = ($perpages * $halaman);
                }

                $acc = $_GET['acc'];
                $ulsql = "SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY LoanNumber asc) as row FROM [dbo].[LoanReportBalance]) a WHERE row between '$posisi' and '$batas' and a.MemberID='$acc'";
                $ulstmt = sqlsrv_query($conn, $ulsql);
                while($ulrow = sqlsrv_fetch_array( $ulstmt, SQLSRV_FETCH_NUMERIC)){
                    $open = $ulrow[2]->format('Y-m-d H:i:s');
                    $close = '';
                    if($ulrow[3] != ''){
                        $close = $ulrow[3];
                    }
                ?>
                    <tr>
                        <td><?php echo $ulrow[7]; ?></td>
                        <td align="right"><a href="rep_reg.php?acc=<?php echo $ulrow[0]; ?>&st=4"><?php echo $ulrow[0]; ?></td>
                        <td align="right"><?php echo $ulrow[1]; ?></td>
                        <td><?php echo $open; ?></td>
                        <td><?php echo $close; ?></td>
                        <td><?php echo $ulrow[4]; ?></td>
                        <td align="right"><?php echo $ulrow[5]; ?></td>
                        <td><?php echo $ulrow[6]; ?></td>
                    </tr>
                <?php
                }
                $jmlhalaman = ceil($jmlulrow[0]/$perpages);
                ?>
                </tbody>
            </table>



            </div>

            <!-- /.box-body -->

            <div class="box-body">
              Tagihan Bulanan

              <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>No</th>
                    <th>Member ID</th>
                    <th><?php echo lang('Nama'); ?></th>
                    <th><?php echo lang('Hutang Simpanan Pokok'); ?></th>
                    <th><?php echo lang('Hutang Simpanan Wajib'); ?></th>
                    <th><?php echo lang('Hutang Simpanan Sukarela'); ?></th>
                    <th><?php echo lang('Pokok Pinjaman'); ?></th>
                    <th><?php echo lang('Bunga Pinjaman'); ?></th>
                    <th><?php echo lang('Belanja Toko'); ?></th>
                    <th><?php echo lang('Toko'); ?></th>
                </tr>
                </thead>
                <tbody>
                <?php
                //count
                if(isset($_GET['acc'])){
                    $sql   = "select count(*) from dbo.TagihanBulanan where MemberID='$_GET[acc]'";    
                }else{
                    $jmlulsql   = "select count(*) from dbo.TagihanBulanan ";    
                } 
                
                $jmlulstmt  = sqlsrv_query($conn,$sql, $jmlulsql);
                $jmlulrow   = sqlsrv_fetch_array($jmlulstmt, SQLSRV_FETCH_NUMERIC);

                //pagging
                $perpages   = 10;
                $halaman    = $_GET['page'];
                if(empty($halaman)){
                    $posisi  = 0;
                    $batas   = $perpages;
                    $halaman = 1;
                }
                else{
                    $posisi  = (($perpages * $halaman) - 10) + 1;
                    $batas   = ($perpages * $halaman);
                }
                $acc = $_GET['acc'];
                $ulsql = "SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY MemberID asc) as row FROM [dbo].[TagihanBulanan]) a WHERE row between '$posisi' and '$batas' and a.MemberID='$acc'";
                $ulstmt = sqlsrv_query($conn, $ulsql);
                while($ulrow = sqlsrv_fetch_array( $ulstmt, SQLSRV_FETCH_NUMERIC)){
                ?>
                    <tr>
                        <td align="right"><?php echo $ulrow[12]; ?></td>
                        <td align="right"><?php echo $ulrow[0]; ?></td>
                        <td><?php echo $ulrow[1]; ?></td>
                        <td align="right"><?php echo number_format($ulrow[3]); ?></td>
                        <td align="right"><?php echo number_format($ulrow[5]); ?></td>
                        <td align="right"><?php echo number_format($ulrow[7]); ?></td>
                        <td align="right"><?php echo number_format($ulrow[8]); ?></td>
                        <td align="right"><?php echo number_format($ulrow[9]); ?></td>
                        <td align="right"><?php echo number_format($ulrow[10]); ?></td>
                        <td align="right"><?php echo number_format($ulrow[11]); ?></td>
                    </tr>
                <?php
                }
                $jmlhalaman = ceil($jmlulrow[0]/$perpages);
                ?>
                </tbody>
            </table>


            </div>

            <!-- /.box-body -->
</div>

<div class="table-responsive">

            <div class="box-footer clearfix pull-right">
                <div style="text-align: center;"><label>Showing <?=$halaman?> to <?=$jmlhalaman?> of <?=$jmlulrow[0]?> entries <?=$posisi." - ".$batas?></label></div>
                    <?php
                    $reload = "rep_bas.php?";
                    $page = intval($_GET["page"]);
                    $adjacents = isset($_GET['adjacents']) ? intval($_GET['adjacents']) : 2; // khusus style pagination 2 dan 3
                    if( $page == 0 ) $page = 1;

                    $tpages = ($jmlhalaman) ? ceil($jmlhalaman) : 0; // total pages, last page number
                    echo "<div>".paginate($reload, $page, $tpages, $adjacents)."</div>";
                    /*
                    <ul class="pagination pagination-sm no-margin pull-right">
                    <li class="paginate_button"><a href="rep_bas.php">&laquo;</a></li>
                    for($ul = 1; $ul <= $jmlhalaman; $ul++){
                        if($ul != $halaman){ $ulpageactive = ""; $ulpagedisabled = "";}else{$ulpageactive = "active"; $ulpagedisabled = "disabled";}
                        echo '<li class="paginate_button '.$ulpageactive.'"><a href="rep_bas.php?page='.$ul.'" '.$ulpagedisabled.'>'.$ul.'</a></li>';
                    }
                    <li class="paginate_button"><a href="rep_bas.php?page=<?=$jmlhalaman?>">&raquo;</a></li>
                    </ul>
                    */
                    ?>
                </div>
            </div>
</div>

<?php
function paginate($reload, $page, $tpages, $adjacents) {

    $prevlabel = "&lsaquo; Prev";
    $nextlabel = "Next &rsaquo;";

    $out = "<nav><ul class=\"pagination\">\n";

    // previous
    if($page==1) {
        $out.= "<li class=\"disabled\"><a href=\"#\">" . $prevlabel . "</a></li>\n";
    }
    elseif($page==2) {
        $out.= "<li><a href=\"" . $reload . "\">" . $prevlabel . "</a></li>\n";
    }
    else {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" . ($page-1) . "\">" . $prevlabel . "</a></li>\n";
    }

    // first
    if($page>($adjacents+1)) {
        $out.= "<li><a href=\"" . $reload . "\">1</a></li>\n";
    }

    // interval
    if($page>($adjacents+2)) {
        $out.= "<li class=\"disabled\"><a href=\"#\">...</a></li>\n";
    }

    // pages
    $pmin = ($page>$adjacents) ? ($page-$adjacents) : 1;
    $pmax = ($page<($tpages-$adjacents)) ? ($page+$adjacents) : $tpages;
    for($i=$pmin; $i<=$pmax; $i++) {
        if($i==$page) {
            //$out.= "<li class=\"active\"><a href=\"#\">" . $i . "</a></li>\n";
            $out.= "<li class=\"active\"><span class=\"current\">Page " . $page . " of " . $tpages ."</span></li>";
        }
        elseif($i==1) {
            $out.= "<li><a href=\"" . $reload . "\">" . $i . "</a></li>\n";
        }
        else {
            $out.= "<li><a href=\"" . $reload . "&amp;page=" . $i . "\">" . $i . "</a></li>\n";
        }
    }

    // interval
    if($page<($tpages-$adjacents-1)) {
        $out.= "<li class=\"disabled\"><a href=\"#\">...</a></li>\n";
    }

    // last
    if($page<($tpages-$adjacents)) {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" . $tpages . "\">" . $tpages . "</a></li>\n";
    }

    // next
    if($page<$tpages) {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" . ($page+1) . "\">" . $nextlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabled\"><a href=\"#\">" . $nextlabel . "</a></li>\n";
    }

    $out.= "</ul></nav>";

    return $out;
}
?>

<?php require('content-footer.php');?>

<?php require('footer.php');?>
