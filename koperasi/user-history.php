<?php require('header.php');?>      
<?php require('sidebar-left-user.php');?>

<section class="content">

<div class="row">
    <!-- Content Header (Page header) -->


    <!-- Main content -->
    <section class="content">
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
		<div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">History</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal">
              <div class="box-body">
                <div class="form-group">
				<div class="col-sm-3"></div>
				  <label for="accno" class="col-sm-2 control-label" style="text-align: left;">Account No</label>
				  <div class="col-sm-3">
                  <div class="btn-group">
                  <button type="button" class="btn btn-default"> Select</button>
                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                  </ul>
                </div>
                </div>
                </div>
                <div class="form-group">
				<div class="col-sm-3"></div>

                <label for="Mutasibulanan" class="col-sm-2 control-label" style="text-align: left;">Mutasi Bulanan</label>
				<div class="col-sm-3">
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="reservation">
                </div>
				</div>
                <!-- /.input group -->
              </div>

				<div class="form-group">
				<div class="col-sm-3"></div>
				  <label for="Mutasibulanan" class="col-sm-2 control-label" style="text-align: left;">Mutasi Bulanan</label>
				  <div class="col-sm-3">
                  <div class="btn-group">
                  <button type="button" class="btn btn-default"> Select</button>
                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                  </ul>
                </div>
                </div>
                </div>
				</div>				
				<div class="box-footer">
				<button type="submit" class="btn btn-info pull-right">Lihat Mutasi</button>
				<button type="submit" class="btn btn-standard">Download Mutasi</button>
				</div>
              </div>
              <!-- /.box-body -->
            </form>
			
          </div>
        </section>
        <!-- /.Left col -->
		<section class="col-lg-12 connectedSortable">
		<div class="box">
            <div class="box-header">
              <h3 class="box-title">Mutasi</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Tgl</th>
                  <th>Keterangan</th>
                  <th>Mutasi</th>
                  <th>Saldo</th>
                  <th>lain lain</th>
				  <th>lain lain</th>

                </tr>
                </thead>
                <tbody>
                <tr>
                  <td> 27/09</td>
                  <td> ini itu</td>
                  <td> 200,000</td>
                  <td> 1,000,000</td>
                  <td> </td>
				  <td> </td>	
                </tr>
                <tr>
                  <td> 27/09</td>
                  <td> ini itu</td>
                  <td> 200,000</td>
                  <td> 1,000,000</td>
                  <td> </td>
				  <td> </td>
                </tr>
                <tr>
                  <td> 27/09</td>
                  <td> ini itu</td>
                  <td> 200,000</td>
                  <td> 1,000,000</td>
                  <td> </td>
				  <td> </td>
                </tr>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
		</section>
      </div>

</section>

<?php require('content-footer.php');?>
<?php require('footer.php');?>