<?php require('header_identity_cs.php');?>

<?php include "connectuser.php"; ?>

<?php include "connectinti.php"; ?>

<?php

$uid = '';
if(isset($_SESSION['RequestID'])){
    $uid = $_SESSION['UID'];
}

if(isset($_GET['new'])){
    unset($_SESSION['RequestID']);
    unset($_SESSION['UID']);
    unset($_SESSION['RequestLink']);
    unset($_SESSION['RequestExpired']);
    unset($_SESSION['RequestMember']);
    unset($_SESSION['RequestMemberName']);

    $_SESSION['error-type'] = 'info';
    $_SESSION['error-message'] = 'Session closed';
    $_SESSION['error-time'] = time() + 5;
}
?>

<div class="body-idcs2">
    <div class="row">
        <div style="text-align: center; margin-top: 35px;">
            <h2><?php echo lang('Pelayanan Pelanggan'); ?></h2>
        </div>

        <div style="margin-top: 70px; margin-left: 25%; margin-right: 25%;">
            <?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
                <div class="alert alert-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <i class="icon fa fa-warning ?>"></i> <?php echo ucfirst($_SESSION['error-type']); ?>
                    <?php echo $_SESSION['error-message']; ?>
                </div>
            <?php } ?>
            <form id="siden" action="proc_identitycs.php" method="post">
                <input type="password" class="form-control" name="uid" id="result-code" value="<?php echo $uid; ?>" placeholder="Nomor ID Baronang App">
                <div class="pull-left" style="margin-top: 10px">
                    <button type="button" id="back" class="btn btn-info" onclick="window.location.href = 'dashboard.php'">Back</button>
                    <button type="button" id="ocam" class="btn btn-info"><i class="fa fa-camera"></i> Scan QR Code</button>
                </div>
                <div class="pull-right" style="margin-top: 10px">
                    <a href="identity_cs.php?new"><button type="button" class="btn btn-warning rounded"><?php echo lang('Sesi Baru'); ?></button></a>
                    <button type="submit" class="btn btn-success rounded" id="btn-go">Go!</button>
                </div>
            </form>
        </div>
    </div>

    <div id="menu"> </div>

    <div style="padding: 10px;">
        <div class="box box-solid hide" id="form-camera" style="margin-top: 50px;">
            <div class="box-header">
                <h3 class="box-title">Scan QR Code/Barcode</h3>
            </div>
            <div class="box-body">
                <div class="form-group"><div style="text-align: center;">
                    <div class="col-sm-2">
                        <div class="pull-right"><b>Pilih Kamera</b></div>
                    </div>
                    <div class="col-sm-7">
                        <select class="form-control" id="camera-select"></select>
                    </div>
                    <div class="col-sm-3">
                        <button title="Start Scan QR Barcode" class="btn btn-success" id="play" type="button" data-toggle="tooltip">Start</button>
                        <button title="Pause" class="btn btn-warning" id="pause" type="button" data-toggle="tooltip">Pause</button>
                        <button title="Stop streams" class="btn btn-danger" id="stop" type="button" data-toggle="tooltip">Stop</button>
                    </div>
                </div></div>
                <br>
                <br>
                <br>
                <div class="form-group">
                    <div class="col-sm-6">
                        <div class="well" style="position: relative;display: inline-block;">
                            <canvas id="webcodecam-canvas"></canvas>
                            <div class="scanner-laser laser-rightBottom" style="opacity: 0.5;"></div>
                            <div class="scanner-laser laser-rightTop" style="opacity: 0.5;"></div>
                            <div class="scanner-laser laser-leftBottom" style="opacity: 0.5;"></div>
                            <div class="scanner-laser laser-leftTop" style="opacity: 0.5;"></div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="thumbnail" id="result">
                            <img width="320" height="240" id="scanned-img" src="">
                            <div class="caption">
                                <p class="text-center" id="scanned-QR"></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $('#ocam').click(function(){
            $('#result-code').val();
            $('#form-camera').removeClass('hide');
        });

        var uid = $('#result-code').val();
        $.ajax({
            url : "identity_menu.php",
            type : 'POST',
            data: { uid: uid},
            success : function(data) {
                $("#menu").html(data);
            },
            error : function(){
                alert('Try again.');
                return false;
            }
        });

        $('#result-code').keyup(function(){
            var uid = $('#result-code').val();
            if(uid.length == 16){
                $('#siden').submit();
            }
        });
    </script>
</div>

<?php require('footer_identity_cs.php');?>