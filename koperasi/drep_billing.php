<?php
session_start();
error_reporting(0);

include('lib/phpExcel/Classes/PHPExcel.php');
include "connect.php";


    $filePath = "uploads/excel/";

    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getProperties()->setCreator("baronang");
    $objPHPExcel->getProperties()->setTitle("Tagihan Bulanan");

// set autowidth
    for($col = 'A'; $col !== 'Z'; $col++) {
        $objPHPExcel->getActiveSheet()
            ->getColumnDimension($col)
            ->setAutoSize(true);
    }

    $listCombo = array();

    //sheet 2
    $objWorkSheet = $objPHPExcel->createSheet(0);
// baris judul
    $objWorkSheet->SetCellValue('A1', 'No');
    $objWorkSheet->SetCellValue('B1', 'Memeber ID');
    $objWorkSheet->SetCellValue('C1', 'Nama');
    $objWorkSheet->SetCellValue('D1', 'Hutang Simpanan Pokok');
    $objWorkSheet->SetCellValue('E1', 'Hutang Simpanan Wajib');
    $objWorkSheet->SetCellValue('F1', 'Hutang Simpanan Sukarela');
    $objWorkSheet->SetCellValue('G1', 'Pokok Pinjaman');
    $objWorkSheet->SetCellValue('H1', 'Bunga Pinjaman');
    $objWorkSheet->SetCellValue('I1', 'Belanja Toko');
    $objWorkSheet->SetCellValue('J1', 'Toko');





    $no = 1;
    $row = 2;
    $x = "select * from dbo.TagihanBulanan ORDER BY MemberID Asc";
    
    $y = sqlsrv_query($conn, $x);
    while($z = sqlsrv_fetch_array( $y, SQLSRV_FETCH_NUMERIC)){
        //var_dump($z).die;
         //push untuk drop down list
        array_push($listCombo, $z[1]);


        $objWorkSheet->SetCellValue("A".$row, $no);
        $objWorkSheet->SetCellValueExplicit("B".$row, $z[0], PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet->SetCellValue("C".$row, $z[1]);
        $objWorkSheet->SetCellValueExplicit("D".$row, number_format($z[3]), PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet->SetCellValueExplicit("E".$row, number_format($z[5]), PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet->SetCellValueExplicit("F".$row, number_format($z[7]), PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet->SetCellValueExplicit("G".$row, number_format($z[8]), PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet->SetCellValueExplicit("H".$row, number_format($z[9]), PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet->SetCellValueExplicit("I".$row, number_format($z[10]), PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet->SetCellValueExplicit("J".$row, number_format($z[11]), PHPExcel_Cell_DataType::TYPE_STRING);


        
        $no++;
        $row++;
    }

    $objWorkSheet->setTitle('Tagihan Bulanan');

    $fileName = 'Tagihanbulanan'.'_'.strtotime(date('Y-m-d H:i:s')).'.xls';
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');

// download ke client
    header('Content-type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename="'.$fileName.'"');
    $objWriter->save('php://output');

    return $filePath.'/'.$fileName;

?>
