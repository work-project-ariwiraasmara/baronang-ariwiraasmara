<?php require('header.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><?php echo lang('Laba Rugi'); ?></h3>
    </div>
    <div class="box-body">
        <div class="row">
            <form action="" method="post">
                <div class="form-group">
                    <label class="col-sm-1 control-label" style="text-align: left;"><?php echo lang('Dari'); ?></label>
                    <div class="col-sm-2">
                        <input type="text" name="from" value="<?php echo $_POST['from']; ?>" class="form-control datepicker">
                    </div>
                    <label class="col-sm-1 control-label" style="text-align: left;"><?php echo lang('Sampai'); ?></label>
                    <div class="col-sm-2">
                        <input type="text" name="to" value="<?php echo $_POST['to']; ?>" class="form-control datepicker">
                    </div>
                    <div class="col-sm-2">
                        <button type="submit" class="btn btn-sm btn-primary"><?php echo lang('Pilih'); ?></button>
                        <a href="rep_pnl.php"><button type="button" class="btn btn-sm btn-primary">Refresh</button></a>
                    </div>
                </div>
            </form>
        </div>
        <hr>


            <?php if($_GET['acc']){ ?>

            <div class="table-responsive">
                <table class="table table-bordered">
                    <?php
                    $a = "exec dbo.TampilDetailReportCOA '0','$_GET[acc]'";
                    echo $a;
                    $b = sqlsrv_query($conn, $a);
                    while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                    ?>
                        <tr>
                            <td><?php echo $c[1]; ?></td>
                            <td colspan="7"></td>
                        </tr>
                        <?php
                        $aa = "exec dbo.TampilDetailReportCOA '1','$c[0]'";
                        $bb = sqlsrv_query($conn, $aa);
                        while($cc = sqlsrv_fetch_array($bb, SQLSRV_FETCH_NUMERIC)){
                        ?>
                        <tr>
                            <td></td>
                            <td><?php echo $cc[0]; ?></td>
                            <td><?php echo $cc[1]; ?></td>
                            <td><?php echo $cc[2]; ?></td>
                            <td colspan="4"></td>
                        </tr>
                            <?php
                            $aaa = "exec dbo.TampilDetailReportCOA '2','$cc[0]'";
                            $bbb = sqlsrv_query($conn, $aaa);
                            while($ccc = sqlsrv_fetch_array($bbb, SQLSRV_FETCH_NUMERIC)){
                            ?>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td><?php echo $ccc[0]; ?></td>
                                    <td><?php echo $ccc[1]; ?></td>
                                    <td><?php echo $ccc[2]->format('Y-m-d H:i:s'); ?></td>
                                    <td>
                                        <?php
                                        $aaaa = "select* from dbo.TransactionType where KodeTransactionType='".$ccc[3]."'";
                                        $bbbb = sqlsrv_query($conn, $aaaa);
                                        $cccc = sqlsrv_fetch_array($bbbb, SQLSRV_FETCH_NUMERIC);
                                        if($cccc != null){
                                            echo $cccc[1];
                                        }
                                        else{
                                            echo $ccc[3];
                                        }
                                        ?>
                                    </td>
                                    <td><?php echo $ccc[6]; ?></td>
                                    <td><?php echo $ccc[7]; ?></td>
                                </tr>
                            <?php } ?>
                        <?php } ?>
                    <?php } ?>
                </table>
            </div>

        <?php } ?>

        <?php if(isset($_POST['from']) and isset($_POST['to'])){ 
            $from = date('Y-m-d', strtotime($_POST['from']));
            $to = date('Y-m-d', strtotime($_POST['to']));
            $a = "exec dbo.ProsesGenerateLaporanRugiLaba '$from','$to','$_SESSION[UserID]'";
            $b = sqlsrv_query($conn, $a);
        ?>
        <?php } ?>

        <div class="table-responsive ">
        <table class="table table-bordered table-striped">
                            <tbody>
                                <ul id="treeview">
                                    <?php
                                        $atotal = 1;
                                        $x = "select* from dbo.GroupAcc where KodeGroup in('4.0.00.00.000','5.0.00.00.000','6.0.00.00.000')";
                                        $y = sqlsrv_query($conn, $x);
                                        while($z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC)){
                                        ?>
                                        <li data-icon-cls="fa fa-folder"><?php echo $z[0].' - '.$z[1].' - '.$z[4]; ?>
                                            <span class="pull-right"></i> 0 </span>
                                            <ul id="treeview">
                                            <?php
                                            $atotal1 = 0;
                                            $xx = "select * from dbo.TypeAcc where KodeGroup = '$z[0]' ORDER BY KodeTipe Asc";
                                            //echo $xx;
                                            $yy = sqlsrv_query($conn, $xx);
                                            while($zz = sqlsrv_fetch_array($yy, SQLSRV_FETCH_NUMERIC)){
                                            ?>
                                            <li data-icon-cls="fa fa-folder"><?php echo $zz[0].' - '.$zz[1].' - '.$zz[4]; ?>
                                                <span class="pull-right"></i> 0 </span>
                                                <ul id="treeview">
                                                    <?php
                                                    $xxx = "select * from dbo.AccountView where KodeTipe = '$zz[0]' and header = 0 ORDER BY KodeAccount Asc"; 
                                                    $yyy = sqlsrv_query($conn, $xxx);
                                                    while($zzz = sqlsrv_fetch_array($yyy, SQLSRV_FETCH_NUMERIC)){
                                                    ?>
                                                    <li data-icon-cls="fa fa-folder"><?php echo $zzz[0].' - '.$zzz[1].' - '.$zzz[4]; ?>
                                                    <span class="pull-right"></i> 0 </span>
                                                    <ul id="ttreeview">
                                                        <?php
                                                        $xxxx = "select * from dbo.AccountView where KodeTipe = '$zzz[2]' and header = 1 ORDER BY KodeAccount Asc";
                                                        $yyyy = sqlsrv_query($conn, $xxxx);
                                                        while($zzzz = sqlsrv_fetch_array($yyyy, SQLSRV_FETCH_NUMERIC)){
                                                        ?>
                                                        <li data-icon-cls="fa fa-folder"><?php echo $zzzz[0].' - '.$zzzz[1].' - '.$zzzz[4]; ?>
                                                            <span class="pull-right"></i> 0 </span>
                                                            <ul class="treesh">
                                                                <?php
                                                                $xxxxx = "select * from dbo.AccountView where KodeTipe = '$zzzz[2]' and header =2 ORDER BY KodeAccount Asc";
                                                                $yyyyy = sqlsrv_query($conn, $xxxxx);
                                                                while($zzzzz = sqlsrv_fetch_array($yyyyy, SQLSRV_FETCH_NUMERIC)){
                                                                ?>
                                                                <li><a href="?acc=<?php echo $zzzzz[0]; ?>&st=2"><?php echo $zzzzz[0].' - '.$zzzzz[1].' - '.$zzzzz[4]; ?></a>
                                                                    <span class="pull-right"></i> 0 </span> 
                                                                </li>  
                                                            <?php } ?>  
                                                            </ul>
                                                        </li>
                                                    <?php } ?>
                                                    </ul>
                                                    </li>
                                                <?php } ?>
                                                </ul>
                                                </li>
                                            <?php $atotal1++; } ?>        
                                            </ul>
                                        </li>

                                 <?php $atotal++; } ?>
                                </ul>
                            
                            </tbody>
                        </table>
        </div>
    </div>
</div>

 <!-- you need to include the ShieldUI CSS and JS assets in order for the TreeView widget to work -->
    <link rel="stylesheet" type="text/css" href="static/plugins/tree/tree.css" />
    <script type="text/javascript" src="static/plugins/tree/tree.js"></script>

<script type="text/javascript">
    jQuery(function ($) {
        $("#treeview").shieldTreeView();
    });
</script>

<?php require('content-footer.php');?>

<?php require('footer.php');?>
