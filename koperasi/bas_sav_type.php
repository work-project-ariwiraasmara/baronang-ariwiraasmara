<?php require('header.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<?php
//edit
$uledit = $_GET['edit'];
$ul0    = "";
$ul1    = "";
$ul2    = "";
$ul3    = "";
$ul4    = "";
$ulprocedit = "";
$uldisabled = "";
if(!empty($uledit)){
    $eulsql   = "select * from dbo.BasicSavingTypeView where KodeBasicSavingType='$uledit'";
    $eulstmt  = sqlsrv_query($conn, $eulsql);
    $eulrow   = sqlsrv_fetch_array( $eulstmt, SQLSRV_FETCH_NUMERIC);
    if(count($eulrow[0]) > 0){
        $ul0    = $eulrow[0];
        $ul1    = $eulrow[1];
        $ul2    = $eulrow[2];
        $ul3    = $eulrow[3];
        $ul4    = $eulrow[4];
        $ul5    = $eulrow[5];
        $ulprocedit = "?page=".$_GET['page']."&edit=".$ul0;
        $uldisabled = "disabled";
        $uldisabled2 = "readonly";
    }
    else{
        if(empty($_GET['page'])){
            echo "<script language='javascript'>document.location='bas_sav_type.php';</script>";
        }
        else{
            echo "<script language='javascript'>document.location='bas_sav_type.php?page=".$_GET['page']."';</script>";
        }
    }
}
?>

<?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
    <div class="alert alert-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-warning ?>"></i> <?php echo ucfirst($_SESSION['error-type']); ?></h4>
        <?php echo $_SESSION['error-message']; ?>
    </div>
<?php } ?>

<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><?php echo lang('Tipe simpanan pokok'); ?></h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <form class="form-horizontal" action="procbas_sav_type.php<?=$ulprocedit?>" method="POST">
        <div class="box-body">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="AccountNumber" class="col-sm-4 control-label" style="text-align: left;"><?php echo lang('Deskripsi'); ?></label>
                        <div class="col-sm-6">
                            <input type="text" name="des" class="form-control" id="AccountNumber" placeholder="" value="<?=$ul1?>">
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="AccountName" class="col-sm-2 control-label" style="text-align: left;"><?php echo lang('Jumlah'); ?></label>
                        <div class="col-sm-6">
                            <input type="text" name="amo" class="form-control price" id="AccountName" placeholder="" value="<?=$ul3?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="AccountName" class="col-sm-2 control-label" style="text-align: left;">KBA</label>
                        <div class="col-sm-6">
                            <select name="kba" class="form-control" <?=$uldisabled2?>>
                                <option value="">- <?php echo lang('Pilih salah satu'); ?> -</option>
                                <?php
                                $julsql   = "select * from [dbo].[BankAccountView]";
                                $julstmt = sqlsrv_query($conn, $julsql);
                                while($rjulrow  = sqlsrv_fetch_array( $julstmt, SQLSRV_FETCH_NUMERIC)){
                                    ?>
                                    <option value="<?=$rjulrow[1];?>" <?php if($rjulrow[1]==$ul5){echo "selected";} ?>><?=$rjulrow[4];?>&nbsp;<?=$rjulrow[3];?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-2"></div>
                        <div class="col-sm-6">
                            <div class="radio" style="border: 1px solid #d2d6de;padding: 7px;border-radius:15px;">
                                <?php
                                $s1 = '';
                                $s0 = '';
                                $s2 = '';
                                if($ul4 == 1){
                                    $s1 = 'checked';
                                }
                                else if($ul4 == 0){
                                    $s0 = 'checked';
                                }
                                else{
                                    $s2 = 'checked';
                                }
                                ?>
                                <label>
                                    <input type="radio" name="bank" class="minimal" id="optionsRadios1" value="1" <?php echo $s1; ?>>
                                    <?php echo lang('Simpanan pokok'); ?>
                                </label>
                                <label>
                                    <input type="radio" name="bank" class="minimal" id="optionsRadios2" value="0" <?php echo $s0; ?>>
                                    <?php echo lang('Simpanan wajib'); ?>
                                </label>
                                <label>
                                    <input type="radio" name="bank" class="minimal" id="optionsRadios3" value="2" <?php echo $s2; ?>>
                                    <?php echo lang('Simpanan sukarela'); ?>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <div class="row">
                    <div class="col-sm-4">
                        <?php if(count($eulrow[0]) > 0){ ?>
                            <button type="submit" class="btn btn-flat btn-block btn-success pull-left"><?php echo lang('Perbaharui'); ?></button>
                        <?php } else { ?>
                            <button type="submit" class="btn btn-flat btn-block btn-primary pull-right"><?php echo lang('Simpan'); ?></button>
                        <?php } ?>
                    </div>
                    <div class="col-sm-4">
                    </div>
                    <div class="col-sm-4">
                        <?php if(count($eulrow[0]) > 0){ ?>
                            <a href="bas_sav_type.php" class="btn btn-flat btn-block btn-default"><?php echo lang('Batal'); ?></a>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
</div>
<!-- /.box-body -->
</form>

<!-- /.Left col -->
<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title"><?php echo lang('Daftar tipe simpanan pokok'); ?></h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="col-xs-12 table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th><?php echo lang('No'); ?></th>
                    <th><?php echo lang('Kode simpanan pokok'); ?></th>
                    <th><?php echo lang('Deskripsi'); ?></th>
                    <th><?php echo lang('Jumlah'); ?></th>
                    <th><?php echo lang('Status'); ?></th>
                    <th><?php echo lang('KBA'); ?></th>
                    <th><?php echo lang('Aksi'); ?></th>
                </tr>
                </thead>
                <tbody>
                <?php
                //count
                $jmlulsql   = "select count(*) from dbo.BasicSavingTypeView";
                $jmlulstmt  = sqlsrv_query($conn, $jmlulsql);
                $jmlulrow   = sqlsrv_fetch_array( $jmlulstmt, SQLSRV_FETCH_NUMERIC);

                //pagging
                $perpages   = 10;
                $halaman    = $_GET['page'];
                if(empty($halaman)){
                    $posisi  = 0;
                    $batas   = $perpages;
                    $halaman = 1;
                }
                else{
                    $posisi  = (($perpages * $halaman) - 10) + 1;
                    $batas   = ($perpages * $halaman);
                }

                $ulsql = "SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY KodeBasicSavingType asc) as row FROM [dbo].[BasicSavingTypeView]) a WHERE row between '$posisi' and '$batas'";

                //$ulsql = "select * from [dbo].[BasicSavingType]";

                $ulstmt = sqlsrv_query($conn, $ulsql);

                while($ulrow = sqlsrv_fetch_array( $ulstmt, SQLSRV_FETCH_NUMERIC)){
                    $status = '';
                    if($ulrow[4] == 1){
                        $status = lang('Simpanan Pokok');
                    }
                    else if($ulrow[4] == 0){
                        $status = lang('Simpanan Wajib');
                    }
                    else{
                        $status = lang('Simpanan Sukarela');
                    }
                    ?>
                    <tr>
                        <td><?=$ulrow[6];?></td>
                        <td><?=$ulrow[0];?></td>
                        <td><?=$ulrow[1];?></td>
                        <td><?=$ulrow[3];?></td>
                        <td><?=$status;?></td>
                        <td><?=$ulrow[5];?></td>
                        <td width="20%" style="padding: 3px">
                            <div class="btn-group" style="padding-right: 15px">
                                <a href="bas_sav_type.php?page=<?=$halaman?>&edit=<?=$ulrow[0]?>" class="btn btn-default btn-flat btn-sm text-green" title="edit"><i class="fa fa-pencil-square-o"></i></a>
                            </div>
                        </td>
                    </tr>
                <?php
                }

                $jmlhalaman = ceil($jmlulrow[0]/$perpages);
                ?>
                </tbody>
            </table>
        </div>
        <div class="box-footer clearfix">
            <label><?php echo lang('Menampilkan'); ?> <?=$halaman?> to <?=$jmlhalaman?> of <?=$jmlulrow[0]?> <?php echo lang('Masukan'); ?></label>
            <?=$posisi." - ".$batas?>
            <ul class="pagination pagination-sm no-margin pull-right">
                <li class="paginate_button"><a href="bas_sav_type.php">&laquo;</a></li>
                <?php
                for($ul = 1; $ul <= $jmlhalaman; $ul++){
                    if($ul != $halaman){ $ulpageactive = ""; $ulpagedisabled = "";}else{$ulpageactive = "active"; $ulpagedisabled = "disabled";}
                    echo '<li class="paginate_button '.$ulpageactive.'"><a href="bas_sav_type.php?page='.$ul.'" '.$ulpagedisabled.'>'.$ul.'</a></li>';
                }
                ?>
                <li class="paginate_button"><a href="bas_sav_type.php?page=<?=$jmlhalaman?>">&raquo;</a></li>
            </ul>
        </div>
    </div>
</div>

<?php require('content-footer.php');?>

<?php require('footer.php');?>
