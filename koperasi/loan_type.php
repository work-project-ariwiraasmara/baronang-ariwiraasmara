<?php require('header.php');?>
<?php require('sidebar-left.php');?>
<?php require('content-header.php');?>

<?php
include "connectinti.php";

//edit
$uledit = $_GET['edit'];
$ul0    = "";
$ul1    = "";
$ul2    = "";
$ul3    = "";
$ul4    = "";
$ul5    = "";
$ul6    = "";
$ul7    = "";
$ul8    = "";
$ul9    = "";
$ul10    = "";
$uzu ="";
$ul13    = "";
$ul14    = "";
$ul15    = "";
$ul16    = "";
$ul17    = "";
$ul18    = "";
$ulprocedit = "";
$uldisabled = "";
$uldisabled2 = "";
if(!empty($uledit)){
    $eulsql   = "select * from dbo.LoanType where KodeLoanType='$uledit'";
    $eulstmt  = sqlsrv_query($conn, $eulsql);
    $eulrow   = sqlsrv_fetch_array( $eulstmt, SQLSRV_FETCH_NUMERIC);
    if(count($eulrow[0]) > 0){
        $ul0    = $eulrow[0];
        $ul1    = $eulrow[1];
        $ul2    = $eulrow[2];
        $ul3    = $eulrow[3];
        $ul4    = substr($eulrow[4], 0, -5);
        $ul5    = substr($eulrow[5], 0, -5);
        $ul6    = $eulrow[6];
        $ul7    = $eulrow[7];
        $ul8    = $eulrow[8];
        $ul9    = $eulrow[9];
        $ul10    = $eulrow[10];
        $uzu =$eulrow[11];
        $ul13 = $eulrow[13]; //is penjamin
        $ul14 = $eulrow[14]; //count penjamin
        $ul15 = $eulrow[15]; //is doc
        $ul17 = $eulrow[17]; //is over limit
        $ul18 = $eulrow[18]; //min approve
        $ul19 = $eulrow[19]; //status
        $ulprocedit = "?page=".$_GET['page']."&edit=".$ul0;
        $uldisabled = "disabled";
        $uldisabled2 = "readonly";
    }
    else{
        if(empty($_GET['page'])){
            echo "<script language='javascript'>document.location='loan_type.php';</script>";
        }
        else{
            echo "<script language='javascript'>document.location='loan_type.php?page=".$_GET['page']."';</script>";
        }
    }
}

?>

<?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
    <div class="alert alert-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-warning ?>"></i> <?php echo ucfirst($_SESSION['error-type']); ?></h4>
        <?php echo $_SESSION['error-message']; ?>
    </div>
<?php } ?>

<!-- Main content -->
<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><?php echo lang('Tipe pinjaman'); ?></h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <form class="form-horizontal" action="procloan_type.php<?=$ulprocedit?>" method = "POST">
        <div class="box-body">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <input type="hidden" name="kod" id="kod" value="<?php echo $ul0; ?>" readonly>
                        <label for="regularsavingdescription" class="col-sm-3 control-label"><?php echo lang('Deskripsi'); ?></label>
                        <div class="col-sm-6">
                            <input type="text" name="loan" class="form-control" id="regularsavingdescription" placeholder="" value="<?=$ul1?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="regularsavingdescription" class="col-sm-3 control-label"><?php echo lang('Tipe bunga'); ?></label>
                        <div class="col-sm-6">
                            <select name="int" class="form-control">
                                <option value="">- <?php echo lang('Pilih salah satu'); ?> -</option>
                                <option value="01" <?php if ($ul2=='01') {echo "selected";} ?>><?php echo lang('Flat'); ?></option>
                                <option value="02" <?php if ($ul2=='02') {echo "selected";} ?>><?php echo lang('Efektif'); ?></option>
                                <option value="03" <?php if ($ul2=='03') {echo "selected";} ?>><?php echo lang('Anuitas'); ?></option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="regularsavingdescription" class="col-sm-3 control-label"><?php echo lang('Suku bunga'); ?> (%)</label>
                        <div class="col-sm-6">
                            <input type="number" name="int_r" class="form-control" id="regularsavingdescription" placeholder="" value="<?=$ul3?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="regularsavingdescription" class="col-sm-3 control-label"><?php echo lang('Minimum jumlah'); ?></label>
                        <div class="col-sm-6">
                            <input type="text" name="min" class="form-control price" id="regularsavingdescription" placeholder="" value="<?=$ul4?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="regularsavingdescription" class="col-sm-3 control-label"><?php echo lang('Maximum jumlah'); ?></label>
                        <div class="col-sm-6">
                            <input type="text" name="max" class="form-control price" id="regularsavingdescription" placeholder="" value="<?=$ul5?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="regularsavingdescription" class="col-sm-3 control-label"><?php echo lang('Tempo hari pengajuan'); ?></label>
                        <div class="col-sm-6">
                            <input type="number" name="tempohari" class="form-control" id="regularsavingdescription" placeholder="" value="<?=$uzu?>">
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="regularsavingdescription" class="col-sm-3 control-label"><?php echo lang('Periode jatuh tempo'); ?></label>
                        <div class="col-sm-6">
                            <select name="mat" class="form-control">
                                <option value="">- <?php echo lang('Pilih salah satu'); ?> -</option>
                                <?php
                                $mpbspksql = "select * from [dbo].[Maturity]";
                                $mpbsstmt = sqlsrv_query($conn, $mpbspksql);
                                while ($mpbsrow = sqlsrv_fetch_array( $mpbsstmt, SQLSRV_FETCH_NUMERIC)) {
                                    ?>
                                    <option value="<?=$mpbsrow[0];?>" <?php if($mpbsrow[0]==$ul6){echo "selected";} ?>><?=$mpbsrow[1];?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="regularsavingdescription" class="col-sm-3 control-label"><?php echo lang('Masa kontrak'); ?></label>
                        <div class="col-sm-6">
                            <input type="number" name="con" class="form-control" id="regularsavingdescription" placeholder="" value="<?=$ul7?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="regularsavingdescription" class="col-sm-3 control-label"><?php echo lang('Pokok penalti'); ?> (%)</label>
                        <div class="col-sm-6">
                            <input type="number" name="pp" class="form-control" id="regularsavingdescription" placeholder="" value="<?=$ul8?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="regularsavingdescription" class="col-sm-3 control-label"><?php echo lang('Bunga penalti'); ?> (%)</label>
                        <div class="col-sm-6">
                            <input type="number" name="ip" class="form-control" id="regularsavingdescription" placeholder="" value="<?=$ul9?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="regularsavingdescription" class="col-sm-3 control-label"><?php echo lang('Kba'); ?></label>
                        <div class="col-sm-6">
                            <select name="kba" class="form-control" onChange="changeValuePositionLimit(this.value)" <?=$uldisabled2?>>
                                <option>- <?php echo lang('Pilih salah satu'); ?> -</option><table>
                                    <?php
                                    $julsql   = "select * from [dbo].[BankAccount] order by KID";
                                    $julstmt = sqlsrv_query($conn, $julsql);
                                    $jsArrayPositionLimit = "var prdName = new Array();\n";
                                    while($rjulrow  = sqlsrv_fetch_array( $julstmt, SQLSRV_FETCH_NUMERIC)){
                                        ?>
                                        <option <?php if($rjulrow[1]==$ul10){echo "selected";} ?> value="<?=$rjulrow[1];?>"><td width ="200"><?=$rjulrow[5];?></td><td width ="200">   </td><td width ="200"><?=$rjulrow[4];?></td></option>
                                        <?php
                                        $jsArrayPositionLimit .= "prdName['" . $rjulrow[0] . "'] = {belanja:'" . addslashes($rjulrow[1]) . "',pinjam:'".addslashes($rjulrow[12])."'};\n";
                                    }
                                    ?>
                                </table>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Interest Text" class="col-sm-3 control-label" style="text-align: left;"><?php echo lang('Status'); ?></label>
                        <div class="col-sm-4">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="status" class="minimal" id="status0" value="1" <?php if($ul19 == 1){ echo "checked";} ?>>
                                    <?php echo lang('Aktif'); ?>
                                </label>
                                <label>
                                    <input type="radio" name="status" class="minimal" id="status1" value="0" <?php if($ul19 ==  0){ echo "checked";} ?>>
                                    <?php echo lang('Tidak aktif'); ?>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <div class="col-sm-3">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name ="penjamin" id="penjamin" value="1" <?php if($ul13 == 1){ echo "checked";} ?>> <?php echo lang('Penjamin'); ?>
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <input type="number" name ="jmlpenjamin" id="jmlpenjamin" value="<?=$ul14?>" class="form-control" placeholder="<?php echo lang('Jumlah penjamin'); ?>" disabled>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name ="dokumen" id="butdoc" value="1" <?php if($ul15 == 1){ echo "checked";} ?>> <?php echo lang('Butuh dokumen'); ?>
                                </label>
                            </div>
                            <br>
                            <?php
                            $disabled = '';
                            if(empty($uledit)){
                                $disabled = 'disabled';
                            }
                            ?>
                            <table class="table table-bordered table-striped" id="tdoc">
                                <thead>
                                <tr>
                                    <th><?php echo lang('Nama dokumen'); ?></th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>
                                        <select name="nama" class="form-control" id="namedoc" <?php echo $disabled; ?>>
                                            <?php
                                            $rty = "select * from [dbo].[JenisDoc] where isRegister = 0";
                                            $dfg = sqlsrv_query($conns, $rty);
                                            while($cvb = sqlsrv_fetch_array($dfg, SQLSRV_FETCH_NUMERIC)){
                                            ?>
                                                <option value=""<?php echo $cvb[1]; ?>><?php echo $cvb[1]; ?></option>
                                            <?php } ?>
                                        </select>
                                    <td>
                                        <button type="button" class="btn btn-sm btn-success btn-add"><?php echo lang('Tambah'); ?></button>
                                    </td>
                                </tr>
                                <?php
                                $a = "select * from [dbo].[SetingDocGeneralSeting]  where LoanType='$ul0'";
                                $b = sqlsrv_query($conn, $a);
                                while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                                    ?>
                                    <tr>
                                        <td><?php echo $c[1]; ?></td>
                                        <td>
                                            <a href="procloan_type.php?n=<?= $c[1]; ?>&kod=<?= $ul0 ?>"><button class="btn btn-sm btn-danger" type="button"><i class="fa fa-times"></i></button></a>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                                <tfoot>

                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <div class="col-lg-12">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" class="minimal" name ="ipbs" id="chkxa" value="1" <?php if($ul17 == 1){ echo "checked";} ?>> <?php echo lang('Bunga'); ?> + <?php echo lang('Pokok'); ?>
                                </label>
                            </div>
                        </div>
                        <!-- /input-group -->
                    </div>
                    <div class="form-group">
                        <div class="col-lg-12">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" class="minimal" name ="over" id="chkxa" value="1" <?php if($ul17 == 1){ echo "checked";} ?>> <?php echo lang('Ijinkan melebihi batas pinjaman'); ?>
                                </label>
                            </div>
                        </div>
                        <!-- /input-group -->
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3">
                            <label class="col-sm-4 control-label" style="text-align: left;"><?php echo lang('Minimum persetujuan pengguna'); ?></label>
                        </div>
                        <div class="col-sm-6">
                            <input type="number" name ="minapp" value="<?=$ul18?>" class="form-control" id="minapp">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <b><?php echo lang('Persetujuan pengguna'); ?></b>
                            <table class="table table-bordered table-striped" id="tusr">
                                <thead>
                                <tr>
                                    <th><?php echo lang('Nama Pengguna'); ?></th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>
                                        <select class="form-control" id="usr">
                                            <option>- <?php echo lang('Pilih salah satu'); ?> -</option>
                                                <?php
                                                $julsql   = "select * from [dbo].[LoginList] where Status = 1";
                                                $julstmt = sqlsrv_query($conn, $julsql);
                                                while($rjulrow  = sqlsrv_fetch_array( $julstmt, SQLSRV_FETCH_NUMERIC)){
                                                    ?>
                                                    <option value="<?php echo $rjulrow[0] ?>"><?php echo $rjulrow[1].' - '.$rjulrow[3]; ?></option>
                                                <?php } ?>
                                        </select>
                                    </td>
                                    <td><button type="button" class="btn btn-sm btn-success btn-addu"><?php echo lang('Tambah'); ?></button></td>
                                </tr>
                                <?php
                                $a = "select * from [dbo].[UserApprovalLoanView]  where LoanType='$ul0'";
                                $b = sqlsrv_query($conn, $a);
                                while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                                    ?>
                                    <tr>
                                        <td><?php echo $c[1]; ?></td>
                                        <td>
                                            <a href="procloan_type.php?u=<?= $c[0]; ?>&kod=<?= $ul0 ?>"><button class="btn btn-sm btn-danger" type="button"><i class="fa fa-times"></i></button></a>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                                <tfoot>

                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <div class="row">
                    <div class="col-sm-4">
                        <?php if(count($eulrow[0]) > 0){ ?>
                            <button type="submit" class="btn btn-flat btn-block btn-success pull-left"><?php echo lang('Perbaharui'); ?></button>
                        <?php } else { ?>
                            <button type="submit" class="btn btn-flat btn-block btn-primary pull-right"><?php echo lang('Simpan'); ?></button>
                        <?php } ?>
                    </div>
                    <div class="col-sm-4">
                    </div>
                    <div class="col-sm-4">
                        <?php if(count($eulrow[0]) > 0){ ?>
                            <a href="loan_type.php" class="btn btn-flat btn-block btn-default"><?php echo lang('Batal'); ?></a>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </form>
</div>

<!-- /.Left col -->
<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title"><?php echo lang('Daftar tipe pinjaman'); ?></h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="col-xs-12 table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th><?php echo lang('No'); ?></th>
                    <th><?php echo lang('Kode tipe pinjaman'); ?></th>
                    <th><?php echo lang('Deskripsi'); ?></th>
                    <th><?php echo lang('Tipe Bunga'); ?></th>
                    <th><?php echo lang('Suku Bunga'); ?> (%)</th>
                    <th><?php echo lang('Minimum jumlah'); ?></th>
                    <th><?php echo lang('Maximum jumlah'); ?></th>
                    <th><?php echo lang('Periode jatuh tempo'); ?></th>
                    <th><?php echo lang('Masa Kontrak'); ?></th>
                    <th><?php echo lang('Pokok penalti'); ?> (%)</th>
                    <th><?php echo lang('Bunga pinalti'); ?> (%)</th>
                    <th><?php echo lang('Kba'); ?></th>
                    <th><?php echo lang('Aksi'); ?></th>
                </tr>
                </thead>
                <tbody>
                <?php
                //count
                $jmlulsql   = "select count(*) from dbo.LoanTypeViewNew";
                $jmlulstmt  = sqlsrv_query($conn, $jmlulsql);
                $jmlulrow   = sqlsrv_fetch_array( $jmlulstmt, SQLSRV_FETCH_NUMERIC);

                //pagging
                $perpages   = 10;
                $halaman    = $_GET['page'];
                if(empty($halaman)){
                    $posisi  = 0;
                    $batas   = $perpages;
                    $halaman = 1;
                }
                else{
                    $posisi  = (($perpages * $halaman) - 10) + 1;
                    $batas   = ($perpages * $halaman);
                }

                $ulsql = "SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY KodeLoanType asc) as row FROM [dbo].[LoanTypeViewNew]) a WHERE row between '$posisi' and '$batas'";

                //$ulsql = "select * from [dbo].[LoanType]";

                $ulstmt = sqlsrv_query($conn, $ulsql);

                while($ulrow = sqlsrv_fetch_array( $ulstmt, SQLSRV_FETCH_NUMERIC)){
                    ?>
                    <tr>
                        <td><?=$ulrow[15];?></td>
                        <td><?=$ulrow[0];?></td>
                        <td><?=$ulrow[1];?></td>
                        <td><?=$ulrow[2];?></td>
                        <td><?=$ulrow[3];?></td>
                        <td><?=$ulrow[5];?></td>
                        <td><?=$ulrow[7];?></td>
                        <td><?=$ulrow[9];?></td>
                        <td><?=$ulrow[10];?></td>
                        <td><?=$ulrow[11];?></td>
                        <td><?=$ulrow[12];?></td>
                        <td><?=$ulrow[14];?></td>
                        <td width="20%" style="padding: 3px">
                            <div class="btn-group" style="padding-right: 15px">
                                <a href="loan_type.php?page=<?=$halaman?>&edit=<?=$ulrow[0]?>" class="btn btn-default btn-flat btn-sm text-green" title="edit"><i class="fa fa-pencil-square-o"></i></a>
                            </div>
                            <!--
						<div class="btn-group">
                        <a href="procloan_type.php?page=<?=$halaman?>&delete=<?=$ulrow[0]?>" class="btn btn-default btn-flat btn-sm text-red" title="delete" onClick="return confirm('Hapus data ini ?')"><i class="fa fa-trash-o"></i></a>
						</div>
						-->
                        </td>
                    </tr>
                <?php
                }

                $jmlhalaman = ceil($jmlulrow[0]/$perpages);
                ?>
                </tbody>
            </table>
        </div>
        <div class="box-footer clearfix">
            <label>Showing <?=$halaman?> to <?=$jmlhalaman?> of <?=$jmlulrow[0]?> entries</label>
            <?=$posisi." - ".$batas?>
            <ul class="pagination pagination-sm no-margin pull-right">
                <li class="paginate_button"><a href="loan_type.php">&laquo;</a></li>
                <?php
                for($ul = 1; $ul <= $jmlhalaman; $ul++){
                    if($ul != $halaman){ $ulpageactive = ""; $ulpagedisabled = "";}else{$ulpageactive = "active"; $ulpagedisabled = "disabled";}
                    echo '<li class="paginate_button '.$ulpageactive.'"><a href="loan_type.php?page='.$ul.'" '.$ulpagedisabled.'>'.$ul.'</a></li>';
                }
                ?>
                <li class="paginate_button"><a href="loan_type.php?page=<?=$jmlhalaman?>">&raquo;</a></li>
            </ul>
        </div>
    </div>
</div>

<script type="text/javascript">
    var tampung = [];
    var tampung2 = [];

    $('#penjamin').click(function(){
        if(document.getElementById("penjamin").checked == true){
            $('#jmlpenjamin').prop('disabled', false);
        }
        else{
            $('#jmlpenjamin').prop('disabled', true);
        }
    });

    $('#butdoc').click(function(){
        if(document.getElementById("butdoc").checked == true){
            $('#namedoc').prop('disabled', false);
        }
        else{
            $('#namedoc').prop('disabled', true);
        }
    });

    $('.btn-add').click(function(){
        var nama = $('#namedoc option:selected').text();
        var loantype = $('#kod').val();

        if(nama == ''){
            alert('Harap isi inputan dengan benar');
            return false;
        }
        else if(jQuery.inArray(nama, tampung) !== -1){
            alert('Tidak dapat memilih dokumen yang sudah ditambahkan sebelumnya');
            return false;
        }
        else{
            $.ajax({
                url : "ajax_addnewdoc.php",
                type : 'POST',
                dataType : 'json',
                data: { loantype: loantype, nama: nama},   // data POST yang akan dikirim
                success : function(data) {
                    if(data.status == 1){
                        tampung.push(data.nama);
                        $("#tdoc tfoot").append("<tr>" +
                            "<td><input type='hidden' name='nama[]' value="+ data.nama +">" + data.nama + "</td>" +
                            "<td><a class='btn btn-default btn-flat btn-sm text-red remove' onclick='$(this).parent().parent().remove();'><i class='fa fa-trash-o'></i></a></td>" +
                            "</tr>");
                    }
                    else{
                        alert('Tidak dapat menambahkan dokumen yang sudah ada.');
                        return false;
                    }
                },
                error : function() {
                    alert('Telah terjadi error.');
                    return false;
                }
            });
        }
    });

    $('.btn-addu').click(function(){
        var usr = $('#usr option:selected').val();
        var loantype = $('#kod').val();

        if(usr == ''){
            alert('Harap isi inputan dengan benar');
            return false;
        }
        else if(jQuery.inArray(usr, tampung2) !== -1){
            alert('Tidak dapat memilih user yang sudah ditambahkan sebelumnya');
            return false;
        }
        else{
            $.ajax({
                url : "ajax_addnewapp.php",
                type : 'POST',
                dataType : 'json',
                data: { usr: usr, loantype: loantype},   // data POST yang akan dikirim
                success : function(data) {
                    if(data.status == 1){
                        tampung2.push(data.id);
                        $("#tusr tfoot").append("<tr>" +
                            "<td><input type='hidden' name='user[]' value="+ data.id +">" + data.nama + " - " + data.jabatan +"</td>" +
                            "<td><a class='btn btn-default btn-flat btn-sm text-red remove' onclick='$(this).parent().parent().remove();'><i class='fa fa-trash-o'></i></a></td>" +
                            "</tr>");
                    }
                    else{
                        alert('Tidak dapat menambahkan user yang sudah ada.');
                        return false;
                    }
                },
                error : function() {
                    alert('Telah terjadi error.');
                    return false;
                }
            });
        }
    });

</script>
<?php require('content-footer.php');?>

<?php require('footer.php');?>
