<?php
error_reporting(0);
include "connect.php";
include "connectinti.php";
include "connectuser.php";

if(isset($_POST['uid']) and isset($_SESSION['RequestID'])){
    $uid = $_POST['uid'];

    if(isset($_SESSION['RequestExpired'])){
        if($_SESSION['RequestExpired'] < date('Y-m-d H:i:s')){
            unset($_SESSION['RequestID']);
            unset($_SESSION['UID']);
            unset($_SESSION['RequestLink']);
            unset($_SESSION['RequestExpired']);
            unset($_SESSION['RequestMember']);
            unset($_SESSION['RequestMemberName']);

            $_SESSION['error-type'] = 'info';
            $_SESSION['error-message'] = 'Your Session Has Expired. Retry input User ID Baronang App.';
            $_SESSION['error-time'] = time() + 5;

            echo "<script>window.location.href='identity_cs.php';</script>";
        }
    }

    $a = "select * from [dbo].[UserPaymentGateway] where KodeUser='$uid'";
    $b = sqlsrv_query($connuser, $a);
    $c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC);
    if($c != null){
        $tanggal = date('Y-m-d H:i:s');
        $aa = "select TOP 1 * from [dbo].[RequestPage] where KID='$_SESSION[KID]' and ID = '$_SESSION[RequestID]' and  TanggalExpired > '$tanggal' and KodeUser='$uid' and Link='identity_cs.php' and UserID='$_SESSION[UserID]' order by TanggalExpired desc";
        $bb = sqlsrv_query($conns, $aa);
        $cc = sqlsrv_fetch_array($bb, SQLSRV_FETCH_NUMERIC);
        if($cc != null){
            if($cc[4] == 1){
                $_SESSION['RequestExpired'] = $cc[6];
            }
        }

        $jml = 0;
        $aaa = "select * from [dbo].[UserMemberKoperasi] where KID='$_SESSION[KID]' and UserID='$_SESSION[UID]'";
        $bbb = sqlsrv_query($connuser, $aaa);
        while($ccc = sqlsrv_fetch_array($bbb, SQLSRV_FETCH_NUMERIC)){
            $jml+=1;
        }
    }
}
?>

<?php if(isset($_SESSION['RequestExpired']) and $_SESSION['RequestExpired'] > date('Y-m-d H:i:s')){ ?>
    <?php if($jml > 0){ ?>
    <div class="row" style="margin-top: 50px;">
        <div style="text-align: center;">
            <a href="#" class="text-black" id="bopen">
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="btn btn-success" style="padding: 25px 65px 25px 65px; font-size: 25px;">
                        Open Account
                    </div>
                    <!-- /.info-box -->
                </div>
            </a>

            <a href="#" class="text-black" id="bclose">
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="btn btn-danger" style="padding: 25px 65px 25px 65px; font-size: 25px;">
                        Close Account
                    </div>
                    <!-- /.info-box -->
                </div>
            </a>

            <a href="cscachangecard.php" class="text-black">
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="btn btn-primary" style="padding: 25px 65px 25px 65px; font-size: 25px;">
                        Link/Change Card
                    </div>
                    <!-- /.info-box -->
                </div>
            </a>
        </div>
    </div>

    <div style="margin-top: 20px;">
        <div class="callout callout-success bopen hide">
            <div class="row" >
                <a href="csoaregsaving.php" class="text-black">
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-blue"><i class="fa fa-desktop"></i></span>
                            <div class="info-box-content">
                                <h3>Regular Saving</h3>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                </a>

                <a href="csoatimedeposit.php" class="text-black">
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-blue"><i class="fa fa-desktop"></i></span>
                            <div class="info-box-content">
                                <h3>Time Deposit</h3>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                </a>

                <a href="procemoney.php" class="text-black" onclick="return confirm('Apakah anda yakin akan membuka e-Money ?');">
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-blue"><i class="fa fa-desktop"></i></span>
                            <div class="info-box-content">
                                <h3>e-Money</h3>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                </a>
            </div>
        </div>
    </div>

    <div class="callout callout-danger bclose hide">
        <div class="row">
            <a href="cscaregsaving.php" class="text-black">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-blue"><i class="fa fa-desktop"></i></span>
                        <div class="info-box-content">
                            <h3>Regular Saving</h3>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
            </a>

            <a href="cscatimedeposit.php" class="text-black">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-blue"><i class="fa fa-desktop"></i></span>
                        <div class="info-box-content">
                            <h3>Time Deposit</h3>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
            </a>

            <a href="csloanclose.php" class="text-black">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-blue"><i class="fa fa-desktop"></i></span>
                        <div class="info-box-content">
                            <h3>Loan</h3>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
            </a>

            <a href="cscamember.php" class="text-black">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-blue"><i class="fa fa-desktop"></i></span>
                        <div class="info-box-content">
                            <h3>Member</h3>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
            </a>
        </div>
    </div>
    <?php } else { ?>
        <div class="row" style="margin-top: 30px;">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="callout callout-info">
                    <h4>Member ID Search Found <?php echo $jml; ?> Data!</h4>

                    <p>Stay create new member ? click <a href="#" id="smember">here</a> or <a href="#" data-toggle="modal" data-target=".bs-example-modal">show data</a> </p>
                </div>
            </div>
        </div>

        <div class="modal fade bs-example-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Member List</h4>
                    </div>
                    <div class="modal-body">
                        <table class="table table-bordered table-striped">
                            <tr>
                                <th>Member ID</th>
                                <th>KTP</th>
                                <th>NIP</th>
                                <th>Name</th>
                                <th>Address</th>
                                <th>Phone</th>
                                <th>Email</th>
                            </tr>
                            <?php
                            $q = "select * from [dbo].[MemberList] where Name='%$c[1]%' or email='$c[2]' or phone='$c[3]' or KTP='$c[9]'";
                            $w = sqlsrv_query($conn, $q);
                            while($e = sqlsrv_fetch_array($w, SQLSRV_FETCH_NUMERIC)){
                                ?>
                                <tr>
                                    <td><?php echo $e[1]; ?></td>
                                    <td><?php echo $e[7]; ?></td>
                                    <td><?php echo $e[8]; ?></td>
                                    <td><?php echo $e[2]; ?></td>
                                    <td><?php echo $e[3]; ?></td>
                                    <td><?php echo $e[4]; ?></td>
                                    <td><?php echo $e[5]; ?></td>
                                </tr>
                            <?php } ?>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <a href="csoamemberlist.php" class="text-black" id="newmember">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-aqua"><i class="fa fa-user-plus"></i></span>
                        <div class="info-box-content">
                            <h3>New Member</h3>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
            </a>

            <a href="csoamemberlink.php" class="text-black">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-green"><i class="fa fa-user"></i></span>
                        <div class="info-box-content">
                            <h3>Member Link</h3>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
            </a>
        </div>
    <?php } ?>
<?php } ?>

<?php if(!isset($_SESSION['RequestExpired'])){ ?>
    <script type="text/javascript">
        setTimeout(function(){
            var uid = $('#result-code').val();

            $.ajax({
                url : "identity_menu.php",
                type : 'POST',
                data: { uid: uid},
                success : function(data) {
                    $("#menu").html(data);
                },
                error : function(){
                    alert('Try again.');
                    return false;
                }
            });
        }, 3000);
    </script>
<?php } ?>

<script type="text/javascript">
    $('#bopen').click(function(){
        $('.bopen').removeClass('hide');
        $('.bclose').addClass('hide');
    });

    $('#bclose').click(function(){
        $('.bopen').addClass('hide');
        $('.bclose').removeClass('hide');
    });

    $('#smember').click(function(){
        $('#newmember').removeClass('hide');
    });
</script>