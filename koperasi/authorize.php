<?php require('header.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<div class="alert alert-warning alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-warning ?>"></i> Warning</h4>
    You are not authorize for this menu!
</div>

<?php require('content-footer.php');?>

<?php require('footer.php');?>