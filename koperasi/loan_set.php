<?php require('header.php');?>
<?php require('sidebar-left.php');?>
<?php require('content-header.php');?>
<script language="javascript">
    function load_page(value){
        window.location.href = "loan_set.php?loantype=" + value;
    };
</script>

<?php
        //edit
      $uledit = $_GET['edit'];
      if(!empty($uledit)){
        $eulsql   = "select * from dbo.LoanType where KodeLoanType='$uledit'";
        $eulstmt  = sqlsrv_query($conn, $eulsql);
        $eulrow   = sqlsrv_fetch_array( $eulstmt, SQLSRV_FETCH_NUMERIC);
        if(count($eulrow[0]) > 0){
          $ul0    = $eulrow[0];
          $ul1    = $eulrow[1];
          $ul2    = $eulrow[2];
		  $ul3    = $eulrow[3];
          $ulprocedit = "?page=".$_GET['page']."&edit=".$ul0;
          $uldisabled = "disabled";
        }
        else{
          if(empty($_GET['page'])){
            echo "<script language='javascript'>document.location='loan_type.php';</script>";
          }
          else{
            echo "<script language='javascript'>document.location='loan_type.php?page=".$_GET['page']."';</script>";
          }
        }
      }
      else{
        $ul0    = "";
        $ul1    = "";
        $ul2    = "";
		$ul3    = "";
        $ulprocedit = "";
        $uldisabled = "";
      }

      ?>
	  <?php
	  echo "<script type='text/javascript'>";
		echo "function yesnoCheck() {";
		echo "if (document.getElementById('yesCheck').checked) {";
			echo "document.getElementById('ifYes').style.display = 'block';";
			echo "document.getElementById('jmlpenjamin').value = '0';";
		echo"}" ;
		echo"else{";
		echo"	document.getElementById('ifYes').style.display = 'none';";
		echo"}"	;
		echo"}";
		echo"</script>";
	  ?>
	  	  <?php
	  echo "<script type='text/javascript'>";
		echo "function yesnoCheck2() {";
		echo "if (document.getElementById('yesCheck2').checked) {";
			echo "document.getElementById('ifYes2').style.display = 'block';";
		echo"}" ;
		echo"else{";
		echo"	document.getElementById('ifYes2').style.display = 'none';";
		echo"}"	;
		echo"}";
		echo"</script>";
	  ?>
 <!-- Main content -->
		<div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Loan Setting</h3>
			   <?php
              /*
              $xtsql    = "select dbo.getKodeLoanType ($_SESSION[KID])";
              $xstmt    = sqlsrv_query($conn, $xtsql);
              $xrow     = sqlsrv_fetch_array( $xstmt, SQLSRV_FETCH_NUMERIC);
              $KIDlope  = $xrow[0];
              */
			  $loantyp = $_GET['loantype'];
              $mgssql    = "select * from [dbo].[GeneralSetingConfig] where KID='$_SESSION[KID]' and LoanType='$loantyp'";
              $mgsstmt    = sqlsrv_query($conn, $mgssql);
              $mgsrow     = sqlsrv_fetch_array( $mgsstmt, SQLSRV_FETCH_NUMERIC);

              if(count($mgsrow[0]) > 0){
                $mgs0  = $mgsrow[0];
                $mgs1  = $mgsrow[1];
                $mgs2  = $mgsrow[2];
				$mgs3  = $mgsrow[3];
				$mgs4  = $mgsrow[4];
				$mgs5  = $mgsrow[5];
				$msg6  = $mgsrow[6];
				$minapp = $mgsrow[8];
              }
              else{
				$mgs0	 ="";
                $mgs1  = "";
                $mgs2  = "0";
                $mgs3  = "";
				$mgs4  = "0";
				$mgs5  = "";
				$msg6  = "0";
				$minapp= "0";
              }
            ?>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
                        <form class="form-horizontal" action="procloan_set.php<?=$ulprocedit?>" method = "POST">
              <div class="box-body">
				<div class="row">
					<div class="col-sm-12">
						<div class="form-group">
						<div class="col-lg-6">
						  <div class="checkbox">
							<label>
								<input type="checkbox" class="checkbox" name ="chk1" id="yesCheck "value="1" <?php if($mgs2 == 1){ echo "checked";} ?>>Penjamin
								</label>
								</div>
						</div>
						<div class="col-lg-6">
						<div class="input-group">
							<!--div id="ifYes" style="display:none"-->
							<input type="text" name ="jmlpenjamin" id="jmlpenjamin" value="<?=$mgs3?>" class="form-control" placeholder="Jumlah Penjamin"  onKeyUp="validAngka(this)">
								<!--/div-->
							</div>
						</div>
						</div>
			  <div class="form-group">
                <div class="col-lg-6">
				  <div class="checkbox">
					<label>
						<input type="checkbox" class="checkbox" name ="chk2" id="yesCheck2" value="1" <?php if($mgs4 == 1){ echo "checked";} ?>>Butuh Dokumen
						</label>
                  </div>
				 </div>
                  <!-- /input-group -->
                <div class="col-lg-6">
                  <div class="input-group">
							<!--div  id="ifYes2" style="display:none"-->
							<input type="text" name ="nama" id="nama" value="" class="form-control" placeholder="Nama Dokumen">
							<input type="text" name ="ket" value="" class="form-control" id="ket" placeholder="Keterangan">
								<!--/div-->
						</div>
					</div>
				</div>
			  <div class="form-group">
			  <div class="col-lg-6">
                    <label class="col-sm-4 control-label" style="text-align: left;">Minimal Approval User</label>
			  </div>
			  <div class="col-lg-6">
                    <input type="number" name ="minapp" value="<?=$minapp?>" class="form-control" id="minapp" placeholder="Min Approval" >
			  </div>
			  </div>
			  <div class="form-group">
			  <div class="col-lg-6">
                    <label class="col-sm-4 control-label" style="text-align: left;">Produk Pinjaman</label>
			  </div>
                <div class="col-lg-6">
                        <select name="produk" class="form-control" id="produk" onchange="load_page(this.value);">
                            <option>--- Select One ---</option>
                            <?php
                            $ltvsql   = "select * from [dbo].[LoanTypeView]";
                            $ltvstmt  = sqlsrv_query($conn, $ltvsql);
                            while($ltvrow   = sqlsrv_fetch_array( $ltvstmt, SQLSRV_FETCH_NUMERIC)){
                                echo "<option value=";
                                echo $ltvrow[0];
                                if ($ltvrow[0]==$loantyp) {
                                    echo " selected='selected' ";
                                }
                                echo ">&nbsp;";
                                echo $ltvrow[0];
                                echo "<br>&nbsp;";
                                echo $ltvrow[1];
                                echo "</option>";
                            }
                            ?>
                        </select>
                </div>
				</div>
				<div class="form-group">
                <div class="col-lg-12">
				  <div class="checkbox">
					<label>
						<input type="checkbox" class="checkbox" name ="chkx" id="chkxa" value="1" <?php if($msg6 == 1){ echo "checked";} ?>>Ijinkan Over Limit Pinjaman
						</label>
                  </div>
				 </div>
                  <!-- /input-group -->
				</div>


                <!-- /.col-lg-6 -->
					<div class="row">
				 <div class="box-footer">
						<div class="row">
							<div class="col-sm-4">
							<?php
								if(count($eulrow[0]) > 0){
								 echo '<button type="submit" class="btn btn-flat btn-block btn-success pull-left">Update</button>';
								}
								else{
										echo '<button type="submit" class="btn btn-flat btn-block btn-primary pull-right">Save</button>';
										}
								?>
							</div>
								<div class="col-sm-4">
								</div>
									<div class="col-sm-4">
									<?php
										if(count($eulrow[0]) > 0){
										echo '<a href="loan_set.php?page='.$_GET['page'].'" class="btn btn-flat btn-block btn-default">Cancel</a>';
										}
									?>
									</div>
									</div>
						</div>
					</div>
              </div>
              <!-- /.box-body -->
            </form>
          </div>
		</div>
		</div>
        <!-- /.Left col -->
			<div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title">Loan Type Data</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="col-xs-12 table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
					<th>No</th>
                    <th>Nama Dokumen</th>
                    <th>Keterangan</th>
					<th>Action</th>
                  </tr>
              </thead>
              <tbody>
			  <?php
                    //count
                    $jmlulsql   = "select count(*) from dbo.SetingDocGeneralSeting where LoanType='$loantyp'";
                    $jmlulstmt  = sqlsrv_query($conn, $jmlulsql);
                    $jmlulrow   = sqlsrv_fetch_array( $jmlulstmt, SQLSRV_FETCH_NUMERIC);

                    //pagging
                    $perpages   = 10;
                    $halaman    = $_GET['page'];
                    if(empty($halaman)){
                       $posisi  = 0;
                       $batas   = $perpages;
                       $halaman = 1;
                    }
                    else{
                       $posisi  = (($perpages * $halaman) - 10) + 1;
                       $batas   = ($perpages * $halaman);
                    }

                    $ulsql = "SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY KID asc) as row FROM [dbo].[SetingDocGeneralSeting] where LoanType='$loantyp') a WHERE row between '$posisi' and '$batas'";

                    //$ulsql = "select * from [dbo].[SetingDocGeneralSeting]";

                    $ulstmt = sqlsrv_query($conn, $ulsql);

                    while($ulrow = sqlsrv_fetch_array( $ulstmt, SQLSRV_FETCH_NUMERIC)){
                  ?>
							 <tr>
							  <td><?=$ulrow[4];?></td>
							  <td><?=$ulrow[1];?></td>
							  <td><?=$ulrow[2];?></td>
							  <td width="20%" style="padding: 3px">
						<div class="btn-group">
                        <a href="procloan_set.php?page=<?=$halaman?>&delete=<?=$ulrow[1]?>&prod=<?=$loantyp?>" class="btn btn-default btn-flat btn-sm text-red" title="delete" onClick="return confirm('Hapus data ini ?')"><i class="fa fa-trash-o"></i></a>
						</div>
							</td>
							</tr>
							<?php
								$mbl++;
							  }
							?>
              </tbody>
            </table>
          </div>
          <div class="box-footer clearfix">
              <label>Showing <?=$halaman?> to <?=$jmlhalaman?> of <?=$jmlulrow[0]?> entries</label>
              <?=$posisi." - ".$batas?>
              <ul class="pagination pagination-sm no-margin pull-right">
                <li class="paginate_button"><a href="loan_set.php">&laquo;</a></li>
                <?php
                  for($ul = 1; $ul <= $jmlhalaman; $ul++){
                    if($ul != $halaman){ $ulpageactive = ""; $ulpagedisabled = "";}else{$ulpageactive = "active"; $ulpagedisabled = "disabled";}
                    echo '<li class="paginate_button '.$ulpageactive.'"><a href="loan_set.php?page='.$ul.'" '.$ulpagedisabled.'>'.$ul.'</a></li>';
                  }
                ?>
                <li class="paginate_button"><a href="loan_set.php?page=<?=$jmlhalaman?>">&raquo;</a></li>
              </ul>
            </div>
          </div>
        </div>

      <?php require('content-footer.php');?>

      <?php require('footer.php');?>
