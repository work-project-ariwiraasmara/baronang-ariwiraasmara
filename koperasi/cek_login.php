<?php 
session_start();
include("conf/conf.inc");
include("connectuser.php");
include("connectinti.php");
include 'lang.lib.php';
include_once("lib/encrDecr/eds.php");

$kid		= $_POST['kid'];
$name 		= $_POST['name'];
$password 	= md5($_POST['password']);

function messageAlert($message, $type){
    $_SESSION['error-message'] = $message;
    $_SESSION['error-type'] = $type;
    $_SESSION['error-time'] = time()+5;
}

$model = new Encr();

$tsqlinti = "select * from [dbo].[ListKoperasi] where KID='$kid'";
$stmtinti = sqlsrv_query($conns, $tsqlinti);
$rowinti = sqlsrv_fetch_array( $stmtinti, SQLSRV_FETCH_NUMERIC);
if($kid == "" || $name == "" || $password == ""){
    messageAlert(lang('Harap isi seluruh kolom'),'info');
    header('Location: login.php');
}
else{
	if(count($rowinti[0]) > 0){
		if($rowinti[6] == 1 ){
            $connectionInfo = array( "UID"=>$rowinti[4],
			                         "PWD"=>$rowinti[5],
			                         "Database"=>$rowinti[3]);
			$conn = sqlsrv_connect($rowinti[2], $connectionInfo);

            $_SESSION['DatabaseName'] =  $model->encrs($rowinti[3], $key);	//UserID
            $_SESSION['UserDB'] =  $model->encrs($rowinti[4], $key);	//UserDB
            $_SESSION['PassDB'] =  $model->encrs($rowinti[5], $key);	//PassDB
            $_SESSION['Server'] = $model->encrs($rowinti[2], $key); //Server

            $tsql = "select * from [dbo].[LoginList] where Name='$name' and Passwd='$password' and Status = 1";
			$stmt = sqlsrv_query($conn, $tsql);
			$row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_NUMERIC);
			if (count($row[0]) > 0)
			{
                $_SESSION['KID'] 			=  $rowinti[0];	//KID
				$_SESSION['NamaKoperasi'] 	=  $rowinti[1];	//KID
				$_SESSION['UserID'] 		=  $row[0];		//UserID
				$_SESSION['Name'] 			=  $row[1];		//Name
				$_SESSION['KodeJabatan'] 	=  $row[3];		//KodeJabatan

                header('Location: dashboard.php');
			}
			else
			{
                //cek user pertama x login
                $aa = "select top 1 * from [dbo].[UserRegister] where KID='$kid' and Status = 1";
                $bb = sqlsrv_query($conns, $aa);
                $cc = sqlsrv_fetch_array( $bb, SQLSRV_FETCH_NUMERIC);
                if($cc != null){
                    $qq = "select top 1 * from [dbo].[UserPaymentGateway] where KodeUser='$cc[0]' and Status = 1";
                    $ww = sqlsrv_query($connuser, $qq);
                    $ee = sqlsrv_fetch_array($ww, SQLSRV_FETCH_NUMERIC);
                    if($ee != null){
                        $_SESSION['KID'] 			=  $rowinti[0];	//KID
                        $_SESSION['NamaKoperasi'] 	=  $rowinti[1];	//KID
                        $_SESSION['UserID'] 		=  $ee[0];		//UserID
                        $_SESSION['Name'] 			=  $ee[2];		//Name
                        $_SESSION['KodeJabatan'] 	=  'SU';		//KodeJabatan

                        $a = "exec [dbo].[ProsesLoginListAwal] '$ee[0]','$ee[2]','$ee[5]','SU'";
                        $b = sqlsrv_query($conn, $a);

                        $aaa = "exec [dbo].[ProsesProfil] '$cc[2]','$cc[5]','','','$cc[3]','$cc[6]','$cc[7]','$cc[8]'";
                        $bbb = sqlsrv_query($conn, $aaa);

                        if($b and $bbb){
                            if($ee[2] == $name and $ee[5] == $password){
                                echo "<script language='javascript'>document.location='dashboard.php';</script>";
                            }
                            else{
                                messageAlert(lang('Gagal login, Username atau Password salah'),'warning');
                                header('Location: login.php');
                            }
                        }
                        else{
                            messageAlert(lang('Gagal mendapatkan username dan password'),'warning');
                            header('Location: login.php');
                        }
                    }
                    else{
                        messageAlert(lang('Gagal login, Username atau Password salah'),'warning');
                        header('Location: login.php');
                    }
                }
                else{
                    messageAlert(lang('Gagal login, koperasi belum melakukan konfirmasi'),'warning');
                    header('Location: login.php');
                }
			}
		}
		else{
            messageAlert(lang('Gagal login, koperasi tidak aktif'),'warning');
            header('Location: login.php');
		}
	}
	else{
        messageAlert(lang('Gagal login, koperasi tidak ditemukan'),'warning');
        header('Location: login.php');
	}
}
?>