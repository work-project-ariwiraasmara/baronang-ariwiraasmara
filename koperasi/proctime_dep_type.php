<?php
session_start();
error_reporting(0);

include "connect.php";

$page			= $_GET['page'];
$edit			= $_GET['edit'];
$delete			= $_GET['delete'];

$dsc	= $_POST['dsc'];
$inter	= $_POST['inter'];
$cp		= $_POST['cp'];
$ma		= $_POST['ma'];
$fp		= $_POST['fp'];
$ip		= $_POST['ip'];
$kba	= $_POST['kba'];

if($dsc == "" || $inter == "" || $cp == "" || $ma == "" || $fp == "" || $ip == "" || $kba == ""){
    messageAlert(lang('Harap isi seluruh kolom'),'info');
    header('Location: time_dep_type.php');
}
else{
    if(!empty($edit)){
        $upultsql = "update [dbo].[TimeDepositType] set Description='$dsc', InterestRate='$inter', ContractPeriod='$cp', MinAmmount='$ma', FixPinaltyFee='$fp', InterestPinalty='$ip', KBA='$kba' where KodeTimeDepositType='$edit'";
        $upulstmt = sqlsrv_query($conn, $upultsql);
        if($upulstmt){
            messageAlert(lang('Berhasil memperbaharui data ke database'),'success');
            header('Location: time_dep_type.php');
        }
        else{
            messageAlert(lang('Gagal memperbaharui data ke database'),'danger');
            header('Location: time_dep_type.php');
        }
    }
    else{
        $a = "select [dbo].[getKodeTimeDepositType]('$_SESSION[KID]')";
        $b = sqlsrv_query($conn, $a);
        $c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC);
        $sel = $c[0];

        $spbltsql = "exec [dbo].[ProsesTimeDepositType] '$sel','$dsc','$inter','$cp','$ma','$fp','$ip','$kba'";
        $spblstmt = sqlsrv_query($conn, $spbltsql);

        if($spblstmt){
            messageAlert(lang('Berhasil menyimpan ke database'),'success');
            header('Location: time_dep_type.php');
        }
        else{
            messageAlert(lang('Gagal memperbaharui data ke database'),'danger');
            header('Location: time_dep_type.php');
        }
    }
}

?>
