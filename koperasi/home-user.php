<?php require('header.php');?>      
<?php require('sidebar-left-user.php');?>

<section class="content">
<div class="row">
    <!-- Content Header (Page header) -->
	<section class="content-header">
      <h1>
        Home
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-8 connectedSortable">
		
          <div class="box box-info">
          <!-- Profile Image -->
		  
            <div class="box-body box-profile">
			  
              <img class="profile-user-img img-responsive img-circle" img src="static/dist/img/user2-160x160.jpg" alt="User Image">

              <h3 class="profile-username text-center"><?=$NameUser?></h3>
			  <a href="#">
              <p class="text-muted text-center">Nomor Anggota</p>
			  </a>
              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>Saldo</b> <a class="pull-right">143,504,544</a>
                </li>
              </ul>
            </div>
            <!-- /.box-body -->
          <!-- /.box -->
        </div>
        </section>
		
        <!-- /.Left col -->
		</div>
		<div class="row">
        
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  </section>
  
<?php require('content-footer.php');?>
<?php require('footer.php');?>