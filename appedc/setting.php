<?php require('header.php');?>
    <script type="text/javascript">
        var tampung = [];
    </script>
	<?php
        $nama; $logo;
        $target_logo = 'images/';
        if(isset($_SESSION['nama_merchant'])) {
            $nama = $_SESSION['nama_merchant'];
        }
        else {
            $_SESSION['nama_merchant'] = 'nama merchant';
            $nama = $_SESSION['nama_merchant'];
        }
        if(isset($_SESSION['logo_merchant'])) {
            $logo = $_SESSION['logo_merchant'];
        }
        else {
            $_SESSION['logo_merchant'] = 'times.png';
            $logo = $_SESSION['logo_merchant'];
        }
    ?>
    <form method="post" action="" enctype="multipart/form-data">
        <div class="row rowhead">
            <div class="col-xs-5 col-sm-5 col-md-3 col-lg-5">
                <img class="img-responsive" src="<?php echo $target_logo.$logo;?>" style="height: 50px;" />
            </div>
            <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                <input type="text" name="txt_mname" class="" id="txt_mname" value="<?php echo $nama;?>"/>
            </div>
            <br><br>
            <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                <input type="file" name="fol_logo" class="" id="fl_logo" value="Change Logo"/>
            </div>
        </div>
        <table class="table table-bordered tableNumber" id="tableNumber">
            <thead>
                <tr>
                    <div class="row trc">
                        <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10"><input type="number" name="inNumb" class="inNumb" id="inNumb"/></div>
                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"><input type="button" name="btnAdd" class="btn-primary btnSetting" id="btnAdd" value="add"/></div>
                    </div>
                </tr>
            </thead>
            <tbody class="cgetNumbSett" id="cgetNumbSett">
                <?php
                    if(isset($_SESSION['gsNumb'])) {
                        foreach($_SESSION['gsNumb'] as $gsn=>$gsV) {
                            echo "<tr><th>".$gsV."</th></tr>";
							echo "<script>tampung.push('".$gsV."')</script>";
                        }
                    }
                ?>
            </tbody>
        </table>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 btnRowSave">
                <input type="submit" name="save" class="btn-primary btnSaveNumb btnSetting" id="btnSaveNumb" value="save"/>
            </div>
        </div>
    </form>
    <?php
        $nama = @$_POST['txt_mname'];
        $logo = @$_FILES['fol_logo']['tmp_name'];
        $dest_logo = @$_FILES['fol_logo']['name'];
        $save = @$_POST['save'];
        if($save) {
            $_SESSION['nama_merchant'] = $nama;
            if( $logo != null || $logo != "" ) {
                $_SESSION['logo_merchant'] = $logo;
                move_uploaded_file($logo, $target_logo.$dest_logo);
            }
            if( is_null($_SESSION['gsNumb']) ) {
                $cout = 0;
                $_SESSION['gsNumb'] = array();
                foreach($_POST['gsNumb'] as $gsVal) {
                    $gsNumb = $_POST['gsNumb'][$cout];
                    array_push($_SESSION['gsNumb'], $gsNumb);
                    $cout++;
                }
            }
            else {
                $cout = 0;
                foreach($_POST['gsNumb'] as $gsVal) {
                    $gsNumb = $_POST['gsNumb'][$cout];
                    if (in_array($gsNumb, $_SESSION['gsNumb'])) {
                        echo '<script language="javascript">';
                        echo 'alert("Data Sudah Ada")';
                        echo '</script>';
                    } else {
                        array_push($_SESSION['gsNumb'], $gsNumb);
                    }
                    $cout++;
                }
            }
            echo '<script language="javascript">';
            echo 'window.location.href = "setting.php"';
            echo '</script>';
        }
    ?>
    <script type="text/javascript">
        $(document).ready(function(){
            var table = document.getElementById("tableNumber");
            $('#btnAdd').click(function(){
                var getNumb = $('#inNumb').val();
                if(getNumb == '') {
                    alert('Harap isi inputan dengan benar');
                    return false;
                }
                else if(jQuery.inArray(getNumb, tampung) !== -1){
                    alert('Tidak dapat menambahkan data yang sudah ditambahkan sebelumnya');
                    return false;
                }
                else {
                    tampung.push(getNumb);
                    var markup = "<tr><th><input type='text' name='gsNumb[]' id='gsNumb' value='" + getNumb + "' readonly/></th></tr>";
                    $("#tableNumber tbody").append(markup);
                }
            });
        });
    </script>
    <style>
        th, td {
            font-size: 30px;
            text-align: center;
        }
        input {
            color: #000;
        }
    </style>
<?php require('footer.php');?>