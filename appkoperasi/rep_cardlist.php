    <?php require('header.php');?>

<?php require('sidebar-right.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>


<div class="animated fadeinup delay-1">
    <div class="page-content">

        <h2 class="uppercase"><?php echo lang('Laporan List Kartu'); ?></h2>

            <form class="form-horizontal" action="" method = "GET">
                <div class="input-field">
                    <input type="text" name="tgl1" id="tgl1" class="datepicker" value="<?php echo "$_GET[tgl1]"; ?>" autocomplete="off"></p></td>
                    <label><?php echo ('Tanggal Awal'); ?></label>
                </div>
                
                <div class="input-field">
                    <input type="text" name="tgl2" id="tgl2" class="datepicker" value="<?php echo "$_GET[tgl2]"; ?>" autocomplete="off"></p></td>
                    <label><?php echo ('Tanggal Akhir'); ?></label>
                </div>

                <div style="margin-top: 30px;">
                    <button type="submit" class="waves-effect waves-light btn-large primary-color width-100"><?php echo lang('Pilih'); ?></button>
                </div>

                <div style="margin-top: 10px; margin-bottom: 30px;">
                    <a href="rep_cardlist.php"><button type="button" class="waves-effect waves-light btn-large primary-color width-100">Refresh</button></a>
                </div>
            </form>

        <?php if($_GET['tgl1'] and $_GET['tgl2']){
            $tgl1 = date ('Y-m-d h:i:s', strtotime($_GET['tgl1']));
            $tanggaldari = date('Y-m-d', strtotime($tgl1));
            $hanyatanggal = date('d', strtotime($tgl1));
            $from1= date('Y/m/d h:i:s', strtotime('-1 days', strtotime($_GET['tgl1'])));
            //echo $tgl1;
            $tgl2 = date ('Y-m-d h:i:s', strtotime($_GET['tgl2']));
            //echo $tgl2;
            $tanggalsampai = date('Y-m-d', strtotime($tgl2));
            $hanyatanggal1 = date('d', strtotime($tgl2));
            $tglh = date('Y-m-d h:i:s', strtotime('+1 days', strtotime($tgl2)));
            $akun = $_GET['akun'];
            $kid = $_SESSION[KID];
            //echo $kid;
            ?>
            <div>
                <a href="drep_cardlist.php?from=<?php echo $tanggaldari; ?>&to=<?php echo $tanggalsampai; ?>"><button type="button" class="waves-effect waves-light btn-large primary-color width-100">Download to excel</button></a>
            </div>

            <div class="box-header" align="center" style="margin-top: 30px;">
                <tr>
                    <h3 class="" align="center"><?php echo ('Laporan List Kartu'); ?></h3>
                </tr>
            </div>
            <div class="box-header " align="Center">
               


            <div class="box-header" align="Center">
                    <tr>
                        <?php
                            $bulan = date('m', strtotime($_GET['tgl1']));
                            $tahun = date('Y', strtotime($_GET['tgl1']));
                            $bulan2 = date('m', strtotime($_GET['tgl2']));
                            $tahun2 = date('Y', strtotime($_GET['tgl2']));
                            if ($bulan != 1) {
                                if ($bulan != 2 ) {
                                   if ($bulan != 3) {
                                       if ($bulan != 4) {
                                            if ($bulan !=5) {
                                                if ($bulan !=6) {
                                                    if ($bulan !=7) {
                                                        if ($bulan !=8) {
                                                            if ($bulan !=9) {
                                                                if ($bulan !=10) {
                                                                    if ($bulan !=11) {
                                                                        $bulan = 'Desember';
                                                                    } else {
                                                                        $bulan = 'Nopember';
                                                                    }
                                                                } else {
                                                                    $bulan = 'Oktober';
                                                                }
                                                            } else {
                                                                $bulan = 'September';
                                                            }
                                                        } else {
                                                            $bulan = 'Agustus';
                                                        }
                                                    } else {
                                                        $bulan = 'Juli';
                                                    }
                                                } else {
                                                    $bulan = 'Juni';
                                                }
                                            } else {
                                              $bulan = 'Mei';  
                                            }
                                       } else {
                                            $bulan = 'April';       
                                       }
                                   } else {
                                    $bulan = 'Maret';
                                   }
                                } else {
                                    $bulan = 'Februari';    
                                }                               
                            } else {
                                $bulan = 'Januari';
                            }
                        ?>
                        <?php
                        if ($bulan2 != 1) {
                                if ($bulan2 != 2 ) {
                                   if ($bulan2 != 3) {
                                       if ($bulan2 != 4) {
                                            if ($bulan2 !=5) {
                                                if ($bulan2 !=6) {
                                                    if ($bulan2 !=7) {
                                                        if ($bulan2 !=8) {
                                                            if ($bulan2 !=9) {
                                                                if ($bulan2 !=10) {
                                                                    if ($bulan2 !=11) {
                                                                        $bulan2 = 'Desember';
                                                                    } else {
                                                                        $bulan2 = 'Nopember';
                                                                    }
                                                                } else {
                                                                    $bulan2 = 'Oktober';
                                                                }
                                                            } else {
                                                                $bulan2 = 'September';
                                                            }
                                                        } else {
                                                            $bulan2 = 'Agustus';
                                                        }
                                                    } else {
                                                        $bulan2 = 'Juli';
                                                    }
                                                } else {
                                                    $bulan2 = 'Juni';
                                                }
                                            } else {
                                              $bulan2 = 'Mei';  
                                            }
                                       } else {
                                            $bulan2 = 'April';       
                                       }
                                   } else {
                                    $bulan2 = 'Maret';
                                   }
                                } else {
                                    $bulan2 = 'Februari';    
                                }                               
                            } else {
                                $bulan2 = 'Januari';
                            }
                        ?>
                        <h5 class="box-title"><?php echo "Periode : ",$hanyatanggal," ",$bulan," ",$tahun, " - ", $hanyatanggal1," ", $bulan2," ",$tahun2; ?></h5>
                    </tr>
               
              
                <div class="row">
                    <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-lg-12 table-responsive">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th><?php echo 'No'; ?></th>
                                    <th><?php echo 'No Kartu'; ?></th>
                                    <th><?php echo 'Debit'; ?></th>
                                    <th><?php echo 'Kredit'; ?></th>
                                    <th><?php echo 'Saldo'; ?></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                            //count
                            $jmlulsql   = "select count(*) from dbo.MasterCard where status = '1' and datecreate between '$from1' and '$tglh'";
                            //echo $jmlulsql;
                            $jmlulstmt  = sqlsrv_query($conn, $jmlulsql);
                            $jmlulrow   = sqlsrv_fetch_array($jmlulstmt, SQLSRV_FETCH_NUMERIC);

                            //pagging
                            $perpages   = 10;
                            $halaman    = $_GET['page'];
                            if(empty($halaman)){
                                $posisi  = 0;
                                $batas   = $perpages;
                                $halaman = 1;
                            }
                            else{
                                $posisi  = (($perpages * $halaman) - 10) + 1;
                                $batas   = ($perpages * $halaman);
                            }

                            
                            $from= date('Y/m/d h:i:s', strtotime($_GET['tgl1']));
                            
                            $to= $_GET['tgl2'];
                            $tgla= date('1990/01/01 00:00:00');
                            $tglk = date('Y/m/d h:i:s', strtotime('+1 days', strtotime($from)));
                            $tglh = date('Y/m/d h:i:s', strtotime('+1 days', strtotime($to)));
                            $ulsql = "SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY datecreate asc) as row FROM [dbo].[MasterCard] a where status = '1' and datecreate between '$from1' and '$tglh') a WHERE row between '$posisi' and '$batas'";
                            //echo $ulsql;
                            //echo $ulsql;
                            
                            $ulstmt = sqlsrv_query($conn, $ulsql);
                            while($ulrow = sqlsrv_fetch_array( $ulstmt, SQLSRV_FETCH_NUMERIC)){
                                $saldo = 0;
                                if ($ulrow != null){

                                    $snilaid = "Select sum(amount) from dbo.translist where AccountDebet = '$ulrow[0]' and transactiontype = 'TOPP' and Date between '$tgla' and '$tglk' or AccountDebet = '$ulrow[0]' and transactiontype = 'PARK' and Date between '$tgla' and '$tglk' or  AccountDebet = '$ulrow[0]' and transactiontype = 'MBRS' and Date between '$tgla' and '$tglk' ";
                                    //echo $snilaid;
                                    $pnilaid = sqlsrv_query($conn, $snilaid);
                                    while ($hnilaid = sqlsrv_fetch_array( $pnilaid, SQLSRV_FETCH_NUMERIC)){
                                    if ($hnilaid != 0){
                                        $hsnilaid = $hnilaid[0];    
                                    } else {
                                        $hsnilaid = 0;    
                                    }
                                    }

                                    $snilaik = "Select sum(amount) from dbo.translist where AccountKredit = '$ulrow[0]' and transactiontype = 'TOPP' and Date between '$tgla' and '$tglk' or AccountKredit = '$ulrow[0]' and transactiontype = 'PARK' and Date between '$tgla' and '$tglk' or  AccountKredit = '$ulrow[0]' and transactiontype = 'MBRS' and Date between '$tgla' and '$tglk' ";
                                    //echo $snilaik;
                                    $pnilaik = sqlsrv_query($conn, $snilaik);
                                    while ($hnilaik = sqlsrv_fetch_array( $pnilaik, SQLSRV_FETCH_NUMERIC)){
                                    if ($hnilaik != 0){
                                        $hsnilaik = $hnilaik[0];    
                                    } else {
                                        $hsnilaik = 0;    
                                    }
                                    }

                                    $sdebit = "Select sum(amount) from dbo.translist where AccountDebet = '$ulrow[0]' and transactiontype = 'TOPP' and Date between '$from' and '$tglh' or AccountDebet = '$ulrow[0]' and transactiontype = 'PARK' and Date between '$from' and '$tglh' or  AccountDebet = '$ulrow[0]' and transactiontype = 'MBRS' and Date between '$from' and '$tglh' ";
                                    //echo $sdebit;
                                    $pdebit = sqlsrv_query($conn, $sdebit);
                                    while ($hdebit = sqlsrv_fetch_array( $pdebit, SQLSRV_FETCH_NUMERIC)){
                                    if ($hdebit != 0){
                                        $hsdebit = $hdebit[0];    
                                    } else {
                                        $hsdebit = 0;    
                                    }
                                    }
                                    

                                    $skredit = "Select sum(amount) from dbo.translist where AccountKredit = '$ulrow[0]' and transactiontype = 'TOPP' and Date between '$from' and '$tglh' or AccountKredit = '$ulrow[0]' and transactiontype = 'PARK' and Date between '$from' and '$tglh' or  AccountKredit = '$ulrow[0]' and transactiontype = 'MBRS' and Date between '$from' and '$tglh'";
                                    //echo $skredit;
                                    $pkredit = sqlsrv_query($conn, $skredit);
                                    while ($hkredit = sqlsrv_fetch_array( $pkredit, SQLSRV_FETCH_NUMERIC)){
                                    if ($hkredit != 0){
                                        $hskredit = $hkredit[0];
                                    } else {
                                        $hskredit = 0;    
                                    }
                                    }

                                    $saldo = ($hsnilaik - $hsnilaid) - $hsdebit + $hskredit;
                                } 

                               ?>

                            <tr>
                                <td><?php echo $ulrow[7]; ?></td>
                                <td><a href="rep_tcardlist.php?pagem=<?php echo $_GET['page'];?>&lok=<?php echo $ulrow[0]; ?>&tgl1=<?php echo $_GET['tgl1']; ?>&tgl2=<?php echo $_GET['tgl2']; ?>"><?php echo $ulrow[0]; ?></a></td>
                                <td style="text-align: right;"><?php echo number_format($hsdebit,2); ?></td>
                                <td style="text-align: right;"><?php echo number_format($hskredit,2); ?></td>
                                <td style="text-align: right;"><?php echo number_format($saldo,2); ?></td>
                            </tr>
                            <?php } ?>   
                            
                            <?php
                            $jmlhalaman = ceil($jmlulrow[0]/$perpages);
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                    <div class="box-footer clearfix right">
                        <div style="text-align: center;">
                            Showing <?=$halaman?> to <?=$jmlhalaman?> of <?=$jmlulrow[0]?> entries <?=$posisi." - ".$batas?>
                            <?php
                            $tgl1 = $_GET['tgl1'];
                            $tgl2 = $_GET['tgl2'];
                            $reload = "rep_cardlist.php?tgl1=$tgl1&tgl2=$tgl2";
                            $page = intval($_GET["page"]);
                            $adjacents = isset($_GET['adjacents']) ? intval($_GET['adjacents']) : 2; // khusus style pagination 2 dan 3
                            if( $page == 0 ) $page = 1;

                            $tpages = ($jmlhalaman) ? ceil($jmlhalaman) : 0; // total pages, last page number
                            echo "<div>".paginate($reload, $page, $tpages)."</div>";
                            ?>
                        </div>
                    </div>           
              
            <!-- div dekat tanggal -->
            </div>

            </div>
        <?php } ?>
    </div>
</div>


<?php
function paginate($reload, $page, $tpages) {

    $firstlabel = "First";
    $prevlabel  = "Prev";
    $nextlabel  = "Next";
    $lastlabel  = "Last";

    $out = "<div class=\"backpaging\"><ul class=\"ownpaging\">\n";

    // first
    if($page>1) {
        $out.= "<li><a href=\"" . $reload . "\">" . $firstlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $firstlabel . "</a></li>\n";
    }

    // previous
    if($page==1) {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $prevlabel . "</a></li>\n";
    }
    elseif($page==2) {
        $out.= "<li><a href=\"" . $reload . "\">" . $prevlabel . "</a></li>\n";
    }
    else {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" . ($page-1) . "\">" . $prevlabel . "</a></li>\n";
    }

    // current
    $out.= "<li class=\"active\"><a href=\"#\">Page " . $page . " of " . $tpages . "</a></li>\n";

    // next
    if($page<$tpages) {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" .($page+1) . "\">" . $nextlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $nextlabel . "</a></li>\n";
    }

    // last
    if($page<$tpages) {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" . $tpages . "\">" . $lastlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $lastlabel . "</a></li>\n";
    }

    $out.= "</ul></div>";

    return $out;
}
?>


<script type="text/javascript">
    var tampung = [];

     $('.btn-addu').click(function(){
       var index = $('#detailUser tr').length;

        var user = $('#user option:selected').val();
        var amount = $('#amount').val();
        var date = $('#date').val();

        if(date == ''){
          alert('Assign date tidak boleh kosong');
          return false;
        }
        else if(user == '' || amount == '' || amount <= 0){
            alert('Harap isi inputan dengan benar');
            return false;
        }
        else if(jQuery.inArray(user, tampung) !== -1){
            alert('Tidak dapat memilih user yang sudah ditambahkan sebelumnya');
            return false;
        }
        else{
            $.ajax({
                url : "ajax_addteller.php",
                type : 'POST',
                dataType : 'json',
                data: { index: index, user: user, amount: amount, date: date},   // data POST yang akan dikirim
                success : function(data) {
                    if(data.status == 1){
                        tampung.push(data.id);
                        $("#detailUser").append("<tr>" +
                            "<td><input type='hidden' name='user["+data.index+"]' value="+ data.id +">" + data.nama + "</td>" +
                            "<td><input type='hidden' name='amount["+data.index+"]' value="+ data.amount +">" + data.amount +
                            "<td><a class='btn btn-default btn-flat btn-sm text-red remove' onclick='$(this).parent().parent().remove();'><i class='ion-android-delete'></i></a></td>" +
                            "</tr>");

                            $('#user').val('');
                            $('#amount').val('');
                    }
                    else{
                        alert(data.message);
                        return false;
                    }
                },
                error : function() {
                    alert('Telah terjadi error.');
                    return false;
                }
            });
        }
    });
</script>

<?php require('footer.php'); ?>
