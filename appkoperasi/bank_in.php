<?php
require('header.php');
require('sidebar-right.php');
require('sidebar-left.php');
require('content-header.php');
?>

<div class="animated fadeinup delay-1">
    <div class="page-content">

            <h2 class="uppercase"><?php echo lang('Penerimaan Bank'); ?></h2>

                  <?php if(isset($_SESSION['error-type']) and isset($_SESSION['error-message']) and isset($_SESSION['error-time'])){ ?>
                      <?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
                          <div class="notification notification-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
                              <h4> <?php echo ucfirst($_SESSION['error-type']); ?> </h4>
                              <?php echo $_SESSION['error-message']; ?>
                          </div>
                      <?php } ?>
                  <?php } ?>

                <form method="post" action="procbank_in.php">

                    <div class="input-field">
                      <label for="note">Tanggal</label>
                        <input type="text" name="tgl" id="tgl" class="validate datepicker" autocomplete="off" required>
                    </div> <br>


                    <div class="input-field">
                        <input type="text" name="note" id="note" class="validate">
                        <label for="note">Note:</label>
                    </div> <br>

                    Bank <br>
                    <select name="kba" id="kba" class="browser-default">
                        <?php
                        $sql = 'Select * from dbo.BankAccount order by AccName ASC';
                        $query = sqlsrv_query($conn, $sql);
                        while($row  = sqlsrv_fetch_array( $query, SQLSRV_FETCH_NUMERIC)) { ?>
                            <option value="<?php echo $row[1] ;?>"> <?php echo $row[3]; ?> - <?php echo $row[4]; ?> </option>
                            <?php
                        }
                        ?>
                    </select>

                    <div class="input-field">
                        <input type="number" name="jumlah" id="jumlah" class="validate" required>
                        <label for="jumlah">Rp.</label>
                    </div>

                    <div class="input-field">
                        <input type="text" name="ket" id="ket" class="validate">
                        <label for="ket">Keterangan:</label>
                    </div>

                    <div class="input-field">
                        <input type="text" name="refnumber" id="refnumber" class="validate">
                        <label for="refnumber">Ref. Number</label>
                    </div> <br>

                    <div class="row">
                        <div class="col s12 m-t-20">
                            <div class="table-responsive">
                                <table class="table" id="table_akuntansi">
                                    <thead>
                                        <tr>
                                            <th class="center">
                                                List Items
                                            </th>

                                            <th class="center">
                                                Jumlah (Rp)
                                            </th>

                                            <th>

                                            </th>
                                        </tr>

                                        <tr>
                                            <td>
                                                <select name="item" id="item" class="browser-default">
                                                    <?php
                                                    $sql = 'Select * from dbo.AccountMax order by KodeAccount asc';
                                                    $query = sqlsrv_query($conn, $sql);
                                                    while($row  = sqlsrv_fetch_array( $query, SQLSRV_FETCH_NUMERIC)) { ?>
                                                        <option value="<?php echo $row[0] ;?>"> <?php echo $row[0]; ?> - <?php echo $row[1]; ?> </option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </td>

                                            <td>
                                                <input type="number" name="numbjumlah" id="numbjumlah" class="validate" style="text-align: right;">
                                            </td>

                                            <td>
                                                <button type="button" name="btn_add" id="btn_add" class="btn btn-large width-100 primary-color">Add</button>
                                            </td>
                                        </tr>
                                    </thead>

                                    <tbody class="detail">

                                    </tbody>

                                    <tfoot class="resdetail">
                                        <tr>
                                            <td>
                                                <span class="m-t-20 right"><b>Total Rp. </b></span>
                                            </td>
                                            <td>
                                                <b><input type="text" name="txt_total" id="txt_total" class="validate" style="text-align: right;" readonly></b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="m-t-20 right"><b>Selisih Rp. </b></span>
                                            </td>
                                            <td>
                                                <b><input type="text" name="txt_selisih" id="txt_selisih" class="validate" style="text-align: right;" readonly></b>
                                            </td>
                                        </tr>
                                    </tfoot>

                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="m-t-30">
                        <button type="submit" name="save" id="save" class="btn btn-large primary-color width-100 waves-effect waves-light" disabled>Simpan</button>
                    </div>
                </form>

                <?php

                ?>

            </div>

    </div>
</div>

<script>
    var jumlah;
    var totnumbjumlah = 0;
    var selisih;
    $('#save').prop('disabled', true);

    $('#jumlah').keyup(function() {
        jumlah = $('#jumlah').val();

        selisih = jumlah - totnumbjumlah;
        $('#txt_selisih').val(selisih);
    });



    $('#btn_add').click(function () {
        var index = $('.detail tr').length;

        var item = $('#item option:selected').val();
        var numbjumlah = $('#numbjumlah').val();
        var jumlah = $('#jumlah').val();

        if( jumlah == '' || jumlah <= 0){
            alert('Harap isi jumlah dengan benar');
            $('#jumlah').focus();
            return false;
        }
        else if( numbjumlah == '' ){
            alert('Harap isi inputan dengan benar');
            return false;
        }
        else{
            $.ajax({
                url : "ajax_addjurnalkasir.php",
                type : 'POST',
                data: {
                    index: index,
                    item: item,
                    numbjumlah: numbjumlah
                },   // data POST yang akan dikirim
                success : function(data) {
                    $("#table_akuntansi tbody").append(data);

                    totnumbjumlah = totnumbjumlah + parseFloat(numbjumlah);
                    selisih = jumlah - totnumbjumlah;

                    $('#txt_total').val(totnumbjumlah);
                    $('#txt_selisih').val(selisih);

                    if( selisih == 0 ){
                        $('#save').prop('disabled', false);
                    }
                    else {
                        $('#save').prop('disabled', true);
                    }
                },
                error : function() {
                    alert('Telah terjadi error.');
                    return false;
                }
            });
        }
    });
</script>

<?php
include('footer.php');
?>
