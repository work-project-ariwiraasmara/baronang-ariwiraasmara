<?php
session_start();
error_reporting(0);

include "connect.php";

$page = $_GET['page'];
$edit = $_GET['edit'];
$delete	= $_GET['delete'];

$id		= $_POST['id'];
$level	= $_POST['levelname'];

if(!empty($delete)){
		
	$dpultsql = "delete from [dbo].[UserLevel] where KodeUserLevel='$delete'";
	$dpulstmt = sqlsrv_query($conn, $dpultsql);
	if($dpulstmt){
		$deupultsql = "delete from [dbo].[UserLevelMenu] where KodeUserLevel='$delete'";
		$deupulstmt = sqlsrv_query($conn, $deupultsql);
		if($deupulstmt){
            messageAlert('Berhasil menghapus data dari database','success');
            header('Location: muserlevel.php');
		}
		else{
            messageAlert('Gagal menghapus data dari database','danger');
            header('Location: muserlevel.php');
		}
	}
	else{
        messageAlert('Gagal menghapus data dari database','danger');
        header('Location: muserlevel.php');
	}
}
else{
	if($id == "" || $level == ""){
        messageAlert('Harap isi seluruh kolom','info');
        header('Location: muserlevel.php');
	}
	else{
		$ulsql 	= "select * from [dbo].[UserLevel] where KodeUserLevel='$id'";
		//echo $ulsql;
		$ulstmt = sqlsrv_query($conn, $ulsql);
		$ulrow 	= sqlsrv_fetch_array( $ulstmt, SQLSRV_FETCH_NUMERIC);

        if(!empty($edit)){
			if(count($ulrow[0] > 0)){
				$upultsql = "update [dbo].[UserLevel] set NamaUserLevel='$level' where KodeUserLevel='$edit'";
				$upulstmt = sqlsrv_query($conn, $upultsql);
				if($upulstmt){
					$dupultsql = "delete from [dbo].[UserLevelMenu] where KodeUserLevel='$edit'";
					$dupulstmt = sqlsrv_query($conn, $dupultsql);
					if($dupulstmt){
						$jmlmenuArray	= count($_POST["menu"]);

						for($i=0; $i < $jmlmenuArray; $i++){
							$menuArray	= $_POST["menu"][$i];
							$menuApprove= $_POST["level"][$i];

                            $statusmenu="0";
							if($menuArray!='')
							{
								$statusmenu="1";
							}

                            $statusapprove = 0;
                            if($menuApprove>0)
                            {
                                $statusapprove="1";
                            }

                            $darraypultsql 	= "exec [dbo].[ProsesUserLevelMenu] '$edit', '$menuArray', '$statusmenu', '$statusapprove'";
                            //echo $darraypultsql;
                            $darraypulstmt 	= sqlsrv_query($conn, $darraypultsql);
                        }

                        messageAlert('Berhasil menyimpan ke database','success');
                        header('Location: muserlevel.php');
					}
					else{
                        messageAlert('Gagal menghapus data dari database','danger');
                        header('Location: muserlevel.php');
					}
				}
				else{
                    messageAlert('Gagal memperbaharui data ke database','danger');
                    header('Location: muserlevel.php');
				}
			}
			else{
                header('Location: muserlevel.php');
			}
		}
		else{
			if(empty($ulrow[0])){
				$pultsql = "exec [dbo].[ProsesUserLevel] '$id', '$level'";
				$pulstmt = sqlsrv_query($conn, $pultsql);

				if($pulstmt){
					$jmlmenuArray	= count($_POST["menu"]);

					for($i=0; $i < $jmlmenuArray; $i++){
							$menuArray	= $_POST["menu"][$i];
							$menuApprove= $_POST["level"][$i];

                            $statusmenu="0";
							if($menuArray!='')
							{
								$statusmenu="1";
							}

                            $statusapprove = 0;
                            if($menuApprove>0)
                            {
                                $statusapprove="1";
                            }

					// for($i=0; $i < $jmlmenuArray; $i++){
					// 	$menuArray	= $_POST["menu"][$i];
					// 	$menuApprove= $_POST["level"][$i];

					// 	if($menuArray>0)
					// 	{
					// 		$statusmenu="1";
					// 	}
					// 	else
					// 	{
					// 		$statusmenu="0";
					// 	}

     //                    $statusapprove = 0;
     //                    if($menuApprove>0)
     //                    {
     //                        $statusapprove="1";
     //                    }

                        $arraypultsql 	= "exec [dbo].[ProsesUserLevelMenu] '$id', '$menuArray', '$statusmenu', '$statusapprove'";
                        //echo $arraypultsql;
						$arraypulstmt 	= sqlsrv_query($conn, $arraypultsql);
					}

                    messageAlert('Berhasil menyimpan ke database','success');
                    header('Location: muserlevel.php');
				}
				else{
                    messageAlert('Gagal menyimpan ke database','danger');
                    header('Location: muserlevel.php');
				}
			}
			else{
                messageAlert('Level ID sudah ada','info');
                header('Location: muserlevel.php');
			}
		}
	} 
} 
?>