<?php
session_start();
error_reporting(0);

include "connect.php";

$page			= $_GET['page'];
$edit			= $_GET['edit'];
$delete			= $_GET['delete'];

$kod	= $_POST['kod'];
$loan	= $_POST['loan'];
$int	= $_POST['int'];
$int_r	= $_POST['int_r'];
$min	= $_POST['min'];
$max	= $_POST['max'];
$mat	= $_POST['mat'];
$con	= $_POST['con'];
$pp		= $_POST['pp'];
$ip		= $_POST['ip'];
$kba	= $_POST['kba'];
$tempo  = $_POST['tempohari'];
$minapp = $_POST['minapp'];
$status = $_POST['status'];

if(!isset($_POST['penjamin'])){
    $penjamin = 0;
    $jmlpenjamin = 0;
}
else{
    $penjamin = $_POST['penjamin'];
    $jmlpenjamin = $_POST['jmlpenjamin'];
}

if(!isset($_POST['dokumen'])){
    $dokumen = 0;
}
else{
    $dokumen = $_POST['dokumen'];
}

if(!isset($_POST['over'])){
    $over = 0;
}
else{
    $over = $_POST['over'];
}

if(!isset($_POST['ipbs'])){
    $ipbs = 0;
}
else{
    $ipbs = $_POST['ipbs'];

}

$a = "select [dbo].[getKodeLoanType]('$_SESSION[KID]')";
$b = sqlsrv_query($conn, $a);
$c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC);
$kodl = $c[0];

//delete doc
if(isset($_GET['n']) and isset($_GET['kod'])){
    $a = "delete [dbo].[SetingDocGeneralSeting] where Name='$_GET[n]' and LoanType='$_GET[kod]'";
    $b = sqlsrv_query($conn, $a);
    if($b){
        messageAlert(lang('Berhasil menghapus dokumen'),'success');
        header('Location: loan_type.php?edit=$_GET[kod]');
    }
}

//delete user
if(isset($_GET['u']) and isset($_GET['kod'])){
    $a = "delete [dbo].[UserApprovalLoan] where UserID='$_GET[u]' and LoanType='$_GET[kod]'";
    $b = sqlsrv_query($conn, $a);
    if($b){
        messageAlert(lang('Berhasil menghapus pengguna'),'success');
        header('Location: loan_type.php?edit=$_GET[kod]');
    }
}

if($loan == '' || $int	== '' || $int_r	== '' || $min == '' ||  $tempo == '' || $max == '' || $mat == '' ||
    $con == '' || $pp == '' || $ip == '' || $kba == '' || $status == ''){
        messageAlert(lang('Harap isi seluruh kolom'),'info');
        header('Location: loan_type.php');
	}
	else{
        if(!empty($edit)){
            //save doc
            $dno = 0;
            foreach($_POST['nama'] as $nama){
                $nama = $_POST['nama'][$dno];
                $ket = $_POST['ket'][$dno];

                $a = "exec [dbo].[ProsesSetingDocGeneralSeting] '$_SESSION[KID]','$nama','$ket','$kod'";
                $b = sqlsrv_query($conn, $a);

                $dno++;
            }

            //save user
            $uno = 0;
            foreach ($_POST["user"] as $user) {
                $usr = $_POST['user'][$uno];

                $aa = "exec [dbo].[ProsesUserApprovalLoan] '$usr','$_SESSION[KID]','$kod'";
                $bb = sqlsrv_query($conn, $aa);

                $uno++;
            }

            $upbltsql = "exec [dbo].[ProsesLoanType] '$kod','$loan','$int','$int_r','$min','$max','$mat','$con','$pp','$ip','$kba',$tempo,$penjamin,$jmlpenjamin,$dokumen,$over,$minapp,$status,$ipbs";
            $upblstmt = sqlsrv_query($conn, $upbltsql);
            if($upblstmt){
                messageAlert(lang('Berhasil memperbaharui data ke database'),'success');
                header('Location: loan_type.php');
            }
            else{
                messageAlert(lang('Gagal memperbaharui data ke database'),'danger');
                header('Location: loan_type.php');
            }
		}
		else{
            $blsql = "select * from [dbo].[LoanType]  where KodeLoanType='$kodl'";
            $blstmt = sqlsrv_query($conn, $blsql);
            $blrow = sqlsrv_fetch_array( $blstmt, SQLSRV_FETCH_NUMERIC);

            if(empty($blrow[0])){
                //save doc
                $dno = 0;
                foreach ($_POST["nama"] as $error) {
                    $nama = $_POST['nama'][$dno];
                    $ket = $_POST['ket'][$dno];

                    $a = "exec [dbo].[ProsesSetingDocGeneralSeting] '$_SESSION[KID]','$nama','$ket','$kodl'";
                    $b = sqlsrv_query($conn, $a);

                    $dno++;
                }

                //save user
                $uno = 0;
                foreach ($_POST["user"] as $error) {
                    $usr = $_POST['user'][$uno];

                    $aa = "exec [dbo].[ProsesUserApprovalLoan] '$usr','$_SESSION[KID]','$kodl'";
                    $bb = sqlsrv_query($conn, $aa);

                    $uno++;
                }

                $spbltsql = "exec [dbo].[ProsesLoanType] '$kodl','$loan','$int','$int_r','$min','$max','$mat','$con','$pp','$ip','$kba',$tempo,$penjamin,$jmlpenjamin,$dokumen,$over,$minapp,$status,$ipbs";
                $spblstmt = sqlsrv_query($conn, $spbltsql);

                if($spblstmt){
                    messageAlert(lang('Berhasil menyimpan ke database'),'success');
                    header('Location: loan_type.php');
                }
                else{
                    messageAlert(lang('Gagal menyimpan ke database'),'danger');
                    header('Location: loan_type.php');
                }
            }
            else{
                messageAlert(lang('Kode Bank sudah ada'),'warning');
                header('Location: loan_type.php');
            }
		}
	}
?>