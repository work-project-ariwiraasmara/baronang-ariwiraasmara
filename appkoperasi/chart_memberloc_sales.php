<?php
require('header.php');
require('sidebar-left.php');
require('sidebar-right.php');
require('content-header.php');
?>

    <div class="animated fadeinup delay-1">
        <div class="page-content txt-black">
            
            <?php
				$page = @$_GET['bulan'];
				
				$json_label = array();
                $json_data1 = array();
                $json_data2 = array();
                $json_data3 = array();
                //$json_data4 = array();
                if (empty($page)) {
					$month = date('m');
					$dmn = date('t M');
					$yn = date('Y');
					$bulan=$dmn;
					$tahun=$yn;
				
			}else{
				$bulan = @$_GET['bulan'];
                $tahun = @$_GET['tahun'];
				$month = $_GET['bln'];
				$yn = $tahun;
				$dmn = $bulan;
				
				
			}
                $bulan_int = date('t', strtotime($bulan));
                $tahun_int = date('Y', strtotime($tahun));
			
                ?>
                
                <div id="container" style="width: 100% ; height: 500px">
					<?php
					
						$title = '<b>Member Sales<br>'.date('F', strtotime($bulan)).' '.date('Y', strtotime($tahun)).'</b>';
					
						$sql_lokasi = "Select LocationID,Nama from [dbo].[LocationMerchant] where Status='1' order by LocationID";
						$queryLoc = sqlsrv_query($conn, $sql_lokasi);
						
						//$dataLoc = sqlsrv_fetch_array($queryLoc);
						while($dataLoc = sqlsrv_fetch_array($queryLoc)) {
							    array_push($json_label, '<a href="chart_member_sales.php?chart=bulan&bulan='.$dmn.'&tahun='.$yn.'&loc='.$dataLoc[0].'" target="_self">'.$dataLoc[1].'</a>');
                                
                                $sql_line1 = "SELECT SUM(Amount) from [dbo].[ParkMemberSales] where LocationID='$dataLoc[0]' and VehicleID='700097VEHICLE001' and Month(Tanggal) = '$month' and YEAR(Tanggal) = '$yn'";
								$query1 = sqlsrv_query($conn, $sql_line1);
								while($data1 = sqlsrv_fetch_array($query1)) {
									array_push($json_data1, $data1[0]); }
									
								$sql_line2 = "SELECT SUM(Amount) from [dbo].[ParkMemberSales] where LocationID='$dataLoc[0]' and VehicleID='700097VEHICLE002' and Month(Tanggal) = '$month' and YEAR(Tanggal) = '$yn'";
								$query2 = sqlsrv_query($conn, $sql_line2);
								while($data2 = sqlsrv_fetch_array($query2)) {
									array_push($json_data2, $data2[0]); }
							
							
								$sql_line3 = "SELECT SUM(Amount) from [dbo].[ParkMemberSales] where LocationID='$dataLoc[0]' and Month(Tanggal) = '$month' and YEAR(Tanggal) = '$yn'";
								$query3 = sqlsrv_query($conn, $sql_line3);
								while($data3 = sqlsrv_fetch_array($query3)) {
									array_push($json_data3, $data3[0]); }
								
                            }
					
                    ?>
				
                    <div class="m-t-10" style="margin-bottom: 50px;">
                            <?php
                            //$dmn = date('t M'); 
                            //$yn = date('Y');
                            ?>

                            <?php 
                            if( !(($bulan == '31 Jan') && ($tahun == '2019')) ) { ?>
                                <a href="chart_memberloc_sales.php?bulan=<?php echo date('t M', strtotime($yn.'-'.$month.'-01'.'-1 month')); ?>&tahun=<?php echo $yn; ?>&bln=<?php echo date('m', strtotime($yn.'-'.$month.'-01'.'-1 month')); ?>" target="_self" class="btn btn-large left primary-color waves-effect waves-light" style="color: #ffffff;">Prev</a>
                            <?php 
                            }
                            ?>

                            <?php
                            if( !(($dmn == date('t M')) && ($yn == date('Y')) )) { ?>
                                <a href="chart_memberloc_sales.php?bulan=<?php echo date('t M', strtotime($yn.'-'.$month.'-01'.'+1 month')); ?>&tahun=<?php echo $yn; ?>&bln=<?php echo date('m', strtotime($yn.'-'.$month.'-01'.'+1 month')); ?>" target="_self" class="btn btn-large right primary-color waves-effect waves-light txt-white" style="color: #ffffff;">Next</a>
                                <?php
                            }
						?>
                        </div>
			</div>
         </div>           
    </div>

    <script src="js/chart.min.js"></script>
    <script src="js/plotly-latest.min.js"></script>
    <script type="text/javascript">
        $('#btn_cj').click(function() {
            var jam = $('#chart_jam :selected').val();
            var url = "chart_member_sales.php?chart=hari&tgl=<?php echo @$_GET['tgl']; ?>&bulan=<?php echo $bulan; ?>&tahun=<?php echo $tahun; ?>" + "&jam=" + jam + " target='_self'";

            window.location.href = url;
        });

        var trace1 = {
            x: <?php echo json_encode($json_label); ?>, 
            y: <?php echo json_encode($json_data1); ?>,
            type: 'bar',
            name: 'Motor',
            marker: {
                color: '#f0ff00',
                opacity: 1
            }
        };

        var trace2 = {
            x: <?php echo json_encode($json_label); ?>,
            y: <?php echo json_encode($json_data2); ?>,
            type: 'bar',
            name: 'Mobil',
            marker: {
                color: '#0000ff',
                opacity: 1
            }
        };

        var trace3 = {
            x: <?php echo json_encode($json_label); ?>,
            y: <?php echo json_encode($json_data3); ?>,
            type: 'bar',
            name: 'Total',
            marker: {
                color: '#555555',
                opacity: 1
            }
        };
        
        
        
        var data = [trace1, trace2, trace3];

        var layout = {
            autosize: false,
            width: 1400,
            height: 500,
            title: '<?php echo $title; ?>',
            xaxis: { type: 'category', fixedrange: true },
            yaxis: { fixedrange: true },
            barmode: 'group'
        };

        Plotly.newPlot('container', data, layout, {displaylogo: false});
    </script>
    
<?php
require('footer.php');
?>
