<?php require('header.php');?>

<?php require('sidebar-right.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<div class="animated fadeinup delay-1">
    <div class="page-content">

        <?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
            <div class="notification notification-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
                <!--<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> <i class="icon fa fa-warning ?>"></i> -->
                <h4> <?php echo ucfirst($_SESSION['error-type']); ?> </h4>
                <?php echo $_SESSION['error-message']; ?>
            </div>
        <?php } ?>

        <h2 class="uppercase">Change Password</h2>

        <div class="form-inputs">

            <form class="form-horizontal" action="acchange_pass.php" method="POST">
                <div class="input-field">
                    <input type="password" class="validate" name="oldpass" id="oldpass">
                    <label for="oldpass">Old Password</label>
                </div>

                <div class="input-field">
                    <input type="password" class="validate" name="pass" id="pass">
                    <label for="pass">New Password</label>
                </div>

                <div class="input-field">
                    <input type="password" class="validate" name="repass" id="repass">
                    <label for="repass">Retype Password</label>
                </div>

                <button type="submit" class="waves-effect waves-light btn-large primary-color width-100">Change</button>
            </form>
        </div>

    </div>
</div>

<?php require('footer_new.php');?>