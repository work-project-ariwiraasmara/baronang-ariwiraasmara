<?php
// locparking   
require('header.php');
require('sidebar-right.php');
require('sidebar-left.php');

$kid = $_SESSION['KID'];

// GET LAST ID LocationMerchant
$sqlloc = 'SELECT TOP 1 LocationID FROM [dbo].[LocationMerchant] ORDER BY LocationID DESC';
$query = sqlsrv_query($conn, $sqlloc);
$dataloc = sqlsrv_fetch_array($query, SQLSRV_FETCH_NUMERIC);
//echo $dataloc[0].'<br>';
$intaidloc = 0;
$graidloc = substr($dataloc[0], -7);
$gploc = (int)$graidloc + 1;
$intaidloc = str_pad($gploc, 7, '0', STR_PAD_LEFT);

// GET LAST ID LocationQuota
$sqlvh = 'SELECT TOP 1 VehicleID FROM [dbo].[LocationQuota] ORDER BY VehicleID DESC';
$query = sqlsrv_query($conn, $sqlvh);
$datavh = sqlsrv_fetch_array($query, SQLSRV_FETCH_NUMERIC);
//echo $dataloc[0].'<br>';
$intaidvh = 0;
$graidvh = substr($datavh[0], -3);
$gpvh = (int)$graidvh + 1;
$intaidvh = str_pad($gpvh, 3, '0', STR_PAD_LEFT);

if(@$_GET['locid']) {
	$btn_text = 'Update';
	$btn_name_id = 'btnupdate';

	$sql1 = "SELECT * from [KOPX700097].[dbo].[LocationMerchant] where LocationID='".@$_GET['locid']."'";
	$query1 = sqlsrv_query($conn, $sql1);
	$data1 = sqlsrv_fetch_array($query1, SQLSRV_FETCH_NUMERIC);

	$sql2 = "SELECT * from [KOPX700097].[dbo].[LocationQuota] where LocationID='".@$_GET['locid']."'";
	$query2 = sqlsrv_query($conn, $sql2);
	$data2= sqlsrv_fetch_array($query2, SQLSRV_FETCH_NUMERIC);
}
else {
	$btn_text = 'Simpan';
	$btn_name_id = 'btnok';
}
?>

	<div class="animated fadeinup delay-1">
		<div class="page-content txt-black">

			<h2 class="uppercase txt-black">Lokasi Parking</h2>
			
			<div class="form-inputs m-t-30">
				<form action="" method="post" class="m-t-30" enctype="multipart/form-data">
					<input type="hidden" name="locid" id="locid" value="<?php echo $kid.'LOC'.$intaidloc; ?>" readonly>
					<input type="hidden" name="vhclid" id="vhclid" value="<?php echo $kid.'VEHICLE'.$intaidvh; ?>" readonly>

					<input type="hidden" name="locid_ed" id="locid_ed" value="<?php echo $data1[0]; ?>" readonly>
					<input type="hidden" name="vhclid_ed" id="vhclid_ed" value="<?php echo $data2[1]; ?>" readonly>

					<div class="input-field">
						<input type="text" name="nama" id="nama" value="<?php if(@$_GET['locid']) echo $data1[2]; ?>">
						<label for="nama">Nama</label>
					</div>

					<div class="input-field">
						<input type="text" name="alamat" id="alamat" value="<?php if(@$_GET['locid']) echo $data1[3]; ?>">
						<label for="alamat">Alamat</label>
					</div>

					<div class="input-field">
						<input type="text" name="telp" id="telp" value="<?php if(@$_GET['locid']) echo $data1[4]; ?>">
						<label for="telp">No. Telepon</label>
					</div>

					<div class="input-field">
						<span>Status :</span> <br>
						<select name="status" id="status" class="browser-default">
							<option value="0" <?php if(@$_GET['locid']) { if($data1[5] == 0) echo 'selected'; } ?> >Tidak Aktif</option>
							<option value="1" <?php if(@$_GET['locid']) { if($data1[5] == 1) echo 'selected'; } ?> >Aktif</option>
						</select>
					</div>

					<div class="input-field">
						<input type="text" name="desk" id="desk" value="<?php if(@$_GET['locid']) echo $data1[8]; ?>">
						<label for="desk">Deskripsi</label>
					</div>

					<div class="input-field">
						<span>Gambar :</span> <br>
						<input type="file" name="gambar" id="gambar">
					</div>

					<div class="input-field">
						<input type="text" name="kode" id="kode" value="<?php if(@$_GET['locid']) echo $data1[10]; ?>">
						<label for="kode">Kode</label>
					</div>

					<div class="row">
						<div class="col s4">
							<div class="input-field">
								<span>Jenis Kendaraan</span>: <br>
								<select name="vhcl" id="vhcl" class="browser-default">
									<?php
									$sql = "SELECT * FROM [KOPX700097].[dbo].[VehicleType] ORDER BY Name ASC";
									$query = sqlsrv_query($conn, $sql);
									while($data = sqlsrv_fetch_array($query, SQLSRV_FETCH_NUMERIC)) { ?>
										<option value="<?php echo $data[0]; ?>"><?php echo $data[1]; ?></option>
										<?php
									}
									?>
								</select>
							</div>
						</div>

						<div class="col s4 m-t-20">
							<div class="input-field">
								<input type="text" name="quota" id="quota">
								<label for="quota">Quota</label>
							</div>
						</div>

						<div class="col s4">
							<div class="input-field">
								<button type="button" id="btnrowadd" class="btn btn-large primary-color width-100 waves-effect waves-light"><i class="ion-android-add"></i></button>
							</div>
						</div>
					</div>

					<div id="rowadd_parking">
						
					</div>

					<?php
					if(@$_GET['locid']) { 
						$sql3 = "SELECT * from [KOPX700097].[dbo].[LocationQuota] where LocationID='".@$_GET['locid']."'";
						$query3 = sqlsrv_query($conn, $sql3);
						$row = 0;
						while($data3 = sqlsrv_fetch_array($query3, SQLSRV_FETCH_NUMERIC)) { 
							$sql4 = "SELECT Name from [KOPX700097].[dbo].[VehicleType] where VehicleId='".$data3[1]."'";
							$query4 = sqlsrv_query($conn, $sql4);
							$data4 = sqlsrv_fetch_array($query4, SQLSRV_FETCH_NUMERIC);
							?>
							<div class="row" id="row-<?php echo $row; ?>">
								<div class="col s4">
									<input type="hidden" name="vhcl_add_isdel[]" id="vhcl_add_isdel" value="<?php echo $data3[1]; ?>">
									<input type="text" name="vhcl_name_isdel[]" id="vhcl_name_isdel" value="<?php echo $data4[0]; ?>" readonly>
								</div>
								<div class="col s4">
									<input type="text" name="qty_add_isdel[]" id="qty_add_isdel" value="<?php echo $data3[2]; ?>">
								</div>
								<div class="col s4">
									<span style="font-weight: bold;">Delete?</span><br>
									<input type="checkbox" name="isdelete[<?php echo $row; ?>]" id="isdelete-<?php echo $row; ?>" value="<?php echo $data3[1]; ?>">
									<label for="isdelete-<?php echo $row; ?>" id="txtdel-<?php echo $row; ?>">No</label>
								</div>
							</div>

							<script type="text/javascript">
								$('#isdelete-<?php echo $row; ?>').click(function(){
									if($("#isdelete-<?php echo $row; ?>").is(':checked')) {
                                        $('#txtdel-<?php echo $row; ?>').text('Yes');
                                    }
                                    else {
                                        $('#txtdel-<?php echo $row; ?>').text('No');
                                    } 
								});
							</script>
							<?php
							$row++;
						}
							
					}	
					?>

					<button type="submit" name="<?php echo $btn_name_id; ?>" id="<?php echo $btn_name_id; ?>" class="btn btn-large primary-color width-100 m-t-30"><?php echo $btn_text; ?></button>
				</form>
			</div>

			<?php
			$ok = @$_POST['btnok'];
			if(isset($ok)) {
				$locid 	= @$_POST['locid'];
				$nama 	= @$_POST['nama'];
				$alamat = @$_POST['alamat'];
				$telp 	= @$_POST['telp'];
				$status = @$_POST['status'];
				$desk 	= @$_POST['desk'];
				$kode 	= @$_POST['kode'];

				$src_gambar = 'appkoperasi/img/parking/';
				$tmp_gambar = @$_FILES['gambar']['tmp_name'];
				$gambar 	= basename(@$_FILES['gambar']['name']);
				$fg 		= $src_gambar.$gambar;
				

				$sql1 	= "INSERT into [KOPX700097].[dbo].[LocationMerchant](LocationID, Nama, Alamat, Telepon, Status, Deskripsi, Gambar, Code) values('$locid','$nama','$alamat','$telp','$status','$desk','$fg','$kode')";
				//echo $sql1;
				$query1 = sqlsrv_query($conn, $sql1);

				$rowadd = 0;
				foreach (@$_POST['vhcl_add'] as $key) {
					$vhclid = @$_POST['vhcl_add'][$rowadd];
					$quota 	= @$_POST['qty_add'][$rowadd];

					$sql2 = "INSERT into [KOPX700097].[dbo].[LocationQuota](LocationID, VehicleID, Quota) values('$locid','$vhclid','$quota')";
					$query2 = sqlsrv_query($conn, $sql2);
					$rowadd++;
				}
				
				?>
				<script type="text/javascript">
					alert('Simpan Berhasil');
					window.location.href = "locparking.php";
				</script>
				<?php
			}

			$update = @$_POST['btnupdate'];
			if(isset($update)) {
				$locid 	= @$_POST['locid_ed'];
				$nama 	= @$_POST['nama'];
				$alamat = @$_POST['alamat'];
				$telp 	= @$_POST['telp'];
				$status = @$_POST['status'];
				$desk 	= @$_POST['desk'];
				$kode 	= @$_POST['kode'];

				$src_gambar = 'appkoperasi/img/parking/';
				$tmp_gambar = @$_FILES['gambar']['tmp_name'];
				$gambar 	= basename(@$_FILES['gambar']['name']);
				$fg 		= $src_gambar.$gambar;


				$sql1 = "UPDATE [KOPX700097].[dbo].[LocationMerchant] set Nama='$nama', Alamat='$alamat',
																		  Telepon='$telp', Status='$status',
																		  Deskripsi='$desk', Code='$kode' where LocationID='$locid'";
				//echo 'SQL 1 : '.$sql1.'<br>';
				$query1 = sqlsrv_query($conn, $sql1);

				$noford = 0;
				if( isset($_POST['isdelete']) ) {
                	foreach (@$_POST['isdelete'] as $val) {
                    	//$chk_status = @$_POST['isdelete'][$noford];
                    	//echo $chk_status.'<br>';
                    	//$id = @$_POST['idpres'][$noford];
                    	$sqld = "Delete From [KOPX700097].[dbo].[LocationQuota] where VehicleID='$val' and LocationID='$locid'";
                    	//echo 'SQL Delete : '.$sqld.'<br>';
                    	$queryd = sqlsrv_query($conn, $sqld) or die(mysqli_error($conn));

                    	$noford++;
                	}
            	}

            	$nofordu = 0;
            	foreach (@$_POST['vhcl_add_isdel'] as $key) {
					$vhclid = @$_POST['vhcl_add_isdel'][$nofordu];
					$quota 	= @$_POST['qty_add_isdel'][$nofordu];

					$sql2 = "UPDATE [KOPX700097].[dbo].[LocationQuota] set Quota='$quota' where VehicleID='$vhclid' and LocationID='$locid'";
					//echo 'SQL UPDATE : '.$sql2.'<br>';
					$query2 = sqlsrv_query($conn, $sql2);
					$nofordu++;
				}

				$rowadd = 0;
				if(isset($_POST['vhcl_add'])) {
					foreach (@$_POST['vhcl_add'] as $key) {
						$vhclid = @$_POST['vhcl_add'][$rowadd];
						$quota 	= @$_POST['qty_add'][$rowadd];

						$sql3 = "INSERT into [KOPX700097].[dbo].[LocationQuota](LocationID, VehicleID, Quota) values('$locid','$vhclid','$quota')";
						//echo 'SQL 2 : '.$sql3.'<br>';
						$query3 = sqlsrv_query($conn, $sql3);
						$rowadd++;
					}
				}
				
				?>
				<script type="text/javascript">
					alert('Update Berhasil');
					//window.location.href = "locparking.php";
				</script>
				<?php
			}
			?>

			<div class="table-responsive m-t-30">
				<table class="table">
					<thead>
						<tr>
							<th>ID</th>
							<th>Nama</th>
							<th>Alamat</th>
							<th>No. Telp</th>
							<th>Status</th>
							<th>Deskripsi</th>
							<th></th>
							<?php /*
							<th>Kode</th>
							<th>Harga (Rp.)</th>
							<th>Kendaraan</th>
							<th>Quota</th>
							*/ ?>
						</tr>
					</thead>

					<tbody>
						<?php
						$sql = "SELECT * from [dbo].[LocationMerchant] order by LocationID ASC";
						$query = sqlsrv_query($conn, $sql);
						while($data = sqlsrv_fetch_array($query, SQLSRV_FETCH_NUMERIC)) { 
							/*
								<tr>
									<td><?php echo $data[0]; ?></td>
									<td><?php echo $data[2]; ?></td>
									<td><?php echo $data[3]; ?></td>
									<td><?php echo $data[4]; ?></td>

									<?php
									if($data[5] == 0) $status = 'Tidak Aktif';
									else $status = 'Aktif';
									?>
									<td><?php echo $status; ?></td>

									<td><?php echo $data[8]; ?></td>
									<td><?php echo $data[10]; ?></td>
									<td><?php echo $data[11]; ?></td>

									<td><?php //echo $data[11]; ?></td>

									<td><?php //echo $data[12]; ?></td>
								</tr>
								
							
							$sql2 = "SELECT * from [dbo].[LocationQuota] where LocationID='".$data[0]."'";
							$query2 = sqlsrv_query($conn, $sql2);
							while($data2 = sqlsrv_fetch_array($query2, SQLSRV_FETCH_NUMERIC)) { */ ?>
								<tr>
									<td><?php echo $data[0]; ?></td>
									<td><?php echo $data[2]; ?></td>
									<td><?php echo $data[3]; ?></td>
									<td><?php echo $data[4]; ?></td>

									<?php
									if($data[5] == 0) $status = 'Tidak Aktif';
									else $status = 'Aktif';
									?>
									<td><?php echo $status; ?></td>

									<td><?php echo $data[8]; ?></td>

									<td>
										<a href="locparking.php?locid=<?php echo $data[0]; ?>"><button type="button" class="btn btn-large primary-color waves-light waves-effect"><i class="ion-android-create"></i></button></a>
									</td>

									<?php /*
									<td><?php echo $data[10]; ?></td>
									<td style="text-align: right;"><?php echo number_format($data[11],2,',','.'); ?></td>


									
									$sql3 = "SELECT Name from [dbo].[VehicleType] where VehicleID='".$data2[1]."'";
	                                $query3 = sqlsrv_query($conn, $sql3);
	                                $data3 = sqlsrv_fetch_array($query3, SQLSRV_FETCH_NUMERIC);
									
									<td><?php echo $data3[0]; ?></td>

									<td><?php echo $data2[2]; ?></td>
									*/ ?>
									
								</tr>
								<?php
							//}
						}
						?>
					</tbody>
				</table>
            </div>

		</div>
	</div>

	<script type="text/javascript">
		var row_vhcl = 0;
		$('#btnrowadd').click(function() {
			var vhcl 	= $('#vhcl :selected').val();
			var qty 	= $('#quota').val();

			$.ajax({
                url : "ajax_locparking.php",
                type : 'POST',
                data: {
                    //item: item,
                    row: row_vhcl,
                    vhcl: vhcl,
                    qty: qty
                },   // data POST yang akan dikirim
                success : function(data) {
                    $("#rowadd_parking").append(data);
                    row_vhcl++;
                    console.log(row_vhcl);
                },
                error : function() {
                    alert('Telah terjadi error.');
                    return false;
                }
            });
		});
	</script>

<?php require('footer.php'); ?>