<?php require('blank-header.php');?>

  <?php
  session_start();

  if(!empty($_SESSION['KID']) && !empty($_SESSION['NamaKoperasi']) && !empty($_SESSION['Server']) && !empty($_SESSION['DatabaseName']) && !empty($_SESSION['UserDB']) && !empty($_SESSION['PassDB']) &&  !empty($_SESSION['UserID']) && !empty($_SESSION['Name']) && !empty($_SESSION['KodeJabatan'])){
    unset($_SESSION['KID']);
    unset($_SESSION['NamaKoperasi']);
    unset($_SESSION['Server']);
    unset($_SESSION['DatabaseName']);
    unset($_SESSION['UserDB']);
    unset($_SESSION['PassDB']);
    unset($_SESSION['Status']);
    unset($_SESSION['UserID']);
    unset($_SESSION['Name']);
    unset($_SESSION['KodeJabatan']);
    exit;
  }
  ?>   

    <div class="login-box">
      <div class="login-logo">
        <a href="" class="logo">
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><img src = "static/images/Logo_Black.png"></img></span>
        </a>
      </div><!-- /.login-logo -->
      <div class="login-box-body">
        <p class="login-box-msg">Register Administrator</p>
        <form action="procregisteradm.php" method="post">
          <!--<div class="form-group has-feedback">
            <input type="text" name="kid" class="form-control" placeholder="Cooperative ID">
            <span class="fa fa-address-card-o form-control-feedback" aria-hidden="true"></span>
          </div> -->
          <div class="form-group has-feedback">
            <input type="text" name="name" class="form-control" placeholder="Username">
            <span class="fa fa-envelope-o form-control-feedback" aria-hidden="true"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" name="password" class="form-control" placeholder="Password">
            <span class="fa fa-lock form-control-feedback" aria-hidden="true"></span>
          </div>
		  <div class="form-group has-feedback">
                  <label for="Position" class="col-sm-5 control-label" style="text-align: left;">Level Jabatan</label>
                    <select name="level" class="form-control">
                      <option>--- Select One ---</option>
                    <?php
                      $julsql   = "select * from [dbo].[JabatanAkses] order by KodeJabatan";  
                      $julstmt = sqlsrv_query($conn, $julsql);
                      while($julrow  = sqlsrv_fetch_array( $julstmt, SQLSRV_FETCH_NUMERIC)){
                    ?>
                      <option value="<?=$julrow[0];?>" <?php if($julrow[0] == $ul3){echo "selected";}?>><?=$julrow[1];?></option>
                    <?php
                      }
                    ?>
                </div>
          <div class="box-footer">
            <button type="submit" class="btn btn-flat btn-primary btn-block">Register</button>
          </div>
        </form>
      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

<?php require('blank-footer.php');?>   
