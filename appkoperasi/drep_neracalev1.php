<?php
session_start();
error_reporting(0);

include('lib/phpExcel/Classes/PHPExcel.php');
include "connect.php";


    $filePath = "uploads/excel/";

    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getProperties()->setCreator("baronang");
    $objPHPExcel->getProperties()->setTitle("Drep Neraca History");

// set autowidth
    for($col = 'A'; $col !== 'Z'; $col++) {
        $objPHPExcel->getActiveSheet()
            ->getColumnDimension($col)
            ->setAutoSize(true);
    }

    $listCombo = array();

    //sheet 2
    $objWorkSheet = $objPHPExcel->createSheet(0);
// baris judul
   
    $objWorkSheet->SetCellValue('A1', 'Kode Akun');
    $objWorkSheet->SetCellValue('B1', 'Nama Akun');
    $objWorkSheet->SetCellValue('C1', 'Type ID');
    $objWorkSheet->SetCellValue('D1', 'Nama ID');
    $objWorkSheet->SetCellValue('E1', 'Level');
    $objWorkSheet->SetCellValue('F1', 'Group ID');
    $objWorkSheet->SetCellValue('G1', 'Nama Group');
    $objWorkSheet->SetCellValue('H1', 'Nilai');
    $objWorkSheet->SetCellValue('I1', 'Bulan');
    $objWorkSheet->SetCellValue('J1', 'Tahun');    



    if($_GET['level'] == 1){

    $no = 1;
    $row = 2;
    $name = $_SESSION[Name];
    $aa = "select * from dbo.LaporanNeracaViewNew where header = '$_GET[level]' and UserName = '$name' ORDER BY KodeAccount Asc";
    //var_dump ($aa);

        $bb = sqlsrv_query($conn, $aa);
        while($cc = sqlsrv_fetch_array($bb, SQLSRV_FETCH_NUMERIC)){
        //var_dump($cc);
                    
            
                $objWorkSheet->SetCellValueExplicit("A".$row,$cc[0]);
                $objWorkSheet->SetCellValue("B".$row,$cc[1]);
                $objWorkSheet->SetCellValueExplicit("C".$row, $cc[2]);
                $objWorkSheet->SetCellValue("D".$row,$cc[3]);
                $objWorkSheet->SetCellValue("E".$row,$cc[4]);
                $objWorkSheet->SetCellValueExplicit("F".$row,$cc[5]);
                $objWorkSheet->SetCellValue("G".$row,$cc[6]);
                $objWorkSheet->SetCellValue("H".$row,$cc[8]);
                $objWorkSheet->SetCellValue("I".$row,$cc[9]);
                $objWorkSheet->SetCellValue("J".$row,$cc[10]);
            
                $no++;
                $row++;
            }
    }
//exit;
    $objWorkSheet->setTitle('Drep Laba rugi');

    $fileName = 'Labarugi'.'_'.strtotime(date('Y-m-d H:i:s')).'.xls';
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');

// download ke client
    header('Content-type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename="'.$fileName.'"');
    $objWriter->save('php://output');

    return $filePath.'/'.$fileName;

?>
