<?php
include "connect.php";
$row		= @$_POST['row'];
$vehicle 	= @$_POST['vhcl'];
$qty 		= @$_POST['qty'];

$sql = "SELECT Name FROM [KOPX700097].[dbo].[VehicleType] where VehicleId='$vehicle'";
$query = sqlsrv_query($conn, $sql);
$data = sqlsrv_fetch_array($query, SQLSRV_FETCH_NUMERIC);
?>

<div class="row" id="row-<?php echo $row; ?>">
	<div class="col s4">
		<input type="hidden" name="vhcl_add[]" id="vhcl_add" value="<?php echo $vehicle; ?>">
		<input type="text" name="vhcl_name" id="vhcl_name" value="<?php echo $data[0]; ?>" readonly>
	</div>
	<div class="col s4">
		<input type="text" name="qty_add[]" id="qty_add" value="<?php echo $qty; ?>" readonly>
	</div>
	<div class="col s4">
		<button type="button" id="btndel_vehicle-<?php echo $row; ?>" class="btn btn-large width-100 primary-color waves-effect waves-light"><i class="ion-android-delete"></i></button>
	</div>

	<script type="text/javascript">
		$('#btndel_vehicle-<?php echo $row; ?>').click(function(){
			$('#row-<?php echo $row; ?>').remove(); 
		});
	</script>
</div>

