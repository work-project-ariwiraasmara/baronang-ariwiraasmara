<?php 
require('header.php');
require('sidebar-right.php');
require('sidebar-left.php');
require('content-header.php');

$tipe = 0;
if(@$_GET['st'] == 5) {
    $tipe = 0;
}
else {
    $tipe = 1;
}
?>


 <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title" align="center" style="margin-top: 50px;"><?php echo ('Daftar List Template'); ?></h3>
            </div>
            <table class="table table-bordered">
                <div class="table-responsive" width="70%"> 
                    <td colspan="2" width="70%">
                        <table class="table table-bordered table-striped" width="50%">
                    
                        <thead>
                        <tr>
                            <th align="center"><?php echo ('No'); ?></th>
                            <th align="center"><?php echo ('Nama List Template'); ?></th>
                            <th align="center"><?php echo ('Aksi'); ?></th>
                            
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $no=1;
                        if ($_POST['set'] == 00)
                        $list = "Select * from dbo.Template where Tipe = '$tipe'";
                        $cek = sqlsrv_query($conn, $list);
                        while($hasil = sqlsrv_fetch_array( $cek, SQLSRV_FETCH_NUMERIC)){
                            ?>
                            <tr>
                                <td><?=$no;?></td>
                                <td><?=$hasil[1];?>
                                
                                <td>
                                    <a href="edit_template.php?id=<?php echo $hasil[0]; ?>&st=<?php echo @$_GET['st']; ?>&set=00" class="btn btn-default btn-flat btn-sm text-green" title="edit"><i class="ion-android-create"></i></a>
                                     <a href="hapus_template.php?id=<?php echo $hasil[0]; ?>&st=<?php echo @$_GET['st']; ?>" class="btn btn-default btn-flat btn-sm text-green" title="Hapus"><i class="ion-android-delete"></i></a>
                                </td>
                            </tr>

                        <?php $no++; ?>
                            <?php
                        }
                        ?>
                        </tbody>
                    </table>
                    </td>
                </div>
            </div>
</div>



<?php require('footer_new.php');?>
