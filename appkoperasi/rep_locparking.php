<?php require('header.php');?>

<?php require('sidebar-right.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>


<div class="animated fadeinup delay-1">
    <div class="page-content">

        <h2 class="uppercase"><?php echo lang('Laporan Parkir'); ?></h2>

            <form class="form-horizontal" action="" method = "GET">
                <div class="input-field">
                    <input type="text" name="tgl1" id="tgl1" class="datepicker" value="<?php echo "$_GET[tgl1]"; ?>" autocomplete="off"></p></td>
                    <label><?php echo ('Tanggal Awal'); ?></label>
                </div>
                
                <div class="input-field">
                    <input type="text" name="tgl2" id="tgl2" class="datepicker" value="<?php echo "$_GET[tgl2]"; ?>" autocomplete="off"></p></td>
                    <label><?php echo ('Tanggal Akhir'); ?></label>
                </div>

                <div class="input-field">
                    <?php
                    if(isset($_GET['template'])){
                    $to = $_GET['template'];
                    }
                    else{
                    $to = date('Y/m/d');
                    }
                    ?>


                    <select id="akun" name="akun" class="browser-default">
                        <option value=''>- <?php echo lang('Pilih Lokasi'); ?> -</option>
                            <?php
                            $julsql   = "select * from [dbo].[LocationMerchant  ] where status ='1' order by LocationID asc";
                            // $julsql1   = "select * from [dbo].[LaporanRugiLabaViewNew] where Header ='2' and Username = '$nameuser'";
                            echo $julsq;
                            $julstmt = sqlsrv_query($conn, $julsql);

                            while($rjulrow  = sqlsrv_fetch_array( $julstmt, SQLSRV_FETCH_NUMERIC)){
                               ?>
                            <option value="<?=$rjulrow[0];?>" <?php if($rjulrow[0]==$_GET['akun']){echo "selected";} ?>>&nbsp;<?=$rjulrow[0];?>&nbsp;<?=$rjulrow[2];?></option>
                            <?php } ?>
                    </select>
                </div> 

                <div style="margin-top: 30px;">
                    <button type="submit" class="waves-effect waves-light btn-large primary-color width-100"><?php echo lang('Pilih'); ?></button>
                </div>

                <div style="margin-top: 10px; margin-bottom: 30px;">
                    <a href="rep_locparking.php"><button type="button" class="waves-effect waves-light btn-large primary-color width-100">Refresh</button></a>
                </div>
            </form>

        <?php if($_GET['akun']){
            $tgl1 = $_GET['tgl1'];
            $tanggaldari = date('Y-m-d', strtotime($tgl1));
            $hanyatanggal = date('d', strtotime($tgl1));
            //echo $tgl1;
            $tgl2 = $_GET['tgl2'];
            //echo $tgl2;
            $tanggalsampai = date('Y-m-d', strtotime($tgl2));
            $hanyatanggal1 = date('d', strtotime($tgl2));
            $tglh = date('Y/m/d', strtotime('+1 days', strtotime($tgl2)));
            $akun = $_GET['akun'];
            //echo $akun;
            ?>
            <div>
                <a href="drep_locparking.php?from=<?php echo $tanggaldari; ?>&to=<?php echo $tanggalsampai; ?>&acc=<?php echo $_GET['akun']; ?>"><button type="button" class="waves-effect waves-light btn-large primary-color width-100">Download to excel</button></a>
            </div>

            <div class="box-header" align="center" style="margin-top: 30px;">
                <tr>
                    <h3 class="" align="center"><?php echo ('Laporan Parkir'); ?></h3>
                </tr>
            </div>
            <div class="box-header " align="Center">
               


            <div class="box-header" align="Center">
                    <tr>
                        <?php
                            $bulan = date('m', strtotime($_GET['tgl1']));
                            $tahun = date('Y', strtotime($_GET['tgl1']));
                            $bulan2 = date('m', strtotime($_GET['tgl2']));
                            $tahun2 = date('Y', strtotime($_GET['tgl2']));
                            if ($bulan != 1) {
                                if ($bulan != 2 ) {
                                   if ($bulan != 3) {
                                       if ($bulan != 4) {
                                            if ($bulan !=5) {
                                                if ($bulan !=6) {
                                                    if ($bulan !=7) {
                                                        if ($bulan !=8) {
                                                            if ($bulan !=9) {
                                                                if ($bulan !=10) {
                                                                    if ($bulan !=11) {
                                                                        $bulan = 'Desember';
                                                                    } else {
                                                                        $bulan = 'Nopember';
                                                                    }
                                                                } else {
                                                                    $bulan = 'Oktober';
                                                                }
                                                            } else {
                                                                $bulan = 'September';
                                                            }
                                                        } else {
                                                            $bulan = 'Agustus';
                                                        }
                                                    } else {
                                                        $bulan = 'Juli';
                                                    }
                                                } else {
                                                    $bulan = 'Juni';
                                                }
                                            } else {
                                              $bulan = 'Mei';  
                                            }
                                       } else {
                                            $bulan = 'April';       
                                       }
                                   } else {
                                    $bulan = 'Maret';
                                   }
                                } else {
                                    $bulan = 'Februari';    
                                }                               
                            } else {
                                $bulan = 'Januari';
                            }
                        ?>
                        <?php
                        if ($bulan2 != 1) {
                                if ($bulan2 != 2 ) {
                                   if ($bulan2 != 3) {
                                       if ($bulan2 != 4) {
                                            if ($bulan2 !=5) {
                                                if ($bulan2 !=6) {
                                                    if ($bulan2 !=7) {
                                                        if ($bulan2 !=8) {
                                                            if ($bulan2 !=9) {
                                                                if ($bulan2 !=10) {
                                                                    if ($bulan2 !=11) {
                                                                        $bulan2 = 'Desember';
                                                                    } else {
                                                                        $bulan2 = 'Nopember';
                                                                    }
                                                                } else {
                                                                    $bulan2 = 'Oktober';
                                                                }
                                                            } else {
                                                                $bulan2 = 'September';
                                                            }
                                                        } else {
                                                            $bulan2 = 'Agustus';
                                                        }
                                                    } else {
                                                        $bulan2 = 'Juli';
                                                    }
                                                } else {
                                                    $bulan2 = 'Juni';
                                                }
                                            } else {
                                              $bulan2 = 'Mei';  
                                            }
                                       } else {
                                            $bulan2 = 'April';       
                                       }
                                   } else {
                                    $bulan2 = 'Maret';
                                   }
                                } else {
                                    $bulan2 = 'Februari';    
                                }                               
                            } else {
                                $bulan2 = 'Januari';
                            }
                        ?>
                        <h5 class="box-title"><?php echo "Periode : ",$hanyatanggal," ",$bulan," ",$tahun, " - ", $hanyatanggal1," ", $bulan2," ",$tahun2; ?></h5>
                    </tr>


               
                <?php 
                $x = "select * from dbo.LocationMerchant where status = 1 and LocationID = '$akun' order by LocationID asc";
                //echo $x;
                $y = sqlsrv_query($conn, $x);
                while($z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC)){
                    

                ?>
                <div class="box-header " align="left">
                <tr>
                    <h5 class="box-title" align="left"><?php echo ('Lokasi : '); ?> <?php echo $z[2]; ?></h5  >
                </tr>
                </div>    
                <div class="row">
                    <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-lg-12 table-responsive">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th><?php echo 'No'; ?></th>
                                    <th><?php echo 'Tanggal'; ?></th>
                                    <th><?php echo 'No Kartu'; ?></th>
                                    <th><?php echo 'Status Member'; ?></th>
                                    <th><?php echo 'Waktu Masuk'; ?></th>
                                    <th><?php echo 'Waktu Keluar'; ?></th>
                                    <th><?php echo 'Jenis Kendaraan'; ?></th>
                                    <th><?php echo 'Poin Bayar'; ?></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                            //count
                            $jmlulsql   = "select count(*) from dbo.ParkingList where LocationIn ='$_GET[akun]' and status = '1' and TimeIn between '$tgl1' and '$tglh'";
                            //echo $jmlulsql;
                            $jmlulstmt  = sqlsrv_query($conn, $jmlulsql);
                            $jmlulrow   = sqlsrv_fetch_array($jmlulstmt, SQLSRV_FETCH_NUMERIC);

                            //pagging
                            $perpages   = 10;
                            $halaman    = $_GET['page'];
                            if(empty($halaman)){
                                $posisi  = 0;
                                $batas   = $perpages;
                                $halaman = 1;
                            }
                            else{
                                $posisi  = (($perpages * $halaman) - 10) + 1;
                                $batas   = ($perpages * $halaman);
                            }

                            
                            $from= $_GET['tgl1'];
                            $to= $_GET['tgl2'];
                            $ulsql = "SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY TimeIn asc) as row FROM [dbo].[ParkingList] a where LocationIn = '$z[0]' and TimeIn between '$from' and '$tglh') a WHERE row between '$posisi' and '$batas'";
                                //echo $ulsql;
                            //echo $ulsql;
                            $ulstmt = sqlsrv_query($conn, $ulsql);
                            while($ulrow = sqlsrv_fetch_array( $ulstmt, SQLSRV_FETCH_NUMERIC)){
                               $jenisk = "select * from dbo.VehicleType where Code = '$ulrow[10]'";
                               $prosesk = sqlsrv_query($conn, $jenisk);
                               $hasilk = sqlsrv_fetch_array($prosesk, SQLSRV_FETCH_NUMERIC);
                            ?>
                                <tr>
                                <td><?php echo $ulrow[12]; ?></td>
                                <td><?php echo $ulrow[3]->format('Y-m-d H:i:s'); ?></td>
                                <td><?php echo $ulrow[2]; ?></td>
                                <td><?php if ($ulrow[11] == 1){
                                            $amountr = 'Member';
                                            } else {
                                            $amountr = 'Non Member';
                                                }
                                            echo $amountr; ; ?></td>
                                <td><?php echo $ulrow[3]->format('Y-m-d H:i:s'); ?></td>
                                <td><?php if ($ulrow[4] == null ){ 
                                            $tanggal = '';
                                            }else {
                                            $tanggal = $ulrow[4]->format('Y-m-d H:i:s');
                                            }
                                            echo $tanggal; ; ?></td>
                                <td><?php echo $hasilk[1]; ?></td>
                                <td><?php echo $ulrow[8]; ?></td>
                            </tr>
                            <?php } ?>    
                            <?php
                            $jmlhalaman = ceil($jmlulrow[0]/$perpages);
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                    <div class="box-footer clearfix right">
                        <div style="text-align: center;">
                            Showing <?=$halaman?> to <?=$jmlhalaman?> of <?=$jmlulrow[0]?> entries <?=$posisi." - ".$batas?>
                            <?php
                            $akun = $_GET['akun'];
                            $tgl1 = $_GET['tgl1'];
                            $tgl2 = $_GET['tgl2'];
                            $reload = "rep_locparking.php?akun=$akun&tgl1=$tgl1&tgl2=$tgl2";
                            $page = intval($_GET["page"]);
                            $adjacents = isset($_GET['adjacents']) ? intval($_GET['adjacents']) : 2; // khusus style pagination 2 dan 3
                            if( $page == 0 ) $page = 1;

                            $tpages = ($jmlhalaman) ? ceil($jmlhalaman) : 0; // total pages, last page number
                            echo "<div>".paginate($reload, $page, $tpages)."</div>";
                            ?>
                        </div>
                    </div>           
                <?php } ?>  
            <!-- div dekat tanggal -->
            </div>

            </div>
        <?php } ?>
    </div>
</div>


<?php
function paginate($reload, $page, $tpages) {

    $firstlabel = "First";
    $prevlabel  = "Prev";
    $nextlabel  = "Next";
    $lastlabel  = "Last";

    $out = "<div class=\"backpaging\"><ul class=\"ownpaging\">\n";

    // first
    if($page>1) {
        $out.= "<li><a href=\"" . $reload . "\">" . $firstlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $firstlabel . "</a></li>\n";
    }

    // previous
    if($page==1) {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $prevlabel . "</a></li>\n";
    }
    elseif($page==2) {
        $out.= "<li><a href=\"" . $reload . "\">" . $prevlabel . "</a></li>\n";
    }
    else {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" . ($page-1) . "\">" . $prevlabel . "</a></li>\n";
    }

    // current
    $out.= "<li class=\"active\"><a href=\"#\">Page " . $page . " of " . $tpages . "</a></li>\n";

    // next
    if($page<$tpages) {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" .($page+1) . "\">" . $nextlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $nextlabel . "</a></li>\n";
    }

    // last
    if($page<$tpages) {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" . $tpages . "\">" . $lastlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $lastlabel . "</a></li>\n";
    }

    $out.= "</ul></div>";

    return $out;
}
?>


<script type="text/javascript">
    var tampung = [];

     $('.btn-addu').click(function(){
       var index = $('#detailUser tr').length;

        var user = $('#user option:selected').val();
        var amount = $('#amount').val();
        var date = $('#date').val();

        if(date == ''){
          alert('Assign date tidak boleh kosong');
          return false;
        }
        else if(user == '' || amount == '' || amount <= 0){
            alert('Harap isi inputan dengan benar');
            return false;
        }
        else if(jQuery.inArray(user, tampung) !== -1){
            alert('Tidak dapat memilih user yang sudah ditambahkan sebelumnya');
            return false;
        }
        else{
            $.ajax({
                url : "ajax_addteller.php",
                type : 'POST',
                dataType : 'json',
                data: { index: index, user: user, amount: amount, date: date},   // data POST yang akan dikirim
                success : function(data) {
                    if(data.status == 1){
                        tampung.push(data.id);
                        $("#detailUser").append("<tr>" +
                            "<td><input type='hidden' name='user["+data.index+"]' value="+ data.id +">" + data.nama + "</td>" +
                            "<td><input type='hidden' name='amount["+data.index+"]' value="+ data.amount +">" + data.amount +
                            "<td><a class='btn btn-default btn-flat btn-sm text-red remove' onclick='$(this).parent().parent().remove();'><i class='ion-android-delete'></i></a></td>" +
                            "</tr>");

                            $('#user').val('');
                            $('#amount').val('');
                    }
                    else{
                        alert(data.message);
                        return false;
                    }
                },
                error : function() {
                    alert('Telah terjadi error.');
                    return false;
                }
            });
        }
    });
</script>

<?php require('footer.php'); ?>
