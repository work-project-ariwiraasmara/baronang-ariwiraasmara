<?php
// locproduk   
require('header.php');
require('sidebar-right.php');
require('sidebar-left.php');

$kid = $_SESSION['KID'];

// GET LAST ID LocationParkingProduct
$sql = 'SELECT TOP 1 ProductID FROM [dbo].[LocationParkingProduct] ORDER BY ProductID DESC';
$query = sqlsrv_query($conn, $sql);
$data = sqlsrv_fetch_array($query, SQLSRV_FETCH_NUMERIC);
//echo $dataloc[0].'<br>';
$intaid = 0;
$graid = substr($data[0], -6);
$gp = (int)$graid + 1;
$intaid = str_pad($gp, 6, '0', STR_PAD_LEFT);

if(@$_GET['locid']) {
	$sql = "SELECT * from [dbo].[LocationParkingProduct] where ProductID='".@$_GET['locid']."'";
	$query = sqlsrv_query($conn, $sql);
	$data = sqlsrv_fetch_array($query, SQLSRV_FETCH_NUMERIC);

	$btn_text = 'Update';
	$btn_name_id = 'btnupdate';
}
else {
	$btn_text = 'Simpan';
	$btn_name_id = 'btnok';
}
?>

	<div class="animated fadeinup delay-1">
		<div class="page-content txt-black">

			<h2 class="uppercase txt-black">Produk Parking</h2>

			<div class="form-inputs m-t-30">
				<form action="" method="post" class="m-t-30">
					<input type="hidden" name="procid" id="procid" value="<?php echo $kid.'PROD'.$intaid; ?>">
					<input type="hidden" name="procid_ed" id="procid_ed" value="<?php echo $data[1]; ?>">

					<div class="input-field">
						<span>Pilih Lokasi:</span>
						<select name="locid" id="locid" class="browser-default">
							<?php
							$sql2 = "SELECT * FROM [KOPX700097].[dbo].[LocationMerchant] ORDER BY Nama ASC";
							$query2 = sqlsrv_query($conn, $sql2);
							while($data2 = sqlsrv_fetch_array($query2, SQLSRV_FETCH_NUMERIC)) { ?>
								<option value="<?php echo $data2[0]; ?>" <?php if(@$_GET['locid']) { if($data2[0] == $data[0] ) echo 'selected'; } ?> ><?php echo $data2[2]; ?></option>
								<?php
							}
							?>
						</select>
					</div>

					<div class="input-field">
						<input type="text" name="nama" id="nama" class="validate" value="<?php if(@$_GET['locid']) echo $data[2]; ?>" >
						<label for="nama">Nama</label>
					</div>

					<div class="input-field">
						<input type="text" name="desk" id="desk" class="validate" value="<?php if(@$_GET['locid']) echo $data[3]; ?>" >
						<label for="desk">Deskripsi</label>
					</div>

					<div class="input-field">
						<span>Status :</span> <br>
						<select name="status" id="status" class="browser-default">
							<option value="0" <?php if(@$_GET['locid']) { if($data[4] == 0) echo 'selected'; } ?> >Tidak Aktif</option>
							<option value="1" <?php if(@$_GET['locid']) { if($data[4] == 1) echo 'selected'; } ?> >Aktif</option>
						</select>
					</div>

					<div class="input-field">
						<span>Tipe</span> : <br>

						<input type="radio" name="tipe" id="tipe0" value="0" <?php if(@$_GET['locid']) { if($data[10] == 0) echo 'checked="checked"'; } ?> >
						<label for="tipe0">Poin</label>

						<input type="radio" name="tipe" id="tipe1" value="1" <?php if(@$_GET['locid']) { if($data[10] == 1) echo 'checked="checked"'; } ?> >
						<label for="tipe1">Member Bulanan</label>
					</div>

					<div class="input-field">
						<input type="number" name="poin" id="poin" class="validate" value="<?php if(@$_GET['locid']) echo $data[5]; ?>" >
						<label for="poin">Jumlah Poin Deduct</label>
					</div>

					<div class="input-field">
						<input type="number" name="hari" id="hari" value="<?php if(@$_GET['locid']) echo $data[6]; ?>" >
						<label for="hari">Jumlah Hari</label>
					</div>

					<div class="input-field">
						<input type="number" name="harga" id="harga" class="validate" value="<?php if(@$_GET['locid']) echo $data[7]; ?>" >
						<label for="harga">Harga</label>
					</div>

					<div class="input-field">
						<span>Jenis Kendaraan :</span> <br>
						<select name="vhcl" id="vhcl" class="browser-default">
							<?php
							$sql2 = "SELECT * FROM [KOPX700097].[dbo].[VehicleType] ORDER BY Name ASC";
							$query2 = sqlsrv_query($conn, $sql2);
							while($data2 = sqlsrv_fetch_array($query2, SQLSRV_FETCH_NUMERIC)) { ?>
								<option value="<?php echo $data2[0]; ?>" <?php if(@$_GET['locid']) { if($data2[0] == $data[9]) echo 'selected'; } ?> ><?php echo $data2[1]; ?></option>
								<?php
							}
							?>
						</select>
					</div>

					<div class="input-field">
						<input type="text" name="ket" id="ket" class="validate" value="<?php if(@$_GET['locid']) echo $data[11]; ?>" >
						<label for="ket">Keterangan</label>
					</div>

					<button type="submit" name="<?php echo $btn_name_id; ?>" id="<?php echo $btn_name_id; ?>" class="btn btn-large primary-color width-100 m-t-30"><?php echo $btn_text; ?></button>
				</form>
			</div>

			<?php
			$ok = @$_POST['btnok'];
			if(isset($ok)) {
				$locid 	= @$_POST['locid'];
				$procid = @$_POST['procid'];
				$nama 	= @$_POST['nama'];
				$desk 	= @$_POST['desk'];
				$status = @$_POST['status'];
				$poin 	= @$_POST['poin'];
				$hari 	= @$_POST['hari'];
				$harga 	= @$_POST['harga'];
				$vhcl 	= @$_POST['vhcl'];
				$tipe 	= @$_POST['tipe'];
				$ket 	= @$_POST['ket'];

				$sql = "INSERT into [KOPX700097].[dbo].[LocationParkingProduct](LocationID, ProductID, Nama, Deskripsi, Status, JumlahPoinDeduct, Jumlah, Harga, VehicleID, Tipe, Keterangan)
						values('$locid','$procid','$nama','$desk','$status','$poin','$hari','$harga','$vhcl', '$tipe', '$ket')";
				//echo $sql;
				$query = sqlsrv_query($conn, $sql);
				if($query) { ?>
					<script type="text/javascript">
						alert('Simpan Berhasil');
						window.location.href = "locproduk.php";
					</script>
					<?php
				}
				else { ?>
					<script type="text/javascript">
						alert('Simpan Gagal');
					</script>
					<?php
				}
			}

			$update = @$_POST['btnupdate'];
			if(isset($update)) {
				$locid 	= @$_POST['locid'];
				$procid = @$_POST['procid_ed'];
				$nama 	= @$_POST['nama'];
				$desk 	= @$_POST['desk'];
				$status = @$_POST['status'];
				$poin 	= @$_POST['poin'];
				$hari 	= @$_POST['hari'];
				$harga 	= @$_POST['harga'];
				$vhcl 	= @$_POST['vhcl'];
				$tipe 	= @$_POST['tipe'];
				$ket 	= @$_POST['ket'];

				$sql = "UPDATE [KOPX700097].[dbo].[LocationParkingProduct] set Nama = '$nama', Deskripsi = '$desk', 
																  			   Status = '$status', JumlahPoinDeduct = '$poin', 
																  			   Jumlah = '$hari', Harga = '$harga',
																  			   Tipe = '$tipe', Keterangan = '$ket' where ProductID='$procid'";
				//echo $sql;
				$query = sqlsrv_query($conn, $sql);
				if($query) { ?>
					<script type="text/javascript">
						alert('Update Berhasil');
						window.location.href = "locproduk.php";
					</script>
					<?php
				}
				else { ?>
					<script type="text/javascript">
						alert('Update Gagal');
					</script>
					<?php
				}
			}
			?>

			<div class="table-responsive m-t-30">
                <table class="table">
                	<thead>
                		<tr>
                			<th>Location</th>
                			<th>Product ID</th>
                			<th>Nama</th>
                			<th>Deskripsi</th>
                			<th>Status</th>
                			<th>Jumlah Poin</th>
                			<th>Jumlah</th>
                			<th>Harga (Rp.)</th>
                			<th>Kendaraan</th>
                			<th>Tipe</th>
                			<th>Keterangan</th>
                			<th></th>
                		</tr>
                	</thead>

                	<tbody>
                		<?php
                		$sql = "SELECT * from [dbo].[LocationParkingProduct] ORDER BY LocationID ASC";
                		$query = sqlsrv_query($conn, $sql);
                		while($data = sqlsrv_fetch_array($query, SQLSRV_FETCH_NUMERIC)) { 
                			$sloc = "SELECT Nama from [dbo].[LocationMerchant] where LocationID='".$data[0]."'";
                			$qloc = sqlsrv_query($conn, $sloc);
                			$dloc = sqlsrv_fetch_array($qloc, SQLSRV_FETCH_NUMERIC)
                			?>
                			<tr>
                				<td><?php echo $dloc[0]; ?></td>
                				<td><?php echo $data[1]; ?></td>
                				<td><?php echo $data[2]; ?></td>
                				<td><?php echo $data[3]; ?></td>
                				<td><?php echo $data[4]; ?></td>
                				<td><?php echo $data[5]; ?></td>
                				<td><?php echo $data[6]; ?></td>
                				<td style="text-align: right;"><?php echo number_format($data[7],2,',','.'); ?></td>

                				<?php
                				$sqlc = "SELECT Name from [dbo].[VehicleType] where VehicleID='".$data[9]."'";
                				$queryc = sqlsrv_query($conn, $sqlc);
                				$datac = sqlsrv_fetch_array($queryc, SQLSRV_FETCH_NUMERIC)
                				?>
                				<td><?php echo $datac[0]; ?></td>

                				<td><?php echo $data[10]; ?></td>
                				<td><?php echo $data[11]; ?></td>
                				<td>
                					<a href="locproduk.php?locid=<?php echo $data[1]; ?>"><button type="button" class="btn btn-large primary-color waves-light waves-effect"><i class="ion-android-create"></i></button></a>
                				</td>
                			</tr>
                			<?php
                		}
                		?>
                	</tbody>
                </table>
            </div>

		</div>
	</div>

<?php require('footer.php');?>
