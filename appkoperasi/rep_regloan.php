<?php require('header.php');?>

<?php require('sidebar-right.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>


<div class="animated fadeinup delay-1">
     <div class="page-content">
         <div class="box-body">
            <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-lg-12 table-responsive">
                <h2 class="uppercase"><?php echo lang('Detail Transaksi'); ?></h2> <br>

                <a href="rep_bas.php?page=<?php echo $_GET['pagem'];?>&acc=<?php echo $_GET['acc'];?>"><button type="button" class="waves-effect waves-light btn-large primary-color width-100" style="margin-top: 10px; margin-bottom: 20px;">Back</button></a>

                <a href="drep_regloan.php?acc2=<?php echo $_GET['acc2'];?>&st=4"><button type="button" class="waves-effect waves-light btn-large primary-color width-100" style="margin-top: 10px; margin-bottom: 20px;">Download To Excel</button></a>

                <table class="table table-bordered table-striped">
                    <tbody>

                            <?php
                            $acc= $_GET['acc2'];
                            $disable = "readonly";
                            if($_GET['st'] == 4){
                            $xxx = "SELECT * FROM [dbo].[LoanReportBalance] WHERE LoanNumber='$acc'";
                            //echo $xxx;
                            $yyy = sqlsrv_query($conn,$xxx );
                            while($zzz = sqlsrv_fetch_array($yyy, SQLSRV_FETCH_NUMERIC)){
                                $xyz = "SELECT * FROM [dbo].[MemberListView] WHERE MemberID='$zzz[7]'";
                                $yza = sqlsrv_query($conn,$xyz); 
                                $zay = sqlsrv_fetch_array($yza, SQLSRV_FETCH_NUMERIC);

                                $def   = "select * from [dbo].[LoanTypeViewNew] where KodeLoanType ='$zzz[8]'";
                                $efg = sqlsrv_query($conn,$def); 
                                $fgh = sqlsrv_fetch_array($efg, SQLSRV_FETCH_NUMERIC);

                                $totalj = "select sum (PrincipalPayment) FROM dbo.LoanTransaction where LoanNumber = '$_GET[acc2]'";
                                $jumlahb = sqlsrv_query($conn,$totalj); 
                                $bayarl = sqlsrv_fetch_array($jumlahb, SQLSRV_FETCH_NUMERIC);
                                //echo $totalj;
                            ?>

                             <tr>
                                <h5 class="box-title" align="left"><?php echo ('Produk Pinjaman : '); ?> <?php echo $zzz[4]; ?> - <?php echo $fgh[9]; ?></h3>
                            </tr>

                            <div class="active">
                                    <label><?php echo lang('Akun'); ?></label>
                                    <input type="text" name="akun" class="validate" id="" value="<?=$acc?>" <?php echo $disable; ?>>   
                            </div>
                            <div class="active">
                                    <label ><?php echo lang('Nama'); ?></label>
                                    <input type="text" name="nama" class="validate" id="" value="<?=$zay[2]?>" <?php echo $disable; ?>>   
                            </div>
                            <div class="active">
                                    <label><?php echo lang('Pinjaman Awal'); ?></label>
                                    <input type="text" name="saldoaw" class="validate" id="" value="<?php echo $zzz[1];?>" <?php echo $disable; ?>>   
                            </div>
                            <div class="active">
                                    <label><?php echo lang('Total Bayar Pinjaman'); ?></label>
                                    <input type="text" name="tbayar" class="validate" id="" value="<?php echo number_format($bayarl[0],2);?>" <?php echo $disable; ?>>   
                            </div>
                            <div class="active">
                                    <label><?php echo lang('Sisa Pinjaman'); ?></label>
                                    <input type="text" name="saldoak" class="validate" id="" value="<?php echo $zzz[5];?>" <?php echo $disable; ?>>   
                            </div>

                            <?php } ?>
                            <?php } ?>




                            <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>No Transaksi</th>
                                    <th>Tanggal Transaksi</th>
                                    <th><?php echo lang('Tipe Transaksi'); ?></th>
                                    <th><?php echo lang('Angsuran Pokok'); ?></th>
                                    <th><?php echo lang('Bunga'); ?></th>
                                    <th><?php echo lang('Pokok + Bunga'); ?></th>
                                </tr>
                            </thead>
                
                            <?php
                            //count
                            if($_GET['st'] == 4){
                                
                            $jmlulsql   = "select count(*) from dbo.LoanTransView where LoanNumber='$_GET[acc2]'";
                            }
                            $jmlulstmt  = sqlsrv_query($conn, $jmlulsql);
                            $jmlulrow   = sqlsrv_fetch_array($jmlulstmt, SQLSRV_FETCH_NUMERIC);

                            //pagging
                            $perpages   = 10;
                            $halaman    = $_GET['page'];
                            if(empty($halaman)){
                                $posisi  = 0;
                                $batas   = $perpages;
                                $halaman = 1;
                            }
                            else{
                                $posisi  = (($perpages * $halaman) - 10) + 1;
                                $batas   = ($perpages * $halaman);
                            }

                            if($_GET['st'] == 4){

                                $aaa = "SELECT * FROM ( SELECT [TransactionNumber],[LoanNumber],[TimeStamp],[KodeTransactionType],[Description],[InvCount],[PrincipalPayment],[InterestPayment],[UserID], ROW_NUMBER() OVER (ORDER BY TimeStamp asc) as row FROM dbo.LoanTransaction where LoanNumber = '$_GET[acc2]') a  where row between '$posisi' and '$batas'";
                                //echo $aaa;
                            }
                            $bbb = sqlsrv_query($conn,$aaa );
                            $total= 0;
                            while($ccc = sqlsrv_fetch_array($bbb, SQLSRV_FETCH_NUMERIC)){
                                $total = $ccc[6]+$ccc[7];
                            //echo $ccc[7];
                            
                            ?>
                                <tr>
                                    <td><?php echo $ccc[9]; ?></td>
                                    <td><?php echo $ccc[0]; ?></td>
                                    <td><?php echo $ccc[2]->format('Y-m-d H:i:s'); ?></td>
                                    <td>
                                        <?php
                                        $aaaa = "select* from dbo.TransactionType where KodeTransactionType='".$ccc[3]."'";
                                        $bbbb = sqlsrv_query($conn, $aaaa);
                                        $cccc = sqlsrv_fetch_array($bbbb, SQLSRV_FETCH_NUMERIC);
                                        if($cccc != null){
                                            echo $cccc[1];
                                        }
                                        else{
                                            echo $ccc[3];
                                        }
                                        ?>
                                    </td>
                                    <td style="text-align: right;"><?php echo number_format($ccc[6],2); ?></td>
                                    <td style="text-align: right;"><?php echo number_format($ccc[7],2) ; ?></td>
                                    <td style="text-align: right;"><?php echo number_format($total,2); ?></td>
                                </tr>
                                <?php
                                $jmlpage++; $iii0++; $iii2++; $iii5++; $iii6++; $iii7++;
                                }
                               $jmlhalaman = ceil($jmlulrow[0]/$perpages);
                                ?>
                            </table>
                        </div>
                    </tbody>

                </table>
                
            </div>
            <div class="box-footer clearfix right">
                        <div style="text-align: center;">
                            Showing <?=$halaman?> to <?=$jmlhalaman?> of <?=$jmlulrow[0]?> entries <?=$posisi." - ".$batas?>
                            <?php
                            $acc2 = $_GET['acc2'];
                            $st = $_GET['st'];
                            $pagem = $_GET['pagem'];
                            $acc = $_GET['acc'];
                            $reload = "rep_regloan.php?acc2=$acc2&st=$st&pagem=$pagem&acc=$acc";
                            $page = intval($_GET["page"]);
                            $adjacents = isset($_GET['adjacents']) ? intval($_GET['adjacents']) : 2; // khusus style pagination 2 dan 3
                            if( $page == 0 ) $page = 1;
                                $tpages = ($jmlhalaman) ? ceil($jmlhalaman) : 0; // total pages, last page number
                                echo "<div>".paginate($reload, $page, $tpages)."</div>";
                                ?>
                        </div>
                </div>
            
            </div>

        </div>
    </div>
</div>


<?php
function paginate($reload, $page, $tpages) {

    $firstlabel = "First";
    $prevlabel  = "Prev";
    $nextlabel  = "Next";
    $lastlabel  = "Last";

    $out = "<div class=\"backpaging\"><ul class=\"ownpaging\">\n";

    // first
    if($page>1) {
        $out.= "<li><a href=\"" . $reload . "\">" . $firstlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $firstlabel . "</a></li>\n";
    }

    // previous
    if($page==1) {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $prevlabel . "</a></li>\n";
    }
    elseif($page==2) {
        $out.= "<li><a href=\"" . $reload . "\">" . $prevlabel . "</a></li>\n";
    }
    else {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" . ($page-1) . "\">" . $prevlabel . "</a></li>\n";
    }

    // current
    $out.= "<li class=\"active\"><a href=\"#\">Page " . $page . " of " . $tpages . "</a></li>\n";

    // next
    if($page<$tpages) {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" .($page+1) . "\">" . $nextlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $nextlabel . "</a></li>\n";
    }

    // last
    if($page<$tpages) {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" . $tpages . "\">" . $lastlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $lastlabel . "</a></li>\n";
    }

    $out.= "</ul></div>";

    return $out;
}
 require('content-footer.php');?>

<?php require('footer.php');?>
