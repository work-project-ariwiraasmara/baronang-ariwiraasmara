<?php
require('header.php');
require('sidebar-left.php');
require('sidebar-right.php');
require('content-header.php');
?>

    <div class="animated fadeinup delay-1">
        <div class="page-content txt-black">
            <a href="dashboard_parkir2.php" target="_self" class="btn waves-effect waves-light primary-color">Dashboard</a>
            <?php
            $chart = @$_GET['chart'];
            if(@$_GET['chart']) {
                $json_label = array();
                $json_data1 = array();
                $json_data2 = array();
                $json_data3 = array();
                //$json_data4 = array();
                
                $bulan = @$_GET['bulan'];
                $tahun = @$_GET['tahun'];
				$month = date('m', strtotime($bulan));
				
                $bulan_int = date('t', strtotime($bulan));
                $tahun_int = date('Y', strtotime($tahun));
                $loc = @$_GET['loc'];
                
                $sql_lokasi = "Select Nama from [dbo].[LocationMerchant] where LocationID='$loc'";
				$queryLoc = sqlsrv_query($conn, $sql_lokasi);
				$lokasi = sqlsrv_fetch_array($queryLoc);
				
				$sql_rp = "SELECT ValueConversion from [dbo].[ParkingSetting]";
				$query_rp = sqlsrv_query($conn, $sql_rp);
				$data_rp = sqlsrv_fetch_array($query_rp);
				$conv = $data_rp[0];

                ?>

                <div id="container" style="width: 100% ; height: 500px">

                    <?php
                    if(@$_GET['chart'] == 'bulan') { ?>
                    
                    <?php
						$sql_membersales = "SELECT SUM(Amount) from [dbo].[TransList] where TransactionType='MBRS' and AccountKredit='$loc' and month([Date]) = '$month'";
						$query_membersales = sqlsrv_query($conn, $sql_membersales);
						$data_membersales = sqlsrv_fetch_array($query_membersales, SQLSRV_FETCH_NUMERIC);
							if ($data_membersales[0] == null) {
								$membersales = 0;}
							else {
								$memsalespoint = $data_membersales[0] / $conv;
								$membersales = number_format($memsalespoint,0,",",".");}
							//echo $data_membersales.'mmm'.$conv;
                        $title = '<b>Member Sales<br>'.$lokasi[0].'<br>'.date('F', strtotime($bulan)).' '.date('Y', strtotime($tahun)).'<br>'.$membersales.' Points<br><br></b>';

                        for($l = 1; $l <= $bulan_int; $l++) {
                            $ln1 = 0;
                            $ln2 = 0;
                            $ln3 = 0;
                           
                            $dtf = $tahun_int."-".date('m', strtotime($bulan))."-".$l;
                            $dtt = $tahun_int."-".date('m', strtotime($bulan))."-".$bulan_int;

                            if($l > 0 && $l < 10) { array_push($json_label, '<a href="chart_membersales_loc.php?chart=hari&tgl='.$l.'&bulan='.$bulan.'&tahun='.$tahun.'&loc='.$loc.'" target="_self">0'.$l.'</a>'); }
                            else { array_push($json_label, '<a href="chart_membersales_loc.php?chart=hari&tgl='.$l.'&bulan='.$bulan.'&tahun='.$tahun.'&loc='.$loc.'" target="_self">'.$l.'</a>'); }
                            

                            $sql_line1 = "SELECT SUM(Amount) from [dbo].[ParkMemberSales] where LocationID='$loc' and VehicleID in ('700097VEHICLE001', '700097VEHICLE006') and Tanggal between '$dtf 00:00:00' and '$dtf 23:59:59'";
                            $query1 = sqlsrv_query($conn, $sql_line1);
                            while($data1 = sqlsrv_fetch_array($query1)) {
                                array_push($json_data1, $data1[0]);
                            }

                            $sql_line2 = "SELECT SUM(Amount) from [dbo].[ParkMemberSales] where LocationID='$loc' and VehicleID in ('700097VEHICLE002', '700097VEHICLE004') and Tanggal between '$dtf 00:00:00' and '$dtf 23:59:59'";
                            $query2 = sqlsrv_query($conn, $sql_line2);
                            while($data2 = sqlsrv_fetch_array($query2)) {
                                array_push($json_data2, $data2[0]);
                            }

                            $sql_line3 = "SELECT SUM(Amount) from [dbo].[ParkMemberSales] where LocationID='$loc' and Tanggal between '$dtf 00:00:00' and '$dtf 23:59:59'";
                            $query3 = sqlsrv_query($conn, $sql_line3);
                            while($data3 = sqlsrv_fetch_array($query3)) {
                                array_push($json_data3, $data3[0]);
                            }

                            
                        }
                        ?>
                        
                         <div id="container2" style="width: 100% ; height: 500px">
				<?php
				$json_label2 = array();
                $json_data4 = array();
                $json_data5 = array();
                $json_data6 = array();
                
                $title2 = '<b>Jumlah Parkir<br>'.$lokasi[0].'<br>'.date('F', strtotime($bulan)).' '.date('Y', strtotime($tahun)).'<br><br></b>';
						
						for($l2 = 1; $l2 <= $bulan_int; $l2++) {
                            $ln4 = 0;
                            $ln5 = 0;
                            $ln6 = 0;
                           
                            $dtf = $tahun_int."-".date('m', strtotime($bulan))."-".$l2;
                            $dtt = $tahun_int."-".date('m', strtotime($bulan))."-".$bulan_int;

                            if($l2 > 0 && $l2 < 10) { array_push($json_label2, '<a href="chart_inout_loc.php?chart=hari&tgl='.$l2.'&bulan='.$bulan.'&tahun='.$tahun.'&loc='.$loc.'" target="_self">0'.$l2.'</a>'); }
                            else { array_push($json_label2, '<a href="chart_inout_loc.php?chart=hari&tgl='.$l2.'&bulan='.$bulan.'&tahun='.$tahun.'&loc='.$loc.'" target="_self">'.$l2.'</a>'); }
                            

                            $sql_line4 = "SELECT COUNT(*) from [dbo].[ParkingList] where LocationIn='$loc' and TimeIn between '$dtf 00:00:00' and '$dtf 23:59:59'";
                            $query4 = sqlsrv_query($conn, $sql_line4);
                            while($data4 = sqlsrv_fetch_array($query4)) {
                                array_push($json_data4, $data4[0]);
                            }

                            $sql_line5 = "SELECT COUNT(*) from [dbo].[ParkingList] where LocationIn='$loc' and Status = '1' and TimeOut between '$dtf 00:00:00' and '$dtf 23:59:59'";
                            $query5 = sqlsrv_query($conn, $sql_line5);
                            while($data5 = sqlsrv_fetch_array($query5)) {
                                array_push($json_data5, $data5[0]);
                            }

                            $sql_line6 = "SELECT COUNT(*) from [dbo].[ParkingList] where LocationIn='$loc' and Status = '0' and TimeIn between '$dtf 00:00:00' and '$dtf 23:59:59'";
                            $query6 = sqlsrv_query($conn, $sql_line6);
                            while($data6 = sqlsrv_fetch_array($query6)) {
                                array_push($json_data6, $data6[0]);
                            }

                            
                        }
					
                ?>
			</div>

                        <div class="m-t-10" style="margin-bottom: 50px;">
                            <?php
                            //$dmn = date('t M'); 
                            //$yn = date('Y');
                            $month = date('m', strtotime($bulan));
                            //echo $dmn;
                            ?>

                            <?php 
                            if( !(($bulan == '31 Jan') && ($tahun == '2019')) ) { ?>
                                <a href="chart_loc_detail.php?chart=bulan&bulan=<?php echo date('t M', strtotime($tahun.'-'.$month.'-01'.'-1 month')); ?>&tahun=<?php echo $tahun; ?>&loc=<?php echo $loc; ?>" target="_self" class="btn btn-large left primary-color waves-effect waves-light" style="color: #ffffff;">Prev</a>
                            <?php 
                            }
                            ?>

                            <?php
                            if( !(($dmn == date('t M')) && ($yn == date('Y'))) ) { ?>
                                <a href="chart_loc_detail.php?chart=bulan&bulan=<?php echo date('t M', strtotime($tahun.'-'.$month.'-01'.'+1 month')); ?>&tahun=<?php echo $tahun; ?>&loc=<?php echo $loc; ?>" target="_self" class="btn btn-large right primary-color waves-effect waves-light txt-white" style="color: #ffffff;">Next</a>
                                <?php
                            }
                            ?>
                        </div>
                        <?php 
                    }
               
            }
            else { ?>
                <script type="text/javascript">
                    window.location.href = "home.php";
                </script>
                <?php
            }
            ?>
            </div>
				
            
			
        </div>
    </div>

    <script src="js/chart.min.js"></script>
    <script src="js/plotly-latest.min.js"></script>
    <script type="text/javascript">
        $('#btn_cj').click(function() {
            var jam = $('#chart_jam :selected').val();
            var url = "chart_member_sales.php?chart=hari&tgl=<?php echo @$_GET['tgl']; ?>&bulan=<?php echo $bulan; ?>&tahun=<?php echo $tahun; ?>" + "&jam=" + jam + "&loc=<?php echo $loc; ?> target='_self'";

            window.location.href = url;
        });

        var trace1 = {
            x: <?php echo json_encode($json_label); ?>, 
            y: <?php echo json_encode($json_data1); ?>,
            type: 'bar',
            name: 'Motor',
            marker: {
                color: '#f0ff00',
                opacity: 1
            }
        };

        var trace2 = {
            x: <?php echo json_encode($json_label); ?>,
            y: <?php echo json_encode($json_data2); ?>,
            type: 'bar',
            name: 'Mobil',
            marker: {
                color: '#0000ff',
                opacity: 1
            }
        };

        var trace3 = {
            x: <?php echo json_encode($json_label); ?>,
            y: <?php echo json_encode($json_data3); ?>,
            type: 'bar',
            name: 'Total',
            marker: {
                color: '#555555',
                opacity: 1
            }
        };
        
        var data = [trace1, trace2, trace3];

        var layout = {
            autosize: false,
            width: 1400,
            height: 500,
            title: '<?php echo $title; ?>',
            xaxis: { type: 'category', fixedrange: true },
            yaxis: { fixedrange: true },
            barmode: 'group'
        };

        Plotly.newPlot('container', data, layout, {displaylogo: false});
        
        
        var trace4 = {
            x: <?php echo json_encode($json_label2); ?>, 
            y: <?php echo json_encode($json_data4); ?>,
            type: 'bar',
            name: 'In',
            marker: {
                color: '#555555',
                opacity: 1
            }
        };

        var trace5 = {
            x: <?php echo json_encode($json_label2); ?>,
            y: <?php echo json_encode($json_data5); ?>,
            type: 'bar',
            name: 'Out',
            marker: {
                color: '#0000ff',
                opacity: 1
            }
        };

        var trace6 = {
            x: <?php echo json_encode($json_label2); ?>,
            y: <?php echo json_encode($json_data6); ?>,
            type: 'bar',
            name: 'In Area',
            marker: {
                color: '#ff0000',
                opacity: 1
            }
        };
        
        var data2 = [trace4, trace5, trace6];

        var layout2 = {
            autosize: false,
            width: 1400,
            height: 500,
            title: '<?php echo $title2; ?>',
            xaxis: { type: 'category', fixedrange: true },
            yaxis: { fixedrange: true },
            barmode: 'group'
        };

        Plotly.newPlot('container2', data2, layout2, {displaylogo: false});
    </script>
    
<?php
require('footer.php');
?>

