<?php require('header.php');?>
<?php require('sidebar-right.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<script language="javascript" type="text/javascript">
function showCompany(type) {
document.frm.submit();
}
</script>

<?php
if($_SESSION['RequestExpired'] < date('Y-m-d H:i:s')){
    unset($_SESSION['RequestID']);
    unset($_SESSION['RequestLink']);
    unset($_SESSION['RequestExpired']);
    unset($_SESSION['RequestMember']);
    unset($_SESSION['RequestMemberName']);

    $_SESSION['error-type'] = 'info';
    $_SESSION['error-message'] = 'Your Session Has Expired. Retry input User ID Baronang App.';
    $_SESSION['error-time'] = time() + 5;
    echo "<script language='javascript'>document.location='identity_cs.php';</script>";
}
?>
<?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
    <div class="alert alert-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-warning ?>"></i> <?php echo ucfirst($_SESSION['error-type']); ?></h4>
        <?php echo $_SESSION['error-message']; ?>
    </div>
<?php } ?>

<div class="animated fadeinup delay-1">
    <div class="page-content">
        <h2 class="uppercase"><?php echo lang('Tutup Tabungan'); ?></h2> <br>
            <form class="form-horizontal" action="" method = "post" name="frm" id="frm">
            
            <div class="box-body">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="csoaregsavmember" style="text-align: left;">Member ID</label>
                        <input type="number" name="member" class="form-control" id="csoaregsavmember" placeholder="" value="<?php echo $_SESSION['RequestMember']; ?>" readonly>
                                
                        <label for="nama" style="text-align: left;">Name</label>
                        <input type="text" name="nama" class="form-control" id="nama" placeholder="" value="<?php echo $_SESSION['RequestMemberName']; ?>" disabled>        
                    </div>
                </div>
                <div class="col-sm-12">
                        <div class="row">
                            <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-lg-12 table-responsive">
                                <label ><?php echo lang('Pilih Tabungan'); ?></label>
                                <select id="ind" name="ind" class="browser-default" value="" onChange="showCompany(this.value);">
                                    <option value=''> <?php echo ('Semua Tabungan'); ?> </option>
                                    <?php
                                     $julsql   = "select * from [dbo].[RegularSavingAcc] where MemberID='$_SESSION[RequestMember]' and status = '1' and AccountNo not in ( select AccNo from dbo.RegularSavingClose )";
                                    //echo $julsql;
                                    $julstmt = sqlsrv_query($conn, $julsql);
                                    while($rjulrow  = sqlsrv_fetch_array( $julstmt, SQLSRV_FETCH_NUMERIC)){
                                    ?>
                                    <option value="<?=$rjulrow[2];?>" <?php if($rjulrow[2]==$_POST['ind']){echo "selected";} ?>><?=$rjulrow[2];?></option>
                                    <?php } ?>
                                </select>
                            
                             
                                <?php
                                if ($_POST['ind']){
                                $acc = $_POST['ind'];
                                $a = "select * from [dbo].[RegSavingAccView] where KID='$_SESSION[KID]' and AccountNo='$acc'";
                                //echo $a;
                                $b =  sqlsrv_query($conn, $a);
                                while ($c = sqlsrv_fetch_array( $b, SQLSRV_FETCH_NUMERIC)) {
                                    $qrs = "select * from dbo.RegularSavingType where RegularSavingType = '$c[3]'";
                                    //echo $qrs;
                                    $rst = sqlsrv_query($conn, $qrs);
                                    $stu = sqlsrv_fetch_array ($rst, SQLSRV_FETCH_NUMERIC);
                                
                                ?>
                                <div class="Active" style="padding-top: 30px">
                                    <label><?php echo ('No Rekening'); ?></label>
                                    <input type="hidden" name="acc" id="acc" value="<?php echo $c[2]; ?>" readonly>
                                    <input type="text" name="" class="validate" id="" value="<?php echo $c[2]; ?>" readonly>
                                
                                </div>

                                <div class="Active">
                                    <label><?php echo ('Nama'); ?></label>
                                    <input type="hidden" name="name" id="name" value="<?php echo $c[7]; ?>" readonly>
                                    <input type="text" name="" class="validate" id="" value="<?php echo $c[7]; ?>" readonly>
                                    
                                </div>

                                <div class="Active">
                                    <label><?php echo ('Saldo'); ?></label>
                                    <input type="hidden" name="saldo" id="saldo" value="<?php echo $c[6]; ?>" readonly>
                                    <input type="text" name="" class="validate" id="" value="<?php echo $c[6]; ?>" readonly>
                                    
                                </div>

                                <div class="Active">
                                    <label><?php echo ('Biaya Penutupan Rekening'); ?></label>
                                    <input type="hidden" name="biaya" id="biaya" value="<?php echo $stu[9]; ?>" readonly>
                                    <input type="text" name="" class="validate" id="" value="<?php echo number_format($stu[9],2); ?>" readonly>
                                    
                                </div>

                                <div class="Active">
                                <label ><?php echo lang('Metode Bayar'); ?></label>
                                <select id="type" name="type" class="browser-default" value="" onChange="showCompany(this.value);">
                                    <option value=''> <?php echo ('Pilih Metode Bayar'); ?> </option>
                                    <?php
                                    $julsql   = "select * from [dbo].[akunbayar]";
                                    //echo $julsql;
                                    $julstmt = sqlsrv_query($conn, $julsql);
                                    while($rjulrow  = sqlsrv_fetch_array( $julstmt, SQLSRV_FETCH_NUMERIC)){
                                    ?>
                                    <option value="<?=$rjulrow[1];?>" <?php if($rjulrow[1]==$_POST['type']){echo "selected";} ?>><?=$rjulrow[1];?></option>
                                    <?php } ?>
                                </select>
                                </div>

                                <?php } ?>
                
                 </div>
                 <div class="col-sm-12" style="padding-top: 50px;">
                        <div class="row">
                            <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-lg-12 table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>No Transaksi</th>
                                            <th>Tanggal Transaksi</th>
                                            <th><?php echo lang('Tipe Transaksi'); ?></th>
                                            <th><?php echo lang('Debit'); ?></th>
                                            <th><?php echo lang('Kredit'); ?></th>
                                        </tr>
                                    </thead>
                        
                                    <?php

                                        $aaa = "SELECT top 5* FROM ( SELECT [TransactionNumber],[AccNumber],[TimeStam],[KodeTransactionType],[Descriptions],[Note],[DebetHide],[KreditHide],[Debet], ROW_NUMBER() OVER (ORDER BY TimeStam Desc) as row FROM [dbo].[RegularSavingTransView] where AccNumber = '$_POST[ind]') a";
                                        //echo $aaa;

                                    $bbb = sqlsrv_query($conn,$aaa );
                                    while($ccc = sqlsrv_fetch_array($bbb, SQLSRV_FETCH_NUMERIC)){
                
                                    ?>
                                        <tr>
                                            <td><?php echo $ccc[9]; ?></td>
                                            <td><?php echo $ccc[0]; ?></td>
                                            <td><?php echo $ccc[2]->format('Y-m-d H:i:s'); ?></td>
                                            <td>
                                                <?php
                                                $aaaa = "select* from dbo.TransactionType where KodeTransactionType='".$ccc[3]."'";
                                                $bbbb = sqlsrv_query($conn, $aaaa);
                                                $cccc = sqlsrv_fetch_array($bbbb, SQLSRV_FETCH_NUMERIC);
                                                if($cccc != null){
                                                    echo $cccc[1];
                                                }
                                                else{
                                                    echo $ccc[3];
                                                }
                                                ?>
                                            </td>
                                            <td style="text-align: right;"><?php echo number_format($ccc[6],2); ?></td>
                                            <td style="text-align: right;"><?php echo number_format($ccc[7],2) ; ?></td>
                                        </tr>
                                    <?php } ?>    
                                    <?php } ?>
                                </table>
                            </div>
                        </div>

                </div>
            
            </div>
            
            </form> 
            <form action="proccscaregsaving.php" method="post" class="form-horizontal">
                <?php 
                if ($_POST['member'] and $_POST['acc'] and $_POST['type'] ){
                    ?>  

                    <div>
                        <input type="hidden" name="member" id="member" value="<?php echo $_POST['member']; ?>" readonly>
                        <input type="hidden" name="acc" id="acc" value="<?php echo $_POST['acc']; ?>" readonly>
                        <input type="hidden" name="type" id="type" value="<?php echo $_POST['type']; ?>" readonly>
                    </div>     
                        <div class="box-footer">
                                <button type="submit" class="waves-effect waves-light btn-large primary-color width-100" style="margin-top: 20px;">Close</button>

                                <a href="identity_cs.php"><button type="button" class="waves-effect waves-light btn-large primary-color width-100" style="margin-top: 20px;">Back</button></a>
                        </div>
                    <?php } ?>
            </form>
    </div>
</div>



<?php require('content-footer.php');?>

<?php require('footer.php');?>
