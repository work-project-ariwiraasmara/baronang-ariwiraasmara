<?php
@session_start();
error_reporting(0);
require('header.php');
require('sidebar-right.php');
require('sidebar-left.php');
?>

	<div class="animated fadeinup delay-1">
        <div class="page-content txt-black">

        	<h1 class="uppercase txt-black">Import Registrasi Compliment</h1><br>

        	<form action="" method="post" enctype="multipart/form-data">
                <div class="m-t-30">
        		
            		<div class="">
            			<span>Pilih Lokasi</span> : <br>
            			<select class="browser-default" name="loc">
            				<?php
            				$sloc = "SELECT * from [dbo].[LocationMerchant] order by Nama ASC";
            				$qloc = sqlsrv_query($conn, $sloc);
            				while($dloc = sqlsrv_fetch_array($qloc)) { ?>
            					<option value="<?php echo $dloc[0]; ?>"><?php echo $dloc[2]; ?></option>
            					<?php
            				}
            				?>
            			</select>
            		</div>

            		<div class="m-t-30">
            			<span>Exp Date</span> : <br>
            			<input type="date" name="expdate">
            		</div>

            		<div class="m-t-30">
            			<span>Vehicle Type</span> : <br>
            			<select class="browser-default" name="vhc">
            				<?php
            				$svhc = "SELECT * from [dbo].[VehicleType] order by Name ASC";
            				$qvhc = sqlsrv_query($conn, $svhc);
            				while($dvhc = sqlsrv_fetch_array($qvhc)) { ?>
            					<option value="<?php echo $dvhc[0]; ?>"><?php echo $dvhc[1]; ?></option>
            					<?php
            				}
            				?>
            			</select>
            		</div>

                    <div class="m-t-30">
                        <span>Upload File</span> <br>
                        <input type="file" name="fxcl" accept=".xls">
                    </div>

            	</div>

            	<div class="m-t-30">
            		<button name="ok" class="btn btn-large width-100 waves-effect waves-light primary-color">import</button>
            	</div>

            </form>

            <?php /*
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th colspan="4" style="text-align: center;">Format Excel</th>
                        </tr>
                    </thead>

                    <tbody>
                        <tr>
                            <th>QR</th>
                            <th>Member ID</th>
                            <th>Nomor Kartu</th>
                            <th>User ID</th>
                        </tr>
                    </tbody>
                </table>
            </div>
            */ ?>

            <?php
            if(isset($_POST['ok'])) {
                include "excel_reader2.php";

                $loc = @$_POST['loc'];
                $tgl = @$_POST['expdate'];
                $vhl = @$_POST['vhc'];

                $target = basename($_FILES['fxcl']['name']) ;
                move_uploaded_file($_FILES['fxcl']['tmp_name'], $target);
                chmod($_FILES['fxcl']['name'],0777);

                $data = new Spreadsheet_Excel_Reader($_FILES['fxcl']['name'],false);
                $jumlah_baris = $data->rowcount($sheet_index=0);
                // jumlah default data yang berhasil di import

                $sl1 = "SELECT RegistrasiComplimentID from [dbo].[RegistrasiCompliment] Order By RegistrasiComplimentID DESC";
                $ql1 = sqlsrv_query($conn, $sl1);
                $dl1 = sqlsrv_fetch_array($ql1);

                $gl1 = substr($dl1[0], -3);
                $gdl1 = (int)$gl1 + 1;
                $itdl1 = str_pad($gdl1, 3, '0', STR_PAD_LEFT);
                $idl1 = '700097REGCMP'.date('dm').$itdl1;

                $tgnow = date('Y-m-d H:i:s');
                $dr = date('Y-m-d').' 00:00:00';
                $jml = $jumlah_baris - 1;

                $s1 = "INSERT into [dbo].[RegistrasiCompliment](RegistrasiComplimentID, LocationID, VehicleID, ExpireDate, Quantity, DateRegister, DateReceipt) 
                        values('$idl1', '$loc', '$vhl', '$tgl', '$jml', '$tgnow', '$dr')";
                //echo $s1.'<br><br>';
                //$q1 = sqlsrv_query($conn, $s1);

                $berhasil = 0;
                $sl2 = "SELECT AktivasiComplimentID from [dbo].[AktivasiCompliment] Order By AktivasiComplimentID DESC";
                $ql2 = sqlsrv_query($conn, $sl2);
                $dl2 = sqlsrv_fetch_array($ql2);

                $gl2 = substr($dl2[0], -6);
                //$gdl2 = (int)$gl2 + 1;
                //$itdl2 = str_pad($gdl2, 0, '0', STR_PAD_LEFT);
                //$idl2 = '700097CMT'.$itdl2;
                $idlr2 = '700097CMT'.date('dm');
                $iidl2 = 1;

                //$itdl2 = str_pad($iidl2, 3, '0', STR_PAD_LEFT);
                $iidl2 = (int)$gl2 + 1;

                $col_nocard = 0;
                for($bs = 1; $bs < 51; $bs++) {
                    if($data->val(1, $bs) == 'No. Kartu' || $data->val(1, $bs) == 'No.Kartu' || $data->val(1, $bs) == 'No.kartu' || $data->val(1, $bs) == 'Nomor Kartu' || $data->val(1, $bs) == 'No Card' || $data->val(1, $bs) == 'NoCard' || $data->val(1, $bs) == 'No. Card' || $data->val(1, $bs) == 'CardNo' || $data->val(1, $bs) == 'Card Number') {
                        $col_nocard = $bs;
                    }
                }
                //echo 'No. Kartu dikolom: '.$col_nocard.'<br><br>';

                for ($i=2; $i<=$jumlah_baris; $i++){
                    // menangkap data dan memasukkan ke variabel sesuai dengan kolumnya masing-masing
                    //$qr = $data->val($i, 1);
                    $qr = $col_nocard;
                    if($qr != ""){
                        //if( ($data->val($i, $qr) != '') || empty($data->val($i, $qr)) || is_null($data->val($i, $qr)) ) {
                        // input data ke database (table data_pegawai)
                        //mysqli_query($koneksi,"INSERT into data_pegawai values('','$nama','$alamat','$telepon')");
                       
                        $idl2 = $idlr2.$iidl2;
                        $s2 = "INSERT into [dbo].[AktivasiCompliment] values('$idl2', '".$data->val($i, $qr)."', '', '$idl1')";
                        //echo $i.'-1). '.$s2.'<br>';
                        //$q2 = sqlsrv_query($conn, $s2);
                        
                        $sp3 = "SELECT LocationID, VehicleID, Status, DateRegister, ValidDate from [dbo].[MemberLocation] where Status='1' and LocationID='$loc' and VehicleID='$vhl' and CardNo='".$data->val($i, $qr)."' order by DateRegister DESC";
                        //echo $sp3;
                        $qp3 = sqlsrv_query($conn, $sp3);
                        $dp3 = sqlsrv_fetch_array($qp3);

                        if($dp3[0] > 1) {
                        //if($rp3 > 0) {
                            $s3 = "UPDATE [dbo].[MemberLocation] set Status='2', MemberType='B2B' where LocationID='$loc' and VehicleID='$vhl' and CardNo='".$data->val($i, $qr)."' and Status='1'";
                            $q3 = sqlsrv_query($conn, $s3);
                            //echo 'UPDATED MORE THAN 1! : '.$sp3.'<br><br>';
                            echo ($i-1).'). UPDATED MORE THAN 1! : [dbo].[MemberLocation]<br>';

                            $s32 = "INSERT into [dbo].[MemberLocation] values('', '$loc', 1, '$tgnow', '$tgl', '', '$vhl', '".$data->val($i, $qr)."', 'B2B')";
                            $q32 = sqlsrv_query($conn, $s32);
                            //echo 'INSERTED AFTER UPDATED! : '.$s32.'<br><br>';
                            echo ($i-1).'). INSERTED AFTER UPDATED! : [dbo].[MemberLocation]<br><br>';
                        }
                        else if($dp3[0] < 1) {
                            $s3 = "INSERT into [dbo].[MemberLocation] values('', '$loc', 1, '$tgnow', '$tgl', '', '$vhl', '".$data->val($i, $qr)."', 'B2B')";
                            $q3 = sqlsrv_query($conn, $s3);
                            //echo 'INSERTED! : '.$s3.'<br><br>';
                            echo ($i-1).'). INSERTED! : [dbo].[MemberLocation]<br><br>';
                        }
                        else {
                            $s3 = "UPDATE [dbo].[MemberLocation] set DateRegister='$tgnow', ValidDate='$tgl', Status='1', MemberType='B2B' where LocationID='$loc' and VehicleID='$vhl' and CardNo='".$data->val($i, $qr)."' and Status='1'";
                            $q3 = sqlsrv_query($conn, $s3);
                            //echo 'UPDATED! : '.$s3.'<br><br>';
                            echo ($i-1).'). UPDATED! : [dbo].[MemberLocation]<br><br>';
                        }

                        //
                        //$s3 = "INSERT into [dbo].[MemberLocation] values('".$data->val($i, 2)."', '$loc', 1, '$tgnow', '$tgl', '".$data->val($i, 4)."', '$vhl', '".$data->val($i, 3)."')";
                        //$q3 = sqlsrv_query($conn, $s3);

                        for($xi = date('m', strtotime($tgnow)); $xi <= date('m', strtotime($tgl)); $xi++) {
                            $sxi = "SELECT Count(*) from [dbo].[MemberLocationMonth] where CardNo='".$data->val($i, $qr)."' and LocationID='$loc' and VehicleID='$vhl' and Bulan='".date('m', strtotime($expdate))."' and Tahun='".date('Y', strtotime($expdate))."'";
                            $qxi = sqlsrv_query($conn, $sxi);
                            $dxi = sqlsrv_fetch_array($qxi);
                            if($dxi[0] < 1) {
                                $s4 = "exec [dbo].[ProsesBuyMemberMonth] '".$data->val($i, $qr)."', '$loc', '$vhl', '$xi', '".date('Y', strtotime($tgl))."'";
                                $q4 = sqlsrv_query($conn, $s4);
                                //echo 'INSERTED exec [dbo].[ProsesBuyMemberMonth]! : '.$s4.'<br><br>';
                                echo ($i-1).'-'.$xi.'). INSERTED [dbo].[ProsesBuyMemberMonth]<br>';
                            }
                            //echo $xi.', ';
                        }

                        //$s4 = "INSERT into [dbo].[MemberLocationMonth] values('".$data->val($i, $qr)."', '".date('m', strtotime($tgl))."', 1, '$loc', '$vhl', '', '".date('Y', strtotime($tgl))."')";
                        //$s4 = "exec [dbo].[ProsesBuyMemberMonth] '".$data->val($i, $qr)."', '$loc', '$vhl', '1', '2020'";
                        //echo  $i.'-3). '.$s4.'<br><br>';
                        //$q4 = sqlsrv_query($conn, $s4);

                        // $s42 = "SELECT Count(*) from [dbo].[MemberLocationMonth] where Year='".date('Y', strtotime($tgl))."' and Bulan='".date('m', strtotime($tgl))."' and VehicleID='700097VEHICLE004' and LocationID='700097LOC0000007' and CardNo='".$data->val($i, 2)."'";
                        // $q42 = sqlsrv_query($conn, $s42);
                        // $d42 = sqlsrv_fetch_array($q42);
                        // echo $i.'). '.$s42.' : '.$d42[0].'<br><br>';
                        
                        // $s43 = "DELETE from [dbo].[MemberLocationMonth] where Year='2019' and Bulan='12' and VehicleID='700097VEHICLE004' and LocationID='700097LOC0000007' and CardNo='".$data->val($i, 2)."'";
                        // echo $i.'). '.$data->val($i, 2).'<br>';
                        // $q43 = sqlsrv_query($conn, $s43);

                        $berhasil++; $iidl2++;
                        echo $data->val($i, $qr).'<br>==========================================================================================================================================<br><br>';
                        //}
                    }
                }

                ?>
                <script type="text/javascript">
                    //alert('Data Berhasil Di Import!');
                    //window.location.href = "?";
                </script>
                <?php
            }
            ?>

        </div>
    </div>

<?php require('footer.php'); ?>