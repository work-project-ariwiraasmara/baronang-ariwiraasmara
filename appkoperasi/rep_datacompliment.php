<?php require('header.php');?>

<?php require('sidebar-right.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>
    

<div class="animated fadeinup delay-1">
    <div class="page-content">

        <h2 class="uppercase"><?php echo lang('Laporan Data Compliment'); ?></h2>   


            <form class="form-horizontal" action="" method = "GET">
                <!-- <div class="input-field">
                    <input type="text" name="tgl1" id="tgl1" class="datepicker" value="<?php echo "$_GET[tgl1]"; ?>" autocomplete="off"></p></td>
                    <label><?php echo ('Tanggal Awal'); ?></label>
                </div>
                
                <div class="input-field">
                    <input type="text" name="tgl2" id="tgl2" class="datepicker" value="<?php echo "$_GET[tgl2]"; ?>" autocomplete="off"></p></td>
                    <label><?php echo ('Tanggal Akhir'); ?></label>
                </div> -->

                <div class="input-field">
                    <select id="akun" name="akun" class="browser-default">
                        <option value=''>- <?php echo lang('Semua Lokasi'); ?> -</option>
                            <?php
                            //$julsql   = "select * from [gateway].[dbo].[EDCList] where SUBSTRING(UserIDBaronang,0,6) = SUBSTRING('700080',0,6)";
                            $julsql   = "select distinct (LocationID) from [dbo].[MemberLocation] where status = '1' order by LocationID asc";
                            echo $julsql1;
                            $julstmt = sqlsrv_query($conn, $julsql);

                            while($rjulrow  = sqlsrv_fetch_array( $julstmt, SQLSRV_FETCH_NUMERIC)){
                                $carmemb = "select * from dbo.LocationMerchant where LocationID = '$rjulrow[0]'";
                                $procmemb = sqlsrv_query($conn, $carmemb);
                                $hasilmemb  = sqlsrv_fetch_array( $procmemb, SQLSRV_FETCH_NUMERIC);

                               ?>
                            <option value="<?=$rjulrow[0];?>" <?php if($rjulrow[0]==$_GET['akun']){echo "selected";} ?>>&nbsp;<?=$hasilmemb[0];?>&nbsp;<?=$hasilmemb[2];?></option>
                            <?php } ?>
                    </select>
                </div>

                <div style="margin-top: 30px;">
                    <button type="submit" class="waves-effect waves-light btn-large primary-color width-100"><?php echo lang('Pilih'); ?></button>
                </div>

                <div style="margin-top: 10px; margin-bottom: 30px;">
                    <a href="rep_datacompliment.php"><button type="button" class="waves-effect waves-light btn-large primary-color width-100">Refresh</button></a>
                </div>
            </form>



            <?php if($_GET['akun'] != null){
            $tgl1 = date('Y-m-d 00:00:00.000', strtotime($_GET['tgl1']));
            $tgl2 = date('Y-m-d 23:59:59.999', strtotime($_GET['tgl2']));
            $akun = $_GET['akun'];
            ?>

                <div class="box-header" align="center" style="margin-top: 30px; margin-bottom: 30px">
                <a href="drep_datacomplement.php?akun=<?php echo $akun; ?>"><button type="button" class="waves-effect waves-light btn-large primary-color width-100">Download to excel</button></a>
                </div>                 
                              
                <div class="row">
                    <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-lg-12 table-responsive">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th><?php echo 'No.'; ?></th>
                                    <th><?php echo 'No. Kartu'; ?></th>
                                    <th><?php echo 'Tanggal Register'; ?></th>
                                    <th><?php echo 'Bulan Valid'; ?></th>
                                    <th><?php echo 'Tahun Valid'; ?></th>
                                    <th><?php echo 'ID Lokasi'; ?></th>
                                    <th><?php echo 'Nama Lokasi'; ?></th>
                                    <th><?php echo 'Vehicle'; ?></th>
                                    <th><?php echo 'Nama'; ?></th>
                                    <th><?php echo 'Alamat'; ?></th>
                                    <th><?php echo 'No Tlp.'; ?></th>
                                    <th><?php echo 'Email'; ?></th> 
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                            //count
                            $from1 = $_GET['tahun'].'/'.$_GET['bulan'].'/01';
                            $from = date('Y-m-01', strtotime($from1));
                            $to3 = $_GET['tahun'].'/'.$_GET['bulan'].'/01';
                            $to = date('Y-m-t', strtotime($to3));
                            $akun = $_GET['akun'];
                            //count
                            $jmlulsql   = "SELECT count (*) from(select a.CardNo, b.DateRegister, (select max(Bulan) from dbo.MemberLocationMonth where CardNo= a.CardNo) as ValidMonth, (select max(Year) from dbo.MemberLocationMonth where CardNo= a.CardNo) as ValidYear, (select LocationID from dbo.LocationMerchant where LocationID= b.LocationID) as IDLocation, (select Nama from dbo.LocationMerchant where LocationID= b.LocationID) as Location, (select Name from dbo.VehicleType where VehicleID= b.VehicleID) as Vehicle, c.Name, c.Addr, c.Phone, c.email, row_number() over (ORDER BY b.Dateregister asc) as row from dbo.MemberCard a inner join dbo.MemberLocation b on a.CardNo = b.CardNo left join dbo.MemberList c on a.MemberID = c.MemberID left join dbo.AktivasiCompliment d on a.CardNo = d.TipeQR where a.Status = 1 and a.CardNo not like '%100010001000%' and locationid = '$akun' and (select max(Bulan) from dbo.MemberLocationMonth where CardNo= a.CardNo) = '12') a ";
                            //echo $jmlulsql;
                            $jmlulstmt  = sqlsrv_query($conn, $jmlulsql);
                            $jmlulrow   = sqlsrv_fetch_array( $jmlulstmt, SQLSRV_FETCH_NUMERIC);

                            //pagging
                            $perpages   = 10;
                            $halaman    = $_GET['page'];
                            if(empty($halaman)){
                                $posisi  = 0;
                                $batas   = $perpages;
                                $halaman = 1;
                            }
                            else{
                                $posisi  = (($perpages * $halaman) - 10) + 1;
                                $batas   = ($perpages * $halaman);
                            }


                                $a = "SELECT * from(select a.CardNo, b.DateRegister, (select max(Bulan) from dbo.MemberLocationMonth where CardNo= a.CardNo) as ValidMonth, (select max(Year) from dbo.MemberLocationMonth where CardNo= a.CardNo) as ValidYear, (select LocationID from dbo.LocationMerchant where LocationID= b.LocationID) as IDLocation, (select Nama from dbo.LocationMerchant where LocationID= b.LocationID) as Location, (select Name from dbo.VehicleType where VehicleID= b.VehicleID) as Vehicle, c.Name, c.Addr, c.Phone, c.email, row_number() over (ORDER BY b.Dateregister asc) as row from dbo.MemberCard a inner join dbo.MemberLocation b on a.CardNo = b.CardNo left join dbo.MemberList c on a.MemberID = c.MemberID left join dbo.AktivasiCompliment d on a.CardNo = d.TipeQR where a.Status = 1 and a.CardNo not like '%100010001000%' and locationid = '$akun' and (select max(Bulan) from dbo.MemberLocationMonth where CardNo= a.CardNo) = '12') a where row between '$posisi' and '$batas' order by row asc";

                                $b = sqlsrv_query($conn, $a);
                                while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                               
                                    ?>                 
                            <tr>
                                <td><?php echo $c[11]; ?></a></td>   
                                <td><?php echo $c[0]; ?></a></td>
                                <td><?php echo $c[1]->format('Y-m-d H:i:s'); ?></td>
                                <td><?php echo $c[2]; ?></a></td>
                                <td><?php echo $c[3]; ?></a></td>
                                <td><?php echo $c[4]; ?></a></td>
                                <td><?php echo $c[5]; ?></a></td>
                                <td><?php echo $c[6]; ?></a></td>
                                <td><?php echo $c[7]; ?></a></td>
                                <td><?php echo $c[8]; ?></a></td>
                                <td><?php echo $c[9]; ?></a></td>
                                <td><?php echo $c[10]; ?></a></td>
                                
                            </tr>
                            
                            <?php $no3++; $jmlhalaman = ceil($jmlulrow[0]/$perpages); } ?>
                            </tbody>
                        </table>
                        <div class="box-footer clearfix right">
                            <div style="text-align: center;">
                                Showing <?=$halaman?> to <?=$jmlhalaman?> of <?=$jmlulrow[0]?> entries <?=$posisi." - ".$batas?>
                                <?php
                                $akun = $_GET['akun'];
                                $bulan = $_GET['bulan'];
                                $tahun = $_GET['tahun'];
                                $reload = "rep_datacompliment.php?akun=$akun";
                                $page = intval($_GET["page"]);
                                $adjacents = isset($_GET['adjacents']) ? intval($_GET['adjacents']) : 2; // khusus style pagination 2 dan 3
                                if( $page == 0 ) $page = 1;

                                $tpages = ($jmlhalaman) ? ceil($jmlhalaman) : 0; // total pages, last page number
                                echo "<div>".paginate($reload, $page, $tpages)."</div>";
                                ?>
                            </div>
                        </div>    
                    </div>
                </div>    
            <?php } ?>
            <!-- Tidak Ada Akun -->
            <?php if($_GET['akun'] == ''){
            ?>

                <div class="box-header" align="center" style="margin-top: 30px; margin-bottom: 30px">
                <a href="drep_datacomplement.php?"><button type="button" class="waves-effect waves-light btn-large primary-color width-100">Download to excel</button></a>
                </div> 


                              
                <div class="row">
                    <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-lg-12 table-responsive">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th><?php echo 'No.'; ?></th>
                                    <th><?php echo 'No. Kartu'; ?></th>
                                    <th><?php echo 'Tanggal Register'; ?></th>
                                    <th><?php echo 'Bulan Valid'; ?></th>
                                    <th><?php echo 'Tahun Valid'; ?></th>
                                    <th><?php echo 'ID Lokasi'; ?></th>
                                    <th><?php echo 'Nama Lokasi'; ?></th>
                                    <th><?php echo 'Vehicle'; ?></th>
                                    <th><?php echo 'Nama'; ?></th>
                                    <th><?php echo 'Alamat'; ?></th>
                                    <th><?php echo 'No Tlp.'; ?></th>
                                    <th><?php echo 'Email'; ?></th> 
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                            //count
                            $jmlulsql   = "SELECT count (*) from(select a.CardNo, b.DateRegister, (select max(Bulan) from dbo.MemberLocationMonth where CardNo= a.CardNo) as ValidMonth, (select max(Year) from dbo.MemberLocationMonth where CardNo= a.CardNo) as ValidYear, (select LocationID from dbo.LocationMerchant where LocationID= b.LocationID) as IDLocation, (select Nama from dbo.LocationMerchant where LocationID= b.LocationID) as Location, (select Name from dbo.VehicleType where VehicleID= b.VehicleID) as Vehicle, c.Name, c.Addr, c.Phone, c.email, row_number() over (ORDER BY b.Dateregister asc) as row from dbo.MemberCard a inner join dbo.MemberLocation b on a.CardNo = b.CardNo left join dbo.MemberList c on a.MemberID = c.MemberID left join dbo.AktivasiCompliment d on a.CardNo = d.TipeQR where a.Status = 1 and a.CardNo not like '%100010001000%' and (select max(Bulan) from dbo.MemberLocationMonth where CardNo= a.CardNo) = '12') a ";

                            $jmlulstmt  = sqlsrv_query($conn, $jmlulsql);
                            $jmlulrow   = sqlsrv_fetch_array( $jmlulstmt, SQLSRV_FETCH_NUMERIC);

                            //pagging
                            $perpages   = 10;
                            $halaman    = $_GET['page'];
                            if(empty($halaman)){
                                $posisi  = 0;
                                $batas   = $perpages;
                                $halaman = 1;
                            }
                            else{
                                $posisi  = (($perpages * $halaman) - 10) + 1;
                                $batas   = ($perpages * $halaman);
                            }
                           

                                $a = "SELECT * from(select a.CardNo, b.DateRegister, (select max(Bulan) from dbo.MemberLocationMonth where CardNo= a.CardNo) as ValidMonth, (select max(Year) from dbo.MemberLocationMonth where CardNo= a.CardNo) as ValidYear, (select LocationID from dbo.LocationMerchant where LocationID= b.LocationID) as IDLocation, (select Nama from dbo.LocationMerchant where LocationID= b.LocationID) as Location, (select Name from dbo.VehicleType where VehicleID= b.VehicleID) as Vehicle, c.Name, c.Addr, c.Phone, c.email, row_number() over (ORDER BY b.Dateregister asc) as row from dbo.MemberCard a inner join dbo.MemberLocation b on a.CardNo = b.CardNo left join dbo.MemberList c on a.MemberID = c.MemberID left join dbo.AktivasiCompliment d on a.CardNo = d.TipeQR where a.Status = 1 and a.CardNo not like '%100010001000%' and (select max(Bulan) from dbo.MemberLocationMonth where CardNo= a.CardNo) = '12') a where row between '$posisi' and '$batas' order by row asc";
                                $b = sqlsrv_query($conn, $a);
                                while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                               
                                    ?>                 
                            <tr>
                                <td><?php echo $c[11]; ?></a></td>   
                                <td><?php echo $c[0]; ?></a></td>
                                <td><?php echo $c[1]->format('Y-m-d H:i:s'); ?></td>
                                <td><?php echo $c[2]; ?></a></td>
                                <td><?php echo $c[3]; ?></a></td>
                                <td><?php echo $c[4]; ?></a></td>
                                <td><?php echo $c[5]; ?></a></td>
                                <td><?php echo $c[6]; ?></a></td>
                                <td><?php echo $c[7]; ?></a></td>
                                <td><?php echo $c[8]; ?></a></td>
                                <td><?php echo $c[9]; ?></a></td>
                                <td><?php echo $c[10]; ?></a></td>
                                
                            </tr>
                            
                            <?php $no3++; $jmlhalaman = ceil($jmlulrow[0]/$perpages); } ?>
                            </tbody>
                        </table>
                        <div class="box-footer clearfix right">
                            <div style="text-align: center;">
                                Showing <?=$halaman?> to <?=$jmlhalaman?> of <?=$jmlulrow[0]?> entries <?=$posisi." - ".$batas?>
                                <?php
                                $akun = $_GET['akun'];
                                $bulan = $_GET['bulan'];
                                $tahun = $_GET['tahun'];
                                $reload = "rep_datacompliment.php?";
                                $page = intval($_GET["page"]);
                                $adjacents = isset($_GET['adjacents']) ? intval($_GET['adjacents']) : 2; // khusus style pagination 2 dan 3
                                if( $page == 0 ) $page = 1;

                                $tpages = ($jmlhalaman) ? ceil($jmlhalaman) : 0; // total pages, last page number
                                echo "<div>".paginate($reload, $page, $tpages)."</div>";
                                ?>
                            </div>
                        </div>    
                    </div>
                </div>    
            <?php } ?>                        
            <!-- di dekat tanggal -->
            </div>
            </div>
    </div>
</div>


<?php
function paginate($reload, $page, $tpages) {

    $firstlabel = "First";
    $prevlabel  = "Prev";
    $nextlabel  = "Next";
    $lastlabel  = "Last";

    $out = "<div class=\"backpaging\"><ul class=\"ownpaging\">\n";

    // first
    if($page>1) {
        $out.= "<li><a href=\"" . $reload . "\">" . $firstlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $firstlabel . "</a></li>\n";
    }

    // previous
    if($page==1) {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $prevlabel . "</a></li>\n";
    }
    elseif($page==2) {
        $out.= "<li><a href=\"" . $reload . "\">" . $prevlabel . "</a></li>\n";
    }
    else {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" . ($page-1) . "\">" . $prevlabel . "</a></li>\n";
    }

    // current
    $out.= "<li class=\"active\"><a href=\"#\">Page " . $page . " of " . $tpages . "</a></li>\n";

    // next
    if($page<$tpages) {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" .($page+1) . "\">" . $nextlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $nextlabel . "</a></li>\n";
    }

    // last
    if($page<$tpages) {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" . $tpages . "\">" . $lastlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $lastlabel . "</a></li>\n";
    }

    $out.= "</ul></div>";

    return $out;
}
?>

<?php require('footer.php'); ?>
