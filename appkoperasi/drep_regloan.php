<?php
session_start();
error_reporting(0);

include('lib/phpExcel/Classes/PHPExcel.php');
include "connect.php";


    $filePath = "uploads/excel/";

    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getProperties()->setCreator("baronang");
    $objPHPExcel->getProperties()->setTitle("Drep Produk Pinjaman");

// set autowidth
    for($col = 'A'; $col !== 'Z'; $col++) {
        $objPHPExcel->getActiveSheet()
            ->getColumnDimension($col)
            ->setAutoSize(true);
    }

    $listCombo = array();

    //sheet 2
    $objWorkSheet = $objPHPExcel->createSheet(0);
// baris judul
   
    $objWorkSheet->getStyle('C1')->getFont()->setBold(true);
    $objWorkSheet->getStyle('C1')->getFont()->setSize(14);
    $objWorkSheet->SetCellValue('C1', 'Detail Transaksi');

    $objWorkSheet->getStyle('C2')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('C2', 'Produk Pinjaman :');

    $objWorkSheet->getStyle('A3')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('A3', 'Akun : ');

    $objWorkSheet->getStyle('A4')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('A4', 'Nama : ');

    $objWorkSheet->getStyle('A5')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('A5', 'Pinjaman Awal : ');

    $objWorkSheet->getStyle('A6')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('A6', 'Total Bayar Pinjaman : ');

    $objWorkSheet->getStyle('A7')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('A7', 'Sisa Pinjaman : ');    


    $objWorkSheet->getStyle('A9')->getFont()->setBold(true);
    $objWorkSheet->getStyle('A9' )->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('A9', 'No');
    $objWorkSheet->getStyle('B9')->getFont()->setBold(true);
    $objWorkSheet->getStyle('B9')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('B9', 'No Transaksi');
    $objWorkSheet->getStyle('C9')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->getStyle('C9')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('C9', 'Tanggal Transaksi');
    $objWorkSheet->getStyle('D9')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->getStyle('D9')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('D9', 'Tipe Transaksi');
    $objWorkSheet->getStyle('E9')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->getStyle('E9')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('E9', 'Angsuran Pokok');
    $objWorkSheet->getStyle('F9')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->getStyle('F9')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('F9', 'Bunga');
    $objWorkSheet->getStyle('G9')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->getStyle('G9')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('G9', 'Pokok + Bunga');   
 


    $acc= $_GET['acc2'];
    $disable = "readonly";

    if($_GET['st'] == 4){


    $k = 2;
    $l = 3;
    $m = 4;
    $n = 5;
    $o = 6;
    $p = 7;

    $xxx = "SELECT * FROM [dbo].[LoanReportBalance] WHERE LoanNumber='$acc'";
    echo $x;
    $yyy = sqlsrv_query($conn, $xxx);
    while($zzz = sqlsrv_fetch_array($yyy, SQLSRV_FETCH_NUMERIC)){
                $xyz = "SELECT * FROM [dbo].[MemberListView] WHERE MemberID='$zzz[7]'";
                $yza = sqlsrv_query($conn,$xyz); 
                $zay = sqlsrv_fetch_array($yza, SQLSRV_FETCH_NUMERIC);

                $def   = "select * from [dbo].[LoanTypeViewNew] where KodeLoanType ='$zzz[8]'";
                $efg = sqlsrv_query($conn,$def); 
                $fgh = sqlsrv_fetch_array($efg, SQLSRV_FETCH_NUMERIC);

                $totalj = "select sum (PrincipalPayment) FROM dbo.LoanTransaction where LoanNumber = '$_GET[acc2]'";
                $jumlahb = sqlsrv_query($conn,$totalj); 
                $bayarl = sqlsrv_fetch_array($jumlahb, SQLSRV_FETCH_NUMERIC);
                //echo $totalj;

        $objWorkSheet->getStyle('D'.$k)->getFont()->setBold(true);
        $objWorkSheet->SetCellValue("D".$k,$zzz[4] .'-'. $fgh[9]);

        $objWorkSheet->getStyle('B'.$l)->getFont()->setBold(true);
        $objWorkSheet->SetCellValueExplicit("B".$l,$acc);

        $objWorkSheet->getStyle('B'.$m)->getFont()->setBold(true);
        $objWorkSheet->SetCellValue("B".$m,$zay[2]);

        $objWorkSheet->getStyle('B' .$n)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $objWorkSheet->getStyle('B'.$n)->getFont()->setBold(true);
        $objWorkSheet->SetCellValue("B".$n,$zzz[1],PHPExcel_Cell_DataType::TYPE_STRING);

        $objWorkSheet->getStyle('B' .$o)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $objWorkSheet->getStyle('B'.$o)->getFont()->setBold(true);
        $objWorkSheet->SetCellValue("B".$o,number_format($bayarl[0],2), PHPExcel_Cell_DataType::TYPE_STRING);

        $objWorkSheet->getStyle('B' .$p)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $objWorkSheet->getStyle('B'.$p)->getFont()->setBold(true);
        $objWorkSheet->SetCellValue("B".$p,$zzz[5], PHPExcel_Cell_DataType::TYPE_STRING);

    }

    
    $row = 10;
    $rw = 10;
    
    $x = "SELECT * FROM ( SELECT [TransactionNumber],[LoanNumber],[TimeStamp],[KodeTransactionType],[Description],[InvCount],[PrincipalPayment],[InterestPayment],[UserID], ROW_NUMBER() OVER (ORDER BY TimeStamp asc) as row FROM dbo.LoanTransaction where LoanNumber = '$_GET[acc2]') a";
    //echo $x;
    $y = sqlsrv_query($conn, $x);
    while($z = sqlsrv_fetch_array( $y, SQLSRV_FETCH_NUMERIC)){
        
        $total2 = $z[6]+$z[7];

        $aaaa = "select* from dbo.TransactionType where KodeTransactionType='".$z[3]."'";
        $bbbb = sqlsrv_query($conn, $aaaa);
        $cccc = sqlsrv_fetch_array($bbbb, SQLSRV_FETCH_NUMERIC);
        if($cccc != null){
            $type = $cccc[1];
            }
        else{
            $type = $z[3];
        }
               
        $objWorkSheet->getStyle('A' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objWorkSheet->SetCellValueExplicit("A".$row,$z[9]);
        $objWorkSheet->SetCellValueExplicit("B".$row,$z[0]);
        $objWorkSheet->SetCellValueExplicit("C".$row,$z[2]->format('Y-m-d H:i:s'));
        $objWorkSheet->SetCellValueExplicit("D".$row,$type);
        $objWorkSheet->getStyle('E' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $objWorkSheet->SetCellValue("E".$row,number_format($z[6],2), PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet->getStyle('F' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $objWorkSheet->SetCellValue("F".$row,number_format($z[7],2), PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet->getStyle('G' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $objWorkSheet->SetCellValue("G".$row,number_format($total2,2), PHPExcel_Cell_DataType::TYPE_STRING);

        $row++;
        $rw++;
        }
 }
//exit;
    $objWorkSheet->setTitle('Drep Transaksi Pinjaman');

    $fileName = 'TransLoan'.'_'.strtotime(date('Y-m-d H:i:s')).'.xls';
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');

// download ke client
    header('Content-type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename="'.$fileName.'"');
    $objWriter->save('php://output');

    return $filePath.'/'.$fileName;

?>
