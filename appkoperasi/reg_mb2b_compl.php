<?php
// reg_mb2b_compl
require('header.php');
require('sidebar-left.php');
require('sidebar-right.php');

if(@$_GET['reg'] == 'b2b') {
    $title = 'B2B';
}
else {
    $title = 'Compliment';
}

if(@$_GET['regid']) {
    $btn_text = 'Update';
    $btn_name_id = 'btnupdate';

    if(@$_GET['reg'] == 'b2b') {
        $sql = "SELECT * from [dbo].[RegistrasiB2B] where RegistrasiB2BID='".@$_GET['regid']."'";
    }
    else {
        $sql = "SELECT * from [dbo].[RegistrasiCompliment] where RegistrasiComplimentID='".@$_GET['regid']."'";
    }

    $query = sqlsrv_query($conn, $sql);
    $data = sqlsrv_fetch_array($query, SQLSRV_FETCH_NUMERIC);
}
else {
    $btn_text = 'Simpan';
    $btn_name_id = 'btnok';
}
?>

	<div class="animated fadeinup delay-1">
    	<div class="page-content">

    		<h1 class="uppercase">Registrasi <?php echo $title; ?></h1>

    		<form class="form-horizontal" action="" method="POST">
                <?php
                if(@$_GET['reg'] == 'b2b') { ?>
                    <input type="hidden" name="regid" id="regid" value="<?php echo $KID.'REGB2B'.date('ym'); ?>">
                    <?php
                }
                else { ?>
                    <input type="hidden" name="regid" id="regid" value="<?php echo $KID.'REGCMP'.date('ym'); ?>">
                    <?php
                }

                if(@$_GET['regid']) { ?>
                    <input type="hidden" name="regid_ed" id="regid_ed" value="<?php echo @$_GET['regid']; ?>">
                    <?php
                }
                else { ?>
                    <input type="hidden" name="regid_ed" id="regid_ed" value="<?php echo @$_GET['regid']; ?>">
                    <?php
                }
                ?>


    			<div class="input-field">
    				<span>Pilih Lokasi</span> :
    				<select class="browser-default" name="vhcl" id="vhcl">
    					<?php
    					$sql2 = "SELECT * from [dbo].[LocationMerchant] order by Nama ASC";
    					$query2 = sqlsrv_query($conn, $sql2);
    					while($data2 = sqlsrv_fetch_array($query2, SQLSRV_FETCH_NUMERIC)) { ?>
    						<option value="<?php echo $data2[0]; ?>" <?php if(@$_GET['regid']) { if($data2[0] == $data[1]) echo 'selected'; } ?> ><?php echo $data2[2]; ?></option>
    						<?php
    					}
    					?>
    				</select>
    			</div>

                <?php
                if(@$_GET['regid']) { ?>
                    <div class="input-field">
                        <input type="number" name="amount" id="amount" value="<?php echo $data[4]; ?>">
                        <label for="amount">Amount (Rp.)</label>
                    </div>
                    <?php
                }
                else { ?>
                    <div class="input-field">
                        <input type="number" name="amount" id="amount" <?php if(@$_GET['reg'] == 'compl') echo 'value="0" readonly' ?> >
                        <label for="amount">Amount (Rp.)</label>
                    </div>
                    <?php
                }
                ?>

    			<div class="input-field">
    				<span>Expire Date</span> :
    				<input type="date" name="expdate" id="expdate" value="<?php //if(@$_GET['regid']) echo $data[3]; ?>">
    			</div>

          <div class="input-field">
    				<span>Date Receipt</span> :
    				<input type="date" name="recdate" value="<?php //if(@$_GET['regid']) echo $data[3]; ?>">
    			</div>

    			<div class="input-field">
    				<div class="row">
    					<div class="col s4">
    						<span>Vehicle Type</span> :
    						<select class="browser-default" name="vhcl_type" id="vhcl_type">
    							<?php
    							$sql2 = "SELECT * from [dbo].[VehicleType] order by Name ASC";
		    					$query2 = sqlsrv_query($conn, $sql2);
		    					while($data2 = sqlsrv_fetch_array($query2, SQLSRV_FETCH_NUMERIC)) { ?>
		    						<option value="<?php echo $data2[0]; ?>"><?php echo $data2[1]; ?></option>
		    						<?php
		    					}
		    					?>
    						</select>
    					</div>
    					<div class="col s4">
    						<input type="number" name="qty" id="qty" class="m-t-20">
    					</div>
    					<div class="col s4">
    						<button type="button" id="btnadd_vehicle" class="btn btn-large width-100 primary-color waves-effect waves-light"><i class="ion-android-add"></i></button>
    					</div>
    				</div>

    				<div id="rowadd_vhcl">

    				</div>

                    <?php
                    if(@$_GET['regid']) {
                        $rowed = 0;
                        if(@$_GET['reg'] == 'b2b') {
                            $sql2 = "SELECT * from [dbo].[RegistrasiB2B] where LocationID='".$data[1]."'";
                        }
                        else {
                            $sql2 = "SELECT * from [dbo].[RegistrasiCompliment] where LocationID='".$data[1]."'";
                        }

                        $query2 = sqlsrv_query($conn, $sql2);
                        while($data2 = sqlsrv_fetch_array($query2, SQLSRV_FETCH_NUMERIC)) {
                            $sql3 = "SELECT Name FROM [KOPX700097].[dbo].[VehicleType] where VehicleId='".$data2[2]."'";
                            $query3 = sqlsrv_query($conn, $sql3);
                            $data3 = sqlsrv_fetch_array($query3, SQLSRV_FETCH_NUMERIC);
                            ?>
                            <div class="input-field">
                                <div class="row" id="row-<?php echo $rowed; ?>">
                                    <div class="col s4">
                                        <input type="hidden" name="vhcl_add_ed[]" id="vhcl_add_ed" value="<?php echo $data[2]; ?>">
                                        <input type="text" name="vhcl_name_ed[]" id="vhcl_name_ed" value="<?php echo $data3[0]; ?>" readonly>
                                    </div>
                                    <div class="col s4">
                                        <input type="text" name="qty_add_ed[]" id="qty_add_ed" value="<?php echo $data2[5]; ?>" readonly>
                                    </div>
                                    <div class="col s4">
                                        <span style="font-weight: bold;">Delete?</span><br>
                                        <input type="checkbox" name="isdelete[<?php echo $rowed; ?>]" id="isdelete-<?php echo $rowed; ?>" value="<?php echo $data3[1]; ?>">
                                        <label for="isdelete-<?php echo $rowed; ?>" id="txtdel-<?php echo $rowed; ?>">No</label>
                                    </div>
                                </div>

                                <script type="text/javascript">
                                    $('#isdelete-<?php echo $rowed; ?>').click(function(){
                                        if($("#isdelete-<?php echo $rowed; ?>").is(':checked')) {
                                            $('#txtdel-<?php echo $rowed; ?>').text('Yes');
                                        }
                                        else {
                                            $('#txtdel-<?php echo $rowed; ?>').text('No');
                                        }
                                    });
                                </script>
                            </div>
                            <?php
                        }
                    }
                    ?>
    			</div>

    			<button type="submit" name="ok" id="ok" class="btn btn-large width-100 primary-color waves-effect waves-light">Simpan</button>
    		</form>

            <?php
            $ok = @$_POST['ok'];
            if(isset($ok)) {
                $regid      = @$_POST['regid'];
                $vhcl       = @$_POST['vhcl'];
                $amount     = @$_POST['amount'];
                $expdate    = @$_POST['expdate'];
                $recdate    = @$_POST['recdate'];

                $rowadd = 0;
                foreach (@$_POST['vhcl_add'] as $key) {
                    if(@$_GET['reg'] == 'b2b') {
                        $regf = $KID.'REGB2B'.date('ym');

                        $sqlloc = "SELECT TOP 1 RegistrasiB2BID FROM [dbo].[RegistrasiB2B] ORDER BY RegistrasiB2BID DESC";
                        $query = sqlsrv_query($conn, $sqlloc);
                        $dataloc = sqlsrv_fetch_array($query, SQLSRV_FETCH_NUMERIC);
                    }
                    else {
                        $regf = $KID.'REGCMP'.date('ym');

                        $sqlloc = "SELECT TOP 1 RegistrasiComplimentID FROM [dbo].[RegistrasiCompliment] ORDER BY RegistrasiComplimentID DESC";
                        $query = sqlsrv_query($conn, $sqlloc);
                        $dataloc = sqlsrv_fetch_array($query, SQLSRV_FETCH_NUMERIC);
                    }
                    //echo $dataloc[0].'<br>';
                    $intaidloc = 0;
                    $graidloc = substr($dataloc[0], -3);

                    $gploc = (int)$graidloc + 1;
                    $intaidloc = str_pad($gploc, 3, '0', STR_PAD_LEFT);
                    //echo $dataloc[0].' -- '.$graidloc.' -- '.$intaidloc.'<br>';
                    $regid = $regf.$intaidloc;

                    $vhclid = @$_POST['vhcl_add'][$rowadd];
                    $quota  = @$_POST['qty_add'][$rowadd];

                    $tanggal = date('Y-m-d H:i:s');

                    if(@$_GET['reg'] == 'b2b') {
                        $sql2 = "INSERT into [dbo].[RegistrasiB2B](RegistrasiB2BID, LocationID, VehicleID, ExpireDate, Amount, Quantity, DateRegister, DateReceipt) values('$regid','$vhcl','$vhclid','$expdate','$amount','$quota','$tanggal','$recdate')";
                    }
                    else {
                        $sql2 = "INSERT into [dbo].[RegistrasiCompliment](RegistrasiComplimentID, LocationID, VehicleID, ExpireDate, Amount, Quantity, DateRegister, DateReceipt) values('$regid','$vhcl','$vhclid','$expdate','$amount','$quota','$tanggal','$recdate')";
                    }

                    //echo '#'.$rowadd.' -- '.$sql2.'<br>';
                    $query2 = sqlsrv_query($conn, $sql2);
                    $rowadd++;

                    if(@$_GET['reg'] == 'b2b') { ?>
                        <script type="text/javascript">
                            alert('Simpan Berhasil');
                            window.location.href = "reg_mb2b_compl.php?reg=b2b";
                        </script>
                        <?php
                    }
                    else { ?>
                        <script type="text/javascript">
                            alert('Simpan Berhasil');
                            window.location.href = "reg_mb2b_compl.php?reg=compl";
                        </script>
                        <?php
                    }
                }
            }
            ?>

            <div class="table-responsive m-t-30">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Registrasi ID</th>
                            <th>Lokasi</th>
                            <?php //<th>Kendaraan</th> ?>
                            <th>Expire Date</th>
                            <th style="text-align: center;">Amount (Rp.)</th>
                            <th>Quantity</th>
                            <th></th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php
                        if(@$_GET['reg'] == 'b2b') {
                            $sql = "SELECT * from [dbo].[RegistrasiB2B] Order by RegistrasiB2BID ASC";
                        }
                        else {
                            $sql = "SELECT * from [dbo].[RegistrasiCompliment] Order by RegistrasiComplimentID ASC";
                        }

                        $query = sqlsrv_query($conn, $sql);
                        while($data = sqlsrv_fetch_array($query, SQLSRV_FETCH_NUMERIC)) { ?>
                            <tr>
                                <td><?php echo $data[0]; ?></td>

                                <?php
                                $sql2 = "SELECT Nama from [dbo].[LocationMerchant] where LocationID='".$data[1]."'";
                                $query2 = sqlsrv_query($conn, $sql2);
                                $data2 = sqlsrv_fetch_array($query2, SQLSRV_FETCH_NUMERIC);
                                ?>
                                <td><?php echo $data2[0]; ?></td>

                                <?php
                                /*
                                $sql3 = "SELECT Name from [dbo].[VehicleType] where VehicleID='".$data[2]."'";
                                $query3 = sqlsrv_query($conn, $sql3);
                                $data3 = sqlsrv_fetch_array($query3, SQLSRV_FETCH_NUMERIC);

                                <td><?php echo $data3[0]; ?></td>
                                */
                                ?>
                                <td><?php echo date_format($data[3],"d-m-Y"); ?></td>
                                <td style="text-align: right;"><?php echo number_format($data[4],2,',','.'); ?></td>
                                <td><?php echo $data[5]; ?></td>
                                <td>
                                    <a href="?reg=<?php echo @$_GET['reg']; ?>&regid=<?php echo $data[0]; ?>" class="btn btn-large primary-color waves-light waves-effect"><i class="ion-android-create"></i></a>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>

		</div>
	</div>

	<script type="text/javascript">
		var row_vhcl = 0;
		$('#btnadd_vehicle').click(function() {
			var vhcl 	= $('#vhcl_type :selected').val();
			var qty 	= $('#qty').val();

			$.ajax({
                url : "ajaxreg_b2b_compl.php",
                type : 'POST',
                data: {
                    //item: item,
                    row: row_vhcl,
                    vhcl: vhcl,
                    qty: qty
                },   // data POST yang akan dikirim
                success : function(data) {
                    $("#rowadd_vhcl").append(data);
                    row_vhcl++;
                    console.log(row_vhcl);
                },
                error : function() {
                    alert('Telah terjadi error.');
                    return false;
                }
            });
		});
	</script>

<?php require('footer.php'); ?>
