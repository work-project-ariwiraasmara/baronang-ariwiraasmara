<?php require('header.php');?>

<?php require('sidebar-right.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<style>
    .tengah{
        text-align:center;
        }
    .kiri{
        text-align:left;
        }
    .kanan{
        text-align:right;
        }
</style>


<?php if($_GET['level'] == 2 ){ ?>

<div class="table-responsive" style="margin-top: 50px; margin-left: 20px; margin-right: 20px;">

                <a href="drep_labarugilev2.php?from=<?php echo $_GET['from']; ?>&to=<?php echo $_GET['to']; ?>&level=<?php echo $_GET['level']; ?>"><button type="button" class="btn btn-primary">Download to excel</button></a>
                <a class="btn btn-primary" onclick="printDiv()" href="#">Print</a>
                <a href="rep_pnl.php"><button type="button" class="btn btn-primary">Back</button></a>



                <div id="DivIdToPrint">
                <!-- Theme style -->
                <link rel="stylesheet" href="static/dist/css/AdminLTE.min.css">
                <!-- AdminLTE Skins. Choose a skin from the css/skins
                 folder instead of downloading all of them to reduce the load. -->
                <link rel="stylesheet" href="static/dist/css/skins/_all-skins.min.css">
                <link rel="stylesheet" href="custom_css.css">
                

                <div class="box-header with-border" align="center" style="margin-top: 50px; margin-left: 20px; margin-right: 20px;">
                    <tr>
                        <h3 class="box-title" align="center"><?php echo lang('Laporan Berdasarkan Level'); ?></h3>
                    </tr>
                </div>

                <table class="table table-bordered" style="margin-top: 50px; margin-left: 20px; margin-right: 20px;">
                <thead>
                <tr>
                    <th><?php echo 'Kode Akun'; ?></th>
                    <th><?php echo 'Nama Akun'; ?></th>
                    <th><?php echo 'Type ID'; ?></th>
                    <th><?php echo 'Nama Type'; ?></th>
                    <th><?php echo 'Level'; ?></th>
                    <th><?php echo 'Group ID'; ?></th>
                    <th><?php echo 'Nama Group'; ?></th>
                    <th><?php echo 'Nilai'; ?></th>
                    <th><?php echo 'Bulan'; ?></th>
                    <th><?php echo 'Tahun'; ?></th>
                </tr>
                </thead>
              
        
                    <?php
            $name = $_SESSION[Name];
            $from=$_GET['from'];
            $to=$_GET['to'];
            $xxx = "select * from dbo.LaporanRugiLabaViewNew where header = '$_GET[level]' and UserName = '$name' ORDER BY KodeAccount Asc";
            //echo $xxx;
            $y = sqlsrv_query($conn, $xxx);
            while($z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC)){
            ?>
                <tr>
                    <td><?php echo $z[0]; ?></td>
                    <td><?php echo $z[1]; ?></td>
                    <td><?php echo $z[2]; ?></td>
                    <td><?php echo $z[3]; ?></td>
                    <td><?php echo $z[4]; ?></td>
                    <td><?php echo $z[5]; ?></td>
                    <td><?php echo $z[6]; ?></td>
                    <td class="kanan"><?php echo $z[8]; ?></td>
                    <td><?php echo $z[9]; ?></td>
                    <td><?php echo $z[10]; ?></td>
                                           
                </tr>   
            <?php } ?>
                </table>
                           
                </div>
            </div>
<?php } ?>

<?php if($_GET['level'] == 0 ){ ?>

<div class="table-responsive" style="margin-top: 50px; margin-left: 20px; margin-right: 20px;">

                <a href="drep_labarugilev0.php?from=<?php echo $_GET['from']; ?>&to=<?php echo $_GET['to']; ?>&level=<?php echo $_GET['level']; ?>"><button type="button" class="btn btn-primary">Download to excel</button></a>
                <a class="btn btn-primary" onclick="printDiv()" href="#">Print</a>
                <a href="rep_neraca.php"><button type="button" class="btn btn-primary">Back</button></a>



                <div id="DivIdToPrint">
                <!-- Theme style -->
                <link rel="stylesheet" href="static/dist/css/AdminLTE.min.css">
                <!-- AdminLTE Skins. Choose a skin from the css/skins
                 folder instead of downloading all of them to reduce the load. -->
                <link rel="stylesheet" href="static/dist/css/skins/_all-skins.min.css">
                <link rel="stylesheet" href="custom_css.css">
                

                <div class="box-header with-border" align="center" style="margin-top: 50px; margin-left: 20px; margin-right: 20px;">
                    <tr>
                        <h3 class="box-title" align="center"><?php echo lang('Laporan Berdasarkan Level'); ?></h3>
                    </tr>
                </div>

                <table class="table table-bordered" style="margin-top: 50px; margin-left: 20px; margin-right: 20px;">
                <thead>
                <tr>
                    <th><?php echo 'Kode Akun'; ?></th>
                    <th><?php echo 'Nama Akun'; ?></th>
                    <th><?php echo 'Type ID'; ?></th>
                    <th><?php echo 'Nama Type'; ?></th>
                    <th><?php echo 'Level'; ?></th>
                    <th><?php echo 'Group ID'; ?></th>
                    <th><?php echo 'Nama Group'; ?></th>
                    <th><?php echo 'Nilai'; ?></th>
                    <th><?php echo 'Bulan'; ?></th>
                    <th><?php echo 'Tahun'; ?></th>
                </tr>
                </thead>
              
        
                    <?php
            $name = $_SESSION[Name];
            $from=$_GET['from'];
            $to=$_GET['to'];
            $xxx = "select * from dbo.LaporanRugiLabaViewNew where header = '$_GET[level]' and UserName = '$name' ORDER BY KodeAccount Asc";
            //echo $xxx;
            $y = sqlsrv_query($conn, $xxx);
            while($z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC)){
            ?>
                <tr>
                    <td><?php echo $z[0]; ?></td>
                    <td><?php echo $z[1]; ?></td>
                    <td><?php echo $z[2]; ?></td>
                    <td><?php echo $z[3]; ?></td>
                    <td><?php echo $z[4]; ?></td>
                    <td><?php echo $z[5]; ?></td>
                    <td><?php echo $z[6]; ?></td>
                    <td class="kanan"><?php echo $z[8]; ?></td>
                    <td><?php echo $z[9]; ?></td>
                    <td><?php echo $z[10]; ?></td>
                                           
                </tr>   
            <?php } ?>
                </table>
                           
                </div>
            </div>
<?php } ?>

<?php if($_GET['level'] == 1 ){ ?>

<div class="table-responsive" style="margin-top: 50px; margin-left: 20px; margin-right: 20px;">

                <a href="drep_labarugilev1.php?from=<?php echo $_GET['from']; ?>&to=<?php echo $_GET['to']; ?>&level=<?php echo $_GET['level']; ?>"><button type="button" class="btn btn-primary">Download to excel</button></a>
                <a class="btn btn-primary" onclick="printDiv()" href="#">Print</a>
                <a href="rep_neraca.php"><button type="button" class="btn btn-primary">Back</button></a>



                <div id="DivIdToPrint">
                <!-- Theme style -->
                <link rel="stylesheet" href="static/dist/css/AdminLTE.min.css">
                <!-- AdminLTE Skins. Choose a skin from the css/skins
                 folder instead of downloading all of them to reduce the load. -->
                <link rel="stylesheet" href="static/dist/css/skins/_all-skins.min.css">
                <link rel="stylesheet" href="custom_css.css">
                

                <div class="box-header with-border" align="center" style="margin-top: 50px; margin-left: 20px; margin-right: 20px;">
                    <tr>
                        <h3 class="box-title" align="center"><?php echo lang('Laporan Berdasarkan Level'); ?></h3>
                    </tr>
                </div>

                <table class="table table-bordered" style="margin-top: 50px; margin-left: 20px; margin-right: 20px;">
                <thead>
                <tr>
                    <th><?php echo 'Kode Akun'; ?></th>
                    <th><?php echo 'Nama Akun'; ?></th>
                    <th><?php echo 'Type ID'; ?></th>
                    <th><?php echo 'Nama Type'; ?></th>
                    <th><?php echo 'Level'; ?></th>
                    <th><?php echo 'Group ID'; ?></th>
                    <th><?php echo 'Nama Group'; ?></th>
                    <th><?php echo 'Nilai'; ?></th>
                    <th><?php echo 'Bulan'; ?></th>
                    <th><?php echo 'Tahun'; ?></th>
                </tr>
                </thead>
              
        
                    <?php
            $name = $_SESSION[Name];
            $from=$_GET['from'];
            $to=$_GET['to'];
            $xxx = "select * from dbo.LaporanRugiLabaViewNew where header = '$_GET[level]' and UserName = '$name' ORDER BY KodeAccount Asc";
            //echo $xxx;
            $y = sqlsrv_query($conn, $xxx);
            while($z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC)){
            ?>
                <tr>
                    <td><?php echo $z[0]; ?></td>
                    <td><?php echo $z[1]; ?></td>
                    <td><?php echo $z[2]; ?></td>
                    <td><?php echo $z[3]; ?></td>
                    <td><?php echo $z[4]; ?></td>
                    <td><?php echo $z[5]; ?></td>
                    <td><?php echo $z[6]; ?></td>
                    <td class="kanan"><?php echo $z[8]; ?></td>
                    <td><?php echo $z[9]; ?></td>
                    <td><?php echo $z[10]; ?></td>
                                           
                </tr>   
            <?php } ?>
                </table>
                           
                </div>
            </div>
<?php } ?>




<script type="text/javascript">
    function printDiv() 
{

  var divToPrint=document.getElementById('DivIdToPrint');

  var newWin=window.open('','Print-Window');

  newWin.document.open();

  newWin.document.write('<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

  newWin.document.close();

  setTimeout(function(){newWin.close();},10);

}

</script>






<?php require('footer_new.php');?>
