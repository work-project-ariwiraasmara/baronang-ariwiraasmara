<?php 
require('header.php');
require('sidebar-right.php');
require('sidebar-left.php');
?>

	<div class="animated fadeinup delay-1">
    	<div class="page-content">
			
    		<h1 class="uppercase center">
    			Data
    			<?php
    			if(isset($_GET['loc'])){
    				$sloc = "SELECT Nama from [dbo].[LocationMerchant] where LocationID='".@$_GET['loc']."'";
    				$qloc = sqlsrv_query($conn, $sloc);
    				$dloc = sqlsrv_fetch_array($qloc);
    				echo '<br>'.$dloc[0].'<br>'.date('F Y', strtotime(@$_GET['thn'].'-'.@$_GET['bln'].'-1'));			
    			}
    			?>
    		</h1>
    		<br>

    		<div class="m-t-30">
    			<div class="row">
    				
    				<div class="col s4">
    					<select class="browser-default" id="lokasi">
    						<?php
    						$s1 = "SELECT LocationID, Nama from [dbo].[LocationMerchant]";
    						$q1 = sqlsrv_query($conn, $s1);
    						while($d1 = sqlsrv_fetch_array($q1, SQLSRV_FETCH_NUMERIC)) { ?>
    							<option value="<?php echo $d1[0]; ?>"><?php echo $d1[1]; ?></option>
    							<?php
    						}
    						?>
    					</select>
    				</div>

    				<div class="col s4">
    					<select class="browser-default" id="bulan">
    						<?php
    						for($x = 1; $x < 13; $x++) { ?>
    							<option value="<?php echo $x; ?>"><?php echo date('F', strtotime(date('Y').'-'.$x.'-1') ); ?></option>
    							<?php
    						}
    						?>
    					</select>
    				</div>

    				<div class="col s4">
    					<select class="browser-default" id="tahun">
    						<?php
    						for($x = date('Y'); $x > 2014; $x--) { ?>
    							<option value="<?php echo $x; ?>"><?php echo $x; ?></option>
    							<?php
    						}
    						?>
    					</select>
    				</div>
    			</div>

    			<button class="btn btn-large width-100 primary-color m-t-10" id="btn_ok">OK</button>
    		</div>

    		<div class="table-responsive m-t-30">
    			<table>
    				<thead>
    					<tr>
    						<th>No.</th>
    						<th>Nama</th>
    						<th>No. Telp</th>
    						<th>Email</th>
    						<th>Alamat</th>
    						<th>CardNo</th>
    						<th>Bulan</th>
    						<th>Tahun</th>
    						<th>Kendaraan</th>
    						<th>Lokasi</th>
    						<th>Status</th>
    					</tr>
    				</thead>

    				<tbody>
    					<?php
    					if(isset($_GET['loc'])) {
    						$no = 1;
    						$s4 =  "SELECT 	c.Name, c.Phone, c.Email, 
    										c.Addr, a.CardNo, b.Bulan, 
    										b.Year, d.Name, e.Nama, b.Status 
    								from KOPX700097.dbo.MemberLocation  as a
                            		inner join KOPX700097.dbo.MemberLocationMonth as b
                                		on a.CardNo = b.CardNo
                                	inner join KOPX700097.dbo.MemberList as c
                                   		on a.MemberID = c.MemberID
                                	inner join KOPX700097.dbo.VehicleType as d
                                    	on a.VehicleID = d.VehicleID
                                	inner join KOPX700097.dbo.LocationMerchant as e
                                    	on a.LocationID = e.LocationID
                                	where b.Bulan='".@$_GET['bln']."' and b.Year='".@$_GET['thn']."' and e.LocationID = '".@$_GET['loc']."' and b.Status='1'
                                	order by b.Bulan ASC, b.Year ASC, c.Name ASC, a.CardNo ASC";
                            $q4 = sqlsrv_query($conn, $s4);
                            while($d4 = sqlsrv_fetch_array($q4, SQLSRV_FETCH_NUMERIC)) { ?>
                            	<tr>
                            		<td><?php echo $no; ?></td>
                            		<td><?php echo $d4[0]; ?></td>
                            		<td><?php echo $d4[1]; ?></td>
                            		<td><?php echo $d4[2]; ?></td>
                            		<td><?php echo $d4[3]; ?></td>
                            		<td><?php echo $d4[4]; ?></td>
                            		<td><?php echo $d4[5]; ?></td>
                            		<td><?php echo $d4[6]; ?></td>
                            		<td><?php echo $d4[7]; ?></td>
                            		<td><?php echo $d4[8]; ?></td>
                            		<td><?php echo $d4[9]; ?></td>
                            	</tr>
                            	<?php
                            	$no++;
                            }
    					}	
    					?>
    				</tbody>
    			</table>
    		</div>

		</div>
	</div>

	<script type="text/javascript">
		$('#btn_ok').click(function(){
			var loc = $('#lokasi').val();
			var bln = $('#bulan').val();
			var thn = $('#tahun').val();

			window.location.href = "?loc="+loc+"&bln="+bln+"&thn="+thn;
		});
	</script>

<?php require('footer.php'); ?>