<?php
require('header.php');
require('sidebar-left.php');
require('sidebar-right.php');
?>

    <div class="animated fadeinup delay-1">
        <div class="page-content">

            <form action="" method="post">
                <div class="row">
                    <div class="col s6">
                        <div class="input-field">
                            <?php
                            $BUL = array("JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC");
                            $BULlen = count($BUL);
                            $BULAN = array("Januari", "Februari", "Maret", "April", "May", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
                            $BULANlen = count($BULAN);
                            ?>
                            <span>Pilih Bulan</span> :<br>
                            <select class="browser-default" name="bulan" id="bulan">
                                <?php
                                $ibul = 1;
                                for($x = 0; $x < $BULlen; $x++) { ?>
                                    <option value="<?php echo date('t M', strtotime($BUL[$x])); ?>"><?php echo $BULAN[$x]; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="col s6">
                        <div class="input-field">
                            <span>Pilih Tahun</span> :<br>
                            <select class="browser-default" name="tahun" id="tahun">
                                <?php
                                for($x = 0; $x < 5; $x++) { ?>
                                    <option value="<?php echo date('Y')-$x; ?>"><?php echo date('Y')-$x; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>

                <button type="submit" name="ok" id="ok" class="btn btn-large primary-color width-100 waves-effect waves-light">OK</button>
            </form>
            
            <?php
            $ok = @$_POST['ok'];
            if(isset($ok)) {
                $bulan = @$_POST['bulan'];
                $tahun = @$_POST['tahun'];

                $bulan_int = date('m', strtotime($bulan));
                $tahun_int = date('Y', strtotime($tahun));
                //echo $bulan.' '.$tahun.' / '.$bulan_int.' '.$tahun_int;

                ?>
                <script type="text/javascript">
                    window.location.href = "chart_totalpark.php?chart=bulan&bulan=<?php echo $bulan; ?>&tahun=<?php echo $tahun_int; ?>";
                </script>
                <?php
            }
            ?>
            

            <?php
            $chart = @$_GET['chart'];
            if(@$_GET['chart']) {
                $json_label = array();
                $json_data1 = array();
                $json_data2 = array();
                $json_data3 = array();
                
                $bulan = @$_GET['bulan'];
                $tahun = @$_GET['tahun'];

                $bulan_int = date('t', strtotime($bulan));
                $tahun_int = date('Y', strtotime($tahun));

                //echo $bulan.' '.$tahun.' / '.$bulan_int.' '.$tahun_int;
                ?>
                
                <?php
				array_push($json_label, null);
				array_push($json_data1, 'Motor');
				array_push($json_data2, 'Mobil');
				array_push($json_data3, 'Total');
				
                for($l = 1; $l <= $bulan_int; $l++) {
                    $ln1 = 0;
                    $ln2 = 0;
                    $ln3 = 0;
                   
                    $dtf = $tahun_int."-".date('m', strtotime($bulan))."-".$l;
                    $dtt = $tahun_int."-".date('m', strtotime($bulan))."-".$bulan_int;
					$link = "<a href='www.google.com/search?q=$l'>$l</a>";
                    array_push($json_label, $link);

                    $sql_line1 = "SELECT count(*) from dbo.ParkingList where LocationIn = '700097LOC0000001' and VehicleType = 'C' and TimeIn between '$dtf 00:00:00.000' and '$dtf 23:23:59.999'";
                    $query1 = sqlsrv_query($conn, $sql_line1);
                    while($data1 = sqlsrv_fetch_array($query1)) {
                        array_push($json_data1, $data1[0]);
                    }

                    $sql_line2 = "SELECT count(*) from dbo.ParkingList where LocationIn = '700097LOC0000001' and VehicleType = 'B' and TimeIn between '$dtf 00:00:00.000' and '$dtf 23:23:59.999'";
                    $query2 = sqlsrv_query($conn, $sql_line2);
                    while($data2 = sqlsrv_fetch_array($query2)) {
                        array_push($json_data2, $data2[0]);
                    }

                    $sql_line3 = "SELECT count(*) from dbo.ParkingList where LocationIn = '700097LOC0000001' and TimeIn between '$dtf 00:00:00.000' and '$dtf 23:23:59.999'";
                    $query3 = sqlsrv_query($conn, $sql_line3);
                    while($data3 = sqlsrv_fetch_array($query3)) {
                        array_push($json_data3, $data3[0]);
                    }
                }

                /*
                ?>
                <div class="form-inputs">
                    <div class="row">
                        <div class="col s6">
                            <span>Pilih Tanggal</span> : <br>
                            <select class="browser-default" name="char_tgl" id="char_tgl">
                                <?php
                                for($t = 1; $t <= $bulan_int; $t) { ?>
                                    <option value="<?php echo $t; ?>"><?php echo $t; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>

                        <div class="col s6">
                            <button class="btn btn-large primary-color width-100 waves-effect waves-light">OK</button>
                            <a href="chart_penerimaan?chart=hari&tgl=&bulan=<?php echo $bulan; ?>&tahun=<?php echo $tahun_int; ?>">Pilih</a>
                        </div>
                    </div>
                </div>
                <?php
                */
            }
            ?>

        </div>
    </div>

   
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/data.js"></script>
	<script src="https://code.highcharts.com/modules/exporting.js"></script>
    <div id="container" style="height: 700px"></div>
	<script type="text/javascript">
	Highcharts.chart('container', {

    chart: {
        type: 'column'
    },
    
    useHTML: true,
    yAxis: {
		allowDecimals: false,
		},
		
    title: {
        text: 'Total Kendaraan Parkir'
    },

    plotOptions: {
        series: {
            cursor: 'pointer',
            point: {
                events: {
                    click: function () {
                        location.href = 'https://en.wikipedia.org/wiki/' +
                            this.options.key;
                    }
                }
            }
        }
    },

    
    data: {
        columns: [
            <?php echo json_encode($json_label); ?>,
            <?php echo json_encode($json_data1); ?>, 
            <?php echo json_encode($json_data2); ?>,
            <?php echo json_encode($json_data3); ?>
        ]
    }
    
});
    </script>

<?php
require('footer.php');
?>
