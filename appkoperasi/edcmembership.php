<?php
@session_start();
error_reporting(0);

if(isset($_GET['download'])) {
	$today = @$_GET['tgl'];
	$filePath = "uploads/excel/";

	require('connect.php');
	require('lib/phpexcel/Classes/PHPExcel.php');

	$objPHPExcel = new PHPExcel();
	$objPHPExcel->getProperties()->setCreator("Baronang");
	$objPHPExcel->getProperties()->setTitle("Membership");

	// set autowidth
	for($col = 'A'; $col !== 'Z'; $col++) {
	    $objPHPExcel->getActiveSheet()
	        ->getColumnDimension($col)
	        ->setAutoSize(true);
	}

	//sheet 2
	$objWorkSheet = $objPHPExcel->createSheet(0);

	// baris judul
	$objWorkSheet->getStyle('A1')->getFont()->setSize(15);
	$objWorkSheet->getStyle('A1')->getFont()->setBold(true);
	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:C1');
	$objWorkSheet->SetCellValue('A1', 'Membership Transaction '.date('d F Y', strtotime($today)));
	$objWorkSheet->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	

	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(50);
	$objWorkSheet->getStyle('A3')->getFont()->setBold(true);
	$objWorkSheet->getStyle('A3')->getFont()->setSize(15);
	$objWorkSheet->getStyle('A3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	$objWorkSheet->SetCellValue('A3', 'Cashier');

	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
	$objWorkSheet->getStyle('B3')->getFont()->setBold(true);
	$objWorkSheet->getStyle('B3')->getFont()->setSize(15);
	$objWorkSheet->getStyle('B3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	$objWorkSheet->SetCellValue('B3', 'Payment Method');

	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(50);
	$objWorkSheet->getStyle('C3')->getFont()->setBold(true);
	$objWorkSheet->getStyle('C3')->getFont()->setSize(15);
	$objWorkSheet->getStyle('C3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	$objWorkSheet->SetCellValue('C3', 'Total Receivable');

	
	$rowno = 4;
	$trans = 0;
	$total = 0;
	//echo $today;
	$sql = "SELECT SerialNumber, UserIDBaronang from [Gateway].[dbo].[EDCList] where KID='700097'";
	$query = sqlsrv_query($conn, $sql);
	while($data = sqlsrv_fetch_array($query)) { 
		$s1 = "SELECT KodeUser, NamaUser from [PaymentGateway].[dbo].[UserPaymentGateway] where KodeUser='".$data[1]."'";
		$q1 = sqlsrv_query($conn, $s1);
		$d1 = sqlsrv_fetch_array($q1);

		$s2 = 	"SELECT (SELECT SUM(Amount) from [dbo].[TransList] where TransactionType = 'Topp' and UserID='".$data[0]."' and Date between '$today 00:00:00.000' and '$today 23:59:59.999')
			 	from [dbo].[TransList] where TransactionType = 'Topp' and UserID='".$data[0]."' and Date between '$today 00:00:00.000' and '$today 23:59:59.999'";
		$q2 = sqlsrv_query($conn, $s2);
		$d2 = sqlsrv_fetch_array($q2);

		$total = $total + $d2[0];

		$objWorkSheet->SetCellValue('A'.$rowno, $d1[1]);
		$objWorkSheet->SetCellValue('B'.$rowno, 'CASH');

		$objWorkSheet->getStyle('C'.$rowno)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		$objWorkSheet->SetCellValue('C'.$rowno, number_format($d2[0], 2,".",","));

		$rowno++; $trans++;
	}


	//
	$rowno = $rowno + 1;
	$objWorkSheet->SetCellValue('A'.$rowno, 'Transactions : ');
	$objWorkSheet->getStyle('A'.$rowno)->getFont()->setBold(true);
	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$rowno.':B'.$rowno);
	$objWorkSheet->getStyle('A'.$rowno)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

	$objWorkSheet->SetCellValue('C'.$rowno,  number_format($trans, 2,".",","));
	$objWorkSheet->getStyle('C'.$rowno)->getFont()->setBold(true);
	$objWorkSheet->getStyle('C'.$rowno)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

	//
	$rowno = $rowno + 1;
	$objWorkSheet->SetCellValue('A'.$rowno, 'Total Collected : Rp. ');
	$objWorkSheet->getStyle('A'.$rowno)->getFont()->setBold(true);
	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$rowno.':B'.$rowno);
	$objWorkSheet->getStyle('A'.$rowno)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

	$objWorkSheet->SetCellValue('C'.$rowno, number_format($total, 2,".",","));
	$objWorkSheet->getStyle('C'.$rowno)->getFont()->setBold(true);
	$objWorkSheet->getStyle('C'.$rowno)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

	//
	$rowno = $rowno + 1;
	$objWorkSheet->SetCellValue('A'.$rowno, 'Net Sales : Rp. ');
	$objWorkSheet->getStyle('A'.$rowno)->getFont()->setBold(true);
	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$rowno.':B'.$rowno);
	$objWorkSheet->getStyle('A'.$rowno)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

	$objWorkSheet->SetCellValue('C'.$rowno, number_format($total, 2,".",","));
	$objWorkSheet->getStyle('C'.$rowno)->getFont()->setBold(true);
	$objWorkSheet->getStyle('C'.$rowno)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

	//exit;
	$objWorkSheet->setTitle('Membership '.date('d F Y', strtotime($today)) );

	$fileName = 'Membership '.date('d F Y', strtotime($today)).'.xls';
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');


	// download ke client
	header('Content-type: application/vnd.ms-excel');
	header('Content-Disposition: attachment; filename="'.$fileName.'"');
	$objWriter->save('php://output');

	return $filePath.'/'.$fileName;
}
else {
	require('header.php');
	require('sidebar-right.php');
	require('sidebar-left.php');

	if( !isset($_GET['tgl']) ) {
		$today = date('Y-m-d');
	}
	else {
		$today = @$_GET['tgl'];
	}

	$yesterday = date('Y-m-d', strtotime('-1 day', strtotime($today) ) );
	$tomorrow = date('Y-m-d', strtotime('+1 day', strtotime($today) ) );
	?>

		<div class="animated fadeinup delay-1">
	        <div class="page-content txt-black">

	        	<?php
	        	if( isset($_GET['det']) ) { 
	        		$sdet = "SELECT SerialNumber, UserIDBaronang from [Gateway].[dbo].[EDCList] where KID='700097' and UserIDBaronang='".@$_GET['det']."'";
			        $qdet = sqlsrv_query($conn, $sdet);
			        $ddet = sqlsrv_fetch_array($qdet);

			        $s1 = "SELECT KodeUser, NamaUser from [PaymentGateway].[dbo].[UserPaymentGateway] where KodeUser='".$ddet[1]."'";
			        $q1 = sqlsrv_query($conn, $s1);
			        $d1 = sqlsrv_fetch_array($q1);
	        		?>

	        		<div class="">
		        		<?php echo '<b>'.date('d F Y', strtotime($today) ).'</b> : '.$d1[1]; ?>
		        	</div>

		        	<div class="table-responsive m-t-30">
		        		<table>

		        			<thead>
		        				<tr>
		        					<th>No. Transaksi</th>
		        					<th>Jam</th>
		        					<th>No. Kartu</th>
		        					<th style="text-align: center;">Amount</th>
		        				</tr>
		        			</thead>

		        			<tbody>
		        				<?php
		        				$total = 0;
		        				$s2 = "SELECT TransNo, Date, AccountKredit, Amount from [dbo].[TransList] where TransactionType = 'Topp' and UserID='".$ddet[0]."' and Date between '$today 00:00:00.000' and '$today 23:59:59.999' order by Date ASC";
		        				$q2 = sqlsrv_query($conn, $s2);
		        				while($d2 = sqlsrv_fetch_array($q2)) { 
		        					$total = $total + $d2[3];
		        					?>
		        					<tr>
		        						<td><?php echo $d2[0]; ?></td>
		        						<td>
		        							<?php
		        							if( $d2[1] == '' || is_null($d2[1]) || empty($d2[1]) ) {
		        								echo '00:00:00';
		        							}
		        							else {
		        								echo $d2[1]->format('h:m:s'); 
		        							}
		        							?>
		        						</td>
		        						<td><?php echo $d2[2]; ?></td>
		        						<td style="text-align: right;"><?php echo number_format($d2[3], 2,".",","); ?></td>
		        					</tr>
		        					<?php
		        				}
		        				?>
		        			</tbody>

		        			<tfoot>
		        				<tr>
		        					<th style="text-align: right;" colspan="3" >Total Rp. </th>
		        					<th style="text-align: right;"><?php echo number_format($total, 2,".",","); ?></th>
		        				</tr>
		        			</tfoot>

		        		</table>
		        	</div>

	        		<?php
	        	}
	        	else { ?>

	        		<div class="">
		        		<b><?php echo date('d F Y', strtotime($today) ); ?></b>
		        		<div class="right hide">Receipt Number : xxx</div>
		        	</div>

		        	<div class="table-responsive m-t-30">
		        		<table>
		        			<thead>
		        				<tr class="m-t-30">
		        					<td colspan="0"><h3 class="uppercase" style="color: #000;"> <span id="trans"></span> <br> Transactions</h3></td>
		        					<td colspan="0"><h3 class="uppercase" style="color: #000;"> <span id="tc"></span> <br> Total Collected</h3></td>
		        					<td colspan="0"><h3 class="uppercase" style="color: #000;"> <span id="ns"></span> <br> Net Sales</h3></td>
		        				</tr>
		        				<tr>
		        					<th style="text-align: center;">Cashier</th>
		        					<th style="text-align: center;">Payment Method</th>
		        					<th style="text-align: center;">Total Receivable</th>
		        				</tr>
		        			</thead>

		        			<tbody>
		        				<?php
		        				$rowstrans = 0;
		        				$total = 0;
		        				//echo $today;
		        				$sql = "SELECT SerialNumber, UserIDBaronang from [Gateway].[dbo].[EDCList] where KID='700097'";
		        				$query = sqlsrv_query($conn, $sql);
			        			while($data = sqlsrv_fetch_array($query)) { 
			        				$s1 = "SELECT KodeUser, NamaUser from [PaymentGateway].[dbo].[UserPaymentGateway] where KodeUser='".$data[1]."'";
			        				$q1 = sqlsrv_query($conn, $s1);
			        				$d1 = sqlsrv_fetch_array($q1);

			        				$s2 = 	"SELECT (SELECT SUM(Amount) from [dbo].[TransList] where TransactionType = 'Topp' and UserID='".$data[0]."' and Date between '$today 00:00:00.000' and '$today 23:59:59.999')
			        						 from [dbo].[TransList] where TransactionType = 'Topp' and UserID='".$data[0]."' and Date between '$today 00:00:00.000' and '$today 23:59:59.999'";
			        				$q2 = sqlsrv_query($conn, $s2);
			        				$d2 = sqlsrv_fetch_array($q2);

			        				$total = $total + $d2[0];
			        				?>
			        				<tr>
			        					<td><?php echo $d1[1]; ?></td>
			        					<td>CASH</td>
			        					<td style="text-align: right;"><a href="?tgl=<?php echo $today; ?>&det=<?php echo $data[1]; ?>"> <?php echo number_format($d2[0], 2,".",","); ?> </a></td>
			        				</tr>
			        				<?php
			        				$rowstrans++;
			        			}
		        				?>
		        				
		        			</tbody>

		        			<tfoot>
		        				<tr>
		        					<td colspan="2" style="text-align: left; margin-top: 50px;">
		        						<a href="?tgl=<?php echo $yesterday; ?>" class="btn btn-large waves-effect waves-light primary-color">
		        							<i class="ion-ios-arrow-back"></i>
		        						</a>
		        					</td>

		        					<td colspan="2">
		        						<a href="<?php echo '?download=1&tgl='.$today; ?>" class="btn btn-large waves-effect waves-light primary-color" target="_blank">Download</a>
		        					</td>

		        					<td colspan="2" style="text-align: right; margin-top: 50px;">
		        						<?php
		        						if( isset($_GET['tgl']) ) { ?>
		        							<a href="?tgl=<?php echo $tomorrow; ?>" class="btn btn-large waves-effect waves-light primary-color">
			        							<i class="ion-ios-arrow-forward"></i>
			        						</a>
		        							<?php
		        						}
		        						?>
		        					</td>
		        				</tr>
		        			</tfoot>
		        		</table>
		        	</div>

		        	<script type="text/javascript">
		        		$('#trans').html('<?php echo $rowstrans; ?>');
		        		$('#tc').html('<?php echo 'Rp. '.number_format($total, 2,".",","); ?>');
		        		$('#ns').html('<?php echo 'Rp. '.number_format($total, 2,".",","); ?>');
		        	</script>

	        		<?php
	        	}
	        	?>
				
			</div>
		</div>

	<?php 
	require('footer.php'); 
}
?>