<?php require('header.php');?>

<?php require('sidebar-right.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<div class="animated fadeinup delay-1">
    <div class="page-content">

        <?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
            <div class="notification notification-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
                <!--<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> <i class="icon fa fa-warning ?>"></i>-->
                <h4> <?php echo ucfirst($_SESSION['error-type']); ?> </h4>
                <?php echo $_SESSION['error-message']; ?>
            </div>
        <?php } ?>

        <h2 class="uppercase"><?php echo lang('Pengaturan umum'); ?></h2>

            <form action="proc_mgeneralsetting.php" class="form-horizontal" method="POST">

                <?php
                $mgssql    = "select * from [dbo].[GeneralSetting] where KID='$_SESSION[KID]'";
                $mgsstmt    = sqlsrv_query($conn, $mgssql);
                $mgsrow     = sqlsrv_fetch_array( $mgsstmt, SQLSRV_FETCH_NUMERIC);
                if(count($mgsrow[0]) > 0){
                    $mgs0  = $mgsrow[0];
                    $mgs1  = $mgsrow[1];
                    $mgs2  = $mgsrow[2];
                    $mgs3  = $mgsrow[3];
                    $mgs4  = $mgsrow[4];
                    $mgs5  = $mgsrow[5];
                    $mgs6  = $mgsrow[6];
                    $mgs7  = $mgsrow[7];
                    $mgs8  = $mgsrow[8];
                    $mgs9  = $mgsrow[9];
                    $mgs10 = $mgsrow[10];
                    $mgs11 = $mgsrow[11];
                    $mgs12 = $mgsrow[12];
                    $mgs13 = $mgsrow[13];
                    $mgs14 = $mgsrow[14];
                    $mgs15 = substr($mgsrow[15], 0 ,-5);
                    $mgs16 = $mgsrow[16];
                    $mgs17 = $mgsrow[17];
                }
                ?>

                <!-- /.box-header -->
                <br><label><?php echo lang('Prioritas pembayaran'); ?></label>
                <select name="template" class="browser-default">
                    <option value="">- <?php echo lang('Pilih salah satu'); ?> -</option>
                    <?php
                    $mpbssksql = "select * from [dbo].[TemplateGeneralSeting]";  //simpanan sukarela
                    $mpbsstmt = sqlsrv_query($conn, $mpbssksql);
                    while ($mpbsrow = sqlsrv_fetch_array( $mpbsstmt, SQLSRV_FETCH_NUMERIC)) {
                        if($mpbsrow[1] == $mgs1 and $mpbsrow[2] == $mgs2 and $mpbsrow[3] == $mgs3){
                            ?>
                            <option value="<?=$mpbsrow[0];?>" selected><?=$mpbsrow[0];?></option>
                        <?php } else { ?>
                            <option value="<?=$mpbsrow[0];?>"><?=$mpbsrow[0];?></option>
                        <?php } ?>
                        <?php
                    }
                    ?>
                </select>

                <br><?php echo lang('Tanggal cut off bulanan') ?><br>

                <div class="input-field">
                    <input type="number" name="BS" id="BasicSaving" class="validate" value="<?=$mgs4?>">
                    <label for="BasicSaving"><?php echo lang('Simpanan pokok'); ?></label>
                </div>

                <div class="input-field">
                    <input type="number" name="RSI" id="RegulerSavingsInterest" class="validate" value="<?=$mgs5?>">
                    <label for="RegulerSavingsInterest"><?php echo lang('Bunga tabungan'); ?></label>
                </div>

                <div class="input-field">
                    <?php echo lang('Aktifkan akuntansi'); ?><br>

                    <input type="radio" name="isAkun" id="isAkunYa" value="1" <?php if($mgs16 == 1){ echo "checked";} ?>>
                    <label for="isAkunYa">Ya</label>

                    <input type="radio" name="isAkun" id="isAkunNo" value="0" <?php if($mgs16 ==  0){ echo "checked";} ?>>
                    <label for="isAkunNo">Tidak</label>
                </div>

                <div class="input-field">
                    <?php echo lang('Limit Gabungan (belanja+pinjaman)'); ?><br>

                    <input type="radio" name="isLimit" id="isLimitYa" value="2" <?php if($mgs17 == 2){ echo "checked";} ?>>
                    <label for="isLimitYa">Ya</label>

                    <input type="radio" name="isLimit" id="isLimitNo" value="1" <?php if($mgs17 ==  1){ echo "checked";} ?>>
                    <label for="isLimitNo">Tidak</label>
                </div><br>

                <div class="input-field">
                    <input type="text" name="DCT" id="cooperativeID" class="validate" placeholder="00:00:00" value="00:00:00" readonly>
                    <label for="cooperativeID"><?php echo lang('Jam cut off harian'); ?></label>
                </div>

                <br><?php echo lang('Hari kredit'); ?>(<?php echo lang('Tahunan'); ?>)
                <div class="input-field">
                    <input type="number" name="CRD" id="Days" class="validate" value="<?=$mgs7?>">
                    <label for="Days"><?php echo lang('Hari'); ?></label>
                </div>

                <label for="BasicSaving" class="control-label"></label>
                <div class="input-field">
                    <input type="number" name="CRW" id="Weeks"class="validate" value="<?=$mgs8?>">
                    <label for="Weeks"><?php echo lang('Minggu'); ?></label>
                </div>

                <div class="input-field">
                    <input type="number" name="CRM" id="Months" class="validate" value="12" readonly>
                    <label for="Months"><?php echo lang('Bulan'); ?></label>
                </div>

                <div class="input-field">
                    <?php echo lang('Aktual hari'); ?><br>
                    <input type="radio" name="isRealTime" class="minimal" id="optionsRadios1_isRealTime" value="1" <?php if($mgs10 == 1){ echo "checked";} ?>>
                    <label for="optionsRadios1_isRealTime"> <?php echo lang('Ya'); ?> </label>

                    <input type="radio" name="isRealTime" class="minimal" id="optionsRadios2_isRealTime" value="0" <?php if($mgs10 == ""){ echo "";}else{if($mgs10 == 0){ echo "checked";}} ?>>
                    <label for="optionsRadios2_isRealTime"> <?php echo lang('Tidak'); ?> </label>
                </div><br>

                <div class="input-field">
                    <input type="number" name="CDM" id="CreditDays" class="validate" value="<?=$mgs11?>">
                    <label for="CreditDays"><?php echo lang('Hari kredit'); ?>(<?php echo lang('Bulanan'); ?>)</label>
                </div>

                <div class="input-field">
                    <?php echo lang('Aktual hari'); ?><br>
                    <input type="radio" name="isRealDay" class="minimal" id="optionsRadios1_isRealDay" value="1" <?php if($mgs12 == 1){ echo "checked";} ?>>
                    <label for="optionsRadios1_isRealDay"><?php echo lang('Ya'); ?></label>

                    <input type="radio" name="isRealDay" class="minimal" id="optionsRadios2_isRealDay" value="0" <?php if($mgs12 == ""){ echo "";}else{if($mgs12 == 0){ echo "checked";}} ?>>
                    <label for="optionsRadios2_isRealDay"><?php echo lang('Tidak'); ?></label>
                </div>

                <div class="input-field">
                    <?php echo lang('Pajak bunga'); ?><br>
                    <input type="radio" name="isInterestTax" class="minimal" id="optionsRadios1_isInterestTax" value="1" <?php if($mgs13 == 1){ echo "checked";} ?>>
                    <label for="optionsRadios1_isInterestTax"><?php echo lang('Ya'); ?></label>

                    <input type="radio" name="isInterestTax" class="minimal" id="optionsRadios2_isInterestTax" value="0" <?php if($mgs13 ==  0){ echo "checked";} ?>>
                    <label for="optionsRadios2_isInterestTax"><?php echo lang('Tidak'); ?></label>
                </div><br>

                <div class="input-field">
                    <input type="number" name="TaxRate" id="CreditDays" class="validate" value="<?=$mgs14?>">
                    <label for="CreditDays"><?php echo lang('Persentase pajak'); ?>(%)</label>
                </div>

                <div class="input-field">
                    <input type="text" name="MinimumTax" class="form-control price" id="MinimumInterest" class="validate" value="<?=$mgs15?>">
                    <label for="MinimumInterest"><?php echo lang('Minimum jumlah bunga untuk dikenakan pajak'); ?></label>
                </div>

                <div style="margin-top: 30px;">
                    <?php if(count($mgsrow[0]) > 0){ ?>
                        <button type="submit" class="waves-effect waves-light btn-large primary-color width-100"><?php echo lang('Perbaharui'); ?></button>
                    <?php }else{ ?>
                        <button type="submit" class="waves-effect waves-light btn-large primary-color width-100"><?php echo lang('Simpan'); ?></button>
                    <?php } ?>
                </div>

            </form>

    </div>
</div>

<?php require('footer_new.php');?>
