<?php
session_start();
error_reporting(0);

include('lib/phpExcel/Classes/PHPExcel.php');
include "connect.php";


   $filePath = "uploads/excel/";

    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getProperties()->setCreator("baronang");
    $objPHPExcel->getProperties()->setTitle("Drep Sesi Kasir");

// set autowidth
    for($col = 'A'; $col !== 'Z'; $col++) {
        $objPHPExcel->getActiveSheet()
            ->getColumnDimension($col)
            ->setAutoSize(true);
    }

    $listCombo = array();

    //sheet 2
    $objWorkSheet = $objPHPExcel->createSheet(0);
// baris judul
   
    $objWorkSheet->getStyle('D1')->getFont()->setBold(true);
    $objWorkSheet->getStyle('D1')->getFont()->setSize(14);
    $objWorkSheet->SetCellValue('D1', 'Laporan Sesi Kasir');

    $objWorkSheet->getStyle('D2')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('D2', 'Periode :');                 

   
    


    $objWorkSheet->getStyle('A4')->getFont()->setBold(true);
    $objWorkSheet->getStyle('A4' )->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('A4', 'No');
    $objWorkSheet->getStyle('B4')->getFont()->setBold(true);
    $objWorkSheet->getStyle('B4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('B4', 'No Transaksi');
    $objWorkSheet->getStyle('C4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->getStyle('C4')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('C4', 'Nama User');
    $objWorkSheet->getStyle('D4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->getStyle('D4')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('D4', 'Begining Balance');
    $objWorkSheet->getStyle('E4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->getStyle('E4')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('E4', 'End Balance');   
    $objWorkSheet->getStyle('F4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->getStyle('F4')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('F4', 'Settlement Balance');
    $objWorkSheet->getStyle('G4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->getStyle('G4')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('G4', 'Time In');
    $objWorkSheet->getStyle('H4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->getStyle('H4')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('H4', 'Time Out');         
 


    if($_GET['from'] and $_GET['to']){
    
    $tgl1 = $_GET['from'];
    $tanggaldari = date('Y-m-d', strtotime($tgl1));
    $hanyatanggal = date('d', strtotime($tgl1));
    $tgl2 = $_GET['to'];
    //echo $tgl2;
    $tanggalsampai = date('Y-m-d', strtotime($tgl2));
    $hanyatanggal1 = date('d', strtotime($tgl2));
    $tglh = date('Y/m/d', strtotime('+1 days', strtotime($tgl2)));
    $kid = $_SESSION[KID];
    $no = 1;
    $bln = 2 ;
    $hanyatanggal = date('d', strtotime($_GET['from']));
    $bulan = date('m', strtotime($_GET['from']));
    $tahun = date('Y', strtotime($_GET['from']));
        if ($bulan != 1) {
            if ($bulan != 2 ) {
                if ($bulan != 3) {
                    if ($bulan != 4) {
                        if ($bulan !=5) {
                            if ($bulan !=6) {
                                if ($bulan !=7) {
                                    if ($bulan !=8) {
                                        if ($bulan !=9) {
                                            if ($bulan !=10) {
                                                if ($bulan !=11) {
                                                    $bulan = 'Desember';
                                                } else {
                                                $bulan = 'Nopember';
                                                }
                                            } else {
                                            $bulan = 'OKtober';   
                                            }
                                        } else {
                                        $bulan = 'September';
                                        }
                                    } else {
                                    $bulan = 'Agustus';
                                    }
                                } else {
                                $bulan = 'Juli';
                                }
                            } else {
                            $bulan = 'Juni';
                            }
                        } else {
                        $bulan = 'Mei';  
                        }
                    } else {
                    $bulan = 'April';       
                    }
                } else {
                $bulan = 'Maret';
                }
            } else {
            $bulan = 'Februari';    
            }                               
        } else {
        $bulan = 'Januari';
        }

    $tgl2 = date('d', strtotime($_GET['to']));
    $bulan2 = date('m', strtotime($_GET['to']));
    $tahun2 = date('Y', strtotime($_GET['to']));
        if ($bulan2 != 1) {
            if ($bulan2 != 2 ) {
                if ($bulan2 != 3) {
                    if ($bulan2 != 4) {
                        if ($bulan2 !=5) {
                            if ($bulan2 !=6) {
                                if ($bulan2 !=7) {
                                    if ($bulan2 !=8) {
                                        if ($bulan2 !=9) {
                                            if ($bulan2 !=10) {
                                                if ($bulan2 !=11) {
                                                    $bulan2 = 'Desember';
                                                } else {
                                                $bulan2 = 'Nopember';
                                                }
                                            } else {
                                            $bulan2 = 'OKtober';   
                                            }
                                        } else {
                                        $bulan2 = 'September';
                                        }
                                    } else {
                                    $bulan2 = 'Agustus';
                                    }
                                } else {
                                $bulan2 = 'Juli';
                                }
                            } else {
                            $bulan2 = 'Juni';
                            }
                        } else {
                        $bulan2 = 'Mei';  
                        }
                    } else {
                    $bulan2 = 'April';       
                    }
                } else {
                $bulan2 = 'Maret';
                }
            } else {
            $bulan2 = 'Februari';    
            }                               
        } else {
        $bulan2 = 'Januari';
        }

        $objWorkSheet->getStyle('E'.$bln)->getFont()->setBold(true);
        $objWorkSheet->SetCellValue("E".$bln,$hanyatanggal .' '.$bulan .' '. $tahun .' - '. $tgl2 .' '. $bulan2 .' '. $tahun2);   


    $k = 4;
    $x = "select * from dbo.LocationMerchant where status = 1 and LocationID = '$akun' order by LocationID asc";
    //echo $x;
    $y = sqlsrv_query($conn, $x);
    while($z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC)){
           
        $objWorkSheet->getStyle('B'.$k)->getFont()->setBold(true);
        $objWorkSheet->SetCellValue("B".$k,$z[0] .' - '. $z[2]);           

    }

    

    
    $row = 5;
    $rw = 5;
    
    $xy = "SELECT * FROM ( SELECT a.TransNo, b.NamaUser, a.BeginingBalance, a.EndBalance, a.SettlementBalance, a.TimeIn, a.TimeOut, ROW_NUMBER() OVER (ORDER BY Settlementtime asc) as row FROM [dbo].[CashierSession] a join [PaymentGateway].[dbo].[UserPaymentGateway] b on a.UserID = b.KodeUser where timein between '$tgl1' and '$tglh' or timeout between '$tgl1' and '$tglh' ) a";
    //echo $xy;
    $yz = sqlsrv_query($conn, $xy);
    
    while($za = sqlsrv_fetch_array( $yz, SQLSRV_FETCH_NUMERIC)){
         // if($za[11] == 1){
         //    $member='Member';
         //    } else {
         //    $member='Non Member';
         //    }
        

               
        $objWorkSheet->SetCellValueExplicit("A".$row,$za[7]);
        $objWorkSheet->SetCellValueExplicit("B".$row,$za[0]);
        $objWorkSheet->SetCellValueExplicit("C".$row,$za[1]);
        $objWorkSheet->SetCellValueExplicit("D".$row, number_format($za[2],2), PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet->SetCellValueExplicit("E".$row, number_format($za[3],2), PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet->SetCellValueExplicit("F".$row, number_format($za[4],2), PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet->SetCellValue("G".$row, $za[5]->format('Y-m-d H:i:s'));
        $objWorkSheet->SetCellValue("H".$row, $za[6]->format('Y-m-d H:i:s'));
        $row++;
        $rw++;
        }
 }
//exit;
    $objWorkSheet->setTitle('Drep Sesi Kasir');

    $fileName = 'LapSesi'.'_'.strtotime(date('Y-m-d H:i:s')).'.xls';
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');

// download ke client
    header('Content-type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename="'.$fileName.'"');
    $objWriter->save('php://output');

    return $filePath.'/'.$fileName;

?>
