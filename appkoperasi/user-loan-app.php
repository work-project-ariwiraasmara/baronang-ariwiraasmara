<?php require('header.php');?>      
<?php require('sidebar-left-user.php');?>

<section class="content">
<div class="row">
    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
		<div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Loan Application</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal">
              <div class="box-body">
			  
                <div class="form-group">
				<div class="col-sm-3"></div>
				  <label for="loantype" class="col-sm-2 control-label" style="text-align: left;">Loan Type</label>
				  <div class="col-sm-3">
                  <div class="btn-group">
                  <button type="button" class="btn btn-default"> Select</button>
                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                  </ul>
                </div>
                </div>
                </div>
				<div class="form-group">
				<div class="col-sm-3"></div>
				  <label for="loantype" class="col-sm-2 control-label" style="text-align: left;">Loan time period</label>
				  <div class="col-sm-3">
                  <div class="btn-group">
                  <button type="button" class="btn btn-default"> Select</button>
                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                  </ul>
                </div>
                </div>
                </div>
                <div class="form-group">
				  <div class="col-sm-3"></div>
				  <label for="amount" class="col-sm-2 control-label" style="text-align: left;">Amount</label>
				  <div class="col-sm-2">
				  <input type="text" class="form-control input-sm" id="amount" placeholder="">
				  </div>
                  </div>
                </div>	
				<div class="box-footer">
				<button type="submit" class="btn btn-info pull-right">Ajukan</button>
				<button type="submit" class="btn btn-standard">Lihat Simulasi</button>
				</div>
              </div>
              <!-- /.box-body -->
            </form>
			
          </div>
        </section>
        <!-- /.Left col -->
		<section class="col-lg-12 connectedSortable">
		<div class="box">
            <div class="box-header">
              <h3 class="box-title">Simulasi</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Loan Type</th>
                  <th>Loan Time Period</th>
                  <th> </th>
                  <th> </th>
                  <th> </th>
				  <th> </th>
                </tr>
                </thead>
                <tbody>
                <tr>
                  <td> </td>
                  <td> </td>
                  <td> </td>
                  <td> </td>
                  <td> </td>
				  <td> </td>	
                </tr>
                <tr>
                  <td> </td>
                  <td> </td>
                  <td> </td>
                  <td> </td>
                  <td> </td>
				  <td> </td>
                </tr>
                <tr>
                  <td> </td>
                  <td> </td>
                  <td> </td>
                  <td> </td>
                  <td> </td>
				  <td> </td>
                </tr>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
		</section>
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </section>
 
<?php require('content-footer.php');?>
<?php require('footer.php');?>