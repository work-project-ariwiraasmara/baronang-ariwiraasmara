<?php
require('header.php');
require('sidebar-left.php');
require('sidebar-right.php');
require('content-header.php');
?>

    <div class="animated fadeinup delay-1">
        <div class="page-content txt-black">
            <a href="dashboard_parkir2.php" target="_self" class="btn waves-effect waves-light primary-color">Dashboard</a>
            <?php
				$page = @$_GET['bulan'];
				
				$json_label = array();
                $json_data1 = array();
                
                if (empty($page)) {
					$month = date('m');
					$dmn = date('t M');
					$yn = date('Y');
					$bulan=$dmn;
					$tahun=$yn;
				
			}else{
				$bulan = @$_GET['bulan'];
                $tahun = @$_GET['tahun'];
				$month = $_GET['bln'];
				$yn = $tahun;
				$dmn = $bulan;
				
				
			}
                $bulan_int = date('t', strtotime($bulan));
                $tahun_int = date('Y', strtotime($tahun));
                
                $sql_rp = "SELECT ValueConversion from [dbo].[ParkingSetting]";
				$query_rp = sqlsrv_query($conn, $sql_rp);
				$data_rp = sqlsrv_fetch_array($query_rp);
				$conv = $data_rp[0];
			
                ?>
                 <div id="container" style="width: 100% ; height: 500px">
					<?php
						
						$sql_pointsales = "SELECT SUM(Amount) from [dbo].[TransList] where TransactionType='TOPP' and month([Date]) = '$month'";
						$query_pointsales = sqlsrv_query($conn, $sql_pointsales);
						$data_pointsales = sqlsrv_fetch_array($query_pointsales, SQLSRV_FETCH_NUMERIC);
							if ($data_pointsales[0] == null) {
								$pointsales = 0;}
							else {
								$pointrp = $data_pointsales[0] / $conv;
								$pointsales = number_format($pointrp,0,",",".");}
						
						$title = '<b>Penjualan Point<br>'.date('F', strtotime($bulan)).' '.date('Y', strtotime($tahun)).'<br>'.$pointsales.' Points<br><br></b>';
						
						$sql_edc = "Select SerialNumber, UserIDBaronang from Gateway.dbo.EDCList where KID = '$_SESSION[KID]' and Status='1' order by SerialNumber";
						$queryEDC = sqlsrv_query($conn, $sql_edc);
						
						while($dataEDC = sqlsrv_fetch_array($queryEDC)) {
							
							$sql_kasir = "Select NamaUser from [PaymentGateway].[dbo].[UserPaymentGateway] where KodeUser = '$dataEDC[1]'";
							$query_kasir = sqlsrv_query($conn, $sql_kasir);
							$data_kasir = sqlsrv_fetch_array($query_kasir);
							
							array_push($json_label, '<a href="chart_point_sales.php?chart=bulan&bulan='.$dmn.'&tahun='.$yn.'&edc='.$dataEDC[0].'" target="_self">'.$data_kasir[0].'</a>');
                            
                            $sql_line1 = "SELECT SUM(Amount) from [dbo].[TransList] where AccountDebet='$dataEDC[0]' and Month([Date]) = '$month' and YEAR([Date]) = '$yn'";
							$query1 = sqlsrv_query($conn, $sql_line1);
							while($data1 = sqlsrv_fetch_array($query1)) {
								if ($data1[0] == null) {
									array_push($json_data1, 0); }
								else {
									array_push($json_data1, $data1[0]); }
								}
						}
					?>
					
						 <div class="m-t-10" style="margin-bottom: 50px;">
                            <?php
                            //$dmn = date('t M'); 
                            //$yn = date('Y');
                            ?>

                            <?php 
                            if( !(($bulan == '31 Jan') && ($tahun == '2019')) ) { ?>
                                <a href="chart_sales_edc.php?bulan=<?php echo date('t M', strtotime($yn.'-'.$month.'-01'.'-1 month')); ?>&tahun=<?php echo $yn; ?>&bln=<?php echo date('m', strtotime($yn.'-'.$month.'-01'.'-1 month')); ?>" target="_self" class="btn btn-large left primary-color waves-effect waves-light" style="color: #ffffff;">Prev</a>
                            <?php 
                            }
                            ?>

                            <?php
                            if( !(($dmn == date('t M')) && ($yn == date('Y')) )) { ?>
                                <a href="chart_sales_edc.php?bulan=<?php echo date('t M', strtotime($yn.'-'.$month.'-01'.'+1 month')); ?>&tahun=<?php echo $yn; ?>&bln=<?php echo date('m', strtotime($yn.'-'.$month.'-01'.'+1 month')); ?>" target="_self" class="btn btn-large right primary-color waves-effect waves-light txt-white" style="color: #ffffff;">Next</a>
                                <?php
                            }
					?>
					</div>	
			</div>				
		</div>
    </div>

    <script src="js/plotly-latest.min.js"></script>
    <script type="text/javascript">
        $('#btn_cj').click(function() {
            var jam = $('#chart_jam :selected').val();
            var url = "chart_sales_edc.php?chart=hari&tgl=<?php echo @$_GET['tgl']; ?>&bulan=<?php echo $bulan; ?>&tahun=<?php echo $tahun; ?>" + "&jam=" + jam + "&loc=<?php echo $loc; ?> target='_self'";

            window.location.href = url;
        });

        
        var data = [
        {
			x: <?php echo json_encode($json_label); ?>, 
            y: <?php echo json_encode($json_data1); ?>,
            type: 'bar'
		}
		];

        var layout = {
            autosize: false,
            width: 900,
            height: 500,
            title: '<?php echo $title; ?>',
            xaxis: { type: 'category', fixedrange: true },
            yaxis: { fixedrange: true },
            barmode: 'group'
        };

        Plotly.newPlot('container', data, layout, {displaylogo: false});
    </script>
    
<?php
require('footer.php');
?>


