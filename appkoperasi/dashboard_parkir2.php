<?php require('header.php');?>

<?php require('sidebar-right.php');?>

<?php require('sidebar-left.php');?>

    <!-- Main Content -->

    <link rel="stylesheet" href="static/dist/css/AdminLTE.min.css">


    <div class="animated fadeinup delay-1">
        <div class="page-content">
            <!-- Mulai coding disini 
            <h2 class="uppercase">Dashboard</h2>-->

            <?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
                <div class="alert alert-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-warning ?>"></i> <?php echo ucfirst($_SESSION['error-type']); ?></h4>
                    <?php echo $_SESSION['error-message']; ?>
                </div>
            <?php } ?>

            <?php if(isset($_SESSION['sesiteller'])){ ?>
            <div class="c-widget">
                <div class="c-widget-figure primary-color">
                    <i class="ion-android-mail"></i>
                </div>
                <div class="c-widget-body">
                    <p class="m-0">Sesi Teller Aktif</p>
                    <p class="small m-0"><?php echo $_SESSION['sesiteller']; ?></p>
                </div>
            </div>
          <?php } ?>

          <?php if(isset($_SESSION['sesikasir'])){ ?>
            <div class="c-widget">
                <div class="c-widget-figure primary-color">
                    <i class="ion-android-mail"></i>
                </div>
                <div class="c-widget-body">
                    <p class="m-0">Sesi Kasir Aktif</p>
                    <p class="small m-0"><?php echo $_SESSION['sesikasir']; ?></p>
                </div>
            </div>
          <?php } ?>

            <!-- Akhir baris coding -->
        </div>

    <section class="content">
      <!-- Small boxes (Stat box) -->
       
      <div class="row">
        <div class="col-lg-3 col-xs-1">
          <!-- small box -->
          <div class="small-box bg-navy">
            <div class="inner">
              <h3 id="hari"></h3>
              <h3 id="tanggal"></h3>
             </div>
         </div>
        </div>
        <!-- ./col -->
           </div>
        <!-- . row -->
          
      <div class="row">
        <div class="col-md-4">
         	<div class="box box-success box-solid">
            <div class="box-header with-border bg-aqua-active">
              <h3 style="text-align:center" class="widget-user-username">POINTS</h3>
              
            </div>
             <div class="box-body no-padding">
              <table class="table table-striped">
                <tr>
					<th style="text-align:left" class="description-text">Previous Balance</th>
					 <td style="text-align:right" class="description-text" id="point_bal"></td>
					 <td style="text-align:right" class="description-text" id="rp_bal"></td>
				</tr>
				<tr>
					<th style="text-align:left" class="description-text"><a href="chart_sales_edc.php" target="_self">Points Sales</a></th>
					<td style="text-align:right" class="description-text" id="point2"></td>
					<td style="text-align:right" class="description-text" id="rp2"></td>
					</tr>
					<tr>
					<th style="text-align:left" class="description-text">Membership Sales</th>
					 <td style="text-align:right; color:red;" class="description-text" id="current_mem_sales"></td>
					 <td style="text-align:right; color:red;" class="description-text" id="rp_current_mem_sales"></td>
					</tr>
					<tr>
					<th style="text-align:left" class="description-text">Parking</th>
					<td style="text-align:right; color:red;" class="description-text" id="current_park"></td>
					<td style="text-align:right; color:red;" class="description-text" id="rp_current_park"></td>
					</tr>
					<tr>
					<th style="text-align:left" class="description-text">Remaining Points</th>
					<td style="text-align:right" class="description-text" id="current_point"></td>
					 <td style="text-align:right" class="description-text" id="current_rp"></td>
					</tr>
			      
                </table>
                </div>
                
               
        </div>
        </div>
        
         </div>
          <!-- ./row -->
          
          <div class="row">
			  <?php
		 $sql_lokasi = "Select Count(*) from [dbo].[LocationMerchant] where Status='1'";
			$queryLoc = sqlsrv_query($conn, $sql_lokasi);
			$i = sqlsrv_fetch_array($queryLoc, SQLSRV_FETCH_NUMERIC);
			?>
		  <div class="col-md-9">
         	<div class="box box-success box-solid">
            <div class="box-header with-border bg-aqua-active">
              <h3 style="text-align:center" class="widget-user-username">Locations</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table class="table table-striped">
                <tr>
                  <th style="text-align:center">Location</th>
                  <th style="text-align:center">Membership Sales</th>
                  <th style="text-align:center">Mem. Sales Rp.</th>
                  <th style="text-align:center">Parking</th>
                  <th style="text-align:center">Parking Rp.</th>
                  <th style="text-align:center">Vehicles In</th>
                  <th style="text-align:center">Vehicles Out</th>
                </tr>
                
                <?php For ($l = 0; $l < $i[0]; $l++) { ?>
                <tr>
                  <td class="description-header" id="<?php echo 'namaloc'.$l ?>"></td>
                   
                  <td style="text-align:right" class="description-header" id="<?php echo 'membersales'.$l ?>"></td>
                  <td style="text-align:right" class="description-header" id="<?php echo 'membersales_rp'.$l ?>"></td>
                  <td style="text-align:right" class="description-header" id="<?php echo 'park'.$l ?>"></td>
                  <td style="text-align:right" class="description-header" id="<?php echo 'park_rp'.$l ?>"></td>
                  <td style="text-align:right" class="description-header" id="<?php echo 'in'.$l ?>"></td>
                  <td style="text-align:right" class="description-header" id="<?php echo 'out'.$l ?>"></td>
                </tr>
                <?php } ?>
                
                </table>
            </div>
            <!-- /.box-body -->
          </div>
          
         	<!-- /.box -->
                </div>
                <!-- /.col -->
                </div>
                <!-- /.row -->
          
     </section>
    </div> <!-- End of Main Contents -->

<script type="text/javascript">
    var interval2 = 1000; //3detik
	
    function doAjaxDashboard(){
        $.ajax({
            url : "ajax_loaddashboard3.php",
            type : 'POST',
            dataType : 'json',
            data: { },
            success : function(data) {
                if(data.status == 1){
					var countloc = <?php echo $i[0]; ?>;
					var x;
					$('#point_bal').html(data.point_bal);
					$('#rp_bal').html(data.rp_bal);
                    $('#point').html(data.point);
                    $('#point2').html(data.point2);
                    $('#hari').html(data.hari);
                    $('#tanggal').html(data.tanggal);
                    $('#rp').html(data.rp);
                    $('#rp2').html(data.rp2);
                    $('#current_point').html(data.current_point);
                    $('#current_rp').html(data.current_rp);
                    $('#current_park').html(data.current_park);
                    $('#rp_current_park').html(data.rp_current_park);
                    $('#current_mem_sales').html(data.current_mem_sales);
                    $('#rp_current_mem_sales').html(data.rp_current_mem_sales);
                    for (x = 0; x < countloc; x++) {
						var text = '#namaloc'+x;
					$(text).html(data.namaloc[x]);
                    $('#membersales'+x).html(data.membersales[x]);
                    $('#membersales_rp'+x).html(data.membersales_rp[x]);
                    $('#park'+x).html(data.park[x]);
                    $('#park_rp'+x).html(data.park_rp[x]);
                    $('#in'+x).html(data.in[x]);
                    $('#out'+x).html(data.out[x]);}
                   
                }
            },
            complete: function (data) {
                // Schedule the next
                setTimeout(doAjaxDashboard, interval2);
            }
        });
    }

    setTimeout(doAjaxDashboard, interval2);
    
    
    </script>

<?php require('footer.php');?>

