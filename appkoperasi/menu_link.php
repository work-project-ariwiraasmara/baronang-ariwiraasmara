<?php
	//mendapatkan parameter URL
	//$url_Aktif 	= stristr($_SERVER['REQUEST_URI'], 'muserlevel');

	/* untuk link menu master */
	if(stristr($_SERVER['REQUEST_URI'], "mgeneralsetting")){	//generalsetting
		$master			= "active";
		$linkgenset 	= "active";
	}
	else if(stristr($_SERVER['REQUEST_URI'], "muserlevel")){	//userlevel
		$master			= "active";
		$linkulev	 	= "active";
	}
	else if(stristr($_SERVER['REQUEST_URI'], "museraccount")){	//useraccount
		$master			= "active";
		$linkuacc	 	= "active";
	}
	else if(stristr($_SERVER['REQUEST_URI'], "mposition")){	//mposition
		$master			= "active";
		$linkpos	 	= "active";
	}
	else if(stristr($_SERVER['REQUEST_URI'], "bank_acc")){	//bankaccount
		$master			= "active";
		$linkbankacc 	= "active";
	}
	else if(stristr($_SERVER['REQUEST_URI'], "bank_list")){	//banklist
		$master			= "active";
		$linkbanklist 	= "active";
	}
	else if(stristr($_SERVER['REQUEST_URI'], "bas_sav_type")){	//basicsavingtype
		$product		= "active";
		$linkbasavty 	= "active";
	}
	else if(stristr($_SERVER['REQUEST_URI'], "reg_sav_type")){	//regularsavingtype
		$product		= "active";
		$linkregsavty 	= "active";
	}
	else if(stristr($_SERVER['REQUEST_URI'], "time_dep_type")){	//timedeposittype
		$product			= "active";
		$linkdeptipe	= "active";
	}
	/*elseif(stristr($_SERVER['REQUEST_URI'], "loan_set")){	//loansetting
		$master			= "active";
		$linkloanset 	= "active";
	}
	else if(stristr($_SERVER['REQUEST_URI'], "loan_set_app")){	//loan_set_app
		$master					= "active";
		$linkloansetapp 		= "active";
	}*/
	else if(stristr($_SERVER['REQUEST_URI'], "loan_type")){	//loantype
		$product			= "active";
		$linkloantype	= "active";
	}
	else if(stristr($_SERVER['REQUEST_URI'], "csoamemberlist")){	//csoamemberlist
		$cuser			= "active";
		$oa				= "active";
		$linkomemlist 	= "active";
	}
	else if(stristr($_SERVER['REQUEST_URI'], "csoaregsaving")){	//csoaregsaving
		$cuser			= "active";
		$oa				= "active";
		$linkoregsav	 = "active";
	}
	else if(stristr($_SERVER['REQUEST_URI'], "csoaloanapp")){	//csoaloanapp
		$cuser			= "active";
		$oa				= "active";
		$linkoloanapp 	= "active";
	}
	else if(stristr($_SERVER['REQUEST_URI'], "csoabasicsaving")){	//csoabasicsaving
		$cuser			= "active";
		$oa				= "active";
		$linkobasav	 	= "active";
	}
	else if(stristr($_SERVER['REQUEST_URI'], "csoatimedeposit")){	//csoatimedeposit
		$cuser			= "active";
		$oa				= "active";
		$linkotidep 	= "active";
	}
	else if(stristr($_SERVER['REQUEST_URI'], "cscaregsaving")){	//cscaregsaving
		$cuser			= "active";
		$ca				= "active";
		$lincregsav 	= "active";
	}
	else if(stristr($_SERVER['REQUEST_URI'], "cscatimedeposit")){	//cscatimedeposit
		$cuser			= "active";
		$ca				= "active";
		$linkctidep	 	= "active";
	}
	else if(stristr($_SERVER['REQUEST_URI'], "cscamember")){	//cscatimedeposit
		$cuser			= "active";
		$ca				= "active";
		$linkcscamb	 	= "active";
	}
	else if(stristr($_SERVER['REQUEST_URI'], "dep_bas_sav")){	//dep_bas_sav
		$cas			= "active";
		$dep			= "active";
		$linkdbasav	 	= "active";
	}
	else if(stristr($_SERVER['REQUEST_URI'], "dep_reg_sav")){	//dep_reg_sav
		$cas			= "active";
		$dep			= "active";
		$linkdregsav 	= "active";
	}
	else if(stristr($_SERVER['REQUEST_URI'], "dep_loan_pay")){	//dep_loan_pay
		$cas			= "active";
		$dep			= "active";
		$linkdloapay	= "active";
	}
	else if(stristr($_SERVER['REQUEST_URI'], "dep_time_dep")){	//dep_time_dep
		$cas			= "active";
		$dep			= "active";
		$linkdtidep	 	= "active";
	}
	else if(stristr($_SERVER['REQUEST_URI'], "with_reg_sav")){	//with_reg_sav
		$cas			= "active";
		$wd				= "active";
		$linkwdregsav	= "active";
	}
	else if(stristr($_SERVER['REQUEST_URI'], "with_basic_sav")){	//with_basic_sav
		$cas			= "active";
		$wd				= "active";
		$linkwdbasav 	= "active";
	}else if(stristr($_SERVER['REQUEST_URI'], "loan_rel")){	//with_reg_sav
		$ln				= "active";
		$linkloanrel	= "active";
	}
	else if(stristr($_SERVER['REQUEST_URI'], "loan_app")){	//with_basic_sav
		$ln				= "active";
		$linkloanapp 	= "active";
	}
	else{
		$master			= "";
		$linkgenset 	= "";
		$linkulev	 	= "";
		$linkuacc	 	= "";
		$linkpos	 	= "";
		$linkbankacc 	= "";
		$linkbanklist 	= "";
		$linkbasavty 	= "";
		$linkregsavty	= "";
		$linkdeptipe 	= "";
		$linkloanset	= "";
		$linkloansetapp = "";
		$linkloantype 	= "";

		$cuser			= "";
		$oa				= "";
		$linkomemlist 	= "";
		$linkoregsav 	= "";
		$linkoloanapp 	= "";
		$linkobasav	 	= "";
		$linkotidep	 	= "";
		$ca				= "";
		$lincregsav 	= "";
		$linkctidep	 	= "";
		$linkcscamb		= "";

		$cas 			= "";
		$dep 			= "";
		$linkdbasav		= "";
		$linkdregsav	= "";
		$linkdloapay	= "";
		$linkdtidep		= "";
		$wd				= "";
		$linkwdregsav	= "";
		$linkwdbasav	= "";

		$ln 			= "";
		$linkloanrel	= "";
		$linkloanapp	= "";
	}

	/* --------------------------- */
?>