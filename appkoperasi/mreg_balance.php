<?php require('header.php');?>

<?php require('sidebar-right.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<div class="animated fadeinup delay-1">
    <div class="page-content">

        <?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
            <div class="notification notification-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
                <!--<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> <i class="icon fa fa-warning ?>"></i> -->
                <h4> <?php echo ucfirst($_SESSION['error-type']); ?> </h4>
                <?php echo $_SESSION['error-message']; ?>
            </div>
        <?php } ?>

        <h2 class="uppercase"><?php echo lang('Upload Data Saldo Tabungan'); ?></h2>

        <form action="ureg_balance.php" method="post" enctype="multipart/form-data">

            <br><br>
            <h4><?php echo lang('Informasi'); ?></h4>
            <p><?php echo lang('Formulir ini hanya menerima 1 (satu) kali upload'); ?></p>

            <label class="validate control-label"><?php echo lang('Data Excel'); ?></label>
            <input type="file" name="filename" class="form-control" accept=".xls"  required="">

            <div style="margin-top: 30px;">
                <button type="submit" class="waves-effect waves-light btn-large primary-color width-100">Submit</button>
            </div>

            <div style="margin-top: 10px;">
                <a href="dreg_balance.php" class="waves-effect waves-light btn-large primary-color width-100">Download Template</a>
            </div>

        </form>

    </div>
</div>

<?php require('footer_new.php');?>
