<?php
@session_start();
//error_reporting(0);
require('header.php');
require('sidebar-right.php');
require('sidebar-left.php');

$gback = @$_GET['back'];
$ghqr = @$_GET['hqr'];
$ggud = @$_GET['gud'];
$gglr = @$_GET['glr'];
$gfud = @$_GET['fud'];
$gflr = @$_GET['flr'];
$gfs = @$_GET['fs'];

	/* Setting SkyParking Baronang
	Setting Gambar QR :
	(atas | bawah) x (kiri | kanan) = 330 x 155 -> 13 / 340 x 165 -> 5

	Setting Tulisan :
	(atas | bawah) x (kiri | kanan) = 740 x 145
	font = 31
	*/

	function get_string_between($string, $start, $end){
	    $string = ' ' . $string;
	    $ini = strpos($string, $start);
	    if ($ini == 0) return '';
	    $ini += strlen($start);
	    $len = strpos($string, $end, $ini) - $ini;
	    return substr($string, $ini, $len);
	}

	function saveQRImage($templ, $qr, $gud, $glr, $fud, $flr, $fs, $pre_sav) {
		

		$temp_format = substr($templ, -4);
		if($temp_format == '.jpg') {
			$dest = imagecreatefromjpeg('qrtemplate/'.$templ);
		}
		else if($temp_format == '.jpeg') {
			$dest = imagecreatefromjpeg('qrtemplate/'.$templ);
		}
		else if($temp_format == '.png') {
			$dest = imagecreatefrompng('qrtemplate/'.$templ);
		}

		$cor = imagecolorallocate($dest, 255, 255, 255);
		$font = 'fonts/seguisb.ttf';

		$qr_bformat = substr($qr, -4);
		$qr_fformat = substr($qr, 0, 2);
		if($qr_bformat == '.jpg') {
			if($qr_fformat == 'qr') {
				$fqr = get_string_between($qr, 'qr', '.jpg');
			}
			else {
				$fqr = split ("\.", $qr);
				$fqr = $fqr[0];
			}
			$src = imagecreatefromjpeg('qrtemplate/'.$qr);
		}
		else if($qr_bformat == '.jpeg') {
			if($qr_fformat == 'qr') {
				$fqr = get_string_between($qr, 'qr', '.jpeg');
			}
			else {
				$fqr = split ("\.", $qr);
				$fqr = $fqr[0];
			}
			$src = imagecreatefromjpeg('qrtemplate/'.$qr);
		}
		else if($qr_bformat == '.png') {
			if($qr_fformat == 'qr') {
				$fqr = get_string_between($qr, 'qr', '.png');
			}
			else {
				$fqr = split ("\.", $qr);
				$fqr = $fqr[0];
			}
			$src = imagecreatefrompng('qrtemplate/'.$qr);
		}
		//$fqr = $fqr[0];
		
		imagettftext($dest, $fs, 0, $flr, $fud, $cor, $font, $fqr);
		imagecopymerge($dest, $src, $glr, $gud, 0, 0, 325, 325, 100);

		//header('Content-Type: image/jpeg');
		//imagejpeg($dest);
		$hqr = 'hasil'.$qr;
		imagepng($dest,'qrpreview/'.$hqr);
		
		if($pre_sav == 'save') { ?>
			<script type="text/javascript">
				window.location.href = "proc_qr_adjust.php?hqr=<?php echo $hqr; ?>&template=<?php echo $templ; ?>&gud=<?php echo $gud; ?>&glr=<?php echo $glr; ?>&fud=<?php echo $fud; ?>&flr=<?php echo $flr; ?>&fs=<?php echo $fs; ?>";
			</script>
			<?php
		}
		else {
			?>
			<script type="text/javascript">
				window.location.href = "qr_adjust.php?back=<?php echo $templ; ?>&qr=<?php echo $qr; ?>&hqr=<?php echo $hqr; ?>&gud=<?php echo $gud; ?>&glr=<?php echo $glr; ?>&fud=<?php echo $fud; ?>&flr=<?php echo $flr; ?>&fs=<?php echo $fs; ?>";
			</script>
			<?php
		}
	}

?>

	<div class="animated fadeinup delay-1">
		<div class="page-content txt-black">

			<h2 class="uppercase txt-black">Pengaturan QR Code</h2>

			<div class="form-inputs">
				<form action="" method="post" enctype="multipart/form-data">

					<!--<img src="qrhasil2/qr7000251810522918.jpg">-->
					<?php
					if(@$_GET['hqr']) { ?>
						<img src="qrpreview/<?php echo $ghqr; ?>">
						<?php
					}
					?>
					
					<div class="input-field">
						<?php 
						if($_SESSION['device'] == 0){ ?>
							
							<?php
						} 
						?>

						<?php 
						if($_SESSION['device'] == 1){ ?>

							<?php
						} ?>
					</div>
					
					<div style="">
						<div class="row">
							<div class="col s6">
								<div class="input-field">
									<span class="">Gambar Template</span><br>
									<input type="file" name="template" id="template">
								</div>
							</div>

							<div class="col s6">
								<div class="input-field">
									<span class="">Gambar qr</span><br>
									<input type="file" name="qr" id="qr">
								</div>
							</div>
						</div>
					</div>

					
					
					<div style="border: 1px solid #aaa; padding: 10px; border-radius: 20px; margin-top: 10px;">
						<span style="font-weight: bold;">Setting Gambar QR</span>

						<div class="row">
							<div class="col s6">
								<div class="input-field">
									<div class="center"><b> <-- Atas | Bawah --> </b></div>
									<input type="range" name="range_gud" id="range_gud" value="<?php if(@$_GET['gud']) echo $ggud; else echo '430'; ?>" max="1000">
								</div>
							</div>

							<div class="col s6">
								<div class="input-field">
									<div class="center"><b> <-- Kiri | Kanan --> </b></div>
									<input type="range" name="range_glr" id="range_glr" value="<?php if(@$_GET['glr']) echo $gglr; else echo '180'; ?>" max="1000">
								</div>
							</div>
						</div>
					</div>

					<div style="border: 1px solid #aaa; padding: 10px; border-radius: 20px; margin-top: 10px;">
						<span style="font-weight: bold;">Setting Tulisan</span>
						
						<div class="row">
							<div class="col s6">
								<div class="input-field">
									<div class="center"><b> <-- Atas | Bawah --> </b></div>
									<input type="range" name="range_fud" id="range_fud" value="<?php if(@$_GET['fud']) echo $gfud; else echo '1470'; ?>" max="5000">
								</div>
							</div>

							<div class="col s6">
								<div class="input-field">
									<div class="center"><b> <-- Kiri | Kanan --> </b></div>
									<input type="range" name="range_flr" id="range_flr" value="<?php if(@$_GET['flr']) echo $gflr; else echo '280'; ?>" max="5000">
								</div>
							</div>
						</div>

						<div class="input-field">
							<div class="center"><b> Ukuran Font </b></div>
							<input type="range" name="range_font" id="range_font" value="<?php if(@$_GET['fs']) echo $gfs; else echo '61'; ?>" max="100">
						</div>
					</div>
					
					<div class="row">
						<div class="col s6">
							<button type="submit" name="ok" id="ok" class="btn btn-large width-100 primary-color waves-effect waves-light m-t-30">Preview</button>
						</div>

						<div class="col s6">
							<button type="submit" name="save" id="save" class="btn btn-large width-100 primary-color waves-effect waves-light m-t-30">Simpan</button>
						</div>
					</div>

					
				</form>

				<?php
				$ok = @$_POST['ok'];
				if(isset($ok)) {
					$src_template = @$_FILES['template']['tmp_name'];
                    $template = @$_FILES['template']['name'];

                    $src_qr = @$_FILES['qr']['tmp_name'];
                    $qr = @$_FILES['qr']['name'];

					$gud = @$_POST['range_gud'];
					$glr = @$_POST['range_glr'];
					$fud = @$_POST['range_fud'];
					$flr = @$_POST['range_flr'];
					$fs  = @$_POST['range_font'];
					
					//echo 'template: '.$src_template.' -- '.$template.'<br>';
					//echo 'qr: '.$src_qr.' -- '.$qr.'<br>';

					if(@$_GET['back']) {
						$template = @$_GET['back'];
					}
					else {
						move_uploaded_file($src_template, 'qrtemplate/'.$template);
					}

					if(@$_GET['qr']) {
						$qr = @$_GET['qr'];
					}
					else {
						move_uploaded_file($src_qr, 'qrtemplate/'.$qr);
					}

					//move_uploaded_file($src_template, 'qrtemplate/'.$template);
					//move_uploaded_file($src_qr, 'qrtemplate/'.$qr);
					//saveQRImage($template,'7000251810522918',$gud,$glr,$fud,$flr,$fs);
					saveQRImage($template, $qr, $gud, $glr, $fud, $flr, $fs, 'preview');
				}

				$hqr = '';
				$save = @$_POST['save'];
				if(isset($save)){
					$src_template = @$_FILES['template']['tmp_name'];
                    $template = @$_FILES['template']['name'];

                    $src_qr = @$_FILES['qr']['tmp_name'];
                    $qr = @$_FILES['qr']['name'];

					
					if(@$_GET['gud']) $gud = @$_GET['gud'];
					else $gud = @$_POST['range_gud'];

					
					if(@$_GET['glr']) $glr = @$_GET['glr'];
					else $glr = @$_POST['range_glr'];

					
					if(@$_GET['fud']) $fud = @$_GET['fud'];
					else $fud = @$_POST['range_fud'];
					

					
					if(@$_GET['flr']) $flr = @$_GET['flr'];
					else $flr = @$_POST['range_flr'];

					
					if(@$_GET['fs']) $fs = @$_GET['fs'];
					else $fs = @$_POST['range_font'];


					if(@$_GET['back']) {
						$template = @$_GET['back'];
					}
					else {
						move_uploaded_file($src_template, 'qrtemplate/'.$template);
					}

					if(@$_GET['qr']) {
						$qr = @$_GET['qr'];
					}
					else {
						move_uploaded_file($src_qr, 'qrtemplate/'.$qr);
					}

					//move_uploaded_file($src_template, 'qrtemplate/'.$template);
					//move_uploaded_file($src_qr, 'qrtemplate/'.$qr);
					//saveQRImage('7000251810522918',$gud,$glr,$fud,$flr,$fs);
					saveQRImage($template, $qr, $gud, $glr, $fud, $flr, $fs, 'save');
				}
				?>

				<div class="table-responsive m-t-30">
					<table class="table">
						<thead>
							<tr>
								<th>#</th>
								<th>QR Code</th>
								<th>Template Atas:Bawah</th>
								<th>Template Kiri:Kanan</th>
								<th>QR Atas:Bawah</th>
								<th>QR Kiri:Kanan</th>
								<th>QR Font</th>
								<th></th>
							</tr>
						</thead>

						<tbody>
							<?php
							$x = 1;
							$sql = "SELECT * from KoperasiNew.dbo.qrcode order by qrcode ASC";
							$query = sqlsrv_query($conn, $sql);
							while($data = sqlsrv_fetch_array($query)) { ?>
								<tr>
									<td><?php echo $x; ?></td>
									<td><?php echo get_string_between($data[0],'hqr','.jpg'); ?></td>
									<td><?php echo $data[1]; ?></td>
									<td><?php echo $data[2]; ?></td>
									<td><?php echo $data[3]; ?></td>
									<td><?php echo $data[4]; ?></td>
									<td><?php echo $data[5]; ?></td>
									<td><button type="button" id="btn_view-qr<?php echo get_string_between($data[0],'hqr','.jpg'); ?>" class="btn primary-color width-100 waves-light waves-effect"><i class="ion-eye"></i></button></td>
								</tr>
								<script type="text/javascript">
									$('#btn_view-qr<?php echo get_string_between($data[0],'hqr','.jpg'); ?>').click(function() {
										window.location.href = "qr_adjust.php?hqr=<?php echo $data[0]; ?>&gud=<?php echo $data[1]; ?>&glr=<?php echo $data[2]; ?>&fud=<?php echo $data[3]; ?>&flr=<?php echo $data[4]; ?>&fs=<?php echo $data[5]; ?>";
									});
								</script>
								<?php
								$x++;
							}
							?>
						</tbody>
					</table>
				</div>

			</div>

		</div>
	</div>

<?php require('footer.php');?>