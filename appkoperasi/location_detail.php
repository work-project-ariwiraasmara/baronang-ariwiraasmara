<?php
require('header.php');
require('sidebar-left.php');
require('sidebar-right.php');
?>

	<div class="animated fadeinup delay-1">
    	<div class="page-content">

    		<h1 class="uppercase">Aktivasi Lokasi</h1>

    		<div class="form-inputs">
    			<form action="" method="post">
    				
    				<div class="input-field">
    					<b>Lokasi</b> : <br>
    					<select class="browser-default" name="loc" id="loc">
    						<?php
    						$sloc = "SELECT LocationID, Nama from [dbo].[LocationMerchant]";
    						$qloc = sqlsrv_query($conn, $sloc);
                    		while($dloc = sqlsrv_fetch_array($qloc)) { ?>
                    			<option value="<?php echo $dloc[0]; ?>"><?php echo $dloc[1]; ?></option>
                    			<?php
							}
    						?>
    					</select>
    				</div>

    				<div class="input-field">
    					<label for="blokname">Nama Blok</label>
    					<input type="text" name="blokname" id="blokname" class="validate" required>
    				</div>

    				<div class="input-field">
    					<label for="blokno">Nomor Blok</label>
    					<input type="number" name="blokno" id="blokno" class="validate">
    				</div>

    				<div class="input-field">
    					<label for="blokdet">Blok Detil</label>
    					<input type="text" name="blokdet" id="blokdet" class="validate">
    				</div>

    				<div class="input-field">
    					<label for="company">Nama Perusahaan</label>
    					<input type="text" name="company" id="company" class="validate" required>
    				</div>

    				<div class="input-field">
    					<label for="user">Username</label>
    					<input type="text" name="user" id="user" class="validate" required>
    				</div>

    				<div class="input-field">
    					<label for="email">Email</label>
    					<input type="email" name="email" id="email" class="validate" required>
    				</div>

    				<div class="input-field">
    					<label for="pass">Password</label>
    					<input type="text" name="pass" id="pass" class="validate" required>
    				</div>

    				<div class="input-field">
    					<label for="pin">PIN</label>
    					<input type="text" name="pin" id="pin" class="validate" required>
    				</div>

    				<div class="input-field">
    					<label for="tlp">No. Telepon</label>
    					<input type="number" name="tlp" id="tlp" class="validate">
    				</div>

    				<div class="input-field">
    					<label for="quota">Quota</label>
    					<input type="number" name="quota" id="quota" class="validate">
    				</div>

    				<button type="submit" name="btnok" id="btnok" class="btn btn-large primary-color width-100 waves-effect waves-light m-t-30">Simpan</button>

    			</form>

    			<?php
    			if(isset($_POST['btnok'])) {
    				$loc 		= @$_POST['loc'];
    				$blokname 	= @$_POST['blokname'];
    				$blokno 	= @$_POST['blokno'];
    				$blokdet 	= @$_POST['blokdet'];
    				$cpy 		= @$_POST['company'];
    				$tlp 		= @$_POST['tlp'];
    				$email 		= @$_POST['email'];
    				$quota 		= @$_POST['quota'];
    				$user 		= @$_POST['user'];
    				$pass 		= sha1( md5( crypt(@$_POST['pass'], '$6$rounds=999999999') ) );
    				$pin 		= sha1( md5( crypt(@$_POST['pin'], '$6$rounds=999999999') ) );

    				if(	$loc == '' || empty($loc) || 
    					$blokname == '' || empty($blokname) || 
    					$cpy == '' || empty($cpy) ||
    					$email == '' || empty($email) ||
    					$user == '' || empty($user) ||
    					$pass == '' || empty($pass) ) { ?>
    					<script type="text/javascript">
    						alert('Nama Blok, Nama Perusahaan, Lokasi, Username, Email, Password Tidak Boleh Kosong!');
    					</script>
    					<?php
    				}
    				else { 

    					// get BLOCK ID
    					$s1 = "SELECT TOP 1 BlockID FROM [dbo].[LocationDetail] ORDER BY BlockID DESC";
                    	$q1 = sqlsrv_query($conn, $s1);
                    	$d1 = sqlsrv_fetch_array($q1);

                    	$is1 = 0;
		                $gls1 = substr($d1[0], -6);
		                $gps1 = (int)$gls1 + 1;
		                $is1 = str_pad($gps1, 7, '0', STR_PAD_LEFT);
		                $blokid = 'LOC'.substr($loc, 10).$is1;
		                echo 'Block ID 1 : '.$d1[0].'<br>Block ID 2 : '.$blokid.'<br>';


		                // get User ID
		                $s2 = "SELECT TOP 1 UserID FROM [dbo].[LocationCardLogin] ORDER BY UserID DESC";
                    	$q2 = sqlsrv_query($conn, $s2);
                    	$d2 = sqlsrv_fetch_array($q2);

                    	$is2 = 0;
		                $gls2 = substr($d2[0], -7);
		                $gps2 = (int)$gls2 + 1;
		                $is2 = str_pad($gps2, 7, '0', STR_PAD_LEFT);
		                $userid = '700097CMP'.$is2;
		                echo 'User ID 1 : '.$d2[0].'<br>User ID 2 : '.$userid.'<br>';

		                $s3 = "INSERT into [dbo].[LocationDetail] values('$loc', '$blokid', '$blokname', '$blokno', '$blokdet', '$cpy', '$tlp', '$email', '', '$quota', '0' , '1')";
		                $q3 = sqlsrv_query($conn, $s3);

		                if($q3) {
			                $s4 = "INSERT into [dbo].[LocationCardLogin] values('$loc', '$blokid', '$userid', '$user', '$cpy', '$tlp', '$email', '', '1', '$pass', '$pin')";
			                $q4 = sqlsrv_query($conn, $s4);

			                if($q4) { ?>
			                	<script type="text/javascript">
			                		alert('Berhasil Simpan Data');
			                	</script>
			                	<?php
			                }
			                else { ?>
			                	<script type="text/javascript">
			                		alert('Gagal Simpan Data User Login!');
			                	</script>
			                	<?php
			                }
		                }
		                else { ?>
			                <script type="text/javascript">
			                	alert('Gagal Simpan Data Lokasi');
			                </script>
			                <?php
		                }		                
    				}
    			}
    			?>

    		</div>

    	</div>
    </div>

<?php require('footer.php'); ?>