<?php require('header.php');?>

<?php require('sidebar-right.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

 <div class="page-content">






<?php if(isset($_GET['trx']) ){ ?>
	<div>
        <?php if($_GET['st'] == 5 ){ ?>
		<a href="buku_besar.php"><button type="button" class="waves-effect waves-light btn-large primary-color width-100" style="margin-top: 20px;">Back</button></a>
        <?php } 
        else if ($_GET['st'] == 4 ) { ?>
        <a href="rep_neraca.php"><button type="button" class="waves-effect waves-light btn-large primary-color width-100" style="margin-top: 20px;">Back</button></a>
        <?php }
        else if ($_GET['st'] == 3 ) { ?>
        <a href="rep_pnl.php"><button type="button" class="waves-effect waves-light btn-large primary-color width-100" style="margin-top: 20px;">Back</button></a>
        <?php } ?>

        <a href="drep_neracaHISAK.php?from=<?php echo $_GET['tgl1']; ?>&to=<?php echo $_GET['tgl2']; ?>&acc2=<?php echo $_GET['trx']; ?>&st=4"><button type="button" class="waves-effect waves-light btn-large primary-color width-100" style="margin-top: 20px;">Download To Excel</button></a>   	
	</div>
	 

	<div class="box-header" align="center" style="margin-top: 30px;">
        <tr>
            <h3 class="box-title" align="center"><?php echo lang('Detail History Transaksi'); ?></h3>
        </tr>
    </div>

    <div class="box-header " align="Center">
        <tr>
            <h3 class="box-title" align="center"><?php echo ('No. Transaksi : '); ?> <?php echo $_GET['trx']; ?></h3>
        </tr>
    </div>


    <div class="box-header with-border" align="center">
        <tr>
		<?php
                            $bulan = date('m', strtotime($_GET['tgl1']));
                            $tahun = date('Y', strtotime($_GET['tgl1']));
                            $bulan2 = date('m', strtotime($_GET['tgl2']));
                            $tahun2 = date('Y', strtotime($_GET['tgl2']));
                            if ($bulan != 1) {
                                if ($bulan != 2 ) {
                                   if ($bulan != 3) {
                                       if ($bulan != 4) {
                                            if ($bulan !=5) {
                                                if ($bulan !=6) {
                                                    if ($bulan !=7) {
                                                        if ($bulan !=8) {
                                                            if ($bulan !=9) {
                                                                if ($bulan !=10) {
                                                                    if ($bulan !=11) {
                                                                        $bulan = 'Desember';
                                                                    } else {
                                                                        $bulan = 'Nopember';
                                                                    }
                                                                } else {
                                                                    $bulan = 'Oktober';
                                                                }
                                                            } else {
                                                                $bulan = 'September';
                                                            }
                                                        } else {
                                                            $bulan = 'Agustus';
                                                        }
                                                    } else {
                                                        $bulan = 'Juli';
                                                    }
                                                } else {
                                                    $bulan = 'Juni';
                                                }
                                            } else {
                                              $bulan = 'Mei';  
                                            }
                                       } else {
                                            $bulan = 'April';       
                                       }
                                   } else {
                                    $bulan = 'Maret';
                                   }
                                } else {
                                    $bulan = 'Februari';    
                                }                               
                            } else {
                                $bulan = 'Januari';
                            }
                        ?>
                        <?php
                        if ($bulan2 != 1) {
                                if ($bulan2 != 2 ) {
                                   if ($bulan2 != 3) {
                                       if ($bulan2 != 4) {
                                            if ($bulan2 !=5) {
                                                if ($bulan2 !=6) {
                                                    if ($bulan2 !=7) {
                                                        if ($bulan2 !=8) {
                                                            if ($bulan2 !=9) {
                                                                if ($bulan2 !=10) {
                                                                    if ($bulan2 !=11) {
                                                                        $bulan2 = 'Desember';
                                                                    } else {
                                                                        $bulan2 = 'Nopember';
                                                                    }
                                                                } else {
                                                                    $bulan2 = 'OKtober';
                                                                }
                                                            } else {
                                                                $bulan2 = 'September';
                                                            }
                                                        } else {
                                                            $bulan2 = 'Agustus';
                                                        }
                                                    } else {
                                                        $bulan2 = 'Juli';
                                                    }
                                                } else {
                                                    $bulan2 = 'Juni';
                                                }
                                            } else {
                                              $bulan2 = 'Mei';  
                                            }
                                       } else {
                                            $bulan2 = 'April';       
                                       }
                                   } else {
                                    $bulan2 = 'Maret';
                                   }
                                } else {
                                    $bulan2 = 'Februari';    
                                }                               
                            } else {
                                $bulan2 = 'Januari';
                            }
                        ?>
            <h3 class="box-title"><?php echo "Periode : "; ?><?php echo $bulan," "; ?><?php echo $tahun," - "; ?><?php echo $bulan2," "; ?><?php echo $tahun2; ?></h3>
        </tr>

        <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th><?php echo lang('Kode Jurnal'); ?></th>
                                <th><?php echo lang('Kode Akun Debit'); ?></th>
                                <th><?php echo lang('Nama Akun Debit'); ?></th>
                                <th><?php echo lang('Kode Akun Kredit'); ?></th>
                                <th><?php echo lang('Nama Akun Kredit'); ?></th>
                                <th><?php echo ('Amount (Rp.)'); ?></th>
                                <th><?php echo lang('Deskripsi'); ?></th>
                                
                            </tr>
                        </thead>

                            <?php
                            $aaa = "SELECT * FROM ( SELECT a.transno, a.debet, b.namaaccount, a.kredit, c.namaaccount as namaaccount2, a.amount, a.note, a.date, ROW_NUMBER() OVER (ORDER BY a.date asc) as row FROM [dbo].[translist] a inner join dbo.account b on a.debet = b.KodeAccount left join dbo.Account c on a.kredit = c.kodeaccount where Transno = '$_GET[trx]' ) a ORDER BY Transno asc";
                            //$aaa = "SELECT * FROM ( SELECT a.KodeJurnal,a.KodeAccount,a.Debet,a.Kredit,a.Keterangan, ROW_NUMBER() OVER (ORDER BY b.Tanggal asc) as row FROM [dbo].[JurnalDetail] a inner join [dbo].[JurnalHeader] b on a.KodeJurnal = b.KodeJurnal where a.KodeJurnal= '$_GET[trx]' ) a ORDER BY KodeJurnal asc";
                            //SELECT * FROM ( SELECT a.debet, b.NamaAccount, a.Kredit, c.NamaAccount as namaaccount2, ROW_NUMBER() OVER (ORDER BY a.Date asc) as row FROM [dbo].[Translist] a inner join [dbo].[Account] b on a.Debet = b.Kodeaccount left join [dbo].[Account] c on a.kredit = c.Kodeaccount where a.Transno= '700095TDOP190401000001'  ) a

                            //echo $aaa;
                            //echo 'Data Masih belum sinkronasi';
                            $bbb = sqlsrv_query($conn, $aaa);
                            while($ccc = sqlsrv_fetch_array($bbb, SQLSRV_FETCH_NUMERIC)){
                                // $noacc = '';
                                // $acc = '';
                                // if ($ccc[3] != null ){
                                //     $sql   = "select * from dbo.Account where KodeAccount='$ccc[3]'";
                                //     $stmt  = sqlsrv_query($conn, $sql);
                                //     $row   = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_NUMERIC);
                                //     if($row != null){
                                        
                                //     }

                                //}
                                ?>

                                <tr>
                                    <td><?php echo $ccc[8]; ?></td>
                                    <td><?php echo $ccc[0]; ?></td>
                                    <td><?php echo $ccc[1]; ?></td>
                                    <td><?php echo $ccc[2]; ?></td>
                                    <td><?php echo $ccc[3]; ?></td>
                                    <td><?php echo $ccc[4]; ?></td>
                                    <td style="text-align: right;"><?php echo number_format($ccc[5],2); ?></td>
                                    <td><?php echo $ccc[6]; ?></td>
                                </tr>
                        <?php } ?>

                </table>
    </div>
<?php } ?>


 </div>


<?php require('footer_new.php');?>