<?php
require('header.php');
require('sidebar-left.php');
require('sidebar-right.php');
?>

	<div class="animated fadeinup delay-1">
    	<div class="page-content">

    		<div class="row">
    			<div class="col s12">
    				<a href="reg_mb2b_compl.php?reg=b2b"><button class="btn btn-large primary-color width-100 waves-effect waves-light">Registrasi B2B</button></a>
    			</div>
    			<div class="col s12">
    				<a href="reg_mb2b_compl.php?reg=compl"><button class="btn btn-large primary-color width-100 waves-effect waves-light">Registrasi Compliment</button></a>
    			</div>
    		</div>

    		<div class="hide" id="mb2b">
    			<div class="row">
    				<div class="col s6">
    					<a href="reg_mb2b_compl.php?reg=b2b"><button class="btn btn-large primary-color width-100 waves-light waves-effect">Registrasi</button></a>
    				</div>
    				<div class="col s6">
    					<a href="activated_mb2b_compl.php?actv=b2b"><button class="btn btn-large primary-color width-100 waves-light waves-effect">Activate</button></a>
    				</div>
    			</div>
    		</div>

    		<div class="hide" id="mcompl">
    			<div class="row">
    				<div class="col s6">
    					<a href="reg_mb2b_compl.php?reg=compl"><button class="btn btn-large primary-color width-100 waves-light waves-effect">Registrasi</button></a>
    				</div>
    				<div class="col s6">
    					<a href="activated_mb2b_compl.php?actv=compl"><button class="btn btn-large primary-color width-100 waves-light waves-effect">Activate</button></a>
    				</div>
    			</div>
    		</div>

    	</div>
    </div>

    <script type="text/javascript">
    	$('#btn_b2b').click(function() {
    		if( $('#mb2b').hasClass('hide')  ) {
    			$('#mb2b').removeClass('hide');
    			$('#mcompl').addClass('hide');
    		}
    		else{
    			$('#mb2b').addClass('hide');
    			$('#mcompl').addClass('hide');
    		}
    	});

    	$('#btn_compl').click(function() {
    		if( $('#mcompl').hasClass('hide')  ) {
    			$('#mcompl').removeClass('hide');
    			$('#mb2b').addClass('hide');
    		}
    		else{
    			$('#mcompl').addClass('hide');
    			$('#mb2b').addClass('hide');
    		}
    	});
    </script>

<?php require('footer.php'); ?>
