<?php
session_start();
error_reporting(0);

include('lib/phpExcel/Classes/PHPExcel.php');
include "connect.php";


    $filePath = "uploads/excel/";

    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getProperties()->setCreator("baronang");
    $objPHPExcel->getProperties()->setTitle("Template COA");

// set autowidth
    for($col = 'A'; $col !== 'Z'; $col++) {
        $objPHPExcel->getActiveSheet()
            ->getColumnDimension($col)
            ->setAutoSize(true);
    }

    $listCombo = array();

    //sheet 2
    $objWorkSheet = $objPHPExcel->createSheet(0);
// baris judul
    
    $objWorkSheet->getStyle('D1')->getFont()->setBold(true);
    $objWorkSheet->getStyle('D1')->getFont()->setSize(12);
    $objWorkSheet->SetCellValue('D1', 'Laporan Neraca All');
    $objWorkSheet->getStyle('D2')->getFont()->setBold(true);
    $objWorkSheet->getStyle('D2')->getFont()->setSize(12);
    $objWorkSheet->SetCellValue('D2', 'Periode :');


    $objWorkSheet->getStyle('B4')->getFont()->setBold(true);
    $objWorkSheet->getStyle('B4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('B4', 'Aktiva');
    $objWorkSheet->getStyle('G4')->getFont()->setBold(true);
    $objWorkSheet->getStyle('G4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('G4', 'Pasiva');


    $objWorkSheet->getStyle('A5:C5')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    $objWorkSheet->getStyle('A5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('A5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('A5', 'Kode Akun');
    $objWorkSheet->getStyle('B5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('B5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('B5', 'Nama');
    $objWorkSheet->getStyle('C5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('C5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('C5', 'Nilai');

    $objWorkSheet->getStyle('F5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('F5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('F5', 'Kode Akun');
    $objWorkSheet->getStyle('G5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('G5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('G5', 'Nama');
    $objWorkSheet->getStyle('H5')->getFont()->setBold(true);

    $objWorkSheet->SetCellValue('H5', 'Nilai');



if($_GET['bulan'] and $_GET['tahun'] ){
    $no = 1;
    $from1 = $_GET['tahun'].'/'.$_GET['bulan'].'/01';
    $from = date('Y-m-01', strtotime($from1));
    $to3 = $_GET['tahun'].'/'.$_GET['bulan'].'/01';
    $to = date('Y-m-t', strtotime($to3));
    $to1 = date('m', strtotime($to3));
    $to2 = date('Y', strtotime($to3));
    $a = "exec dbo.ProsesGenerateLaporanNeraca '$to','$_SESSION[Name]'";
    //echo $a;
    $b = sqlsrv_query($conn, $a);    

    $bln = 2 ;
    $bulan = date('m', strtotime($to));
    //echo $bulan;
    $tahun = date('Y', strtotime($to));
        if ($bulan != 01) {
            if ($bulan != 2 ) {
                if ($bulan != 3) {
                    if ($bulan != 4) {
                        if ($bulan !=5) {
                            if ($bulan !=6) {
                                if ($bulan !=7) {
                                    if ($bulan !=8) {
                                        if ($bulan !=9) {
                                            if ($bulan !=10) {
                                                if ($bulan !=11) {
                                                    $bulan = 'Desember';
                                                } else {
                                                $bulan = 'Nopember';
                                                }
                                            } else {
                                            $bulan = 'OKtober';   
                                            }
                                        } else {
                                        $bulan = 'September';
                                        }
                                    } else {
                                    $bulan = 'Agustus';
                                    }
                                } else {
                                $bulan = 'Juli';
                                }
                            } else {
                            $bulan = 'Juni';
                            }
                        } else {
                        $bulan = 'Mei';  
                        }
                    } else {
                    $bulan = 'April';       
                    }
                } else {
                $bulan = 'Maret';
                }
            } else {
            $bulan = 'Februari';    
            }                               
        } else {
        $bulan = 'Januari';
        }

        $objWorkSheet->getStyle('E')->getFont()->setBold(true);
        $objWorkSheet->SetCellValue("E".$bln,$bulan .'-'. $tahun);   



        $H1 = 6;
           
        $from1 = $_GET['tahun'].'/'.$_GET['bulan'].'/01';
        $from = date('Y-m-01', strtotime($from1));
        $to3 = $_GET['tahun'].'/'.$_GET['bulan'].'/01';
        $to = date('Y-m-t', strtotime($to3));
        $to1 = date('m', strtotime($to3));
        $to2 = date('Y', strtotime($to3));
        $name = $_SESSION[Name];
        $x = "select* from dbo.GroupAcc where KodeGroup in('1.0.00.00.000')";
        $atotal = 0;
        $y = sqlsrv_query($conn, $x);
        while($z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC)){
            //var_dump($z);
             
        $objWorkSheet->SetCellValue("A".$H1,$z[0]);
        $objWorkSheet->SetCellValue("B".$H1,$z[1]);

        $H1++;

                $H2 = 6;
                $xx = "select * from dbo.TypeAcc where KodeGroup = '$z[0]' ORDER BY KodeTipe Asc";
                //echo $atotal1; 
                $yy = sqlsrv_query($conn, $xx);
                $total2=0;
                $total3=0;
                while($zz = sqlsrv_fetch_array($yy, SQLSRV_FETCH_NUMERIC)){
                    //var_dump($zz);    

                $abc = "select SUM (NilaiHide) from dbo.LaporanNeracaViewNew where GroupID = '$z[0]' and header =2 and Bulan = '$to1' and Tahun = '$to2' and UserName = '$name'";
                //echo $abc;
                $bcd = sqlsrv_query($conn, $abc);
                $b=sqlsrv_fetch_array($bcd, SQLSRV_FETCH_NUMERIC);
                $dtotal1=$b[0];
                $atotal= $atotal1+$dtotal1;

                $H2=$H1++;
                $dtotal1+=$b[0];

                $objWorkSheet->SetCellValue("A".$H2,$zz[0]);    
                $objWorkSheet->SetCellValue("B".$H2,$zz[1]);    


                    $h3 = 6 ;
                    $xxx = "select * from dbo.LaporanNeracaViewNew where TypeID = '$zz[0]' and header = 0 and Bulan = '$to1' and Tahun = '$to2' and UserName = '$name' ORDER BY KodeAccount Asc"; 
                    //echo $xxx;
                    $yyy = sqlsrv_query($conn, $xxx);
                    while($zzz = sqlsrv_fetch_array($yyy, SQLSRV_FETCH_NUMERIC)){
                        //var_dump($zzz);


                        $abc = "select SUM (NilaiHide) from dbo.LaporanNeracaViewNew where TypeID = '$zzz[2]' and header =2 and Bulan = '$to1' and Tahun = '$to2' and UserName = '$name' and KodeAccount like '%".substr($zzz[0],0,6)."%'";
                        //echo $abc;
                        $bcd = sqlsrv_query($conn, $abc);
                        $b=sqlsrv_fetch_array($bcd, SQLSRV_FETCH_NUMERIC);
                        $c=$b[0];

                    
                    $h3=$H1++;

                    $objWorkSheet->SetCellValue("A".$h3,$zzz[0]);    
                    $objWorkSheet->SetCellValue("B".$h3,$zzz[1]);  
                    $objWorkSheet->getStyle('C')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                    $objWorkSheet->SetCellValue("C".$h3, number_format($c), PHPExcel_Cell_DataType::TYPE_STRING);  


                        $h4 = 6 ;
                        $xxxx = "select * from dbo.LaporanNeracaViewNew where TypeID = '$zzz[2]' and header = 1 and UserName = '$name' and KodeAccount like '%".substr($zzz[0],0,6)."%' ORDER BY KodeAccount Asc";
                                                        
                        $yyyy = sqlsrv_query($conn, $xxxx);
                        while($zzzz = sqlsrv_fetch_array($yyyy, SQLSRV_FETCH_NUMERIC)){
                        $abc = "select SUM (NilaiHide) from dbo.LaporanNeracaViewNew where TypeID = '$zzzz[2]' and header =2 and Bulan = '$to1' and Tahun = '$to2' and UserName = '$name' and KodeAccount like '%".substr($zzzz[0],0,9)."%'";
                        //echo $abc;
                        $bcd = sqlsrv_query($conn, $abc);
                        $b=sqlsrv_fetch_array($bcd, SQLSRV_FETCH_NUMERIC);
                        $a=$b[0];


                        $h4=$H1++;

                        $objWorkSheet->SetCellValue("A".$h4,$zzzz[0]);    
                        $objWorkSheet->SetCellValue("B".$h4,$zzzz[1]);  
                        $objWorkSheet->SetCellValue("C".$h4, number_format($a), PHPExcel_Cell_DataType::TYPE_STRING);  


                            $h5 = 6;
                            $from1 = $_GET['tahun'].'/'.$_GET['bulan'].'/01';
                            $from = date('Y-m-01', strtotime($from1));
                            $to3 = $_GET['tahun'].'/'.$_GET['bulan'].'/01';
                            $to = date('Y-m-t', strtotime($to3));
                            $to1 = date('m', strtotime($to3));
                            $to2 = date('Y', strtotime($to3));
                            $name = $_SESSION[Name];
                            $xxxxx ="select * from dbo.LaporanNeracaViewNew where TypeID = '$zzzz[2]' and header =2 and Bulan = '$to1' and Tahun = '$to2' and UserName = '$name' and KodeAccount like '%".substr($zzzz[0],0,9)."%' ORDER BY KodeAccount Asc";
                            //echo $xxxxx;
                            $yyyyy = sqlsrv_query($conn, $xxxxx);
                            while($zzzzz = sqlsrv_fetch_array($yyyyy, SQLSRV_FETCH_NUMERIC)){  


                            $h5=$H1++;

                            $objWorkSheet->SetCellValue("A".$h5,$zzzzz[0]);    
                            $objWorkSheet->SetCellValue("B".$h5,$zzzzz[1]);  
                            $objWorkSheet->SetCellValue("C".$h5, number_format($zzzzz[7]), PHPExcel_Cell_DataType::TYPE_STRING);  


                        }
                    }
                }

            }


          }
            $objWorkSheet->getStyle('A5:C' .$H1)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objWorkSheet->getStyle('A')->getFont()->setBold(true);
            $objWorkSheet->SetCellValue('A'.$H1,'Saldo Akhir');
            $objWorkSheet->getStyle('C')->getFont()->setBold(true);
            $objWorkSheet->SetCellValue("C".$H1, number_format($atotal), PHPExcel_Cell_DataType::TYPE_STRING);




    $no2 = 1;
    $from2 = $_GET['tahun'].'/'.$_GET['bulan'].'/01';
    $from3 = date('Y-m-01', strtotime($from2));
    $to7 = $_GET['tahun'].'/'.$_GET['bulan'].'/01';
    $to4= date('Y-m-t', strtotime($to7));
    $to5 = date('m', strtotime($to7));
    $to6 = date('Y', strtotime($to7));
    $a1 = "exec dbo.ProsesGenerateLaporanNeraca '$to','$_SESSION[Name]'";
    //echo $a;
    $b1 = sqlsrv_query($conn, $a1);    

    $bln1 = 2 ;
    $bulan1 = date('m', strtotime($to4));
    //echo $bulan;
    $tahun1 = date('Y', strtotime($to4));
        if ($bulan1 != 01) {
            if ($bulan1 != 2 ) {
                if ($bulan1 != 3) {
                    if ($bulan1 != 4) {
                        if ($bulan1 !=5) {
                            if ($bulan1 !=6) {
                                if ($bulan1 !=7) {
                                    if ($bula1 !=8) {
                                        if ($bulan1 !=9) {
                                            if ($bulan !=10) {
                                                if ($bulan1 !=11) {
                                                    $bulan1 = 'Desember';
                                                } else {
                                                $bulan1 = 'Nopember';
                                                }
                                            } else {
                                            $bulan1 = 'OKtober';   
                                            }
                                        } else {
                                        $bulan1 = 'September';
                                        }
                                    } else {
                                    $bulan1 = 'Agustus';
                                    }
                                } else {
                                $bulan1 = 'Juli';
                                }
                            } else {
                            $bulan1 = 'Juni';
                            }
                        } else {
                        $bulan1 = 'Mei';  
                        }
                    } else {
                    $bulan1 = 'April';       
                    }
                } else {
                $bulan1 = 'Maret';
                }
            } else {
            $bulan1 = 'Februari';    
            }                               
        } else {
        $bulan1 = 'Januari';
        }

        $objWorkSheet->getStyle('E')->getFont()->setBold(true);
        $objWorkSheet->SetCellValue("E".$bln1,$bulan1 .'-'. $tahun1);   



        $H6 = 6;
           
        $from1 = $_GET['tahun'].'/'.$_GET['bulan'].'/01';
        $from = date('Y-m-01', strtotime($from1));
        $to3 = $_GET['tahun'].'/'.$_GET['bulan'].'/01';
        $to = date('Y-m-t', strtotime($to3));
        $to1 = date('m', strtotime($to3));
        $to2 = date('Y', strtotime($to3));
        $name = $_SESSION[Name];
        $x = "select* from dbo.GroupAcc where KodeGroup in('2.0.00.00.000','3.0.00.00.000')";
        $btotal = 0;
        $y = sqlsrv_query($conn, $x);
        while($z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC)){
            //var_dump($z);
             
        $objWorkSheet->SetCellValue("F".$H6,$z[0]);
        $objWorkSheet->SetCellValue("G".$H6,$z[1]);

        $H6++;

                $H7 = 6;
                $xx = "select * from dbo.TypeAcc where KodeGroup = '$z[0]' ORDER BY KodeTipe Asc";
                $abc = "select SUM (NilaiHide) from dbo.LaporanNeracaViewNew where GroupID = '$z[0]' and header =2 and Bulan = '$to1' and Tahun = '$to2' and UserName = '$name'";
                //echo $abc;
                $bcd = sqlsrv_query($conn, $abc);
                $b=sqlsrv_fetch_array($bcd, SQLSRV_FETCH_NUMERIC);
                $atotal1=$b[0];
                $total1= $total1+$atotal1;
                //echo $atotal1; 
                $yy = sqlsrv_query($conn, $xx);
                $total2=0;
                $total3=0;
                while($zz = sqlsrv_fetch_array($yy, SQLSRV_FETCH_NUMERIC)){
                    //var_dump($zz);    

                $H7=$H6++;
                $atotal1+=$b[0];

                $objWorkSheet->SetCellValue("F".$H7,$zz[0]);    
                $objWorkSheet->SetCellValue("G".$H7,$zz[1]);    


                    $h8 = 6 ;
                    $xxx = "select * from dbo.LaporanNeracaViewNew where TypeID = '$zz[0]' and header = 0 and Bulan = '$to1' and Tahun = '$to2' and UserName = '$name' ORDER BY KodeAccount Asc"; 
                    //echo $xxx;
                    $yyy = sqlsrv_query($conn, $xxx);
                    while($zzz = sqlsrv_fetch_array($yyy, SQLSRV_FETCH_NUMERIC)){
                        //var_dump($zzz);


                        $abc = "select SUM (NilaiHide) from dbo.LaporanNeracaViewNew where TypeID = '$zzz[2]' and header =2 and Bulan = '$to1' and Tahun = '$to2' and UserName = '$name' and KodeAccount like '%".substr($zzz[0],0,6)."%'";
                        //echo $abc;
                        $bcd = sqlsrv_query($conn, $abc);
                        $b=sqlsrv_fetch_array($bcd, SQLSRV_FETCH_NUMERIC);
                        $c=$b[0];

                    
                    $h8=$H6++;

                    $objWorkSheet->SetCellValue("F".$h8,$zzz[0]);    
                    $objWorkSheet->SetCellValue("G".$h8,$zzz[1]);  
                    $objWorkSheet->getStyle('H')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                    $objWorkSheet->SetCellValue("H".$h8, number_format($c), PHPExcel_Cell_DataType::TYPE_STRING);  


                        $h9 = 6 ;
                        $xxxx = "select * from dbo.LaporanNeracaViewNew where TypeID = '$zzz[2]' and header = 1 and UserName = '$name' and KodeAccount like '%".substr($zzz[0],0,6)."%' ORDER BY KodeAccount Asc";
                                                        
                        $yyyy = sqlsrv_query($conn, $xxxx);
                        while($zzzz = sqlsrv_fetch_array($yyyy, SQLSRV_FETCH_NUMERIC)){
                        $abc = "select SUM (NilaiHide) from dbo.LaporanNeracaViewNew where TypeID = '$zzzz[2]' and header =2 and Bulan = '$to1' and Tahun = '$to2' and UserName = '$name' and KodeAccount like '%".substr($zzzz[0],0,9)."%'";
                        //echo $abc;
                        $bcd = sqlsrv_query($conn, $abc);
                        $b=sqlsrv_fetch_array($bcd, SQLSRV_FETCH_NUMERIC);
                        $a=$b[0];


                        $h9=$H6++;

                        $objWorkSheet->SetCellValue("F".$h9,$zzzz[0]);    
                        $objWorkSheet->SetCellValue("G".$h9,$zzzz[1]);  
                        $objWorkSheet->SetCellValue("H".$h9, number_format($a), PHPExcel_Cell_DataType::TYPE_STRING);  


                            $h10 = 6;
                            $from1 = $_GET['tahun'].'/'.$_GET['bulan'].'/01';
                            $from = date('Y-m-01', strtotime($from1));
                            $to3 = $_GET['tahun'].'/'.$_GET['bulan'].'/01';
                            $to = date('Y-m-t', strtotime($to3));
                            $to1 = date('m', strtotime($to3));
                            $to2 = date('Y', strtotime($to3));
                            $name = $_SESSION[Name];
                            $xxxxx ="select * from dbo.LaporanNeracaViewNew where TypeID = '$zzzz[2]' and header =2 and Bulan = '$to1' and Tahun = '$to2' and UserName = '$name' and KodeAccount like '%".substr($zzzz[0],0,9)."%' ORDER BY KodeAccount Asc";
                            //echo $xxxxx;
                            $yyyyy = sqlsrv_query($conn, $xxxxx);
                            while($zzzzz = sqlsrv_fetch_array($yyyyy, SQLSRV_FETCH_NUMERIC)){  


                            $h10=$H6++;

                            $objWorkSheet->SetCellValue("F".$h10,$zzzzz[0]);    
                            $objWorkSheet->SetCellValue("G".$h10,$zzzzz[1]);  
                            $objWorkSheet->SetCellValue("H".$h10, number_format($zzzzz[7]), PHPExcel_Cell_DataType::TYPE_STRING);  

                        }
                    }
                }

            }

            
          }
            
            // memberi border dari A1 sd C5
            //$objWorkSheet->getSheet(0)->getStyle('F',$H1)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

            $objWorkSheet->getStyle('F5:H' .$H1)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objWorkSheet->getStyle('F')->getFont()->setBold(true);
            $objWorkSheet->SetCellValue('F'.$H1,'Saldo Akhir');
            $objWorkSheet->getStyle('H')->getFont()->setBold(true);
            $objWorkSheet->SetCellValue("H".$H1, number_format($total1), PHPExcel_Cell_DataType::TYPE_STRING);
    }




//exit;
    $objWorkSheet->setTitle('Template COA');

    $fileName = 'TemplateCOA'.'_'.strtotime(date('Y-m-d H:i:s')).'.xls';
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');

// download ke client
    header('Content-type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename="'.$fileName.'"');
    $objWriter->save('php://output');

    return $filePath.'/'.$fileName;

?>
