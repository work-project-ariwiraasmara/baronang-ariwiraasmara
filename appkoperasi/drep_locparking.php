<?php
session_start();
error_reporting(0);

include('lib/phpExcel/Classes/PHPExcel.php');
include "connect.php";


    $filePath = "uploads/excel/";

    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getProperties()->setCreator("baronang");
    $objPHPExcel->getProperties()->setTitle("Drep Lokasi Parkir");

// set autowidth
    for($col = 'A'; $col !== 'Z'; $col++) {
        $objPHPExcel->getActiveSheet()
            ->getColumnDimension($col)
            ->setAutoSize(true);
    }

    $listCombo = array();

    //sheet 2
    $objWorkSheet = $objPHPExcel->createSheet(0);
// baris judul
   
    $objWorkSheet->getStyle('C1')->getFont()->setBold(true);
    $objWorkSheet->getStyle('C1')->getFont()->setSize(14);
    $objWorkSheet->SetCellValue('C1', 'Laporan Parkir');

    $objWorkSheet->getStyle('C2')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('C2', 'Periode :');

    $objWorkSheet->getStyle('A4')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('A4', 'Lokasi : ');

    


    $objWorkSheet->getStyle('A6')->getFont()->setBold(true);
    $objWorkSheet->getStyle('A6' )->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('A6', 'No');
    $objWorkSheet->getStyle('B6')->getFont()->setBold(true);
    $objWorkSheet->getStyle('B6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('B6', 'Tanggal');
    $objWorkSheet->getStyle('C6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->getStyle('C6')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('C6', 'No. Kartu');
    $objWorkSheet->getStyle('D6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->getStyle('D6')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('D6', 'Status Member');
    $objWorkSheet->getStyle('E6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->getStyle('E6')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('E6', 'Waktu Masuk');
    $objWorkSheet->getStyle('F6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->getStyle('F6')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('F6', 'Waktu Keluar');
    $objWorkSheet->getStyle('G6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->getStyle('G6')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('G6', 'Jenis Kendaraan');   
    $objWorkSheet->getStyle('H6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->getStyle('H6')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('H6', 'Point Bayar');   
 


    if($_GET['acc']){

    $akun = $_GET['acc'];
    $tanggaldari = $_GET['from'];
    $tanggalsampai = $_GET['to'];
    $tglh = date('Y/m/d', strtotime('+1 days', strtotime($tanggalsampai)));

    $no = 1;
    $bln = 2 ;
    $Tglpertama = date('d', strtotime($_GET['from']));
    $bulan = date('m', strtotime($_GET['from']));
    $tahun = date('Y', strtotime($_GET['from']));
        if ($bulan != 1) {
            if ($bulan != 2 ) {
                if ($bulan != 3) {
                    if ($bulan != 4) {
                        if ($bulan !=5) {
                            if ($bulan !=6) {
                                if ($bulan !=7) {
                                    if ($bulan !=8) {
                                        if ($bulan !=9) {
                                            if ($bulan !=10) {
                                                if ($bulan !=11) {
                                                    $bulan = 'Desember';
                                                } else {
                                                $bulan = 'Nopember';
                                                }
                                            } else {
                                            $bulan = 'OKtober';   
                                            }
                                        } else {
                                        $bulan = 'September';
                                        }
                                    } else {
                                    $bulan = 'Agustus';
                                    }
                                } else {
                                $bulan = 'Juli';
                                }
                            } else {
                            $bulan = 'Juni';
                            }
                        } else {
                        $bulan = 'Mei';  
                        }
                    } else {
                    $bulan = 'April';       
                    }
                } else {
                $bulan = 'Maret';
                }
            } else {
            $bulan = 'Februari';    
            }                               
        } else {
        $bulan = 'Januari';
        }

    $tgl2 = date('d', strtotime($_GET['to']));
    $bulan2 = date('m', strtotime($_GET['to']));
    $tahun2 = date('Y', strtotime($_GET['to']));
        if ($bulan2 != 1) {
            if ($bulan2 != 2 ) {
                if ($bulan2 != 3) {
                    if ($bulan2 != 4) {
                        if ($bulan2 !=5) {
                            if ($bulan2 !=6) {
                                if ($bulan2 !=7) {
                                    if ($bulan2 !=8) {
                                        if ($bulan2 !=9) {
                                            if ($bulan2 !=10) {
                                                if ($bulan2 !=11) {
                                                    $bulan2 = 'Desember';
                                                } else {
                                                $bulan2 = 'Nopember';
                                                }
                                            } else {
                                            $bulan2 = 'OKtober';   
                                            }
                                        } else {
                                        $bulan2 = 'September';
                                        }
                                    } else {
                                    $bulan2 = 'Agustus';
                                    }
                                } else {
                                $bulan2 = 'Juli';
                                }
                            } else {
                            $bulan2 = 'Juni';
                            }
                        } else {
                        $bulan2 = 'Mei';  
                        }
                    } else {
                    $bulan2 = 'April';       
                    }
                } else {
                $bulan2 = 'Maret';
                }
            } else {
            $bulan2 = 'Februari';    
            }                               
        } else {
        $bulan2 = 'Januari';
        }

        $objWorkSheet->getStyle('D'.$bln)->getFont()->setBold(true);
        $objWorkSheet->SetCellValue("D".$bln,$Tglpertama     .' '.$bulan .' '. $tahun .' - '. $tgl2 .' '. $bulan2 .' '. $tahun2);   


    $k = 4;
    $x = "select * from dbo.LocationMerchant where status = 1 and LocationID = '$akun' order by LocationID asc";
    //echo $x;
    $y = sqlsrv_query($conn, $x);
    while($z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC)){
           
        $objWorkSheet->getStyle('B'.$k)->getFont()->setBold(true);
        $objWorkSheet->SetCellValue("B".$k,$z[0] .' - '. $z[2]);           

    }

    

    
    $row = 7;
    $rw = 7;
    
    $xy = "SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY TimeIn asc) as row FROM [dbo].[ParkingList] a where LocationIn = '$akun' and status = '1' and TimeIn between '$tanggaldari' and '$tglh') a";
    //echo $x;
    $yz = sqlsrv_query($conn, $xy);
    
    while($za = sqlsrv_fetch_array( $yz, SQLSRV_FETCH_NUMERIC)){
         if($za[11] == 1){
            $member='Member';
            } else {
            $member='Non Member';
            }


        $jenisk = "select * from dbo.VehicleType where Code = '$za[10]'";
        $prosesk = sqlsrv_query($conn, $jenisk);
        $hasilk = sqlsrv_fetch_array($prosesk, SQLSRV_FETCH_NUMERIC);
               
        $objWorkSheet->SetCellValueExplicit("A".$row,$za[12]);
        $objWorkSheet->SetCellValueExplicit("B".$row,$za[3]->format('Y-m-d H:i:s'));
        $objWorkSheet->SetCellValueExplicit("C".$row,$za[2]);
        $objWorkSheet->SetCellValueExplicit("D".$row,$member);
        $objWorkSheet->SetCellValueExplicit("E".$row,$za[3]->format('Y-m-d H:i:s'));
        $objWorkSheet->SetCellValueExplicit("F".$row,$za[4]->format('Y-m-d H:i:s'));
        $objWorkSheet->SetCellValueExplicit("G".$row,$hasilk[1]);
        $objWorkSheet->SetCellValueExplicit("H".$row,$za[8]);
        $row++;
        $rw++;
        }
 }
//exit;
    $objWorkSheet->setTitle('Drep Laporan Parkir');

    $fileName = 'LapPar'.'_'.strtotime(date('Y-m-d H:i:s')).'.xls';
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');

// download ke client
    header('Content-type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename="'.$fileName.'"');
    $objWriter->save('php://output');

    return $filePath.'/'.$fileName;

?>
