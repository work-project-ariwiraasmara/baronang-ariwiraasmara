<?php require('header.php');?>

<?php require('sidebar-right.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>


<div class="animated fadeinup delay-1">
    <div class="page-content">
        <h2 class="uppercase"><?php echo lang('Buka Sesi'); ?></h2>

        <?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
            <div class="alert alert-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-warning ?>"></i> <?php echo ucfirst($_SESSION['error-type']); ?></h4>
                <?php echo $_SESSION['error-message']; ?>
            </div>
        <?php } ?>

        <h4 class="uppercase" style="margin-top: 20px; text-align: center;"><?php echo lang('Konfirmasi Saldo Awal'); ?></h4>
            <form class="form-horizontal" action="procbuka_sesi.php" method = "POST">
                <?php
                $balancekasir = 0;
                $tanggal = date('Y-m-d');
                //kasir
                $q = "SELECT a.AssignDate, b.UserID, b.Amount FROM dbo.CashierAssign a inner join dbo.CashierAssignUser b on a.IDCashierAssign = b.IDCashierAssign where a.Type = 1 and b.Status = 0 and b.UserID='$_SESSION[UserID]' and a.AssignDate='$tanggal'";
                $w = sqlsrv_query($conn, $q);
                $e = sqlsrv_fetch_array($w, SQLSRV_FETCH_NUMERIC);
                if($e != null){
                    $balancekasir = $e[2];
                }

                $balanceteller = 0;
                $tanggal = date('Y-m-d');
                //teller
                $qq = "SELECT a.AssignDate, b.UserID, b.Amount FROM dbo.CashierAssign a inner join dbo.CashierAssignUser b on a.IDCashierAssign = b.IDCashierAssign where a.Type = 0 and b.Status = 0 and b.UserID='$_SESSION[UserID]' and a.AssignDate='$tanggal'";
                $ww = sqlsrv_query($conn, $qq);
                $ee = sqlsrv_fetch_array($ww, SQLSRV_FETCH_NUMERIC);
                if($ee != null){
                    $balanceteller = $ee[2];
                }

                ?>
                <div>
                    <label><?php echo lang('Tanggal'); ?></label>
                    <input type="text" name="tgl" class="validate" id="tgl" value="<?php echo date('d F Y');?>" readonly>
                </div>

                <?php if($e != null){ ?>
                <p>
                    <h3><?php echo ('Saldo Awal Kasir Anda Adalah (Rp '.number_format($balancekasir).')'); ?></h3>
                </p>
                <br>
              <?php } ?>

              <?php if($ee != null){ ?>
              <p>
                  <h3><?php echo ('Saldo Awal Teller Anda Adalah (Rp '.number_format($balanceteller).')'); ?></h3>
              </p>
              <br>
            <?php } ?>

                <button type="submit" class="waves-effect waves-light btn-large primary-color width-100" id="btn_sesi-confirm"> <?php echo lang('Setuju'); ?></button>

            </form>
    </div>
</div>


<?php require('footer-new.php'); ?>
