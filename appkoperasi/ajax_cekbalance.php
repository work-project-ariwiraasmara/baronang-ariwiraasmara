<?php
include 'connect.php';

if(isset($_POST['mid'])){
    //cek member
    $a = "select * from [dbo].[MemberList] where MemberID='$_POST[mid]' and StatusMember=1";
    $b = sqlsrv_query($conn, $a);
    $c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC);
    ?>
    <?php if($c != null){ ?>
        <h3>Time Deposit Account</h3>
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>Account Number</th>
                <th>Balance</th>
                <th>Interest Rate</th>
                <th>Interest Penalty</th>
                <th>Fixed Penalty</th>
                <th>Total Interest Transfered</th>
                <th>Total</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $aa = "select * from [dbo].[HitungDeposito] where MemberID='$_POST[mid]' and AccountNumber not in (select AccountNumber from [dbo].[TimeDepositClose])";
            $bb = sqlsrv_query($conn, $aa);
            while($cc = sqlsrv_fetch_array($bb, SQLSRV_FETCH_NUMERIC)){
            ?>
            <tr>
                <td><?php echo $cc[1]; ?></td>
                <td><?php echo number_format($cc[10]); ?></td>
                <td><?php echo $cc[4]; ?></td>
                <td><?php echo $cc[5]; ?></td>
                <td><?php echo number_format($cc[3]); ?></td>
                <td><?php echo number_format($cc[12]); ?></td>
                <td><?php echo number_format($cc[16]); ?></td>
            </tr>
            <?php } ?>
            </tbody>
        </table>

        <h3>Regular Saving Account</h3>
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>Account Number</th>
                <th>Balance</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $aaa = "select * from [dbo].[RegularSavingAcc] where MemberID='$_POST[mid]' and AccountNo not in (select AccNo from [dbo].[RegularSavingClose])";
            $bbb = sqlsrv_query($conn, $aaa);
            while($ccc = sqlsrv_fetch_array($bbb, SQLSRV_FETCH_NUMERIC)){
            ?>
            <tr>
                <td><?php echo $ccc[2]; ?></td>
                <td><?php echo number_format($ccc[4]); ?></td>
            </tr>
            <?php } ?>
            </tbody>
        </table>

        <h3>Basic Saving Account</h3>
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>Account Number</th>
                <th>Billing Balance</th>
                <th>Payment Balance</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $aaaa = "select * from [dbo].[BasicSavingBalance] where MemberID='$_POST[mid]' and AccNo not in (select AccNo from [dbo].[BasicSavingCloseTransaction])";
            $bbbb = sqlsrv_query($conn, $aaaa);
            while($cccc = sqlsrv_fetch_array($bbbb, SQLSRV_FETCH_NUMERIC)){
                ?>
                <tr>
                    <td><?php echo $cccc[3]; ?></td>
                    <td><?php echo number_format($cccc[4]); ?></td>
                    <td><?php echo number_format($cccc[5]); ?></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    <?php } else { ?>
        <div class="alert alert-warning alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-warning ?>"></i> Warning</h4>
            Member ID not found
        </div>
    <?php } ?>
<?php
} else{
    echo '<h3>Data tidak ditemukan</h3>';
}
?>