<?php require('header.php');?>

<?php require('sidebar-right.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>


<div class="animated fadeinup delay-1">
    <div class="page-content">
        <h2 class="uppercase"><?php echo lang('Tutup Sesi'); ?></h2>
        <h4 class="uppercase" style="margin-top: 20px; text-align: center;"><?php echo lang('Konfirmasi Tutup Sesi'); ?></h4>
        <?php if(isset($_SESSION['sesikasir'])){ ?>
          <div class="c-widget">
              <div class="c-widget-figure primary-color">
                  <i class="ion-android-mail"></i>
              </div>
              <div class="c-widget-body">
                  <p class="m-0">Sesi Kasir Aktif</p>
                  <p class="small m-0"><?php echo $_SESSION['sesikasir']; ?></p>
              </div>
          </div>
        <?php } ?>

            <form class="form-horizontal" action="proctutup_sesi_kasir.php" method = "POST">
                <?php
                $id = '';
                $balance = 0;
                $debet = 0;
                $kredit = 0;
                $last = 0;
                $tanggal = '';
                $tgl = date('Y-m-d');
                $xx = "select top 1 TransNo, Tanggal, BeginingBalance, Debet, Kredit from [dbo].[CashierSession] where UserID = '$_SESSION[UserID]' and Type = 1 and Status = 0 and Tanggal <= '$tgl' order by TimeIn ASC";
                $yy = sqlsrv_query($conn, $xx);
                $zz = sqlsrv_fetch_array($yy, SQLSRV_FETCH_NUMERIC);
                if($zz != null){
                    $id = $zz[0];
                    $balance = $zz[2];
                    $debet = $zz[3];
                    $kredit = $zz[4];
                    $last = ($balance+$debet) - $kredit;
                    $tanggal = $zz[1]->format('Y-m-d');
                }
                ?>
                <div>
                    <label><?php echo lang('Tanggal'); ?></label>
                    <input type="text" class="validate" id="tgl" value="<?php echo $tanggal; ?>" readonly>
                </div>

                <div class="input-field">
                    <h3><?php echo 'Saldo Awal Anda Adalah (Rp. '.number_format($balance).')'; ?></h3>
                </div>
                <div class="input-field">
                    <h3><?php echo 'Jumlah Penerimaan (Rp. '.number_format($debet).')'; ?></h3>
                </div>
                <div class="input-field">
                    <h3><?php echo 'Jumlah Pengeluaran (Rp. '.number_format($kredit).')'; ?></h3>
                </div>
                <div class="input-field">
                    <h3><?php echo 'Sisa (Rp. '.number_format($last).')'; ?></h3>
                </div>

                <div class="input-field">
                    <label class="active"><?php echo ('Jumlah Tutup Sesi (Rp)'); ?></label>
                    <input type="number" name="endbalance" class="validate">
                </div>

                <div class="row m-l-0">
                    <div class="col">
                            <button type="submit" class="waves-effect waves-light btn-large primary-color width-100 m-b-20 animated bouncein delay-4" id="btn_sesi-confirm"> <?php echo lang('Setuju'); ?></button>
                        </div>
                        <!-- <div class="col">
                            <button type="button" class="waves-effect waves-light btn-large primary-color width-100 m-b-20 animated bouncein delay-4" id="cancel"> <?php echo lang('Batal'); ?></button>
                        </div> -->
                    </div>

            </form>
    </div>
</div>


<?php require('footer-new.php'); ?>
