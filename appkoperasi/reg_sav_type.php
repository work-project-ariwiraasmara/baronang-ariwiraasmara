<?php require('header.php');?>

<?php require('sidebar-right.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<?php
//edit
$uledit = $_GET['edit'];
$ul0    = "";
$ul1    = "";
$ul2    = "";
$ul3    = "";
$ul4    = "";
$ul5    = "";
$ul6    = "";
$ul7    = "";
$ul8    = "";
$ul9    = "";
$ulprocedit = "";
$uldisabled = "";
$uldisabled2 = "";
if(!empty($uledit)){
    $eulsql   = "select * from dbo.RegularSavingType where RegularSavingType='$uledit'";
    $eulstmt  = sqlsrv_query($conn, $eulsql);
    $eulrow   = sqlsrv_fetch_array( $eulstmt, SQLSRV_FETCH_NUMERIC);
    if(count($eulrow[0]) > 0){
        $ul0    = $eulrow[0];
        $ul1    = $eulrow[1];
        $ul2    = $eulrow[2];
        $ul3    = $eulrow[3];
        $ul4    = substr($eulrow[4], 0, -5);
        $ul5    = substr($eulrow[5],0,-5);
        $ul6    = substr($eulrow[6], 0, -5);
        $ul7    = $eulrow[7];
        $ul8    = $eulrow[8];
        $ul9    = substr($eulrow[9],0, -5);
        $ulprocedit = "?page=".$_GET['page']."&edit=".$ul0;
        $uldisabled = "disabled";
        $uldisabled2 = "readonly";
    }
    else{
        if(empty($_GET['page'])){
            echo "<script language='javascript'>document.location='reg_sav_type.php';</script>";
        }
        else{
            echo "<script language='javascript'>document.location='reg_sav_type.php?page=".$_GET['page']."';</script>";
        }
    }
}

?>

<div class="animated fadeinup delay-1">
    <div class="page-content">

        <?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
            <div class="notification notification-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
                <!--<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> <i class="icon fa fa-warning ?>"></i> -->
                <h4> <?php echo ucfirst($_SESSION['error-type']); ?> </h4>
                <?php echo $_SESSION['error-message']; ?>
            </div>
        <?php } ?>

        <h2 class="uppercase"><?php echo lang('Tipe tabungan'); ?></h2>

            <form class="form-horizontal" action="procreg_sav_type.php<?=$ulprocedit?>" method = "POST">

                    <?php
                    $sql   = "select * from dbo.RegularSavingType";
                    $stmt  = sqlsrv_query($conn, $sql);
                    $row   = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_NUMERIC);
                    if($row == null){ ?>

                        <input type="hidden" name="wiz" value="<?php echo $step; ?>" readonly>
                        <h4><?php echo lang('Info'); ?>!</h4>

                        <p>
                            <b><?php echo lang('Anda harus membuat satu tipe tabungan'); ?></b>(<?php echo lang('Satu kali input'); ?>)<br>
                            <?php echo lang('Pilih kba'); ?>
                        </p>

                        <br>
                        <label></label><?php echo lang('Kba'); ?></label>
                        <select name="kba" class="browser-default" <?=$uldisabled2?>>
                            <option value="">- <?php echo lang('Pilih salah satu'); ?> -</option>
                            <?php
                            $julsql   = "select * from [dbo].[LaporanNeracaViewNew] where header = '2'";
                            $julstmt = sqlsrv_query($conn, $julsql);
                            while($rjulrow  = sqlsrv_fetch_array( $julstmt, SQLSRV_FETCH_NUMERIC)){
                                ?>
                                <option value="<?=$rjulrow[1];?>" <?php if($rjulrow[1]==$ul5){echo "selected";} ?>>&nbsp;<?=$rjulrow[4];?>&nbsp;<?=$rjulrow[3];?></option>
                            <?php } ?>
                        </select>

                        <div style="margin-top: 30px;">
                            <button type="submit" class="waves-effect waves-light btn-large primary-color width-100"><?php echo lang('Simpan'); ?></button>
                        </div>

                    <?php }
                    else { ?>

                        <div class="input-field">
                            <input type="text" name="reg" class="validate" id="regularsavingdescription_reg" value="<?=$ul1?>">
                            <label for="regularsavingdescription_reg"><?php echo lang('Deskripsi'); ?></label>
                        </div>

                        <br><?php echo lang('Tipe bunga'); ?>
                        <?php
                        $a = '';
                        $b = '';
                        $c = '';
                        if($ul2 == 0){
                            $a = 'selected';
                        }
                        else if($ul2 == 1){
                            $b = 'selected';
                        }
                        else{
                            $c = 'selected';
                        }
                        ?>
                        <select name="intt" class="validate browser-default">
                            <option>- <?php echo lang('Pilih salah satu'); ?> -</option>
                            <option value="0" <?php echo $a; ?>><?php echo lang('Saldo harian'); ?></option>
                            <option value="1" <?php echo $b; ?>><?php echo lang('Saldo rata-rata'); ?></option>
                            <option value="2" <?php echo $c; ?>><?php echo lang('Saldo terendah'); ?></option>
                        </select>

                        <div class="input-field">
                            <input type="text" name="min" class="validate price" id="regularsavingdescription_min" value="<?=$ul4?>">
                            <label for="regularsavingdescription_min"><?php echo lang('Minimum saldo'); ?></label>
                        </div>

                        <div class="input-field">
                            <input type="text" name="max" class="validate price" id="regularsavingdescription_max" value="<?=$ul5?>">
                            <label for="regularsavingdescription_max"><?php echo lang('Maximum saldo'); ?></label>
                        </div>

                        <div class="input-field">
                            <input type="text" name="adm" class="validate price" id="regularsavingdescription_adm" value="<?=$ul6?>">
                            <label for="regularsavingdescription_adm"><?php echo lang('Biaya admin'); ?></label>
                        </div>

                        <div class="input-field">
                            <input type="text" name="clos" class="validate price" id="regularsavingdescription_clos" value="<?=$ul9?>">
                            <label for="regularsavingdescription_clos"><?php echo lang('Biaya penutupan'); ?></label>
                        </div>

                        <label><?php echo lang('Kba'); ?></label>
                        <select name="kba" class="browser-default" <?=$uldisabled2?>>
                            <option value="">- <?php echo lang('Pilih salah satu'); ?> -</option>
                            <?php
                            $julsql   = "select * from [dbo].[BankAccountView]";
                            $julstmt = sqlsrv_query($conn, $julsql);
                            while($rjulrow  = sqlsrv_fetch_array( $julstmt, SQLSRV_FETCH_NUMERIC)){
                                ?>
                                <option value="<?=$rjulrow[1];?>" <?php if($rjulrow[1]==$ul5){echo "selected";} ?>>&nbsp;<?=$rjulrow[4];?>&nbsp;<?=$rjulrow[3];?></option>
                            <?php } ?>
                        </select>

                        <br>
                        <b><?php echo lang('Skema bunga'); ?></b>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped" id="tskema">
                                <thead>
                                <tr>
                                    <th><?php echo lang('No'); ?></th>
                                    <th><?php echo lang('Saldo awal'); ?></th>
                                    <th><?php echo lang('Saldo akhir'); ?></th>
                                    <th><?php echo lang('Persentase'); ?></th>
                                    <th></th>
                                </tr>
                                <?php
                                $jum = 0;
                                $max = 0;
                                $qwe = "select * from [dbo].[SkemaBunga] where RegularSavingType = '$uledit' order by Urutan asc";
                                $asd = sqlsrv_query($conn, $qwe);
                                while($zxc = sqlsrv_fetch_array($asd, SQLSRV_FETCH_NUMERIC)){
                                    $jum+=1;

                                    ?>
                                    <tr>
                                        <td><?php echo $zxc[1]; ?></td>
                                        <td><?php echo number_format($zxc[2]); ?></td>
                                        <td><?php echo number_format($zxc[3]); ?></td>
                                        <td><?php echo $zxc[4]; ?></td>
                                        <td>
                                            <a href="procreg_sav_type.php?del=<?php echo $zxc[0]; ?>&u=<?php echo $zxc[1]; ?>"><button type="button" class="btn btn-sm btn-danger btn-delete"><i class="ion-android-close"></i> </button></a>
                                        </td>
                                    </tr>
                                    <?php
                                    $max = $zxc[3]+1;
                                }
                                ?>
                                <input type="hidden" id="last" value="<?php echo $jum; ?>" readonly>
                                </thead>
                                <tbody>

                                </tbody>
                                <tfoot>
                                <tr>
                                    <td></td>
                                    <td><input type="text" id="start" class="validate price" value="<?php echo $max; ?>" readonly></td>
                                    <td><input type="text" id="end" class="validate price" value="0"></td>
                                    <td><input type="text" id="percentage" class="validate" value="0"></td>
                                    <td><button type="button" class="btn btn-sm btn-success btn-add"><?php echo lang('Tambah'); ?></button></td>
                                </tr>
                                </tfoot>
                            </table>
                        </div>

                        <div style="margin-top: 30px;">
                            <?php if(count($eulrow[0]) > 0){ ?>
                                <button type="submit" class="waves-effect waves-light btn-large primary-color width-100"><?php echo lang('Perbaharui'); ?></button>
                            <?php } else { ?>
                                <button type="submit" class="waves-effect waves-light btn-large primary-color width-100"><?php echo lang('Simpan'); ?></button>
                            <?php } ?>
                        </div>

                        <div style="margin-top: 10px; margin-bottom: 30px;">
                            <?php if(count($eulrow[0]) > 0){ ?>
                                <a href="reg_sav_type.php" class="waves-effect waves-light btn-large primary-color width-100"><?php echo lang('Batal'); ?></a>
                            <?php } ?>
                        </div>

                        <?php
                    } ?>
            </form>

        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title"><?php echo lang('Daftar tipe tabungan'); ?></h3>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th><?php echo lang('No'); ?></th>
                            <th><?php echo lang('Kode tipe tabungan'); ?></th>
                            <th><?php echo lang('Deskripsi'); ?></th>
                            <th><?php echo lang('Tipe bunga'); ?></th>
                            <th><?php echo lang('Minimum saldo'); ?></th>
                            <th><?php echo lang('Maximum saldo'); ?></th>
                            <th><?php echo lang('Biaya admin'); ?></th>
                            <th><?php echo lang('Kba'); ?></th>
                            <th><?php echo lang('Biaya penutupan'); ?></th>
                            <th><?php echo lang('Aksi'); ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        //count
                        $jmlulsql   = "select count(*) from dbo.RegularSavingTypeView";
                        $jmlulstmt  = sqlsrv_query($conn, $jmlulsql);
                        $jmlulrow   = sqlsrv_fetch_array( $jmlulstmt, SQLSRV_FETCH_NUMERIC);

                        //pagging
                        $perpages   = 10;
                        $halaman    = $_GET['page'];
                        if(empty($halaman)){
                            $posisi  = 0;
                            $batas   = $perpages;
                            $halaman = 1;
                        }
                        else{
                            $posisi  = (($perpages * $halaman) - 10) + 1;
                            $batas   = ($perpages * $halaman);
                        }

                        $ulsql = "SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY RegularSavingType asc) as row FROM [dbo].[RegularSavingTypeView]) a WHERE row between '$posisi' and '$batas'";
                        $ulstmt = sqlsrv_query($conn, $ulsql);
                        while($ulrow = sqlsrv_fetch_array( $ulstmt, SQLSRV_FETCH_NUMERIC)){
                            ?>
                            <tr>
                                <td><?=$ulrow[14];?></td>
                                <td><?=$ulrow[0];?>
                                <td><?=$ulrow[1];?></td>
                                <td>
                                    <?php
                                    $saldo = '';
                                    if($ulrow[2] == 0){
                                        $saldo = lang('Saldo Harian');
                                    }
                                    else if($ulrow[2] == 1){
                                        $saldo = lang('Saldo Rata-rata');
                                    }
                                    else{
                                        $saldo = lang('Saldo Terendah');
                                    }

                                    echo $saldo;
                                    ?>
                                </td>
                                <td><?=$ulrow[5];?></td>
                                <td><?=$ulrow[7];?></td>
                                <td><?=$ulrow[9];?></td>
                                <td><?=$ulrow[10];?></td>
                                <td><?=$ulrow[13];?></td>
                                <td>
                                    <a href="reg_sav_type.php?page=<?=$halaman?>&edit=<?=$ulrow[0]?>" class="btn btn-default btn-flat btn-sm text-green" title="edit"><i class="ion-android-create"></i></a>
                                </td>
                            </tr>
                            <?php
                        }
                        $jmlhalaman = ceil($jmlulrow[0]/$perpages);
                        ?>
                        </tbody>
                    </table>
                </div>
                <div class="box-footer clearfix right">
                    <div style="text-align: center;">
                        <?php echo lang('Menampilkan'); ?> <?=$halaman?> to <?=$jmlhalaman?> of <?=$jmlulrow[0]?> <?php echo lang('Masukan'); ?> <?=$posisi." - ".$batas?>
                        <?php
                        $reload = "reg_sav_type.php?";
                        $page = intval($_GET["page"]);
                        $adjacents = isset($_GET['adjacents']) ? intval($_GET['adjacents']) : 2; // khusus style pagination 2 dan 3
                        if( $page == 0 ) $page = 1;

                        $tpages = ($jmlhalaman) ? ceil($jmlhalaman) : 0; // total pages, last page number
                        echo "<div>".paginate($reload, $page, $tpages)."</div>";
                        ?>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>



<script type="text/javascript">
    var tampung = [];

    $('.btn-delete').click(function(){
        if(confirm('<?php echo lang('Urutan dibawah kolom ini akan dihapus juga. Apakah anda yakin'); ?> ?') == false){
            return false;
        }
    });

    $('.btn-add').click(function(){
        var start = $('#start').val();
        var end = $('#end').val();
        var percent = $('#percentage').val();
        var last = $('#last').val();

        if(start == '' || end == '' || percent == ''){
            alert('<?php echo lang('Harap isi inputan dengan benar'); ?>');
            return false;
        }
        else if(jQuery.inArray(start, tampung) !== -1 || jQuery.inArray(end, tampung) !== -1){
            alert('<?php echo lang('Jumlah saldo harus berbeda dengan yang sudah ditambahkan sebelumnya'); ?>');
            return false;
        }
        else if(parseInt(start) > parseInt(end)){
            alert('<?php echo lang('Saldo akhir harus lebih besar'); ?>');
            return false;
        }
        else{
            $('#start').val(parseInt(end)+1);

            $.ajax({
                url : "ajax_addskema.php",
                type : 'POST',
                dataType : 'json',
                data: { start: start, end: end, percent: percent},   // data POST yang akan dikirim
                success : function(data) {
                    tampung.push(data.start);
                    tampung.push(data.end);

                    var urutan = parseInt(last)+1;

                    $('#last').val(urutan);

                    $("#tskema tbody").append("<tr>" +
                        "<td><input type='hidden' name='urutan[]' class='urutan' value="+ urutan +">" + urutan + "</td>" +
                        "<td><input type='hidden' name='start[]' value="+ data.start +">" + data.start + "</td>" +
                        "<td><input type='hidden' name='end[]' value="+ data.end +">" + data.end + "</td>" +
                        "<td><input type='hidden' name='percentage[]' value="+ data.percent +">" + data.percent + "</td>" +
                        "<td><a class='btn btn-default btn-flat btn-sm text-red remove' onclick='$(this).parent().parent().remove();'><i class='fa fa-trash-o'></i></a></td>" +
                        "</tr>");
                    $('#end').val('0');
                    $('#percentage').val('0');
                },
                error : function() {
                    alert('<?php echo lang('Telah terjadi error'); ?>');
                    return false;
                }
            });
        }
    });
</script>

<?php
function paginate($reload, $page, $tpages) {

    $firstlabel = "First";
    $prevlabel  = "Prev";
    $nextlabel  = "Next";
    $lastlabel  = "Last";

    $out = "<div class=\"backpaging\"><ul class=\"ownpaging\">\n";

    // first
    if($page>1) {
        $out.= "<li><a href=\"" . $reload . "\">" . $firstlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $firstlabel . "</a></li>\n";
    }

    // previous
    if($page==1) {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $prevlabel . "</a></li>\n";
    }
    elseif($page==2) {
        $out.= "<li><a href=\"" . $reload . "\">" . $prevlabel . "</a></li>\n";
    }
    else {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" . ($page-1) . "\">" . $prevlabel . "</a></li>\n";
    }

    // current
    $out.= "<li class=\"active\"><a href=\"#\">Page " . $page . " of " . $tpages . "</a></li>\n";

    // next
    if($page<$tpages) {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" .($page+1) . "\">" . $nextlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $nextlabel . "</a></li>\n";
    }

    // last
    if($page<$tpages) {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" . $tpages . "\">" . $lastlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $lastlabel . "</a></li>\n";
    }

    $out.= "</ul></div>";

    return $out;
}

require('footer_new.php');
?>
