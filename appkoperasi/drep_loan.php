<?php
session_start();
error_reporting(0);

include('lib/phpExcel/Classes/PHPExcel.php');
include "connect.php";


    $filePath = "uploads/excel/";

    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getProperties()->setCreator("baronang");
    $objPHPExcel->getProperties()->setTitle("Daftar Pinjaman");

// set autowidth
    for($col = 'A'; $col !== 'Z'; $col++) {
        $objPHPExcel->getActiveSheet()
            ->getColumnDimension($col)
            ->setAutoSize(true);
    }

    $listCombo = array();

    //sheet 2
    $objWorkSheet = $objPHPExcel->createSheet(0);
// baris judul
    $objWorkSheet->SetCellValue('A1', 'No');
    $objWorkSheet->SetCellValue('B1', 'No Pinjaman');
    $objWorkSheet->SetCellValue('C1', 'Jumlah');
    $objWorkSheet->SetCellValue('D1', 'Tanggal Buka');
    $objWorkSheet->SetCellValue('E1', 'Tanggal Tutup');
    $objWorkSheet->SetCellValue('F1', 'Produk');
    $objWorkSheet->SetCellValue('G1', 'Saldo');
    $objWorkSheet->SetCellValue('H1', 'Status');



    $no = 1;
    $row = 2;
    $x =  "select * from dbo.LoanReportBalance ORDER BY LoanNUmber Asc";

    $y = sqlsrv_query($conn, $x);
    while($z = sqlsrv_fetch_array( $y, SQLSRV_FETCH_NUMERIC)){
        //var_dump($z).die;
         //push untuk drop down list
        array_push($listCombo, $z[1]);

        $open = $z[2]->format('Y-m-d H:i:s');
                    $close = '';
                    if($z[3] != ''){
                        $close = $z[3];
                    }

        $objWorkSheet->SetCellValue("A".$row, $no);
        $objWorkSheet->SetCellValueExplicit("B".$row, $z[0], PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet->SetCellValueExplicit("C".$row, $z[1], PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet->SetCellValue("D".$row, $open);
        $objWorkSheet->SetCellValue("E".$row, $close);
        $objWorkSheet->SetCellValueExplicit("F".$row, $z[4]);
        $objWorkSheet->SetCellValueExplicit("G".$row, $z[5], PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet->SetCellValueExplicit("H".$row, $z[6]);

        
        $no++;
        $row++;
    }

    $objWorkSheet->setTitle('Daftar Pinjaman');

    $fileName = 'Daftarpinjaman'.'_'.strtotime(date('Y-m-d H:i:s')).'.xls';
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');

// download ke client
    header('Content-type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename="'.$fileName.'"');
    $objWriter->save('php://output');

    return $filePath.'/'.$fileName;

?>
