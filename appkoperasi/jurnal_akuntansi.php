<?php
require('header.php');
require('sidebar-right.php');
require('sidebar-left.php');
require('content-header.php');
?>

<div class="animated fadeinup delay-1">
    <div class="page-content">

      <?php if(isset($_SESSION['error-type']) and isset($_SESSION['error-message']) and isset($_SESSION['error-time'])){ ?>
          <?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
              <div class="notification notification-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
                  <h4> <?php echo ucfirst($_SESSION['error-type']); ?> </h4>
                  <?php echo $_SESSION['error-message']; ?>
              </div>
          <?php } ?>
      <?php } ?>

            <h2 class="uppercase"><?php echo lang('Jurnal Manual'); ?></h2>

            <form method="post" action="procjurnal_akuntansi.php">

                <div class="input-field">
                    <input type="text" name="tgl" id="tgl" class="datepicker" value="" autocomplete="off"></p></td>
                    <label><?php echo ('Tanggal'); ?></label>
                </div>

                <div class="input-field">
                    <input type="text" name="ket" id="ket" class="validate">
                    <label for="ket">Keterangan</label>
                </div>

                <div class="row m-t-20">
                    <div class="col s12 m-t-20">
                        <div class="table-responsive">
                            <table class="table" id="table_acc">
                                <thead>
                                    <tr>
                                        <th>
                                            Kode Account<br>
                                        </th>
                                        <th class="center">Debet</th>
                                        <th>
                                            Kode Account<br>
                                        </th>
                                        <th class="center">Kredit</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    
                                    <?php for($a=0;$a<19;$a++){
                                      $kredit = '';
                                      $kredit2 = '';
                                      $debit = '';
                                      $debit2 = '';
                                      if($a>0){
                                          $kredit = 'kredit';
                                          $kredit2 = 'disabled';
                                          $debit = 'debit';
                                          $debit2 = 'disabled';
                                      }
                                      ?>
                                    <tr>
                                        <td>
                                            <select name="kba<?php echo $a; ?>" id="kba1<?php echo $a; ?>" class="browser-default">
                                             <option value=''>- <?php echo lang('Pilih Akun'); ?> -</option>
                                                <?php
                                                $sql = "Select * from dbo.AccountMax where substring(KodeAccount,1,6) not in('1.1.01') order by KodeAccount ASC";
                                                $query = sqlsrv_query($conn, $sql);
                                                while($row  = sqlsrv_fetch_array( $query, SQLSRV_FETCH_NUMERIC)) { ?>
                                                        <option value="<?php echo $row[0] ;?>"> <?php echo $row[0]; ?> - <?php echo $row[1]; ?> </option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </td>
                                        <td>
                                            <input type="hidden" id="hdebit<?php echo $a; ?>" value="0" readonly>
                                            <input type="number" name="debit<?php echo $a; ?>" id="debit<?php echo $a; ?>" style="text-align: right;" placeholder="0">
                                        </td>
                                        <td>
                                            <select name="kbb<?php echo $a; ?>" id="kba2<?php echo $a; ?>" class="browser-default">
                                             <option value=''>- <?php echo lang('Pilih Akun'); ?> -</option>
                                                <?php
                                                $sql = "Select * from dbo.AccountMax where substring(KodeAccount,1,6) not in('1.1.01') order by KodeAccount ASC";
                                                $query = sqlsrv_query($conn, $sql);
                                                while($row  = sqlsrv_fetch_array( $query, SQLSRV_FETCH_NUMERIC)) { ?>
                                                        <option value="<?php echo $row[0] ;?>"> <?php echo $row[0]; ?> - <?php echo $row[1]; ?> </option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </td>
                                        <td>
                                            <input type="hidden" id="hkredit<?php echo $a; ?>" value="0" readonly>
                                            <input type="number" name="kredit<?php echo $a; ?>" id="kredit<?php echo $a; ?>" style="text-align: right;" placeholder="0">
                                        </td>
                                    </tr>


                                     <script type="text/javascript">
                                    	$('#kba1<?php echo $a; ?>').change(function(){
		                               		var kba = $('#kba1<?php echo $a; ?>').val();
		                                if(kba != "" ){
		                                   $('#kba2<?php echo $a; ?>').prop('disabled',true);
		                                }else{
		                                	$('#kba2<?php echo $a; ?>').prop('disabled',false);
		                                }
		                            	});

		                                $('#kba2<?php echo $a; ?>').change(function(){
		                                	var kba = $('#kba2<?php echo $a; ?>').val();
		                                 if(kba != "" ){
		                                   	$('#kba1<?php echo $a; ?>').prop('disabled',true);
		                                } else {
		                                	$('#kba1<?php echo $a; ?>').prop('disabled',false);
		                                }
		                            	});
                                    </script>

                                    <script type="text/javascript">
			                            $('#debit<?php echo $a; ?>').keyup(function(){
			                                var amount = $('#debit<?php echo $a; ?>').val();
			                                if(amount > 0){
			                                    $('#kredit<?php echo $a; ?>').prop('readonly', 'disabled');
			                                    $('#kredit<?php echo $a; ?>').val('0');
			                                }
			                                if(amount == 0){
			                                    $('#kredit<?php echo $a; ?>').prop('readonly', false);
			                                }
			                                if(amount.length == 0){
			                                    $('#kredit<?php echo $a; ?>').prop('readonly', false);
			                                }
			                            });

			                            $('#kredit<?php echo $a; ?>').keyup(function(){
			                                var amount = $('#kredit<?php echo $a; ?>').val();
			                                if(amount > 0){
			                                    $('#debit<?php echo $a; ?>').prop('readonly', 'disabled');
			                                    $('#debit<?php echo $a; ?>').val('0');
			                                }
			                                if(amount == 0){
			                                    $('#debit<?php echo $a; ?>').prop('readonly', false);
			                                }
			                                if(amount.length == 0){
			                                    $('#kredit<?php echo $x; ?>').prop('readonly', false);
			                                }
			                            });
			                            </script>


                                    <script type="text/javascript">
                                        $('#debit<?php echo $a; ?>').keyup(function(){
                                            var amount = $('#debit<?php echo $a; ?>').val();
                                            var amounth = $('#hdebit<?php echo $a; ?>').val();
                                            var totdebit = $('#totdebit').val();
                                            if(amount > 0){
                                                var amounth2 = $('#hdebit<?php echo $a; ?>').val();

                                                total = (parseFloat(totdebit) - parseFloat(amounth2)) + parseFloat(amount);
                                                $('#totdebit').val(total);

                                                $('#hdebit<?php echo $a; ?>').val(amount);
                                            }

                                            if(amount == 0){
                                                var amounth = $('#hdebit<?php echo $a; ?>').val();
                                                var totdebit = $('#totdebit').val();

                                                total = parseFloat(totdebit) - parseFloat(amounth);
                                                $('#totdebit').val(total);

                                                $('#hdebit<?php echo $a; ?>').val(amount);
                                            }

                                            if(amount.length == 0){
                                                $('#hdebit<?php echo $a; ?>').val(0);
                                            }

                                            selisih();
                                        });

                                        $('#kredit<?php echo $a; ?>').keyup(function(){
                                            var amount = $('#kredit<?php echo $a; ?>').val();
                                            var amounth = $('#hkredit<?php echo $a; ?>').val();
                                            var totkredit = $('#totkredit').val();
                                            if(amount > 0){
                                                var amounth2 = $('#hkredit<?php echo $a; ?>').val();

                                                total = (parseFloat(totkredit) - parseFloat(amounth2)) + parseFloat(amount);
                                                $('#totkredit').val(total);

                                                $('#hkredit<?php echo $a; ?>').val(amount);
                                            }

                                            if(amount == 0){
                                                var amounth = $('#hkredit<?php echo $a; ?>').val();
                                                var totkredit = $('#totkredit').val();

                                                total = parseFloat(totkredit) - parseFloat(amounth);
                                                $('#totkredit').val(total);

                                                $('#hkredit<?php echo $a; ?>').val(amount);
                                            }

                                            if(amount.length == 0){
                                                $('#hkredit<?php echo $a; ?>').val(0);
                                            }

                                            selisih();
                                        });

                                        function selisih(){
                                          var a = $('#totdebit').val();
                                          var b = $('#totkredit').val();

                                          var selisih = 0;
                                          sisa = parseFloat(a) - parseFloat(b);
                                          if(sisa < 0){
                                              selisih = sisa * -1;
                                          }
                                          else{
                                              selisih = sisa;
                                          }

                                          var kata = '';
                                          if(selisih == 0){
                                              kata = 'Simpan';
                                              $('#save').prop('disabled', false);
                                          }
                                          else{
                                            kata = 'Selisih '+selisih;
                                            $('#save').prop('disabled', true);
                                          }

                                          $('#kata').html(kata);
                                        }
                                    </script>
                                  <?php } ?>

                                  <tr>
                                        <td><b>Total Debet</b></td>
                                        <td>
                                            <b><input type="text" id="totdebit" class="validate" value="0" style="text-align: right;" readonly></b>
                                        </td>
                                        <td><b>Total Kredit</b></td>
                                        <td>
                                            <b><input type="text" id="totkredit" class="validate" value="0" style="text-align: right;" readonly></b>
                                        </td>
                                    </tr>
                                </body>
                                <tfoot>
                                   <!--  <tr>
                                        <td><b>Total Debet</b></td>
                                        <td>
                                            <b><input type="text" id="totdebit" class="validate" value="0" style="text-align: right;" readonly></b>
                                        </td>
                                        <td><b>Total Kredit</b></td>
                                        <td>
                                            <b><input type="text" id="totkredit" class="validate" value="0" style="text-align: right;" readonly></b>
                                        </td>
                                    </tr> -->
                                </tfoot>
                            </table>

                            <div style="bottom: 0;position: fixed;width:96%;text-align: center;left: 50%;margin-right: -50%;transform: translate(-50%, -50%);">
                                <button type="submit" name="save" id="save" class="btn btn-large primary-color width-100 waves-effect waves-light" disabled>
                                  <span id="kata">Simpan</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

    </div>
</div>

<?php include('footer.php'); ?>
