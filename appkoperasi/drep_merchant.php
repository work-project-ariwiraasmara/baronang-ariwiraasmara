<?php
session_start();
error_reporting(0);

include('lib/phpExcel/Classes/PHPExcel.php');
include "connect.php";


    $filePath = "uploads/excel/";

    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getProperties()->setCreator("baronang");
    $objPHPExcel->getProperties()->setTitle("Drep Laporan Merchant EDC");

// set autowidth
    for($col = 'A'; $col !== 'Z'; $col++) {
        $objPHPExcel->getActiveSheet()
            ->getColumnDimension($col)
            ->setAutoSize(true);
    }

    $listCombo = array();

    //sheet 2
    $objWorkSheet = $objPHPExcel->createSheet(0);
// baris judul
    

    $objWorkSheet->getStyle('C1')->getFont()->setBold(true);
    $objWorkSheet->getStyle('C1')->getFont()->setSize(12);
    $objWorkSheet->SetCellValue('C1', 'Laporan Merchant EDC');
    $objWorkSheet->getStyle('C2')->getFont()->setBold(true);
    $objWorkSheet->getStyle('C2')->getFont()->setSize(12);
    $objWorkSheet->SetCellValue('C2', 'Periode :');
    $objWorkSheet->getStyle('A3')->getFont()->setBold(true);
    $objWorkSheet->getStyle('A3')->getFont()->setSize(12);
    $objWorkSheet->SetCellValue('A3', 'Akun EDC :');

    
    $objWorkSheet->getStyle('A5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('A5' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('A5', 'Kode Transaksi');
    $objWorkSheet->getStyle('B5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('B5' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('B5', 'Tanggal Transaksi');
    $objWorkSheet->getStyle('C5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('C5' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('C5', 'Deskripsi');
    $objWorkSheet->getStyle('D5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('D5' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('D5', 'Debit');
    $objWorkSheet->getStyle('E5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('E5' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('E5', 'Kredit');
    $objWorkSheet->getStyle('F5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('F5' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('F5', 'Total');    



    if($_GET['akun'] and $_GET['from'] and $_GET['to']){
    $no = 1;
    $from= $_GET['from'];
    $tanggalsebelum = date('Y/m/d', strtotime('-1 days', strtotime($_GET['from'])));;
    $to= $_GET['to'];
    $lok = $_GET['akun'];
    $kop = $_GET['kop'];
    $edc = $_GET['edc'];
    $niledc = $_GET['nedc'];
    $num = $_GET['number'];
    $tglb = date('Y-m-d H:i:s', strtotime($from));
    $tglh = date('Y-m-d H:i:s', strtotime('+1 days', strtotime($to)));
    $bln = 2 ;
    $hanyatanggal = date('d', strtotime($_GET['from']));
    $bulan = date('m', strtotime($_GET['from']));
    $tahun = date('Y', strtotime($_GET['from']));
        if ($bulan != 1) {
            if ($bulan != 2 ) {
                if ($bulan != 3) {
                    if ($bulan != 4) {
                        if ($bulan !=5) {
                            if ($bulan !=6) {
                                if ($bulan !=7) {
                                    if ($bulan !=8) {
                                        if ($bulan !=9) {
                                            if ($bulan !=10) {
                                                if ($bulan !=11) {
                                                    $bulan = 'Desember';
                                                } else {
                                                $bulan = 'Nopember';
                                                }
                                            } else {
                                            $bulan = 'OKtober';   
                                            }
                                        } else {
                                        $bulan = 'September';
                                        }
                                    } else {
                                    $bulan = 'Agustus';
                                    }
                                } else {
                                $bulan = 'Juli';
                                }
                            } else {
                            $bulan = 'Juni';
                            }
                        } else {
                        $bulan = 'Mei';  
                        }
                    } else {
                    $bulan = 'April';       
                    }
                } else {
                $bulan = 'Maret';
                }
            } else {
            $bulan = 'Februari';    
            }                               
        } else {
        $bulan = 'Januari';
        }

    $tgl2 = date('d', strtotime($_GET['to']));
    $bulan2 = date('m', strtotime($_GET['to']));
    $tahun2 = date('Y', strtotime($_GET['to']));
        if ($bulan2 != 1) {
            if ($bulan2 != 2 ) {
                if ($bulan2 != 3) {
                    if ($bulan2 != 4) {
                        if ($bulan2 !=5) {
                            if ($bulan2 !=6) {
                                if ($bulan2 !=7) {
                                    if ($bulan2 !=8) {
                                        if ($bulan2 !=9) {
                                            if ($bulan2 !=10) {
                                                if ($bulan2 !=11) {
                                                    $bulan2 = 'Desember';
                                                } else {
                                                $bulan2 = 'Nopember';
                                                }
                                            } else {
                                            $bulan2 = 'OKtober';   
                                            }
                                        } else {
                                        $bulan2 = 'September';
                                        }
                                    } else {
                                    $bulan2 = 'Agustus';
                                    }
                                } else {
                                $bulan2 = 'Juli';
                                }
                            } else {
                            $bulan2 = 'Juni';
                            }
                        } else {
                        $bulan2 = 'Mei';  
                        }
                    } else {
                    $bulan2 = 'April';       
                    }
                } else {
                $bulan2 = 'Maret';
                }
            } else {
            $bulan2 = 'Februari';    
            }                               
        } else {
        $bulan2 = 'Januari';
        }

        $objWorkSheet->getStyle('D'.$bln)->getFont()->setBold(true);
        $objWorkSheet->SetCellValue("D".$bln,$hanyatanggal .' '.$bulan .' '. $tahun .' - '. $tgl2 .' '. $bulan2 .' '. $tahun2);  

    $objWorkSheet->getStyle('B3')->getFont()->setBold(true);
    $objWorkSheet->SetCellValueExplicit("B3",$_GET['edc']);

    $saldo = 6;
    

    $snilaid = "select * from dbo.AccountBalance where KodeAccount ='$niledc' and Header = '5'and Tanggal = '$tanggalsebelum'";
    //echo $snilaid;
    $pnilaid = sqlsrv_query($conn, $snilaid);
    while ($hnilaid = sqlsrv_fetch_array( $pnilaid, SQLSRV_FETCH_NUMERIC)){
        $hsnilaid = 0;
        if ($hnilaid == null){
            $hsnilaid = 0;    
        } else {
            $hsnilaid = $hnilaid[10];    
        }
    }


    $objWorkSheet->getStyle('A'.$saldo,'Saldo Awal')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('A'.$saldo,'Saldo Awal');
    $objWorkSheet->getStyle('F'.$saldo, number_format($hsnilaid))->getFont()->setBold(true);
    $objWorkSheet->SetCellValue("F".$saldo, number_format($hsnilaid), PHPExcel_Cell_DataType::TYPE_STRING);

    $row = 7;

    $aaa = "select * from dbo.TransList where AccountDebet = '$num' and date between '$tglb' and '$tglh' and TransactionType between 'TOPE' and 'TOPP' or AccountKredit = '$z[0]' and Date between '$tglb' and '$tglh' and TransactionType between 'TOPE' and 'TOPP' order by TransNo Asc";
    //echo $aaa;
    $bbb = sqlsrv_query($conn, $aaa);
    $total=$hsnilaid;
    //echo $total;
    $ctotal=$total;
    while($ccc = sqlsrv_fetch_array($bbb, SQLSRV_FETCH_NUMERIC)){
        if($ccc[1] == $num ){
            $total+=$ccc[5];
            $ctotal=$total;               
        }else{
            $total-=$ccc[5];
            $ctotal=$total;
        }
     
        // $aaaa = "select* from dbo.TransactionType where KodeTransactionType='".$ccc[3]."'";
        // $bbbb = sqlsrv_query($conn, $aaaa);
        // $cccc = sqlsrv_fetch_array($bbbb, SQLSRV_FETCH_NUMERIC);
        // if($cccc != null){
        //     $abc = $cccc[1];
        // }
        // else{
        //     $abc = $ccc[3];
        // }

        if ($num == $ccc[1]){
            $ndebit = $ccc[5];   
        } else {
            $ndebit = 0;                                    
        } 

        if ($num == $ccc[2]){
            $nkredit = $ccc[5];   
        } else {
            $nkredit = 0;        
        }


            $objWorkSheet->SetCellValue("A".$row,$ccc[0]);
            $objWorkSheet->SetCellValue("B".$row, $ccc[6]->format('Y-m-d H:i:s'));
            $objWorkSheet->SetCellValue("C".$row,$ccc[9].' - '.$ccc[7]);
            $objWorkSheet->SetCellValue("D".$row, number_format($ndebit), PHPExcel_Cell_DataType::TYPE_STRING);
            $objWorkSheet->SetCellValue("E".$row, number_format($nkredit), PHPExcel_Cell_DataType::TYPE_STRING);
            $objWorkSheet->SetCellValue("F".$row, number_format($ctotal), PHPExcel_Cell_DataType::TYPE_STRING);            

                $no++;
                $row++;
            
    }

    $amountdebit = "select sum(amount) from dbo.TransList where AccountDebet = '$num' and date between '$tglb' and '$tglh' and TransactionType between 'TOPE' and 'TOPP'";
    //echo $amountdebit;
    $prosesdebit = sqlsrv_query($conn, $amountdebit);
    $hasilDebit1 = sqlsrv_fetch_array($prosesdebit, SQLSRV_FETCH_NUMERIC); 
    $hasilDebit = $hasilDebit1[0];

    $amountKredit = "select sum(amount) from dbo.TransList where AccountKredit = '$num' and Date between '$tglb' and '$tglh' and TransactionType between 'TOPE' and 'TOPP'";
    //echo $amountKredit;
    $prosesKredit = sqlsrv_query($conn, $amountKredit);
    $hasilKredit1 = sqlsrv_fetch_array($prosesKredit, SQLSRV_FETCH_NUMERIC); 
    $hasilKredit = $hasilKredit1[0];




            $objWorkSheet->getStyle('A5:F' .$row)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objWorkSheet->getStyle('A'.$row,'Saldo Akhir')->getFont()->setBold(true);
            $objWorkSheet->SetCellValue('A'.$row,'Saldo Akhir');
            $objWorkSheet->getStyle('D'.$row, number_format($hasilDebit))->getFont()->setBold(true);
            $objWorkSheet->SetCellValue("D".$row, number_format($hasilDebit), PHPExcel_Cell_DataType::TYPE_STRING);
            $objWorkSheet->getStyle('E'.$row, number_format($hasilKredit))->getFont()->setBold(true);
            $objWorkSheet->SetCellValue("E".$row, number_format($hasilKredit), PHPExcel_Cell_DataType::TYPE_STRING);
            $objWorkSheet->getStyle('F'.$row, number_format($ctotal))->getFont()->setBold(true);
            $objWorkSheet->SetCellValue("F".$row, number_format($ctotal), PHPExcel_Cell_DataType::TYPE_STRING);
    }
//exit;
    $objWorkSheet->setTitle('Drep Laporan Merchant EDC');

    $fileName = 'LapMerEDC'.'_'.strtotime(date('Y-m-d H:i:s')).'.xls';
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');

// download ke client
    header('Content-type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename="'.$fileName.'"');
    $objWriter->save('php://output');

    return $filePath.'/'.$fileName;

?>
