<?php require('header.php');?>

<?php require('sidebar-right.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<div class="animated fadeinup delay-1">
    <div class="page-content">

        <h2 class="uppercase"><?php echo lang('Transfer Antar Bank'); ?></h2>

              <?php if(isset($_SESSION['error-type']) and isset($_SESSION['error-message']) and isset($_SESSION['error-time'])){ ?>
                  <?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
                      <div class="notification notification-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
                          <h4> <?php echo ucfirst($_SESSION['error-type']); ?> </h4>
                          <?php echo $_SESSION['error-message']; ?>
                      </div>
                  <?php } ?>
              <?php } ?>

            <form class="form-horizontal" action="procbank_transfer.php" method = "POST">
                <div>
                    <label><?php echo lang('Tanggal'); ?></label>
                    <input type="text" name="tgl" class="validate" value="<?php echo date('d F Y');?>" readonly>
                </div>

                <div style="margin-top: 20px;">
                <?php echo lang('Bank Pengirim'); ?>
                <select class="browser-default" name="kba1">
                    <option>- <?php echo lang('Pilih salah satu'); ?> -</option>
                    <?php
                    $sql = 'Select * from dbo.BankAccount order by AccName ASC';
                    $query = sqlsrv_query($conn, $sql);
                    while($row  = sqlsrv_fetch_array( $query, SQLSRV_FETCH_NUMERIC)) { ?>
                        <option value="<?php echo $row[1] ;?>"> <?php echo $row[3]; ?> - <?php echo $row[4]; ?> </option>
                        <?php
                    }
                    ?>
                </select>
                </div>

                <div style="margin-top: 20px;">
                <?php echo lang('Bank Penerima'); ?>
                <select class="browser-default" name="kba2">
                    <option>- <?php echo lang('Pilih salah satu'); ?> -</option>
                    <?php
                    $sql = 'Select * from dbo.BankAccount order by AccName ASC';
                    $query = sqlsrv_query($conn, $sql);
                    while($row  = sqlsrv_fetch_array( $query, SQLSRV_FETCH_NUMERIC)) { ?>
                        <option value="<?php echo $row[1] ;?>"> <?php echo $row[3]; ?> - <?php echo $row[4]; ?> </option>
                        <?php
                    }
                    ?>
                </select>
                </div>

                <div class="input-field">
                    <input type="number" name="amount" class="validate">
                    <label><?php echo lang('Amount'); ?></label>
                </div>

                <div class="input-field">
                    <input type="text" name="keterangan" class="validate">
                    <label><?php echo lang('Keterangan'); ?></label>
                </div>

                <div style="margin-top: 30px;">
                    <button type="submit" class="waves-effect waves-light btn-large primary-color width-100"><?php echo lang('Simpan'); ?></button>
                </div>

            </form>
    </div>
</div>

<?php require('footer.php'); ?>
