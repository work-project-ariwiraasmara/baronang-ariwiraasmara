<?php require('header.php');?>

<?php require('sidebar-right.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<div class="animated fadeinup delay-1">
    <div class="page-content">
        <h2 class="uppercase"><?php echo lang('Jurnal'); ?></h2><br>

        <div class="box-body" style="margin-top: 30px;">
            <form action="" method="get">
                <label ><?php echo lang('Pencarian'); ?></label>
                <select id="" name="userid" class="browser-default" value="<?php echo "$_GET[userid]";?>" onChange="showCompany(this.value); ">
                    <option value=''>- <?php echo ('Pilih Pencarian'); ?> -</option>
                        <?php
                        $bln=array(01=>"Tanggal","No Referensi");
                                echo $bln;
                                $from='';
                                for ($bulan=01; $bulan<=02; $bulan++) { 
                                    if ($_POST['bulan'] == $bulan){
                                        $ck="selected";
                                     }   else {
                                        $ck="" ;
                                     }
                                     //echo "<option value='$bulan' $ck>$bln[$bulan]</option>";
                        ?>
                        <!-- <option value="$bulan" <?php if($bulan==$_GET['userid']){echo "selected";} ?>><?=$ck>$bln[$bulan];?></option> -->
                        <option value="<?=$bulan;?>" <?php if($bulan==$_GET['userid']){echo "selected";} ?><?="$ck>$bln[$bulan]";?></option>
                        <?php } ?>
                </select>
            <div style="margin-top: 30px;">
            <?php 
                if ($_GET['userid'] == 1){
                    ?>
                    <div class="input-field">
                      <input type="text" name="cari1" id="cari1" class="datepicker" value="<?php echo "$_GET[cari1]";?>" autocomplete="off"></p></td>
                      <label><?php echo ('Tanggal Awal'); ?></label>
                    </div>
                 
                    <div class="input-field">
                      <input type="text" name="cari2" id="cari2" class="datepicker" value="<?php echo "$_GET[cari2]";?>" autocomplete="off"></p></td>
                      <label><?php echo ('Tanggal Akhir'); ?></label>
                    </div>

                  <div style="margin-top: 30px;">
                      <button type="submit" value = "rep_jurnalcek.php?page=<?=$halaman?>&userid=1"class="waves-effect waves-light btn-large primary-color width-100"><?php echo lang('Cari'); ?></button>
                  </div>
              <?php } else if ($_GET['userid'] == 2){
                  ?>
                  <input type="hidden" name="userid"  id="userid" value="<?php echo "$_GET[userid]";?>">
                  <div class="input-field">
                      <input type="text" name="ref" id="ref" class="" value="<?php echo "$_GET[ref]";?>" autocomplete="off"></p></td>
                      <label><?php echo ('No Referensi'); ?></label>
                  </div>
                  
                  <div style="margin-top: 30px;">
                      <button type="submit" class="waves-effect waves-light btn-large primary-color width-100"><?php echo lang('Cari'); ?></button>
                  </div>
                  <?php } ?>


            </form>


        <!-- Pencarian berdasarkan No Referensi -->
            <?php
            if (isset($_GET['ref']) and isset($_GET['userid'])){
                $ref = $_GET['ref'];
            ?>
            <div style="margin-top: 20px;">
                Hasil Pencarian<br>
                No Referensi<br>
                <input type="text" name="" value="<?php echo ($ref) ?>" readonly>
                
               <a href="rep_jurnal.php?userid=<?php echo $_GET['userid'];?>"><button type="button" class="waves-effect waves-light btn-large primary-color width-100" style="margin-top: 20px;">Back To All Data</button></a>
            </div>

            <div style="margin-bottom: 30px;" style="margin-top: 30px;">
               <a href="drep_jurnal1.php?userid=<?php echo $_GET['userid']; ?>&ref=<?php echo $ref; ?>"><button type="button" class="waves-effect waves-light btn-large primary-color width-100">Download to excel</button></a>
            </div> 

            <?php } ?>


            <!-- Pencarian Berdasarkan Tanggal -->
            <?php
            if(isset($_GET['cari1']) and isset($_GET['cari2']) and isset($_GET['userid'])){
            $cari1 = $_GET['cari1'];
            $format1 = date('d F Y', strtotime($cari1));
            $cari2 = $_GET['cari2'];
            $format2 = date('d F Y', strtotime($cari2));
            
            //echo "<b>Hasil pencarian : ".$cari."</b>";
            
            ?>

            <div style="margin-top: 20px; margin-bottom: 30px;">
                Hasil Pencarian<br>
                Dari Tanggal<br>
                <input type="text" name="" value="<?php echo ($format1) ?>" readonly>
                Sampai Tanggal<br>
                <input type="text" name="" value="<?php echo ($format2) ?>" readonly>
               <!-- <a href="rep_jurnal.php?userid=<?php echo $_GET['userid'];?>"><button type="button" class="waves-effect waves-light btn-large primary-color width-100" style="margin-top: 20px;">Download To Excel</button></a> -->
               <a href="rep_jurnal.php?userid=<?php echo $_GET['userid'];?>"><button type="button" class="waves-effect waves-light btn-large primary-color width-100" style="margin-top: 20px;">Back To All Data</button></a>
            </div>

            <div style="margin-bottom: 30px;" style="margin-top: 30px;">
                <a href="drep_jurnal1.php?userid=<?php echo $_GET['userid']; ?>&cari1=<?php echo $cari1; ?>&cari2=<?php echo $cari2; ?>"><button type="button" class="waves-effect waves-light btn-large primary-color width-100">Download to excel</button></a>
            </div>          
            
             <?php } ?>
           
        </div>



        <?php if (!empty($_GET['userid'])){
            ?>
        <div>
             <h3 class="box-title" style="margin-top: 30px;"><?php echo lang('Daftar Junal'); ?></h3>
        </div>
        <div class="box-body">
            <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-lg-12 table-responsive">
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th><?php echo lang('Tanggal'); ?></th>
                        <th><?php echo lang('No Referensi'); ?></th>
                        <th><?php echo lang('Kode Jurnal'); ?></th>
                        <th><?php echo lang('Kode Debit'); ?> / <?php echo lang('Nama Debit'); ?></th>
                        <th><?php echo lang('Kode Kredit'); ?> / <?php echo lang('Nama Kredit'); ?></th>
                        <th><?php echo lang('Harga'); ?></th>
                        <th><?php echo lang('Deskripsi'); ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    //count
                    if(isset($_GET['userid'])){

                    $ref = $_GET['ref'];
                    //echo $ref;
                    $cari1 = $_GET['cari1'];
                    $cari2 = $_GET['cari2'];

                    $cekref = "select * from dbo.JurnalTrans where RefNumber = '$ref'";
                    //echo $cekref;
                    $cekref2 = sqlsrv_query($conn, $cekref);
                    $hasilref   = sqlsrv_fetch_array($cekref2, SQLSRV_FETCH_NUMERIC);
                    
                    //var_dump($hasilref);
                    
                    //echo $cari2;
                    if (isset($_GET['cari1']) and isset($_GET['cari2'])) {
                        $cari1= date('Y-m-d H:i:s', strtotime($_GET['cari1']));
                        $cari2= date('Y-m-d 23:59:59', strtotime($_GET['cari2']));
                        $jmlulsql =  "SELECT count (*) FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY Date asc) as row FROM [dbo].[Translist] a where TransNo is not null and Date between '".$cari1."' and '".$cari2."' ) a";
                        //echo $jmlulsql;
                    } else if (isset($_GET['ref'])) {
                        $jmlulsql = "SELECT count (*) FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY Date asc) as row FROM [dbo].[Translist] a where TransNo is not null and RefNumber = '$ref' ) a";
                       // echo $jmlulsql;
                    } else {
                    $jmlulsql   = "SELECT count (*) FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY Date asc) as row FROM [dbo].[Translist] a where TransNo is not null) a";
                    }

                    //echo $jmlulsql;
                    $jmlulstmt  = sqlsrv_query($conn, $jmlulsql);
                    $jmlulrow   = sqlsrv_fetch_array($jmlulstmt, SQLSRV_FETCH_NUMERIC);

                    //pagging
                    $perpages   = 10;
                    $halaman    = $_GET['page'];
                    if(empty($halaman)){
                        $posisi  = 0;
                        $batas   = $perpages;
                        $halaman = 1;
                    }
                    else{
                        $posisi  = (($perpages * $halaman) - 10) + 1;
                        $batas   = ($perpages * $halaman);
                    }

                   

                    $ulsql = "SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY Date asc) as row FROM [dbo].[Translist] a where TransNo is not null) a WHERE row between '$posisi' and '$batas'";
                    //echo $ulsql;
                    if(isset($_GET['cari1']) and isset($_GET['cari2'])){
                    $cari1new= $_GET['cari1'];
                    $cari2new= $_GET['cari2'];
                    $cari1 = date('Y-m-d H:i:s', strtotime($cari1new));
                    $cari2 = date('Y-m-d 23:59:59', strtotime($cari2new));
                    $ulsql = "SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY Date asc) as row FROM [dbo].[Translist] a where TransNo is not null and Date between '".$cari1."' and '".$cari2."' ) a WHERE row between '$posisi' and '$batas'";
                    //echo $ulsql;
                    } else if (isset($_GET['ref'])) {
                        $ulsql = "SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY Date asc) as row FROM [dbo].[Translist] a where TransNo is not null and RefNumber = '$ref') a WHERE row between '$posisi' and '$batas'";
                    }
                    //echo $ulsql;
                    $ulstmt = sqlsrv_query($conn, $ulsql);
                    while($ulrow = sqlsrv_fetch_array( $ulstmt, SQLSRV_FETCH_NUMERIC)){
                            $tanggal = $ulrow[6]->format('Y-m-d H:i:s');
                            $noacc = '';
                            $acc = '';
                            $sql   = "select * from dbo.Account where KodeAccount='$ulrow[3]'";
                            //echo $sql;
                            $stmt  = sqlsrv_query($conn, $sql);
                            $row   = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_NUMERIC);
                            $row1 = $row[1];

                            $sql1   = "select * from dbo.Account where KodeAccount='$ulrow[4]'";
                            //echo $sql1;
                            $stmt1  = sqlsrv_query($conn, $sql1);
                            $row2   = sqlsrv_fetch_array($stmt1, SQLSRV_FETCH_NUMERIC);
                            $row3 = $row2[1];
                            ?>                                  
                        <tr>
                            <td><?php echo $ulrow[15]; ?></td>
                            <td><?php echo $tanggal; ?></td>
                            <td><?php echo $ulrow[8]; ?></td>
                            <td><?php echo $ulrow[0]; ?></td>
                            <td><?php echo $ulrow[3]; ?> / <?php echo $row1; ?></td>
                            <td><?php echo $ulrow[4]; ?> / <?php echo $row3; ?></td>
                            <td style="text-align: right;"><?php echo number_format($ulrow[5],2); ?></td>
                            <td><?php echo $ulrow[9]; ?></td>
                        </tr>
                    <?php
                    }
                    $jmlhalaman = ceil($jmlulrow[0]/$perpages);
                    ?>
                    </tbody>
                </table>
            </div>

            <div class="box-footer clearfix right">
                <div style="text-align: center;">
                    Showing <?=$halaman?> to <?=$jmlhalaman?> of <?=$jmlulrow[0]?> entries <?=$posisi." - ".$batas?>
                    <?php
                    if ($_GET['cari1'] and $_GET['cari2']){
                        $userid = $_GET['userid'];
                        $cari1 = $_GET['cari1'];
                        $cari2 = $_GET['cari2'];
                        $reload = "rep_jurnal.php?userid=1&cari1=$cari1&cari2=$cari2";    
                    } else if ($_GET['ref']) {
                        $ref = $_GET['ref'];
                        $reload = "rep_jurnal.php?userid=2&ref=$ref";
                    } else {
                        $userid = $_GET['userid'];
                        $reload = "rep_jurnal.php?userid=$userid";
                    }                   
                    $page = intval($_GET["page"]);
                    $adjacents = isset($_GET['adjacents']) ? intval($_GET['adjacents']) : 0; // khusus style pagination 2 dan 3
                    if( $page == 0 ) $page = 1;

                    $tpages = ($jmlhalaman) ? ceil($jmlhalaman) : 0; // total pages, last page number
                    echo "<div>".paginate($reload, $page, $tpages, $adjacents)."</div>";
                    ?>
                </div>
            </div>
        </div>
         <?php } ?>
        <?php } ?>
    </div>
</div>

<script language="javascript" type="text/javascript">
    function showCompany(bulan) {
        window.location.href="rep_jurnal.php?userid="+bulan;
    }
</script>

<?php
function paginate($reload, $page, $tpages) {

    $firstlabel = "First";
    $prevlabel  = "Prev";
    $nextlabel  = "Next";
    $lastlabel  = "Last";

    $out = "<div class=\"backpaging\"><ul class=\"ownpaging\">\n";

    // first
    if($page>1) {
        $out.= "<li><a href=\"" . $reload . "\">" . $firstlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $firstlabel . "</a></li>\n";
    }

    // previous
    if($page==1) {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $prevlabel . "</a></li>\n";
    }
    elseif($page==2) {
        $out.= "<li><a href=\"" . $reload . "\">" . $prevlabel . "</a></li>\n";
    }
    else {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" . ($page-1) . "\">" . $prevlabel . "</a></li>\n";
    }

    // current
    $out.= "<li class=\"active\"><a href=\"#\">Page " . $page . " of " . $tpages . "</a></li>\n";

    // next
    if($page<$tpages) {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" .($page+1) . "\">" . $nextlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $nextlabel . "</a></li>\n";
    }

    // last
    if($page<$tpages) {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" . $tpages . "\">" . $lastlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $lastlabel . "</a></li>\n";
    }

    $out.= "</ul></div>";

    return $out;
}

require('footer.php');
?>
