<?php require('blank-header.php');?>

<?php
include "connectinti.php";

if(!empty($_SESSION['KID']) && !empty($_SESSION['NamaKoperasi']) && !empty($_SESSION['Server']) && !empty($_SESSION['DatabaseName']) && !empty($_SESSION['UserDB']) && !empty($_SESSION['PassDB']) &&  !empty($_SESSION['UserID']) && !empty($_SESSION['Name']) && !empty($_SESSION['KodeJabatan'])){
    unset($_SESSION['KID']);
    unset($_SESSION['NamaKoperasi']);
    unset($_SESSION['Server']);
    unset($_SESSION['DatabaseName']);
    unset($_SESSION['UserDB']);
    unset($_SESSION['PassDB']);
    unset($_SESSION['Status']);
    unset($_SESSION['UserID']);
    unset($_SESSION['Name']);
    unset($_SESSION['KodeJabatan']);
}
?>

        <div class="animated delay-1">
            <div class="form-inputs">
              <center><img src ="static/images/logo_vertical.png" style="width: 30%;" align="middle"></center>

                <div style="margin-top: 80px;">
                  <?php if(isset($_SESSION['error-type']) and isset($_SESSION['error-message']) and isset($_SESSION['error-time'])){ ?>
                      <?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
                          <div class="notification notification-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
                              <!--<i class="ion-android-close btn btn-default right" data-dismiss="alert" aria-hidden="true"></i>-->
                              <h4> <?php echo ucfirst($_SESSION['error-type']); ?> </h4>
                              <?php echo $_SESSION['error-message']; ?>
                          </div>
                      <?php } ?>
                  <?php } ?>
                <b style="color: #fff;"><?php echo lang('Masuk'); ?></b>
                <form action="cek_login.php" method="post">
                    <div class="input-field">
                        <input type="number" name="kid" class="validate" style="color: white;">
                        <label for="login"><?php echo lang('ID Koperasi'); ?></label>
                    </div>
                    <div class="input-field">
                        <input type="text" name="name" class="validate" autocomplete="off" style="color: white;">
                        <label for="login-psw"><?php echo lang('Username'); ?></label>
                    </div>
                    <div class="input-field">
                        <input type="password" name="password" class="validate" style="color: white;">
                        <label for="login-psw"><?php echo lang('Password'); ?></label>
                    </div>

                    <span class="right" style="margin-bottom:10px;">
                            <?php
                            $y = "select * from [dbo].[LanguageList]";
                            $n = sqlsrv_query($conns, $y);
                            while($m = sqlsrv_fetch_array( $n, SQLSRV_FETCH_NUMERIC)){
                                if($_SESSION['Language'] == $m[0]){
                                    echo $m[0].' /';
                                } else { ?>
                                    <a href="?lang=<?php echo $m[0]; ?>" title="<?php echo $m[1]; ?>"><?php echo $m[0]; ?></a> /
                                <?php } ?>
                            <?php } ?>
                        </span>

                    <div style="margin-bottom: -10px;">
                        <button type="submit" class="waves-effect waves-light width-100 m-b-20" style="background: #00c0ef; border-radius: 50px; color: #fff; padding: 15px; border: none;"><?php echo lang('Masuk'); ?></button>
                    </div>

                    <a href="register.php"><button type="button" class="waves-effect waves-light width-100 m-b-20" style="background: #fff; border-radius: 50px; color: #000; padding: 15px; border: none;"><?php echo lang('Daftar'); ?></button></a>

                </form>
                </div>
              </div>
        </div>

<?php require('blank-footer.php');?>
