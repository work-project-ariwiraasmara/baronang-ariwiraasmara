<?php
if(isset($_GET['download'])) {
	require('connect.php');
    require('lib/phpexcel/Classes/PHPExcel.php');

    $filePath = "uploads/excel/";

    $sh1 = "SELECT Nama from [dbo].[LocationMerchant] where LocationID='".@$_GET['loc']."'";
	$qh1 = sqlsrv_query($conn, $sh1);
	$dh1 = sqlsrv_fetch_array($qh1);

	$title = "Laporan Lokasi ".$dh1[0];

    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getProperties()->setCreator("Baronang");
    $objPHPExcel->getProperties()->setTitle("Laporan Lokasi");

    // set autowidth
    for($col = 'A'; $col !== 'Z'; $col++) {
        $objPHPExcel->getActiveSheet()
            ->getColumnDimension($col)
            ->setAutoSize(true);
    }

    //sheet 2
    $objWorkSheet = $objPHPExcel->createSheet(0);

    // baris judul
	$objWorkSheet->getStyle('A1')->getFont()->setSize(20);
	$objWorkSheet->getStyle('A1')->getFont()->setBold(true);
	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:C1');
	$objWorkSheet->SetCellValue('A1', 'Detail Laporan Lokasi Parkir '.$title);
	$objWorkSheet->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    $sps = "SELECT ValueConversion from [dbo].[ParkingSetting]";
	$qps = sqlsrv_query($conn, $sps);
	$dps = sqlsrv_fetch_array($qps);


	//CONTENT 1
	// baris judul
	$objWorkSheet->getStyle('A1')->getFont()->setSize(20);
	$objWorkSheet->getStyle('A1')->getFont()->setBold(true);
	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:B1');
	$objWorkSheet->SetCellValue('A1', 'Detail Laporan Lokasi '.$title);
	$objWorkSheet->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

	$objWorkSheet->getStyle('A2')->getFont()->setSize(17);
	$objWorkSheet->getStyle('A2')->getFont()->setBold(true);
	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:B2');
	$objWorkSheet->SetCellValue('A2', date('d F Y', strtotime($_GET['t'].'-'.$_GET['b'].'-'.$_GET['d'])));
	$objWorkSheet->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);	

	$objPHPExcel->getActiveSheet()
                        ->getStyle('A')
                        ->getNumberFormat()
                        ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(50);
	$objWorkSheet->getStyle('A4')->getFont()->setBold(true);
	$objWorkSheet->SetCellValue('A4', 'No. Kartu');
	$objWorkSheet->getStyle('A4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);	

	$objPHPExcel->getActiveSheet()
                        ->getStyle('B')
                        ->getNumberFormat()
                        ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(50);
	$objWorkSheet->getStyle('B4')->getFont()->setBold(true);
	$objWorkSheet->SetCellValue('B4', 'Point');
	$objWorkSheet->getStyle('B4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);	


	//CONTENT 2
	$row = 5;
	$totd = 0;
	$tglxa = @$_GET['t'].'-'.@$_GET['b'].'-'.@$_GET['d'].' 00:00:00';
	$tglxt = @$_GET['t'].'-'.@$_GET['b'].'-'.@$_GET['d'].' 23:59:59';
	$s1 = "SELECT Date, AccountDebet, Amount from [dbo].[TransList] where AccountKredit='".@$_GET['loc']."' and TransactionType='PARK' and Date between '$tglxa' and '$tglxt' and Amount != 0 order by Date ASC";
	//echo $s1;
	$q1 = sqlsrv_query($conn, $s1);
	while($d1 = sqlsrv_fetch_array($q1)) {

		$objPHPExcel->getActiveSheet()
					->getStyle('A'.$row)
					->getNumberFormat()
					->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
		$objWorkSheet->SetCellValue('A'.$row, ' '.$d1[1]);

		$objPHPExcel->getActiveSheet()
					->getStyle('B'.$row)
					->getNumberFormat()
					->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
		$objWorkSheet->SetCellValue('B'.$row, number_format( ($d1[2]*$dps[0]), 2, ',', '.'));
		$objWorkSheet->getStyle('B'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);	

		$totd = $totd + $d1[2];
		$row++;
	}

	$objWorkSheet->getStyle('A'.$row)->getFont()->setBold(true);
	$objWorkSheet->SetCellValue('A'.$row, 'Total : ');
	$objWorkSheet->getStyle('A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

	$objWorkSheet->getStyle('B'.$row)->getFont()->setBold(true);
	$objWorkSheet->SetCellValue('B'.$row, number_format( ($totd*$dps[0]), 2, ',', '.'));
	$objWorkSheet->getStyle('B'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);	

	$fileName = 'Laporan Lokasi '.$title.' '.date('d F Y', strtotime($_GET['t'].'-'.$_GET['b'].'-'.$_GET['d'])).'.xls';
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');


	// download ke client
	header('Content-type: application/vnd.ms-excel');
	header('Content-Disposition: attachment; filename="'.$fileName.'"');
	$objWriter->save('php://output');
}
else {
	@session_start();
	error_reporting(0);
	require('header.php');
	require('sidebar-right.php');
	require('sidebar-left.php');

	if(isset($_GET['tm'])) {
		$bln = @$_GET['b'];
		$thn = @$_GET['t'];
		$tga = date('t', strtotime($thn.'-'.$bln.'-1'));
	}
	else {
		$bln = date('m');
		$thn = date('Y');
		$tga = date('t', strtotime($thn.'-'.$bln.'-1'));
	}
	$tgla = $thn.'-'.$bln.'-1';
	$tglt = $thn.'-'.$bln.'-'.$tga;

	$sh1 = "SELECT Nama from [dbo].[LocationMerchant] where LocationID='".@$_GET['loc']."'";
	$qh1 = sqlsrv_query($conn, $sh1);
	$dh1 = sqlsrv_fetch_array($qh1);

	$sps = "SELECT ValueConversion from [dbo].[ParkingSetting]";
	$qps = sqlsrv_query($conn, $sps);
	$dps = sqlsrv_fetch_array($qps);
	?>

		<div class="animated fadeinup delay-1">
	        <div class="page-content txt-black">
				
	        	<h1 class="uppercase txt-black">
	        		Laporan Lokasi
	        		<?php
	        		if(isset($_GET['det'])) {
						if(@$_GET['det'] == 2) {
		        			echo '<br>'.$dh1[0].' '.date('d F Y', strtotime($thn.'-'.$bln.'-'.@$_GET['d']));
			        	}
			        	else {
			        		echo '<br>'.$dh1[0].' '.date('F Y', strtotime($thn.'-'.$bln.'-1'));
			        	}
					}
					else {
						echo '<br>'.date('F Y', strtotime($thn.'-'.$bln.'-1'));
					}
					?>
	        	</h1>
	        	<br>

	        	<?php
	        	if(!isset($_GET['det'])) { ?>
		        	<div class="m-t-30 row">
		        		<div class="col s4">
		        			<span>Bulan</span> : <br>
		        			<select class="browser-default" id="bulan">
		        				<?php
		        				for($x = 1; $x < 13; $x++) { ?>
		        					<option value="<?php echo date('m', strtotime(date('Y').'-'.$x.'-1') ); ?>"><?php echo date('F', strtotime(date('Y').'-'.$x.'-1') ); ?></option>
		        					<?php
		        				}
		        				?>
		        			</select>
		        		</div>

		        		<div class="col s4">
		        			<span>Tahun</span> : <br>
		        			<select class="browser-default" id="tahun">
		        				<?php
		        				for($x = date('Y'); $x > 2014; $x--) { ?>
		        					<option value="<?php echo $x; ?>"><?php echo $x; ?></option>
		        					<?php
		        				}
		        				?>
		        			</select>
		        		</div>

		        		<div class="col s4">
		        			<button class="btn btn-large width-100 primary-color m-t-10" id="btn_ok">OK</button>
		        		</div>
		        	</div>
	        	<?php } ?>

	        	<div class="m-t-30">
	        		<?php
	        		if( isset($_GET['det']) ) {
	        			if( @$_GET['det'] == 2 ) { ?>
	        				<div class="table-responsive m-t-10">
	        					<div class="right">
		        					<a href="<?php echo '?download=1&det=2&loc='.@$_GET['loc'].'&tm='.@$_GET['tm'].'&b='.@$_GET['b'].'&t='.@$_GET['t'].'&d='.@$_GET['d']; ?>" class="btn primary-color">Download</a>
		        				</div>
		        				<table class="table">
		        					<thead>
		        						<tr>
		        							<th>No. Kartu</th>
		        							<th style="text-align: center;">Point</th>
		        						</tr>
		        					</thead>

		        					<tbody>
		        						<?php
		        						$totd = 0;
		        						$tglxa = $thn.'-'.$bln.'-'.@$_GET['d'].' 00:00:00';
		        						$tglxt = $thn.'-'.$bln.'-'.@$_GET['d'].' 23:59:59';
		        						$s1 = "SELECT Date, AccountDebet, Amount from [dbo].[TransList] where AccountKredit='".@$_GET['loc']."' and TransactionType='PARK' and Date between '$tglxa' and '$tglxt' and Amount != 0 order by Date ASC";
		        						//echo $s1;
		        						$q1 = sqlsrv_query($conn, $s1);
		        						while($d1 = sqlsrv_fetch_array($q1)) { ?>
		        							<tr>
		        								<td><?php echo $d1[1]; ?></td>
		        								<td style="text-align: right;"><?php echo number_format( ($d1[2]*$dps[0]), 2, ',', '.'); ?></td>
		        							</tr>
		        							<?php
		        							$totd = $totd + $d1[2];
		        						}
		        						?>
		        					</tbody>

		        					<tfoot>
		        						<tr>
		        							<td style="text-align: right;"><b>Total</b></td>
		        							<th style="text-align: right;"><?php echo number_format( ($totd*$dps[0]), 2, ',', '.') ?></th>
		        						</tr>
		        					</tfoot>
		        				</table>
		        			</div>
	        				<?php
	        			}
	        			else { ?>
	        				<div class="table-responsive m-t-10">
		        				<table class="table">
		        					<thead>
		        						<tr>
		        							<th>Tanggal</th>
		        							<th>Pemakaian Point</th>
		        						</tr>
		        					</thead>

		        					<tbody>
		        						<?php
		        						//echo $tga;
		        						$tot = 0;
		        						for($x = 1; $x <= $tga; $x++) {
		        							$tglxa = $thn.'-'.$bln.'-'.$x.' 00:00:00';
		        							$tglxt = $thn.'-'.$bln.'-'.$x.' 23:59:59';
		        							$s1 = "SELECT distinct (SELECT SUM(Amount) from [dbo].[TransList] where AccountKredit='".@$_GET['loc']."' and TransactionType='PARK' and Date between '$tglxa' and '$tglxt' Having SUM(Amount) != 0) from [dbo].[TransList] where AccountKredit='".@$_GET['loc']."' and TransactionType='PARK' and Date between '$tglxa' and '$tglxt'";
			        						//echo $x.'). '.$s1.'<br>';
			        						$q1 = sqlsrv_query($conn, $s1);
			        						while($d1 = sqlsrv_fetch_array($q1)) { 
			        							if( !((int)$d1[0] == 0) || $d1[0] != '' || !empty($d1[0]) || !is_null($d1[0])) { ?>
				        							<tr>
				        								<td><a href="<?php if( !((int)$d1[0] == 0) ) { echo '?det=2&loc='.@$_GET['loc'].'&tm=1&b='.$bln.'&t='.$thn.'&d='.$x; } else { echo '#'; } ?>"><?php echo $thn.'-'.$bln.'-'.$x; ?></a></td>
				        								<td style="text-align: right;"><?php echo number_format( ($d1[0]*$dps[0]), 2, ',', '.'); ?></td>
				        							</tr>
				        							<?php
			        							}
			        							$tot = $tot + $d1[0];
			        						}
		        						}
		        						?>
		        					</tbody>

		        					<tfoot>
		        						<tr>
		        							<th style="text-align: right;"><b>Total</b></th>
		        							<th style="text-align: right;"><?php echo number_format( ($tot*$dps[0]), 2, ',', '.');; ?></th>
		        						</tr>
		        					</tfoot>
		        				</table>
		        			</div>
	        				<?php
	        			}
	        		}
	        		else { ?>
	        			<div class="table-responsive m-t-10">
	        				<table class="table">
	        					<thead>
	        						<tr>
	        							<th>Lokasi</th>
	        							<th style="text-align: center;">Pemakaian Point</th>
	        						</tr>
	        					</thead>

	        					<tbody>
	        						<?php
		        						$s1 = "SELECT LocationID, Nama from [dbo].[LocationMerchant] order by Nama ASC";
		        						$q1 = sqlsrv_query($conn, $s1);
		        						while($d1 = sqlsrv_fetch_array($q1)) {
		        							$s2 = 	"SELECT (SELECT Sum(Amount) from [dbo].[TransList] where AccountKredit='$d1[0]' and TransactionType='PARK' and Date between '$tgla' and '$tglt')
		        									from [dbo].[TransList] where TransactionType='PARK' and Date between '$tgla' and '$tglt'";
		        							$q2 = sqlsrv_query($conn, $s2);
		        							$d2 = sqlsrv_fetch_array($q2);
		        							?>
		        							<tr>
		        								<td>
		        									<a href="<?php if($d2[0] != 0 || $d2[0] != '') { echo '?det=1&loc='.$d1[0].'&tm=1&b='.$bln.'&t='.$thn; } else { echo '#'; } ?>"><?php echo $d1[1]; ?></a>
		        								</td>
		        								<td style="text-align: right;">
		        									<?php 
		        									if($d2[0] == 0 || $d2[0] == '') {
		        										echo number_format('0', 2, ',', '.'); 
		        									}
		        									else {
		        										echo number_format( ($d2[0]*$dps[0]), 2, ',', '.'); 
		        									}
		        									?>		
		        								</td>
		        							</tr>
		        							<?php
		        						}
		        						?>
	        					</tbody>
	        				</table>
	        			</div>
	        			<?php
	        		}
	        		?>
	        	</div>

			</div>
		</div>

		<script type="text/javascript">
			$('#btn_ok').click(function(){
				var bulan = $('#bulan :selected').val();
				var tahun = $('#tahun :selected').val();
				window.location.href = "?tm=1&b="+bulan+"&t="+tahun;
			});
		</script>

	<?php 
	require('footer.php'); 
}