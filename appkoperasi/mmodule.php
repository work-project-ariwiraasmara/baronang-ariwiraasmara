<?php require('header.php');?>

<?php require('sidebar-right.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<div class="animated fadeinup delay-1">
    <div class="page-content">

        <?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
            <div class="notification notification-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
                <!--<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> <i class="icon fa fa-warning ?>"></i>-->
                <h4> <?php echo ucfirst($_SESSION['error-type']); ?></h4>
                <?php echo $_SESSION['error-message']; ?>
            </div>
        <?php } ?>

        <h2 class="uppercase">Tambah EDC</h2>

        <div class="form-inputs">
            <form action="procedc.php" method="post">

                KBA<br>
                <select name="kba" class="browser-default">
                    <?php
                    $sql = "select* from [dbo].[BankAccount]";
                    $exec = sqlsrv_query($conn, $sql);
                    while($row = sqlsrv_fetch_array($exec, SQLSRV_FETCH_NUMERIC)){
                        ?>
                        <option value="<?php echo $row[1]; ?>"><?php echo $row[3].' - '.$row[4]; ?></option>
                    <?php } ?>
                </select>

                <div style="margin-top: 30px;">
                    <button type="submit" class="waves-effect waves-light btn-large primary-color width-100">Simpan</button>
                </div>

            </form>
        </div>

        <hr>

        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Tambah EDC</h3>
            </div>

            <div class="box-body">
                <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-lg-12 table-responsive">
                    <span class="lead">Daftar EDC</span>
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>Serial Number</th>
                            <th>KBA</th>
                            <th>Private Key</th>
                        </tr>
                        <?php
                        $xx = "select* from [Gateway].[dbo].[EDCList] where KID = '$_SESSION[KID]'";
                        $yy = sqlsrv_query($conn, $xx);
                        while($zz = sqlsrv_fetch_array($yy, SQLSRV_FETCH_NUMERIC)){
                            ?>
                            <tr>
                                <td><?php echo $zz[0]; ?></td>
                                <td>
                                    <?php
                                    $s = "select* from [dbo].[BankAccount] where KodeKoperasiBankAccount = '$zz[2]'";
                                    $e = sqlsrv_query($conn, $s);
                                    $r = sqlsrv_fetch_array($e, SQLSRV_FETCH_NUMERIC);
                                    if($r != null){
                                        echo $r[3].' - '.$r[4];
                                    }
                                    ?>
                                </td>
                                <td><?php echo $zz[4]; ?></td>
                            </tr>
                        <?php } ?>
                    </table>
                </div>
            </div>
        </div>

    </div>
</div>

<?php require('footer_new.php');?>