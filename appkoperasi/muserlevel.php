<?php require('header.php');?>

<?php require('sidebar-right.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<?php
//edit
$uledit = $_GET['edit'];
$ul0        = "";
$ul1        = "";
$ulprocedit = "";
$uldisabled = "";
if(!empty($uledit)){
    $eulsql       = "select * from dbo.UserLevel where KodeUserLevel='$uledit'";
    $eulstmt      = sqlsrv_query($conn, $eulsql);
    $eulrow       = sqlsrv_fetch_array( $eulstmt, SQLSRV_FETCH_NUMERIC);
    if(count($eulrow[0]) > 0){
        $ul0        = $eulrow[0];
        $ul1        = $eulrow[1];

        $ulprocedit = "?page=".$_GET['page']."&edit=".$ul0;
        $uldisabled = "readonly";

    }
    else{
        if(empty($_GET['page'])){
            echo "<script language='javascript'>document.location='muserlevel.php';</script>";
        }
        else{
            echo "<script language='javascript'>document.location='muserlevel.php?page=".$_GET['page']."';</script>";
        }
    }
}

?>
<div class="animated fadeinup delay-1">
    <div class="page-content">

        <?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
            <div class="notification notification-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
                <!--<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> <i class="icon fa fa-warning ?>"></i>-->
                <h4> <?php echo ucfirst($_SESSION['error-type']); ?> </h4>
                <?php echo $_SESSION['error-message']; ?>
            </div>
        <?php } ?>

        <h2 class="uppercase"><?php echo lang('Level Pengguna'); ?></h2>

            <form class="form-horizontal" action="proc_muserlevel.php<?=$ulprocedit?>" method="POST">

                <div class="input-field">
                    <input type="text" name="id" class="validate" id="levelid" value="<?=$ul0?>" <?=$uldisabled?>>
                    <label for="levelid"><?php echo lang('Kode Level'); ?></label>
                </div>

                <div class="input-field">
                    <input type="text" name="levelname" class="validate" id="levelname" value="<?=$ul1?>">
                    <label for="levelname"><?php echo lang('Nama Level'); ?></label>
                </div><br>

                <ul class="faq collapsible animated fadeinright delay-1">
                    <?php
                    $lvl1chkno = 0;
                    $a = "select * from dbo.MenuHeader where Status=1 order by HeaderNo asc";
                    $b = sqlsrv_query($conn, $a);
                    while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                        $lvl1chkno++;
                        ?>
                        <li>
                            <div class="collapsible-header">
                                <a href="#<?php echo $c[0]; ?>" data-toggle="tab"> <i class="ion-android-arrow-dropdown right"></i> <?php echo lang($c[1]); ?>  </a>
                            </div>

                            <div class="collapsible-body">
                                    <div class="row">
                                        <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-lg-12 table-responsive">
                                            <table class="table table-bordered">
                                                <tr>
                                                    <?php
                                                    $lvl2chkno = 0;
                                                    $aa = "select * from dbo.MenuDetail where HeaderNo='$c[0]' and Status=1 order by DetailNo asc";
                                                    $bb = sqlsrv_query($conn, $aa);
                                                    while($cc = sqlsrv_fetch_array($bb, SQLSRV_FETCH_NUMERIC)){
                                                    $lvl2chkno++;
                                                        if($cc[5] == 0){
                                                            if(!empty($uledit)){
                                                                $x = "select * from dbo.UserLevelMenu where KodeUserLevel='$uledit' and MenuName='$cc[3]'";
                                                                $y = sqlsrv_query($conn, $x);
                                                                $z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC);
                                                                $le = '';
                                                                $ap = '';
                                                                if($z != NULL){
                                                                    $le = 'checked';

                                                                    if($z[1] == 1){
                                                                        $ap = 'checked';
                                                                    }
                                                                }
                                                            } ?>
                                                            <td>
                                                                <!-- <div class="input-field">
                                                                    <div title="Need Approve SPV ?">
                                                                        <input type="checkbox" name="level[]" id="chk<?php echo $lvl1chkno.$lvl2chkno; ?>" value="1" <?php echo $ap; ?>/>
                                                                        <label for="chk<?php echo $lvl1chkno.$lvl2chkno; ?>"> <i class="ion-android-contact prefix"></i> </label>
                                                                    </div>
                                                                </div> -->
                                                                <div class="input-field" style="margin-top: 10px;">
                                                                    <input type="checkbox" name="menu[]" id="chkmenu<?php echo $cc[3]; ?>" value="<?php echo $cc[3]; ?>" <?php echo $le; ?>>
                                                                    <label for="chkmenu<?php echo $cc[3]; ?>"></label><?php echo lang($cc[2]); ?>
                                                                </div>
                                                            </td>
                                                        <?php
                                                        } else { ?>
                                                            <tr>
                                                                <td><label><?php echo lang($cc[2]); ?></label></td>
                                                                <td>
                                                                    <table class="table table-bordered">
                                                                        <tr>
                                                                            <?php
                                                                            $aaa = "select * from dbo.MenuChild where HeaderNo='$c[0]' and DetailNo='$cc[1]' and Status=1 order by ChildNo asc";
                                                                            $bbb = sqlsrv_query($conn, $aaa);
                                                                            while($ccc = sqlsrv_fetch_array($bbb, SQLSRV_FETCH_NUMERIC)){
                                                                                ?>
                                                                                <?php
                                                                                if(!empty($uledit)){
                                                                                    $xx = "select * from dbo.UserLevelMenu where KodeUserLevel='$uledit' and MenuName='$ccc[4]'";
                                                                                    $yy = sqlsrv_query($conn, $xx);
                                                                                    $zz = sqlsrv_fetch_array($yy, SQLSRV_FETCH_NUMERIC);
                                                                                    $lle = '';
                                                                                    $aap = '';
                                                                                    if($zz != NULL){
                                                                                        $lle = 'checked';

                                                                                        if($zz[1] == 1){
                                                                                            $aap = 'checked';
                                                                                        }
                                                                                    }
                                                                                }
                                                                                ?>
                                                                                <td>
                                                                                    <input type="checkbox" name="level[]" class="minimal" value="1" <?php echo $aap; ?>>
                                                                                    <input type="checkbox" name="menu[]" id="chk<?php echo $ccc[4]; ?>" value="<?php echo $ccc[4]; ?>" <?php echo $lle; ?>>
                                                                                    <label for="chk<?php echo $ccc[4]; ?>"><?php echo lang($ccc[3]); ?></label>
                                                                                </td>
                                                                            <?php } ?>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr> <?php
                                                        }
                                                    } ?>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                            </div>
                        </li>
                    <?php
                    } ?>
                </ul>

                <div style="margin-top: 30px;">
                    <?php if(count($eulrow[0]) > 0){ ?>
                        <a href="muserlevel.php" class="waves-effect waves-light btn-large primary-color width-100"><?php echo lang('Batal'); ?></a>
                    <?php } ?>
                </div>

                <div style="margin-top: 10px; margin-bottom: 30px;">
                    <?php if(count($eulrow[0]) > 0){ ?>
                        <button type="submit" class="waves-effect waves-light btn-large primary-color width-100"><?php echo lang('Perbaharui'); ?></button>
                    <?php }else{ ?>
                        <button type="submit" class="waves-effect waves-light btn-large primary-color width-100"><?php echo lang('Simpan'); ?></button>
                    <?php } ?>
                </div>

            </form>


        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title"><?php echo lang('Daftar Level Pengguna'); ?></h3>
            </div>
            <div class="table-responsive">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th class=""><?php echo lang('No'); ?></th>
                        <th class="-"><?php echo lang('Kode Level'); ?></th>
                        <th class=""><?php echo lang('Nama Level'); ?></th>
                        <th class=""><?php echo lang('Aksi'); ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    //count
                    $jmlulsql   = "select count(*) from dbo.UserLevel";
                    $jmlulstmt  = sqlsrv_query($conn, $jmlulsql);
                    $jmlulrow   = sqlsrv_fetch_array( $jmlulstmt, SQLSRV_FETCH_NUMERIC);

                    //pagging
                    $perpages   = 10;
                    $halaman    = $_GET['page'];
                    if(empty($halaman)){
                        $posisi  = 0;
                        $batas   = $perpages;
                        $halaman = 1;
                    }
                    else{
                        $posisi  = (($perpages * $halaman) - 10) + 1;
                        $batas   = ($perpages * $halaman);
                    }

                    $ulsql = "SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY KodeUserLevel asc) as row FROM [dbo].[UserLevel]) a WHERE row between '$posisi' and '$batas'";
                    $ulstmt = sqlsrv_query($conn, $ulsql);
                    while($ulrow = sqlsrv_fetch_array( $ulstmt, SQLSRV_FETCH_NUMERIC)){
                        ?>
                        <tr>
                            <td><?=$ulrow[2];?></td>
                            <td><?=$ulrow[0];?></td>
                            <td><?=$ulrow[1];?></td>
                            <td width="20%" style="padding: 3px">
                                <div class="btn-group" style="padding-right: 15px">
                                    <a href="muserlevel.php?page=<?=$halaman?>&edit=<?=$ulrow[0]?>" class="btn btn-default btn-flat btn-sm text-green" title="edit"><i class="ion-android-create"></i></a>
                                    <a href="#" class="btn btn-default btn-flat btn-sm btn-detail<?php echo $ulrow[0]; ?>" title="show detail"><i class="fa fa-arrows-v"></i> </a>
                                    <a href="#" class="hide btn btn-default btn-flat btn-sm btn-close<?php echo $ulrow[0]; ?>" title="close detail"><i class="fa fa-arrows"></i> </a>
                                </div>
                            </td>
                        </tr>
                        <tr class="t<?php echo $ulrow[0]; ?> hide">
                            <td colspan="4" class="d<?php echo $ulrow[0]; ?>">

                            </td>
                        </tr>
                        <script type="text/javascript">
                            $('.btn-detail<?php echo $ulrow[0]; ?>').click(function(){
                                $('.t<?php echo $ulrow[0]; ?>').removeClass('hide');
                                $.ajax({
                                    url : "ajax_userlevel.php",
                                    type : 'POST',
                                    data: { param: '<?php echo $ulrow[0]; ?>' },
                                    success : function(data) {
                                        $(".d<?php echo $ulrow[0]; ?>").html(data);
                                    },
                                    error : function(){
                                        alert('<?php echo lang('Silahkan coba lagi'); ?>';
                                        return false;
                                    }
                                });

                                $('.btn-close<?php echo $ulrow[0]; ?>').removeClass('hide');
                                $('.btn-detail<?php echo $ulrow[0]; ?>').addClass('hide');
                            });

                            $('.btn-close<?php echo $ulrow[0]; ?>').click(function(){
                                $('.t<?php echo $ulrow[0]; ?>').addClass('hide');
                                $(".t<?php echo $ulrow[0]; ?>").html('');
                                $('.btn-close<?php echo $ulrow[0]; ?>').addClass('hide');
                                $('.btn-detail<?php echo $ulrow[0]; ?>').removeClass('hide');
                            });
                        </script>
                        <?php
                    }

                    $jmlhalaman = ceil($jmlulrow[0]/$perpages);
                    ?>
                    </tbody>
                </table>
            </div>
            <div class="box-footer clearfix right">
                <div style="text-align: center;">
                    <?php echo lang('Menampilkan'); ?> <?=$halaman?> to <?=$jmlhalaman?> of <?=$jmlulrow[0]?> <?php echo lang('Masukan'); ?> <?=$posisi." - ".$batas?>
                    <?php
                    $reload = "muserlevel.php?";
                    $page = intval($_GET["page"]);
                    $adjacents = isset($_GET['adjacents']) ? intval($_GET['adjacents']) : 2; // khusus style pagination 2 dan 3
                    if( $page == 0 ) $page = 1;

                    $tpages = ($jmlhalaman) ? ceil($jmlhalaman) : 0; // total pages, last page number
                    echo "<div>".paginate($reload, $page, $tpages)."</div>";
                    ?>
                </div>
            </div>
        </div>

    </div>
</div>

<?php
function paginate($reload, $page, $tpages) {

    $firstlabel = "First";
    $prevlabel  = "Prev";
    $nextlabel  = "Next";
    $lastlabel  = "Last";

    $out = "<div class=\"backpaging\"><ul class=\"ownpaging\">\n";

    // first
    if($page>1) {
        $out.= "<li><a href=\"" . $reload . "\">" . $firstlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $firstlabel . "</a></li>\n";
    }

    // previous
    if($page==1) {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $prevlabel . "</a></li>\n";
    }
    elseif($page==2) {
        $out.= "<li><a href=\"" . $reload . "\">" . $prevlabel . "</a></li>\n";
    }
    else {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" . ($page-1) . "\">" . $prevlabel . "</a></li>\n";
    }

    // current
    $out.= "<li class=\"active\"><a href=\"#\">Page " . $page . " of " . $tpages . "</a></li>\n";

    // next
    if($page<$tpages) {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" .($page+1) . "\">" . $nextlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $nextlabel . "</a></li>\n";
    }

    // last
    if($page<$tpages) {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" . $tpages . "\">" . $lastlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $lastlabel . "</a></li>\n";
    }

    $out.= "</ul></div>";

    return $out;
}

require('footer_new.php');
?>
