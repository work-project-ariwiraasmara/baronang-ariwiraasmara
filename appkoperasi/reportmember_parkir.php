<?php
@session_start();
error_reporting(0);

if( isset($_GET['download']) && isset($_GET['member']) ) {
    $fdm = date( 'Y-m', strtotime(@$_GET['tgl']) ).'-1';
    $ldm = date( 'Y-m-t', strtotime(@$_GET['tgl']) );

    $filePath = "uploads/excel/";

    require('connect.php');
    require('lib/phpexcel/Classes/PHPExcel.php');

    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getProperties()->setCreator("Baronang");
    $objPHPExcel->getProperties()->setTitle("Laporan Usage Member Kendaraan Parkir");

    // set autowidth
    for($col = 'A'; $col !== 'Z'; $col++) {
        $objPHPExcel->getActiveSheet()
            ->getColumnDimension($col)
            ->setAutoSize(true);
    }

    //sheet 2
    $objWorkSheet = $objPHPExcel->createSheet(0);

    $sloc = "SELECT LocationID, Nama from [dbo].[LocationMerchant] where LocationID='$_GET[loc]'";
    $qloc = sqlsrv_query($conn, $sloc);
    $dloc = sqlsrv_fetch_array($qloc);

    // baris judul
    $objWorkSheet->getStyle('A1')->getFont()->setSize(20);
    $objWorkSheet->getStyle('A1')->getFont()->setBold(true);
    $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:H1');
    $objWorkSheet->SetCellValue('A1', 'Laporan Usage Member Kendaraan Parkir');
    $objWorkSheet->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    if( isset($_GET['summary']) ) {

        $objWorkSheet->getStyle('A4')->getFont()->setBold(true);
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A4:H4');
        $objWorkSheet->SetCellValue('A4', 'Summary');

        $objWorkSheet->getStyle('A5')->getFont()->setBold(true);
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A5:H5');
        $objWorkSheet->SetCellValue('A5', 'Sky Parking @ '.$dloc[1]);

        $objWorkSheet->getStyle('A6')->getFont()->setBold(true);
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A6:H6');
        $objWorkSheet->SetCellValue('A6', date('F Y', strtotime(@$_GET['tgl'])) );

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
        $objWorkSheet->getStyle('A7')->getFont()->setBold(true);
        $objWorkSheet->SetCellValue('A7', 'No.');
        $objWorkSheet->getStyle('A7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
        $objWorkSheet->getStyle('B7')->getFont()->setBold(true);
        $objWorkSheet->SetCellValue('B7', 'No. Kartu');

        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(50);
        $objWorkSheet->getStyle('C7')->getFont()->setBold(true);
        $objWorkSheet->SetCellValue('C7', 'Nama');

        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $objWorkSheet->getStyle('D7')->getFont()->setBold(true);
        $objWorkSheet->SetCellValue('D7', 'Jumlah');
        $objWorkSheet->getStyle('D7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        $objWorkSheet->getStyle('E7')->getFont()->setBold(true);
        $objWorkSheet->SetCellValue('E7', 'Durasi (Menit)');
        $objWorkSheet->getStyle('E7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
        $objWorkSheet->getStyle('F7')->getFont()->setBold(true);
        $objWorkSheet->SetCellValue('F7', 'Rata-Rata (Menit)');
        $objWorkSheet->getStyle('F7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(30);
        $objWorkSheet->getStyle('G7')->getFont()->setBold(true);
        $objWorkSheet->SetCellValue('G7', 'Kendaraan');

        $row = 8;
        $no = 1;
        $sd = " SELECT DISTINCT a.MemberID, 
                                a.CardNo,
                                b.Name,
                                (SELECT Count(CardNo) from [dbo].[ParkingList] where CardNo = a.CardNo and TimeIn BETWEEN '$fdm 00:00:00' and '$ldm 23:59:59' and LocationIn='$_GET[loc]') as jml,
                                (SELECT Sum(DATEDIFF(Minute, '$fdm 00:00:00', '$ldm 23:59:59')) from [dbo].[ParkingList] where CardNo = a.CardNo and TimeIn BETWEEN '$fdm 00:00:00' and '$ldm 23:59:59' and LocationIn = '$_GET[loc]' ) as sumtot,
                                d.Name
                from [dbo].[MemberCard] as a 
                inner join [dbo].[MemberList] as b 
                    on a.MemberID = b.MemberID
                inner join [dbo].[ParkingList] as c 
                    on a.CardNo = c.CardNo 
                inner join [dbo].[VehicleType] as d
                    on c.VehicleType = d.Code
                where   a.CardNo != '' and 
                        c.TimeIn BETWEEN '$fdm 00:00:00' and '$ldm 23:59:59' and
                        c.TimeIn != '' and 
                        c.TimeOut != '' and 
                        c.LocationIn='$_GET[loc]' 
                order by a.MemberID ASC";

        $qd = sqlsrv_query($conn, $sd);
        while( $dd = sqlsrv_fetch_array($qd) ) {
            $rata2 = $dd[4] / $dd[3];

            $objWorkSheet->SetCellValue('A'.$row, $no);
            $objWorkSheet->getStyle('A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            
            $objPHPExcel->getActiveSheet()
                        ->getStyle('B'.$row)
                        ->getNumberFormat()
                        ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
            $objWorkSheet->SetCellValue('B'.$row, ' '.$dd[1]);

            $objWorkSheet->SetCellValue('C'.$row, $dd[2]);

            $objWorkSheet->SetCellValue('D'.$row, number_format($dd[3], 2, ",", "."));
            $objWorkSheet->getStyle('D'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

            $objWorkSheet->SetCellValue('E'.$row, number_format($dd[4], 2, ",", "."));
            $objWorkSheet->getStyle('E'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

            $objWorkSheet->SetCellValue('F'.$row, number_format($rata2, 2, ",", "."));
            $objWorkSheet->getStyle('F'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

            $objWorkSheet->SetCellValue('G'.$row, $dd[5]);

                
            $objWorkSheet->SetCellValue('A'.$row, $no);
            $objWorkSheet->getStyle('A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            
            $row++; $no++;
        }
        

        //exit;
        $objWorkSheet->setTitle('Member Parkir '.date('d F Y', strtotime($fdm)) );

        $fileName = 'Summary Member Parkir '.date('d F Y', strtotime($fdm)).'.xls';
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');

    }
    else {

        $objWorkSheet->getStyle('A4')->getFont()->setBold(true);
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A4:I4');
        $objWorkSheet->SetCellValue('A4', 'Sky Parking @ '.$dloc[1]);

        $objWorkSheet->getStyle('A5')->getFont()->setBold(true);
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A5:I5');
        $objWorkSheet->SetCellValue('A5', date('F Y', strtotime(@$_GET['tgl'])) );

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
        $objWorkSheet->getStyle('A6')->getFont()->setBold(true);
        $objWorkSheet->SetCellValue('A6', 'No.');
        $objWorkSheet->getStyle('A6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(12);
        $objWorkSheet->getStyle('B6')->getFont()->setBold(true);
        $objWorkSheet->SetCellValue('B6', 'Tanggal');


        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
        $objWorkSheet->getStyle('C6')->getFont()->setBold(true);
        $objWorkSheet->SetCellValue('C6', 'No. Kartu');

        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(50);
        $objWorkSheet->getStyle('D6')->getFont()->setBold(true);
        $objWorkSheet->SetCellValue('D6', 'Nama');

        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(7);
        $objWorkSheet->getStyle('E6')->getFont()->setBold(true);
        $objWorkSheet->SetCellValue('E6', 'In');

        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(7);
        $objWorkSheet->getStyle('F6')->getFont()->setBold(true);
        $objWorkSheet->SetCellValue('F6', 'Out');

        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(40);
        $objWorkSheet->getStyle('G6')->getFont()->setBold(true);
        $objWorkSheet->SetCellValue('G6', 'Gate In');

        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(40);
        $objWorkSheet->getStyle('H6')->getFont()->setBold(true);
        $objWorkSheet->SetCellValue('H6', 'Gate Out');

        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(12);
        $objWorkSheet->getStyle('I6')->getFont()->setBold(true);
        $objWorkSheet->SetCellValue('I6', 'Kendaraan');

        $row = 7;
        $no = 1;
        $sql = "SELECT TOP 300 a.MemberID, a.CardNo, b.Name, c.TimeIn, c.TimeOut, c.LocationIn, c.LocationOut, d.Name from [dbo].[MemberCard] as a 
                inner join [dbo].[MemberList] as b 
                    on a.MemberID = b.MemberID
                inner join [dbo].[ParkingList] as c 
                    on a.CardNo = c.CardNo
                inner join [dbo].[VehicleType] as d 
                    on c.VehicleType = d.Code 
                where c.LocationIn = '$_GET[loc]' and b.KID = '700097' and a.MemberID != '' and a.Status='1' and a.isLink='1' and c.TimeIn BETWEEN '$fdm 00:00:00' and '$ldm 23:59:59' and c.TimeIn != '' and c.TimeOut != ''
                order by c.TimeIn ASC, a.MemberID ASC, b.Name ASC";
        //echo $sql;
        $query = sqlsrv_query($conn, $sql);
        while($data = sqlsrv_fetch_array($query)) {

            $slin = "SELECT Nama from [dbo].[LocationMerchant] where LocationID='".$data[5]."'";
            $qlin = sqlsrv_query($conn, $slin);
            $dlin = sqlsrv_fetch_array($qlin);

            $slout = "SELECT Nama from [dbo].[LocationMerchant] where LocationID='".$data[6]."'";
            $qlout = sqlsrv_query($conn, $slout);
            $dlout = sqlsrv_fetch_array($qlout);

            $objWorkSheet->SetCellValue('A'.$row, $no);
            $objWorkSheet->getStyle('A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            $objWorkSheet->SetCellValue('B'.$row,  $data[3]->format('d-m-Y'));

            $objPHPExcel->getActiveSheet()
                        ->getStyle('C'.$row)
                        ->getNumberFormat()
                        ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
            $objWorkSheet->SetCellValue('C'.$row, $data[1]);
            
            $objWorkSheet->SetCellValue('D'.$row, $data[2]);
            

            if( empty($data[3]) || is_null($data[3]) || $data[3] == '' ) {
                $objWorkSheet->SetCellValue('E'.$row, '00:00:00');
            }
            else {
                $objWorkSheet->SetCellValue('E'.$row, $data[3]->format('H:m'));
            }

            if( empty($data[4]) || is_null($data[4]) || $data[4] == '' ) {
                $objWorkSheet->SetCellValue('F'.$row, '00:00:00');
            }
            else {
                $objWorkSheet->SetCellValue('F'.$row, $data[4]->format('H:m'));
            }

            $objWorkSheet->SetCellValue('G'.$row, $dlin[0]);

            $objWorkSheet->SetCellValue('H'.$row, $dlout[0]);

            $objWorkSheet->SetCellValue('I'.$row, $data[7]);

            $row++; $no++;
        }

        //exit;
        $objWorkSheet->setTitle('Member Parkir '.date('d F Y', strtotime($fdm)) );

        $fileName = 'Member Kendaraan Parkir '.date('d F Y', strtotime($fdm)).'.xls';
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');

    }

    // download ke client
    header('Content-type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename="'.$fileName.'"');
    $objWriter->save('php://output');

    return $filePath.'/'.$fileName;
}
else {
    require('header.php');
    require('sidebar-right.php');
    require('sidebar-left.php');

    if( !isset($_GET['tgl']) ) {
    	$fdm = date('Y-m').'-1';
    	$ldm = date('Y-m-t');
    }
    else {
    	$fdm = date( 'Y-m', strtotime(@$_GET['tgl']) ).'-1';
    	$ldm = date( 'Y-m-t', strtotime(@$_GET['tgl']) );
    }

    $yesterday = date('Y-m-t', strtotime('-1 month', strtotime($fdm) ) );
    $tomorrow = date('Y-m-t', strtotime('+1 month', strtotime($fdm) ) );

    //echo $fdm.' :: '.$ldm.'<br>'.$yesterday.' -- '.$tomorrow;

    function paginate($reload, $page, $tpages) {

        $firstlabel = "First";
        $prevlabel  = "Prev";
        $nextlabel  = "Next";
        $lastlabel  = "Last";

        $out = "<div class=\"backpaging\"><ul class=\"ownpaging\">\n";

        // first
        if($page>1) {
            $out.= "<li><a href=\"" . $reload . "\">" . $firstlabel . "</a></li>\n";
        }
        else {
            $out.= "<li class=\"disabledd\"><a href=\"#\">" . $firstlabel . "</a></li>\n";
        }

        // previous
        if($page==1) {
            $out.= "<li class=\"disabledd\"><a href=\"#\">" . $prevlabel . "</a></li>\n";
        }
        elseif($page==2) {
            $out.= "<li><a href=\"" . $reload . "\">" . $prevlabel . "</a></li>\n";
        }
        else {
            $out.= "<li><a href=\"" . $reload . "&amp;page=" . ($page-1) . "\">" . $prevlabel . "</a></li>\n";
        }

        // current
        $out.= "<li class=\"active\"><a href=\"#\">Page " . $page . " of " . $tpages . "</a></li>\n";

        // next
        if($page<$tpages) {
            $out.= "<li><a href=\"" . $reload . "&amp;page=" .($page+1) . "\">" . $nextlabel . "</a></li>\n";
        }
        else {
            $out.= "<li class=\"disabledd\"><a href=\"#\">" . $nextlabel . "</a></li>\n";
        }

        // last
        if($page<$tpages) {
            $out.= "<li><a href=\"" . $reload . "&amp;page=" . $tpages . "\">" . $lastlabel . "</a></li>\n";
        }
        else {
            $out.= "<li class=\"disabledd\"><a href=\"#\">" . $lastlabel . "</a></li>\n";
        }

        $out.= "</ul></div>";

        return $out;
    }

    function paginate_one($reload, $page, $tpages, $adjacents) {
    
        $firstlabel = "<b>1</b>";
        $prevlabel  = "<b>Prev</b>";
        $nextlabel  = "<b>Next</b>";
        $lastlabel  = "<b>$tpages</b>";
        
        $out = "<div class=\"m-t-10 m-b-10 center backpaging\"><ul class=\"ownpaging\">\n";
        
        // first
        if($page>1) {
            $out.= "<li><a href=\"" . $reload . "\">" . $firstlabel . "</a></li>\n";
        }
        else {
            $out.= "<li class=\"disabledd\"><a href=\"#\">" . $firstlabel . "</a></li>\n";
        }
        
        // previous
        if($page==1) {
            $out.= "<li class=\"disabledd\"><a href=\"#\">" . $prevlabel . "</a></li>\n";
        }
        elseif($page==2) {
            $out.= "<li><a href=\"" . $reload . "\">" . $prevlabel . "</a></li>\n";
        }
        else {
            $out.= "<li><a href=\"" . $reload . "&amp;hlm=" . ($page-1) . "\">" . $prevlabel . "</a></li>\n";
        }
        
        // current
        $out.= "<li class=\"active\"><a href=\"#\">Page " . $page . " of " . $tpages . "</a></li>\n";
        
        // next
        if($page<$tpages) {
            $out.= "<li><a href=\"" . $reload . "&amp;hlm=" .($page+1) . "\">" . $nextlabel . "</a></li>\n";
        }
        else {
            $out.= "<li class=\"disabledd\"><a href=\"#\">" . $nextlabel . "</a></li>\n";
        }
        
        // last
        if($page<$tpages) {
            $out.= "<li><a href=\"" . $reload . "&amp;hlm=" . $tpages . "\">" . $lastlabel . "</a></li>\n";
        }
        else {
            $out.= "<li class=\"disabledd\"><a href=\"#\">" . $lastlabel . "</a></li>\n";
        }
        
        $out.= "</ul></div>";
        
        return $out;
    }
    ?>

    	<div class="animated fadeinup delay-1">
            <div class="page-content txt-black">

            <h1 class="uppercase">Laporan Usage Member Kendaraan Parkir</h1><br>
    			
            <?php
            if( isset($_GET['summary']) && isset($_GET['member']) && isset($_GET['loc']) ) { 
                $sloc = "SELECT LocationID, Nama from [dbo].[LocationMerchant] where LocationID='$_GET[loc]'";
                $qloc = sqlsrv_query($conn, $sloc);
                $dloc = sqlsrv_fetch_array($qloc);
                ?>
                <h3 class="uppercase">Summary</h3><br>
            	<div class="table-responsive m-t-30">
            		<table class="width-100">
            			<thead>
                            <tr>
                                <th colspan="5">
                                    <?php 
                                        echo 'Sky Parking @ '.$dloc[1].'<br>';
                                        echo date('F Y', strtotime($_GET['tgl'])); 
                                    ?>
                                </th>

                                <th colspan="2" style="text-align: right;">
                                    <a href="?download=1&summary=1&member=1&tgl=<?php echo $_GET['tgl']; ?>&loc=<?php echo $_GET['loc']; ?>" target="_blank" class="btn primary-color waves-effect waves-light">Download</a>
                                </th>
                            </tr>

            				<tr>
            					<th style="text-align: right;">No.</th>
            					<th>Nomor Kartu</th>
            					<th>Nama</th>
            					<th style="text-align: right;">Jumlah</th>
            					<th style="text-align: right;">Durasi<br>(Menit)</th>
            					<th style="text-align: right;">Rata-Rata<br>(Menit)</th>
                                <th>Kendaraan</th>
            				</tr>
            			</thead>

            			<tbody>
            				<?php
            				$no = 1;

                            $sd = " SELECT DISTINCT a.MemberID, 
                                                    a.CardNo,
                                                    b.Name,
                                                    (SELECT Count(CardNo) from [dbo].[ParkingList] where CardNo = a.CardNo and TimeIn BETWEEN '$fdm 00:00:00' and '$ldm 23:59:59' and LocationIn='$_GET[loc]') as jml,
                                                    (SELECT Sum(DATEDIFF(Minute, '$fdm 00:00:00', '$ldm 23:59:59')) from [dbo].[ParkingList] where CardNo = a.CardNo and TimeIn BETWEEN '$fdm 00:00:00' and '$ldm 23:59:59' and LocationIn = '$_GET[loc]' ) as sumtot,
                                                    d.Name
                                    from [dbo].[MemberCard] as a 
                                    inner join [dbo].[MemberList] as b 
                                        on a.MemberID = b.MemberID
                                    inner join [dbo].[ParkingList] as c 
                                        on a.CardNo = c.CardNo 
                                    inner join [dbo].[VehicleType] as d
                                        on c.VehicleType = d.Code
                                    where   a.CardNo != '' and 
                                            c.TimeIn BETWEEN '$fdm 00:00:00' and '$ldm 23:59:59' and
                                            c.TimeIn != '' and 
                                            c.TimeOut != '' and 
                                            c.LocationIn='$_GET[loc]' 
                                    order by a.MemberID ASC";

            				$qd = sqlsrv_query($conn, $sd);
            				while( $dd = sqlsrv_fetch_array($qd) ) { 

                                $rata2 = $dd[4] / $dd[3];
    	        				?>
    	        				<tr>
    		        				<td style="text-align: right;"><?php echo $no.'. '; ?></td>
    		        				<td><?php echo $dd[1]; ?></td>
    		        				<td><?php echo $dd[2]; ?></td>
    		        				<td style="text-align: right;"><?php echo number_format($dd[3], 2, ",", "."); ?></td>
    		        				<td style="text-align: right;"><?php echo number_format($dd[4], 2, ",", "."); ?> </td>
    		        				<td style="text-align: right;"><?php echo number_format($rata2, 2, ",", "."); ?></td>
    		        				<td><?php echo $dd[5]; ?></td> 
                                </tr>
    	        				<?php
    	        				$no++;

            				}
                            
            				?>
            			</tbody>
            		</table>
            	</div>
            	<?php
            }
            else { 
                if(isset($_GET['member']) && isset($_GET['loc'])) { 
                    $sloc = "SELECT LocationID, Nama from [dbo].[LocationMerchant] where LocationID='$_GET[loc]'";
                    $qloc = sqlsrv_query($conn, $sloc);
                    $dloc = sqlsrv_fetch_array($qloc);
                    ?>
                	<div class="table-responsive m-t-30">
                		<table class="width-100">
                			<thead>
                                <tr>
                                    <th colspan="3"></th>
                                    <th colspan="3" style="text-align: center;">
                                        Sky Parking @ <?php echo $dloc[1] ?><br>

                                        <a href="?tgl=<?php echo $yesterday; ?>" class="left">
                                            <i class="ion-ios-arrow-back" style="margin-top: 15px;"></i><i class="ion-ios-arrow-back" style="margin-top: 15px;"></i>
                                        </a>

                                        <?php echo date('F Y', strtotime($_GET['tgl'])); ?>

                                        <a href="?tgl=<?php echo $tomorrow; ?>" class="right">
                                            <i class="ion-ios-arrow-forward" style="margin-top: 15px;"></i><i class="ion-ios-arrow-forward" style="margin-top: 15px;"></i>
                                        </a>
                                    </th>
                                    <th colspan="1"></th>

                                    <th colspan="2" style="text-align: right;">
                                        <a href="?summary=1&member=1&tgl=<?php echo $_GET['tgl']; ?>&loc=<?php echo $_GET['loc']; ?>" class="btn primary-color waves-effect waves-light" style="margin-right: 5px; margin-left: 5px;">
                                            Summary
                                        </a>

                                        <a href="?download=1&member=1&tgl=<?php echo $_GET['tgl']; ?>&loc=<?php echo $_GET['loc']; ?>" target="_blank" class="btn primary-color waves-effect waves-light" style="margin-right: 5px; margin-left: 5px;">
                                            Download
                                        </a>
                                    </th>
                                </tr>
                				<tr>
                					<th style="text-align: right;">No.</th>
                					<th>Tanggal</th>
                					<th>No. Kartu</th>
                					<th>Nama</th>
                					<th>In</th>
                					<th>Out</th>
                					<th>Gate In</th>
                					<th>Gate Out</th>
                                    <th>Kendaraan</th>
                				</tr>
                			</thead>

                			<tbody>
                				<?php
                				$sql = "SELECT  ROW_NUMBER() OVER (ORDER BY c.TimeIn asc) as rowss,
                                                a.MemberID, 
                                                a.CardNo, 
                                                b.Name, 
                                                c.TimeIn, 
                                                c.TimeOut, 
                                                c.LocationIn, 
                                                c.LocationOut, 
                                                d.Name 
                                        from [dbo].[MemberCard] as a 
        								inner join [dbo].[MemberList] as b 
        									on a.MemberID = b.MemberID
        								inner join [dbo].[ParkingList] as c 
        									on a.CardNo = c.CardNo
                                        inner join [dbo].[VehicleType] as d 
                                            on c.VehicleType = d.Code 
        								where c.LocationIn = '$_GET[loc]' and b.KID = '700097' and a.MemberID != '' and a.Status='1' and a.isLink='1' and c.TimeIn BETWEEN '$fdm 00:00:00' and '$ldm 23:59:59' and c.TimeIn != '' and c.TimeOut != ''";
                				//echo $sql.'<br>';
        						//$query = sqlsrv_query($conn, $sql);   
                                $no = 1;

                                $params = array();
                                $options =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );
                                
                                $jmlulsql   = "SELECT count(*) FROM (".$sql.") a";
                                echo $jmlulsql;
                                $jmlulstmt  = sqlsrv_query($conn, $jmlulsql, $params, $options);

                                
                                $jmlulrow   = sqlsrv_fetch_array( $jmlulstmt, SQLSRV_FETCH_NUMERIC);

        						//pagging
                                $perpages   = 10;
                                $halaman    = $_GET['page'];
                                if(empty($halaman)){
                                    $posisi  = 0;
                                    $batas   = $perpages;
                                    $halaman = 1;
                                }
                                else{
                                    $posisi  = (($perpages * $halaman) - 10) + 1;
                                    $batas   = ($perpages * $halaman);
                                }

                                $a = "SELECT * FROM ( ".$sql." ) a WHERE rowss between '$posisi' and '$batas'";
                                echo $a;
                                $params2 = array();
                                $options2 =  array( "Scrollable" => SQLSRV_CURSOR_KEYSET );

                                $b = sqlsrv_query($conn, $a, $params2, $options2);

                				while($data = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)) {

                					$slin = "SELECT Nama from [dbo].[LocationMerchant] where LocationID='".$data[5]."'";
                					$qlin = sqlsrv_query($conn, $slin);
                					$dlin = sqlsrv_fetch_array($qlin);

                					$slout = "SELECT Nama from [dbo].[LocationMerchant] where LocationID='".$data[6]."'";
                					$qlout = sqlsrv_query($conn, $slout);
                					$dlout = sqlsrv_fetch_array($qlout);
                					?>
                					<tr>
                						<td style="text-align: right;"><?php echo $no.'. '; ?></td>
                						<td><?php echo $data[3]->format('d-m-Y'); ?></td>
                						<td><?php echo $data[1]; ?></td>
                						<td><?php echo $data[2]; ?></td>
                						<td>
                							<?php 
                							if( empty($data[3]) || is_null($data[3]) || $data[3] == '' ) {
                								echo '00:00:00';
                							}
                							else {
                								echo $data[3]->format('H:m'); 
                							}
                							?>
                						</td>
                						<td>
                							<?php 
                							if( empty($data[4]) || is_null($data[4]) || $data[4] == '' ) {
                								echo '00:00:00';
                							}
                							else {
                								echo $data[4]->format('H:m'); 
                							}
                							?>
                						</td>
                						<td><?php echo $dlin[0]; //echo $data[5]; ?></td>
                						<td><?php echo $dlout[0]; //echo $data[6]; ?></td>
                                        <td><?php echo $data[7]; ?></td>
                					</tr>
                					<?php
                					$no++;
                                    
                				}

                                $jmlhalaman = ceil($jmlulrow[0]/$perpages);
                				?>
                			</tbody>

                			<tfoot>
                				<tr>
                					

                                    
                					<td colspan="9" style="text-align: center;">
                                        
                						<div style="text-align: center;">
                                            Showing <?=$halaman?> to <?=$jmlhalaman?> of <?=$jmlulrow[0]?> entries <?=$posisi." - ".$batas?>
                                            <?php
                                            $acc2 = @$_GET['acc2'];
                                            $st = @$_GET['st'];
                                            $pagem = @$_GET['pagem'];
                                            $acc = @$_GET['acc'];
                                            $reload = "reportmember_parkir.php?membe=1&tgl=".@$_GET['tgl']."&loc=".@$_GET['loc']."&acc2=$acc2&st=$st&pagem=$pagem&acc=$acc";
                                            $page = intval($_GET["page"]);
                                            $adjacents = isset($_GET['adjacents']) ? intval($_GET['adjacents']) : 2; // khusus style pagination 2 dan 3
                                            if( $page == 0 ) $page = 1;
                                                $tpages = ($jmlhalaman) ? ceil($jmlhalaman) : 0; // total pages, last page number
                                                echo "<div>".paginate($reload, $page, $tpages)."</div>";
                                            
                                            ?>
                                        </div>
                                        
                					</td>
                                    

                					
                				</tr>
                                
                			</tfoot>
                		</table>
                	</div>
            	<?php 
                }
                else { ?>
                    <div class="row">
                        <div class="col s3">
                            <b>Lokasi</b> : 
                            <select class="browser-default" id="loc">
                                <?php
                                $sq_ioqty = "SELECT LocationID, Nama from [dbo].[LocationMerchant] order by Nama ASC";
                                $qr_ioqty = sqlsrv_query($conn, $sq_ioqty);
                                while($dr_ioqty = sqlsrv_fetch_array($qr_ioqty)) { ?>
                                    <option value="<?php echo $dr_ioqty[0]; ?>"><?php echo $dr_ioqty[1]; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>

                        <div class="col s3">
                            <b>Bulan</b> : <br>
                            <select class="browser-default" id="tgl_bulan">
                                <?php
                                for($x = 1; $x < 13; $x++) { 
                                    $time = date('Y').'-'.$x.'-1';
                                    ?>
                                    <option value="<?php echo date('m-t', strtotime($time) ); ?>"><?php echo date('F', strtotime($time) ); ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>

                        <div class="col s3">
                            <b>Tahun</b> : <br>
                            <select class="browser-default" id="tgl_tahun">
                                <?php
                                for($x = date('Y'); $x >= 2016; $x--) { ?>
                                    <option value="<?php echo $x; ?>"><?php echo $x; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>

                    <button type="button" name="btn_ok" id="btn_ok" class="btn btn-large width-100 primary-color waves-efeect waves-light m-t-30">OK</button>
                    <script type="text/javascript">
                        $('#btn_ok').click(function(){
                            var loc = $('#loc :selected').val();
                            var tgl = $('#tgl_tahun').val() + "-" + $('#tgl_bulan').val();
                            window.location.href = "?member=1&tgl=" + tgl + "&loc=" + loc;
                        });
                    </script>
                    <?php
                }
        	}
        	?>

    		</div>
    	</div>
        
    <?php 
    require('footer.php'); 
}
?>
