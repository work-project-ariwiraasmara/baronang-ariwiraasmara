<?php
//error_reporting(0);
include "connect.php";

$KID = $_SESSION['KID'];
$NamaKop = $_SESSION['NamaKoperasi'];

$UserID = $_SESSION['UserID'];
$NameUser = $_SESSION['Name'];
$JabatanUser = $_SESSION['KodeJabatan'];

$filepath = basename($_SERVER['REQUEST_URI'], '?' . $_SERVER['QUERY_STRING']);
$_SESSION['FileAkses'] = $filepath;

$FileAkses = $_SESSION['FileAkses'];

$y = "select top 1 * from [dbo].[Profil]";
$n = sqlsrv_query($conn, $y);
$m = sqlsrv_fetch_array( $n, SQLSRV_FETCH_NUMERIC);
if($m != null){
    $_SESSION['Logo'] = $m[6];
    if($m[1] != ''){
        $_SESSION['NamaKoperasi'] = $m[1];
    }
}

if($UserID == "" || $NameUser == "" || $JabatanUser == ""){
    echo "<script language='javascript'>document.location='login.php';</script>";
}
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Baronang</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="IE=edge" http-equiv="x-ua-compatible">
    <meta content="initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" name="viewport">
    <meta content="yes" name="apple-mobile-web-app-capable">
    <meta content="yes" name="apple-touch-fullscreen">

    <link href='static/images/Logo-fish-icon.png' type='image/x-icon'/>
    <link href='static/images/Logo-fish-icon.png' rel='shortcut icon'/>

    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
    <!-- Icons -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" media="all" rel="stylesheet" type="text/css">
    <!-- Styles -->
    <link href="css/keyframes.css" rel="stylesheet" type="text/css">
    <link href="css/materialize.min.css" rel="stylesheet" type="text/css">
    <link href="css/swiper.css" rel="stylesheet" type="text/css">
    <link href="css/swipebox.min.css" rel="stylesheet" type="text/css">
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="static/bootstrap/css/bootstrap.min.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="static/plugins/datepicker/datepicker3.css">
    <!-- daterange picker -->
    <link rel="stylesheet" href="static/plugins/daterangepicker/daterangepicker.css">
    <!-- Bootstrap time Picker -->
    <link rel="stylesheet" href="static/plugins/timepicker/bootstrap-timepicker.min.css">
    <!-- Bootstrap Color Picker -->
    <link rel="stylesheet" href="static/plugins/colorpicker/bootstrap-colorpicker.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="static/plugins/select2/select2.min.css">

    <link rel="stylesheet" href="lib/vakata-jstree/dist/themes/default/style.min.css" />

    <script type="text/javascript" src="js/jquery-2.1.0.min.js"></script>

    

    <style>
        .owncolor {
            background: #375ce4;
        }
        .clcolor {
            background: #ffffff;
        }
        .input-field {
            margin-top: 30px;
        }
        .backpaging {
            border-radius: 10px;
        }
        ul.ownpaging {
            padding: 5px;
        }
        ul.ownpaging li{
            border-radius: 5px;
            padding-top: 5px;
            padding-left: 5px;
            padding-right: 5px;
            padding-bottom: 5px;
            display: inline;
            margin-left: 3px;
            margin-right: 3px;
        }
        ul.ownpaging li a {
            color: #000;
        }
        ul.ownpaging li a:hover {
            color: #c0c0c0;
        }
        ul.ownpaging li.disabledd a {
            color: #a0a0a0;
            pointer-events: none;
        }
        ul.ownpaging li.active {
            background: #0a3177;
            font-weight: bold;
        }
        ul.ownpaging li.active a {
            color: #fff;
        }
    </style>
</head>
<body>
<?php
if(isset($_SESSION['error-time'])){
    if($_SESSION['error-time'] <= time()){
        unset($_SESSION['error-time']);
        unset($_SESSION['error-type']);
        unset($_SESSION['error-message']);
    }
}
?>
<div class="m-scene" id="main">
    <!-- Page Content -->
    <div id="content">
