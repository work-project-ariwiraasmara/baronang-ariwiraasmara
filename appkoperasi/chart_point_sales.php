<?php
require('header.php');
require('sidebar-left.php');
require('sidebar-right.php');
require('content-header.php');
?>

    <div class="animated fadeinup delay-1">
        <div class="page-content txt-black">
            
            <?php
            $chart = @$_GET['chart'];
            if(@$_GET['chart']) {
                $json_label = array();
                $json_data1 = array();
                
                $bulan = @$_GET['bulan'];
                $tahun = @$_GET['tahun'];
                
                $bulan_int = date('t', strtotime($bulan));
                $tahun_int = date('Y', strtotime($tahun));
                $month = date('m', strtotime($bulan));
                
                $edc = @$_GET['edc'];
                
                $sql_edc = "Select UserIDBaronang from Gateway.dbo.EDCList where SerialNumber = '$edc'";
				$queryEDC = sqlsrv_query($conn, $sql_edc);
				$dataEDC = sqlsrv_fetch_array($queryEDC);
				
				$sql_kasir = "Select NamaUser from [PaymentGateway].[dbo].[UserPaymentGateway] where KodeUser = '$dataEDC[0]'";
				$query_kasir = sqlsrv_query($conn, $sql_kasir);
				$data_kasir = sqlsrv_fetch_array($query_kasir);
				
				$sql_rp = "SELECT ValueConversion from [dbo].[ParkingSetting]";
				$query_rp = sqlsrv_query($conn, $sql_rp);
				$data_rp = sqlsrv_fetch_array($query_rp);
				$conv = $data_rp[0];
				
                ?>

                
                <?php
                if(@$_GET['chart'] == 'hari') { ?>
                    <a href="chart_point_sales.php?chart=bulan&bulan=<?php echo $bulan; ?>&tahun=<?php echo $tahun; ?>&edc=<?php echo $edc; ?>" target="_self" class="btn waves-effect waves-light primary-color"><i class="ion-android-arrow-back txt-white"></i></a> Kembali ke Tampilan Bulanan
                    <?php
                }
                ?>

                <div id="container" style="width: 100% ; height: 500px">

                    <?php
                    if(@$_GET['chart'] == 'bulan') { ?>
                    <a href="chart_sales_edc.php" target="_self" class="btn waves-effect waves-light primary-color"><i class="ion-android-arrow-back txt-white"></i></a> Kembali ke Penjualan per Kasir
                    <?php
						
						$sql_pointsales = "SELECT SUM(Amount) from [dbo].[TransList] where TransactionType='TOPP' and month([Date]) = '$month' and AccountDebet='$edc'";
						$query_pointsales = sqlsrv_query($conn, $sql_pointsales);
						$data_pointsales = sqlsrv_fetch_array($query_pointsales, SQLSRV_FETCH_NUMERIC);
							if ($data_pointsales[0] == null) {
								$pointsales = 0;}
							else {
								$pointrp = $data_pointsales[0] / $conv;
								$pointsales = number_format($pointrp,0,",",".");}
								
                        $title = '<b>Penjualan Poin<br>'.$data_kasir[0].'<br>'.date('F', strtotime($bulan)).' '.date('Y', strtotime($tahun)).'<br>'.$pointsales.' Points<br><br></b>';

                        for($l = 1; $l <= $bulan_int; $l++) {
                            $ln1 = 0;
                           
                            $dtf = $tahun_int."-".date('m', strtotime($bulan))."-".$l;
                            $dtt = $tahun_int."-".date('m', strtotime($bulan))."-".$bulan_int;

                            if($l > 0 && $l < 10) { array_push($json_label, '<a href="chart_point_sales.php?chart=hari&tgl='.$l.'&bulan='.$bulan.'&tahun='.$tahun.'&edc='.$edc.'" target="_self">0'.$l.'</a>'); }
                            else { array_push($json_label, '<a href="chart_point_sales.php?chart=hari&tgl='.$l.'&bulan='.$bulan.'&tahun='.$tahun.'&edc='.$edc.'" target="_self">'.$l.'</a>'); }
                            
							$sql_line1 = "SELECT SUM(Amount) from [dbo].[TransList] where AccountDebet='$edc' and [Date] between '$dtf 00:00:00' and '$dtf 23:59:59'";
                            $query1 = sqlsrv_query($conn, $sql_line1);
                            while($data1 = sqlsrv_fetch_array($query1)) {
								if ($data1[0] == null) {
									array_push($json_data1, 0); }
								else {
									array_push($json_data1, $data1[0]);}
								
                            }

                            
                        }
                        ?>

                        <div class="m-t-10" style="margin-bottom: 50px;">
                            <?php
                            //$dmn = date('t M'); 
                            //$yn = date('Y');
                            $month = date('m', strtotime($bulan));
                            //echo $dmn;
                            ?>

                            <?php 
                            if( !(($bulan == '31 Jan') && ($tahun == '2019')) ) { ?>
                                <a href="chart_point_sales.php?chart=bulan&bulan=<?php echo date('t M', strtotime($tahun.'-'.$month.'-01'.'-1 month')); ?>&tahun=<?php echo $tahun; ?>&edc=<?php echo $edc; ?>" target="_self" class="btn btn-large left primary-color waves-effect waves-light" style="color: #ffffff;">Prev</a>
                            <?php 
                            }
                            ?>

                            <?php
                            if( !(($dmn == date('t M')) && ($yn == date('Y'))) ) { ?>
                                <a href="chart_point_sales.php?chart=bulan&bulan=<?php echo date('t M', strtotime($tahun.'-'.$month.'-01'.'+1 month')); ?>&tahun=<?php echo $tahun; ?>&edc=<?php echo $edc; ?>" target="_self" class="btn btn-large right primary-color waves-effect waves-light txt-white" style="color: #ffffff;">Next</a>
                                <?php
                            }
                            ?>
                        </div>
                        <?php 
                    }
                    else if(@$_GET['chart'] == 'hari') { 
						$day =  @$_GET['tgl'];
						$tanggal = $tahun.'-'.$month.'-'.$day;
						$sql_pointsalesday = "SELECT SUM(Amount) from [dbo].[TransList] where TransactionType='TOPP' and CONVERT(DATETIME, FLOOR(CONVERT(FLOAT,[Date]))) = '$tanggal' and AccountDebet='$edc'";
						$query_pointsalesday = sqlsrv_query($conn, $sql_pointsalesday);
						$data_pointsalesday = sqlsrv_fetch_array($query_pointsalesday, SQLSRV_FETCH_NUMERIC);
							if ($data_pointsalesday[0] == null) {
								$pointsalesday = 0;}
							else {
								$pointrpday = $data_pointsalesday[0] / $conv;
								$pointsalesday = number_format($pointrpday,0,",",".");}
						
                        $title = '<b>Penjualan Poin<br>'.$data_kasir[0].'<br>'.@$_GET['tgl'].' '.date('F', strtotime($bulan)).' '.date('Y', strtotime($tahun)).'<br>'.$pointsalesday.' Points<br><br></b>';
                        
                        for($j = 0; $j < 24; $j++) {
                            $ln1 = 0;
                            $ln2 = 0;
                            $ln3 = 0;
                            
                            $dtf = $tahun_int."-".date('m', strtotime($bulan))."-".@$_GET['tgl'];
                            $jf = $j.':00:00.000';
                            $jt = $j.':59:59.999';

                            if($j == 0) $jam = 'nol';
                            else $jam = $j;

                            //echo $dtf.' '.$jf.' - '.$dtf.' '.$jt.'<br>';

                            array_push($json_label, '<a href="chart_point_sales.php?chart=hari&tgl='.@$_GET['tgl'].'&bulan='.$bulan.'&tahun='.$tahun.'&jam='.$jam.'&edc='.$edc.'" target="_self">'.$j.':00 - '.$j.':59</a>');

                            $sql_line1 = "SELECT SUM(Amount) from [dbo].[TransList] where AccountDebet='$edc' and [Date] between '$dtf $jf' and '$dtf $jt'";
                            $query1 = sqlsrv_query($conn, $sql_line1);
                            while($data1 = sqlsrv_fetch_array($query1)) {
								if ($data1[0] == null) {
									array_push($json_data1, 0); }
								else {
                                array_push($json_data1, $data1[0]);}
                            }

                           
                        } ?>
                        <div class="m-t-10" style="margin-bottom: 50px;">
                            <?php 
                            $tn = @$_GET['tgl'];
                            $jnp = @$_GET['jam'];

                            if($jnp == 'nol') $jnp = 0;
                            
                            if( @$_GET['jam'] ) { 

                                if($jnp == 'nol') { ?>
                                    <a href="chart_point_sales.php?chart=hari&tgl=<?php echo ($tn-1); ?>&bulan=<?php echo date('t M', strtotime($bulan)); ?>&tahun=<?php echo $tahun; ?>&jam=23&edc=<?php echo $edc; ?>" target="_self" class="btn btn-large left primary-color waves-effect waves-light txt-white" style="color: #ffffff;">Prev</a>
                            
                                    <a href="chart_point_sales.php?chart=hari&tgl=<?php echo $tn; ?>&bulan=<?php echo date('t M', strtotime($bulan)); ?>&tahun=<?php echo $tahun; ?>&jam=<?php if( ($jnp+1) == $bulan_int ) { echo '#'; } else { echo ($jnp+1); } ?>&edc=<?php echo $edc; ?>" target="_self" class="btn btn-large right primary-color waves-effect waves-light txt-white" style="color: #ffffff;">Next</a>
                                <?php

                                }
                                else { 
                                    if($jnp == 23) { ?>
                                            <a href="chart_point_sales.php?chart=hari&tgl=<?php echo $tn; ?>&bulan=<?php echo date('t M', strtotime($bulan)); ?>&tahun=<?php echo $tahun; ?>&jam=<?php if( ($jnp-1) == 0 ) { echo 'nol'; } else { echo ($jnp-1); } ?>&edc=<?php echo $edc; ?>" target="_self" class="btn btn-large left primary-color waves-effect waves-light txt-white" style="color: #ffffff;">Prev</a>
                                    
                                            <a href="chart_point_sales.php?chart=hari&tgl=<?php echo ($tn+1); ?>&bulan=<?php echo date('t M', strtotime($bulan)); ?>&tahun=<?php echo $tahun; ?>&jam=nol&edc=<?php echo $edc; ?>" target="_self" class="btn btn-large right primary-color waves-effect waves-light txt-white" style="color: #ffffff;">Next</a>
                                        <?php
                                    }
                                    else { ?>
                                            <a href="chart_point_sales.php?chart=hari&tgl=<?php echo $tn; ?>&bulan=<?php echo date('t M', strtotime($bulan)); ?>&tahun=<?php echo $tahun; ?>&jam=<?php if( ($jnp-1) == 0 ) { echo 'nol'; } else { echo ($jnp-1); } ?>&edc=<?php echo $edc; ?>" target="_self" class="btn btn-large left primary-color waves-effect waves-light txt-white" style="color: #ffffff;">Prev</a>
                                    
                                            <a href="chart_point_sales.php?chart=hari&tgl=<?php echo $tn; ?>&bulan=<?php echo date('t M', strtotime($bulan)); ?>&tahun=<?php echo $tahun; ?>&jam=<?php if( ($jnp+1) == $bulan_int ) { echo '#'; } else { echo ($jnp+1); } ?>&edc=<?php echo $edc; ?>" target="_self" class="btn btn-large right primary-color waves-effect waves-light txt-white" style="color: #ffffff;">Next</a>
                                        <?php
                                    }
                                }
                                
                            }
                            else {
                                if( !($tn == '1') ) { ?>
                                    <a href="chart_point_sales.php?chart=hari&tgl=<?php echo ($tn-1); ?>&bulan=<?php echo date('t M', strtotime($bulan)); ?>&tahun=<?php echo $tahun; ?>&edc=<?php echo $edc; ?>" target="_self" class="btn btn-large left primary-color waves-effect waves-light txt-white" style="color: #ffffff;">Prev</a>
                                    <?php
                                }
                                
                                if( !($tn == date('t', strtotime($bulan)) ) ) { ?>
                                    <a href="chart_point_sales.php?chart=hari&tgl=<?php echo ($tn+1); ?>&bulan=<?php echo date('t M', strtotime($bulan)); ?>&tahun=<?php echo $tahun; ?>&edc=<?php echo $edc; ?>" target="_self" class="btn btn-large right primary-color waves-effect waves-light txt-white" style="color: #ffffff;">Next</a>
                                    <?php
                                }
                            }
                            
                             ?>
                        </div>
                        <?php 
                    } 
                    ?>
                    

                <?php
                    if(@$_GET['chart'] == 'hari') { 
                        if( @$_GET['jam'] ) {
                            $dtf = $tahun_int."-".date('m', strtotime($bulan))."-".@$_GET['tgl'];

                            if(@$_GET['jam'] == 'nol') $jam = 0;
                            else $jam = @$_GET['jam'];

                            $jf = $jam.':00';
                            $jt = $jam.':59';

                            $title = '<b>Penjualan Poin<br>'.$data_kasir[0].'<br>'.@$_GET['tgl'].' '.date('F', strtotime($bulan)).' '.date('Y', strtotime($tahun)).'</b>';
                            ?>
                            <div class="table-responsive m-t-30 p-10">
                                <h3 class="uppercase txt-black"><?php echo 'Tabel Sales Detil Perhari<br>Jam '.$jf.'-'.$jt; ?> </h3> <br>
								
                                <table class="table" style="width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>Jam</th>
                                            <th>CardNo</th>
                                            <th>Amount</th>
                                        </tr>
                                  </thead>
  
                                    <tbody>
										  
                                        <?php
                                        $sqlj = "SELECT *, CONVERT(varchar(255), [Date], 120) from [dbo].[TransList] where AccountDebet='$edc' and [Date] between '$dtf $jf' and '$dtf $jt' Order By [Date] ASC";
                                        $queryj = sqlsrv_query($conn, $sqlj);
                                        while( $dataj = sqlsrv_fetch_array($queryj) ) { 
                                            //if($dataj[2] == '1') $status = 'Aktif';
                                            //else $status = 'Tidak Aktif';
                                            //$tglj = date_create($dataj[4]);
                                            ?>
                                            <tr>
                                                <td><?php echo date('H:i:s', strtotime($dataj[15])); ?></td>
                                                <td><?php echo $dataj[2]; ?></td>
                                                <td><?php echo $dataj[5]; ?></td>
                                                
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody>
                                </table> 
                            </div>
                            <?php
                        }
                    }
                ?> 
                                
                </div>
                
                <?php
			
            }
            else { ?>
                <script type="text/javascript">
                    window.location.href = "home.php";
                </script>
                <?php
            }
            ?>
            </div>
        </div>
    </div>

   
    <script src="js/plotly-latest.min.js"></script>
    <script type="text/javascript">
        $('#btn_cj').click(function() {
            var jam = $('#chart_jam :selected').val();
            var url = "chart_point_sales.php?chart=hari&tgl=<?php echo @$_GET['tgl']; ?>&bulan=<?php echo $bulan; ?>&tahun=<?php echo $tahun; ?>" + "&jam=" + jam + "&edc=<?php echo $edc; ?> target='_self'";

            window.location.href = url;
        });

        var data = [
        {
			x: <?php echo json_encode($json_label); ?>, 
            y: <?php echo json_encode($json_data1); ?>,
            type: 'bar'
		}
		];
		
        var layout = {
            autosize: false,
            width: 1400,
            height: 500,
            title: '<?php echo $title; ?>',
            xaxis: { type: 'category', fixedrange: true },
            yaxis: { fixedrange: true },
            barmode: 'group'
        };

        Plotly.newPlot('container', data, layout, {displaylogo: false});
    </script>
    
<?php
require('footer.php');
?>

