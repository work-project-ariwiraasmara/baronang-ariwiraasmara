<?php require('header.php');?>      
<?php require('sidebar-left-user.php');?>

<section class="content">
<div class="row">
    <!-- Main content -->
    <section class="content">
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
		<div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Deposit</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal">
              <div class="box-body">
                <div class="form-group">
				<div class="col-sm-3"></div>
				  <label for="accno" class="col-sm-2 control-label"style="text-align: left;">Account Number</label>
				  <div class="col-sm-3">
                  <div class="btn-group">
                  <button type="button" class="btn btn-default"> Select</button>
                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                  </ul>
                </div>
                </div>
                </div>
                <div class="form-group">
				<div class="col-sm-3"></div>
				  <label for="amount" class="col-sm-2 control-label"style="text-align: left;">Amount</label>
				  <div class="col-sm-2">
				  <input type="text" class="form-control input-sm" id="amount" placeholder="">
				  </div>
                </div>
				<div class="form-group">
				<div class="col-sm-3"></div>
				<label for="description" class="col-sm-2 control-label"style="text-align: left;">Description</label>
				<div class="col-sm-3">
                <textarea class="form-control" rows="3" placeholder="Description"></textarea>
                </div>
				</div>
                </div>
				
				<div class="box-footer">
				<button type="submit" class="btn btn-info pull-right">Save</button>
				</div>
              </div>
              <!-- /.box-body -->
            </form>
			
          </div>
        </section>
        <!-- /.Left col -->
		
      </div>
	  </section>
	  </section>
<?php require('content-footer.php');?>
<?php require('footer.php');?>