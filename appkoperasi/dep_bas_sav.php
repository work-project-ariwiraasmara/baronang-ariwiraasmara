<?php require('header.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<script language="javascript">
    function load_page(memberid){
        window.location.href = "dep_bas_sav.php?memberid=" + memberid;
    }
</script>

<?php
$qq= $_GET['memberid'];
$accsimp3= "";
$outstan4 = "0";
$_SESSION ['AccSimpananPokok']	= "";
$_SESSION ['Namex']	= "";
$_SESSION ['OutstandingSimpananPokok']	= "";
$_SESSION ['AccSimpananWajib']	= "";
$_SESSION ['OutstandingSimpananWajib']	= "";
$dis = '';
if(isset($qq)){
    $a = "select * from [dbo].[MemberList] where MemberID='$qq' and StatusMember = 1";
    $b = sqlsrv_query($conn, $a);
    $c = sqlsrv_fetch_array( $b, SQLSRV_FETCH_NUMERIC);
    if($c != null){
        $xxx   = "select * from [dbo].[BasicSavReport] where MemberID='$qq'";
        $xxxx  = sqlsrv_query($conn, $xxx);
        while($xx   = sqlsrv_fetch_array( $xxxx, SQLSRV_FETCH_NUMERIC)){
            $accsimp= $xx[2];
            $names = $xx[1];
            $_SESSION ['AccSimpananPokok']	= $accsimp;
            $_SESSION ['Namex']	= $names;
            $_SESSION ['OutstandingSimpananPokok']	= $xx[3];
            $_SESSION ['AccSimpananWajib']	= $xx[4];
            $_SESSION ['OutstandingSimpananWajib']	= $xx[5];
            $outstan = $xx[3];
            $accsimp2 = $xx [4];
            $outstan2 = $xx [5];
        }

        $xxx2   = "select * from [dbo].[BasicSavingSukarela] where MemberID='$qq'";
        $xxxx2  = sqlsrv_query($conn, $xxx2);
        while($xx2   = sqlsrv_fetch_array( $xxxx2, SQLSRV_FETCH_NUMERIC)){
            $accsimp3= $xx2[3];
            $outstan4 = $xx2[4];
            $_SESSION ['AccNo']				= $xx2[3];
            $_SESSION ['PaymentBalance']	= $xx2[5];
        }
    }
    else{
        $dis = 'disabled';

        $_SESSION['error-message'] = 'Member ID tidak aktif';
        $_SESSION['error-type'] = 'warning';
        $_SESSION['error-time'] = time()+5;
    }
}
?>

<?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
    <div class="alert alert-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-warning ?>"></i> <?php echo ucfirst($_SESSION['error-type']); ?></h4>
        <?php echo $_SESSION['error-message']; ?>
    </div>
<?php } ?>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Basic Saving Deposit Cashier</h3>
    </div>
    <form action="procdep_bas_sav.php" method="POST" class="form-horizontal">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="csoaregsavmember" class="col-sm-3 control-label" style="text-align: left;">Member ID</label>
                            <div class="col-sm-6">
                                <input type="text" name="member" class="form-control" onblur="load_page(this.value);" id="csoaregsavmember" placeholder="" value=<?php echo $qq; ?> >
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="nama" class="col-sm-2 control-label" style="text-align: left;">Name</label>
                            <div class="col-sm-6">
                                <input type="text" name="nama" class="form-control" id="nama" placeholder="" value="<?php echo"$names"; ?>" disabled>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12" style="height:20px;">
                </div>
                <div class="col-sm-12">
                    <div class="col-sm-4" style="border:1px solid #d2d6de; border-radius:15px;">
                        <div class="col-sm-12" style="margin-bottom:10px;">
                            <label class="control-label text-center" style="width:100%;text-align: center;">Simpanan Pokok</label>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <small for="name" class="col-sm-3" style="text-align: left;">Payment Amount</small>
                                <div class="col-sm-9">
                                    <input type="text" name="sppax" class="form-control" id="sppax" placeholder="" value="0">
                                </div>
                            </div>
                            <div class="form-group">
                                <small for="name" class="col-sm-3" style="text-align: left;">Outstanding Amount</small>
                                <div class="col-sm-9">
                                    <input type="text" name="spoa" class="form-control price" id="spoa" placeholder="" value="<?php echo "$outstan"; ?>" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <small for="name" class="col-sm-3" style="text-align: left;">Account Number</small>
                                <div class="col-sm-9">
                                    <input type="text" name="span" class="form-control" id="span" placeholder="" value="<?php echo "$accsimp"; ?>"  disabled>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4" style="border:1px solid #d2d6de; border-radius:15px;">
                        <div class="col-sm-12" style="margin-bottom:10px;">
                            <label class="control-label" style="width:100%;text-align: center;">Simpanan Wajib</label>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <small for="name" class="col-sm-3" style="text-align: left;">Payment Amount</small>
                                <div class="col-sm-9">
                                    <input type="text" name="swpa" class="form-control" id="swpa" placeholder=""  value="0">
                                </div>
                            </div>
                            <div class="form-group">
                                <small for="name" class="col-sm-3" style="text-align: left;">Outstanding Amount</small>
                                <div class="col-sm-9">
                                    <input type="text" name="swoa" class="form-control price" id="swoa" placeholder=""  value="<?php echo "$outstan2"; ?>"  disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <small for="name" class="col-sm-3" style="text-align: left;">Account Number</small>
                                <div class="col-sm-9">
                                    <input type="text" name="swan" class="form-control" id="swan" placeholder=""  value="<?php echo "$accsimp2"; ?>"  disabled>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4" style="border:1px solid #d2d6de; border-radius:15px;">
                        <div class="col-sm-12" style="margin-bottom:10px;">
                            <label class="control-label" style="width:100%;text-align: center;">Simpanan Sukarela</label>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <small for="name" class="col-sm-3" style="text-align: left;">Payment Amount</small>
                                <div class="col-sm-9">
                                    <input type="text" name="sspa" class="form-control" id="sspa" placeholder=""  value="0">
                                </div>
                            </div>
                            <div class="form-group">
                                <small for="name" class="col-sm-3" style="text-align: left;">Outstanding Ammount</small>
                                <div class="col-sm-9">
                                    <input type="text" name="ssoa" class="form-control price" id="ssoa" placeholder=""  value="<?php echo "$outstan4"; ?>"  disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <small for="name" class="col-sm-3" style="text-align: left;">Account Number</small>
                                <div class="col-sm-9">
                                    <input type="text" name="ssan" class="form-control" id="ssan" placeholder=""  value="<?php echo "$accsimp3"; ?>"  disabled>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <div class="row">
                <div class="col-sm-4">
                    <?php if($qq != ''){ ?>
                        <a href="dep_bas_sav.php"><button type="button" class="btn btn-flat btn-block btn-default">Cancel</button></a>
                    <?php } ?>
                </div>
                <div class="col-sm-4">
                    <button type="submit" class="btn btn-flat btn-block btn-primary" <?php echo $dis; ?>>Save</button>
                </div>
                <div class="col-sm-4">
                </div>
            </div>
        </div>
    </form>
</div>

<?php  require('content-footer.php');?>

<?php  require('footer.php');?>
