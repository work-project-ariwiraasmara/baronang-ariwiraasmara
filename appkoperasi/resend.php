<?php
require("lib/PHPMailer_5.2.0/class.phpmailer.php");

include("connect.php");

if(isset($_GET['konfemail'])){
    $email = $_GET['konfemail'];

    $a = "select* from KoneksiKoperasiBaronang.[dbo].[Register] where emailKoperasi = '$email'";
    $b = sqlsrv_query($conn, $a);
    $c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC);
    if($c != null){
        sendEmail($c[2],$c[0],$c[4]);

        messageAlert('Email konfirmasi terkirim.','success');
        header('Location: register.php');
    }
    else{
        messageAlert('Email tidak ditemukan.','warning');
        header('Location: register.php');
    }
}

function sendEmail($email,$kode,$nama){
    $root = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/';
    $link = $root."koperasi/confregister.php?u=".$kode;

    $mail = new PHPMailer();
    $mail->IsSMTP();
    $mail->Host = "baronang.com";  // specify main and backup server
    $mail->SMTPAuth = true;     // turn on SMTP authentication
    $mail->Username = "konfirmasi@baronang.com";  // SMTP username
    $mail->Password = "alva7000"; // SMTP password
    $mail->Port = 587;
    $mail->SMTPDebug = 1;

    $mail->From = "konfirmasi@baronang.com";
    $mail->FromName = "Baronang Mail Service";
    $mail->AddAddress($email);
    $mail->AddReplyTo("konfirmasi@baronang.com", "Baronang Konfirmasi");
    $mail->IsHTML(true);// set email format to HTML

    $mail->Subject = "Email Verifikasi Koperasi";
    $mail->Body =
        "Hi! ".$nama.",<br><br>
            Terima kasih telah bergabung dengan Baronang.<br>
            Kami perlu memastikan bahwa email anda valid. Harap mengkonfirmasi email anda dengan mengklik link dibawah ini.<br>
            <a href=".$link.">Konfirmasi</a>
            <br>
            <br>
            <br>
            <br>
            Terima kasih.<br>
            Salam,<br>
            <br>
            <br>
            Baronang";
    if(!$mail->Send())
    {
        messageAlert('Message could not be sent. Mailer Error:'.$mail->ErrorInfo,'warning');
        header('Location: register.php');
    }
}
?>