<?php
session_start();
error_reporting(0);

include('lib/phpExcel/Classes/PHPExcel.php');
include "connect.php";


    $filePath = "uploads/excel/";

    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getProperties()->setCreator("baronang");
    $objPHPExcel->getProperties()->setTitle("Drep Jurnal Transaksi");

// set autowidth
    for($col = 'A'; $col !== 'Z'; $col++) {
        $objPHPExcel->getActiveSheet()
            ->getColumnDimension($col)
            ->setAutoSize(true);
    }

    $listCombo = array();

    //sheet 2
    $objWorkSheet = $objPHPExcel->createSheet(0);
// baris judul
    

    $objWorkSheet->getStyle('D1')->getFont()->setBold(true);
    $objWorkSheet->getStyle('D1')->getFont()->setSize(12);
    $objWorkSheet->SetCellValue('D1', 'Jurnal Transaksi');

    if ($_GET['userid'] == 1){
    $objWorkSheet->getStyle('D2')->getFont()->setBold(true);
    $objWorkSheet->getStyle('D2')->getFont()->setSize(12);
    $objWorkSheet->SetCellValue('D2', 'Periode :');
    } else {
    $objWorkSheet->getStyle('D2')->getFont()->setBold(true);
    $objWorkSheet->getStyle('D2')->getFont()->setSize(12);
    $objWorkSheet->SetCellValue('D2', 'No Referensi :');
    }
    
    $objWorkSheet->getStyle('A5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('A5' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('A5', 'No');
    $objWorkSheet->getStyle('B5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('B5' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('B5', 'Tanggal');
    $objWorkSheet->getStyle('C5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('C5' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('C5', 'No. Referensi');
    $objWorkSheet->getStyle('D5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('D5' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('D5', 'Kode Jurnal');
    $objWorkSheet->getStyle('E5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('E5' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('E5', 'Kode Debit / Nama Debit');
    $objWorkSheet->getStyle('F5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('F5' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('F5', 'Kode Kredit / Nama Kredit'); 
    $objWorkSheet->getStyle('G5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('G5' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('G5', 'Harga');
    $objWorkSheet->getStyle('H5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('H5' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('H5', 'Deskripsi');    



    if(isset($_GET['userid'])){
    
    $ref = $_GET['ref'];
    //echo $ref;
    $cari1new = $_GET['cari1'];
    $cari2new = $_GET['cari2'];
    $cari1 = date('Y-m-d H:i:s', strtotime($cari1new));
    $cari2 = date('Y-m-d 23:59:59', strtotime($cari2new));

    $no = 1;
    $bln = 2 ;

    if (isset($_GET['cari1']) and isset($_GET['cari2'])) {
    $hanyatanggal = date('d', strtotime($cari1));
    $bulan = date('m', strtotime($cari1));
    $tahun = date('Y', strtotime($cari1));
        if ($bulan != 1) {
            if ($bulan != 2 ) {
                if ($bulan != 3) {
                    if ($bulan != 4) {
                        if ($bulan !=5) {
                            if ($bulan !=6) {
                                if ($bulan !=7) {
                                    if ($bulan !=8) {
                                        if ($bulan !=9) {
                                            if ($bulan !=10) {
                                                if ($bulan !=11) {
                                                    $bulan = 'Desember';
                                                } else {
                                                $bulan = 'Nopember';
                                                }
                                            } else {
                                            $bulan = 'OKtober';   
                                            }
                                        } else {
                                        $bulan = 'September';
                                        }
                                    } else {
                                    $bulan = 'Agustus';
                                    }
                                } else {
                                $bulan = 'Juli';
                                }
                            } else {
                            $bulan = 'Juni';
                            }
                        } else {
                        $bulan = 'Mei';  
                        }
                    } else {
                    $bulan = 'April';       
                    }
                } else {
                $bulan = 'Maret';
                }
            } else {
            $bulan = 'Februari';    
            }                               
        } else {
        $bulan = 'Januari';
        }

    $tgl2 = date('d', strtotime($cari2));
    $bulan2 = date('m', strtotime($cari2));
    $tahun2 = date('Y', strtotime($cari2));
        if ($bulan2 != 1) {
            if ($bulan2 != 2 ) {
                if ($bulan2 != 3) {
                    if ($bulan2 != 4) {
                        if ($bulan2 !=5) {
                            if ($bulan2 !=6) {
                                if ($bulan2 !=7) {
                                    if ($bulan2 !=8) {
                                        if ($bulan2 !=9) {
                                            if ($bulan2 !=10) {
                                                if ($bulan2 !=11) {
                                                    $bulan2 = 'Desember';
                                                } else {
                                                $bulan2 = 'Nopember';
                                                }
                                            } else {
                                            $bulan2 = 'OKtober';   
                                            }
                                        } else {
                                        $bulan2 = 'September';
                                        }
                                    } else {
                                    $bulan2 = 'Agustus';
                                    }
                                } else {
                                $bulan2 = 'Juli';
                                }
                            } else {
                            $bulan2 = 'Juni';
                            }
                        } else {
                        $bulan2 = 'Mei';  
                        }
                    } else {
                    $bulan2 = 'April';       
                    }
                } else {
                $bulan2 = 'Maret';
                }
            } else {
            $bulan2 = 'Februari';    
            }                               
        } else {
        $bulan2 = 'Januari';
        }

        $objWorkSheet->getStyle('E'.$bln)->getFont()->setBold(true);
        $objWorkSheet->SetCellValue("E".$bln,$hanyatanggal .' '.$bulan .' '. $tahun .' - '. $tgl2 .' '. $bulan2 .' '. $tahun2);

    } else if (isset($_GET['ref'])) { 


    $objWorkSheet->getStyle('E2')->getFont()->setBold(true);
    $objWorkSheet->SetCellValueExplicit("E2",$_GET['ref']);
    }

    $row = 6;
    if($_GET['userid'] == 1){
    $aaa = "SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY Date asc) as row FROM [dbo].[Translist] a where TransNo is not null and Date between '".$cari1."' and '".$cari2."' ) a";
    } else {
         $aaa = "SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY Date asc) as row FROM [dbo].[Translist] a where TransNo is not null and RefNumber = '$ref') a";
     }  
    //echo $aaa;
    $bbb = sqlsrv_query($conn, $aaa);
    $total= $awal;
    $ctotal = $total;
    while($ccc = sqlsrv_fetch_array($bbb, SQLSRV_FETCH_NUMERIC)){
        $tanggal = $ccc[6]->format('Y-m-d H:i:s');

        $dua   = "select * from dbo.Account where KodeAccount='$ccc[3]'";
        //echo $sql;
        $pdua  = sqlsrv_query($conn, $dua);
        $hdua   = sqlsrv_fetch_array($pdua, SQLSRV_FETCH_NUMERIC);
        $hsdua = $hdua[1];

        $tiga   = "select * from dbo.Account where KodeAccount='$ccc[4]'";
        //echo $sql1;
        $ptiga  = sqlsrv_query($conn, $tiga);
        $htiga   = sqlsrv_fetch_array($ptiga, SQLSRV_FETCH_NUMERIC);
        $hstiga = $htiga[1];


        $objWorkSheet->SetCellValue("A".$row,$ccc[15]);
        $objWorkSheet->SetCellValue("B".$row,$tanggal);
        $objWorkSheet->SetCellValue("C".$row, $ccc[8]);
        $objWorkSheet->SetCellValue("D".$row,$ccc[0]);
        $objWorkSheet->SetCellValue("E".$row,$ccc[3].' / '.$hsdua);
        $objWorkSheet->SetCellValue("F".$row,$ccc[4].' / '.$hstiga);
        $objWorkSheet->getStyle('G' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $objWorkSheet->SetCellValue("G".$row, number_format($ccc[5],2), PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet->SetCellValue("H".$row,$ccc[9]);


                $row++;
           
    }
            
            
    }
//exit;
    $objWorkSheet->setTitle('Drep Jurnal Transaksi');

    $fileName = 'JurnalTrans'.'_'.strtotime(date('Y-m-d H:i:s')).'.xls';
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');

// download ke client
    header('Content-type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename="'.$fileName.'"');
    $objWriter->save('php://output');

    return $filePath.'/'.$fileName;

?>
