<?php
session_start();
error_reporting(0);

include('lib/phpExcel/Classes/PHPExcel.php');
include "connect.php";

$a = "select * from [dbo].[JabatanDetail]";
$b = sqlsrv_query($conn, $a);
$c = sqlsrv_fetch_array( $b, SQLSRV_FETCH_NUMERIC);
if($c == null){
    messageAlert('Position belum di set. Tidak dapat mendownload template.','warning');
    header('Location: upmemberlist.php');
}
else{

    $filePath = "uploads/excel/";

    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getProperties()->setCreator("baronang");
    $objPHPExcel->getProperties()->setTitle("Data Upload Member");

// set autowidth
    for($col = 'A'; $col !== 'Z'; $col++) {
        $objPHPExcel->getActiveSheet()
            ->getColumnDimension($col)
            ->setAutoSize(true);
    }

    $listCombo = array();

    //sheet 2
    $objWorkSheet2 = $objPHPExcel->createSheet(1);
// baris judul
    $objWorkSheet2->SetCellValue('A1', 'No');
    $objWorkSheet2->SetCellValue('B1', 'Kode Jabatan');
    $objWorkSheet2->SetCellValue('C1', 'Nama Jabatan');
    $objWorkSheet2->SetCellValue('D1', 'Keterangan');

    $no = 1;
    $row = 2;
    $x = "select * from [dbo].[JabatanView]";
    $y = sqlsrv_query($conn, $x);
    while($z = sqlsrv_fetch_array( $y, SQLSRV_FETCH_NUMERIC)){
        //push untuk drop down list
        array_push($listCombo, $z[1]);

        $objWorkSheet2->SetCellValue("A".$row, $no);
        $objWorkSheet2->SetCellValue("B".$row, $z[0]);
        $objWorkSheet2->SetCellValue("C".$row, $z[1]);
        $objWorkSheet2->SetCellValue("D".$row, $z[2]);

        $no++;
        $row++;
    }

    $objWorkSheet2->setTitle('Data Master Jabatan');
    $objWorkSheet = $objPHPExcel->createSheet(0);
// baris judul
    $objWorkSheet->SetCellValue('A1', 'No');
    $objWorkSheet->SetCellValue('B1', 'Member ID');
    $objWorkSheet->SetCellValue('C1', 'No. KTP');
    $objWorkSheet->SetCellValue('D1', 'NIP');
    $objWorkSheet->SetCellValue('E1', 'Nama');
    $objWorkSheet->SetCellValue('F1', 'Alamat');
    $objWorkSheet->SetCellValue('G1', 'Telepon');
    $objWorkSheet->SetCellValue('H1', 'Email');
    $objWorkSheet->SetCellValue('I1', 'Kode Jabatan');

    //looping isi combo box
    $row = 2;
    for($no=1;$no<=100;$no++){
        $objWorkSheet->SetCellValue("A".$row, $no);

        $combo = $objWorkSheet->GetCell("I".$row)->getDataValidation();
        $combo->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
        $combo->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
        $combo->setAllowBlank(false);
        $combo->setShowInputMessage(true);
        $combo->setShowErrorMessage(true);
        $combo->setShowDropDown(true);
        $combo->setErrorTitle('Input error');
        $combo->setError('Value is not in list.');
        $combo->setPromptTitle('Pick from list');
        $combo->setPrompt('Please pick a value from the drop-down list.');
        $combo->setFormula1('"'.implode(',',$listCombo).'"');

        $row++;
    }

    $objWorkSheet->setTitle('Data Upload Member');

    $fileName = 'templateMember'.'_'.strtotime(date('Y-m-d H:i:s')).'.xls';
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');

// download ke client
    header('Content-type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename="'.$fileName.'"');
    $objWriter->save('php://output');

    return $filePath.'/'.$fileName;
}
?>