<?php
session_start();
error_reporting(0);

include "connect.php";

$page			= $_GET['page'];
$edit			= $_GET['edit'];
$delete			= $_GET['delete'];

$dsc	= $_POST['dsc'];
$inter	= $_POST['inter'];
$cp		= $_POST['cp'];
$ma		= $_POST['ma'];

if($dsc == "" || $inter == "" || $cp == "" || $ma == "" ){
    messageAlert(lang('Harap isi seluruh kolom'),'info');
    header('Location: emoneytype.php');
}
else{
    if(!empty($edit)){
        $upultsql = "update [dbo].[EmoneyType] set NamaemoneyType='$dsc', MaxLimit='$inter', FeeTopup='$cp', Priceemoney='$ma' where KodeemoneyType='$edit'";
        $upulstmt = sqlsrv_query($conn, $upultsql);
        if($upulstmt){
            messageAlert(lang('Berhasil memperbaharui data ke database'),'success');
            header('Location: emoneytype.php');
        }
        else{
            messageAlert(lang('Gagal memperbaharui data ke database'),'danger');
            header('Location: emoneytype.php');
        }
    }
    else{
        $a = "select [dbo].[getKodeemoneyType]('$_SESSION[KID]')";
        $b = sqlsrv_query($conn, $a);
        $c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC);
        $sel = $c[0];

        $spbltsql = "exec [dbo].[ProsesEmoneyType] '$sel','$dsc','$inter','$cp','$ma',0";
        $spblstmt = sqlsrv_query($conn, $spbltsql);

        if($spblstmt){
            messageAlert(lang('Berhasil menyimpan ke database'),'success');
            header('Location: emoneytype.php');
        }
        else{
            messageAlert(lang('Gagal memperbaharui data ke database'),'danger');
            header('Location: emoneytype.php');
        }
    }
}

?>
