<?php
$start = $_POST['start'];
$end = $_POST['end'];
$percent = $_POST['percent'];

$json = array(
    'start'=>0,
    'end'=>0,
    'percent'=>0,
);

$json['start'] = $start;
$json['end'] = $end;
$json['percent'] = $percent;

echo json_encode($json);
?>