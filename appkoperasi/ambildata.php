<?php
@session_start();
//error_reporting(0);
require('header.php');
require('sidebar-right.php');
require('sidebar-left.php');
?>

	<div class="animated fadeinup delay-1">
        <div class="page-content txt-black">

        	<table>
        		<thead>
        			<tr>
        				<th>No.</th>
        				<th>CardNo</th>
        				<th>Name</th>
        				<?php /* <th>Last Month</th> */ ?>
        				<th>Tanggal</th>
        				<th>Status</th>
        				<th>Amount</th>
        			</tr>
        		</thead>

        		<tbody>
        			<?php
					$no = 1;
					$s1 = "SELECT * from [dbo].[TransList] where TransactionType='MBRS' and AccountKredit='700097LOC0000001' and Date between '2019-10-20 00:00:00' and '2019-11-10 23:59:59' order by Date ASC;";
					$q1 = sqlsrv_query($conn, $s1);
					while($d1 = sqlsrv_fetch_array($q1)) {

						$s2 = "SELECT MemberId from [dbo].[MemberCard] where CardNo='".$d1[1]."'";
						$q2 = sqlsrv_query($conn, $s2);
						$d2 = sqlsrv_fetch_array($q2);

						$s3 = "SELECT Name from [dbo].[MemberList] where MemberID='".$d2[0]."'";
						$q3 = sqlsrv_query($conn, $s3);
						$d3 = sqlsrv_fetch_array($q3);

						$s4 = "SELECT TOP 1 Bulan from [dbo].[MemberLocationMonth] order by Bulan DESC";
						$q4 = sqlsrv_query($conn, $s4);
						$d4 = sqlsrv_fetch_array($q4);

						$s5 = "SELECT TOP 1 Status from [dbo].[TransList] where TransactionType='PARK' and AccountKredit='".$d1[2]."' and Date between '2019-10-20 00:00:00' and '2019-11-10 23:59:59'";
						$q5 = sqlsrv_query($conn, $s5);
						$d5 = sqlsrv_fetch_array($q5);
						if($d5 > 0) {
							$status = 'Berbayar';
						}
						else {
							$status = 'Member';
						}

						?>
						<tr>
							<td><?php echo $no; ?></td>
							<td><?php echo $d1[1]; ?></td>
							<td><?php echo $d3[0]; ?></td>
							<?php /*<td><?php echo $d4[0]; ?></td>*/ ?>
							<td><?php echo $d1[6]->format('Y-m-d'); ?></td>
							<td><?php echo $status; ?></td>
							<td><?php echo number_format($d1[5],2,',','.'); ?></td>
						</tr>
						<?php

						$no++;
					}
					?>
        		</tbody>
        	</table>

        </div>
    </div>



<?php
require('footer.php'); 
?>