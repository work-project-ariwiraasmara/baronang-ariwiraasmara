<?php
require('header.php');
require('sidebar-right.php');
require('sidebar-left.php');
require('content-header.php');
?>


<script language="javascript" type="text/javascript">
    function cek(jur) {
        document.frm.submit();
    }
</script>

<div class="animated fadeinup delay-1">
    <div class="page-content">

      <?php if(isset($_SESSION['error-type']) and isset($_SESSION['error-message']) and isset($_SESSION['error-time'])){ ?>
          <?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
              <div class="notification notification-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
                  <h4> <?php echo ucfirst($_SESSION['error-type']); ?> </h4>
                  <?php echo $_SESSION['error-message']; ?>
              </div>
          <?php } ?>
      <?php } ?>

            <h2 class="uppercase"><?php echo lang('Jurnal Koreksi'); ?></h2>

             <form class="form-horizontal" action="" method = "POST" name="frm" id="frm">
                <label ><?php echo lang('No Jurnal'); ?></label>
                  <select id="jur" name="jur" class="browser-default" onChange="cek(this.value);">
                    <option value=''>- <?php echo ('Pilih No. Jurnal'); ?> -</option>
                      <?php
                      $julsql   = "select distinct(TransactionNumber) from JurnalTrans where status=0";
                      $julstmt = sqlsrv_query($conn, $julsql);
                      while($rjulrow  = sqlsrv_fetch_array( $julstmt, SQLSRV_FETCH_NUMERIC)){
                      ?>
                      <option value="<?=$rjulrow[0];?>" <?php if($rjulrow[0]==$_POST['jur']){echo "selected";} ?>><?=$rjulrow[0];?></option>
                      <?php } ?>
                  </select>
             </form>

            <form method="POST" action="procrjurnal_akuntansi.php">



                <div class="input-field">
                    <input type="text" name="tgl" id="tgl" class="datepicker" value="" autocomplete="off"></p></td>
                    <label><?php echo ('Tanggal Awal'); ?></label>
                </div>

                <div class="input-field">
                    <input type="text" name="ket" id="ket" class="validate">
                    <label for="ket">Keterangan</label>
                </div>


                <?php
                if ($_POST['jur']){
                  $k = $_POST['jur'];

                ?>
                  

                 <input type="hidden" id="jur" name="jur" value="<?php echo $k; ?>" readonly>

                <div class="row m-t-20">
                    <div class="col s12 m-t-20">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped" id="table_acc">
                                <thead>
                                    <tr>
                                        <th>
                                            Kode Account<br>
                                        </th>
                                        <th class="center">Debet</th>
                                        <th>
                                            Kode Account<br>
                                        </th>
                                        <th class="center">Kredit</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  <?php
                                  $jurnal = $_POST['jur'];
                                  $tot=0;
                                  $tot2=0;
                                  $lr = "select * from dbo.JurnalTrans where TransactionNumber='$jurnal'";
                                  $rl = sqlsrv_query($conn, $lr);
                                  while($rrl  = sqlsrv_fetch_array( $rl, SQLSRV_FETCH_NUMERIC)){

                                    if ($rrl[4] == null){
                                      $nilai = $rrl[3];
                                      $tot += $rrl[5];
                                    } else {
                                      $nilai = $rrl[4];
                                      $tot2 += $rrl[5];


                                    }

                                    $akun = "select * from dbo.Account where KodeAccount='$nilai'";
                                    $akun1 = sqlsrv_query($conn, $akun);

                                    $akun2  = sqlsrv_fetch_array( $akun1, SQLSRV_FETCH_NUMERIC);
                                    if ($rrl[4] == null){

                                      ?>
                                      <tr>
                                        <td><?php echo $rrl[3]; ?> - <?php echo $akun2[1]; ?></td>
                                        <td style="text-align: right;"><?php echo number_format($rrl[5]); ?></td>
                                        <td></td>
                                        <td></td>
                                      </tr>
                                      <?php } else {

                                        ?>
                                      <tr>
                                        <td></td>
                                        <td></td>
                                        <td><?php echo $rrl[4]; ?> - <?php echo $akun2[1]; ?></td>
                                        <td style="text-align: right;"><?php echo number_format($rrl[5]); ?></td>
                                      </tr>

                                      <?php } ?>
                                  <?php   } ?>

                                </tbody>
                                <tfoot>

                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <?php } ?>


                <div class="row m-t-20">
                    <div class="col s12 m-t-20">
                        <div class="table-responsive">
                            <table class="table" id="table_acc">
                                <thead>
                                  <tr>
                                    <th>
                                      Kode Account<br>
                                    </th>
                                      <th class="center">Debet</th>
                                    <th>
                                      Kode Account<br>
                                    </th>
                                      <th class="center">Kredit</th>
                                  </tr>
                                </thead>
                                  <tbody>
                                    
                                    <?php
                                    if ($_POST['jur']){
                                    ?>


                                    <?php
                                    $jurnal = $_POST['jur'];
                                    $lr = "select * from dbo.JurnalTrans where TransactionNumber='$jurnal'";
                                    $rl = sqlsrv_query($conn, $lr);
                                    $a = 0;
                                    while($rrl  = sqlsrv_fetch_array( $rl, SQLSRV_FETCH_NUMERIC)){
                                      
                                      ?>

                                      <tr>
                                        <td>
                                            <select name="kba<?php echo $a; ?>" id="kba1<?php echo $a; ?>" class="browser-default">
                                              <?php
                                              $sql = "Select * from dbo.AccountMax where substring(KodeAccount,1,6) not in('1.1.01') order by KodeAccount ASC";
                                                $query = sqlsrv_query($conn, $sql);
                                              while($row  = sqlsrv_fetch_array( $query, SQLSRV_FETCH_NUMERIC)) { ?>
                                                <option value="<?=$row[0];?>" <?php if($row[0]==$rrl[4]){echo "selected";} ?>><?php echo $row[0]; ?> - <?php echo $row[1]; ?></option>
                                                    <?php
                                              }
                                              ?>
                                            </select>
                                        </td>
                                        <td>
                                            <?php
                                            if ($rrl[4] == null){
                                              $nilai = 0;
                                              } else {
                                                $nilai = $rrl[5];
                                              ?>
                                            <?php } ?>
                                            <input type="hidden" id="hdebit<?php echo $a; ?>" value="<?php echo ($nilai); ?>" readonly>
                                            <input type="number" name="debit<?php echo $a; ?>" id="debit<?php echo $a; ?>" style="text-align: right;" value="<?php echo round($nilai); ?>" readonly>

                                        </td>
                                        <td>
                                            <select name="kbb<?php echo $a; ?>" id="kba2<?php echo $a; ?>" class="browser-default">
                                                <?php
                                                $sql = "Select * from dbo.AccountMax where substring(KodeAccount,1,6) not in('1.1.01') order by KodeAccount ASC";
                                                $query = sqlsrv_query($conn, $sql);
                                                while($row  = sqlsrv_fetch_array( $query, SQLSRV_FETCH_NUMERIC)) { ?>
                                                        <option value="<?=$row[0];?>" <?php if($row[0]==$rrl[3]){echo "selected";} ?>><?php echo $row[0]; ?> - <?php echo $row[1]; ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </td>
                                        <td>
                                        <?php
                                            if ($rrl[3] == null){
                                              $nilai1 = 0;
                                            } else {
                                              $nilai1 = $rrl[5];

                                              ?>
                                            <?php } ?>
                                            <input type="hidden" id="hkredit<?php echo $a; ?>" value="<?php echo ($nilai1); ?>" readonly>
                                            <input type="number" name="kredit<?php echo $a; ?>" id="kredit<?php echo $a; ?>" style="text-align: right;" value="<?php echo round($nilai1); ?>" readonly>
                                        </td>
                                      </tr>
                                    <?php $a++; }  ?>  
                              <?php } ?>

                              <tr>
                                        <td>
                                            <select name="kba<?php echo 'fix'; ?>" id="kba1<?php echo 'fix'; ?>" class="browser-default">
                                              <option value=''>- <?php echo lang('Pilih Akun'); ?> -</option>
                                                <?php
                                                $sql = "Select * from dbo.AccountMax where substring(KodeAccount,1,6) not in('1.1.01') order by KodeAccount ASC";
                                                $query = sqlsrv_query($conn, $sql);
                                                while($row  = sqlsrv_fetch_array( $query, SQLSRV_FETCH_NUMERIC)) { ?>
                                                        <option value="<?php echo $row[0] ;?>"> <?php echo $row[0]; ?> - <?php echo $row[1]; ?> </option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </td>
                                        <td>
                                            <input type="hidden" id="hdebit<?php echo 'fix'; ?>" value="0" readonly>
                                            <input type="number" name="debit<?php echo 'fix'; ?>" id="debit<?php echo 'fix'; ?>" style="text-align: right;" placeholder="0">
                                        </td>
                                        <td>
                                            <select name="kbb<?php echo 'fix'; ?>" id="kba2<?php echo 'fix'; ?>" class="browser-default">
                                              <option value=''>- <?php echo lang('Pilih Akun'); ?> -</option>
                                                <?php
                                                $sql = "Select * from dbo.AccountMax where substring(KodeAccount,1,6) not in('1.1.01') order by KodeAccount ASC";
                                                $query = sqlsrv_query($conn, $sql);
                                                while($row  = sqlsrv_fetch_array( $query, SQLSRV_FETCH_NUMERIC)) { ?>
                                                        <option value="<?php echo $row[0] ;?>"> <?php echo $row[0]; ?> - <?php echo $row[1]; ?> </option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </td>
                                        <td>
                                            <input type="hidden" id="hkredit<?php echo 'fix'; ?>" value="0" readonly>
                                            <input type="number" name="kredit<?php echo 'fix'; ?>" id="kredit<?php echo 'fix'; ?>" style="text-align: right;" placeholder="0">
                                        </td>
                                    </tr>


                                    <script type="text/javascript">
                                            $('#kba1fix').change(function(){
                                            var kba = $('#kba1fix').val();
                                            //alert ('kba');
                                          if(kba != "" ){
                                             $('#kba2fix').prop('disabled',true);
                                          }else{
                                            $('#kba2fix').prop('disabled',false);
                                          }
                                        });

                                          $('#kba2fix').change(function(){
                                            var kba = $('#kba2fix').val();
                                           if(kba != "" ){
                                              $('#kba1fix').prop('disabled',true);
                                          } else {
                                            $('#kba1fix').prop('disabled',false);
                                          }
                                        });

                                        $('#debitfix').keyup(function(){
                                            var amount = $('#debitfix').val();
                                            if(amount > 0){
                                                $('#kreditfix').prop('readonly', 'disabled');
                                                $('#kreditfix').val('0');
                                            }
                                            if(amount == 0){
                                                $('#kreditfix').prop('readonly', false);
                                            }
                                            if(amount.length == 0){
                                                $('#kreditfix').prop('readonly', false);
                                            }
                                        });

                                        $('#kreditfix').keyup(function(){
                                            var amount = $('#kreditfix').val();
                                            if(amount > 0){
                                                $('#debitfix').prop('readonly', 'disabled');
                                                $('#debitfix').val('0');
                                            }
                                            if(amount == 0){
                                                $('#debitfix').prop('readonly', false);
                                            }
                                            if(amount.length == 0){
                                                $('#kreditfix').prop('readonly', false);
                                            }
                                        });
                                    </script>


                                   



                                    <script type="text/javascript">
                                        $('#debit<?php echo $a; ?>').keyup(function(){
                                            var amount = $('#debit<?php echo $a; ?>').val();
                                            if(amount > 0){
                                                $('#kredit<?php echo $a; ?>').prop('readonly', 'disabled');
                                                $('#kredit<?php echo $a; ?>').val('0');
                                            }
                                            if(amount == 0){
                                                $('#kredit<?php echo $a; ?>').prop('readonly', false);
                                            }
                                            if(amount.length == 0){
                                                $('#kredit<?php echo $a; ?>').prop('readonly', false);
                                            }
                                        });

                                        $('#kredit<?php echo $a; ?>').keyup(function(){
                                            var amount = $('#kredit<?php echo $a; ?>').val();
                                            if(amount > 0){
                                                $('#debit<?php echo $a; ?>').prop('readonly', 'disabled');
                                                $('#debit<?php echo $a; ?>').val('0');
                                            }
                                            if(amount == 0){
                                                $('#debit<?php echo $a; ?>').prop('readonly', false);
                                            }
                                            if(amount.length == 0){
                                                $('#kredit<?php echo $x; ?>').prop('readonly', false);
                                            }
                                        });
                                    </script>


                                    <script type="text/javascript">
                                        $('#debit<?php echo 'fix'; ?>').keyup(function(){
                                            var amount = $('#debit<?php echo 'fix'; ?>').val();
                                            var amounth = $('#hdebit<?php echo 'fix'; ?>').val();
                                            var totdebit = $('#totdebit').val();
                                            if(amount > 0){
                                                var amounth2 = $('#hdebit<?php echo 'fix'; ?>').val();

                                                total = (parseFloat(totdebit) - parseFloat(amounth2)) + parseFloat(amount);
                                                $('#totdebit').val(total);

                                                $('#hdebit<?php echo 'fix'; ?>').val(amount);
                                            }

                                            if(amount == 0){
                                                var amounth = $('#hdebit<?php echo 'fix'; ?>').val();
                                                var totdebit = $('#totdebit').val();

                                                total = parseFloat(totdebit) - parseFloat(amounth);
                                                $('#totdebit').val(total);

                                                $('#hdebit<?php echo 'fix'; ?>').val(amount);
                                            }

                                            if(amount.length == 0){
                                                $('#hdebit<?php echo 'fix'; ?>').val(0);
                                            }

                                            selisih();
                                        });

                                        $('#kredit<?php echo 'fix'; ?>').keyup(function(){
                                            var amount = $('#kredit<?php echo 'fix'; ?>').val();
                                            var amounth = $('#hkredit<?php echo 'fix'; ?>').val();
                                            var totkredit = $('#totkredit').val();
                                            if(amount > 0){
                                                var amounth2 = $('#hkredit<?php echo 'fix'; ?>').val();

                                                total = (parseFloat(totkredit) - parseFloat(amounth2)) + parseFloat(amount);
                                                $('#totkredit').val(total);

                                                $('#hkredit<?php echo 'fix'; ?>').val(amount);
                                            }

                                            if(amount == 0){
                                                var amounth = $('#hkredit<?php echo 'fix'; ?>').val();
                                                var totkredit = $('#totkredit').val();

                                                total = parseFloat(totkredit) - parseFloat(amounth);
                                                $('#totkredit').val(total);

                                                $('#hkredit<?php echo 'fix'; ?>').val(amount);
                                            }

                                            if(amount.length == 0){
                                                $('#hkredit<?php echo 'fix'; ?>').val(0);
                                            }

                                            selisih();
                                        });
                                    </script>



                                     <?php
                                    $ct = "select count (*) from dbo.JurnalTrans where TransactionNumber='$jurnal'";
                                    $ct2 = sqlsrv_query($conn, $ct);
                                    $ct3 = sqlsrv_fetch_array( $ct2, SQLSRV_FETCH_NUMERIC);


                                    for($a=$ct3[0];$a<19;$a++){
                                      $kredit = '';
                                      $kredit2 = '';
                                      $debit = '';
                                      $debit2 = '';
                                      if($a>0){
                                          $kredit = 'kredit';
                                          $kredit2 = 'disabled';
                                          $debit = 'debit';
                                          $debit2 = 'disabled';
                                      }
                                      ?>
                                      


                                    <!-- Memakai $a -->
                                    <tr>
                                        <td>
                                            <select name="kba<?php echo $a; ?>" id="kba1<?php echo $a; ?>" class="browser-default">
                                              <option value=''>- <?php echo lang('Pilih Akun'); ?> -</option>
                                                <?php
                                                $sql = "Select * from dbo.AccountMax where substring(KodeAccount,1,6) not in('1.1.01') order by KodeAccount ASC";
                                                $query = sqlsrv_query($conn, $sql);
                                                while($row  = sqlsrv_fetch_array( $query, SQLSRV_FETCH_NUMERIC)) { ?>
                                                        <option value="<?php echo $row[0] ;?>"> <?php echo $row[0]; ?> - <?php echo $row[1]; ?> </option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </td>
                                        <td>
                                            <input type="hidden" id="hdebit<?php echo $a; ?>" value="0" readonly>
                                            <input type="number" name="debit<?php echo $a; ?>" id="debit<?php echo $a; ?>" style="text-align: right;" placeholder="0">
                                        </td>
                                        <td>
                                            <select name="kbb<?php echo $a; ?>" id="kba2<?php echo $a; ?>" class="browser-default">
                                              <option value=''>- <?php echo lang('Pilih Akun'); ?> -</option>
                                                <?php
                                                $sql = "Select * from dbo.AccountMax where substring(KodeAccount,1,6) not in('1.1.01') order by KodeAccount ASC";
                                                $query = sqlsrv_query($conn, $sql);
                                                while($row  = sqlsrv_fetch_array( $query, SQLSRV_FETCH_NUMERIC)) { ?>
                                                        <option value="<?php echo $row[0] ;?>"> <?php echo $row[0]; ?> - <?php echo $row[1]; ?> </option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </td>
                                        <td>
                                            <input type="hidden" id="hkredit<?php echo $a; ?>" value="0" readonly>
                                            <input type="number" name="kredit<?php echo $a; ?>" id="kredit<?php echo $a; ?>" style="text-align: right;" placeholder="0">
                                        </td>
                                    </tr>
                                     <script type="text/javascript">
                                            $('#kba1<?php echo $a; ?>').change(function(){
                                            var kba = $('#kba1<?php echo $a; ?>').val();
                                            //alert ('kba');
                                          if(kba != "" ){
                                             $('#kba2<?php echo $a; ?>').prop('disabled',true);
                                          }else{
                                            $('#kba2<?php echo $a; ?>').prop('disabled',false);
                                          }
                                        });

                                          $('#kba2<?php echo $a; ?>').change(function(){
                                            var kba = $('#kba2<?php echo $a; ?>').val();
                                           if(kba != "" ){
                                              $('#kba1<?php echo $a; ?>').prop('disabled',true);
                                          } else {
                                            $('#kba1<?php echo $a; ?>').prop('disabled',false);
                                          }
                                        });

                                        $('#debit<?php echo $a; ?>').keyup(function(){
                                            var amount = $('#debit<?php echo $a; ?>').val();
                                            if(amount > 0){
                                                $('#kredit<?php echo $a; ?>').prop('readonly', 'disabled');
                                                $('#kredit<?php echo $a; ?>').val('0');
                                            }
                                            if(amount == 0){
                                                $('#kredit<?php echo $a; ?>').prop('readonly', false);
                                            }
                                            if(amount.length == 0){
                                                $('#kredit<?php echo $a; ?>').prop('readonly', false);
                                            }
                                        });

                                        $('#kredit<?php echo $a; ?>').keyup(function(){
                                            var amount = $('#kredit<?php echo $a; ?>').val();
                                            if(amount > 0){
                                                $('#debit<?php echo $a; ?>').prop('readonly', 'disabled');
                                                $('#debit<?php echo $a; ?>').val('0');
                                            }
                                            if(amount == 0){
                                                $('#debit<?php echo $a; ?>').prop('readonly', false);
                                            }
                                            if(amount.length == 0){
                                                $('#kredit<?php echo $a; ?>').prop('readonly', false);
                                            }
                                        });
                                    </script>




                                    
                                    <script type="text/javascript">
                                        $('#debit<?php echo $a; ?>').keyup(function(){
                                            var amount = $('#debit<?php echo $a; ?>').val();
                                            var amounth = $('#hdebit<?php echo $a; ?>').val();
                                            var totdebit = $('#totdebit').val();
                                            if(amount > 0){
                                                var amounth2 = $('#hdebit<?php echo $a; ?>').val();

                                                total = (parseFloat(totdebit) - parseFloat(amounth2)) + parseFloat(amount);
                                                $('#totdebit').val(total);

                                                $('#hdebit<?php echo $a; ?>').val(amount);
                                            }

                                            if(amount == 0){
                                                var amounth = $('#hdebit<?php echo $a; ?>').val();
                                                var totdebit = $('#totdebit').val();

                                                total = parseFloat(totdebit) - parseFloat(amounth);
                                                $('#totdebit').val(total);

                                                $('#hdebit<?php echo $a; ?>').val(amount);
                                            }

                                            if(amount.length == 0){
                                                $('#hdebit<?php echo $a; ?>').val(0);
                                            }

                                            selisih();
                                        });

                                        $('#kredit<?php echo $a; ?>').keyup(function(){
                                            var amount = $('#kredit<?php echo $a; ?>').val();
                                            var amounth = $('#hkredit<?php echo $a; ?>').val();
                                            var totkredit = $('#totkredit').val();
                                            if(amount > 0){
                                                var amounth2 = $('#hkredit<?php echo $a; ?>').val();

                                                total = (parseFloat(totkredit) - parseFloat(amounth2)) + parseFloat(amount);
                                                $('#totkredit').val(total);

                                                $('#hkredit<?php echo $a; ?>').val(amount);
                                            }

                                            if(amount == 0){
                                                var amounth = $('#hkredit<?php echo $a; ?>').val();
                                                var totkredit = $('#totkredit').val();

                                                total = parseFloat(totkredit) - parseFloat(amounth);
                                                $('#totkredit').val(total);

                                                $('#hkredit<?php echo $a; ?>').val(amount);
                                            }

                                            if(amount.length == 0){
                                                $('#hkredit<?php echo $a; ?>').val(0);
                                            }

                                            selisih();
                                        });

                                        function selisih(){
                                          var a = $('#totdebit').val();
                                          var b = $('#totkredit').val();

                                          var selisih = 0;
                                          sisa = parseFloat(a) - parseFloat(b);
                                          if(sisa < 0){
                                              selisih = sisa * -1;
                                          }
                                          else{
                                              selisih = sisa;
                                          }

                                          var kata = '';
                                          if(selisih == 0){
                                              kata = 'Simpan';
                                              $('#save').prop('disabled', false);
                                          }
                                          else{
                                            kata = 'Selisih '+selisih;
                                            $('#save').prop('disabled', true);
                                          }

                                          $('#kata').html(kata);
                                        }
                                    </script>
                                  <?php } ?>
                                  <tr>
                                        <td><b>Total Debet</b></td>
                                        <td>
                                            <b><input type="number" id="totdebit" class="validate" value="<?php echo ($tot); ?>" style="text-align: right;" placeholder="0" readonly></b>
                                        </td>
                                        <td><b>Total Kredit</b></td>
                                        <td>
                                            <b><input type="number" id="totkredit" class="validate" value="<?php echo ($tot2); ?>" style="text-align: right;" placeholder="0" readonly></b>
                                        </td>
                                    </tr>
                                </body>
                                <tfoot>
                                    <!-- <tr>
                                       <td><b>Total Debet</b></td>
                                        <td>
                                            <b><input type="number" id="totdebit" class="validate" value="<?php echo ($tot); ?>" style="text-align: right;" placeholder="0" readonly></b>
                                        </td>
                                        <td><b>Total Kredit</b></td>
                                        <td>
                                            <b><input type="number" id="totkredit" class="validate" value="<?php echo ($tot2); ?>" style="text-align: right;" placeholder="0" readonly></b>
                                        </td>
                                    </tr> -->
                                </tfoot>
                            </table>

                            <div style="bottom: 0;position: fixed;width:96%;text-align: center;left: 50%;margin-right: -50%;transform: translate(-50%, -50%);">
                                <button type="submit" name="save" id="save" class="btn btn-large primary-color width-100 waves-effect waves-light" disabled>
                                  <span id="kata">Simpan</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

    </div>
</div>




<?php include('footer.php'); ?>
