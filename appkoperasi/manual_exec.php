<form action='' method="post">
    <button type="submit" name="a">Basic Saving Billing</button><br><br>
    <button type="submit" name="b">Regular Saving Interest Daily</button><br><br>
    <button type="submit" name="c">Regular Saving Interest Monthly</button><br><br>
    <button type="submit" name="d">Time Deposit Interest</button><br><br>
    <button type="submit" name="e">Loan Billing</button><br><br>

    <?php
    include("connect.php");

    if(isset($_POST['a'])){
        $sql = "exec dbo.ProsesBilingBasicSaving";
        $stmt = sqlsrv_query($conn, $sql);
        if($stmt){
            echo 'Success generate basic saving billing';
        }
        else{
            echo 'Failed generate basic saving billing';
        }
    }

    if(isset($_POST['b'])){
        $sql = "exec dbo.ProsesBungaRegSavingHarian";
        $stmt = sqlsrv_query($conn, $sql);
        if($stmt){
            echo 'Success generate regular saving interest daily';
        }
        else{
            echo 'Failed generate regular saving interest daily';
        }
    }

    if(isset($_POST['c'])){
        $sql = "exec dbo.ProsesBungaRegularSavingBulanan";
        $stmt = sqlsrv_query($conn, $sql);
        if($stmt){
            echo 'Success generate regular saving interest mothly';
        }
        else{
            echo 'Failed generate regular saving interest monthly';
        }
    }

    if(isset($_POST['d'])){
        $sql = "exec dbo.ProsesTimeDepositHitungBunga";
        $stmt = sqlsrv_query($conn, $sql);
        if($stmt){
            echo 'Success generate time deposit interest transfer';
        }
        else{
            echo 'Failed generate time deposit interest transfer';
        }
    }

    if(isset($_POST['e'])){
        $sql = "exec dbo.ProsesBillingLoan";
        $stmt = sqlsrv_query($conn, $sql);
        if($stmt){
            echo 'Success generate loan billing';
        }
        else{
            echo 'Failed generate loan billing';
        }
    }
    ?>

</form>
