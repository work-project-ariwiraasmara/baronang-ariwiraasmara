<?php
@session_start();
error_reporting(0);
require('header.php');
require('sidebar-right.php');
require('sidebar-left.php');
?>

    <div class="animated fadeinup delay-1">
        <div class="page-content txt-black">

            <?php
            if(@$_GET['type'] == 'ioqty') { ?>
                <h1 class="m-t-30 uppercase"> Laporan Keluar Masuk Kendaraan </h1>

                <div class="m-t-10">
                   <a href="reportmember.php">
                        <button class="btn btn-large primary-color waves-efeect waves-light">
                            <i class="ion-android-arrow-back"></i>
                        </button>
                    </a> 

                    <a href="?type=history_ioqty&loc=<?php echo @$_GET['loc']; ?>&bulan=<?php echo @$_GET['bulan']; ?>&tahun=<?php echo @$_GET['tahun']; ?>">
                        <button class="btn btn-large waves-effect waves-light primary-color">History</button>
                    </a>
                </div>
                <?php
            }
            else if(@$_GET['type'] == 'history_ioqty') { ?>
                <h1 class="m-t-30 uppercase"> History Laporan Keluar Masuk Kendaraan </h1>

                <a href="?type=ioqty&loc=<?php echo @$_GET['loc']; ?>&bulan=<?php echo @$_GET['bulan']; ?>&tahun=<?php echo @$_GET['tahun']; ?>">
                    <button class="btn btn-large primary-color waves-efeect waves-light">
                        <i class="ion-android-arrow-back"></i>
                    </button>
                </a>

                <div class="m-t-30"></div>
                <?php
            }
            else if(@$_GET['type'] == 'aktifasi') { ?>
                <h1 class="m-t-30 uppercase"> Laporan Aktifasi </h1>

                <a href="reportmember.php">
                    <button class="btn btn-large primary-color waves-efeect waves-light">
                        <i class="ion-android-arrow-back"></i>
                    </button>
                </a> 
                <?php
            }
            else if(@$_GET['type'] == 'member') { ?> 
                <h1 class="m-t-30 uppercase"> MeLaporan mber </h1>

                <a href="reportmember.php">
                    <button class="btn btn-large primary-color waves-efeect waves-light">
                        <i class="ion-android-arrow-back"></i>
                    </button>
                </a> 
                <?php 
            }
            else { ?>
                <div class="row">
                    <div class="col s4">
                        <button id="btn_inoutqty" class="btn btn-large width-100 primary-color waves-efeect waves-light">In Out</button>
                    </div>

                    <div class="col s4">
                        <button id="btn_aktifasi" class="btn btn-large width-100 primary-color waves-efeect waves-light">Aktifasi</button>
                    </div>

                    <div class="col s4">
                        <button id="btn_member" class="btn btn-large width-100 primary-color waves-efeect waves-light">Member</button>
                    </div>
                </div>
            <?php 
            }
            ?>

            
            <?php
            if(@$_GET['type'] == 'history_ioqty') { ?>
                    <div class="row m-t-30">
                        <div class="col s6">
                            <b>Nomor Kartu</b> : <br>
                            <select id="nocard" name="nocard" class="browser-default">
                                <?php
                                $snc = "SELECT CardNo from [dbo].[MemberCard] where Status='1' and isLink='1' order by MemberID ASC, CardNo ASC";
                                $qnc = sqlsrv_query($conn, $snc);
                                while($dnc = sqlsrv_fetch_array($qnc)) { ?>
                                    <option value="<?php echo $dnc[0]; ?>"><?php echo $dnc[0]; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>

                        <div class="col s6 m-t-10">
                            <button id="btn_nocard" class="btn btn-large width-100 primary-color waves-efeect waves-light">OK</button>
                        </div>
                    </div>

                    <script type="text/javascript">
                        $('#btn_nocard').click(function(){
                            var nocard = $('#nocard :selected').val();
                            window.location.href = "?type=history_ioqty&loc=<?php echo @$_GET['loc']; ?>&bulan=<?php echo @$_GET['bulan']; ?>&tahun=<?php echo @$_GET['tahun']; ?>&crd="+nocard;
                        });
                    </script>
                <?php
            }
            ?>

            <div class="m-t-30 hide" id="inoutqty">
                <form action="" method="post">
                    <div class="row">
                        <div class="col s4">
                            <b>Lokasi</b> : 
                            <select class="browser-default" id="loc_ioqty" name="loc_ioqty">
                                <?php
                                $sq_ioqty = "SELECT LocationID, Nama from [dbo].[LocationMerchant] order by Nama ASC";
                                $qr_ioqty = sqlsrv_query($conn, $sq_ioqty);
                                while($dr_ioqty = sqlsrv_fetch_array($qr_ioqty)) { ?>
                                    <option value="<?php echo $dr_ioqty[0]; ?>"><?php echo $dr_ioqty[1]; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>

                        <div class="col s4">
                            <b>Bulan</b> : <br>
                            <select class="browser-default" id="bulan_ioqty" name="bulan_ioqty">
                                <?php
                                for($x = 1; $x < 13; $x++) { 
                                    $dtn = date('Y').'-'.$x.'-1';
                                    ?>
                                    <option value="<?php echo $x; ?>"><?php echo date('F', strtotime($dtn)); ?></option>
                                    <?php                                    
                                }
                                ?>
                            </select>
                        </div>

                        <div class="col s4">
                        <b>Tahun</b> : <br>
                            <select class="browser-default" id="tahun_ioqty" name="tahun_ioqty">
                                <?php
                                for($x = date('Y'); $x >= 2017; $x--) { 
                                    $dtn = date('Y').'-'.$x.'-1';
                                    ?>
                                    <option value="<?php echo $x; ?>"><?php echo $x; ?></option>
                                    <?php                                    
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <button type="button" name="fbtn_ioqty" id="fbtn_ioqty" class="btn btn-large width-100 primary-color waves-efeect waves-light m-t-10">OK</button>
                    <script type="text/javascript">
                        $('#fbtn_ioqty').click(function(){
                            var loc = $('#loc_ioqty :selected').val();
                            var bulan = $('#bulan_ioqty :selected').val();
                            var tahun = $('#tahun_ioqty :selected').val();
                            window.location.href = "?type=ioqty&loc="+loc+"&bulan="+bulan+"&tahun="+tahun;
                        });
                    </script>
                </form>
            </div>

            <div class="m-t-30 hide" id="aktifasi">
                <form action="" method="post">
                    <?php /*
                    <div class="row">
                         <div class="col s6">
                            <b>Lokasi</b> : 
                            <select class="browser-default" id="loc_aktifasi" name="loc_akaktifasi">
                                <?php
                                $sq_ioqty = "SELECT LocationID, Nama from [dbo].[LocationMerchant] order by Nama ASC";
                                $qr_ioqty = sqlsrv_query($conn, $sq_ioqty);
                                while($dr_ioqty = sqlsrv_fetch_array($qr_ioqty)) { ?>
                                    <option value="<?php echo $dr_ioqty[0]; ?>"><?php echo $dr_ioqty[1]; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>

                        <div class="col s6">
                            <b>Kendaraan</b> : <br>
                            <select class="browser-default" id="vhcl_aktifasi" name="vhcl_akaktifasi">
                                <?php
                                $sq_vhcl = "SELECT VehicleID, Name from [dbo].[VehicleType] where Status='1' order by Name ASC";
                                $qr_vhcl = sqlsrv_query($conn, $sq_vhcl);
                                while($dr_vhcl = sqlsrv_fetch_array($qr_vhcl)) { ?>
                                    <option value="<?php echo $dr_vhcl[0]; ?>"><?php echo $dr_vhcl[1]; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    */ ?>

                    <div class="row">
                        <div class="col s6">
                            <b>Bulan</b> : <br>
                            <select class="browser-default" id="bulan_aktifasi" name="bulan_aktifasi">
                                <?php
                                for($x = 1; $x < 13; $x++) { 
                                    $dx = '1-'.$x.'-'.date('Y');
                                    ?>
                                    <option value="<?php echo date('m-t', strtotime($dx)); ?>"><?php echo date('F', strtotime($dx)); ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>

                        <div class="col s6">
                            <b>Tahun</b> : <br>
                            <select class="browser-default" id="tahun_aktifasi" name="tahun_aktifasi">
                                <?php
                                for($x = (int)date('Y'); $x > 2014; $x--) { ?>
                                    <option value="<?php echo $x; ?>"><?php echo $x; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <button type="button" name="fbtn_aktifasi" id="fbtn_aktifasi" class="btn btn-large width-100 primary-color waves-efeect waves-light m-t-10">OK</button>
                    <script type="text/javascript">
                        $('#fbtn_aktifasi').click(function(){
                            var bulan = $('#bulan_aktifasi :selected').val();
                            var tahun = $('#tahun_aktifasi :selected').val();
                            window.location.href = "?type=aktifasi&bulan="+bulan+"&tahun="+tahun;
                        });
                    </script>
                </form>
            </div>

            <div class="m-t-30 hide" id="member">
                <form action="" method="post">
                    <div class="row">
                         <div class="col s6">
                            <b>Lokasi</b> : 
                            <select class="browser-default" id="loc_member" name="loc_member">
                                <?php
                                $sq_ioqty = "SELECT LocationID, Nama from [dbo].[LocationMerchant] order by Nama ASC";
                                $qr_ioqty = sqlsrv_query($conn, $sq_ioqty);
                                while($dr_ioqty = sqlsrv_fetch_array($qr_ioqty)) { ?>
                                    <option value="<?php echo $dr_ioqty[0]; ?>"><?php echo $dr_ioqty[1]; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>

                        <div class="col s6">
                            <b>Kendaraan</b> : <br>
                            <select class="browser-default" id="vhcl_member" name="vhcl_member">
                                <?php
                                $sq_vhcl = "SELECT VehicleID, Name from [dbo].[VehicleType] where Status='1' order by Name ASC";
                                $qr_vhcl = sqlsrv_query($conn, $sq_vhcl);
                                while($dr_vhcl = sqlsrv_fetch_array($qr_vhcl)) { ?>
                                    <option value="<?php echo $dr_vhcl[0]; ?>"><?php echo $dr_vhcl[1]; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col s6">
                            <b>Bulan</b> : <br>
                            <select class="browser-default" id="bulan_member" name="bulan_member">
                                <?php
                                for($x = 1; $x < 13; $x++) { 
                                    $dx = '1-'.$x.'-'.date('Y');
                                    ?>
                                    <option value="<?php echo $x; ?>"><?php echo date('F', strtotime($dx)); ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>

                        <div class="col s6">
                            <b>Tahun</b> : <br>
                            <select class="browser-default" id="tahun_member" name="tahun_member">
                                <?php
                                for($x = (int)date('Y'); $x > 2014; $x--) { ?>
                                    <option value="<?php echo $x; ?>"><?php echo $x; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <button type="button" name="fbtn_member" id="fbtn_member" class="btn btn-large width-100 primary-color waves-efeect waves-light m-t-10">OK</button>
                    <script type="text/javascript">
                        $('#fbtn_member').click(function(){
                            var loc = $('#loc_member :selected').val();
                            var vhcl = $('#vhcl_member :selected').val();
                            var bulan = $('#bulan_member :selected').val();
                            var tahun = $('#tahun_member :selected').val();

                            window.location.href = "?type=member&loc="+loc+"&vhcl="+vhcl+"&bulan="+bulan+"&tahun="+tahun;
                        });
                    </script>
                </form>
            </div>

                <?php
                $fbtn_ioqty = @$_POST['fbtn_ioqty'];
                $fbtn_aktifasi = @$_POST['fbtn_aktifasi'];
                $fbtn_member = @$_POST['fbtn_member'];
                ?>

                <div class="table-responsive m-t-30">
                    <table class="table txt-black" width="100%">
                        <thead>
                            
                                <?php
                                //if(isset($fbtn_ioqty)) { 
                                if(@$_GET['type'] == 'ioqty') {
                                    /*
                                    <th>Tanggal</th>
                                    <th>Lokasi</th>
                                    <th>Kendaraan</th>
                                    <th>Jumlah Masuk</th>
                                    <th>Jumlah Keluar</th>
                                    <th>Pendapatan</th>
                                    */ 
                                    ?>
                                    <tr>
                                        <th rowspan="2" style="text-align: center;">Tanggal</th>
                                        <th colspan="3" style="text-align: center;">Mobil</th>
                                        <th colspan="3" style="text-align: center;">Motor</th>
                                        <th colspan="3" style="text-align: center;">Truck</th>
                                        <th colspan="3" style="text-align: center;">Valet</th>
                                        <th colspan="3" style="text-align: center;">VIP Mobil</th>
                                        <th colspan="3" style="text-align: center;">VIP Motor</th>
                                    </tr>

                                    <tr>
                                        <?php // mobil ?>
                                        <th>In</th>
                                        <th>Out</th>
                                        <th>Area</th>

                                        <?php // motor ?>
                                        <th>In</th>
                                        <th>Out</th>
                                        <th>Area</th>

                                        <?php // truk ?>
                                        <th>In</th>
                                        <th>Out</th>
                                        <th>Area</th>

                                        <?php // valet ?>
                                        <th>In</th>
                                        <th>Out</th>
                                        <th>Area</th>

                                        <?php // vip mobil ?>
                                        <th>In</th>
                                        <th>Out</th>
                                        <th>Area</th>

                                        <?php // vip motor ?>
                                        <th>In</th>
                                        <th>Out</th>
                                        <th>Area</th>
                                    </tr>
                                    <?php
                                } 
                                else if(@$_GET['type'] == 'history_ioqty') { ?>
                                    <tr>
                                        <th>Tanggal & Time In</th>
                                        <th>Lokasi Masuk</th>
                                        <th>Tanggal & Time Out</th>
                                        <th>Lokasi Keluar</th>
                                        <th>Status</th>
                                    </tr>
                                    <?php
                                }
                                //else if(isset($fbtn_aktifasi)) { 
                                else if(@$_GET['type'] == 'aktifasi') { ?>
                                    <tr>
                                        <th>Nama</th>
                                        <th>Telp</th>
                                        <th>Email</th>
                                        <th>Alamat</th>
                                        <th>Nomor Kartu</th>
                                        <th>Jenis Kendaraan</th>
                                        <th>Tanggal Daftar</th>
                                    </tr>
                                    <?php
                                }
                                //else if(isset($fbtn_member)) { 
                                else if(@$_GET['type'] == 'member') { ?>
                                    <tr>
                                        <th>Nama</th>
                                        <th>Telp</th>
                                        <th>Email</th>
                                        <th>Alamat</th>
                                        <th>Nomor Kartu</th>
                                        <th>Bulan</th>
                                        <th>Tahun</th>
                                        <th>Jenis Kendaraan</th>
                                        <th>Lokasi</th>
                                    </tr>
                                    <?php
                                }
                                ?>
                            
                        </thead>

                        <tbody>
                            
                                <?php
                                if(@$_GET['type'] == 'ioqty') {
                                //if(isset($fbtn_ioqty)) {
                                    $loc    = @$_GET['loc'];
                                    $bulan  = @$_GET['bulan'];
                                    $tahun  = @$_GET['tahun'];
                                    $ld     = date('t', strtotime( $tahun.'-'.$bulan.'-1' ));

                                    $inarea_mobil = 0;
                                    $inarea_motor = 0;
                                    $inarea_truk = 0;
                                    $inarea_valet = 0;
                                    $inarea_vipmobil = 0;
                                    $inarea_vipmotor = 0;

                                    //echo $ld.'<br>';
                                    for($x = 1; $x <= $ld; $x++) {

                                        //include("connect.php");
                                        $tgl = $tahun.'-'.$bulan.'-'.$x;
                                        $sql = "SELECT TOP 1 (SELECT Count(TimeIn) from [dbo].[ParkingList] where LocationIn='$loc' and VehicleType='C' and Convert(date, TimeIn) = '$tgl' ),
                                                             (SELECT Count(TimeOut) from [dbo].[ParkingList] where LocationIn='$loc' and VehicleType='C' and Convert(date, TimeOut) = '$tgl'),
                                                             (SELECT Count(TimeIn) from [dbo].[ParkingList] where LocationIn='$loc' and VehicleType='B' and Convert(date, TimeIn) = '$tgl' ),
                                                             (SELECT Count(TimeOut) from [dbo].[ParkingList] where LocationIn='$loc' and VehicleType='B' and Convert(date, TimeOut) = '$tgl' ),
                                                             (SELECT Count(TimeIn) from [dbo].[ParkingList] where LocationIn='$loc' and VehicleType='T' and Convert(date, TimeIn) = '$tgl' ),
                                                             (SELECT Count(TimeOut) from [dbo].[ParkingList] where LocationIn='$loc' and VehicleType='T' and Convert(date, TimeOut) = '$tgl' ),
                                                             (SELECT Count(TimeIn) from [dbo].[ParkingList] where LocationIn='$loc' and VehicleType='V' and Convert(date, TimeIn) = '$tgl' ),
                                                             (SELECT Count(TimeOut) from [dbo].[ParkingList] where LocationIn='$loc' and VehicleType='V' and Convert(date, TimeOut) = '$tgl' ),
                                                             (SELECT Count(TimeIn) from [dbo].[ParkingList] where LocationIn='$loc' and VehicleType='P' and Convert(date, TimeIn) = '$tgl' ),
                                                             (SELECT Count(TimeOut) from [dbo].[ParkingList] where LocationIn='$loc' and VehicleType='P' and Convert(date, TimeOut) = '$tgl' ),
                                                             (SELECT Count(TimeIn) from [dbo].[ParkingList] where LocationIn='$loc' and VehicleType='Z' and Convert(date, TimeIn) = '$tgl' ),
                                                             (SELECT Count(TimeOut) from [dbo].[ParkingList] where LocationIn='$loc' and VehicleType='Z' and Convert(date, TimeOut) = '$tgl')
                                                from [KOPX700097].[dbo].[ParkingList] where LocationIn='$loc' and Convert(date, TimeIn) = '$tgl'";
                                        $query = sqlsrv_query($conn, $sql);
                                        $data = sqlsrv_fetch_array($query);

                                        
                                        // mobil
                                        if($data[0] == '') { $inmobil = 0; } else { $inmobil = $data[0]; }           
                                        if($data[1] == '') { $outmobil = 0; } else { $outmobil = $data[1]; }
                                        $inarea_mobil = $inarea_mobil + $inmobil - $outmobil;
                                        if($inarea_mobil < 0) { $inarea_mobil = $inarea_mobil * -1; }

                                        // motor
                                        if($data[2] == '') { $inmotor = 0; } else { $inmotor = $data[0]; }           
                                        if($data[3] == '') { $outmotor = 0; } else { $outmotor = $data[1]; }
                                        $inarea_motor = $inarea_motor + $inmotor - $outmotor;
                                        if($inarea_motor < 0) { $inarea_motor = $inarea_motor * -1; }

                                        // truk
                                        if($data[4] == '') { $intruk = 0; } else { $intruk = $data[0]; }             
                                        if($data[5] == '') { $outtruk = 0; } else { $outtruk = $data[1]; }
                                        $inarea_truk = $inarea_truk + $intruk - $outtruk;
                                        if($inarea_truk < 0) { $inarea_truk = $inarea_truk * -1; }

                                        // valet
                                        if($data[8] == '') { $invalet = 0; } else { $invalet = $data[0]; }           
                                        if($data[9] == '') { $outvalet = 0; } else { $outvalet = $data[1]; }
                                        $inarea_valet = $inarea_valet + $invalet - $outvalet;
                                        if($inarea_valet < 0) { $inarea_valet = $inarea_valet * -1; }

                                        // vip mobil
                                        if($data[6] == '') { $invipmobil = 0; } else { $invipmobil = $data[0]; }     
                                        if($data[7] == '') { $outvipmobil = 0; } else { $outvipmobil = $data[1]; }
                                        $inarea_vipmobil = $inarea_vipmobil + $invipmobil - $outvipmobil;
                                        if($inarea_vipmobil < 0) { $inarea_vipmobil = $inarea_vipmobil * -1; }

                                        // vip motor
                                        if($data[10] == '') { $invipmotor = 0; } else { $invipmotor = $data[0]; }    
                                        if($data[11] == '') { $outvipmotor = 0; } else { $outvipmotor = $data[1]; }
                                        $inarea_vipmotor = $inarea_vipmotor + $invipmotor - $outvipmotor;
                                        if($inarea_vipmotor < 0) { $inarea_vipmotor = $inarea_vipmotor * -1; }
                                        
                                        ?>
                                        <tr>
                                            <td style="text-align: center;"><?php echo $x//.'<br>'.$sql; ?></td>


                                            <?php // mobil ?>
                                            <td><?php echo $inmobil; ?></td>
                                            <td><?php echo $outmobil; ?></td>
                                            <td><?php echo $inarea_mobil; ?></td>

                                            <?php // motor ?>
                                            <td><?php echo $inmotor; ?></td>
                                            <td><?php echo $outmotor; ?></td>
                                            <td><?php echo $inarea_motor; ?></td>

                                            <?php // truk ?>
                                            <td><?php echo $intruk; ?></td>
                                            <td><?php echo $outtruk; ?></td>
                                            <td><?php echo $inarea_truk; ?></td>

                                            <?php // valet ?>
                                            <td><?php echo $invalet; ?></td>
                                            <td><?php echo $outvalet; ?></td>
                                            <td><?php echo $inarea_valet; ?></td>

                                            <?php // vip mobil ?>
                                            <td><?php echo $invipmobil; ?></td>
                                            <td><?php echo $outvipmobil; ?></td>
                                            <td><?php echo $inarea_vipmobil; ?></td>

                                            <?php // vip motor ?>
                                            <td><?php echo $invipmotor; ?></td>
                                            <td><?php echo $outvipmotor; ?></td>
                                            <td><?php echo $inarea_vipmotor; ?></td>

                                        </tr>
                                        <?php
                                    }

                                    // [dbo].[ParkingList]
                                    

                                    /*
                                    $lokasi = @$_POST['loc_ioqty'];
                                    $vhcl   = @$_POST['vhcl_ioqty'];

                                    $sql1 = "SELECT a.TimeIn,
                                                    b.Nama,
                                                    c.Name, 
                                                    (SELECT Count(TimeIn) from [dbo].[ParkingList] where LocationIn = '$lokasi' and VehicleType = '$vhcl' and TimeIn between '$tgl 00:00:00.000' and '$tgl 23:59:59.999' and MONTH(TimeIn) = 1) as Masuk, 
                                                    (SELECT Count(TimeOut) from [dbo].[ParkingList] where LocationIn = '$lokasi' and VehicleType = '$vhcl' and TimeOut between '$tgl 00:00:00.000' and '$tgl 23:59:59.999') as Keluar, 
                                                    (SELECT Count(TotalDeduct) from [dbo].[ParkingList] where LocationIn = '$lokasi' and VehicleType = '$vhcl') as Profit
                                            from [dbo].[ParkingList] as a 
                                            inner join [dbo].[LocationMerchant] as b
                                                on a.LocationIn = b.LocationID
                                            inner join [dbo].[VehicleType] as c
                                                on a.VehicleType = c.Code
                                            where LocationIn='$lokasi' and VehicleType = '$vhcl' and TimeIn between '$tgl 00:00:00.000' and '$tgl 23:59:59.999'";
                                    
                                    SELECT  Convert(date, a.TimeIn), 
                                            b.Nama, 
                                            c.Name, 
                                            (SELECT Count(TimeIn) from [dbo].[ParkingList] where LocationIn = a.LocationIn and Convert(date,TimeIn) = convert(date, a.TimeIn)) as Masuk, 
                                            (SELECT Count(TimeOut) from [dbo].[ParkingList] where LocationIn = a.LocationIn and Convert(date,TimeOut) = convert(date, a.TimeOut)) as Keluar, 
                                            (SELECT SUM(TotalDeduct) from [dbo].[ParkingList] where LocationIn = a.LocationIn and VehicleType = c.Code) as Profit 
                                    from [dbo].[ParkingList] as a 
                                    inner join [dbo].[LocationMerchant] as b 
                                        on a.LocationIn = b.LocationID 
                                    inner join [dbo].[VehicleType] as c 
                                        on a.VehicleType = c.Code 
                                    where b.LocationID='700097LOC0000002' and c.Code = 'C' and Convert(date, a.TimeIn) = '2019-01-09'
                                    group by 
                                        Convert(date, a.TimeIn), 
                                        b.Nama, 
                                        c.Name, 
                                        a.LocationIn
    
                                    $sql =  "SELECT  a.CardNo,
                                                    (Select max(TimeIn) from dbo.ParkingList where CardNo = a.CardNo) as LastIn,
                                                    (Select count(TimeIn) from dbo.ParkingList where CardNo = a.CardNo and MONTH(TimeIn) = 1),
                                                    (Select count(TimeOut) from dbo.ParkingList where CardNo = a.CardNo and MONTH(TimeIn) = 2),
                                                    b.isLink,
                                                    c.Name,
                                                    c.Phone,
                                                    c.KTP,
                                                    c.email
                                            from dbo.MemberLocation a
                                            inner join dbo.MemberCard b
                                                on a.CardNo = b.CardNo
                                            left join dbo.MemberList c
                                                on b.MemberID = c.MemberID
                                            where a.LocationID='$lokasi' and a.CardNo not in (select TipeQR from dbo.AktivasiCompliment) and VehicleType = '$vhcl' and TimeIn between '$tgl 00:00:00.000' and '$tgl 23:59:59.999'";
                                    
                                    echo $sql1.'<br><br>';
                                    $query1 = sqlsrv_query($conn, $sql1);
                                    while($data1 = sqlsrv_fetch_array($query1)) { ?>
                                        <tr>
                                            <td><?php echo $data1[1]->format('Y-m-d'); ?></td>
                                            <td><?php echo $data1[1]; ?></td>
                                            <td><?php echo $data1[2]; ?></td>
                                            <td><?php echo $data1[3]; ?></td>
                                            <td><?php echo $data1[4]; ?></td>
                                            <td><?php echo $data1[5]; ?></td>
                                        </tr>
                                        <?php
                                    }
                                    */
                                }
                                else if(@$_GET['type'] == 'history_ioqty') {
                                    $loc    = @$_GET['loc'];
                                    $bulan  = @$_GET['bulan'];
                                    $tahun  = @$_GET['tahun'];
                                    $card   = @$_GET['crd'];

                                    $sql = "SELECT TimeIn, LocationIn, TimeOut, LocationOut, Status from [dbo].[ParkingList] where CardNo='$card' and Year(TimeIn) = '$tahun' and MONTH(TimeIn) = '$bulan' Order By TimeIn ASC, TimeOut ASC";
                                    //echo $sql;
                                    $query = sqlsrv_query($conn, $sql);
                                    while($data = sqlsrv_fetch_array($query)) { 
                                        if($data[4] == '1') {
                                            $ismember = 'Member';
                                        }
                                        else {
                                            $ismember = 'Non-Member';
                                        }
                                        ?>
                                        <tr>
                                            <td><?php echo $data[0]->format('d-m-Y'); ?></td>
                                            <td><?php echo $data[1]; ?></td>
                                            <td><?php echo $data[2]->format('d-m-Y'); ?></td>
                                            <td><?php echo $data[3]; ?></td>
                                            <td><?php echo $ismember; ?></td>
                                        </tr>
                                        <?php
                                    }
                                }
                                else if(@$_GET['type'] == 'aktifasi') {
                                //else if(isset($fbtn_aktifasi)) {
                                    //$lokasi = @$_POST['loc_aktifasi'];
                                    //$vhcl   = @$_POST['vhcl_aktifasi'];
                                    $bulan  = date('Y').'-'.@$_GET['bulan'];
                                    $tahun  = @$_GET['tahun'];

                                    $tgl_awal = $tahun.'-'.date('m', strtotime($bulan)).'-1';
                                    $tgl_akhir = $bulan;
                                    //echo $tgl_awal.' : '.$tgl_akhir;

                                    $sql =  "SELECT b.Name, b.Phone, b.Email, b.Addr, a.CardNo, c.Name, a.DateRegister
                                            from [KOPX700097].[dbo].[MemberLocation] as a
                                            inner join [KOPX700097].[dbo].[MemberList] as b
                                            on a.MemberID = b.MemberID
                                            inner join [KOPX700097].[dbo].[VehicleType] as c
                                            on a.VehicleID = c.VehicleID
                                            where a.DateRegister between '$tgl_awal' and '$tgl_akhir'";

                                    //echo $sql;
                                    $query = sqlsrv_query($conn, $sql);
                                    while( $data = sqlsrv_fetch_array($query) ) { ?>
                                        <tr>
                                            <td><?php echo $data[0]; ?></td>
                                            <td><?php echo $data[1]; ?></td>
                                            <td><?php echo $data[2]; ?></td>
                                            <td><?php echo $data[3]; ?></td>
                                            <td><?php echo $data[4]; ?></td>
                                            <td><?php echo $data[5]; ?></td>
                                            <td><?php echo $data[6]->format('Y-m-d'); ?></td>
                                        </tr>
                                        <?php
                                    }
                                }
                                else if(@$_GET['type'] == 'member') {
                                //else if(isset($fbtn_member)) {
                                    $lokasi = @$_GET['loc'];
                                    $vhcl   = @$_GET['vhcl'];
                                    $bulan  = @$_GET['bulan'];
                                    $tahun  = @$_GET['tahun'];
                                    //echo 'OK';
                                    $sql =  "SELECT c.Name, c.Phone, c.Email, c.Addr, a.CardNo, b.Bulan, b.Year, d.Name, e.Nama, b.Status from [dbo].[MemberLocation]  as a
                                            inner join [dbo].[MemberLocationMonth] as b
                                            on a.CardNo = b.CardNo
                                            inner join [dbo].[MemberList] as c
                                            on a.MemberID = c.MemberID
                                            inner join [dbo].[VehicleType] as d
                                            on a.VehicleID = d.VehicleID
                                            inner join [dbo].[LocationMerchant] as e
                                            on a.LocationID = e.LocationID
                                            where b.Bulan='$bulan' and b.Year='$tahun' and e.LocationID = '$lokasi' and d.VehicleID = '$vhcl' and b.Status='1'
                                            order by b.Bulan ASC, b.Year ASC, c.Name ASC, a.CardNo ASC";

                                    $query = sqlsrv_query($conn, $sql);
                                    //echo $sql;
                                    while( $data = sqlsrv_fetch_array($query) ) { ?>
                                        <tr>
                                            <td><?php echo $data[0]; ?></td>
                                            <td><?php echo $data[1]; ?></td>
                                            <td><?php echo $data[2]; ?></td>
                                            <td><?php echo $data[3]; ?></td>
                                            <td><?php echo $data[4]; ?></td>
                                            <td><?php echo $data[5]; ?></td>
                                            <td><?php echo $data[6]; ?></td>
                                            <td><?php echo $data[7]; ?></td>
                                            <td><?php echo $data[8]; ?></td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                                
                            
                        </tbody>
                    </table>
                </div>

        </div>
    </div>

    <script type="text/javascript">
        $('#btn_inoutqty').click(function() {
            if( $('#inoutqty').hasClass('hide') ) {
                $('#inoutqty').removeClass('hide');
                $('#aktifasi').addClass('hide');
                $('#member').addClass('hide');
            }
            else {
                $('#inoutqty').addClass('hide');
            }
        });

        $('#btn_aktifasi').click(function() {
            if( $('#aktifasi').hasClass('hide') ) {
                $('#aktifasi').removeClass('hide');
                $('#inoutqty').addClass('hide');
                $('#member').addClass('hide');
            }
            else {
                $('#aktifasi').addClass('hide');
            }
        });

        $('#btn_member').click(function() {
            if( $('#member').hasClass('hide') ) {
                $('#member').removeClass('hide');
                $('#inoutqty').addClass('hide');
                $('#aktifasi').addClass('hide');
            }
            else {
                $('#member').addClass('hide');
            }
        });
    </script>

<?php require('footer.php'); ?>