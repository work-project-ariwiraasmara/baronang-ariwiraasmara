<?php
require('header.php');
require('sidebar-left.php');
require('sidebar-right.php');
require('content-header.php');
?>

    <div class="animated fadeinup delay-1">
        <div class="page-content txt-black">
            
            <?php
            $chart = @$_GET['chart'];
            if(@$_GET['chart']) {
                $json_label = array();
                $json_data1 = array();
                $json_data2 = array();
                $json_data3 = array();
                //$json_data4 = array();
                
                $bulan = @$_GET['bulan'];
                $tahun = @$_GET['tahun'];

                $bulan_int = date('t', strtotime($bulan));
                $tahun_int = date('Y', strtotime($tahun));
                $loc = @$_GET['loc'];
                $vehicle = @$_GET['veh'];
                
                $sql_lokasi = "Select Nama from [dbo].[LocationMerchant] where LocationID='$loc'";
				$queryLoc = sqlsrv_query($conn, $sql_lokasi);
				$lokasi = sqlsrv_fetch_array($queryLoc);
				
				$sql_vehicle = "Select * from [dbo].[VehicleType] where Code = '$vehicle'";
				$queryVehicle = sqlsrv_query($conn, $sql_vehicle);
				$dataVehicle = sqlsrv_fetch_array($queryVehicle);
				$titleTop = '<b>Jumlah '.$dataVehicle[1].' Parkir<br>'.$lokasi[0].'<br>'.date('F', strtotime($bulan)).' '.date('Y', strtotime($tahun)).'</b>';
				
                ?>

                
                <?php
                if(@$_GET['chart'] == 'hari') { ?>
                    <a href="chart_loc_detail.php?chart=bulan&bulan=<?php echo $bulan; ?>&tahun=<?php echo $tahun; ?>&loc=<?php echo $loc; ?>" target="_self" class="btn waves-effect waves-light primary-color"><i class="ion-android-arrow-back txt-white"></i> Back</a> 
                    <?php
                }
                ?>
				<h3 class="uppercase txt-black"><?php echo $titleTop; ?> </h3> <br>
                <div id="container" style="width: 100% ; height: 500px">
				
                    <?php
                    if(@$_GET['chart'] == 'bulan') { ?>
                    <a href="rep_totpark_loc.php" target="_self" class="btn waves-effect waves-light primary-color"><i class="ion-android-arrow-back txt-white"></i></a> Rekap Jumlah Kendaraan per Lokasi
                    <?php
				
                        $title = '<b>Jumlah '.$dataVehicle[1].' Parkir<br>'.$lokasi[0].'<br>'.date('F', strtotime($bulan)).' '.date('Y', strtotime($tahun)).'</b>';
						
						for($l = 1; $l <= $bulan_int; $l++) {
                            $ln1 = 0;
                            $ln2 = 0;
                            $ln3 = 0;
                           
                            $dtf = $tahun_int."-".date('m', strtotime($bulan))."-".$l;
                            $dtt = $tahun_int."-".date('m', strtotime($bulan))."-".$bulan_int;

                            if($l > 0 && $l < 10) { array_push($json_label, '<a href="chart_inout_loc.php?chart=hari&tgl='.$l.'&bulan='.$bulan.'&tahun='.$tahun.'&loc='.$loc.'" target="_self">0'.$l.'</a>'); }
                            else { array_push($json_label, '<a href="chart_inout_loc.php?chart=hari&tgl='.$l.'&bulan='.$bulan.'&tahun='.$tahun.'&loc='.$loc.'" target="_self">'.$l.'</a>'); }
                            

                            $sql_line1 = "SELECT COUNT(*) from [dbo].[ParkingList] where LocationIn='$loc' and TimeIn between '$dtf 00:00:00' and '$dtf 23:59:59'";
                            $query1 = sqlsrv_query($conn, $sql_line1);
                            while($data1 = sqlsrv_fetch_array($query1)) {
                                array_push($json_data1, $data1[0]);
                            }

                            $sql_line2 = "SELECT COUNT(*) from [dbo].[ParkingList] where LocationIn='$loc' and Status = '1' and TimeOut between '$dtf 00:00:00' and '$dtf 23:59:59'";
                            $query2 = sqlsrv_query($conn, $sql_line2);
                            while($data2 = sqlsrv_fetch_array($query2)) {
                                array_push($json_data2, $data2[0]);
                            }

                            $sql_line3 = "SELECT COUNT(*) from [dbo].[ParkingList] where LocationIn='$loc' and Status = '0' and TimeIn between '$dtf 00:00:00' and '$dtf 23:59:59'";
                            $query3 = sqlsrv_query($conn, $sql_line3);
                            while($data3 = sqlsrv_fetch_array($query3)) {
                                array_push($json_data3, $data3[0]);
                            }

                            
                        }
					
                        ?>

                        <div class="m-t-10" style="margin-bottom: 50px;">
                            <?php
                            //$dmn = date('t M'); 
                            //$yn = date('Y');
                            $month = date('m', strtotime($bulan));
                            //echo $dmn;
                            ?>

                            <?php 
                            if( !(($bulan == '31 Jan') && ($tahun == '2019')) ) { ?>
                                <a href="chart_inout_loc.php?chart=bulan&bulan=<?php echo date('t M', strtotime($tahun.'-'.$month.'-01'.'-1 month')); ?>&tahun=<?php echo $tahun; ?>&loc=<?php echo $loc; ?>" target="_self" class="btn btn-large left primary-color waves-effect waves-light" style="color: #ffffff;">Prev</a>
                            <?php 
                            }
                            ?>

                            <?php
                            if( !(($dmn == date('t M')) && ($yn == date('Y'))) ) { ?>
                                <a href="chart_inout_loc.php?chart=bulan&bulan=<?php echo date('t M', strtotime($tahun.'-'.$month.'-01'.'+1 month')); ?>&tahun=<?php echo $tahun; ?>&loc=<?php echo $loc; ?>" target="_self" class="btn btn-large right primary-color waves-effect waves-light txt-white" style="color: #ffffff;">Next</a>
                                <?php
                            }
                            ?>
                        </div>
                        <?php 
                    }
                    else if(@$_GET['chart'] == 'hari') { 
						
                        $title = '<b>Jumlah Parkir<br>'.$lokasi[0].'<br>'.@$_GET['tgl'].' '.date('F', strtotime($bulan)).' '.date('Y', strtotime($tahun)).'</b>';
                        
                        for($j = 0; $j < 24; $j++) {
                            $ln1 = 0;
                            $ln2 = 0;
                            $ln3 = 0;
                            
                            $dtf = $tahun_int."-".date('m', strtotime($bulan))."-".@$_GET['tgl'];
                            $jf = $j.':00:00.000';
                            $jt = $j.':59:59.999';

                            if($j == 0) $jam = 'nol';
                            else $jam = $j;

                            //echo $dtf.' '.$jf.' - '.$dtf.' '.$jt.'<br>';

                            array_push($json_label, '<a href="chart_inout_loc.php?chart=hari&tgl='.@$_GET['tgl'].'&bulan='.$bulan.'&tahun='.$tahun.'&jam='.$jam.'&loc='.$loc.'&veh='.$vehicle.'" target="_self">'.$j.':00 - '.$j.':59</a>');

                            $sql_line1 = "SELECT COUNT(*) from [dbo].[ParkingList] where LocationIn='$loc' and TimeIn between '$dtf $jf' and '$dtf $jt'";
                            $query1 = sqlsrv_query($conn, $sql_line1);
                            while($data1 = sqlsrv_fetch_array($query1)) {
                                array_push($json_data1, $data1[0]);
                            }

                            $sql_line2 = "SELECT COUNT(*) from [dbo].[ParkingList] where LocationIn='$loc' and Status = '1' and TimeOut between '$dtf $jf' and '$dtf $jt'";
                            $query2 = sqlsrv_query($conn, $sql_line2);
                            while($data2 = sqlsrv_fetch_array($query2)) {
                                array_push($json_data2, $data2[0]);
                            }

                            $sql_line3 = "SELECT COUNT(*) from [dbo].[ParkingList] where LocationIn='$loc' and Status = '0' and TimeIn between '$dtf $jf' and '$dtf $jt'";
                            $query3 = sqlsrv_query($conn, $sql_line3);
                            while($data3 = sqlsrv_fetch_array($query3)) {
                                array_push($json_data3, $data3[0]);
                            }

                           
                        } ?>
                        <div class="m-t-10" style="margin-bottom: 50px;">
                            <?php 
                            $tn = @$_GET['tgl'];
                            $jnp = @$_GET['jam'];

                            if($jnp == 'nol') $jnp = 0;
                            
                            if( @$_GET['jam'] ) { 

                                if($jnp == 'nol') { ?>
                                    <a href="chart_inout_loc.php?chart=hari&tgl=<?php echo ($tn-1); ?>&bulan=<?php echo date('t M', strtotime($bulan)); ?>&tahun=<?php echo $tahun; ?>&jam=23&loc=<?php echo $loc; ?> target="_self" class="btn btn-large left primary-color waves-effect waves-light txt-white" style="color: #ffffff;">Prev</a>
                            
                                    <a href="chart_inout_loc.php?chart=hari&tgl=<?php echo $tn; ?>&bulan=<?php echo date('t M', strtotime($bulan)); ?>&tahun=<?php echo $tahun; ?>&jam=<?php if( ($jnp+1) == $bulan_int ) { echo '#'; } else { echo ($jnp+1); } ?>&loc=<?php echo $loc; ?>" target="_self" class="btn btn-large right primary-color waves-effect waves-light txt-white" style="color: #ffffff;">Next</a>
                                <?php

                                }
                                else { 
                                    if($jnp == 23) { ?>
                                            <a href="chart_inout_loc.php?chart=hari&tgl=<?php echo $tn; ?>&bulan=<?php echo date('t M', strtotime($bulan)); ?>&tahun=<?php echo $tahun; ?>&jam=<?php if( ($jnp-1) == 0 ) { echo 'nol'; } else { echo ($jnp-1); } ?>&loc=<?php echo $loc; ?>" target="_self" class="btn btn-large left primary-color waves-effect waves-light txt-white" style="color: #ffffff;">Prev</a>
                                    
                                            <a href="chart_inout_loc.php?chart=hari&tgl=<?php echo ($tn+1); ?>&bulan=<?php echo date('t M', strtotime($bulan)); ?>&tahun=<?php echo $tahun; ?>&jam=nol&loc=<?php echo $loc; ?>" target="_self" class="btn btn-large right primary-color waves-effect waves-light txt-white" style="color: #ffffff;">Next</a>
                                        <?php
                                    }
                                    else { ?>
                                            <a href="chart_inout_loc.php?chart=hari&tgl=<?php echo $tn; ?>&bulan=<?php echo date('t M', strtotime($bulan)); ?>&tahun=<?php echo $tahun; ?>&jam=<?php if( ($jnp-1) == 0 ) { echo 'nol'; } else { echo ($jnp-1); } ?>&loc=<?php echo $loc; ?>" target="_self" class="btn btn-large left primary-color waves-effect waves-light txt-white" style="color: #ffffff;">Prev</a>
                                    
                                            <a href="chart_inout_loc.php?chart=hari&tgl=<?php echo $tn; ?>&bulan=<?php echo date('t M', strtotime($bulan)); ?>&tahun=<?php echo $tahun; ?>&jam=<?php if( ($jnp+1) == $bulan_int ) { echo '#'; } else { echo ($jnp+1); } ?>&loc=<?php echo $loc; ?>" target="_self" class="btn btn-large right primary-color waves-effect waves-light txt-white" style="color: #ffffff;">Next</a>
                                        <?php
                                    }
                                }
                                
                            }
                            else {
                                if( !($tn == '1') ) { ?>
                                    <a href="chart_inout_loc.php?chart=hari&tgl=<?php echo ($tn-1); ?>&bulan=<?php echo date('t M', strtotime($bulan)); ?>&tahun=<?php echo $tahun; ?>&loc=<?php echo $loc; ?>" target="_self" class="btn btn-large left primary-color waves-effect waves-light txt-white" style="color: #ffffff;">Prev</a>
                                    <?php
                                }
                                
                                if( !($tn == date('t', strtotime($bulan)) ) ) { ?>
                                    <a href="chart_inout_loc.php?chart=hari&tgl=<?php echo ($tn+1); ?>&bulan=<?php echo date('t M', strtotime($bulan)); ?>&tahun=<?php echo $tahun; ?>&loc=<?php echo $loc; ?>" target="_self" class="btn btn-large right primary-color waves-effect waves-light txt-white" style="color: #ffffff;">Next</a>
                                    <?php
                                }
                            }
                            
                             ?>
                        </div>
                        <?php 
                    } 
                    ?>
                    

                <?php
                    if(@$_GET['chart'] == 'hari') { 
                        if( @$_GET['jam'] ) {
                            $dtf = $tahun_int."-".date('m', strtotime($bulan))."-".@$_GET['tgl'];

                            if(@$_GET['jam'] == 'nol') $jam = 0;
                            else $jam = @$_GET['jam'];

                            $jf = $jam.':00';
                            $jt = $jam.':59';

                            $title = '<b>Jumlah Parkir<br>'.$lokasi[0].'<br>'.@$_GET['tgl'].' '.date('F', strtotime($bulan)).' '.date('Y', strtotime($tahun)).'</b>';
                            ?>
                            <div class="table-responsive m-t-30 p-10">
                                <h3 class="uppercase txt-black"><?php echo 'List Parkir'.$dataVehicle[1].' Perhari<br>Jam '.$jf.'-'.$jt; ?> </h3> <br>
								
                                <table class="table" style="width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>Time In</th>
                                            <th>Card No</th>
                                            <th>Time Out</th>
                                            <th>Member</th>
                                        </tr>
                                  </thead>
  
                                    <tbody>
										  
                                        <?php
                                        $sqlj = "SELECT *, CONVERT(varchar(255), TimeIn, 120), CONVERT(varchar(255), TimeOut, 120) from [dbo].[ParkingList] where LocationIn='$loc' and TimeIn between '$dtf $jf' and '$dtf $jt' Order By TimeIn ASC";
                                        $queryj = sqlsrv_query($conn, $sqlj);
                                        while( $dataj = sqlsrv_fetch_array($queryj) ) { 
                                            if($dataj[11] == '1') { $status = 'Yes';}
                                            else {$status = 'No';}
                                            if($dataj[4] == null) { $out = 'In Area';}
                                            else {$out = date('H:i:s', strtotime($dataj[13]));}
                                            //$tglj = date_create($dataj[4]);
                                            ?>
                                            <tr>
                                                <td><?php echo date('H:i:s', strtotime($dataj[12])); ?></td>
                                                <td><?php echo $dataj[2]; ?></td>
                                                <td><?php echo $out; ?></td>
                                                <td><?php echo $status; ?></td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody>
                                </table> 
                            </div>
                            <?php
                        }
                    }
                ?> 
                                
                </div>
                
                <?php
			
            }
            else { ?>
                <script type="text/javascript">
                    window.location.href = "home.php";
                </script>
                <?php
            }
            ?>
            </div>
        </div>
    </div>

    <script src="js/chart.min.js"></script>
    <script src="js/plotly-latest.min.js"></script>
    <script type="text/javascript">
        $('#btn_cj').click(function() {
            var jam = $('#chart_jam :selected').val();
            var url = "chart_inout_loc.php?chart=hari&tgl=<?php echo @$_GET['tgl']; ?>&bulan=<?php echo $bulan; ?>&tahun=<?php echo $tahun; ?>" + "&jam=" + jam + "&loc=<?php echo $loc; ?> target='_self'";

            window.location.href = url;
        });

        var trace1 = {
            x: <?php echo json_encode($json_label); ?>, 
            y: <?php echo json_encode($json_data1); ?>,
            type: 'bar',
            name: 'In',
            marker: {
                color: '#555555',
                opacity: 1
            }
        };

        var trace2 = {
            x: <?php echo json_encode($json_label); ?>,
            y: <?php echo json_encode($json_data2); ?>,
            type: 'bar',
            name: 'Out',
            marker: {
                color: '#0000ff',
                opacity: 1
            }
        };

        var trace3 = {
            x: <?php echo json_encode($json_label); ?>,
            y: <?php echo json_encode($json_data3); ?>,
            type: 'bar',
            name: 'In Area',
            marker: {
                color: '#ff0000',
                opacity: 1
            }
        };
        
        
        
        var data = [trace1, trace2, trace3];

        var layout = {
            autosize: false,
            width: 1400,
            height: 500,
            title: '<?php echo $title; ?>',
            xaxis: { type: 'category', fixedrange: true },
            yaxis: { fixedrange: true },
            barmode: 'group'
        };

        Plotly.newPlot('container', data, layout, {displaylogo: false});
    </script>
    
<?php
require('footer.php');
?>

