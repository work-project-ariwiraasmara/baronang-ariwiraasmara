<?php require('header.php');?>

<?php require('sidebar-right.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<div class="animated fadeinup delay-1">
    <div class="page-content">

        <?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
            <div class="notification notification-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
                <!--<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> <i class="icon fa fa-warning ?>"></i> -->
                <h4> <?php echo ucfirst($_SESSION['error-type']); ?> </h4>
                <?php echo $_SESSION['error-message']; ?>
            </div>
        <?php } ?>

        <?php
        $sql = "select * from dbo.TypeAcc order by KodeGroup asc";
        $stmt = sqlsrv_query($conn, $sql);
        $row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_NUMERIC);
        ?>
        <div class="box">
            <h2 class="uppercase"><?php echo lang('Chart of Accounts'); ?></h2><br>

            <div class="box-body">
                <div class="form">

                    <div class="row m-l-0 hide">
                        <div class="col">
                            <button type="button" class="waves-effect waves-light btn-large primary-color width-100 m-b-20 animated bouncein delay-4" id="btn_modal-example" onclick="arHide('#modal-example')"> <?php echo lang('Contoh Upload Data'); ?></button>
                        </div>
                        <div class="col">
                            <button type="button" class="waves-effect waves-light btn-large primary-color width-100 m-b-20 animated bouncein delay-4" id="btn_modal-confirm" onclick="arHide('#modal-confirm')"> <?php echo lang('Download Template'); ?></button>
                        </div>
                        <div class="col">
                            <button type="button" class="waves-effect waves-light btn-large primary-color width-100 m-b-20 animated bouncein delay-4" id="btn_modal-upload" onclick="arHide('#modal-upload')"> <?php echo lang('Upload Data'); ?></button>
                        </div>
                        <div class="col">
                            <button type="button" class="waves-effect waves-light btn-large primary-color width-100 m-b-20 animated bouncein delay-4" id="delete"> <?php echo lang('Hapus Semua Data'); ?></button>
                        </div>
                    </div>

                        <div class="hide" id="modal-example">
                            <h4 class="modal-title"><?php echo lang('Contoh Upload Data'); ?></h4>
                            <p><?php echo lang('Anda harus mengikuti contoh ini untuk sukses meng-upload data. <b> Jangan ubah judul header, row, dan kolum pada data excel tersebut! </b>'); ?></p>

                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <tr>
                                        <th colspan="5" class="text-center"><?php echo lang('Kode Akun'); ?></th>
                                        <th rowspan="2" class="text-center"><?php echo lang('Nama Akun'); ?></th>
                                        <th rowspan="2" class="text-center"><?php echo lang('Kategori Akun'); ?></th>
                                    </tr>
                                    <tr>
                                        <th>Header 1</th>
                                        <th>Header 2</th>
                                        <th>Header 3</th>
                                        <th>Header 4</th>
                                        <th>Header 5</th>
                                    </tr>
                                    <tr>
                                        <td>1.0.00.00.000</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td><?php echo lang('Aktiva'); ?></td>
                                        <td><?php echo lang('Aktiva'); ?></td>
                                    </tr>
                                    <tr>
                                        <td>1.0.00.00.000</td>
                                        <td>1.1.00.00.000</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td><?php echo lang('Aktiva Lancar'); ?></td>
                                        <td><?php echo lang('Aktiva Lancar'); ?></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>1.1.00.00.000</td>
                                        <td>1.1.01.00.000</td>
                                        <td></td>
                                        <td></td>
                                        <td><?php echo lang('Kas & Bank'); ?></td>
                                        <td><?php echo lang('Aktiva Lancar'); ?></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>1.1.01.00.000</td>
                                        <td>1.1.01.01.000</td>
                                        <td></td>
                                        <td><?php echo lang('Kas'); ?></td>
                                        <td><?php echo lang('Aktiva Lancar'); ?></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>1.1.01.01.000</td>
                                        <td>1.1.01.01.001</td>
                                        <td><?php echo lang('Kas'); ?></td>
                                        <td><?php echo lang('Aktiva Lancar'); ?></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>1.1.01.00.000</td>
                                        <td>1.1.01.02.000</td>
                                        <td></td>
                                        <td><?php echo lang('Bank'); ?></td>
                                        <td><?php echo lang('Aktiva Lancar'); ?></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>1.1.01.02.000</td>
                                        <td>1.1.01.02.001</td>
                                        <td>Bank Mandiri</td>
                                        <td><?php echo lang('Aktiva Lancar'); ?></td>
                                    </tr>
                                </table>
                            </div>
                        </div>

                    <div class="hide" id="modal-upload">
                        <div class="form-inputs">
                            <form action="ucoa.php" method="post" enctype="multipart/form-data">
                                <div class="">
                                    <div style="text-align: center;"><h3 class="uppercase"><?php echo lang('Upload Data'); ?></h3></div><br>

                                    <?php if($row == null){ ?>
                                        <h4><?php echo lang('Informasi'); ?></h4>

                                        <p><?php echo lang('Anda harus mengikuti contoh ini untuk sukses meng-upload data. <b> Jangan ubah judul header, row, dan kolum pada data excel tersebut! </b>'); ?></p>

                                        <input type="file" name="filename" class="form-control" accept=".xls"  required="" placeholder="File Excel">
                                    <?php } else { ?>
                                        <div class="callout callout-info">
                                            <h4><?php echo lang('Informasi!'); ?></h4>
                                            <p><?php echo lang('COA sudah di Upload'); ?></p>
                                        </div>
                                    <?php } ?>

                                </div>
                                <div class="">
                                    <?php if($row == null){ ?>
                                        <button type="submit" id="btn-save" class="btn waves-effect waves-light primary-color m-b-20 animated bouncein delay-4 right"><?php echo lang('Simpan'); ?></button>
                                        <button type="button" id="tree-loading" class="btn waves-effect waves-light primary-color m-b-20 animated bouncein delay-4 right"><i class="fa fa-rotate-right fa-spin"></i></button>
                                    <?php } ?>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="hide" id="modal-confirm">
                        <h3 class="uppercase"><?php echo lang('Konfirmasi'); ?></h3>
                        <?php echo lang('Download Template dengan contoh data?'); ?><br><br>
                        <div class="row m-l-0">
                            <div class="col">
                                <a href="dcoa.php?example"><button type="button" class="waves-effect waves-light btn-large primary-color width-100 m-b-20 animated bouncein delay-4"><?php echo lang('Ya'); ?></button></a>
                            </div>
                            <div class="col">
                                <a href="dcoa.php"><button type="button" class="waves-effect waves-light btn-large primary-color width-100 m-b-20 animated bouncein delay-4"><?php echo lang('Tidak'); ?></button></a>
                            </div>
                            <div class="col">
                                <button type="button" class="waves-effect waves-light btn-large primary-color width-100 m-b-20 animated bouncein delay-4" onclick="arHide('#modal-confirm')"><?php echo lang('Tutup'); ?></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form">
                    <ul class="faq collapsible popout animated fadeinright delay-2" data-collapsible="accordion">
                        <?php
                        $index = 1;
                        $x = "select* from dbo.GroupAcc order by substring (ltrim([KodeGroup]),1,1) asc";
                        $y = sqlsrv_query($conn, $x);
                        while($z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC)){
                            ?>
                            <li>
                                <div class="collapsible-header">
                                    <?php echo $z[0].' - '.$z[1]; ?><br>
                                </div>
                                <div class="collapsible-body" style="padding: 10px;">
                                    <ul class="faq collapsible popout animated fadeinright delay-2" data-collapsible="accordion">
                                        <li>
                                            <div class="collapsible-header">
                                                <i class="ion-android-add right"> </i>
                                            </div>
                                            <div class="collapsible-body" style="padding: 10px;">
                                                <div class="" id="#modal-default<?php echo str_replace('.','', $z[0]); ?>" style="margin-bottom: 40px;">
                                                    <div class="">
                                                        <h4 class="uppercase"><?php echo lang('Tambah Sub Akun'); ?> <?php echo $z[1]; ?></h4>

                                                        <input type="text" class="validate" id="name2<?php echo $index; ?>" placeholder="Account Name">
                                                        <input type="text" class="validate" id="ket2<?php echo $index; ?>" placeholder="Description">
                                                    </div>
                                                    <div class="">
                                                        <!--<button type="button" class="btn btn-default left"><?php echo lang('Tutup'); ?></button>-->
                                                        <button type="button" class="btn btn-default right" id="btn-add<?php echo $index; ?>" class="btn btn-primary"><?php echo lang('Simpan'); ?></button>
                                                    </div>
                                                </div>
                                            </div>

                                            <script type="text/javascript">
                                                $('#btn-add<?php echo $index; ?>').click(function(){
                                                    var name = $('#name2<?php echo $index; ?>').val();
                                                    var ket = $('#ket2<?php echo $index; ?>').val();
                                                    $.ajax({
                                                        url : "ajax_coa.php",
                                                        type : 'POST',
                                                        dataType: 'json',
                                                        data: { index: '<?php echo $index; ?>', id: '<?php echo $row[0]; ?>', name: name, ket: ket, type: 'type'},
                                                        success : function(responseJSON) {
                                                            if(responseJSON.status == 1){
                                                                $('#name2<?php echo $index; ?>').val('');
                                                                $('#ket2<?php echo $index; ?>').val('');
                                                                alert('Success save data.');
                                                                window.location.href='coa.php';
                                                            }
                                                            else{
                                                                alert('Failed save data.');
                                                                return false;
                                                            }
                                                        },
                                                        error : function(){
                                                            alert('Silahkan coba lagi.');
                                                        }
                                                    });
                                                });
                                            </script>
                                        </li>
                                    </ul>

                                    <ul class="faq collapsible popout animated fadeinright delay-2" data-collapsible="accordion">
                                        <?php
                                        $index2 = 0;
                                        $xx = "select* from dbo.TypeAcc where KodeGroup = '$z[0]' order by KodeTipe ASC";
                                        $yy = sqlsrv_query($conn, $xx);
                                        while($zz = sqlsrv_fetch_array($yy, SQLSRV_FETCH_NUMERIC)){
                                            ?>
                                            <li>
                                                <div class="collapsible-header">
                                                    <?php echo $zz[0].' - '.$zz[1]; ?>
                                                </div>
                                                <div class="collapsible-body" style="padding: 10px;">
                                                    <ul class="faq collapsible popout animated fadeinright delay-2" data-collapsible="accordion">
                                                        <li>
                                                            <div class="collapsible-header">
                                                                <i class="ion-android-add right" > </i>
                                                            </div>
                                                            <div class="collapsible-body" style="padding: 10px;">
                                                                <div class="" id="modal-detail<?php echo str_replace('.','', $zz[0]); ?>">
                                                                    <div class="">
                                                                        <h4 class="uppercase"><?php echo lang('Tambah Sub Akun'); ?> <?php echo $zz[0]; ?> - <?php echo $zz[1]; ?></h4>
                                                                        <!--
                                                                        <select id="type<?php echo $index.$index2; ?>" class="form-control">
                                                                            <option value=""><?php echo lang('Pangkat'); ?></option>
                                                                            <?php for($q=0;$q<=2;$q++){ ?>
                                                                                <option value="<?php echo $q; ?>"><?php echo $q+3; ?></option>
                                                                            <?php } ?>
                                                                        </select>
                                                                        -->
                                                                        <input type="hidden" class="validate" id="id<?php echo $index.$index2; ?>" value="<?php echo $zz[0]; ?>" readonly>
                                                                        <input type="hidden" class="validate" id="type<?php echo $index.$index2; ?>" value="0" readonly>
                                                                        <input type="text" class="validate" id="name<?php echo $index.$index2; ?>" placeholder="Account Name">
                                                                        <input type="text" class="validate" id="ket<?php echo $index.$index2; ?>" placeholder="Description">
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <!--<button type="button" class="btn btn-default pull-left" data-dismiss="modal"><?php echo lang('Tutup'); ?></button>-->
                                                                        <button type="button" id="btn-det<?php echo $index.$index2; ?>" class="btn btn-primary"><?php echo lang('Simpan'); ?></button>
                                                                    </div>
                                                                    <!-- /.modal-dialog -->
                                                                </div>
                                                            </div>

                                                            <script type="text/javascript">
                                                                $('#btn-det<?php echo $index.$index2; ?>').click(function(){
                                                                    //var type = $('#type<?php echo $index.$index2; ?> :selected').val();
                                                                    //var type = 0;
                                                                    var id   = $('#id<?php echo $index.$index2; ?>').val();
                                                                    var type = $('#type<?php echo $index.$index2; ?>').val();
                                                                    var name = $('#name<?php echo $index.$index2; ?>').val();
                                                                    var ket  = $('#ket<?php echo $index.$index2; ?>').val();
                                                                    alert('index: ' + type + ';\n id: ' + id +';\n name: ' + name + ';\n ket: ' + ket);
                                                                    $.ajax({
                                                                        url : "ajax_coa2.php",
                                                                        type : 'POST',
                                                                        dataType: 'json',
                                                                        data: {
                                                                            index: type,
                                                                            id: id,
                                                                            name: name,
                                                                            ket: ket,
                                                                            type: 'detail'
                                                                        },
                                                                        success : function(responseJSON) {
                                                                            //alert('index: ' + type + ';\n id: ' + id +';\n name: ' + name + ';\n ket: ' + ket);
                                                                            if(responseJSON.status == 1){
                                                                                $('#type<?php echo $index2; ?>').val('');
                                                                                $('#name<?php echo $index2; ?>').val('');
                                                                                $('#ket<?php echo $index2; ?>').val('');
                                                                                alert('Success save data.');
                                                                                window.location.href='coa.php';
                                                                            }
                                                                            else{
                                                                                alert('Failed save data.');
                                                                                return false;
                                                                            }
                                                                        },
                                                                        error : function(){
                                                                            alert('Silahkan coba lagi.');
                                                                        }
                                                                    });
                                                                });
                                                            </script>
                                                        </li>
                                                    </ul>

                                                    <ul class="faq collapsible popout animated fadeinright delay-2" data-collapsible="accordion">
                                                        <?php
                                                        $xxx = "select* from dbo.AccountView where KodeTipe = '$zz[0]' and Header = 0 ORDER BY KodeTipe ASC";
                                                        $yyy = sqlsrv_query($conn, $xxx);
                                                        while($zzz = sqlsrv_fetch_array($yyy, SQLSRV_FETCH_NUMERIC)){
                                                            ?>
                                                            <li>
                                                                <div class="collapsible-header">
                                                                    <?php echo $zzz[0].' - '.$zzz[1]; ?>
                                                                </div>
                                                                <div class="collapsible-body" style="padding: 10px;">
                                                                    <ul class="faq collapsible popout animated fadeinright delay-2" data-collapsible="accordion">
                                                                        <li>
                                                                            <div class="collapsible-header">
                                                                                <i class="ion-android-add right" > </i>
                                                                            </div>
                                                                            <div class="collapsible-body" style="padding: 10px;">
                                                                                <div class="form-inputs" style="margin-bottom: 30px;">
                                                                                    <h4 class="uppercase">Input Account Di Bawah <?php echo $zzz[1]; ?></h4>
                                                                                    <form name ="formx" action ="prosescoa.php" method="post" id="form_acclvl1_<?php echo str_replace(".","", $zzz[0]); ?>">
                                                                                        <div class="">
                                                                                            <div class="input-field">
                                                                                                <input type="text" class="validate" name="sacc" id="sacc">
                                                                                                <label for="sacc" name="lblnm" class="badge">Nama Account</label>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="">
                                                                                            <span class="right">
                                                                                                <input type="submit" class="btn btn-default btn-sm" name = "btnsave" value="<?php echo lang('Simpan'); ?>">
                                                                                            </span>
                                                                                            <!--
                                                                                            <span class="pull-right">
                                                                                                <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><?php echo lang('Tutup'); ?></button>
                                                                                            </span>-->

                                                                                            <input type ="hidden" name ="tipex" value ="<?php echo $zzz[2]; ?>">
                                                                                            <input type ="hidden" name ="accx" value ="<?php echo $zzz[0]; ?>">
                                                                                            <input type ="hidden" name ="headerx" value ="<?php echo $zzz[4]; ?>">
                                                                                        </div>
                                                                                    </form>
                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                    </ul>

                                                                    <ul class="faq collapsible popout animated fadeinright delay-2" data-collapsible="accordion">
                                                                        <?php
                                                                        $xxxx = "select* from dbo.AccountView where KodeTipe = '$zz[0]' and Header = 1 and KodeAccount like '%".substr($zzz[0],0,6)."%' ORDER BY KodeAccount Asc";
                                                                        $yyyy = sqlsrv_query($conn, $xxxx);
                                                                        while($zzzz = sqlsrv_fetch_array($yyyy, SQLSRV_FETCH_NUMERIC)){
                                                                            ?>
                                                                            <li data-icon-cls="fa fa-folder">
                                                                                <div class="collapsible-header">
                                                                                    <?php echo $zzzz[0].' - '.$zzzz[1]; ?>
                                                                                </div>
                                                                                <div class="collapsible-body" style="padding: 10px;">
                                                                                    <ul class="faq collapsible popout animated fadeinright delay-2" data-collapsible="accordion">
                                                                                        <li>
                                                                                            <div class="collapsible-header">
                                                                                                <i class="ion-android-add right" > </i>
                                                                                            </div>
                                                                                            <div class="collapsible-body" style="padding: 10px;">
                                                                                                <div class="form-inputs" style="margin-bottom: 30px;">
                                                                                                    <h4 class="uppercase">Input Account Di Bawah <?php echo $zzzz[1]; ?></h4>
                                                                                                    <form name ="formx" action ="prosescoa.php" method="post" id="form_acclvl2_<?php echo str_replace(".","", $zzzz[0]); ?>">
                                                                                                        <div class ="input-field">
                                                                                                            <input type="text" class="validate" style="border-radius: 10px" name="sacc" id="sacc">
                                                                                                            <label for="sacc" name="lblnm" class="badge"Nama Account></label>
                                                                                                        </div>
                                                                                                        <div class="">
                                                                                                            <span class="right">
                                                                                                                <input type="submit" class="btn btn-danger" name = "btnsave" value="<?php echo lang('Simpan'); ?>">
                                                                                                            </span>
                                                                                                            <!--<span class="pull-right">
                                                                                                                <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><?php echo lang('Tutup'); ?></button>
                                                                                                            </span>-->

                                                                                                            <input type ="hidden" name ="tipex" value ="<?php echo $zzzz[2]; ?>">
                                                                                                            <input type ="hidden" name ="accx" value ="<?php echo $zzzz[0]; ?>">
                                                                                                            <input type ="hidden" name ="headerx" value ="<?php echo $zzzz[4]; ?>">
                                                                                                        </div>
                                                                                                    </form>
                                                                                                </div>
                                                                                            </div>
                                                                                        </li>
                                                                                    </ul>

                                                                                    <ul class="faq collapsible popout animated fadeinright delay-2" data-collapsible="accordion">
                                                                                        <?php
                                                                                        $xxxxx = "select* from dbo.AccountView where KodeTipe = '$zz[0]' and Header = 2 and KodeAccount like '%".substr($zzzz[0],0,9)."%' ORDER BY KodeAccount Asc";
                                                                                        $yyyyy = sqlsrv_query($conn, $xxxxx);
                                                                                        while($zzzzz = sqlsrv_fetch_array($yyyyy, SQLSRV_FETCH_NUMERIC)){
                                                                                            ?>
                                                                                            <li>
                                                                                                <div class="collapsible-header"><?php echo $zzzzz[0].' - '.$zzzzz[1]; ?></div>
                                                                                                <div class="collapsible-body" style="padding: 10px;"></div>
                                                                                            </li>
                                                                                        <?php } ?>
                                                                                    </ul>
                                                                                </div>
                                                                            </li>
                                                                        <?php } ?>
                                                                    </ul>
                                                                </div>
                                                            </li>
                                                        <?php } ?>
                                                    </ul>
                                                </div>
                                            </li>
                                        <?php $index2++; } ?>
                                    </ul>

                                </div>
                            </li>
                        <?php $index++; } ?>
                    </ul>
                </div>

            </div>
        </div>

    </div>
</div>

    <!-- you need to include the ShieldUI CSS and JS assets in order for the TreeView widget to work -->
    <link rel="stylesheet" type="text/css" href="static/plugins/tree/tree.css" />
    <script type="text/javascript" src="static/plugins/tree/tree.js"></script>

    <script type="text/javascript">
        jQuery(function ($) {
            $("#treeview").shieldTreeView();
        });

        $('#delete').click(function(){
            if(confirm('Are you sure delete all data COA ? data cannot be restore after delete.') == true){
                window.location.href='ucoa.php?delete';
            }
            return false;
        });

        $('#btn-save').click(function(){
            //$('#btn-save').prop('disabled',true);
            $('#tree-loading').removeClass('hide');
        });

        function arHide(setID) {
            if( $(setID).hasClass('hide') ) $(setID).removeClass('hide');
            else $(setID).addClass('hide');
        }

    </script>

<?php require('footer_new.php');?>
