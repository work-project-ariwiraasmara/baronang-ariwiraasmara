<?php
session_start();
error_reporting(0);

include('lib/phpExcel/Classes/PHPExcel.php');
include "connect.php";


   $filePath = "uploads/excel/";

    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getProperties()->setCreator("baronang");
    $objPHPExcel->getProperties()->setTitle("Rekap Saldo Anggota");

// set autowidth
    for($col = 'A'; $col !== 'Z'; $col++) {
        $objPHPExcel->getActiveSheet()
            ->getColumnDimension($col)
            ->setAutoSize(true);
    }

    $listCombo = array();

    //sheet 2
    $objWorkSheet = $objPHPExcel->createSheet(0);
    
    // baris judul
    $objWorkSheet->getStyle('E1')->getFont()->setBold(true);
    $objWorkSheet->getStyle('E1')->getFont()->setSize(14);
    $objWorkSheet->SetCellValue('E1', 'Rekap Saldo Anggota');



    //$objWorkSheet->getActiveSheet()->margeCell('A5:B5');
    $objWorkSheet->getStyle('A5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('A5' )->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('A5', 'No. Anggota');
    $objWorkSheet->getStyle('C5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('C5' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('C5', 'Nama');
    $objWorkSheet->getStyle('D5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('D5' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('D5', 'Simpanan');
    $objWorkSheet->getStyle('G5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('G5' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('G5', 'Pinjaman');
    $objWorkSheet->getStyle('I5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('I5' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('I5', 'Tabungan');
    $objWorkSheet->getStyle('J5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('J5' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('J5', 'Deposito');
    $objWorkSheet->getStyle('A6')->getFont()->setBold(true);
    $objWorkSheet->getStyle('A6' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('A6', 'New');  
    $objWorkSheet->getStyle('B6')->getFont()->setBold(true);
    $objWorkSheet->getStyle('B6' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('B6', 'Old');
    $objWorkSheet->getStyle('D6')->getFont()->setBold(true);
    $objWorkSheet->getStyle('D6' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('D6', 'Simpanan Pokok');
    $objWorkSheet->getStyle('E6')->getFont()->setBold(true);
    $objWorkSheet->getStyle('E6' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('E6', 'Simpanan Wajib');
    $objWorkSheet->getStyle('F6')->getFont()->setBold(true);
    $objWorkSheet->getStyle('F6' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('F6', 'Simpanan Sukarela');
    $objWorkSheet->getStyle('G6')->getFont()->setBold(true);
    $objWorkSheet->getStyle('G6' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('G6', 'Pokok');
    $objWorkSheet->getStyle('H6')->getFont()->setBold(true);
    $objWorkSheet->getStyle('H6' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('H6', 'Bunga');


    

    $row = 7;
    $rw = 7;


    if($_GET['cari']){
    
    $xy = "SELECT * from (select *, ROW_NUMBER () OVER (order by memberid asc) as row from dbo.memberlist  a where statusmember = '1' and Name like '%".$_GET['cari']."%' or MemberID like '%".$_GET['cari']."%' or oldmemberID like '%".$_GET['cari']."%') a";
    //echo $xy;
    $yz = sqlsrv_query($conn, $xy);
    
    while($za = sqlsrv_fetch_array( $yz, SQLSRV_FETCH_NUMERIC)){
            //Simpanan Pokok 
            $sql1   = "SELECT * FROM ( SELECT a.KID,a.MemberID,a.KodeBasicSavingType,a.AccNo,a.BillingBalance,a.PaymentBalance,c.Status, ROW_NUMBER() OVER (ORDER BY a.KID asc) as row FROM [dbo].[BasicSavingBalance] a inner join[dbo].[BasicSavingType] c on a.KodeBasicSavingType = c.KodeBasicSavingType where a.MemberID = '$za[1]' and c.Status = '1') a";
            $stmt1  = sqlsrv_query($conn, $sql1);
            $row1  = sqlsrv_fetch_array($stmt1, SQLSRV_FETCH_NUMERIC);  

            //Simpanan Wajib
            $sql2   = "SELECT * FROM ( SELECT a.KID,a.MemberID,a.KodeBasicSavingType,a.AccNo,a.BillingBalance,a.PaymentBalance,c.Status, ROW_NUMBER() OVER (ORDER BY a.KID asc) as row FROM [dbo].[BasicSavingBalance] a inner join[dbo].[BasicSavingType] c on a.KodeBasicSavingType = c.KodeBasicSavingType where a.MemberID = '$za[1]' and c.Status = '0') a";
            $stmt2  = sqlsrv_query($conn, $sql2);
            $row2  = sqlsrv_fetch_array($stmt2, SQLSRV_FETCH_NUMERIC);
                                    
            //Simpanan Sukarela
            $sql   = "SELECT * FROM ( SELECT a.KID,a.MemberID,a.KodeBasicSavingType,a.AccNo,a.BillingBalance,a.PaymentBalance,c.Status, ROW_NUMBER() OVER (ORDER BY a.KID asc) as row FROM [dbo].[BasicSavingBalance] a inner join[dbo].[BasicSavingType] c on a.KodeBasicSavingType = c.KodeBasicSavingType where a.MemberID = '$za[1]' and c.Status = '2') a";
            //echo $sql;                            
            $stmt  = sqlsrv_query($conn, $sql);
            $rsw   = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_NUMERIC); 


            //Penjumlahan Tabungan
            $tabungan = "SELECT sum(balance) FROM [dbo].[RegularSavingAcc] WHERE KID='$_SESSION[KID]' and MemberID='$za[1]' and status != '0'";
            //echo $tabungan;
            $ptabungan = sqlsrv_query($conn, $tabungan);
            $htabungan = sqlsrv_fetch_array($ptabungan, SQLSRV_FETCH_NUMERIC);


            //Penjumlahan Deposito
            $Deposito = "SELECT sum(amount) FROM [dbo].[TimeDepositAccount] where MemberID='$za[1]'";
            //echo $Deposito;
            $pdeposito = sqlsrv_query($conn, $Deposito);
            $hdeposito = sqlsrv_fetch_array($pdeposito, SQLSRV_FETCH_NUMERIC);


            //Penjumlahan Pinjaman 
            $pinj = "SELECT sum (loanammount) row FROM ( SELECT a.KID, a.memberID, a.name, a.Addr, b.userID, c.loanammount, ROW_NUMBER() OVER (ORDER BY a.memberid asc) as row from dbo.memberlist a inner join paymentgateway.dbo.usermemberkoperasi b on a.memberid = b.kodemember join dbo.loanlist c on b.UserID = c.UserID where a.KID = '$_SESSION[KID]' and memberID = '$za[1]') a";
            $ppinj = sqlsrv_query($conn, $pinj);
            $hpinj = sqlsrv_fetch_array($ppinj, SQLSRV_FETCH_NUMERIC);


            $bpinjaman = "SELECT * from (select a.loanappnumber, a.timestamp, a.loanammount, a.invcount, a.interestrate, a.principal_penalty, a.interest_penalty, b.memberid, b.KodeLoantype, c.kodeinteresttype, (select top 1(invcount) from dbo.loantransaction where LoanNumber= a.Loanappnumber order by invcount desc) as jumlahbayar,(select top 1(PrincipalDue) from dbo.loantransaction where LoanNumber= a.Loanappnumber order by invcount desc) as bayarterakhir,(select top 1(interestdue) from dbo.loantransaction where LoanNumber= a.Loanappnumber order by invcount desc) as bungaterakhir, ROW_NUMBER() over (order by loanappnumber asc) as row from dbo.loanlist a inner join dbo.LoanApplicationList b on a.loanappnumber = b.loanappnum join dbo.loantype c on b.kodeloantype = c.kodeloantype where memberid = '$za[1]' ) a";
                                    $pbpinjaman = sqlsrv_query($conn, $bpinjaman);
                                    $coba1 = 0;
                                    $coba2 = 0;
                                    $coba3 = 0;
                                    while ($hbpinjaman = sqlsrv_fetch_array($pbpinjaman, SQLSRV_FETCH_NUMERIC)) {
                                        
                                        if ($hbpinjaman[9] == '01'){
                                            $hasil1 = 0;
                                            if ($hbpinjaman[10] == null) {
                                                $hasil1 = $hbpinjaman[2]*($hbpinjaman[4]/100)/$hbpinjaman[3];
                                            } else {
                                                $hasil1 = (($hbpinjaman[2]*($hbpinjaman[4]/100)/$hbpinjaman[3])+(((($hbpinjaman[12]*$hbpinjaman[10])-($hbpinjaman[12]*$hbpinjaman[10]))*$hbpinjaman[6])+(($hbpinjaman[11]*$hbpinjaman[10])-($hbpinjaman[11]*$hbpinjaman[10]))*$hbpinjaman[5])*($hbpinjaman[4]/100)/$hbpinjaman[3])+($hbpinjaman[12]-$hbpinjaman[12]);
                                            }

                                            $jmhasil1 = ($hasil1*$hbpinjaman[3])-($hbpinjaman[11]*$hbpinjaman[10]);
                                            $coba1 += $jmhasil1;


                                        } else if ($hbpinjaman[9] == '02'){
                                            $hasil2 = 0;
                                            if ($hbpinjaman[10] == null) {
                                                $hasil2 =  $hbpinjaman[2]*($hbpinjaman[4]/100)/$hbpinjaman[3];
                                            } else {
                                                $hasil2 = $hbpinjaman[11]*($hbpinjaman[4]/100)/$hbpinjaman[3];
                                            }

                                            $jmhasil2 = ($hasil2*$hbpinjaman[3])-($hbpinjaman[11]*$hbpinjaman[10]);
                                            //echo $hasil2;
                                            //echo $hbpinjaman[11];
                                            //echo $hbpinjaman[11];                                            
                                            $coba2 += $jmhasil2;
                                            //echo $jmhasil2;
                                        } else if ($hbpinjaman[9] == '03') {
                                            $hasil3 = 0;
                                            if ($hbpinjaman[10] == null) {
                                                $hasil3 =  $hbpinjaman[2]*($hbpinjaman[4]/100)/$hbpinjaman[3];
                                            } else {
                                                $hasil3 = $hbpinjaman[11]*($hbpinjaman[4]/100)/$hbpinjaman[3]+($hbpinjaman[12]-$hbpinjaman[12]);
                                            }

                                            $jmhasil3 = ($hasil3*$hbpinjaman[3])-($hbpinjaman[11]*$hbpinjaman[10]);
                                            $coba3 += $jmhasil3;
                                        }
                                        
                                    }

                                    $hasilsemua = $coba1+$coba2+$coba3;
        
               
            $objWorkSheet->SetCellValueExplicit("A".$row,$za[1]);
            $objWorkSheet->SetCellValueExplicit("B".$row,$za[10]);
            $objWorkSheet->SetCellValueExplicit("C".$row,$za[2]);
            $objWorkSheet->getStyle('D' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            $objWorkSheet->SetCellValue("D".$row, number_format($row1[5],2), PHPExcel_Cell_DataType::TYPE_STRING);
            $objWorkSheet->getStyle('E' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            $objWorkSheet->SetCellValue("E".$row, number_format($row2[5],2), PHPExcel_Cell_DataType::TYPE_STRING);
            $objWorkSheet->getStyle('F' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            $objWorkSheet->SetCellValue("F".$row, number_format($rsw[5],2), PHPExcel_Cell_DataType::TYPE_STRING);
            $objWorkSheet->getStyle('G' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            $objWorkSheet->SetCellValue("G".$row, number_format($hpinj[0],2), PHPExcel_Cell_DataType::TYPE_STRING);
            $objWorkSheet->getStyle('H' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            $objWorkSheet->SetCellValue("H".$row, number_format($hasilsemua,2), PHPExcel_Cell_DataType::TYPE_STRING);
            $objWorkSheet->getStyle('I' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            $objWorkSheet->SetCellValue("I".$row, number_format($htabungan[0],2), PHPExcel_Cell_DataType::TYPE_STRING);
            $objWorkSheet->getStyle('J' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            $objWorkSheet->SetCellValue("J".$row, number_format($hdeposito[0],2), PHPExcel_Cell_DataType::TYPE_STRING);
        
        
        

        
        $row++;
        $rw++;
        }

}

//exit;
    $objWorkSheet->setTitle('Rekap Saldo Anggota');

    $fileName = 'RekapSaldoAngg'.'_'.strtotime(date('Y-m-d H:i:s')).'.xls';
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');

// download ke client
    header('Content-type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename="'.$fileName.'"');
    $objWriter->save('php://output');

    return $filePath.'/'.$fileName;

?>
