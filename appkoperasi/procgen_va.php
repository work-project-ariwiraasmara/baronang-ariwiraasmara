<?php
session_start();
error_reporting(0);

include "connect.php";

$gen	= $_POST['generate'];
$bank	= $_POST['bank'];
	if($gen == "" || $bank == ""){
        messageAlert(lang('Harap isi seluruh kolom'),'info');
        header('Location: gen_va.php');
	}
	else{
      if($bank == 'B0080'){
        if($gen > 0 and $gen <= 1000){
          $tanggal = date('Y-m-d H:i:s');
          $tanggalexp = date('Y-m-d H:i:s', strtotime($tanggal . ' +1 day'));

					$aa = "select top 1 * from dbo.VaMemberHeader order by DownloadCount desc";
					$bb = sqlsrv_query($conn, $aa);
					$cc = sqlsrv_fetch_array($bb, SQLSRV_FETCH_NUMERIC);
					if($cc == null){
							$count = '001';
					}
					else{
							$add = $cc[7]+1;
							if(strlen($cc[7]) < 10){
									$count = '00'.$add;
							}
							else if(strlen($cc[7]) >= 10 and strlen($cc[7]) < 100){
								$count = '0'.$add;
							}
							else{
								$count = $add;
							}
					}

					$filename = 'ADD-8308-'.date('Ymd').'-'.$count.'.txt';
					$locationname = 'va/'.$filename;

					$myfile = fopen("$locationname", "w") or die("Unable to open file!");
					$txt = "NAME|MEMBER_ID|COMPANY_UNIT|ADDRESS|DESCRIPTION|CURRENCY|\n";
          fwrite($myfile, $txt);

          $a = "select top $gen CardNo, RIGHT(CardNo, 10) as Va, MemberID from dbo.MemberCard where CardNo not in(select CardNo from dbo.VaMember where Status = 1) and CardNo not like '%100010001000%' and isLink = 1 and Status = 1 order by CardNo asc";
          $b = sqlsrv_query($conn, $a);
          while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
              $cardno = $c[0];
              $vn = $c[1];

              $va = '8308'.$vn;
              $upultsqll = "insert into [dbo].[VaMember](BankCode, VaNumber, Status, CreatedDate, ActiveDate, CardNo)values('$bank','$va',1,'$tanggal','$tanggalexp','$cardno')";
              $upulstmtt = sqlsrv_query($conn, $upultsqll);
							if($upulstmtt){
								$aaa = "select top 1 * from dbo.MemberList where MemberID = '$c[2]'";
								$bbb = sqlsrv_query($conn, $aaa);
								$ccc = sqlsrv_fetch_array($bbb, SQLSRV_FETCH_NUMERIC);
								if($ccc != null){
										$txt = "SKY CARD $cardno|$vn|PT Sky Parking Utama|KOP|NEW CARD|IDR|\n";
										fwrite($myfile, $txt);
								}
							}
          }

          fclose($myfile);

					$tsqll = "insert into [dbo].[VaMemberHeader](GenerateDate, Filename, Qty, FileLocation, DownloadCount)values('$tanggal','$filename','$gen','$locationname','$count')";
					$stmtt = sqlsrv_query($conn, $tsqll);
					if($stmtt){
							messageAlert('Berhasil menggenerate va','success');
		          header('Location: gen_va.php');
					}
					else{
							messageAlert('Coba lagi','info');
							header('Location: gen_va.php');
					}
        }
        else{
          messageAlert(lang('Maksimal generator 1 sampai 100'),'info');
          header('Location: gen_va.php');
        }
      }
      else{
        messageAlert(lang('Saat ini hanya menerima Bank Sinarmas saja'),'info');
        header('Location: gen_va.php');
      }
	}
?>
