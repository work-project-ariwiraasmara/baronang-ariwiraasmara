<?php require('header.php');?>

<?php require('sidebar-right.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<style type="text/css">
    body{
    font-family: arial, verdana, sans-serif;
}
/* jwpopup box style */
.jwpopup {
    display: none;
    position: fixed;
    z-index: 1;
    padding-top: 110px;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    overflow: auto;
    background-color: rgb(0,0,0);
    background-color: rgba(0,0,0,0.7);
}
.jwpopup-content {
    position: relative;
    background-color: #fefefe;
    margin: auto;
    padding: 0;
    max-width: 500px;
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
    -webkit-animation-name: animatetop;
    -webkit-animation-duration: 0.4s;
    animation-name: animatetop;
    animation-duration: 0.4s
}

.jwpopup-head {
    font-size: 11px;
    padding: 1px 16px;
    color: black;
    /*background: #006faa; /* For browsers that do not support gradients */
    /*background: -webkit-linear-gradient(#006faa, #002c44);*/ /* For Safari 5.1 to 6.0 */
    /*background: -o-linear-gradient(#006faa, #002c44); *//* For Opera 11.1 to 12.0 */
    /*background: -moz-linear-gradient(#006faa, #002c44);*/ /* For Firefox 3.6 to 15 */
    /*background: linear-gradient(#006faa, #002c44);*/ /* Standard syntax */*/
}
.jwpopup-main {padding: 5px 16px;}
.jwpopup-foot {
    font-size: 12px;
    padding: .5px 16px;
    color: #ffffff;
    background: #006faa; /* For browsers that do not support gradients */
    background: -webkit-linear-gradient(#006faa, #002c44); /* For Safari 5.1 to 6.0 */
    background: -o-linear-gradient(#006faa, #002c44); /* For Opera 11.1 to 12.0 */
    background: -moz-linear-gradient(#006faa, #002c44); /* For Firefox 3.6 to 15 */
    background: linear-gradient(#006faa, #002c44); /* Standard syntax */
}

/* tambahkan efek animasi */
@-webkit-keyframes animatetop {
    from {top:-300px; opacity:0}
    to {top:0; opacity:1}
}

@keyframes animatetop {
    from {top:-300px; opacity:0}
    to {top:0; opacity:1}
}

/* style untuk tombol close */
.close {
    margin-top: 7px;
    color: white;
    float: right;
    font-size: 28px;
    font-weight: bold;
}
.close:hover, .close:focus {
    color: #999999;
    text-decoration: none;
    cursor: pointer;
}
</style>





<div class="animated fadeinup delay-1">
    <div class="page-content">
        <h2 class="uppercase"><?php echo lang('Daftar Anggota'); ?></h2> <br>
           

            <div class="box-body">
                <a href="drep_bas.php?acc=<?php echo $_GET['acc']; ?>"><button type="button" class="waves-effect waves-light btn-large primary-color width-100" style="margin-top: 20px;">Download To Excel</button></a>

                <a href="rep_list.php?page=<?php echo $_GET['page'];?>"><button type="button" class="waves-effect waves-light btn-large primary-color width-100" style="margin-top: 20px;">Back</button></a>
                <div class="row">
                    <?php
                        if(isset($_GET['acc'])){
                            $cs = $_GET['acc'];

                            $cariuser = "select * from [PaymentGateway].[dbo].[UserMemberKoperasi] where KodeMember = '$cs' and KID = '$_SESSION[KID]'";
                            $pcariuser = sqlsrv_query($conn, $cariuser);
                            $hcariuser = sqlsrv_fetch_array($pcariuser, SQLSRV_FETCH_NUMERIC);




                            $sel = "SELECT * FROM [dbo].[MemberListView] WHERE KID='$_SESSION[KID]' and MemberID='$cs'";
                            //echo $sel;
                            $cek = sqlsrv_query($conn, $sel);
                            $disable = 'readonly';
                            while($hasil = sqlsrv_fetch_array( $cek, SQLSRV_FETCH_NUMERIC)){
                                $tes = $hasil[8];
                                $jab = "select * from dbo.JabatanDetail where KodeJabatan = '$tes'";
                                //echo $jab;
                                $jab1 = sqlsrv_query($conn, $jab);
                                $jab2 = sqlsrv_fetch_array( $jab1, SQLSRV_FETCH_NUMERIC);
                            ?>


                            <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-lg-12 table-responsive"> 
                                <table border=”0″ cellpadding=”5″ cellspacing=”0″>
                                    <tr>
                                        <td>MemberID</td>
                                        <td>:</td>
                                        <td class="col-sm-6"><input type="number" name="memberid" id="memberid" value="<?=$hasil[1]?>" <?php echo $disable; ?>></td>
                                    </tr>

                                    <tr>
                                        <td>Nama</td>
                                        <td>:</td>
                                        <td class="col-sm-6"><input type="text" name="nama" id="nama" value="<?=$hasil[2]?>" <?php echo $disable; ?>></td>
                                    </tr>

                                    <tr>
                                        <td>Alamat</td>
                                        <td>:</td>
                                        <td class="col-sm-6"><input type="text" name="alamat" id="alamat" value="<?=$hasil[3]?>" <?php echo $disable; ?>></td>
                                    </tr>

                                    <!-- <tr>
                                        <td>Telepon</td>
                                        <td>:</td>
                                        <td class="col-sm-6"><input type="number" name="telp" id="telp" value="<?=$hasil[4]?>" <?php echo $disable; ?>></td>
                                    </tr>


                                    <tr>
                                        <td>Email</td>
                                        <td>:</td>
                                        <td class="col-sm-6"><input type="email" name="email" id="email" value="<?=$hasil[5]?>" <?php echo $disable; ?>></td>
                                    </tr> -->
                                </table>

                                <a href="javascript:void(0);" id="jwpopupLink">Klik disini untuk detail</a>
                                

                                    <!-- jwpopup box -->
                                    <div id="jwpopupBox" class="jwpopup">
                                        <!-- jwpopup content -->
                                        <div class="jwpopup-content">
                                            <div class="jwpopup-head">
                                                <span class="close" style="background: black;">×</span>
                                                <h3>Detail Daftar Anggota</h3>
                                            </div>
                                            <div class="jwpopup-main">
                                                <form >
                                                <table >
                                                    <tbody>
                                                        <tr>
                                                            <td>Member ID</td>
                                                            <td>:</td>
                                                            <td><input type="number" name="memberid" id="memberid" value="<?=$hasil[1]?>" <?php echo $disable; ?>></td></td>
                                                        </tr>
                                                         <tr>
                                                            <td>Nama</td>
                                                            <td>:</td>
                                                            <td><input type="text" name="nama" id="nama" value="<?=$hasil[2]?>" <?php echo $disable; ?>></td></td>
                                                        </tr>
                                                         <tr>
                                                            <td>Alamat</td>
                                                            <td>:</td>
                                                            <td><input type="text" name="alamat" id="alamat" value="<?=$hasil[3]?>" <?php echo $disable; ?>></td></td>
                                                        </tr>
                                                         <tr>
                                                            <td>Telepon</td>
                                                            <td>:</td>
                                                            <td><input type="number" name="telepon" id="telepon" value="<?=$hasil[4]?>" <?php echo $disable; ?>></td></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Email</td>
                                                            <td>:</td>
                                                            <td><input type="email" name="email" id="email" value="<?=$hasil[5]?>" <?php echo $disable; ?>></td></td>
                                                        </tr>
                                                         <tr>
                                                            <td>Jabatan</td>
                                                            <td>:</td>
                                                            <td><input type="text" name="jabatan" id="jabatan" value="<?=$jab2[6]?>" <?php echo $disable; ?>></td></td>
                                                        </tr>
                                                        <tr>
                                                            <td>NIP</td>
                                                            <td>:</td>
                                                            <td><input type="text" name="nip" id="nip" value="<?=$hasil[7]?>" <?php echo $disable; ?>></td></td>
                                                        </tr>
                                                        <tr>
                                                            <td>KTP</td>
                                                            <td>:</td>
                                                            <td><input type="text" name="ktp" id="ktp" value="<?=$hasil[6]?>" <?php echo $disable; ?>></td></td>
                                                        </tr>
                                                        
                                                    </tbody>
                                                </table>
                                                </form>
                                            </div>
                                            <div class="jwpopup-foot">
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            <?php } ?>
                            <?php } ?>
                </div>

                <div class="row">
                <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-lg-12 table-responsive">
                    <h4>Saldo Simpanan</h4>

                    <?php
                    //cek
                    if(isset($_GET['acc'])){
                    $acc = $_GET['acc'];

                    $no1 = 1;
                    $ulsql = "SELECT * FROM[dbo].[MemberListView] WHERE KID='$_SESSION[KID]' and MemberID='$acc'";
                    //echo $ulsql;
                    $ulstmt = sqlsrv_query($conn, $ulsql);
                    while($ulrow = sqlsrv_fetch_array( $ulstmt, SQLSRV_FETCH_NUMERIC)){
                        
                        $sql1   = "SELECT * FROM ( SELECT a.KID,a.MemberID,a.KodeBasicSavingType,a.AccNo,a.BillingBalance,a.PaymentBalance,c.Status, ROW_NUMBER() OVER (ORDER BY a.KID asc) as row FROM [dbo].[BasicSavingBalance] a inner join[dbo].[BasicSavingType] c on a.KodeBasicSavingType = c.KodeBasicSavingType where a.MemberID = '$ulrow[1]' and c.Status = '1') a";

                        //echo $sql1;
                        $stmt1  = sqlsrv_query($conn, $sql1);
                        $row1  = sqlsrv_fetch_array($stmt1, SQLSRV_FETCH_NUMERIC);                    
                        $sql2   = "SELECT * FROM ( SELECT a.KID,a.MemberID,a.KodeBasicSavingType,a.AccNo,a.BillingBalance,a.PaymentBalance,c.Status, ROW_NUMBER() OVER (ORDER BY a.KID asc) as row FROM [dbo].[BasicSavingBalance] a inner join[dbo].[BasicSavingType] c on a.KodeBasicSavingType = c.KodeBasicSavingType where a.MemberID = '$ulrow[1]' and c.Status = '0') a";
                        $stmt2  = sqlsrv_query($conn, $sql2);
                        $row2  = sqlsrv_fetch_array($stmt2, SQLSRV_FETCH_NUMERIC);
                        $sql   = "SELECT * FROM ( SELECT a.KID,a.MemberID,a.KodeBasicSavingType,a.AccNo,a.BillingBalance,a.PaymentBalance,c.Status, ROW_NUMBER() OVER (ORDER BY a.KID asc) as row FROM [dbo].[BasicSavingBalance] a inner join[dbo].[BasicSavingType] c on a.KodeBasicSavingType = c.KodeBasicSavingType where a.MemberID = '$ulrow[1]' and c.Status = '2') a";
                        //echo $sql;                            
                        $stmt  = sqlsrv_query($conn, $sql);
                        $row   = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_NUMERIC);

                        $sql3   = "SELECT sum(a.PaymentBalance) FROM ( SELECT a.KID,a.MemberID,a.KodeBasicSavingType,a.AccNo,a.BillingBalance,a.PaymentBalance,c.Status, ROW_NUMBER() OVER (ORDER BY a.KID asc) as row FROM [dbo].[BasicSavingBalance] a inner join[dbo].[BasicSavingType] c on a.KodeBasicSavingType = c.KodeBasicSavingType where a.MemberID = '$ulrow[1]') a";
                        //echo $sql3;
                        $stmt3  = sqlsrv_query($conn, $sql3);
                        $row3  = sqlsrv_fetch_array($stmt3, SQLSRV_FETCH_NUMERIC);   
                    ?>
                    <?php } ?>
                    <?php } ?>

                    <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-lg-12 table-responsive">
                        <tr>
                            <th>1. Saldo Simpanan Pokok</th>
                        </tr>
                        <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th><?php echo lang('Kode Akun'); ?></th>
                                <th><?php echo lang('Total'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td align="right"><a href="rep_regbasic.php?acc2=<?php echo $row1[3]; ?>&st=1&pagem=<?php echo $_GET['page'];?>&acc=<?php echo $_GET['acc'];?>&user=<?php echo $hcariuser[1];?>"><?php echo $row1[3]; ?></a></td>
                                <td style="text-align: right"><?php echo number_format($row1[5],2); ?></td>
                            </tr>
                        </tbody>
                        </table>
                    </div>

                    <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-lg-12 table-responsive">
                        <tr>
                            <th>2. Saldo Simpanan Wajib</th>
                        </tr>
                        <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th><?php echo lang('Kode Akun'); ?></th>
                                <th><?php echo lang('Total'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td align="right"><a href="rep_regbasic.php?acc2=<?php echo $row2[3]; ?>&st=1&pagem=<?php echo $_GET['page'];?>&acc=<?php echo $_GET['acc'];?>&user=<?php echo $hcariuser[1];?>"><?php echo $row2[3]; ?></a></td>
                                <td style="text-align: right"><?php echo number_format($row2[5],2); ?></td>
                            </tr>
                        </tbody>
                        </table>
                    </div>

                    <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-lg-12 table-responsive">
                        <tr>
                            <th>3. Saldo Simpanan Sukarela</th>
                        </tr>
                        <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th><?php echo lang('Kode Akun'); ?></th>
                                <th><?php echo lang('Total'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td align="right"><a href="rep_regbasic.php?acc2=<?php echo $row[3]; ?>&st=1&pagem=<?php echo $_GET['page'];?>&acc=<?php echo $_GET['acc'];?>&user=<?php echo $hcariuser[1];?>"><?php echo $row[3]; ?></a></td>
                                <td style="text-align: right"><?php echo number_format($row[5],2); ?></td>
                            </tr>
                        </tbody>
                        </table>
                    </div>

                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th><?php echo lang('Total Keseluruhan Simpanan'); ?></th>
                                <th style="text-align: right"><?php echo number_format($row3[0],2); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                          
                        </tbody>
                    </table>
                </div>
                </div>

                <div class="row">
                <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-lg-12 table-responsive">
                    <h4>Saldo Tabungan</h4>
                        <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-lg-12 table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th><?php echo lang('No. Akun'); ?></th>
                                        <!-- <th><?php echo lang('Nama'); ?></th> -->
                                        <th><?php echo lang('Tanggal Buka'); ?></th>
                                        <th><?php echo lang('Tanggal Tutup'); ?></th>
                                        <th><?php echo lang('Produk'); ?></th>
                                        <th><?php echo lang('Saldo'); ?></th>           
                                        <th><?php echo lang('Status'); ?></th>
                                    </tr>
                                </thead>
                                 <?php       
                                    //count
                                    if(isset($_GET['acc'])){
                                    $acc = $_GET['acc'];
                                    $ulsql = "SELECT * FROM [dbo].[RegularSavingAcc] WHERE KID='$_SESSION[KID]' and MemberID='$acc' and status != '0'";
                                    //echo $ulsql;
                                    $ulstmt = sqlsrv_query($conn, $ulsql);
                                    $no2 = 1;
                                    while($ulrow = sqlsrv_fetch_array( $ulstmt, SQLSRV_FETCH_NUMERIC)){
                                    $open = '';
                                    $close = '';
                                    if ($ulrow[6] == '0') {
                                            $sts = 'Close';
                                    } else if($ulrow[6] == '9') {
                                            $sts = 'Default';
                                    } else {
                                            $sts = 'Open';
                                    }



                                    $sql   = "select * from dbo.RegularSavingReportList where AccNo = '$ulrow[2]'";
                                    //echo $sql;
                                    $stmt  = sqlsrv_query($conn, $sql);
                                    $row   = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_NUMERIC);
                                    
                                    $status = "select * from dbo.RegularSavingtype where RegularSavingtype = '$ulrow[3]'";
                                    $statusproses = sqlsrv_query($conn, $status);
                                    while ($statushasil = sqlsrv_fetch_array($statusproses, SQLSRV_FETCH_NUMERIC)){
                                        $type = $statushasil[1];
                                    }

                                    $close = "select * from dbo.RegularSavingClose where AccNo = '$ulrow[2]'";
                                    //echo $close;
                                    $closeproses = sqlsrv_query($conn, $close);
                                    $closehasil = sqlsrv_fetch_array($closeproses, SQLSRV_FETCH_NUMERIC);
                                        //echo $closehasil;
                                        if ($closehasil != null ){
                                            $closetanggal = $closehasil[1]->format('Y-m-d H:i:s');        
                                        } else {
                                            $closetanggal = '-';
                                        }

                                        //echo $closetanggal;

                                    $opentanggal1 = "select top 1 * from dbo.RegularSavingTrans where AccNumber = '$ulrow[2]' order by TimeStam asc";
                                    //echo $opentanggal1;
                                    $openproses = sqlsrv_query($conn, $opentanggal1);
                                    $openhasil = sqlsrv_fetch_array($openproses, SQLSRV_FETCH_NUMERIC);
                                    if ($openhasil != null){
                                        $opentanggal = $openhasil[3]->format('Y-m-d H:i:s');
                                    } else {
                                        $opentanggal = '-';
                                    } 

                                    

                                    ?>
                                    <tr>
                                        <td style="text-align: right"><?php echo $no2; ?></td>
                                        <td align="right"><a href="rep_regreg.php?acc2=<?php echo $ulrow[2]; ?>&st=2&pagem=<?php echo $_GET['page'];?>&acc=<?php echo $_GET['acc'];?>&user=<?php echo $hcariuser[1];?>"><?php echo $ulrow[2]; ?></a></td>
                                        <!-- <td><?php echo $ulrow[7]; ?></td> -->
                                        <td><?php echo $opentanggal; ?></td>
                                        <td><?php echo $closetanggal; ?></td>
                                        <td><?php echo $type; ?></td>
                                        <td style="text-align: right;"><?php echo number_format($ulrow[4],2); ?></td>
                                        <td><?php echo $sts; ?></td>
                                    </tr>
                                    <?php
                                     $no2++; }
                                    ?>
                                    <?php $no2++; } ?>
                                    </tbody>
                            </table>

                        </div>
                        

                </div>
                </div>

                <div class="row">
                <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-lg-12 table-responsive">
                    <h4>Saldo Deposito</h4>
                        <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-lg-12 table-responsive">
                            <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th><?php echo lang('Nomor Akun'); ?></th>
                                    <!-- <th><?php echo lang('Nama'); ?></th> -->
                                    <th><?php echo lang('Tanggal Buka'); ?></th>
                                    <th><?php echo lang('Tanggal Tutup'); ?></th>
                                    <th><?php echo lang('Produk'); ?></th>
                                    <th><?php echo lang('Suku Bunga%'); ?></th>
                                    <th><?php echo lang('Saldo'); ?></th>
                                    <th><?php echo lang('Satus'); ?></th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php
                                if(isset($_GET['acc'])){
                                $acc = $_GET['acc'];
                                $ulsql = "SELECT * FROM [dbo].[TimeDepositReport] where MemberID='$acc'";
                                //echo $ulsql;
                                $tanggalbaku = '1900-01-01 00:00:00.0000000';
                                //echo $tanggalbaku;
                                $ulstmt = sqlsrv_query($conn, $ulsql);
                                $no3 = 1;
                                while($ulrow = sqlsrv_fetch_array( $ulstmt, SQLSRV_FETCH_NUMERIC)){
                                    $open = $ulrow[7]->format('Y-m-d H:i:s');
                                    $tglcls = $ulrow[9]->format('Y-m-d H:i:s');
                                    $close = '';
                                    $tanggalbaku = '1900-01-01 00:00:00';
                                    //echo $tglcls;
                                    //echo $tanggalbaku;
                                    

                                    if ($tglcls !== $tanggalbaku ){
                                            $close = $ulrow[9]->format('Y-m-d H:i:s');                                                   
                                        } else {
                                            
                                            $close = '-'; 
                                        }
                                    ?>
                                    <tr>
                                        <td style="text-align: right"><?php echo $no3; ?></td>
                                        <td align="right"><a href="rep_regtime.php?acc2=<?php echo $ulrow[2]; ?>&st=3&pagem=<?php echo $_GET['page'];?>&acc=<?php echo $_GET['acc'];?>&user=<?php echo $hcariuser[1];?>"><?php echo $ulrow[2]; ?></a></td>
                                        <!-- <td><?php echo $ulrow[1]; ?></td> -->
                                        <td><?php echo $open; ?></td>
                                        <td><?php echo $close; ?></td>
                                        <td><?php echo $ulrow[4]; ?></td>
                                        <td style="text-align: right;"><?php echo $ulrow[5]; ?>%</td>
                                        <td style="text-align: right;"><?php echo $ulrow[6]; ?></td>
                                        <td><?php echo $ulrow[10]; ?></td>
                                    </tr>
                                <?php $no3++; } ?>
                                <?php } ?>
                            </tbody>

                            </table>
                        </div>
                </div>
                </div>

                <div class="row">
                <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-lg-12 table-responsive">
                    <h4>Daftar Pinjaman</h4>
                        <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-lg-12 table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th><?php echo lang('No. Pinjaman'); ?></th>
                                        <th><?php echo lang('Pinjaman Awal'); ?></th>
                                        <th><?php echo lang('Pinjaman Disetujui'); ?></th>
                                        <th><?php echo lang('Periode Awal'); ?></th>
                                        <th><?php echo lang('Periode Akhir'); ?></th>
                                        <th><?php echo lang('Produk'); ?></th>
                                        <th><?php echo lang('Sisa Pinjaman'); ?></th>
                                        <th><?php echo lang('Status'); ?></th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <?php
                                    //count
                                    if(isset($_GET['acc'])){  
                                        $acc = $_GET['acc'];
                                        $no4 = 1;
                                        $cuser = "select * from [PaymentGateway].[dbo].[UserMemberKoperasi] where KodeMember = '$acc' and KID = '$_SESSION[KID]'";
                                        //echo $cuser;
                                        $puser = sqlsrv_query($conn, $cuser);
                                            while ($huser = sqlsrv_fetch_array( $puser, SQLSRV_FETCH_NUMERIC)){
                                            $ulsql1 = "SELECT sum(LoanAmmount) FROM [dbo].[LoanList] WHERE UserID = '$huser[1]'";
                                            //echo $ulsql1;
                                            $ulstmt1 = sqlsrv_query($conn, $ulsql1);
                                            $ulrow1 = sqlsrv_fetch_array( $ulstmt1, SQLSRV_FETCH_NUMERIC);    
                                         }

                                        
                                        $ulsql = "SELECT * FROM [dbo].[LoanReportBalance] WHERE MemberID='$acc'";
                                        //echo $ulsql;
                                        $ulstmt = sqlsrv_query($conn, $ulsql);
                                        
                                        while($ulrow = sqlsrv_fetch_array( $ulstmt, SQLSRV_FETCH_NUMERIC)){
                                            $ammoun = "select * from dbo.loanrelease where KodeAppNum = '$ulrow[0]'";
                                            $pammoun = sqlsrv_query($conn, $ammoun);
                                            $hammoun  = sqlsrv_fetch_array( $pammoun, SQLSRV_FETCH_NUMERIC);
                                            
                                            $close = '';
                                            $open = '';
                                            if ($ulrow[2] != ''){
                                                $open = $ulrow[2]->format('Y-m-d H:i:s');    
                                            } else {
                                                $open = '-';
                                            }
                                            if($ulrow[3] != ''){
                                                $close = $ulrow[3]->format('Y-m-d H:i:s');;
                                            } else {
                                                $close = '-';
                                            }
                                            ?>
                                            <tr>
                                                <td style="text-align: right"><?php echo $no4; ?></td>
                                                <td align="right"><a href="rep_regloan.php?acc2=<?php echo $ulrow[0]; ?>&st=4&pagem=<?php echo $_GET['page'];?>&acc=<?php echo $_GET['acc'];?>&user=<?php echo $hcariuser[1];?>"><?php echo $ulrow[0]; ?></a></td>
                                                <td style="text-align: right;"><?php echo $ulrow[1]; ?></td>
                                                <td style="text-align: right;"><?php echo number_format($hammoun[3],2); ?></td>
                                                <td><?php echo $open; ?></td>
                                                <td><?php echo $close; ?></td>
                                                <td><?php echo $ulrow[4]; ?></td>
                                                <td style="text-align: right;"><?php echo $ulrow[5]; ?></td>
                                                <td><?php echo $ulrow[6]; ?></td>
                                            </tr>
                                        <?php  } ?>
                                        <?php $no4++; } ?>
                                    </tbody>
                            </table>
                        </div>
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th><?php echo lang('Total Keseluruhan Pinjaman'); ?></th>
                                    <th style="text-align: right"><?php echo number_format($ulrow1[0],2); ?></th>
                                </tr>
                            </thead>
                            <tbody> 
                            </tbody>
                        </table>
                </div>
                </div>

                <div class="row">
                <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-lg-12 table-responsive">
                    <h4>Tagihan Bulanan</h4>
                        <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-lg-12 table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Member ID</th>
                                        <!-- <th><?php echo lang('Nama'); ?></th> -->
                                        <th><?php echo lang('Hutang Simpanan Pokok'); ?></th>
                                        <th><?php echo lang('Hutang Simpanan Wajib'); ?></th>
                                        <th><?php echo lang('Hutang Simpanan Sukarela'); ?></th>
                                        <th><?php echo lang('Pokok Pinjaman'); ?></th>
                                        <th><?php echo lang('Bunga Pinjaman'); ?></th>
                                        <th><?php echo lang('Belanja Toko'); ?></th>
                                        <th><?php echo lang('Total'); ?></th>
                                    </tr>
                                </thead>
                        
                                <tbody>
                                <?php
                                if(isset($_GET['acc'])){
                                $acc = $_GET['acc'];
                                $ulsql = "SELECT * FROM [dbo].[TagihanBulanan] WHERE MemberID='$acc'";
                                $ulstmt = sqlsrv_query($conn, $ulsql);
                                $no5 = 1;
                                while($ulrow = sqlsrv_fetch_array( $ulstmt, SQLSRV_FETCH_NUMERIC)){
                                ?>
                                    <tr>
                                        <td style="text-align: right"><?php echo $no5; ?></td>
                                        <td align="right"><?php echo $ulrow[0]; ?></td>
                                        <!-- <td><?php echo $ulrow[1]; ?></td> -->
                                        <td style="text-align: right;"><?php echo number_format($ulrow[3],2); ?></td>
                                        <td style="text-align: right;"><?php echo number_format($ulrow[5],2); ?></td>
                                        <td style="text-align: right;"><?php echo number_format($ulrow[7],2); ?></td>
                                        <td style="text-align: right;"><?php echo number_format($ulrow[8],2); ?></td>
                                        <td style="text-align: right;"><?php echo number_format($ulrow[9],2); ?></td>
                                        <td style="text-align: right;"><?php echo number_format($ulrow[10],2); ?></td>
                                        <td style="text-align: right;"><?php echo number_format($ulrow[11],2); ?></td>
                                    </tr>
                                <?php $no5++; } ?>
                                <?php  } ?>
                                </tbody>
                            </table>
                        </div>
                </div>
                </div>

            </div>
    </div>
</div>

<script type="text/javascript">
    // untuk mendapatkan jwpopup
var jwpopup = document.getElementById('jwpopupBox');

// untuk mendapatkan link untuk membuka jwpopup
var mpLink = document.getElementById("jwpopupLink");

// untuk mendapatkan aksi elemen close
var close = document.getElementsByClassName("close")[0];

// membuka jwpopup ketika link di klik
mpLink.onclick = function() {
    jwpopup.style.display = "block";
}

// membuka jwpopup ketika elemen di klik
close.onclick = function() {
    jwpopup.style.display = "none";
}

// membuka jwpopup ketika user melakukan klik diluar area popup
window.onclick = function(event) {
    if (event.target == jwpopup) {
        jwpopup.style.display = "none";
    }
}
</script>

<?php require('footer_new.php'); ?>
