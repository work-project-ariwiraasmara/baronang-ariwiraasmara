<?php
session_start();
error_reporting(0);
include "connect.php";
include "connectinti.php";

$tanggal = date('Y-m-d');
$idsession = $_POST['idsession'];
$userid = $_POST['userid'];
$amount = $_POST['amount'];

if(isset($idsession) and isset($userid) and isset($amount)){
  $a = "select * from [dbo].[CashierSession] where TransNo='$idsession' and Status = 1";
  $b = sqlsrv_query($conn, $a);
  $c = sqlsrv_fetch_array( $b, SQLSRV_FETCH_NUMERIC);
  if($c != null){
    $tanggal = date('Y-m-d H:i:s');

    $beginbalance = $c[3];
    $debet = $c[4];
    $kredit = $c[5];
    $endbalance = $c[6];

    $balance = ($beginbalance + $debet) - $kredit;
    $sisa = $balance - $amount;

    $sql = "update dbo.CashierSession set Status = 2, SettlementBalance = '$amount', SettlementTime = '$tanggal', SettlementBy = '$_SESSION[UserID]' where TransNo = '$idsession'";
    $stmt = sqlsrv_query($conn, $sql);
    if($stmt){
      $a = "select dbo.getKodeJurnalTransaction ('$_SESSION[KID]','SETT')";
      $b = sqlsrv_query($conn, $a);
      $c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC);
      $transNumber = $c[0];
      if($transNumber != null){
        if($balance > $amount){
            $kba1 = '6.1.01.01.007'; //biaya lain2
            $kba2 = '1.1.01.01.001'; //kas teller
        }

        if($balance < $amount){
            $kba1 = '1.1.01.01.002'; //kas teller
            $kba2 = '4.1.03.01.002'; //pendapatan
        }

        if($kba1 != '' and $kba2 != ''){
          $keterangan = 'Settlement Cashier / Teller';
          $sql = "exec [dbo].[ProsesJurnalTrans] '$_SESSION[KID]','$transNumber','$tanggal','SETT','$kba1','','$keterangan','$sisa',9,'$_SESSION[UserID]'";
          $stmt = sqlsrv_query($conn, $sql);

          $sql = "exec [dbo].[ProsesJurnalTrans] '$_SESSION[KID]','$transNumber','$tanggal','SETT','','$kba2','$keterangan','$sisa',9,'$_SESSION[UserID]'";
          $stmt = sqlsrv_query($conn, $sql);


          //untuk translist
          $trans = "exec [dbo].[ProsesTransList] '$transNumber','','','$kba1','$kba2','$sisa','SETT','','$keterangan','$_SESSION[UserID]',''";
          //echo "$trans";
          $list = sqlsrv_query($conn, $trans);

          $note = 'Transaksi Kas '.$keterangan;
          $sql2 = "exec [dbo].[ProsesJurnalDetail] '$transNumber','$kba1','$sisa',0,'$note'";
          $stmt2 = sqlsrv_query($conn, $sql2);

          $sql3 = "exec [dbo].[ProsesJurnalDetail] '$transNumber','$kba2',0,'$sisa','$note'";
          $stmt3 = sqlsrv_query($conn, $sql3);

          $sql4 = "exec [dbo].[ProsesJurnalHeader] '$transNumber','$tanggal','$_SESSION[KID]','$_SESSION[UserID]'";
          $stmt4 = sqlsrv_query($conn, $sql4);
        }
      }

      messageAlert(lang('Berhasil simpan settlement'),'success');
      header('Location: settlement.php?userid='.$userid);
    }
    else{
      messageAlert(lang('Gagal simpan settlement'),'danger');
      header('Location: settlement.php?userid='.$userid);
    }

  }
  else{
    messageAlert(lang('Session tidak ditemukan'),'info');
    header('Location: settlement.php?userid='.$userid);
  }
}
else{
  messageAlert(lang('Inputan tidak valid'),'info');
  header('Location: settlement.php?userid='.$userid);

} 

?>
