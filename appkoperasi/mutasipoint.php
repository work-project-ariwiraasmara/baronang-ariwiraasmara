<?php
@session_start();
error_reporting(0);
require('header.php');
require('sidebar-right.php');
require('sidebar-left.php');

$point = '-';
$thpoint1 = '-';
$thpoint2 = '-';
?>

	<div class="animated fadeinup delay-1">
        <div class="page-content txt-black">
			
        	<h1 class="txt-black uppercase bold">mutasi point</h1><br>

        	<?php
        	if(@$_GET['type'] == 'perbulan') { ?>
        		<h3 class="uppercase "><?php echo date('F', strtotime(@$_GET['time'])); ?></h3><br>

	        	<div class="table-responsive m-t-30 p-20">
	        		<table class="table">
	        			<thead>
	        				<tr>
	        					<th style="border-top: none; border-bottom: 1px solid #000;"> 
	        						<a href="?subtype=pertahun&tahun=<?php echo date('Y', strtotime(@$_GET['time'])); ?>">
	        							<button class="btn primary-color width-100 waves-effect waves-light"> 
		        							<i class="ion-ios-arrow-back"></i> 
		        						</button> 
	        						</a>
	        					</th>
		        			<?php
				        	if( !(date('m', strtotime(@$_GET['time'])) == '01') ) { ?>
				        		<th colspan="3" style="text-align: right; border-top: none; border-bottom: 1px solid #000;">Saldo Bulan Sebelumnya :</th>
		        				<th style="text-align: right; border-top: none; border-bottom: 1px solid #000;"><?php echo number_format(@$_GET['saldo'], 2,".",","); ?></th>
		        				<?php
				        	}
				        	?>
				        	</tr>
	        				
	        				<tr>
	        					<th rowspan="2" style="text-align: center; border: 1px solid #000;">Tanggal</th>
	        					<th rowspan="2" style="text-align: center; border: 1px solid #000;">Pembelian Point</th>
	        					<th colspan="2" style="text-align: center; border: 1px solid #000;">Pemakaian Point</th>
	        					<th rowspan="2" style="text-align: center; border: 1px solid #000;">Saldo</th>
	        				</tr>

	        				<tr>
	        					<th style="text-align: center; border: 1px solid #000;">Beli Member</th>
	        					<th style="text-align: center; border: 1px solid #000;">Parkir</th>
	        				</tr>
	        			</thead>

	        			<tbody>
	        				<tr>
	        					<th style="text-align: center; border: 1px solid #000;">1</th>
	        					<th style="text-align: center; border: 1px solid #000;">2</th>
	        					<th style="text-align: center; border: 1px solid #000;">3</th>
	        					<th style="text-align: center; border: 1px solid #000;">4</th>
	        					<th style="text-align: center; border: 1px solid #000;">5</th>
	        				</tr>

	        				<?php
	        				$total = @$_GET['saldo'];
	        				$totbul = @$_GET['saldo'];
	        				$tahbul = date('Y-m', strtotime(@$_GET['time']));
	        				$ld = date('t', strtotime(@$_GET['time']));

	        				$pembelian = 0;
	        				$member = 0;
	        				$parkir = 0;
	        				for($x = 1; $x <= $ld; $x++) { 
	        					$tgl = $tahbul.'-'.$x;
	        					$sql = "SELECT TOP 1 (SELECT Sum(Kredit) from [dbo].[ParkingTrans] where KodeTransactionType='TOPP' and Convert(date, TimeStam) = '$tgl'),
	        											 (SELECT Sum(Kredit) from [dbo].[ParkingTrans] where KodeTransactionType='MBRS' and Convert(date, TimeStam) = '$tgl'),
	        											 (SELECT Sum(Kredit) from [dbo].[ParkingTrans] where KodeTransactionType='PARK' and Convert(date, TimeStam) = '$tgl')
	        								from [dbo].[ParkingTrans] where Convert(date, TimeStam) = '$tgl'";
	        					$query = sqlsrv_query($conn, $sql);
	        					$data = sqlsrv_fetch_array($query);
	        					$totbul = $total + $data[0] - $data[1] - $data[2];

	        					$pembelian = $pembelian + $data[0];
	        					$member = $member + $data[1];
	        					$parkir = $parkir + $data[2];
	        					?>
	        					<tr>
		        					<td style="text-align: center; border: 1px solid #000;">
		        						<?php echo $x; ?>
		        					</td>
		        					<td style="text-align: right; border: 1px solid #000;">
		        						<?php echo number_format($data[0], 2,".",","); ?>
		        					</td>
		        					<td style="text-align: right; border: 1px solid #000;">
		        						<?php echo number_format($data[1], 2,".",","); ?>
		        					</td>
		        					<td style="text-align: right; border: 1px solid #000;">
		        						<?php echo number_format($data[2], 2,".",","); ?>
		        					</td>
		        					<td style="text-align: right; border: 1px solid #000;">
		        						<?php echo number_format($totbul, 2,".",","); ?>
		        					</td>
	        					</tr>
	        					<?php
	        					$total = $totbul;
	        				}
	        				?>
	        			</tbody>

	        			<tfoot>
	        				<tr>
	        					<th style="text-align: center; border: 1px solid #000;">Total</th>
	        					<th style="text-align: right; border: 1px solid #000;"><?php echo number_format($pembelian, 2,".",","); ?></th>
	        					<th style="text-align: right; border: 1px solid #000;"><?php echo number_format($member, 2,".",","); ?></th>
	        					<th style="text-align: right; border: 1px solid #000;"><?php echo number_format($parkir, 2,".",","); ?></th>
	        					<th style="text-align: right; border: 1px solid #000;">
	        						<?php
	        						$total2 = @$_GET['saldo'];
	        						$totbul2 = $total2 + $pembelian - $member - $parkir;
	        						echo number_format($totbul2, 2,".",",");
	        						?>
	        					</th>
	        				</tr>
	        			</tfoot>
	        		</table>
	        	</div>
        		<?php
        	}
        	else if(@$_GET['type'] == 'point') { 
        		if(@$_GET['subtype'] == 'pembelian') {
        			$point = 'Pembelian Point';
        			$thpoint1 = 'EDC';
        			$thpoint2 = 'No. Kartu';
        		}
        		else if(@$_GET['subtype'] == 'member') {
        			$point = 'Pembelian Member';
        			$thpoint1 = 'No. Kartu';
        			$thpoint2 = 'Lokasi';
        		}
        		else {
        			$point = 'Parkir';
        			$thpoint1 = 'No. Kartu';
        			$thpoint2 = 'Lokasi';
        		}
        		?>
        		<h3 class="uppercase"><?php echo date('F', strtotime(@$_GET['time'])); ?></h3><br>
        		
        		<div class="table-responsive m-t-30 p-20">
        			<table class="table">
        				<thead>
        					<tr>
        						<th style="border-top: none; border-bottom: 1px solid #000;"> 
        							<a href="?subtype=pertahun&tahun=<?php echo date('Y', strtotime(@$_GET['time'])); ?>">
        								<button class="btn primary-color width-100 waves-effect waves-light"> 
        									<i class="ion-ios-arrow-back"></i> 
        								</button>
        							</a>
        						</th>
        						<th style="border-top: none; border-bottom: 1px solid #000;"><h3 class="uppercase"><?php echo $point; ?></u></b></h3></th>
        					</tr>
        					<tr>
        						<th style="border: 1px solid #000;">Tanggal</th>
        						<th style="border: 1px solid #000;"><?php echo $thpoint1; ?></th>
        						<th style="border: 1px solid #000;"><?php echo $thpoint2; ?></th>
        						<th style="border: 1px solid #000;">Jumlah</th>
        					</tr>
        				</thead>

        				<tbody>
        					<?php
        					$sqtpst = "SELECT TOP 1 * from [dbo].[ParkingSetting]";
        					$qrtpst = sqlsrv_query($conn, $sqtpst);
	        				$drtpst = sqlsrv_fetch_array($qrtpst);
	        				$tpcv = $drtpst[1];

	        				$tahbul = date('Y-m', strtotime(@$_GET['time']));
	        				$ld = date('t', strtotime(@$_GET['time']));

	        				$total = 0;
	        				for($x = 1; $x <= $ld; $x++) {
	        					$amount = 0;
	        					$tgl = $tahbul.'-'.$x;

	        					if(@$_GET['subtype'] == 'pembelian') {
	        						$type = 'TOPP';
	        					}
				        		else if(@$_GET['subtype'] == 'member') {
				        			$type = 'MBRS';
				        		}
				        		else {
				        			$type = 'PARK';
				        		}

				        		$sql = "SELECT TOP 1 AccountDebet,
	        										 AccountKredit, 
	        										 (SELECT Sum(Amount) from [dbo].[TransList] where TransactionType='$type' and Convert(date, [Date]) = '$tgl') 
	        							from [dbo].[TransList] where TransactionType='$type' and Convert(date, [Date]) = '$tgl' ORDER BY [Date] DESC";

				        		$query = sqlsrv_query($conn, $sql);
	        					$data = sqlsrv_fetch_array($query);

	        					$amount = $data[2] / $tpcv;
	        					if(@$_GET['subtype'] != 'pembelian') {
	        						$sloc = "SELECT * from [dbo].[LocationMerchant] where LocationID='".$data[1]."'";
	        						$qloc = sqlsrv_query($conn, $sloc);
	        						$dloc = sqlsrv_fetch_array($qloc);
	        						$loc = $dloc[2];
	        					}
	        					?>
	        					<tr>
	        						<td style="text-align: center; border: 1px solid #000;"><?php echo $x; ?></td>
	        						<td style="border: 1px solid #000;">
	        							<?php 
	        							if( empty($data[0]) || is_null($data[0]) || $data[0] == '' ) {
	        								echo '-';
	        							}
	        							else {
	        								echo $data[0]; 
	        							}
	        							?>
	        						</td>
	        						<td style="border: 1px solid #000;">
	        							<?php
	        							if(@$_GET['subtype'] != 'pembelian') {
	        								if( empty($loc) || is_null($loc) || $loc == '' ) {
		        								echo '-';
		        							}
		        							else {
		        								echo $loc; 
		        							}
	        								
	        							}
	        							else {
	        								if( empty($data[1]) || is_null($data[1]) || $data[1] == '' ) {
		        								echo '-';
		        							}
		        							else {
		        								echo $data[1]; 
		        							}
	        							}
	        							?>
	        						</td>
	        						<td style="text-align: right; border: 1px solid #000;"><?php echo number_format($amount, 2,".",","); ?></td>
	        					</tr>
	        					<?php
	        					$total = $total + $amount;
	        				}
        					?>

        					<tr>
        						<th colspan="3" style="text-align: right; border: 1px solid #000;">Total</th>
        						<th style="text-align: right; border: 1px solid #000;"><?php echo number_format($total, 2,".",","); ?></th>
        					</tr>
        				</tbody>
        			</table>
        		</div>
        		<?php
        	}
        	else { ?>
        		<div class="m-t-30">
        			<b>Tahun :</b><br>
			        <select id="mp_tahun" class="browser-default">
			        	<?php
			        	for($x = date('Y'); $x >= 2017; $x--) { ?>
			        		<option value="<?php echo $x; ?>"><?php echo $x; ?></option>
			        		<?php
			        	}
			        	?>
			        </select>

			        <button id="btn-type_default" class="btn btn-large width-100 waves-effect waves-light primary-color m-t-10">OK</button>
        		</div>

	        	<div class="table-responsive m-t-30">
	        		<table class="table">
	        			<thead>
	        				<tr>
	        					<th rowspan="2" style="text-align: center; border: 1px solid #000;">Bulan</th>
	        					<th rowspan="2" style="text-align: center; border: 1px solid #000;">Pembelian Point</th>
	        					<th colspan="2" style="text-align: center; border: 1px solid #000;">Pemakaian Point</th>
	        					<th rowspan="2" style="text-align: center; border: 1px solid #000;">Saldo</th>
	        				</tr>

	        				<tr>
	        					<th style="text-align: center; border: 1px solid #000;">Beli Member</th>
	        					<th style="text-align: center; border: 1px solid #000;">Parkir</th>
	        				</tr>
	        			</thead>

	        			<tbody>
	        				<tr>
	        					<th style="text-align: center; border: 1px solid #000;">1</th>
	        					<th style="text-align: center; border: 1px solid #000;">2</th>
	        					<th style="text-align: center; border: 1px solid #000;">3</th>
	        					<th style="text-align: center; border: 1px solid #000;">4</th>
	        					<th style="text-align: center; border: 1px solid #000;">5</th>
	        				</tr>

	        				<?php
	        				if(@$_GET['subtype'] == 'pertahun') {
	        					//echo 'OK';
	        					$tahun = @$_GET['tahun'];
	        					$bulan = array('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12');
	        					$total = 0;
	        					$totbul = 0;

	        					for($x = 0; $x < count($bulan); $x++) {
	        						$ds = $tahun.'-'.($x+1).'-1';
	        						$sql = "SELECT TOP 1 (SELECT Sum(Kredit) from [dbo].[ParkingTrans] where KodeTransactionType='TOPP' and YEAR(TimeStam) = '$tahun' and MONTH(TimeStam) = '".$bulan[$x]."'),
	        											 (SELECT Sum(Kredit) from [dbo].[ParkingTrans] where KodeTransactionType='MBRS' and YEAR(TimeStam) = '$tahun' and MONTH(TimeStam) = '".$bulan[$x]."'),
	        											 (SELECT Sum(Kredit) from [dbo].[ParkingTrans] where KodeTransactionType='PARK' and YEAR(TimeStam) = '$tahun' and MONTH(TimeStam) = '".$bulan[$x]."')
	        								from [dbo].[ParkingTrans] where YEAR(TimeStam) = '$tahun' and MONTH(TimeStam) = '".$bulan[$x]."'";
	        						$query = sqlsrv_query($conn, $sql);
	        						$data = sqlsrv_fetch_array($query);
	        						$totbul = $total + $data[0] - $data[1] - $data[2];
	        						?>
	        						<tr>
	        							<td style="border: 1px solid #000;">
	        								<a href="?type=perbulan&<?php echo 'time='.date('t-m-Y', strtotime($ds)).'&saldo='.$total; ?>">
	        									<?php echo date('F', strtotime($ds)); ?>
	        								</a>
	        							</td>
	        							<td style="text-align: right; border: 1px solid #000; text-align: right;">
	        								<a href="?type=point&subtype=pembelian&<?php echo 'time='.date('t-m-Y', strtotime($ds)); ?>">
	        									<?php echo number_format($data[0], 2,".",","); ?>
	        								</a>
	        							</td>
	        							<td style="text-align: right; border: 1px solid #000; text-align: right;">
	        								<a href="?type=point&subtype=member&<?php echo 'time='.date('t-m-Y', strtotime($ds)); ?>">
	        									<?php echo number_format($data[1], 2,".",","); ?>
	        								</a>
	        							</td>
	        							<td style="text-align: right; border: 1px solid #000; text-align: right;">
	        								<a href="?type=point&subtype=parkir&<?php echo 'time='.date('t-m-Y', strtotime($ds)); ?>">
	        									<?php echo number_format($data[2], 2,".",","); ?>
	        								</a>
	        							</td>
	        							<td style="text-align: right; border: 1px solid #000; text-align: right;">
	        								<?php echo number_format($totbul, 2,".",","); ?>
	        							</td>
	        						</tr>
	        						<?php
	        						$total = $totbul;
	        					}
	        					
	        				}
	        				?>
	        			</tbody>
	        		</table>
	        	</div>

	        	<script type="text/javascript">
	        		$('#btn-type_default').click(function(){
	        			var tahun = $('#mp_tahun :selected').val();
	        			window.location.href = "mutasipoint.php?subtype=pertahun&tahun="+tahun;
	        		});
	        	</script>
        		<?php
        	}
        	?>
        	
		</div>
	</div>
	

<?php require('footer.php'); ?>