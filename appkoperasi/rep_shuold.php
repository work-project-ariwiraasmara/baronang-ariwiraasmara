<?php require('header.php');?>

<?php require('sidebar-right.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>
    

<div class="animated fadeinup delay-1">
    <div class="page-content">

        <h2 class="uppercase"><?php echo lang('Rekap SHU'); ?></h2>                     
                              
            <form class="form-horizontal" action="" method = "GET">
                <div class="input-field">
                    <?php
                        if(isset($_GET['tahun'])){
                            $to = $_GET['tahun'];
                        }
                        else{
                            $to = date('Y/m/d');
                        }
                        ?>

                        <select id="tahun" name="tahun" class="browser-default">
                            <option type="text" name="bulan" id="bulan" value="<?php echo $_POST['tahun']; ?>">- <?php echo lang('Pilih Tahun'); ?> -</option>
                            <?php
                            $now=date("Y");
                            for ($tahun=2017; $tahun<=$now; $tahun++) { 
                                 if ($_GET['tahun'] == $tahun){
                                    $ck="selected";
                                 }   else {
                                    $ck="" ;
                                 }
                                echo "<option value='$tahun' $ck>$tahun</option>"; 
                            }

                            ?>
                        </select>
                </div>


                <div style="margin-top: 30px;">
                    <button type="submit" class="waves-effect waves-light btn-large primary-color width-100"><?php echo lang('Pilih'); ?></button>
                </div>

                <div style="margin-top: 10px; margin-bottom: 30px;">
                    <a href="rep_shu.php"><button type="button" class="waves-effect waves-light btn-large primary-color width-100">Refresh</button></a>
                </div>
            </form>

         <?php if(isset($_GET['tahun'])){
            //echo $_POST['bulan'];
            $from1 = $_GET['tahun'].'/01'.'/01';
            $from = date('Y-m-01 00:00:00.000', strtotime($from1));
            $to3 = $_GET['tahun'].'/12'.'/01';
            $to = date('Y-m-t 23:59:59.999', strtotime($to3));
            $to1 = date('m', strtotime($to3));
            //echo $to1;
            $to2 = date('Y', strtotime($to3));
            $date = date('Y-m-d');
            //echo $date;
            ?>
            

            <div class="box-header" align="center" style="margin-top: 30px;">
                <tr>
                    <h3 class="" align="center"><?php echo ('Rekap SHU Per Anggota'); ?></h3>
                </tr>
            </div>
            <div class="box-header " align="Center">
                <tr>
                    <h3 class="box-title" align="center"><?php echo $_SESSION['NamaKoperasi']; ?></h3>
                </tr>
                </div>   


            <div class="box-header" align="Center">
                    <tr>
                        <?php
                            $bulan = date('m', strtotime($to3));
                            $tahun = date('Y', strtotime($to3));
                            if ($bulan != 1) {
                                if ($bulan != 2 ) {
                                   if ($bulan != 3) {
                                       if ($bulan != 4) {
                                            if ($bulan !=5) {
                                                if ($bulan !=6) {
                                                    if ($bulan !=7) {
                                                        if ($bulan !=8) {
                                                            if ($bulan !=9) {
                                                                if ($bulan !=10) {
                                                                    if ($bulan !=11) {
                                                                        $bulan = 'Desember';
                                                                    } else {
                                                                        $bulan = 'Nopember';
                                                                    }
                                                                } else {
                                                                    $bulan = 'Oktober';
                                                                }
                                                            } else {
                                                                $bulan = 'September';
                                                            }
                                                        } else {
                                                            $bulan = 'Agustus';
                                                        }
                                                    } else {
                                                        $bulan = 'Juli';
                                                    }
                                                } else {
                                                    $bulan = 'Juni';
                                                }
                                            } else {
                                              $bulan = 'Mei';  
                                            }
                                       } else {
                                            $bulan = 'April';       
                                       }
                                   } else {
                                    $bulan = 'Maret';
                                   }
                                } else {
                                    $bulan = 'Februari';    
                                }                               
                            } else {
                                $bulan = 'Januari';
                            }
                        ?>
    
                        <h3 class="box-title"><?php echo "Periode Tahun : ",$tahun; ?></h3>
                    </tr>



                
                <div style="margin-bottom: 30px;" style="margin-top: 30px;">
                    <a href="drep_pembelian.php?from=<?php echo $from; ?>&to=<?php echo $to; ?>&akun=<?php echo $_GET['akun']; ?>&kop=<?php echo $hasedc[1];?>&vehicle=<?php echo $z[0]; ?>&total=<?php echo $hsjumsum[0]; ?>"><button type="button" class="waves-effect waves-light btn-large primary-color width-100">Download to excel</button></a>
                </div>    
                <div class="row">
                    <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-lg-12 table-responsive">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th colspan="2" style="text-align: center"><?php echo 'No. Anggota'; ?></th>
                                    <th rowspan="2" style="text-align: center"><?php echo 'Nama'; ?></th>
                                    <th colspan="2" style="text-align: center"><?php echo 'Simpanan'; ?></th>
                                    <th rowspan="2" style="text-align: center"><?php echo 'Pokok Pinjaman'; ?></th>
                                    <th rowspan="2" style="text-align: center"><?php echo 'Bunga Pinjaman'; ?></th>
                                    
                                </tr>
                                <tr>
                                    <th style="text-align: center"><?php echo 'New'; ?></th>
                                    <th style="text-align: center"><?php echo 'Old'; ?></th>
                                    <th style="text-align: center;"><?php echo 'Simpanan Pokok'; ?></th>
                                    <th style="text-align: center;"><?php echo 'Simpanan Wajib'; ?></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                            //count
                            $jmlulsql   = "SELECT count(distinct memberid) FROM ( SELECT a.accountkredit, a.date, a.transactiontype, a.amount, b.MemberID, b.KodeBasicSavingtype, b.paymentbalance, c.OldMemberID, c.name, d.status, ROW_NUMBER() OVER (ORDER BY a.date asc) as row from dbo.translist a inner join dbo.basicsavingbalance b on a.accountKredit = b.Accno join dbo.memberlist c on b.MemberID = c.MemberID join dbo.BasicSavingType d on b.Kodebasicsavingtype = d.kodebasicsavingtype where transactiontype = 'BSCP' and d.status in ('0', '1') or transactiontype = 'LNCP') a where a.date between '$from' and '$to'";
                            //echo $jmlulsql;
                            $jmlulstmt  = sqlsrv_query($conn, $jmlulsql);
                            $jmlulrow   = sqlsrv_fetch_array( $jmlulstmt, SQLSRV_FETCH_NUMERIC);

                            //pagging
                            $perpages   = 10;
                            $halaman    = $_GET['page'];
                            if(empty($halaman)){
                                $posisi  = 0;
                                $batas   = $perpages;
                                $halaman = 1;
                            }
                            else{
                                $posisi  = (($perpages * $halaman) - 10) + 1;
                                $batas   = ($perpages * $halaman);
                            }
                           

                                $a = "SELECT distinct memberid, OldMemberID, name FROM ( SELECT a.accountkredit, a.date, a.transactiontype, a.amount, b.MemberID, b.KodeBasicSavingtype, b.paymentbalance, c.OldMemberID, c.name, d.status, ROW_NUMBER() OVER (ORDER BY a.date asc) as row from dbo.translist a inner join dbo.basicsavingbalance b on a.accountKredit = b.Accno join dbo.memberlist c on b.MemberID = c.MemberID join dbo.BasicSavingType d on b.Kodebasicsavingtype = d.kodebasicsavingtype where transactiontype = 'BSCP' and d.status in ('0', '1') or transactiontype = 'LNCP') a where a.date between '$from' and '$to' and row between '$posisi' and '$batas'";
                                //$a = "SELECT * FROM ( SELECT a.accountdebet, a.accountkredit, a.date, a.amount, b.validdate, b.vehicleID, c.name, ROW_NUMBER() OVER (ORDER BY a.date asc) as row from dbo.translist a inner join dbo.memberlocation b on a.accountdebet = b.cardno join dbo.VehicleType c on b.VehicleID = c.vehicleID where date between '$from' and '$to' and a.accountKredit = '$_GET[akun]' and a.transactiontype = 'MBRS' and b.vehicleID = '$hasedc[0]') a WHERE row between '$posisi' and '$batas'";
                                echo $a;
                                $b = sqlsrv_query($conn, $a);
                                while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                                    $sp = "SELECT sum (amount) FROM ( SELECT a.accountkredit, a.date, a.transactiontype, a.amount, b.MemberID, b.KodeBasicSavingtype, b.paymentbalance, c.OldMemberID, c.name, d.status, ROW_NUMBER() OVER (ORDER BY a.date asc) as row from dbo.translist a inner join dbo.basicsavingbalance b on a.accountKredit = b.Accno join dbo.memberlist c on b.MemberID = c.MemberID join dbo.BasicSavingType d on b.Kodebasicsavingtype = d.kodebasicsavingtype where transactiontype = 'BSCP' and d.status = '1' and b.memberID = '$c[0]' ) a where a.date between '$from' and '$to'";
                                    $psp = sqlsrv_query($conn, $sp);
                                    $hsp = sqlsrv_fetch_array($psp, SQLSRV_FETCH_NUMERIC);



                                    $sw = "SELECT sum (amount) FROM ( SELECT a.accountkredit, a.date, a.transactiontype, a.amount, b.MemberID, b.KodeBasicSavingtype, b.paymentbalance, c.OldMemberID, c.name, d.status, ROW_NUMBER() OVER (ORDER BY a.date asc) as row from dbo.translist a inner join dbo.basicsavingbalance b on a.accountKredit = b.Accno join dbo.memberlist c on b.MemberID = c.MemberID join dbo.BasicSavingType d on b.Kodebasicsavingtype = d.kodebasicsavingtype where transactiontype = 'BSCP' and d.status = '0' and b.memberID = '$c[0]' ) a where a.date between '$from' and '$to'";
                                    $psw = sqlsrv_query($conn, $sw);
                                    $hsw = sqlsrv_fetch_array($psw, SQLSRV_FETCH_NUMERIC);


                                    $pokok = "SELECT sum (amount) FROM ( SELECT a.accountkredit, a.date, a.transactiontype, a.amount, b.MemberID, b.KodeBasicSavingtype, b.paymentbalance, c.OldMemberID, c.name, d.status, ROW_NUMBER() OVER (ORDER BY a.date asc) as row from dbo.translist a inner join dbo.basicsavingbalance b on a.accountKredit = b.Accno join dbo.memberlist c on b.MemberID = c.MemberID join dbo.BasicSavingType d on b.Kodebasicsavingtype = d.kodebasicsavingtype where transactiontype = 'LNCP' and b.memberID = '$c[0]' ) a where a.date between '$from' and '$to'";
                                    //echo $pokok;





                                    ?>                 
                            <tr>   
                                <td style="text-align: center;"><?php echo $c[0]; ?></a></td>
                                <td style="text-align: center;"><?php echo $c[1]; ?></a></td>
                                <td><?php echo $c[2]; ?></a></td>
                                <td style="text-align: right;"><?php echo 'Rp. '; ?><?php echo number_format($hsp[0],2) ; ?></a></td>
                                <td style="text-align: right;"><?php echo 'Rp. '; ?><?php echo number_format($hsw[0],2) ; ?></a></td>
                                
                            </tr>
                            
                            <?php $no3++; $jmlhalaman = ceil($jmlulrow[0]/$perpages); } ?>
                            </tbody>
                        </table>
                        <div class="box-footer clearfix right">
                            <div style="text-align: center;">
                                Showing <?=$halaman?> to <?=$jmlhalaman?> of <?=$jmlulrow[0]?> entries <?=$posisi." - ".$batas?>
                                <?php
                                $akun = $_GET['akun'];
                                $bulan = $_GET['bulan'];
                                $tahun = $_GET['tahun'];
                                $reload = "rep_pembelian.php?akun=$akun&bulan=$bulan&tahun=$tahun";
                                $page = intval($_GET["page"]);
                                $adjacents = isset($_GET['adjacents']) ? intval($_GET['adjacents']) : 2; // khusus style pagination 2 dan 3
                                if( $page == 0 ) $page = 1;

                                $tpages = ($jmlhalaman) ? ceil($jmlhalaman) : 0; // total pages, last page number
                                echo "<div>".paginate($reload, $page, $tpages)."</div>";
                                ?>
                            </div>
                        </div>    
                    </div>
                </div>    
                          
                <?php $nm++;} ?>
                                        
            <!-- di dekat tanggal -->
            </div>
            </div>
    </div>
</div>


<?php
function paginate($reload, $page, $tpages) {

    $firstlabel = "First";
    $prevlabel  = "Prev";
    $nextlabel  = "Next";
    $lastlabel  = "Last";

    $out = "<div class=\"backpaging\"><ul class=\"ownpaging\">\n";

    // first
    if($page>1) {
        $out.= "<li><a href=\"" . $reload . "\">" . $firstlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $firstlabel . "</a></li>\n";
    }

    // previous
    if($page==1) {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $prevlabel . "</a></li>\n";
    }
    elseif($page==2) {
        $out.= "<li><a href=\"" . $reload . "\">" . $prevlabel . "</a></li>\n";
    }
    else {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" . ($page-1) . "\">" . $prevlabel . "</a></li>\n";
    }

    // current
    $out.= "<li class=\"active\"><a href=\"#\">Page " . $page . " of " . $tpages . "</a></li>\n";

    // next
    if($page<$tpages) {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" .($page+1) . "\">" . $nextlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $nextlabel . "</a></li>\n";
    }

    // last
    if($page<$tpages) {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" . $tpages . "\">" . $lastlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $lastlabel . "</a></li>\n";
    }

    $out.= "</ul></div>";

    return $out;
}
?>



<?php require('footer.php'); ?>
