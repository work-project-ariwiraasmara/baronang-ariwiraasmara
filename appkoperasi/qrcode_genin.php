<?php
//@session_start();
require('header.php');
require('sidebar-right.php');
require('sidebar-left.php');

	require('qrcode/qrlib.php');

	//barcode
	include('barcode128/BarcodeGenerator.php');
	include('barcode128/BarcodeGeneratorPNG.php');
	include('barcode128/BarcodeGeneratorSVG.php');
	include('barcode128/BarcodeGeneratorJPG.php');
	include('barcode128/BarcodeGeneratorHTML.php');
?>

	<div class="animated fadeinup delay-1">
		<div class="page-content txt-black">

			<h2 class="uppercase txt-black">QR Code Generator Input User</h2>

			<div class="form-inputs m-t-30">
				
				<form action="" method="post" class="m-t-30">
					
					<div class="input-field">
						<b>Tipe Kartu</b> : <br>
						<input type="radio" name="tipekartu" id="tipekartu-a" value="kuning">
						<label for="tipekartu-a">Kuning</label>

						<input type="radio" name="tipekartu" id="tipekartu-b" value="biru">
						<label for="tipekartu-b">Biru</label>
					</div>

					<div class="input-field">
						<label for="nocard">Nomor Kartu</label>
						<input type="text" name="nocard" id="nocard">
						
					</div>

					<div class="m-t-30">
						<button type="submit" name="ok" class="btn btn-large primary-color waves-effect waves-light width-100">Generate</button>
					</div>

				</form>

				<?php
				$ok = @$_POST['ok'];

				function saveQRImage_HASIL($biru_kuning, $templ, $qr, $gud, $glr, $fud, $flr, $fs, $pre_sav) {

					$temp_format = substr($templ, -4);
					if($temp_format == '.jpg') {
						$dest = imagecreatefromjpeg('qrtemplate/'.$templ);
					}
					else if($temp_format == '.jpeg') {
						$dest = imagecreatefromjpeg('qrtemplate/'.$templ);
					}
					else if($temp_format == '.png') {
						$dest = imagecreatefrompng('qrtemplate/'.$templ);
					}

					$cor = imagecolorallocate($dest, 255, 255, 255);
					$font = 'fonts/seguisb.ttf';

					$qr_bformat = substr($qr, -4);
					$qr_fformat = substr($qr, 0, 2);
					if($qr_bformat == '.jpg') {
						if($qr_fformat == 'qr') {
							$fqr = get_string_between($qr, 'qr', '.jpg');
						}
						else {
							$fqr = split ("\.", $qr);
							$fqr = $fqr[0];
						}
						$src = imagecreatefromjpeg('temp/By User Input/qr'.$qr);
					}
					else if($qr_bformat == '.jpeg') {
						if($qr_fformat == 'qr') {
							$fqr = get_string_between($qr, 'qr', '.jpeg');
						}
						else {
							$fqr = split ("\.", $qr);
							$fqr = $fqr[0];
						}
						$src = imagecreatefromjpeg('temp/By User Input/qr'.$qr);
					}
					else if($qr_bformat == '.png') {
						if($qr_fformat == 'qr') {
							$fqr = get_string_between($qr, 'qr', '.png');
						}
						else {
							$fqr = split ("\.", $qr);
							$fqr = $fqr[0];
						}
						$src = imagecreatefrompng('temp/By User Input/qr'.$qr);
					}
					//$fqr = $fqr[0];

					imagettftext($dest, $fs, 0, $flr, $fud, $cor, $font, $fqr);
					imagecopymerge($dest, $src, $glr, $gud, 0, 0, 425, 425, 100);

					//header('Content-Type: image/jpeg');
					//imagejpeg($dest);
					$hqr = 'hasil'.$qr;
					imagepng($dest,'qrpreview/By User Input/'.$biru_kuning.'/'.$hqr);

					if($pre_sav == 'save') {
						?>
						<script type="text/javascript">
							alert('Berhasil Input Data!');
							window.location.href = "qrcode_genin.php";
						</script>
						<?php
					}
					else {
						?>
						<script type="text/javascript">
							alert('Tidak bisa menyimpan data generate kartu input oleh user!');
							window.location.href = "qrcode_genin.php";
						</script>
						<?php
					}
				}

				function saveQRImage($qr) {
					$sqr = substr($qr,0,4);

					$template = 'bag2.jpg';

					$dest = imagecreatefromjpeg('template/'.$template);
					$cor = imagecolorallocate($dest, 255, 255, 255);
					$font = 'fonts/seguisb.ttf';

					imagettftext($dest, 61, 0, 280,1470, $cor, $font, $qr);
					$src = imagecreatefrompng('temp/By User Input/qr'.$qr.'.png');

					imagecopymerge($dest, $src, 180, 430, 0, 0, 100, 100, 100);

					//header('Content-Type: image/jpeg');
					//imagejpeg($dest);
					imagejpeg($dest,'qrhasil/qr'.$qr.'.jpg');
				}

				function QRCode($biru_kuning, $imgback, $kode, $cp, $matrixPointSize) {
					//QR code

					$PNG_TEMP_DIR = dirname(__FILE__).DIRECTORY_SEPARATOR.'temp'.DIRECTORY_SEPARATOR;
					$PNG_WEB_DIR = 'temp/By User Input/';

					$errorCorrectionLevel = 'H';
					//$matrixPointSize = 500;

					//$kode = '1000100010001004';
					$filename = $PNG_WEB_DIR.'qr'.$kode.'.png';
					if (!file_exists($filename)){
						QRcode::png($cp, $filename, $errorCorrectionLevel, $matrixPointSize, 2);
					}

					$generator = new Picqer\Barcode\BarcodeGeneratorPNG();
					$resource = 'data:image/png;base64,'.base64_encode($generator->getBarcode($kode, $generator::TYPE_CODE_128));

					//saveQRImage($kode);
					saveQRImage_HASIL($biru_kuning, $imgback, $kode.'.png',262,130,765,162,31,'save');
					//$templ, $qr, $gud, $glr, $fud, $flr, $fs, $pre_sa
				}

				if(isset($ok)) {
					$tipe = @$_POST['tipekartu'];
					$card = @$_POST['nocard']; 
					$date = date('Y-m-d H:i:s');

					if($tipe == '' || empty($tipe) || is_null($tipe)) { ?>
						<script type="text/javascript">
							alert('Tipe Kartu harus dipilih!');
						</script>
						<?php
					}
					else {
						if($tipe == 'kuning') {
							$src = 'Kuning/';
							$backimg = 'Sky Parking Card_Model 2 Depan.jpg';
						}
						else {
							$src = 'Biru/';
							$backimg = 'Sky Parking Card_Model 1 Depan.jpg';
						}

						$ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
						$iv = openssl_random_pseudo_bytes($ivlen);
						$ciphertext_raw = openssl_encrypt($card, $cipher, '70007000', $options=OPENSSL_RAW_DATA, $iv);
						$hmac = hash_hmac('sha256', $ciphertext_raw, '70007000', $as_binary=true);
						$ciphertext = base64_encode( $iv.$hmac.$ciphertext_raw );

						$sql123 = "INSERT into dbo.MasterCard(CardNo, Barcode, DateCreate, Status) values('$card','$ciphertext','$date','0')";
						//echo $sql123.'<br>';
						$query123 = sqlsrv_query($conn, $sql123);
						QRCode($src, $backimg, $card, $ciphertext, 7);
					}

					
				}
				?>

			</div>

		</div>
	</div>

<?php require('footer.php');?>
