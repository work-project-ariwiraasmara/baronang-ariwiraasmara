<?php require('header.php');?>

<?php require('sidebar-right.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<div class="animated fadeinup delay-1">
    <div class="page-content">
        <h2 class="uppercase"><?php echo lang('Rekap Saldo Anggota'); ?></h2><br><br>

        <div class="box-body">
           
            <form action="" method="get">
                <label>Cari</label>
                <input type="text" name="cari">

                <button type="submit"  value = "cari " class="waves-effect waves-light btn-large primary-color width-100" style="margin-bottom: 30px;">Cari</button>
 
            </form>


            <?php
            if(isset($_GET['cari'])){
            ?>
            <div style="margin-bottom: 30px;" style="margin-top: 30px;">
                <a href="drep_saldo.php?cari=<?php echo $_GET['cari']; ?>"><button type="button" class="waves-effect waves-light btn-large primary-color width-100">Download Data</button></a>
            </div>
            <?php } else { 
            ?>
                <div style="margin-bottom: 30px;" style="margin-top: 30px;">
                <a href="drep_saldoall.php"><button type="button" class="waves-effect waves-light btn-large primary-color width-100">Downlaod Data All</button></a>
            </div>

            <?php } ?>


            <?php 
            if(isset($_GET['cari'])){
             ?>
            <div style="margin-bottom: 30px;" style="margin-top: 30px;">
                <a href="rep_saldo.php"><button type="button" class="waves-effect waves-light btn-large primary-color width-100">Refresh All Data</button></a>
            </div>
            
            <?php 
            $cari = $_GET['cari'];
            echo "<b>Hasil pencarian : ".$cari."</b>";
            }
            ?>

            <div class="box-header" align="center" style="margin-top: 30px;">
                <tr>
                    <h3 class="" align="center"><?php echo ('Rekap Saldo Per Anggota'); ?></h3>
                </tr>
            </div>

            <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-lg-12 table-responsive" id="example1" role="grid" aria-describedby="example1_info">
                <table class="table table-bordered table-striped" id="example1" role="grid" aria-describedby="example1_info" style="margin-top: 30px;">
                    <thead>
                                <tr>
                                    <th colspan="2" style="text-align: center"><?php echo 'No. Anggota'; ?></th>
                                    <th rowspan="2" style="text-align: center"><?php echo 'Nama'; ?></th>
                                    <th colspan="3" style="text-align: center"><?php echo 'Simpanan'; ?></th>
                                    <th colspan="2" style="text-align: center"><?php echo 'Pinjaman'; ?></th>
                                    <th rowspan="2" style="text-align: center"><?php echo 'Tabungan'; ?></th>
                                    <th rowspan="2" style="text-align: center"><?php echo 'Deposito'; ?></th>
                                    
                                </tr>
                                <tr>
                                    <th style="text-align: center"><?php echo 'New'; ?></th>
                                    <th style="text-align: center"><?php echo 'Old'; ?></th>
                                    <th style="text-align: center;"><?php echo 'Simpanan Pokok'; ?></th>
                                    <th style="text-align: center;"><?php echo 'Simpanan Wajib'; ?></th>
                                    <th style="text-align: center;"><?php echo 'Simpanan Sukarela'; ?></th>
                                    <th style="text-align: center;"><?php echo 'Pokok'; ?></th>
                                    <th style="text-align: center;"><?php echo 'Bunga'; ?></th>
                                </tr>
                    </thead>
                    <tbody>
                    <?php
                    //count
                    if(isset($_GET['cari'])){
                    $cari = $_GET['cari'];
                        $jmlulsql = "SELECT count(*) from (select *, ROW_NUMBER () OVER (order by memberid asc) as row from dbo.memberlist  a where statusmember = '1' and Name like '%".$cari."%' or MemberID like '%".$cari."%' or oldmemberID like '%".$cari."%') a";
                    } else {
                        $jmlulsql   = "SELECT count(*) from (select *, ROW_NUMBER () OVER (order by memberid asc) as row from dbo.memberlist  a where statusmember = '1') a";
                    }
                    //echo $jmlulsql;
                    $jmlulstmt  = sqlsrv_query($conn, $jmlulsql);
                    $jmlulrow   = sqlsrv_fetch_array($jmlulstmt, SQLSRV_FETCH_NUMERIC);

                    //pagging
                    $perpages   = 10;
                    $halaman    = $_GET['page'];
                    if(empty($halaman)){
                        $posisi  = 0;
                        $batas   = $perpages;
                        $halaman = 1;
                    }
                    else{
                        $posisi  = (($perpages * $halaman) - 10) + 1;
                        $batas   = ($perpages * $halaman);
                    }

                    if(isset($_GET['cari'])){
                    $cari = $_GET['cari'];
                    $a = "SELECT * from (select *, ROW_NUMBER () OVER (order by memberid asc) as row from dbo.memberlist  a where statusmember = '1' and Name like '%".$cari."%' or MemberID like '%".$cari."%' or oldmemberID like '%".$cari."%') a where row between '$posisi' and '$batas' order by row asc";
                        //echo $ulsql;
                    } else {
                        $a = "SELECT * from (select *, ROW_NUMBER () OVER (order by memberid asc) as row from dbo.memberlist  a where statusmember = '1') a where row between '$posisi' and '$batas'";
                    }
                    //echo $a;
                    $b = sqlsrv_query($conn, $a);
                    while($c = sqlsrv_fetch_array( $b, SQLSRV_FETCH_NUMERIC)){

                                   //Simpanan Pokok 
                                   $sql1   = "SELECT * FROM ( SELECT a.KID,a.MemberID,a.KodeBasicSavingType,a.AccNo,a.BillingBalance,a.PaymentBalance,c.Status, ROW_NUMBER() OVER (ORDER BY a.KID asc) as row FROM [dbo].[BasicSavingBalance] a inner join[dbo].[BasicSavingType] c on a.KodeBasicSavingType = c.KodeBasicSavingType where a.MemberID = '$c[1]' and c.Status = '1') a";
                                    $stmt1  = sqlsrv_query($conn, $sql1);
                                    $row1  = sqlsrv_fetch_array($stmt1, SQLSRV_FETCH_NUMERIC);  

                                    //Simpanan Wajib
                                    $sql2   = "SELECT * FROM ( SELECT a.KID,a.MemberID,a.KodeBasicSavingType,a.AccNo,a.BillingBalance,a.PaymentBalance,c.Status, ROW_NUMBER() OVER (ORDER BY a.KID asc) as row FROM [dbo].[BasicSavingBalance] a inner join[dbo].[BasicSavingType] c on a.KodeBasicSavingType = c.KodeBasicSavingType where a.MemberID = '$c[1]' and c.Status = '0') a";
                                    $stmt2  = sqlsrv_query($conn, $sql2);
                                    $row2  = sqlsrv_fetch_array($stmt2, SQLSRV_FETCH_NUMERIC);
                                    
                                    //Simpanan Sukarela
                                    $sql   = "SELECT * FROM ( SELECT a.KID,a.MemberID,a.KodeBasicSavingType,a.AccNo,a.BillingBalance,a.PaymentBalance,c.Status, ROW_NUMBER() OVER (ORDER BY a.KID asc) as row FROM [dbo].[BasicSavingBalance] a inner join[dbo].[BasicSavingType] c on a.KodeBasicSavingType = c.KodeBasicSavingType where a.MemberID = '$c[1]' and c.Status = '2') a";
                                    //echo $sql;                            
                                    $stmt  = sqlsrv_query($conn, $sql);
                                    $row   = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_NUMERIC); 


                                    //Penjumlahan Tabungan
                                    $tabungan = "SELECT sum(balance) FROM [dbo].[RegularSavingAcc] WHERE KID='$_SESSION[KID]' and MemberID='$c[1]' and status != '0'";
                                    //echo $tabungan;
                                    $ptabungan = sqlsrv_query($conn, $tabungan);
                                    $htabungan = sqlsrv_fetch_array($ptabungan, SQLSRV_FETCH_NUMERIC);


                                    //Penjumlahan Deposito
                                    $Deposito = "SELECT sum(amount) FROM [dbo].[TimeDepositAccount] where MemberID='$c[1]'";
                                    //echo $Deposito;
                                    $pdeposito = sqlsrv_query($conn, $Deposito);
                                    $hdeposito = sqlsrv_fetch_array($pdeposito, SQLSRV_FETCH_NUMERIC);


                                    //Penjumlahan Pinjaman 
                                    $pinj = "SELECT sum (loanammount) row FROM ( SELECT a.KID, a.memberID, a.name, a.Addr, b.userID, c.loanammount, ROW_NUMBER() OVER (ORDER BY a.memberid asc) as row from dbo.memberlist a inner join paymentgateway.dbo.usermemberkoperasi b on a.memberid = b.kodemember join dbo.loanlist c on b.UserID = c.UserID where a.KID = '$_SESSION[KID]' and memberID = '$c[1]') a";
                                    $ppinj = sqlsrv_query($conn, $pinj);
                                    $hpinj = sqlsrv_fetch_array($ppinj, SQLSRV_FETCH_NUMERIC);

                                    //Penjumlahan bunga
                                    $bpinjaman = "SELECT * from (select a.loanappnumber, a.timestamp, a.loanammount, a.invcount, a.interestrate, a.principal_penalty, a.interest_penalty, b.memberid, b.KodeLoantype, c.kodeinteresttype, (select top 1(invcount) from dbo.loantransaction where LoanNumber= a.Loanappnumber order by invcount desc) as jumlahbayar,(select top 1(PrincipalDue) from dbo.loantransaction where LoanNumber= a.Loanappnumber order by invcount desc) as bayarterakhir,(select top 1(interestdue) from dbo.loantransaction where LoanNumber= a.Loanappnumber order by invcount desc) as bungaterakhir, ROW_NUMBER() over (order by loanappnumber asc) as row from dbo.loanlist a inner join dbo.LoanApplicationList b on a.loanappnumber = b.loanappnum join dbo.loantype c on b.kodeloantype = c.kodeloantype where memberid = '$c[1]' ) a";
                                    $pbpinjaman = sqlsrv_query($conn, $bpinjaman);
                                    $coba1 = 0;
                                    $coba2 = 0;
                                    $coba3 = 0;
                                    while ($hbpinjaman = sqlsrv_fetch_array($pbpinjaman, SQLSRV_FETCH_NUMERIC)) {
                                        
                                        if ($hbpinjaman[9] == '01'){
                                            $hasil1 = 0;
                                            if ($hbpinjaman[10] == null) {
                                                $hasil1 = $hbpinjaman[2]*($hbpinjaman[4]/100)/$hbpinjaman[3];
                                            } else {
                                                $hasil1 = (($hbpinjaman[2]*($hbpinjaman[4]/100)/$hbpinjaman[3])+(((($hbpinjaman[12]*$hbpinjaman[10])-($hbpinjaman[12]*$hbpinjaman[10]))*$hbpinjaman[6])+(($hbpinjaman[11]*$hbpinjaman[10])-($hbpinjaman[11]*$hbpinjaman[10]))*$hbpinjaman[5])*($hbpinjaman[4]/100)/$hbpinjaman[3])+($hbpinjaman[12]-$hbpinjaman[12]);
                                            }

                                            $jmhasil1 = ($hasil1*$hbpinjaman[3])-($hbpinjaman[11]*$hbpinjaman[10]);
                                            $coba1 += $jmhasil1;


                                        } else if ($hbpinjaman[9] == '02'){
                                            $hasil2 = 0;
                                            if ($hbpinjaman[10] == null) {
                                                $hasil2 =  $hbpinjaman[2]*($hbpinjaman[4]/100)/$hbpinjaman[3];
                                            } else {
                                                $hasil2 = $hbpinjaman[11]*($hbpinjaman[4]/100)/$hbpinjaman[3];
                                            }

                                            $jmhasil2 = ($hasil2*$hbpinjaman[3])-($hbpinjaman[11]*$hbpinjaman[10]);
                                            //echo $hasil2;
                                            //echo $hbpinjaman[11];
                                            //echo $hbpinjaman[11];                                            
                                            $coba2 += $jmhasil2;
                                            //echo $jmhasil2;
                                        } else if ($hbpinjaman[9] == '03') {
                                            $hasil3 = 0;
                                            if ($hbpinjaman[10] == null) {
                                                $hasil3 =  $hbpinjaman[2]*($hbpinjaman[4]/100)/$hbpinjaman[3];
                                            } else {
                                                $hasil3 = $hbpinjaman[11]*($hbpinjaman[4]/100)/$hbpinjaman[3]+($hbpinjaman[12]-$hbpinjaman[12]);
                                            }

                                            $jmhasil3 = ($hasil3*$hbpinjaman[3])-($hbpinjaman[11]*$hbpinjaman[10]);
                                            $coba3 += $jmhasil3;
                                        }
                                        
                                    }

                                    $hasilsemua = $coba1+$coba2+$coba3;

                                    //echo $hasilsemua;

                                     

                                ?>
                         <tr>   
                            <td style="text-align: center;"><?php echo $c[1]; ?></a></td>
                            <td style="text-align: center;"><?php echo $c[10]; ?></a></td>
                            <td><?php echo $c[2]; ?></a></td>
                            <td style="text-align: right;"><?php echo 'Rp. '; ?><?php echo number_format($row1[5],2) ; ?></a></td>
                            <td style="text-align: right;"><?php echo 'Rp. '; ?><?php echo number_format($row2[5],2) ; ?></a></td>
                            <td style="text-align: right;"><?php echo 'Rp. '; ?><?php echo number_format($row[5],2) ; ?></a></td>
                            <td style="text-align: right;"><?php echo 'Rp. '; ?><?php echo number_format($hpinj[0],2) ; ?></a></td>
                            <td style="text-align: right;"><?php echo 'Rp. '; ?><?php echo number_format($hasilsemua,2) ; ?></a></td>
                            <td style="text-align: right;"><?php echo 'Rp. '; ?><?php echo number_format($htabungan[0],2) ; ?></a></td>
                            <td style="text-align: right;"><?php echo 'Rp. '; ?><?php echo number_format($hdeposito[0],2) ; ?></a></td>
                                
                        </tr>
                    <?php } ?>    
                    <?php
                    $jmlhalaman = ceil($jmlulrow[0]/$perpages);
                    ?>
                    </tbody>
                </table>
            </div>

            <div class="box-footer clearfix right">
                <div style="text-align: center;">
                    Showing <?=$halaman?> to <?=$jmlhalaman?> of <?=$jmlulrow[0]?> entries <?=$posisi." - ".$batas?>
                    <?php
                    if(isset($_GET['cari'])){
                    $cari = $_GET['cari'];
                    $reload = "rep_saldo.php?cari=$cari";
                    } else {
                    $reload = "rep_saldo.php?";
                    }
                    $page = intval($_GET["page"]);
                    $adjacents = isset($_GET['adjacents']) ? intval($_GET['adjacents']) : 2; // khusus style pagination 2 dan 3
                    if( $page == 0 ) $page = 1;
                    $tpages = ($jmlhalaman) ? ceil($jmlhalaman) : 0; // total pages, last page number
                    echo "<div>".paginate($reload, $page, $tpages)."</div>";
                    ?>
                </div>
            </div>
        </div>

    </div>
</div>

<?php
function paginate($reload, $page, $tpages) {

    $firstlabel = "First";
    $prevlabel  = "Prev";
    $nextlabel  = "Next";
    $lastlabel  = "Last";

    $out = "<div class=\"backpaging\"><ul class=\"ownpaging\">\n";

    // first
    if($page>1) {
        $out.= "<li><a href=\"" . $reload . "\">" . $firstlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $firstlabel . "</a></li>\n";
    }

    // previous
    if($page==1) {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $prevlabel . "</a></li>\n";
    }
    elseif($page==2) {
        $out.= "<li><a href=\"" . $reload . "\">" . $prevlabel . "</a></li>\n";
    }
    else {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" . ($page-1) . "\">" . $prevlabel . "</a></li>\n";
    }

    // current
    $out.= "<li class=\"active\"><a href=\"#\">Page " . $page . " of " . $tpages . "</a></li>\n";

    // next
    if($page<$tpages) {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" .($page+1) . "\">" . $nextlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $nextlabel . "</a></li>\n";
    }

    // last
    if($page<$tpages) {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" . $tpages . "\">" . $lastlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $lastlabel . "</a></li>\n";
    }

    $out.= "</ul></div>";

    return $out;
}
?>



<?php require('footer_new.php');?>
