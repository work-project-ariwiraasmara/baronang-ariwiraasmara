<?php
require_once 'lib/googleLib/GoogleAuthenticator.php';
include "connect.php";
include "connectuser.php";

if(isset($_POST['tid'])){
    $id = $_POST['tid'];
    $type = $_POST['type'];

    $qrCodeUrl = '';
    $x = "select* from [dbo].[UserTokenTransaksi] where BarcodeTransaksi = '$id'";
    $y = sqlsrv_query($connuser, $x);
    $z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC);
    if($z != null){
        $a = "select* from [dbo].[UserPaymentGateway] where KodeUser = '$z[2]'";
        $b = sqlsrv_query($connuser, $a);
        $c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC);

        //$ga = new GoogleAuthenticator();
        //$qrCodeUrl = $ga->getQRCodeGoogleUrl($c[2], $c[11], 'Baronang : '.$_SESSION['NamaKoperasi']);
    }
    else{
        messageAlert('Barcode not found','warning');
        header('Location: identity_cas.php');
    }
}
else{
    messageAlert('Failed request data','warning');
    header('Location: identity_cas.php');
}
?>

<?php if($z != null){ ?>
    <form action="proc_auth.php" method="post">
        <div class="form-group">
            Mengirim persetujuan ke user ?
            <input type="hidden" name="tid" value="<?php echo $id; ?>" readonly>
            <input type="hidden" name="type" value="<?php echo $type; ?>" readonly>
            <input type="hidden" name="uid" value="<?php echo $z[2]; ?>" readonly>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <button type="submit" class="btn btn-flat btn-block btn-success">Ya</button>
            </div>
            <div class="col-sm-6">
                <button type="button" class="btn btn-flat btn-block btn-default" data-dismiss="modal">Tidak</button>
            </div>
        </div>
    </form>
<?php } ?>