<?php
if(isset($_GET['download'])) {
	require('connect.php');
    require('lib/phpexcel/Classes/PHPExcel.php');

    $filePath = "uploads/excel/";

    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getProperties()->setCreator("Baronang");
    $objPHPExcel->getProperties()->setTitle("Laporan TOPP");

    // set autowidth
    for($col = 'A'; $col !== 'Z'; $col++) {
        $objPHPExcel->getActiveSheet()
            ->getColumnDimension($col)
            ->setAutoSize(true);
    }

    //sheet 2
    $objWorkSheet = $objPHPExcel->createSheet(0);

    $sps = "SELECT ValueConversion from [dbo].[ParkingSetting]";
	$qps = sqlsrv_query($conn, $sps);
	$dps = sqlsrv_fetch_array($qps);

	if( isset($_GET['ch']) ) {

		if( @$_GET['ch'] == 1 ) { 
			$where1 = "TransactionType='TOPP'"; 
			$where2 = "TransactionType='FEEVA'";
			$where3 = "Status='0'";
		}
		else if( @$_GET['ch'] == 2 ) { 
			$where1 = "UserID='B0080'";
			$where2 = "TransactionType='FEEVA'";
			$where3 = "TransactionType='TOPVA'";

		}
		else {
			$where1 = "UserID='B0008'";
			$where2 = "TransactionType='FEEVA'";
			$where3 = "TransactionType='TOPVA'";
		}

		if( @$_GET['ch'] == 1 ) { $title = 'EDC'; }
		else if( $_GET['ch'] == 2 ) { $title = 'VA Sinar Mas'; }
		else $title = 'VA BCA';

		if(isset($_GET['bulan'])) { $bulan = @$_GET['bulan']; } else { $bulan = date('m'); }
		if(isset($_GET['tahun'])) { $tahun = @$_GET['tahun']; } else { $tahun = date('Y'); }

		$ts1 = $tahun.'-'.$bulan.'-1';
		$ts2 = $tahun.'-'.$bulan.'-'.@$_GET['t'];

		if( isset($_GET['det']) ) {

			// baris judul
		    $objWorkSheet->getStyle('A1')->getFont()->setSize(20);
		    $objWorkSheet->getStyle('A1')->getFont()->setBold(true);
		    $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:H1');
		    $objWorkSheet->SetCellValue('A1', 'Detail Laporan TOPP '.$title);
		    $objWorkSheet->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		    // baris judul
		    $objWorkSheet->getStyle('A2')->getFont()->setSize(20);
		    $objWorkSheet->getStyle('A2')->getFont()->setBold(true);
		    $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:H2');
		    $objWorkSheet->SetCellValue('A2', date('F Y', strtotime($tahun.'-'.$bulan.'-1') ) );
		    $objWorkSheet->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		    //
		    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
        	$objWorkSheet->getStyle('A5')->getFont()->setBold(true);
        	$objWorkSheet->SetCellValue('A5', 'No.');
        	$objWorkSheet->getStyle('A5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
        	$objWorkSheet->getStyle('B5')->getFont()->setBold(true);
        	$objWorkSheet->SetCellValue('B5', 'Tanggal');
        	$objWorkSheet->getStyle('B5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
        	//$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        	$objWorkSheet->getStyle('C5')->getFont()->setBold(true);
        	$objWorkSheet->SetCellValue('C5', 'CardNo');
        	$objWorkSheet->getStyle('C5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        	


        	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(50);
        	$objWorkSheet->getStyle('D5')->getFont()->setBold(true);
        	$objWorkSheet->SetCellValue('D5', 'Nama');
        	$objWorkSheet->getStyle('D5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        	$objWorkSheet->getStyle('E5')->getFont()->setBold(true);
        	$objWorkSheet->SetCellValue('E5', 'Top Up Point');
        	$objWorkSheet->getStyle('E5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
        	$objWorkSheet->getStyle('F5')->getFont()->setBold(true);
        	$objWorkSheet->SetCellValue('F5', 'Fee');
        	$objWorkSheet->getStyle('F5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        	$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
        	$objWorkSheet->getStyle('G5')->getFont()->setBold(true);
        	$objWorkSheet->SetCellValue('G5', 'Total Point');
        	$objWorkSheet->getStyle('G5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        	$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(25);
        	$objWorkSheet->getStyle('H5')->getFont()->setBold(true);
        	$objWorkSheet->SetCellValue('H5', 'Total (Rp)');
        	$objWorkSheet->getStyle('H5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		    $sch = "SELECT 	a.date, a.AccountKredit, c.name, a.Amount,
							(SELECT Sum(Amount) from [dbo].[TransList] where $where2 and TransNo = a.TransNo)
					from [dbo].[TransList] as a 
					inner join [dbo].[MemberCard] as b 
						on a.AccountKredit = b.CardNo 
					inner join [dbo].[MemberList] as c 
						on b.MemberID = c.MemberID
					where a.".$where1." and a.date between '$ts2 00:00:00.000' and '$ts2 23:59:59.999' ORDER BY date ASC";
			//echo $sch;
			$qch = sqlsrv_query($conn, $sch);
			$fee = 0; $no = 1; $row = 6;
			while( $dch = sqlsrv_fetch_array($qch) ) {
				if (is_null($dch[4])) { $fee = 0; }
				else { $fee = $dch[4]; }
				$ch_tot = $dch[3] - $fee;

				$objWorkSheet->SetCellValue('A'.$row, $no);

				$objWorkSheet->SetCellValue('B'.$row, $dch[0]->format('Y-m-d'));
				$objWorkSheet->getStyle('B'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

				$objPHPExcel->getActiveSheet()
						    ->getStyle('C'.$row)
						    ->getNumberFormat()
						    ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
				$objWorkSheet->SetCellValue('C'.$row, ' '.$dch[1].' ');

				$objWorkSheet->SetCellValue('D'.$row, $dch[2]);

				$objWorkSheet->SetCellValue('E'.$row, number_format($dch[3] / $dps[0], 0, ",", "."));
				$objWorkSheet->getStyle('E'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

				$objWorkSheet->SetCellValue('F'.$row, number_format($fee / $dps[0], 0, ",", "."));
				$objWorkSheet->getStyle('F'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

				$objWorkSheet->SetCellValue('G'.$row, number_format($ch_tot / $dps[0], 0, ",", "."));
				$objWorkSheet->getStyle('G'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

				$objWorkSheet->SetCellValue('H'.$row, number_format($dch[3], 2, ",", "."));
				$objWorkSheet->getStyle('H'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

				$no++; $row++;
			}

			//exit;
	        $objWorkSheet->setTitle('Laporan TOPP Detail '.$title);
	        $fileName = 'Laporan TOPP '.$title.' '.date('F Y', strtotime($tahun.'-'.$bulan.'-'.@$_GET['t'])).'.xls';

		}
		else {

			// baris judul
		    $objWorkSheet->getStyle('A1')->getFont()->setSize(20);
		    $objWorkSheet->getStyle('A1')->getFont()->setBold(true);
		    $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:F1');
		    $objWorkSheet->SetCellValue('A1', 'Laporan TOPP '.$title);
		    $objWorkSheet->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		    // baris judul
		    $objWorkSheet->getStyle('A2')->getFont()->setSize(20);
		    $objWorkSheet->getStyle('A2')->getFont()->setBold(true);
		    $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:F2');
		    $objWorkSheet->SetCellValue('A2', date('F Y', strtotime($tahun.'-'.$bulan.'-'.@$_GET['t']) ) );
		    $objWorkSheet->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);


		    //
		    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
        	$objWorkSheet->getStyle('A5')->getFont()->setBold(true);
        	$objWorkSheet->SetCellValue('A5', 'No.');
        	$objWorkSheet->getStyle('A5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
        	$objWorkSheet->getStyle('B5')->getFont()->setBold(true);
        	$objWorkSheet->SetCellValue('B5', 'Tanggal');
        	$objWorkSheet->getStyle('B5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        	$objWorkSheet->getStyle('C5')->getFont()->setBold(true);
        	$objWorkSheet->SetCellValue('C5', 'Top Up Point');
        	$objWorkSheet->getStyle('C5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        	$objWorkSheet->getStyle('D5')->getFont()->setBold(true);
        	$objWorkSheet->SetCellValue('D5', 'Fee');
        	$objWorkSheet->getStyle('D5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
        	$objWorkSheet->getStyle('E5')->getFont()->setBold(true);
        	$objWorkSheet->SetCellValue('E5', 'Total Point');
        	$objWorkSheet->getStyle('E5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
        	$objWorkSheet->getStyle('F5')->getFont()->setBold(true);
        	$objWorkSheet->SetCellValue('F5', 'Total (Rp)');
        	$objWorkSheet->getStyle('F5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        	$lastdate = date('t', strtotime($tahun.'-'.$bulan.'-1'));

        	$row = 6;
		    for($x = 1; $x <= $lastdate; $x++) {
		    	$ts1 = $tahun.'-'.$bulan.'-'.$x.' 00:00:00.000';
				$ts2 = $tahun.'-'.$bulan.'-'.$x.' 23:59:59.999';
				$sch = "SELECT DISTINCT (SELECT Sum(Amount) from [dbo].[TransList] where $where1 and Date between '$ts1' and '$ts2' and $where3),
										(SELECT Sum(Amount) from [dbo].[TransList] where $where2 and Date between '$ts1' and '$ts2' and $where1)
						from [dbo].[TransList] where Date between '$ts1' and '$ts2'";
				$qch = sqlsrv_query($conn, $sch);
				$dch = sqlsrv_fetch_array($qch);
				if (is_null($dch[1])) {
					$fee = 0;
				}
				else {
					$fee = $dch[1];
				}
				$ch_tot = $dch[0] - $fee;

				if( $dch[0] != '' ) {
					$objWorkSheet->SetCellValue('A'.$row, $x);
					$objWorkSheet->SetCellValue('B'.$row, @$_GET['tahun'].'-'.@$_GET['bulan'].'-'.$x);
					
					$objWorkSheet->SetCellValue('C'.$row, number_format($dch[0] / $dps[0], 0, ",", "."));
					$objWorkSheet->getStyle('C'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

					$objWorkSheet->SetCellValue('D'.$row, number_format($fee / $dps[0], 0, ",", "."));
					$objWorkSheet->getStyle('D'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

					$objWorkSheet->SetCellValue('E'.$row, number_format($ch_tot / $dps[0], 0, ",", "."));
					$objWorkSheet->getStyle('E'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

					$objWorkSheet->SetCellValue('F'.$row, number_format($dch[0], 2, ",", "."));
					$objWorkSheet->getStyle('F'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				}

				$row++;
		    }

		    //exit;
	        $objWorkSheet->setTitle('Laporan TOPP '.$title);
	        $fileName = 'Laporan TOPP '.$title.' '.date('F Y', strtotime($tahun.'-'.$bulan.'-1')).'.xls';

		}

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');

		// download ke client
	    header('Content-type: application/vnd.ms-excel');
	    header('Content-Disposition: attachment; filename="'.$fileName.'"');
	    $objWriter->save('php://output');
	}
}
else {
	require('header.php');
    require('sidebar-right.php');
    require('sidebar-left.php');

    if(isset($_GET['bulan'])) { $bulan = @$_GET['bulan']; } else { $bulan = date('m'); }
	if(isset($_GET['tahun'])) { $tahun = @$_GET['tahun']; } else { $tahun = date('Y'); }
?>

	<div class="animated fadeinup delay-1">
		<div class="page-content txt-black">

			<h2 class="center uppercase txt-black">
				<?php
					if(isset($_GET['ch'])) {

						if(isset($_GET['det'])) {
							if( @$_GET['ch'] == 1 ) { $title = 'EDC'; }
							else if( $_GET['ch'] == 2 ) { $title = 'VA Sinar Mas'; }
							else $title = 'VA BCA';

							echo date('F Y', strtotime($tahun.'-'.$bulan.'-1')).'<br>'.$title;

							$link_download = '?download=1&det=1&ch='.@$_GET['ch'].'&bulan='.$bulan.'&tahun='.$tahun.'&t='.@$_GET['t'];
						}
						else {
							if( @$_GET['ch'] == 1 ) { $title = 'EDC'; }
							else if( $_GET['ch'] == 2 ) { $title = 'VA Sinar Mas'; }
							else $title = 'VA BCA';

							echo date('F Y', strtotime($tahun.'-'.$bulan.'-1')).'<br>'.$title;

							$link_download = '?download=1&ch='.@$_GET['ch'].'&bulan='.$bulan.'&tahun='.$tahun.'&t='.date('t', strtotime($tahun.'-'.$bulan.'-1'));
						}

					}
					else {
						echo date('F Y', strtotime($tahun.'-'.$bulan.'-1') );
					}	
				?>
			</h2><br>

			<?php
			if( isset($_GET['ch']) ) { 
				if( isset($_GET['det']) ) { 
					$link_back = '?ch=1&bulan='.@$_GET['bulan'].'&tahun='.@$_GET['tahun'];
				}
				else {
					$link_back = '?bulan='.@$_GET['bulan'].'&tahun='.@$_GET['tahun'];
				}
			}
			else { ?>
				<div class="form-inputs">
					<div class="row">
						<div class="col s4">
							<span>Bulan</span> : <br>
							<select class="browser-default" id="pilbul">
								<?php
								for($x = 1; $x < 13; $x++) { ?>
									<option value="<?php echo date('m', strtotime('2014-'.$x.'-1') ); ?>"><?php echo date('F', strtotime('2015-'.$x.'-1') ); ?></option>
									<?php
								}
								?>
							</select>
						</div>

						<div class="col s4">
							<span>Tahun</span> : <br>
							<select class="browser-default" id="piltah">
								<?php
								for($x = date('Y'); $x > 2015; $x--) { ?>
									<option value="<?php echo $x; ?>"><?php echo $x; ?></option>
									<?php
								}
								?>
							</select>
						</div>

						<div class="col s4">
							<button class="btn btn-large width-100 primary-color waves-light waves-effect m-t-10" id="btnpilok">OK</button>
						</div>
					</div>
				</div>

				<script type="text/javascript">
					$('#btnpilok').click(function(){
						var bulan = $('#pilbul :selected').val();
						var tahun = $('#piltah :selected').val();
						window.location.href = "?bulan=" + bulan + "&tahun=" + tahun
					});
				</script>
				<?php
			}
			?>
			<div class="table-responsive m-t-30">
				<a href="<?php echo '?ch='.@$_GET['ch'].'&bulan='.@$_GET['bulan'].'&tahun='.@$_GET['tahun']; ?>">
					  	<button class="btn primary-color waves-efeect waves-light">
					      	<i class="ion-android-arrow-back"></i>
					  	</button>
				</a>

				<a href="<?php echo $link_download; ?>">
					<button class="btn primary-color waves-efeect waves-light">
					    <i class="ion-android-download"></i>
					</button>
				</a>
				<table class="table m-t-10">
					<thead>
						<tr>
							<?php
							if(isset($_GET['ch'])) { ?>
								<th>Tanggal</th>
								<?php
								if(isset($_GET['det'])) { ?>
									<th>Card No.</th>
									<th>Nama</th>
									<?php
								}
							}
							else { ?>
								<th>Channel</th>
								<?php
							}
							?>
							<th style="text-align: center;">Top Up Point</th>
							<th style="text-align: center;">Fee</th>
							<th style="text-align: center;">Total Point</th>
							<th style="text-align: center;">Total (Rp)</th>
						</tr>
					</thead>

					<tbody>
						<?php
						$sps = "SELECT ValueConversion from [dbo].[ParkingSetting]";
						$qps = sqlsrv_query($conn, $sps);
						$dps = sqlsrv_fetch_array($qps);

						if(isset($_GET['ch'])) {

							if( @$_GET['ch'] == 1 ) { 
								$where1 = "TransactionType='TOPP'"; 
								$where2 = "TransactionType='FEEVA'";
								$where3 = "Status='0'";
							}
							else if( $_GET['ch'] == 2 ) { 
								$where1 = "UserID='B0080'";
								$where2 = "TransactionType='FEEVA'";
								$where3 = "TransactionType='TOPVA'";

							}
							else {
								$where1 = "UserID='B0008'";
								$where2 = "TransactionType='FEEVA'";
								$where3 = "TransactionType='TOPVA'";
							}

							$lastdate = date('t', strtotime($tahun.'-'.$bulan.'-1'));
							
							if(isset($_GET['det'])) {
								/*
								for($x = 1; $x <= $lastdate; $x++) {
									$ts1 = $tahun.'-'.$bulan.'-'.$x.' 00:00:00';
									$ts2 = $tahun.'-'.$bulan.'-'.$x.' 23:59:59';
									$sch = "SELECT TOP 1 	a.AccNo,
															c.Name,
															(SELECT Sum(Kredit) from [dbo].[ParkingTrans] where $where1 and TimeStam between '$ts1' and '$ts2' and Kredit != 0),
															(SELECT Sum(Kredit) from [dbo].[ParkingTrans] where $where2 and TimeStam between '$ts1' and '$ts2' and Kredit != 0)
											from [dbo].[ParkingTrans] as a 
											inner join [dbo].[MemberCard] as b 
												on a.AccNo = b.CardNo 
											inner join [dbo].[MemberList] as c 
												on b.MemberID = c.MemberID
											where a.".$where." and a.TimeStam between '$ts1' and '$ts2' and a.AccNo IS NOT NULL";
									//echo $sch.'<br><br>';
									$qch = sqlsrv_query($conn, $sch);

									$dch = sqlsrv_fetch_array($qch);
									$ch_tot = $dch[2] - $dch[3];
									?>
									<tr>
										<td><?php echo $x; ?></td>
										<td><?php echo $dch[0]; ?></td>
										<td><?php echo $dch[1]; ?></td>
										<td style="text-align: right;"><?php echo number_format($dch[2], 2, ",", "."); ?></td>
										<td style="text-align: right;"><?php echo number_format($dch[3], 2, ",", "."); ?></td>
										<td style="text-align: right;"><?php echo number_format($ch_tot, 2, ",", "."); ?></td>
										<td style="text-align: right;"><?php echo number_format($ch_tot * $dps[0], 2, ",", "."); ?></td>
									</tr>
									<?php
								}
								*/
								$ts1 = @$_GET['tahun'].'-'.@$_GET['bulan'].'-'.@$_GET['t'].' 00:00:00.000';
								$ts2 = @$_GET['tahun'].'-'.@$_GET['bulan'].'-'.@$_GET['t'].' 23:59:59.999';

								$sch = "SELECT 	a.date, a.AccountKredit, c.name, a.Amount,
												(SELECT Sum(Amount) from [dbo].[TransList] where $where2 and TransNo = a.TransNo)
										from [dbo].[TransList] as a 
										inner join [dbo].[MemberCard] as b 
											on a.AccountKredit = b.CardNo 
										inner join [dbo].[MemberList] as c 
											on b.MemberID = c.MemberID
										where a.".$where1." and a.date between '$ts1' and '$ts2' ORDER BY date ASC";
								//echo $sch;
								$qch = sqlsrv_query($conn, $sch);
								$fee = 0;
								while( $dch = sqlsrv_fetch_array($qch) ) { 
									if (is_null($dch[4])) {
										$fee = 0;
									}
									else {
										$fee = $dch[4];
									}
									$ch_tot = $dch[3] - $fee;
									 
									?>
									<tr>
										<td><?php echo $dch[0]->format('Y-m-d'); ?></td>
										<td><?php echo $dch[1]; ?></td>
										<td><?php echo $dch[2]; ?></td>
										<td style="text-align: right;"><?php echo number_format($dch[3] / $dps[0], 0, ",", "."); ?></td>
										<td style="text-align: right;"><?php echo number_format($fee / $dps[0], 0, ",", "."); ?></td>
										<td style="text-align: right;"><?php echo number_format($ch_tot / $dps[0], 0, ",", "."); ?></td>
										<td style="text-align: right;"><?php echo number_format($dch[3], 2, ",", "."); ?></td>
									</tr>
									<?php
								}
							}
							else {
								for($x = 1; $x <= $lastdate; $x++) {
									$ts1 = $tahun.'-'.$bulan.'-'.$x.' 00:00:00.000';
									$ts2 = $tahun.'-'.$bulan.'-'.$x.' 23:59:59.999';
									$sch = "SELECT DISTINCT (SELECT Sum(Amount) from [dbo].[TransList] where $where1 and Date between '$ts1' and '$ts2' and $where3),
															(SELECT Sum(Amount) from [dbo].[TransList] where $where2 and Date between '$ts1' and '$ts2' and $where1)
											from [dbo].[TransList] where Date between '$ts1' and '$ts2'";
									$qch = sqlsrv_query($conn, $sch);
									$dch = sqlsrv_fetch_array($qch);
									if (is_null($dch[1])) {
										$fee = 0;
									}
									else {
										$fee = $dch[1];
									}
									$ch_tot = $dch[0] - $fee;

									if( $dch[0] != '' ) { ?>
										<tr>
											<td><a href="<?php echo '?ch='.@$_GET['ch'].'&det=1&tahun='.@$_GET['tahun'].'&bulan='.@$_GET['bulan'].'&t='.$x; ?>"><?php echo @$_GET['tahun'].'-'.@$_GET['bulan'].'-'.$x; ?></a></td>
											<td style="text-align: right;"><?php echo number_format($dch[0] / $dps[0], 0, ",", "."); ?></td>
											<td style="text-align: right;"><?php echo number_format($fee / $dps[0], 0, ",", "."); ?></td>
											<td style="text-align: right;"><?php echo number_format($ch_tot / $dps[0], 0, ",", "."); ?></td>
											<td style="text-align: right;"><?php echo number_format($dch[0], 2, ",", "."); ?></td>
										</tr>
										<?php
									}
									
								}
							}

						}
						else {
							// TOPP
							$sch1 = "SELECT DISTINCT	(SELECT SUM(Kredit) from [dbo].[ParkingTrans] where KodeTransactionType='TOPP' and YEAR(TimeStam) = '$tahun' and MONTH(TimeStam) = '$bulan' and Kredit != 0),
														(SELECT SUM(Kredit) from [dbo].[ParkingTrans] where UserID='B0080' and YEAR(TimeStam) = '$tahun' and MONTH(TimeStam) = '$bulan' and Kredit != 0),
														(SELECT SUM(Kredit) from [dbo].[ParkingTrans] where UserID='B0008' and YEAR(TimeStam) = '$tahun' and MONTH(TimeStam) = '$bulan' and Kredit != 0),
														(SELECT SUM(Kredit) from [dbo].[ParkingTrans] where KodeTransactionType='FEEVA'and KodeTransactionType='TOPP' and YEAR(TimeStam) = '$tahun' and MONTH(TimeStam) = '$bulan' and Kredit != 0),
														(SELECT SUM(Kredit) from [dbo].[ParkingTrans] where KodeTransactionType='FEEVA' and UserID='B0080' and YEAR(TimeStam) = '$tahun' and MONTH(TimeStam) = '$bulan' and Kredit != 0),
														(SELECT SUM(Kredit) from [dbo].[ParkingTrans] where KodeTransactionType='FEEVA' and UserID='B0008' and YEAR(TimeStam) = '$tahun' and MONTH(TimeStam) = '$bulan' and Kredit != 0)
										from [dbo].[ParkingTrans]";
							$qch1 = sqlsrv_query($conn, $sch1);
							$dch1 = sqlsrv_fetch_array($qch1);
							$ch1_tot = $dch1[0] - $dch1[3];
							$ch2_tot = $dch1[1] - $dch1[4];
							$ch3_tot = $dch1[2] - $dch1[5];
							?>
							<tr>
								<td><a href="?ch=1<?php echo '&bulan='.$bulan.'&tahun='.$tahun; ?>">EDC</a></td>
								<td style="text-align: right;"><?php echo number_format($dch1[0], 0, ",", "."); ?></td>
								<td style="text-align: right;"><?php echo number_format($dch1[3], 0, ",", "."); ?></td>
								<td style="text-align: right;"><?php echo number_format($ch1_tot, 0, ",", "."); ?></td>
								<td style="text-align: right;"><?php echo number_format($dch1[0] * $dps[0], 2, ",", "."); ?></td>
							</tr>

							<tr>
								<td><a href="?ch=2<?php echo '&bulan='.$bulan.'&tahun='.$tahun; ?>">Sinar Mas</a></td>
								<td style="text-align: right;"><?php echo number_format($dch1[1], 0, ",", "."); ?></td>
								<td style="text-align: right;"><?php echo number_format($dch1[4], 0, ",", "."); ?></td>
								<td style="text-align: right;"><?php echo number_format($ch2_tot, 0, ",", "."); ?></td>
								<td style="text-align: right;"><?php echo number_format($dch1[1] * $dps[0], 2, ",", "."); ?></td>
							</tr>

							<tr>
								<td><a href="?ch=3<?php echo '&bulan='.$bulan.'&tahun='.$tahun; ?>">VA BCA</a></td>
								<td style="text-align: right;"><?php echo number_format($dch1[2], 0, ",", "."); ?></td>
								<td style="text-align: right;"><?php echo number_format($dch1[5], 0, ",", "."); ?></td>
								<td style="text-align: right;"><?php echo number_format($ch3_tot, 0, ",", "."); ?></td>
								<td style="text-align: right;"><?php echo number_format($dch1[2] * $dps[0], 2, ",", "."); ?></td>
							</tr>
							<?php
						}
						?>
					</tbody>
				</table>
			</div>

		</div>
	</div>

<?php
	require('footer.php'); 
}
?>
