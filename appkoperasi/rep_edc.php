<?php require('header.php');?>

<?php require('sidebar-right.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>


<div class="animated fadeinup delay-1">
    <div class="page-content">

        <h2 class="uppercase"><?php echo lang('Laporan List EDC'); ?></h2>

            <form class="form-horizontal" action="" method = "GET">
                <!-- <div class="input-field">
                    <input type="text" name="tgl1" id="tgl1" class="datepicker" value="<?php echo "$_GET[tgl1]"; ?>" autocomplete="off"></p></td>
                    <label><?php echo ('Tanggal Awal'); ?></label>
                </div>
                
                <div class="input-field">
                    <input type="text" name="tgl2" id="tgl2" class="datepicker" value="<?php echo "$_GET[tgl2]"; ?>" autocomplete="off"></p></td>
                    <label><?php echo ('Tanggal Akhir'); ?></label>
                </div> -->

                <div class="input-field">
                    <?php
                        if(isset($_GET['bulan'])){
                            $from = $_GET['bulan'];
                        }
                        else{
                            $from = date('Y/m/d');
                        }
                        ?>
                        <select id="bulan" name="bulan" class="browser-default">
                            <option type="text" name="bulan" id="bulan" value="<?php echo $_GET['bulan']; ?>">- <?php echo lang('Pilih Bulan'); ?> -</option>
                            <?php
                            $bln=array(01=>"Januari","Februari","Maret","April","Mei","Juni","July","Agustus","September","Oktober","November","Desember");
                            echo $bln;
                            $from='';
                            for ($bulan=01; $bulan<=12; $bulan++) { 
                                 if ($_GET['bulan'] == $bulan){
                                    $ck="selected";
                                 }   else {
                                    $ck="" ;
                                 }
                                echo "<option value='$bulan' $ck>$bln[$bulan]</option>";
                            }
                            ?>
                        </select>
                </div>
                <div class="input-field">
                    <?php
                        if(isset($_GET['tahun'])){
                            $to = $_GET['tahun'];
                        }
                        else{
                            $to = date('Y/m/d');
                        }
                        ?>
                        <select id="tahun" name="tahun" class="browser-default">
                            <option type="text" name="tahun" id="tahun" value="<?php echo $_GET['tahun']; ?>">- <?php echo lang('Pilih Tahun'); ?> -</option>
                            <?php
                            $now=date("Y");
                            for ($tahun=2017; $tahun<=$now; $tahun++) { 
                                 if ($_GET['tahun'] == $tahun){
                                    $ck="selected";
                                 }   else {
                                    $ck="" ;
                                 }
                                echo "<option value='$tahun' $ck>$tahun</option>"; 
                            }

                            ?>
                        </select>
                </div>

                <div style="margin-top: 30px;">
                    <button type="submit" class="waves-effect waves-light btn-large primary-color width-100"><?php echo lang('Pilih'); ?></button>
                </div>

                <div style="margin-top: 10px; margin-bottom: 30px;">
                    <a href="rep_edc.php"><button type="button" class="waves-effect waves-light btn-large primary-color width-100">Refresh</button></a>
                </div>
            </form>

        <?php if($_GET['bulan'] and $_GET['tahun']){
            $from1 = $_GET['tahun'].'/'.$_GET['bulan'].'/01';
            $from = date('Y-m-01', strtotime($from1));
            $to3 = $_GET['tahun'].'/'.$_GET['bulan'].'/01';
            $to = date('Y-m-t', strtotime($to3)); 
            $hanyatanggal = date('d', strtotime($from));
            $hanyatanggal1 = date('d', strtotime($to));
            $kid = $_SESSION[KID];
           
            ?>
           
           <div>
                <a href="drep_edc.php?from=<?php echo $tanggaldari; ?>&to=<?php echo $tanggalsampai; ?>"><button type="button" class="waves-effect waves-light btn-large primary-color width-100">Download to excel</button></a>
            </div>

            <div class="box-header" align="center" style="margin-top: 30px;">
                <tr>
                    <h3 class="" align="center"><?php echo ('Laporan List EDC'); ?></h3>
                </tr>
            </div>
            
               
            <div class="box-header" align="Center">
                <tr>
                    <?php
                    $bulan = date('m', strtotime($from));
                    $tahun = date('Y', strtotime($from));
                    $bulan2 = date('m', strtotime($to));
                    $tahun2 = date('Y', strtotime($to));
                        if ($bulan != 1) {
                            if ($bulan != 2 ) {
                               if ($bulan != 3) {
                                   if ($bulan != 4) {
                                        if ($bulan !=5) {
                                            if ($bulan !=6) {
                                                if ($bulan !=7) {
                                                    if ($bulan !=8) {
                                                        if ($bulan !=9) {
                                                            if ($bulan !=10) {
                                                                if ($bulan !=11) {
                                                                        $bulan = 'Desember';
                                                                } else {
                                                                    $bulan = 'Nopember';
                                                                }
                                                            } else {
                                                                $bulan = 'Oktober';
                                                            }
                                                        } else {
                                                            $bulan = 'September';
                                                        }
                                                    } else {
                                                        $bulan = 'Agustus';
                                                    }
                                                } else {
                                                    $bulan = 'Juli';
                                                }
                                            } else {
                                            $bulan = 'Juni';
                                            }
                                        } else {
                                          $bulan = 'Mei';  
                                        }
                                   } else {
                                        $bulan = 'April';       
                                   }
                                } else {
                                    $bulan = 'Maret';
                                }
                            } else {
                                $bulan = 'Februari';    
                            }                               
                        } else {
                            $bulan = 'Januari';
                        }
                    ?>
                    <?php
                        if ($bulan2 != 1) {
                            if ($bulan2 != 2 ) {
                               if ($bulan2 != 3) {
                                   if ($bulan2 != 4) {
                                        if ($bulan2 !=5) {
                                            if ($bulan2 !=6) {
                                                if ($bulan2 !=7) {
                                                    if ($bulan2 !=8) {
                                                        if ($bulan2 !=9) {
                                                            if ($bulan2 !=10) {
                                                                if ($bulan2 !=11) {
                                                                        $bulan2 = 'Desember';
                                                                } else {
                                                                    $bulan2 = 'Nopember';
                                                                }
                                                            } else {
                                                                $bulan2 = 'Oktober';
                                                            }
                                                        } else {
                                                            $bulan2 = 'September';
                                                        }
                                                    } else {
                                                        $bulan2 = 'Agustus';
                                                    }
                                                } else {
                                                    $bulan2 = 'Juli';
                                                }
                                            } else {
                                                $bulan2 = 'Juni';
                                            }
                                        } else {
                                            $bulan2 = 'Mei';  
                                        }
                                    } else {
                                        $bulan2 = 'April';       
                                   }
                                } else {
                                    $bulan2 = 'Maret';
                                }
                            } else {
                                $bulan2 = 'Februari';    
                            }                               
                        } else {
                            $bulan2 = 'Januari';
                        }
                    ?>

                    <h5 class="box-title"><?php echo "Periode : ",$hanyatanggal," ",$bulan," ",$tahun, " - ", $hanyatanggal1," ", $bulan2," ",$tahun2; ?></h5>
                </tr>

            </div>
               
            <div class="row">
                <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-lg-12 table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th><?php echo 'No'; ?></th>
                                <th><?php echo 'No EDC'; ?></th>
                                <th><?php echo 'Nama'; ?></th>
                                <th><?php echo 'Saldo Awal'; ?></th>
                                <th><?php echo 'Debit'; ?></th>
                                <th><?php echo 'Kredit'; ?></th>
                                <th><?php echo 'Sisa Saldo'; ?></th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        //count
                        $jmlulsql   = "select count(*) from  Gateway.dbo.EDCList where KID = '$_SESSION[KID]'";
                        //echo $jmlulsql;
                        $jmlulstmt  = sqlsrv_query($conn, $jmlulsql);
                        $jmlulrow   = sqlsrv_fetch_array($jmlulstmt, SQLSRV_FETCH_NUMERIC);

                        //pagging
                        $perpages   = 10;
                        $halaman    = $_GET['page'];
                        if(empty($halaman)){
                            $posisi  = 0;
                            $batas   = $perpages;
                            $halaman = 1;
                        } else {
                            $posisi  = (($perpages * $halaman) - 10) + 1;
                            $batas   = ($perpages * $halaman);
                        }

                            
                       
                        $tgla= date('1990-01-01 00:00:00');
                        $tglk = date('Y-m-d', strtotime('-1 days', strtotime($from)));
                        
                        $ulsql = "SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY SerialNumber asc) as row FROM [Gateway].[dbo].[EDCList] a where KID = '$kid' ) a WHERE row between '$posisi' and '$batas'";
                        //echo $ulsql;
                        $ulstmt = sqlsrv_query($conn, $ulsql);
                        while($ulrow = sqlsrv_fetch_array( $ulstmt, SQLSRV_FETCH_NUMERIC)){
                            $saldo = 0;
                            if ($ulrow != null){
                                $snilaid = "select sum(amount) from dbo.translist where AccountDebet = '$ulrow[0]' and transactiontype between 'TOPE' and 'TOPP' and Date between '$tgla' and '$tglk 23:59:59.999'";
                                //echo $snilaid;
                                $pnilaid = sqlsrv_query($conn, $snilaid);
                                while ($hnilaid = sqlsrv_fetch_array( $pnilaid, SQLSRV_FETCH_NUMERIC)){
                                    if ($hnilaid != 0){
                                        $hsnilaid = $hnilaid[0];    
                                    } else {
                                        $hsnilaid = 0;    
                                    }
                                }
                                //echo $hsnilaid;


                                $snilaik = "select sum(amount) from dbo.translist where AccountKredit = '$ulrow[0]' and transactiontype between 'TOPE' and 'TOPP' and Date between '$tgla' and '$tglk 23:59:59.999'";
                                //echo $snilaik;
                                $pnilaik = sqlsrv_query($conn, $snilaik);
                                while ($hnilaik = sqlsrv_fetch_array( $pnilaik, SQLSRV_FETCH_NUMERIC)){
                                    if ($hnilaik != 0){
                                        $hsnilaik = $hnilaik[0];    
                                    } else {
                                        $hsnilaik = 0;    
                                    }
                                }
                                // echo $hsnilaik;

                                $sdebit = "select sum(amount) from dbo.translist where AccountDebet = '$ulrow[0]' and transactiontype between 'TOPE' and 'TOPP' and Date between '$from 00:00:00' and '$to 23:59:59.999'";
                                //echo $sdebit;
                                $pdebit = sqlsrv_query($conn, $sdebit);
                                while ($hdebit = sqlsrv_fetch_array( $pdebit, SQLSRV_FETCH_NUMERIC)){
                                    if ($hdebit != 0){
                                        $hsdebit = $hdebit[0];    
                                    } else {
                                        $hsdebit = 0;    
                                    }
                                }
                                    

                                $skredit = "select sum(amount) from dbo.translist where AccountKredit = '$ulrow[0]' and transactiontype between 'TOPE' and 'TOPP'and Date between '$from 00:00:00' and '$to 23:59:59.999'";
                                //echo $skredit;
                                $pkredit = sqlsrv_query($conn, $skredit);
                                while ($hkredit = sqlsrv_fetch_array( $pkredit, SQLSRV_FETCH_NUMERIC)){
                                    if ($hkredit != 0){
                                        $hskredit = $hkredit[0];
                                    } else {
                                        $hskredit = 0;    
                                    }
                                }

                                $awlsld = $hsnilaik - $hsnilaid;
                                $saldo = ($hsnilaik - $hsnilaid) - $hsdebit + $hskredit;


                                $nmedc = "select * from [PaymentGateway].[dbo].[UserPaymentGateway] where KodeUser = '$ulrow[5]'";
                                $pnmedc = sqlsrv_query($conn, $nmedc);
                                $hnmedc = sqlsrv_fetch_array($pnmedc, SQLSRV_FETCH_NUMERIC);


                              
                            } 



                            ?>

                            <tr>
                                <td><?php echo $ulrow[9]; ?></td>
                                <td><a href="rep_tedc.php?pagem=<?php echo $_GET['page'];?>&lok=<?php echo $ulrow[0]; ?>&tgl1=<?php echo $from; ?>&tgl2=<?php echo $to; ?>&bulan=<?php echo $_GET['bulan']; ?>&tahun=<?php echo $_GET['tahun']; ?>&nmak=<?php echo $hnmedc[1]; ?>"><?php echo $ulrow[0]; ?></a></td>
                                <td><?php echo $hnmedc[1]; ?></td>
                                <td style="text-align: right;"><b><?php echo number_format($awlsld,2); ?></b></td>
                                <td style="text-align: right;"><?php echo number_format($hsdebit,2); ?></td>
                                <td style="text-align: right;"><?php echo number_format($hskredit,2); ?></td>
                                <td style="text-align: right;"><b><?php echo number_format($saldo,2); ?></b></td>
                            </tr>

                            <?php } ?>   
                            <?php
                            $jmlhalaman = ceil($jmlulrow[0]/$perpages);
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="box-footer clearfix right">
                <div style="text-align: center;">
                    Showing <?=$halaman?> to <?=$jmlhalaman?> of <?=$jmlulrow[0]?> entries <?=$posisi." - ".$batas?>
                    <?php
                    $bulan = $_GET['bulan'];
                    $tahun = $_GET['tahun'];
                    $reload = "rep_edc.php?bulan=$bulan&tahun=$tahun";
                    $page = intval($_GET["page"]);
                    $adjacents = isset($_GET['adjacents']) ? intval($_GET['adjacents']) : 2; // khusus style pagination 2 dan 3
                        if( $page == 0 ) $page = 1;
                            $tpages = ($jmlhalaman) ? ceil($jmlhalaman) : 0; // total pages, last page number
                            echo "<div>".paginate($reload, $page, $tpages)."</div>";
                            ?>
                </div>
            </div>           
        <?php } ?>
    </div>
</div>


<?php
function paginate($reload, $page, $tpages) {

    $firstlabel = "First";
    $prevlabel  = "Prev";
    $nextlabel  = "Next";
    $lastlabel  = "Last";

    $out = "<div class=\"backpaging\"><ul class=\"ownpaging\">\n";

    // first
    if($page>1) {
        $out.= "<li><a href=\"" . $reload . "\">" . $firstlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $firstlabel . "</a></li>\n";
    }

    // previous
    if($page==1) {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $prevlabel . "</a></li>\n";
    }
    elseif($page==2) {
        $out.= "<li><a href=\"" . $reload . "\">" . $prevlabel . "</a></li>\n";
    }
    else {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" . ($page-1) . "\">" . $prevlabel . "</a></li>\n";
    }

    // current
    $out.= "<li class=\"active\"><a href=\"#\">Page " . $page . " of " . $tpages . "</a></li>\n";

    // next
    if($page<$tpages) {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" .($page+1) . "\">" . $nextlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $nextlabel . "</a></li>\n";
    }

    // last
    if($page<$tpages) {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" . $tpages . "\">" . $lastlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $lastlabel . "</a></li>\n";
    }

    $out.= "</ul></div>";

    return $out;
}
?>


<script type="text/javascript">
    var tampung = [];

     $('.btn-addu').click(function(){
       var index = $('#detailUser tr').length;

        var user = $('#user option:selected').val();
        var amount = $('#amount').val();
        var date = $('#date').val();

        if(date == ''){
          alert('Assign date tidak boleh kosong');
          return false;
        }
        else if(user == '' || amount == '' || amount <= 0){
            alert('Harap isi inputan dengan benar');
            return false;
        }
        else if(jQuery.inArray(user, tampung) !== -1){
            alert('Tidak dapat memilih user yang sudah ditambahkan sebelumnya');
            return false;
        }
        else{
            $.ajax({
                url : "ajax_addteller.php",
                type : 'POST',
                dataType : 'json',
                data: { index: index, user: user, amount: amount, date: date},   // data POST yang akan dikirim
                success : function(data) {
                    if(data.status == 1){
                        tampung.push(data.id);
                        $("#detailUser").append("<tr>" +
                            "<td><input type='hidden' name='user["+data.index+"]' value="+ data.id +">" + data.nama + "</td>" +
                            "<td><input type='hidden' name='amount["+data.index+"]' value="+ data.amount +">" + data.amount +
                            "<td><a class='btn btn-default btn-flat btn-sm text-red remove' onclick='$(this).parent().parent().remove();'><i class='ion-android-delete'></i></a></td>" +
                            "</tr>");

                            $('#user').val('');
                            $('#amount').val('');
                    }
                    else{
                        alert(data.message);
                        return false;
                    }
                },
                error : function() {
                    alert('Telah terjadi error.');
                    return false;
                }
            });
        }
    });
</script>

<?php require('footer.php'); ?>
