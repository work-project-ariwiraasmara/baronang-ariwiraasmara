<?php require('blank-header.php');?>

<?php
session_start();
include("connectinti.php");
?>

<div class="m-scene" id="main">
    <div id="content" class="primary-color login">
        <div class="login-form animated fadeinup delay-2 primary-color" style="color: #fff; margin-right: -7px; margin-left: -7px;">

                <?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
                    <div class="notification notification-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
                        <!--<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> <i class="icon fa fa-warning"></i>-->
                        <h4> <?php echo ucfirst($_SESSION['error-type']); ?></h4>
                        <?php echo $_SESSION['error-message']; ?>
                    </div>
                <?php } ?> <br>

                <h1 style="color: #fff;">Register</h1>
                <form action="procregister.php" method="post" enctype="multipart/form-data">
                    <div class="input-field has-feedback">
                        <input type="number" id="uid" name="uid" class="validate" required="">
                        <label for="login"><?php echo lang('User ID Baronang App'); ?></label>
                        <span class="fa fa-user form-control-feedback" aria-hidden="true"></span>
                        <a href="https://play.google.com/store/apps/details?id=com.baronang.app"><?php echo lang('Cara mendapatkannya'); ?> ?</a>
                    </div>
                    <div class="input-field has-feedback">
                        <input type="text" name="name" class="validate" autocomplete="off">
                        <label for="login"><?php echo lang('Nama koperasi'); ?></label>
                        <span class="fa fa-address-card-o form-control-feedback" aria-hidden="true"></span>
                    </div>
                    <div class="input-field has-feedback">
                        <input type="email" name="email" class="validate" autocomplete="off" required="">
                        <label for="login"><?php echo lang('Email'); ?></label>
                        <span class="fa fa-envelope-o form-control-feedback" aria-hidden="true"></span>
                    </div>
                    <div class="input-field has-feedback">
                        <input type="number" name="telp" class="validate" autocomplete="off" required="">
                        <label for="login"><?php echo lang('Telepon'); ?></label>
                        <span class="fa fa-phone form-control-feedback" aria-hidden="true"></span>
                    </div>
                    <div class="input-field has-feedback">
                        <input type="text" name="address" class="validate" autocomplete="off" required="">
                        <label for="login"><?php echo lang('Alamat'); ?></label>
                        <span class="fa fa-globe form-control-feedback" aria-hidden="true"></span>
                    </div>
                    <div class="input-field has-feedback">
                        <select name="province" class="browser-default txt-black" id="province" required="">
                            <option value="">- <?php echo lang('Pilih provinsi'); ?> -</option>
                            <?php
                            $a = "select * from [dbo].[Provinsi]";
                            $b = sqlsrv_query($conns, $a);
                            while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                                ?>
                                <option value="<?php echo $c[0]; ?>"><?php echo $c[1]; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="input-field has-feedback" id="city">
                        <select name="city" class="browser-default txt-black" required="">
                            <option value="">- <?php echo lang('Pilih kota'); ?> -</option>
                        </select>
                    </div>

                    <div style="margin-top: 20px; margin-bottom: -10px;">
                        <button type="submit" class="waves-effect waves-light width-100 m-b-20 animated bouncein delay-4" style="background: #00c0ef; border-radius: 50px; color: #fff; padding: 15px; border: none;"><?php echo lang('Daftar'); ?></button>
                    </div>

                    <a href="login.php"><button type="button" class="waves-effect waves-light width-100 m-b-20 animated bouncein delay-4" style="background: #fff; border-radius: 50px; color: #000; padding: 15px; border: none;"><?php echo lang('Masuk'); ?></button></a>
                </form>

        </div>
    </div>
</div>
<script type="text/javascript">
    $('#uid').keyup(function(){
        var uid = $('#uid').val();

        if(uid.length > 16){
            alert('<?php echo lang('User ID Baronang App Harus 16 Digit'); ?>');
            return false;
        }
    });

    $('#province').change(function(){
        var province = $('#province :selected').val();

        $.ajax({
            url : "ajax_register.php",
            type : 'POST',
            data: { province: province},
            success : function(data) {
                $("#city").html(data);
            },
            error : function(){
                alert('<?php echo lang('Silahkan coba lagi'); ?>');
                return false;
            }
        });
    });
</script>

<?php require('footer_new.php');?>