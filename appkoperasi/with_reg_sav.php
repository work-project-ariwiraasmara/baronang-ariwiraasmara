<?php require('header.php');?>      
<?php require('sidebar-left.php');?>
<?php require('content-header.php');?>
		<div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Regular Saving Withdrawal</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" action="procwith_reg_sav.php" method = "POST">
              <div class="box-body">
				<div class="row">
					<div class="col-sm-6">
							<div class="form-group">
								<label for="csoaregsavmember" class="col-sm-2 control-label" style="text-align: left;">Member ID</label>
								<div class="col-sm-6">
								<input type="text" name="member" class="form-control" id="csoaregsavmember" placeholder="" value="<?=$ul0?>" <?=$uldisabled;?>>
								</div>
							</div>
						  <div class="form-group">
							<label for="nama" class="col-sm-2 control-label" style="text-align: left;">Name</label>
							<div class="col-sm-6">
							  <input type="text" name="nama" class="form-control" id="nama" placeholder="" value="<?=$ul1?>" disabled>
							</div>
						  </div>
						  </div>
						 <div class="col-sm-6">
					<div class="form-group">
							<label for="regularsavingdescription" class="col-sm-2 control-label">Amount</label>
							<div class="col-sm-6">
								<input type="text" name="amo" class="form-control" id="regularsavingdescription" placeholder="" value="">
							</div>
						</div>
					<div class="form-group">
							<label for="regularsavingdescription" class="col-sm-2 control-label">Account Number</label>
							<div class="col-sm-6">
								<input type="text"  name="acc" class="form-control" id="regularsavingdescription" placeholder="">
							</div>
						</div>
						</div>
						</div>
				<div class="col-md-12">
				<br>
                <button type="submit" class="btn btn-info pull-left">Save</button>
				</div>
              </div>
			  </div>

      <?php require('content-footer.php');?>

      <?php require('footer.php');?>