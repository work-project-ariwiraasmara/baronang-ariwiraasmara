<?php require('header.php');?>

<?php require('sidebar-right.php');?>

<?php require('sidebar-left.php');?>

<?php
include "connectuser.php";

$id = '';
$date = '';
$type = '';
$nama = '';
$amount = 0;
if(isset($_POST['close'])){
    unset($_SESSION['BarcodeCode']);
}

if(isset($_POST['barcode'])){
    $id = $_POST['barcode'];
}

if(isset($_GET['id'])){
    $id = $_GET['id'];

    $qrCodeUrl = '';
    $x = "select * from [dbo].[UserTokenTransaksi] where BarcodeTransaksi='$id' and StatusTrx = 0 and KID = '$_SESSION[KID]'";
    //echo $x;
    $y = sqlsrv_query($connuser, $x);
    $z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC);
    if($z != null){
        //cek user
        $asd = "select * from [dbo].[UserPaymentGateway] where KodeUser='$z[2]'";
        $cvb = sqlsrv_query($connuser, $asd);
        $bnm = sqlsrv_fetch_array($cvb, SQLSRV_FETCH_NUMERIC);
        if($bnm == null){
            $qwe = "select * from [dbo].[MemberList] where MemberID='$z[2]'";
            $ert = sqlsrv_query($conn, $qwe);
            $tyu = sqlsrv_fetch_array($ert, SQLSRV_FETCH_NUMERIC);
            if($tyu == null){
                $nama = ' - ';
            }
            else{
                $nama = $tyu[2];
            }
        }
        else{
            $nama = $bnm[1];
        }

        $date =  $z[7]->format('Y-m-d H:i:s');

        if($z[5] == 0){
            $type = 'Setoran Simpanan';
            $amount = number_format($z[6]);
        }
        else if($z[5] == 1){
            $type = 'Setoran Tabungan';
            $amount = number_format($z[6]);
        }
        else if($z[5] == 2){
            $type = 'Penarikan Tunai Tabungan';
            $amount = number_format($z[6]);
        }
        else if($z[5] == 3){
            $type = 'Pembayaran Pokok Pinjaman';
            $amount = number_format($z[6]);
        }
        else if($z[5] == 4){
            $type = 'Pembayaran Bunga Pinjaman';
            $amount = number_format($z[6]);
        }
        else if($z[5] == 5){
            $type = 'Deposito';
            $amount = number_format($z[6]);
        }
        else if($z[5] == 6){
            $type = 'Penutupan Tabungan';
            $amount = '-';
        }
        else if($z[5] == 7){
            $type = 'Penutupan Pinjaman';
            $amount = '-';
        }
        else if($z[5] == 8){
            $type = 'Penutupan Deposito';
            $amount = '-';
        }
        else if($z[5] == 9){
            $type = 'Pembukaan Tabungan';
            $amount = number_format($z[6]);
        }
        else if($z[5] == 10){
            $type = 'Pembukaan Pinjaman';
            $amount = number_format($z[6]);
        }
        else if($z[5] == 11){
            $type = 'Pembukaan Deposito';
            $amount = number_format($z[6]);
        }
        else if($z[5] == 12){
            $type = 'Pembayaran Pinjaman';
            $amount = number_format($z[6]);
        }
        else if($z[5] == 13){
            $type = 'Pembukaan e-Money';
            $amount = number_format($z[6]);
        }
        else{
            $type = '';
            $amount = '-';
        }
    }
    else{
        $_SESSION['error-type'] = 'warning';
        $_SESSION['error-message'] = 'Barcode Not Found';
        $_SESSION['error-time'] = time() + 5;
    }
}
?>

<div class="animated fadeinup delay-1">
    <div class="page-content">

        <h3 class="uppercase"><?php echo lang('Pelayanan Pelanggan'); ?></h3>

        <?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
            <div class="notification notification-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
                <!--<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> <i class="icon fa fa-warning ?>"></i>-->
                <h4> <?php echo ucfirst($_SESSION['error-type']); ?> </h4>
                <?php echo $_SESSION['error-message']; ?>
            </div>
        <?php } ?>

        <?php if($_SESSION['BarcodeCode'] == ''){ ?>

            <div class="input-field">
              <label for="result-code" class="active">Barcode</label>
                <input type="password" class="validate" name="code" id="result-code" value="<?php echo $id; ?>" >
            </div>
            <br>
            <div class="row">
                <div class="col">
                    <button type="button" class="waves-effect waves-light btn-large primary-color width-100" id="btn-go">Go!</button>
                </div>

                <div class="col">
                    <button type="button" id="ocam" class="waves-effect waves-light btn-large primary-color width-100">Scan QR Code</button>
                </div>
            </div>

            <div class="row">
                <?php if($z != null){ ?>
                    <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-lg-12 table-responsive">
                        <h3><?php echo lang('Detail Transaksi'); ?></h3>
                        <input type="hidden" name="tid" id="tid" value="<?php echo $z[0] ?>" readonly>
                        <table class="table table-bordered table-hover table-striped">
                            <tr>
                                <th><?php echo lang('Nomor Akun'); ?></th>
                                <td><?php echo $z[3]; ?></td>
                            </tr>
                            <tr>
                                <th><?php echo lang('Nama'); ?></th>
                                <td><?php echo $nama; ?></td>
                            </tr>
                            <tr>
                                <th><?php echo lang('Tipe Transaksi'); ?></th>
                                <td><?php echo $type; ?></td>
                            </tr>
                            <tr>
                                <th><?php echo lang('Jumlah'); ?></th>
                                <td><?php echo $amount; ?></td>
                            </tr>
                            <tr>
                                <th><?php echo lang('Tanggal'); ?></th>
                                <td><?php echo $date; ?></td>
                            </tr>
                        </table>

                        <input type="hidden" id="type" value="<?php echo $z[5]; ?>" readonly>

                        <div style="margin-top: 30px;">
                            <a href="#modal3" id="btn-send" class="waves-effect waves-light btn-large modal-trigger primary-color width-100 m-b-20"><?php echo lang('Kirim Konfirmasi'); ?></a>
                        </div>

                        <div style="margin-top: 10px; margin-bottom: 30px;">
                            <a href="identity_cas.php"><button type="button" class="waves-effect waves-light btn-large primary-color width-100"><?php echo lang('Batal'); ?></button></a>
                        </div>
                    </div>
                <?php } ?>
            </div>
        <?php
        } else {
            $amount2 = 0;
            $date2 = '';
            $xx = "select * from [dbo].[UserTokenTransaksi] where BarcodeTransaksi='$_SESSION[BarcodeCode]' and KID = '$_SESSION[KID]'";
            $yy = sqlsrv_query($connuser, $xx);
            $zz = sqlsrv_fetch_array($yy, SQLSRV_FETCH_NUMERIC);
            if($zz != null){

                //cek user
                $asd = "select * from [dbo].[UserPaymentGateway] where KodeUser='$zz[2]'";
                $cvb = sqlsrv_query($connuser, $asd);
                $bnm = sqlsrv_fetch_array($cvb, SQLSRV_FETCH_NUMERIC);
                if($bnm == null){
                    $qwe = "select * from [dbo].[MemberList] where MemberID='$zz[2]'";
                    $ert = sqlsrv_query($conn, $qwe);
                    $tyu = sqlsrv_fetch_array($ert, SQLSRV_FETCH_NUMERIC);
                    if($tyu == null){
                        $nama = ' - ';
                    }
                    else{
                        $nama = $tyu[2];
                    }
                }
                else{
                    $nama = $bnm[1];
                }
    
                $status = '';
                if($zz[4] == 1){
                    $status = 'Menunggu Persetujuan';
                }

                if($zz[4] == 2){
                    $status = 'Transaksi berhasil';
                }

                $date2 =  $zz[7]->format('Y-m-d H:i:s');

                 if($zz[5] == 0){
                     $type2 = 'Setoran Simpanan';
                     $amount2 = number_format($zz[6]);
                 }
                 else if($zz[5] == 1){
                     $type2 = 'Setoran Tabungan';
                     $amount2 = number_format($zz[6]);
                 }
                 else if($zz[5] == 2){
                     $type2 = 'Penarikan Tunai Tabungan';
                     $amount2 = number_format($zz[6]);
                 }
                 else if($zz[5] == 3){
                     $type2 = 'Pembayaran Pokok Pinjaman';
                     $amount2 = number_format($zz[6]);
                 }
                 else if($zz[5] == 4){
                     $type2 = 'Pembayaran Bunga Pinjaman';
                     $amount2 = number_format($zz[6]);
                 }
                 else if($zz[5] == 5){
                     $type2 = 'Deposito';
                     $amount2 = number_format($zz[6]);
                 }
                 else if($zz[5] == 6){
                     $type2 = 'Penutupan Tabungan';
                     $amount2 = '-';
                 }
                 else if($zz[5] == 7){
                     $type2 = 'Penutupan Pinjaman';
                     $amount2 = '-';
                 }
                 else if($zz[5] == 8){
                     $type2 = 'Penutupan Deposito';
                     $amount2 = '-';
                 }
                 else if($zz[5] == 9){
                     $type2 = 'Pembukaan Tabungan';
                     $amount2 = number_format($zz[6]);
                 }
                 else if($zz[5] == 10){
                     $type2 = 'Pembukaan Pinjaman';
                     $amount2 = number_format($zz[6]);
                 }
                 else if($zz[5] == 11){
                     $type2 = 'Pembukaan Deposito';
                     $amount2 = number_format($zz[6]);
                 }
                 else if($zz[5] == 12){
                     $type2 = 'Pembayaran Pinjaman';
                     $amount2 = number_format($zz[6]);
                 }
                 else{
                     $type2 = '';
                     $amount2 = '-';
                 }
            }
            ?>
                    <form action="" method="post">
                        <div class="table-responsive">
                            <h3><?php echo lang('Detail Transaksi'); ?></h3>
                            <table class="table table-bordered table-hover table-striped">
                                <tr>
                                    <th><?php echo lang('Nomor Akun'); ?></th>
                                    <td><?php echo $zz[3]; ?></td>
                                </tr>
                                <tr>
                                    <th><?php echo lang('Nama'); ?></th>
                                    <td><?php echo $nama; ?></td>
                                </tr>
                                <tr>
                                    <th><?php echo lang('Tipe Transaksi'); ?></th>
                                    <td><?php echo $type2; ?></td>
                                </tr>
                                <tr>
                                    <th><?php echo lang('Jumlah'); ?></th>
                                    <td><?php echo $amount2; ?></td>
                                </tr>
                                <tr>
                                    <th><?php echo lang('Tanggal'); ?></th>
                                    <td><?php echo $date2; ?></td>
                                </tr>
                                <tr>
                                    <th>Status</th>
                                    <td><b><?php echo $status; ?></b></td>
                                </tr>
                            </table>

                            <div style="margin-top: 30px;">
                                <button type="submit" name="close" class="waves-effect waves-light btn-large primary-color width-100"><?php echo lang('Tutup'); ?></button>
                            </div>

                            <div style="margin-top: 10px; margin-bottom: 30px;">
                                <a href="identity_cas.php"><button type="button" class="waves-effect waves-light btn-large primary-color width-100">Refresh Page</button></a>
                            </div>

                        </div>
                    </form>
            <?php
        } ?>

        <div class="box box-solid hide" id="form-camera">
            <h3 class="uppercase">Scan QR Code/Barcode</h3><br>

            <div class=""><b>Pilih Kamera</b></div>
            <select class="browser-default" id="camera-select"></select>
            <br>
            <div class="box-body">
                <div class="row">
                    <div class="col">
                        <button title="Start Scan QR Barcode" class="waves-effect waves-light btn-large primary-color width-100" id="play" type="button">Start</button>
                    </div>
                    <div class="col">
                        <button title="Stop streams" class="waves-effect waves-light btn-large primary-color width-100" id="stop" type="button">Stop</button>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <div class="well" style="position: relative;display: inline-block;">
                            <canvas id="webcodecam-canvas"></canvas>
                            <div class="scanner-laser laser-rightBottom" style="opacity: 0.5;"></div>
                            <div class="scanner-laser laser-rightTop" style="opacity: 0.5;"></div>
                            <div class="scanner-laser laser-leftBottom" style="opacity: 0.5;"></div>
                            <div class="scanner-laser laser-leftTop" style="opacity: 0.5;"></div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6">
                        <div class="thumbnail" id="result">
                            <img style="width: 100%;" id="scanned-img" src="">
                            <div class="caption">
                                <p class="text-center" id="scanned-QR"></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<?php if($_SESSION['device'] == 0){ ?>
    <script type="text/javascript">
        $('#ocam').click(function(){
            Intent.openActivity("QRActivity","identity_cas.php");
          });
    </script>
<?php } ?>

<?php if($_SESSION['device'] == 1){ ?>
  <script type="text/javascript">
      $('#ocam').click(function(){
          $('#result-code').val();
          $('#form-camera').removeClass('hide');
      });

      $('#stop').click(function(){
          $('#form-camera').addClass('hide');
      });
  </script>
<?php } ?>

    <script type="text/javascript">
        $('#result-code').keyup(function(){
            var id = $('#result-code').val();

            if(id.length == 15){
                window.location.href = 'identity_cas.php?id=' + id;
            }
        });

        $('#btn-go').click(function(){
            var id = $('#result-code').val();

            window.location.href = 'identity_cas.php?id=' + id;
        });

        $('#btn-send').click(function(){
            var tid = $('#tid').val();
            var type = $('#type').val();

            $('#result-code').prop('disabled', true);

            $.ajax({
                url : "ajax_sendrequest.php",
                type : 'POST',
                data: { tid: tid, type: type},
                success : function(data) {
                    $(".modal-content").html(data);
                },
                error : function(){
                    alert('Silahkan coba lagi.');
                }
            });

        });
    </script>
</section>

<?php require('footer.php');?>
