<!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
          <div class="pull-left image">
            <img src="static/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
          </div>
          <div class="pull-left info">
          <p><?=$NameUser?></p>
            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
          </div>
        </div>
        <div></div>
        <ul class="sidebar-menu">
          <li class="header">MAIN MENU</li>
          <li class="<?=$uhome?> treeview">
            <a href="home-user.php">
              <i class="fa fa-dashboard"></i> <span>Home</span>
              <span class="pull-right-container">
              </span>
            </a>
		  </li>
		  <li class="<?=$ucard?> treeview">
            <a href="user-card.php">
              <i class="fa fa-credit-card"></i> <span>Cards</span>
              <span class="pull-right-container">
              </span>
            </a>
		  </li>
		 <li class="<?=$uloan?> treeview">	
      </ul>
      </section>
      <!-- /.sidebar -->
    </aside>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">