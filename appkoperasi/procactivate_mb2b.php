<?php
$acttype        = $_POST['acttype'];

$id             = $_POST['actid'];
$vhlid          = $_POST['vhlid'];
$sqr            = $_POST['result-code'];
$sqrnumber      = $_POST['result-number'];

if($vhlid == ''){
    messageAlert('Harap pilih lokasi', 'info');
    echo "<script>document.location='activated_mb2b_compl.php?actv=$_GET[actv]';</script>";
}
else if($sqrnumber == ''){
  messageAlert('Harap masukan nomor kartu qr', 'info');
  echo "<script>document.location='activated_mb2b_compl.php?actv=$_GET[actv]';</script>";
}
else{

    $xu = "select* from dbo.MasterCard where CardNo='$sqrnumber'";
    $yu = sqlsrv_query($conn, $xu);
    $zu = sqlsrv_fetch_array($yu, SQLSRV_FETCH_NUMERIC);
    if($zu == null){
      messageAlert('Kartu QR tidak ditemukan', 'info');
      echo "<script>document.location='activated_mb2b_compl.php?actv=$_GET[actv]';</script>";
    }
    else{
      $xx = "select* from dbo.MasterCard where Barcode='$sqr'";
      $yy = sqlsrv_query($conn, $xx);
      $zz = sqlsrv_fetch_array($yy, SQLSRV_FETCH_NUMERIC);
      if($zz == null){
          $sql2 = "update dbo.MasterCard set Barcode = '$sqr' where CardNo = '$sqrnumber'";
          $query2 = sqlsrv_query($conn, $sql2);
      }

      if($acttype == 'B2B') {
          $sql = "insert into dbo.AktivasiB2b(AktivasiB2BID, RegistrasiB2BID, TipeQR) values('$id','$vhlid','$sqrnumber')";
      }
      else {
          $sql = "insert into dbo.AktivasiCompliment(AktivasiComplimentID, RegistrasiComplimentID, TipeQR) values('$id','$vhlid','$sqrnumber')";
      }
      $query = sqlsrv_query($conn, $sql);
      if($query){
          if($_GET['actv'] == 'b2b') {
              $sql2 = "update dbo.RegistrasiB2B set Quantity = Quantity - 1 where RegistrasiB2BID='$vhlid'";
              $query2 = sqlsrv_query($conn, $sql2);

              $x = "select* from dbo.RegistrasiB2B where RegistrasiB2BID='$vhlid'";
              $y = sqlsrv_query($conn, $x);
              $z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC);
              if($z != null){
                  $location = $z[1];
                  $vehicle = $z[2];
                  $validdate = $z[3]->format('Y-m-d');

                  $year = date('Y');
                  $startmonth = date('m');
                  $endmonth = $z[3]->format('m');

                  $register = date('Y-m-d H:i:s');
                  $sql3 = "insert into dbo.MemberLocation(LocationID, Status, DateRegister, ValidDate, VehicleID, CardNo)values('$location',1,'$register','$validdate','$vehicle','$sqrnumber')";
                  $query3 = sqlsrv_query($conn, $sql3);

                  for($a=$startmonth;$a<=$endmonth;$a++){
                      $qwes = "insert into dbo.MemberLocationMonth(CardNo, LocationID, VehicleID, Bulan, Year, Status) values('$sqrnumber','$location','$vehicle',$a,'$year',1)";
                      $asds =  sqlsrv_query($conn, $qwes);
                  }

              }

              messageAlert('Berhasil melakukan aktivasi B2B', 'success');
              echo "<script>document.location='activated_mb2b_compl.php?actv=b2b';</script>";
          }
          else {
              $sql2 = "update dbo.RegistrasiCompliment set Quantity = Quantity - 1 where RegistrasiComplimentID='$vhlid'";
              $query2 = sqlsrv_query($conn, $sql2);

              $x = "select* from dbo.RegistrasiCompliment where RegistrasiComplimentID='$vhlid'";
              $y = sqlsrv_query($conn, $x);
              $z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC);
              if($z != null){
                  $location = $z[1];
                  $vehicle = $z[2];
                  $validdate = $z[3]->format('Y-m-d');

                  $year = date('Y');
                  $startmonth = date('m');
                  $endmonth = $z[3]->format('m');

                  $register = date('Y-m-d H:i:s');
                  $sql3 = "insert into dbo.MemberLocation(LocationID, Status, DateRegister, ValidDate, VehicleID, CardNo)values('$location',1,'$register','$validdate','$vehicle','$sqrnumber')";
                  $query3 = sqlsrv_query($conn, $sql3);

                  for($a=$startmonth;$a<=$endmonth;$a++){
                      $qwes = "insert into dbo.MemberLocationMonth(CardNo, LocationID, VehicleID, Bulan, Year, Status) values('$sqrnumber','$location','$vehicle',$a,'$year',1)";
                      $asds =  sqlsrv_query($conn, $qwes);
                  }
              }

              messageAlert('Berhasil melakukan aktivasi Compliment', 'success');
              echo "<script>document.location='activated_mb2b_compl.php?actv=compl';</script>";
          }
      }
      else{
          messageAlert('Gagal melakukan aktivasi B2B', 'danger');
          echo "<script>document.location='activated_mb2b_compl.php?actv=$_GET[actv]';</script>";
      }
    }
}

?>
