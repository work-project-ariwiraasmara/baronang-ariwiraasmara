<?php require('header.php');?>

<?php require('sidebar-right.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>



<div class="animated fadeinup delay-1">
    <div class="page-content txt-black">
        <form action="" method="GET">
            <div class="input-field" >
                <?php
                if(isset($_GET['template'])){
                $to = $_GET['template'];
                }
                else{
                $to = date('Y/m/d');
                }
                ?>


                <select id="akun" name="akun" class="browser-default">
                    <option value=''>- <?php echo lang('Pilih Lokasi'); ?> -</option>
                        <?php
                        $julsql   = "select * from [dbo].[LocationMerchant  ] where status ='1' order by LocationID asc";
                        // $julsql1   = "select * from [dbo].[LaporanRugiLabaViewNew] where Header ='2' and Username = '$nameuser'";
                        echo $julsq;
                        $julstmt = sqlsrv_query($conn, $julsql);
                            while($rjulrow  = sqlsrv_fetch_array( $julstmt, SQLSRV_FETCH_NUMERIC)){
                               ?>
                    <option value="<?=$rjulrow[0];?>" <?php if($rjulrow[0]==$_GET['lok']){echo "selected";} ?>>&nbsp;<?=$rjulrow[0];?>&nbsp;<?=$rjulrow[2];?></option>
                            <?php } ?>
                </select>
            </div> 
               
            <div style="margin-top: 30px;">
                <button type="submit" name="ok" id="ok" class="btn btn-large primary-color width-100 waves-effect waves-light">OK</button>
            </div>
        </form>
            
        <?php
        $ok = @$_GET['ok'];
        if(isset($ok)) {        
            $lok = $_GET['akun'];
            $dte = date('d-m-Y');
            //echo $dte;
            $bulan_int = date('m', strtotime($dte));
            $tahun_int = date('Y', strtotime($dte));
            //echo $bulan.' '.$tahun.' / '.$bulan_int.' '.$tahun_int;
            ?>
                <script type="text/javascript">
                    window.location.href = "chart_kendaraan.php?chart=bulan&lok=<?php echo $lok; ?>&bulan=<?php echo $bulan_int; ?>&tahun=<?php echo $tahun_int; ?>";
                </script>
        <?php } ?>

        <?php 
        if ($_GET['chart']){
            $sim = "SELECT distinct a.VehicleType, v.name as Nama, v.VehicleID from dbo.ParkingList a inner join dbo.VehicleType v on a.VehicleType = v.Code where LocationIn = '$_GET[lok]' order by v.VehicleID asc";
                echo $sim;
            $nm = 1;
            $pan = sqlsrv_query($conn, $sim);
            while($nan = sqlsrv_fetch_array( $pan, SQLSRV_FETCH_NUMERIC)){


                ?>
                
                <div>
                    <h4><?php echo $nm; ?>. <?php echo $nan[1]; ?></h4>
                </div>

           
                <?php
                $chart = $_GET['chart'];
                if($_GET['chart'] and $_GET['lok']) {
                    $json_label = array();
                    $json_data1 = array();
                    $json_data2 = array();
            
                    $dte = date('d-m-Y');
                    //echo $dte;
                    $Lok = $_GET['lok'];
                    $bulan_int = date('t', strtotime($dte));
                    $tgl_int = date('m', strtotime($dte));
                    $tahun_int = date('Y', strtotime($dte));


                //echo $bulan.' '.$tahun.' / '.$bulan_int.' '.$tahun_int;
                ?>
                    <div id="bar_chart" class="card animated fadeinup delay-3">
                        <div class="legend txt-black">
                            <h5 class="uppercase txt-black"> <?php echo '01/'.$tgl_int.'/'.$tahun_int.' - '.$bulan_int.'/'.$tgl_int.'/'.$tahun_int; ?></h5>
                            <p><span class="data-color red lighten-0"></span>Waktu Masuk</p>
                            <p><span class="data-color blue lighten-0"></span>Waktu Keluar</p>

                    </div>

                    <div class="barChartCont">
                        <canvas id="barChart"></canvas>
                    </div>
                
                    <?php
                    if(@$_GET['chart'] == 'bulan') {
                        for($l = 1; $l <= $bulan_int; $l++) {
                            $ln1 = 0;
                            $ln2 = 0;
                           
                            $dtf = $tahun_int."-".$tgl_int."-".$l;
                            $dtt = $tahun_int."-".$tgl_int."-".$bulan_int;

                            if($l > 0 && $l < 10) { array_push($json_label, '0'.$l); }
                                else { array_push($json_label, $l); }
                        

                            $sql_line1 = "SELECT sum(ismember) from [dbo].[ParkingList] where Vehicletype = '$nan[0]' and LocationIn='$_GET[lok]' and TimeIn between '$dtf 00:00:00.000' and '$dtf 23:59:59.999'";
                            //echo $sql_line1;
                            $query1 = sqlsrv_query($conn, $sql_line1);
                            while($data1 = sqlsrv_fetch_array($query1)) {
                                array_push($json_data1, $data1[0]);
                            }

                            $sql_line2 = "SELECT sum(ismember) from [dbo].[ParkingList] where where Vehicletype = '$nan[0]' and LocationIn='$_GET[lok]' and TimeOut between '$dtf 00:00:00.000' and '$dtf 23:59:59.999'";
                            $query2 = sqlsrv_query($conn, $sql_line2);
                            while($data2 = sqlsrv_fetch_array($query2)) {
                                array_push($json_data2, $data2[0]);
                            }
                        }
                    } ?>
               
                    <div class="m-t-30">
                        <div class="row">
                            
                            <?php
                            for($tgl = 1; $tgl <= $bulan_int; $tgl++) { ?>
                                <div class="col s1">
                                    <?php 
                                    if($tgl > 0 && $tgl < 10) { 
                                        echo '<a href="chart_kendaraan.php?chart=hari&lok='.$Lok.'&tgl='.$tgl.'&bulan='.$tgl_int.'&tahun='.$tahun_int.'" style="color: #0000ff;">0'.$tgl.'</a>'; 
                                    }
                                    else { 
                                        echo '<a href="chart_kendaraan.php?chart=hari&lok='.$Lok.'&tgl='.$tgl.'&bulan='.$tgl_int.'&tahun='.$tahun_int.'" style="color: #0000ff;">'.$tgl.'</a>'; ; 
                                    }
                                    ?>
                                </div>
                                <?php
                            }
                            ?>

                        </div>
                    </div>
                <?php } else if(@$_GET['chart'] == 'hari') {
                    for($j = 0; $j < 24; $j++) {
                        $ln1 = 0;
                        $ln2 = 0;
                     
                        $dtf = $tahun_int."-".$tgl_int."-".@$_GET['tgl'];
                        $jf = $j.':00:00.000';
                        $jt = $j.':59:59.999';

                        //echo $dtf.' '.$jf.' - '.$dtf.' '.$jt.'<br>';
                        array_push($json_label, $j.':00 - '.$j.':59');
                        $sql_line1 = "SELECT SUM(ismember) from [dbo].[ParkingList] where Vehicletype = '$nan[0]' and TimeIn between '$dtf $jf' and '$dtf $jt'";
                        //echo $sql_line1;
                        $query1 = sqlsrv_query($conn, $sql_line1);
                        while($data1 = sqlsrv_fetch_array($query1)) {
                            array_push($json_data1, $data1[0]);
                        }

                        $sql_line2 = "SELECT SUM(ismember) from [dbo].[ParkingList] where Vehicletype = '$nan[0]' and TimeOut between '$dtf $jf' and '$dtf $jt'";
                        $query2 = sqlsrv_query($conn, $sql_line2);
                        while($data2 = sqlsrv_fetch_array($query2)) {
                            array_push($json_data2, $data2[0]);
                        }
                        //echo '<br>';
                    }
                }
            
            $nm++;}
       }
       ?>

    </div>
    
</div>

    <script src="js/chart.min.js"></script>
    <script type="text/javascript">
    var barChartData = {
        //labels: ["1", "2", "3", "4", "5"],
        labels: <?php echo json_encode($json_label); ?>,
        datasets: [{
            fillColor: "#ff0000",
            strokeColor: "#ff0000",
            highlightFill: "rgba(255, 100, 100, 0.9)",
            highlightStroke: "rgba(255, 100, 100, 0)",
            //data: [65, 59, 90, 56, 40]
            data: <?php echo json_encode($json_data1); ?>
        }, {
            fillColor: "#0000ff",
            strokeColor: "#0000ff",
            highlightFill: "rgba(100, 100, 255, 0.9)",
            highlightStroke: "rgba(100, 100, 255, 0)",
            //data: [28, 48, 19, 27, 90]
            data: <?php echo json_encode($json_data2); ?>
        }]
    }

    var ctx3 = document.getElementById("barChart").getContext("2d");
    window.myBar = new Chart(ctx3).Bar(barChartData, {
        responsive: true
    });
    </script>


<?php require('footer_new.php'); ?>