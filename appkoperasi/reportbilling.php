<?php
@session_start();
error_reporting(0);
require('header.php');
require('sidebar-right.php');
require('sidebar-left.php');

/*
create table ReportBilling(
   BillingID varchar(255) NOT NULL,
   UserIDMaker varchar(255) NOT NULL,
   UserIDBayar varchar(255) NOT NULL
   Date datetime NULL
   DateBayar datetime NULL
   Bulan month NULL
   Tahun year NULL
   NIP varhcar(255) NULL
   NoAnggotaLama varchar(255) 
   SP money
   SW money
   SS money
   AngsuranPokok money
   AngsuranBunga money
   Total money
   TotalDibayar money
   Status int
);

// Buat nyari NIP, No. Anggota Lama, Nama, SP, SW, SS
SELECT a.MemberID, a.OldMemberID ,a.Name, a.NIP, b.KodeBasicSavingType, b.AccNo, c.TimeStam, c.BillingAmmount, d.Status
from KoperasiNew.dbo.MemberList a
--SELECT b.KodeBasicSavingType, b.AccNo, c.TimeStam, c.BillingAmmount, d.Status
--from KoperasiNew.dbo.BasicSavingBalance b
inner join KoperasiNew.dbo.BasicSavingBalance b
   on a.MemberID = b.MemberID
inner join KoperasiNew.dbo.BasicSavingTransaction c
   on b.KodeBasicSavingType = c.KodeBasicSavingType
inner join KoperasiNew.dbo.BasicSavingType d
   on c.KodeBasicSavingType = d.KodeBasicSavingType
where --b.MemberID = '0006' and
c.TimeStam between '2018-01-01 00:00:00.000' and '2018-01-31 23:59:59.999' order by a.MemberID ASC, c.TimeStam ASC, d.Status ASC
// <-- END -->


// Buat nyari Angsuran Poko dan Angsuran Bunga
SELECT a.MemberID, a.OldMemberID ,a.Name, a.NIP, b.LoanAppNum, b.TimeStamp, c.LoanNumber--, d.Pokok, d.Bunga
from KoperasiNew.dbo.MemberList a
inner join [KoperasiNew].[dbo].[LoanApplicationList] b
   on a.MemberID = b.MemberID
inner join [KoperasiNew].[dbo].[LoanList] c
   on b.LoanAppNum = c.LoanAppNumber
--SELECT c.LoanNumber, d.Pokok, d.Bunga
--from [KoperasiNew].[dbo].[LoanList] c
inner join [KoperasiNew].[dbo].[LogLoanSchedule] d
   on c.LoanNumber = d.LoanNum
order by a.MemberID ASC, b.LoanAppNum ASC, b.TimeStamp ASC, c.LoanNumber ASC
// <-- END -->



SELECT MemberID, NIP, OldMemberID, Name from KoperasiNew.dbo.MemberList ORDER BY MemberID ASC
SELECT KodeBasicSavingType from KoperasiNew.dbo.BasicSavingBalance where MemberID = '0001'
SELECT Status, KodeBasicSavingType from KoperasiNew.dbo.BasicSavingType where KodeBasicSavingType = '700025101' --Status = 1 and 
SELECT * from [KoperasiNew].[dbo].[BasicSavingTransaction]
--SELECT Sum(BillingAmmount) as BillingAmmount from [KoperasiNew].[dbo].[BasicSavingTransaction] where KodeBasicSavingType = '700025101'

SELECT b.KodeBasicSavingType, SUM(c.BillingAmmount) from KoperasiNew.dbo.BasicSavingType b
inner join KoperasiNew.dbo.BasicSavingTransaction c
on b.KodeBasicSavingType = c.KodeBasicSavingType
where b.KodeBasicSavingType = '700025101' and Status = 1

SELECT [dbo].[getSPWS] ('0001','2018-11-01','2018-11-30','0')

--SELECT c.KodeBasicSavingType, c.TimeStam, c.BillingAmmount, d.Status
SELECT SUM(d.BillingAmmount)
from KoperasiNew.dbo.BasicSavingTransaction c
   --on b.KodeBasicSavingType = c.KodeBasicSavingType
inner join KoperasiNew.dbo.BasicSavingType d
   on c.KodeBasicSavingType = d.KodeBasicSavingType
where c.KodeBasicSavingType = '700025101' and
c.TimeStam between '2018-01-01 00:00:00.000' and '2018-01-31 23:59:59.999' order by c.TimeStam ASC, d.Status ASC



<?php
													$sqlsp = "SELECT [dbo].[getSPWS] ('$mid','$from','$to','0')";
													echo $sqlsp;
													$querysp = sqlsrv_query($conn, $sqlsp);
													$data = sqlsrv_fetch_array($querysp);
													$sp = $data[0];
													echo $sp;
													?>
*/
?>

	<div class="animated fadeinup delay-1">
		<div class="page-content txt-black">
			
			<h2 class="uppercase txt-black">Report Billing</h2>

			<div class="form-inputs">
				<form action="" method="post" enctype="multipart/form-data">
					
					<div class="input-field">
						<span><b>Tanggal</b></span> : <br>
						<input type="date" name="tgl" id="tgl" class="validate" value="<?php echo date('Y-m-d'); ?>" readonly>
					</div>

					<div class="row">
						<div class="col s6">
							<div class="input-field">
								<?php
				                    $BUL = array("JAN","FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC");
				                    $BULlen = count($BUL);
				                    $BULAN = array("Januari", "Februari", "Maret", "April", "May", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
				                    $BULANlen = count($BULAN);
				                ?>
				                <span><b>Bulan</b></span> : <br>
				                <select class="browser-default" id="bulan" name="bulan">
				                    <?php /* option value="<?php echo date('t M', strtotime("NOV+1 Month")); ?>">Januari</option> */ ?>
				                    <?php
				                    $ibul = 1;
				                    for($x = 0; $x < $BULlen; $x++) { ?>
				                        <?php /* <option value="<?php echo date('t M', strtotime($BUL[$x].date("Y"))); ?>"><?php echo $BULAN[$x]; ?></option> */ ?>
				                        <option value="<?php echo $BUL[$x]; ?>"><?php echo $BULAN[$x]; ?></option>
				                        <?php
				                    }
				                    ?>
				                </select>
							</div>
						</div>

						<div class="col s6">
							<div class="input-field">
								<span>Tahun</span> :
								<select class="browser-default" id="tahun" name="tahun">
									<?php
									for($x = 0; $x < 10; $x++) { ?>
										<option value="<?php echo date('Y') - $x; ?>"><?php echo date('Y') - $x; ?></option>
										<?php
									}
									?>
								</select>
							</div>
						</div>
					</div>
					

					<input type="hidden" name="" id="" value="">

					<div class="row">
						<div class="col s6">
							<button type="submit" name="btn_preview" id="btn_preview" class="btn btn-large primary-color waves-effect waves-light width-100 m-t-30">Preview</button>
						</div>

						<div class="col s6">
							<button type="submit" name="btn_ok" id="btn_ok" class="btn btn-large primary-color waves-effect waves-light width-100 m-t-30">simpan</button>
						</div>
					</div>

					<input type="hidden" name="kid" id="kid" value="<?php echo @$_SESSION['KID']; ?>">
					<input type="hidden" name="tglbayar" id="tglbayar" class="validate">
					<input type="hidden" name="uidmaker" id="uidmaker" class="validate">
					<input type="hidden" name="uidbayar" id="uidbayar" class="validate">
					<input type="hidden" name="nip" id="nip" class="validate">
					<input type="hidden" name="noagtlama" id="noagtlama" class="validate">
					<input type="hidden" name="sp" id="sp" class="validate">
					<input type="hidden" name="sw" id="sw" class="validate">
					<input type="hidden" name="ss" id="ss" class="validate">
					<input type="hidden" name="angspokok" id="angspokok" class="validate">
					<input type="hidden" name="angsbunga" id="angsbunga" class="validate">

					<input type="hidden" name="total" id="total" class="validate">
					<input type="hidden" name="totbayar" id="totbayar" class="validate">

					<input type="hidden" name="status" id="status" class="validate" value="0">

				</form>

					<?php
					$sp = 0;
					$sw = 0;
					$ss = 0;
					$pokok = 0;
					$bunga = 0;
					
					$ok = @$_POST['btn_ok'];
					if(isset($ok)) {
						$bulan = date('m', strtotime(@$_POST['bulan']));
						$ldbulan = date('t', strtotime(@$_POST['bulan']));
						$tahun = @$_POST['tahun'];
						
						$from = $tahun.'-'.$bulan.'-1';
						$to = $tahun.'-'.$bulan.'-'.$ldbulan;
						$url = 'drep_reportbilling.php?id='.$KID.'&bulan='.$bulan.'&ldbulan='.$ldbulan.'&tahun='.$tahun;
						//echo $url;

						//header('location:'.$url);
						?>
						<script type="text/javascript">
							window.location.href = "<?php echo $url; ?>";
						</script>
						<?php
					}

					$preview = @$_POST['btn_preview'];
					if(isset($preview)) {
						$bulan = date('m', strtotime(@$_POST['bulan']));
						$ldbulan = date('t', strtotime(@$_POST['bulan']));
						$tahun = @$_POST['tahun'];
						
						$from = $tahun.'-'.$bulan.'-1';
						$to = $tahun.'-'.$bulan.'-'.$ldbulan;
						//echo $from.' to '.$to;
					?>

						<div class="table-responsive" id="preview">
							<table>
								<thead>
									<tr>
										<th style="text-align: center;" colspan="13"><h2 class="uppercase" style="color: #000;"><?php echo 'Billing '.date('F', strtotime(@$_POST['bulan'])).' '.$tahun; ?></h2></th>
									</tr>
									<tr>
										<th>NIP</th>
										<th>No. Anggota Lama</th>
										<th>Nama</th>
										<th style="text-align: center;">SP (Rp.)</th>
										<th style=""></th>
										<th style="text-align: center; margin-left: 10px; margin-right: 10px;">SW (Rp.)</th>
										<th style=""></th>
										<th style="text-align: center;">SS (Rp.)</th>
										<th style="text-align: center;">Angsuran Pokok (Rp.)</th>
										<th style="text-align: center; margin-left: 10px; margin-right: 10px;"></th>
										<th style="text-align: center;">Angsuran Bunga (Rp.)</th>
										<th style="text-align: center; margin-left: 10px; margin-right: 10px;"></th>
										<th>Total (Rp.)</th>
									</tr>
								</thead>

								<tbody>
									
										<?php
										$sqla = "SELECT MemberID, NIP, OldMemberID, Name from [dbo].[MemberList] where KID='$KID' ORDER BY MemberID ASC";
										echo '-- LVL 1 --'.$sqla.'<br>';
										$querya = sqlsrv_query($conn, $sqla);
										while( $data1 = sqlsrv_fetch_array($querya) ) {
											$total = 0;
											$mid = $data1[0];
											?>
											<tr>
												<td><?php echo $data1['1']; ?></td>
												<td><?php echo $data1['2']; ?></td>
												<td><?php echo $data1['3']; ?></td>

												<?php
												$sqlb1 = "SELECT KodeBasicSavingType from [dbo].[BasicSavingBalance] where MemberID = '$mid'";
												echo '--- LVL 2 A ---'.$sqlb1.'<br>';
												$queryb1 = sqlsrv_query($conn, $sqlb1);
												while( $data2 = sqlsrv_fetch_array($queryb1) ) { 
													$kbst = $data2[0];

													$sqlc = "SELECT Status from [dbo].[BasicSavingType] where KodeBasicSavingType = '$kbst'";
													echo '--- LVL 3 A ---'.$sqlc.'<br>';
													$queryc = sqlsrv_query($conn, $sqlc);
													$data3 = sqlsrv_fetch_array($queryc);

													$sqld = "SELECT BillingAmmount from [dbo].[BasicSavingTransaction] where KodeBasicSavingType = '$kbst' and TimeStam between '$from' and '$to'";
													echo '--- LVL 3 B ---'.$sqld.'<br>';
													$queryd = sqlsrv_query($conn, $sqld);
													$data4 = sqlsrv_fetch_array($queryd);

													if($data3[0] == 0) {
														$sp = $data4[0];
														$total = intval($total) + intval($sp);
													}
													else if($data3[0] == 1) {
														$sw = $data4[0];
														$total = intval($total) + intval($sw);
													}
													else if($data3[0] == 2) {
														$ss = $data4[0];
														$total = intval($total) + intval($ss);
													}

												}

												$sqlb2 = "SELECT SUM(Pokok) as Pokok, SUM(Bunga) as Bunga from [dbo].[LoanApplicationList] a 
														  INNER JOIN [KoperasiNew].[dbo].[LogLoanSchedule] b ON a.LoanAppNum = b.LoanNum
														  where MemberID = '$mid' and Tanggal between '$from' and '$to' ";
												echo '--- LVL 2 B ---'.$sqlb2.'<br>';
												$queryb2 = sqlsrv_query($conn, $sqlb2);
												$data2 = sqlsrv_fetch_array($queryb2);

												$pokok = $data2[0];
												$total = intval($total) + intval($pokok);

												$bunga = $data2[1];
												$total = intval($total) + intval($bunga);
												
												?>
												<td><div class="right"><?php echo number_format($sp,2,',','.'); ?></div></td>
												<td>
												<td><div class="right"><?php echo number_format($sw,2,',','.'); ?></div></td>
												<td>
												<td><div class="right"><?php echo number_format($ss,2,',','.'); ?></div></td>

												<td><div class="right"><?php echo number_format($pokok,2,',','.'); ?></div></td>
												<td></td>
												<td><div class="right"><?php echo number_format($bunga,2,',','.'); ?></div></td>
												<td></td>
												<td><div class="right"><?php echo number_format($total,2,',','.'); ?></div></td>
											</tr>
											<?php
										}
										?>
									
								</tbody>
							</table>				
						</div>
					<?php
					}
					?>
			</div>

			<script type="text/javascript">
				/*
				$('#btn_preview').click(function(){
					if($('#preview').hasClass('hide')) {
						$('#preview').removeClass('hide');
					}
					else {
						$('#preview').addClass('hide');
					}
				});
				*/
			</script>

		</div>
	</div>

<?php require('footer.php');?>

