<?php
session_start();
error_reporting(0);

include('lib/phpExcel/Classes/PHPExcel.php');
include "connect.php";


   $filePath = "uploads/excel/";

    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getProperties()->setCreator("baronang");
    $objPHPExcel->getProperties()->setTitle("Drep List Jumlah Kendaraan");

// set autowidth
    for($col = 'A'; $col !== 'Z'; $col++) {
        $objPHPExcel->getActiveSheet()
            ->getColumnDimension($col)
            ->setAutoSize(true);
    }

    $listCombo = array();

    //sheet 2
    $objWorkSheet = $objPHPExcel->createSheet(0);
// baris judul
   
    $objWorkSheet->getStyle('C1')->getFont()->setBold(true);
    $objWorkSheet->getStyle('C1')->getFont()->setSize(14);
    $objWorkSheet->SetCellValue('C1', 'Laporan Jumlah Kendaraan');

    $objWorkSheet->getStyle('C2')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('C2', 'Periode :');                 

   
    


    $objWorkSheet->getStyle('A4')->getFont()->setBold(true);
    $objWorkSheet->getStyle('A4' )->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('A4', 'No');
    $objWorkSheet->getStyle('B4')->getFont()->setBold(true);
    $objWorkSheet->getStyle('B4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('B4', 'Lokasi');
    $objWorkSheet->getStyle('C4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->getStyle('C4')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('C4', 'Kendaraan Motor');
    $objWorkSheet->getStyle('D4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->getStyle('D4')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('D4', 'Kendaraan Mobil');
    $objWorkSheet->getStyle('E4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->getStyle('E4')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('E4', 'Kendaraan Truck');
    $objWorkSheet->getStyle('F4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->getStyle('F4')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('F4', 'Kendaraan VIP');
    $objWorkSheet->getStyle('G4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->getStyle('G4')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('G4', 'Kendaraan Valet');
    $objWorkSheet->getStyle('H4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->getStyle('H4')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('H4', 'Lain - Lain');
 


    if($_GET['from'] and $_GET['to']){
    
    $from= $_GET['from'];
    $to= $_GET['to'];
    $tglh = date('Y/m/d', strtotime('+1 days', strtotime($to)));
    $no = 1;
    $bln = 2 ;
    $hanyatanggal = date('d', strtotime($_GET['from']));
    $bulan = date('m', strtotime($_GET['from']));
    $tahun = date('Y', strtotime($_GET['from']));
        if ($bulan != 1) {
            if ($bulan != 2 ) {
                if ($bulan != 3) {
                    if ($bulan != 4) {
                        if ($bulan !=5) {
                            if ($bulan !=6) {
                                if ($bulan !=7) {
                                    if ($bulan !=8) {
                                        if ($bulan !=9) {
                                            if ($bulan !=10) {
                                                if ($bulan !=11) {
                                                    $bulan = 'Desember';
                                                } else {
                                                $bulan = 'Nopember';
                                                }
                                            } else {
                                            $bulan = 'OKtober';   
                                            }
                                        } else {
                                        $bulan = 'September';
                                        }
                                    } else {
                                    $bulan = 'Agustus';
                                    }
                                } else {
                                $bulan = 'Juli';
                                }
                            } else {
                            $bulan = 'Juni';
                            }
                        } else {
                        $bulan = 'Mei';  
                        }
                    } else {
                    $bulan = 'April';       
                    }
                } else {
                $bulan = 'Maret';
                }
            } else {
            $bulan = 'Februari';    
            }                               
        } else {
        $bulan = 'Januari';
        }

    $tgl2 = date('d', strtotime($_GET['to']));
    $bulan2 = date('m', strtotime($_GET['to']));
    $tahun2 = date('Y', strtotime($_GET['to']));
        if ($bulan2 != 1) {
            if ($bulan2 != 2 ) {
                if ($bulan2 != 3) {
                    if ($bulan2 != 4) {
                        if ($bulan2 !=5) {
                            if ($bulan2 !=6) {
                                if ($bulan2 !=7) {
                                    if ($bulan2 !=8) {
                                        if ($bulan2 !=9) {
                                            if ($bulan2 !=10) {
                                                if ($bulan2 !=11) {
                                                    $bulan2 = 'Desember';
                                                } else {
                                                $bulan2 = 'Nopember';
                                                }
                                            } else {
                                            $bulan2 = 'OKtober';   
                                            }
                                        } else {
                                        $bulan2 = 'September';
                                        }
                                    } else {
                                    $bulan2 = 'Agustus';
                                    }
                                } else {
                                $bulan2 = 'Juli';
                                }
                            } else {
                            $bulan2 = 'Juni';
                            }
                        } else {
                        $bulan2 = 'Mei';  
                        }
                    } else {
                    $bulan2 = 'April';       
                    }
                } else {
                $bulan2 = 'Maret';
                }
            } else {
            $bulan2 = 'Februari';    
            }                               
        } else {
        $bulan2 = 'Januari';
        }

        $objWorkSheet->getStyle('D'.$bln)->getFont()->setBold(true);
        $objWorkSheet->SetCellValue("D".$bln,$hanyatanggal .' '.$bulan .' '. $tahun .' - '. $tgl2 .' '. $bulan2 .' '. $tahun2);   


    $k = 4;
    $x = "select * from dbo.LocationMerchant where status = 1 and LocationID = '$akun' order by LocationID asc";
    //echo $x;
    $y = sqlsrv_query($conn, $x);
    while($z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC)){
           
        $objWorkSheet->getStyle('B'.$k)->getFont()->setBold(true);
        $objWorkSheet->SetCellValue("B".$k,$z[0] .' - '. $z[2]);           

    }

    

    
    $row = 5;
    $rw = 5;
    
    $xy = "SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY LocationID asc) as row FROM [dbo].[LocationMerchant] a where status = '1' ) a";
    //echo $x;
    $yz = sqlsrv_query($conn, $xy);
    
    while($za = sqlsrv_fetch_array( $yz, SQLSRV_FETCH_NUMERIC)){
         // if($za[11] == 1){
         //    $member='Member';
         //    } else {
         //    $member='Non Member';
         //    }
        $tkend = "select distinct (LocationOut) from dbo.parkinglist where timein between '$from' and '$tglh'";
        //echo $tkend;
        $ptkend = sqlsrv_query($conn, $tkend);
        while ($hasptkend = sqlsrv_fetch_array( $ptkend, SQLSRV_FETCH_NUMERIC)){
            if ($hasptkend == null){
                $nilai1 = 0;
                $nilai2 = 0;
                $nilai3 = 0;
                $nilai4 = 0;
                $nilai5 = 0;
                $nilai6 = 0;

            } else {
                $nilaic = "select count(VehicleType) from dbo.parkinglist where VehicleType = 'C' and LocationOut = '$za[0]' and timein between '$from' and '$tglh'";
                //echo $nilaic;
                $procnilaic = sqlsrv_query($conn, $nilaic);
                $nilai1 = sqlsrv_fetch_array( $procnilaic, SQLSRV_FETCH_NUMERIC);
                                
                $nilaib = "select count(VehicleType) from dbo.parkinglist where VehicleType = 'B' and LocationOut = '$za[0]' and timein between '$from' and '$tglh'";
                $procnilaib = sqlsrv_query($conn, $nilaib);
                $nilai2 = sqlsrv_fetch_array( $procnilaib, SQLSRV_FETCH_NUMERIC);

                $nilait = "select count(VehicleType) from dbo.parkinglist where VehicleType = 'T' and LocationOut = '$za[0]' and timein between '$from' and '$tglh'";
                $procnilait = sqlsrv_query($conn, $nilait);
                $nilai3 = sqlsrv_fetch_array( $procnilait, SQLSRV_FETCH_NUMERIC);

                $nilaiv = "select count(VehicleType) from dbo.parkinglist where VehicleType = 'V' and LocationOut = '$za[0]' and timein between '$from' and '$tglh'";
                $procnilaiv = sqlsrv_query($conn, $nilaiv);
                $nilai4 = sqlsrv_fetch_array( $procnilaiv, SQLSRV_FETCH_NUMERIC);


                $nilaip = "select count(VehicleType) from dbo.parkinglist where VehicleType = 'P' and LocationOut = '$za[0]' and timein between '$from' and '$tglh'";
                $procnilaip = sqlsrv_query($conn, $nilaip);
                $nilai5 = sqlsrv_fetch_array( $procnilaip, SQLSRV_FETCH_NUMERIC);


                $nilaix = "select count(VehicleType) from dbo.parkinglist where VehicleType NOT IN('C', 'B', 'T', 'V', 'P') and LocationOut = '$za[0]' and timein between '$from' and '$tglh'";
                //echo $nilaix;
                $procnilaix = sqlsrv_query($conn, $nilaix);
                $nilai6 = sqlsrv_fetch_array( $procnilaix, SQLSRV_FETCH_NUMERIC);


            }
        }


               
        $objWorkSheet->SetCellValueExplicit("A".$row,$za[12]);
        $objWorkSheet->SetCellValueExplicit("B".$row,$za[0].' - '.$za[2]);
        $objWorkSheet->SetCellValueExplicit("C".$row, number_format($nilai1[0]), PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet->SetCellValueExplicit("D".$row, number_format($nilai2[0]), PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet->SetCellValueExplicit("E".$row, number_format($nilai3[0]), PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet->SetCellValueExplicit("F".$row, number_format($nilai4[0]), PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet->SetCellValueExplicit("G".$row, number_format($nilai5[0]), PHPExcel_Cell_DataType::TYPE_STRING);
        $objWorkSheet->SetCellValueExplicit("H".$row, number_format($nilai6[0]), PHPExcel_Cell_DataType::TYPE_STRING);
        $row++;
        $rw++;
        }
 }
//exit;
    $objWorkSheet->setTitle('Drep Laporan Jumlah Kendaraan');

    $fileName = 'LapJmlkend'.'_'.strtotime(date('Y-m-d H:i:s')).'.xls';
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');

// download ke client
    header('Content-type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename="'.$fileName.'"');
    $objWriter->save('php://output');

    return $filePath.'/'.$fileName;

?>
