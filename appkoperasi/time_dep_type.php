<?php require('header.php');?>

<?php require('sidebar-right.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<?php
//edit
$uledit = $_GET['edit'];
$ul0    = "";
$ul1    = "";
$ul2    = "";
$ul3    = "";
$ul4    = "";
$ul5    = "";
$ul6    = "";
$ul7    = "";
$ulprocedit = "";
$uldisabled = "";
$uldisabled2 = "";
if(!empty($uledit)){
    $eulsql   = "select * from dbo.TimeDepositType where KodeTimeDepositType='$uledit'";
    $eulstmt  = sqlsrv_query($conn, $eulsql);
    $eulrow   = sqlsrv_fetch_array( $eulstmt, SQLSRV_FETCH_NUMERIC);
    if(count($eulrow[0]) > 0){
        $ul0    = $eulrow[0];
        $ul1    = $eulrow[1];
        $ul2    = $eulrow[2];
        $ul3    = $eulrow[3];
        $ul4    = substr($eulrow[4], 0, -5);
        $ul5    = substr($eulrow[5],0,-5);
        $ul6    = $eulrow[6];
        $ul7    = $eulrow[7];
        $ulprocedit = "?page=".$_GET['page']."&edit=".$ul0;
        $uldisabled = "disabled";
        $uldisabled2 = "readonly";
    }
    else{
        if(empty($_GET['page'])){
            echo "<script language='javascript'>document.location='time_dep_type.php';</script>";
        }
        else{
            echo "<script language='javascript'>document.location='time_dep_type.php?page=".$_GET['page']."';</script>";
        }
    }
}

?>

<div class="animated fadeinup delay-1">
    <div class="page-content">

        <?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
            <div class="notification notification-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
                <!--<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> <i class="icon fa fa-warning ?>"></i>-->
                <h4> <?php echo ucfirst($_SESSION['error-type']); ?> </h4>
                <?php echo $_SESSION['error-message']; ?>
            </div>
        <?php } ?>

        <h2 class="uppercase"><?php echo lang('Tipe deposit'); ?></h2>

            <form class="form-horizontal" action="proctime_dep_type.php<?=$ulprocedit?>" method = "POST">

                <div class="input-field">
                    <input type="text" name="dsc" class="validate" id="regularsavingdescription_dsc" value="<?=$ul1?>">
                    <label for="regularsavingdescription_dsc"><?php echo lang('Deskripsi'); ?></label>
                </div>

                <div class="input-field">
                    <input type="text" name="inter" class="validate" id="regularsavingdescription_inter" value="<?=$ul2?>">
                    <label for="regularsavingdescription_inter"><?php echo lang('Suku bunga'); ?> %</label>
                </div>

                <div class="input-field">
                    <input type="text" name="cp" class="validate" id="regularsavingdescription_cp" value="<?=$ul3?>">
                    <label for="regularsavingdescription_cp"><?php echo lang('Masa kontrak'); ?> (<?php echo lang('Bulan'); ?>)</label>
                </div>

                <div class="input-field">
                    <input type="text" name="ma" class="validate price" id="regularsavingdescription_ma" value="<?=$ul4?>">
                    <label for="regularsavingdescription_ma"><?php echo lang('Minimum jumlah'); ?></label>
                </div>

                <div class="input-field">
                    <input type="text" name="fp" class="validate price" id="regularsavingdescription_fp" value="<?=$ul5?>">
                    <label for="regularsavingdescription_fp"><?php echo lang('Biaya penalti'); ?></label>
                </div>

                <div class="input-field">
                    <input type="text" name="ip" class="validate" id="regularsavingdescription_ip" value="<?=$ul6?>">
                    <label for="regularsavingdescription_ip"><?php echo lang('Bunga penalti'); ?> (%)</label>
                </div><br>

                <label><?php echo lang('Kba'); ?></label>
                <select name="kba" class="browser-default" <?=$uldisabled2?>>
                    <option value="">- <?php echo lang('Pilih salah satu'); ?> -</option>
                    <?php
                    $julsql   = "select * from [dbo].[BankAccountView]";
                    $julstmt = sqlsrv_query($conn, $julsql);
                    while($rjulrow  = sqlsrv_fetch_array( $julstmt, SQLSRV_FETCH_NUMERIC)){
                        ?>
                        <option value="<?=$rjulrow[1];?>" <?php if($rjulrow[1]==$ul5){echo "selected";} ?>>&nbsp;<?=$rjulrow[4];?>&nbsp;<?=$rjulrow[3];?></option>
                    <?php } ?>
                </select>

                <div style="margin-top: 30px;">
                    <?php if(count($eulrow[0]) > 0){ ?>
                        <button type="submit" class="waves-effect waves-light btn-large primary-color width-100"><?php echo lang('Perbaharui'); ?></button>
                    <?php } else { ?>
                        <button type="submit" class="waves-effect waves-light btn-large primary-color width-100"><?php echo lang('Simpan'); ?></button>
                    <?php } ?>
                </div>

                <div style="margin-top: 10px; margin-bottom: 30px;">
                    <?php if(count($eulrow[0]) > 0){ ?>
                        <a href="time_dep_type.php" class="waves-effect waves-light btn-large primary-color width-100">Batal</a>
                    <?php } ?>
                </div>

            </form>

        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title"><?php echo lang('Daftar tipe deposit'); ?></h3>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th><?php echo lang('No'); ?></th>
                            <th><?php echo lang('Kode tipe deposit'); ?></th>
                            <th><?php echo lang('Deskripsi'); ?></th>
                            <th><?php echo lang('Suku bunga'); ?> %</th>
                            <th><?php echo lang('Masa kontrak'); ?> (<?php echo lang('Bulan'); ?>)</th>
                            <th><?php echo lang('Minimum jumlah'); ?></th>
                            <th><?php echo lang('Biaya penalty'); ?></th>
                            <th><?php echo lang('Bunga penalty'); ?> (%)</th>
                            <th><?php echo lang('KBA'); ?></th>
                            <th><?php echo lang('Aksi'); ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        //count
                        $jmlulsql   = "select count(*) from dbo.TimeDepositType";
                        $jmlulstmt  = sqlsrv_query($conn, $jmlulsql);
                        $jmlulrow   = sqlsrv_fetch_array( $jmlulstmt, SQLSRV_FETCH_NUMERIC);

                        //pagging
                        $perpages   = 10;
                        $halaman    = $_GET['page'];
                        if(empty($halaman)){
                            $posisi  = 0;
                            $batas   = $perpages;
                            $halaman = 1;
                        }
                        else{
                            $posisi  = (($perpages * $halaman) - 10) + 1;
                            $batas   = ($perpages * $halaman);
                        }

                        $ulsql = "SELECT * FROM ( SELECT *, ROW_NUMBER() OVER (ORDER BY KodeTimeDepositType asc) as row FROM [dbo].[TimeDepositTypeView]) a WHERE row between '$posisi' and '$batas'";

                        //$ulsql = "select * from [dbo].[TimeDepositType]";

                        $ulstmt = sqlsrv_query($conn, $ulsql);

                        while($ulrow = sqlsrv_fetch_array( $ulstmt, SQLSRV_FETCH_NUMERIC)){
                            ?>
                            <tr>
                                <td><?=$ulrow[10];?></td>
                                <td><?=$ulrow[0];?>
                                <td><?=$ulrow[1];?></td>
                                <td><?=$ulrow[2];?></td>
                                <td><?=$ulrow[3];?></td>
                                <td><?=$ulrow[6];?></td>
                                <td><?=$ulrow[7];?></td>
                                <td><?=$ulrow[8];?></td>
                                <td><?=$ulrow[9];?></td>
                                <td width="20%" style="padding: 3px">
                                    <div class="btn-group" style="padding-right: 15px">
                                        <a href="time_dep_type.php?page=<?=$halaman?>&edit=<?=$ulrow[0]?>" class="btn btn-default btn-flat btn-sm text-green" title="edit"><i class="ion-android-create"></i></a>
                                    </div>
                                </td>
                            </tr>
                        <?php
                        }
                        $jmlhalaman = ceil($jmlulrow[0]/$perpages);
                        ?>
                        </tbody>
                    </table>
                </div>
                <div class="box-footer clearfix right">
                    <div style="text-align: center;">
                        <?php echo lang('Menampilkan'); ?> <?=$halaman?> to <?=$jmlhalaman?> of <?=$jmlulrow[0]?> <?php echo lang('Masukan'); ?> <?=$posisi." - ".$batas?>
                        <?php
                        $reload = "time_dep_type.php?";
                        $page = intval($_GET["page"]);
                        $adjacents = isset($_GET['adjacents']) ? intval($_GET['adjacents']) : 2; // khusus style pagination 2 dan 3
                        if( $page == 0 ) $page = 1;

                        $tpages = ($jmlhalaman) ? ceil($jmlhalaman) : 0; // total pages, last page number
                        echo "<div>".paginate($reload, $page, $tpages)."</div>";
                        ?>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<?php
function paginate($reload, $page, $tpages) {

    $firstlabel = "First";
    $prevlabel  = "Prev";
    $nextlabel  = "Next";
    $lastlabel  = "Last";

    $out = "<div class=\"backpaging\"><ul class=\"ownpaging\">\n";

    // first
    if($page>1) {
        $out.= "<li><a href=\"" . $reload . "\">" . $firstlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $firstlabel . "</a></li>\n";
    }

    // previous
    if($page==1) {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $prevlabel . "</a></li>\n";
    }
    elseif($page==2) {
        $out.= "<li><a href=\"" . $reload . "\">" . $prevlabel . "</a></li>\n";
    }
    else {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" . ($page-1) . "\">" . $prevlabel . "</a></li>\n";
    }

    // current
    $out.= "<li class=\"active\"><a href=\"#\">Page " . $page . " of " . $tpages . "</a></li>\n";

    // next
    if($page<$tpages) {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" .($page+1) . "\">" . $nextlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $nextlabel . "</a></li>\n";
    }

    // last
    if($page<$tpages) {
        $out.= "<li><a href=\"" . $reload . "&amp;page=" . $tpages . "\">" . $lastlabel . "</a></li>\n";
    }
    else {
        $out.= "<li class=\"disabledd\"><a href=\"#\">" . $lastlabel . "</a></li>\n";
    }

    $out.= "</ul></div>";

    return $out;
}

require('footer_new.php');
?>
