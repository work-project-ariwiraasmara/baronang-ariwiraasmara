<?php require('header.php');?>

<?php require('sidebar-right.php');?>

<?php require('sidebar-left.php');?>

<?php require('content-header.php');?>

<div class="animated fadeinup delay-1">
    <div class="page-content">



            <form class="form-horizontal" action="procsetting_parking.php" method = "POST">
                <?php
                $a = "select * from dbo.ParkingSetting";
                //echo $a;
                $b = sqlsrv_query($conn, $a);
                $c  = sqlsrv_fetch_array( $b, SQLSRV_FETCH_NUMERIC);
                ?>

                <div>
                    <label><?php echo ('1 Point = (Rp)'); ?></label>
                    <input type="number" name="amount1" class="validate" value="<?php echo round($c[1]);?>">
                </div>

                <div>
                    <label><?php echo ('Maksimal Top Up EDC (Rp)'); ?></label>
                    <input type="number" name="amount2" class="validate" value="<?php echo round($c[2]);?>">
                </div>

                <div>
                    <label><?php echo ('Maksimal Point User (Point)'); ?></label>
                    <input type="number" name="amount3" class="validate" value="<?php echo round($c[3]);?>">
                </div>



                <div style="margin-top: 30px;">
                    <button type="submit" class="waves-effect waves-light btn-large primary-color width-100"><?php echo lang('Simpan'); ?></button>
                </div>

            </form>
    </div>
</div>

<?php require('footer.php'); ?>
