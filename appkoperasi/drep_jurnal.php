<?php
session_start();
error_reporting(0);

include('lib/phpExcel/Classes/PHPExcel.php');
include "connect.php";


    $filePath = "uploads/excel/";

    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getProperties()->setCreator("baronang");
    $objPHPExcel->getProperties()->setTitle("Drep Neraca History");

// set autowidth
    for($col = 'A'; $col !== 'Z'; $col++) {
        $objPHPExcel->getActiveSheet()
            ->getColumnDimension($col)
            ->setAutoSize(true);
    }

    $listCombo = array();

    //sheet 2
    $objWorkSheet = $objPHPExcel->createSheet(0);
// baris judul
   
    $objWorkSheet->getStyle('C1')->getFont()->setBold(true);
    $objWorkSheet->getStyle('C1')->getFont()->setSize(14);
    $objWorkSheet->SetCellValue('C1', 'Laporan Buku Besar');

    $objWorkSheet->getStyle('C2')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('C2', 'Akun :');

    $objWorkSheet->getStyle('C3')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('C3', 'Periode  : ');


    $objWorkSheet->getStyle('A5')->getFont()->setBold(true);
    $objWorkSheet->getStyle('A5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->SetCellValue('A5', 'Kode Transaksi');
    $objWorkSheet->getStyle('B5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->getStyle('B5')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('B5', 'Kode Akun');
    $objWorkSheet->getStyle('C5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->getStyle('C5')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('C5', 'Tanggal Transaksi');
    $objWorkSheet->getStyle('D5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->getStyle('D5')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('D5', 'Deskripsi');
    $objWorkSheet->getStyle('E5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->getStyle('E5')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('E5', 'Debit');
    $objWorkSheet->getStyle('F5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->getStyle('F5')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('F5', 'Kredit');   
    $objWorkSheet->getStyle('G5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objWorkSheet->getStyle('G5')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('G5', 'Total'); 
    $objWorkSheet->getStyle('A6' , 'Saldo Awal')->getFont()->setBold(true);
    $objWorkSheet->SetCellValue('A6', 'Saldo Awal'); 



    if($_GET['st'] == 5 ){


    $k = 2;
    $x = "select * from dbo.AccountView where KodeAccount = '$_GET[acc3]'";
    //echo $x;
    $y = sqlsrv_query($conn, $x);
    while($z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC)){

        $objWorkSheet->getStyle('D'.$k)->getFont()->setBold(true);
        $objWorkSheet->SetCellValue("D".$k,$z[1]);

    }




    $no = 1;
    $bln = 3 ;
    $bulan = date('m', strtotime($_GET['from']));
    $tahun = date('Y', strtotime($_GET['from']));
        if ($bulan != 1) {
            if ($bulan != 2 ) {
                if ($bulan != 3) {
                    if ($bulan != 4) {
                        if ($bulan !=5) {
                            if ($bulan !=6) {
                                if ($bulan !=7) {
                                    if ($bulan !=8) {
                                        if ($bulan !=9) {
                                            if ($bulan !=10) {
                                                if ($bulan !=11) {
                                                    $bulan = 'Desember';
                                                } else {
                                                $bulan = 'Nopember';
                                                }
                                            } else {
                                            $bulan = 'OKtober';   
                                            }
                                        } else {
                                        $bulan = 'September';
                                        }
                                    } else {
                                    $bulan = 'Agustus';
                                    }
                                } else {
                                $bulan = 'Juli';
                                }
                            } else {
                            $bulan = 'Juni';
                            }
                        } else {
                        $bulan = 'Mei';  
                        }
                    } else {
                    $bulan = 'April';       
                    }
                } else {
                $bulan = 'Maret';
                }
            } else {
            $bulan = 'Februari';    
            }                               
        } else {
        $bulan = 'Januari';
        }

        $objWorkSheet->getStyle('D'.$bln)->getFont()->setBold(true);
        $objWorkSheet->SetCellValue("D".$bln,$bulan .'-'. $tahun);        


    
    $saldo = 6;

    $from1= date('Y-m-d', strtotime('-1 month', strtotime($_GET['from'])));
    $to = date('Y-m-t', strtotime($from1));
    //echo $to;
    $a = "select * from dbo.AccountBalance where KodeAccount ='$_GET[acc3]' and Header = '5'and Tanggal = '$to'";
    //echo $a;
    $b = sqlsrv_query($conn, $a);
    $total13=0;
    //$btotal = 0;
    $c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC);
    if($c == null){
        $total13 = $total13;
            } else {
        $total13 = $c[10]+$total13;
    }

    $objWorkSheet->getStyle('G' .$saldo)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
    $objWorkSheet->getStyle('G'.$saldo, number_format($total13))->getFont()->setBold(true);
    $objWorkSheet->SetCellValue("G".$saldo, number_format($total13), PHPExcel_Cell_DataType::TYPE_STRING);







    $row = 7;
    $rw = 7;
    $total=$total13;
    $x = "SELECT * FROM ( SELECT a.TransNo,a.AccountDebet,a.AccountKredit,a.Debet,a.Kredit,a.Amount,Date,a.TransactionType,a.RefNumber,a.Note,a.Message,a.UserID,a.ApprovedBy,a.TransID,c.KodeGroup,c.status, ROW_NUMBER() OVER (ORDER BY a.Date asc) as row FROM [dbo].[TransList] a inner join[dbo].[GroupAcc] c on SUBSTRING('$_GET[acc3]',1,1)= SUBSTRING(c.KodeGroup,1,1) where a.TransNo is not null and a.Debet = '$_GET[acc3]' and a.Date between '$_GET[from]' and '$_GET[to]' or a.Kredit = '$_GET[acc3]' and a.Date between '$_GET[from]' and '$_GET[to]') a";
    //echo $x;
    $y = sqlsrv_query($conn, $x);
    while($z = sqlsrv_fetch_array( $y, SQLSRV_FETCH_NUMERIC)){
             if($z[15] == 0) {
            if($z[3] != $_GET['acc3'] ){
                $total-=$z[5];
                $ctotal=$total;               
            }else{
                $total+=$z[5];
                $ctotal=$total; 
            }
                            
        } else if ($z[15] == 1) {
        //var_dump($c);
        if($z[3] == $_GET['acc3'] ){
            $total+=$z[5];
            $ctotal=$total;               
        }else{
            $total-=$c[5];
            $ctotal=$total;
                                
            }
        }
        //echo $ctotal;
        //Akun
        if ($_GET['acc3'] == $z[3]){
                $akunr = $z[1];
                } else {
                $akunr = $z[2];
            }

        //debet
        if ($_GET['acc3'] == $z[3]){
            $amountd = $z[5];
                } else {
            $amountd = 0;
            } 

        //Kredit
        if ($_GET['acc3'] == $z[4]){
            $amountr = $z[5];
                } else {
            $amountr = 0;
            }        
               

                $objWorkSheet->SetCellValueExplicit("A".$row,$z[0]);     
                $objWorkSheet->SetCellValueExplicit("B".$row,$akunr);
                $objWorkSheet->SetCellValue("C".$row, $z[6]->format('Y-m-d H:i:s'));
                $objWorkSheet->SetCellValue("D".$row,$z[9]);
                $objWorkSheet->getStyle('E' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                $objWorkSheet->SetCellValue("E".$row, number_format($amountd), PHPExcel_Cell_DataType::TYPE_STRING);
                $objWorkSheet->getStyle('F' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                $objWorkSheet->SetCellValue("F".$row, number_format($amountr), PHPExcel_Cell_DataType::TYPE_STRING);
                $objWorkSheet->getStyle('G' .$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                $objWorkSheet->SetCellValue("G".$row, number_format($total), PHPExcel_Cell_DataType::TYPE_STRING);
                $no++;
                $row++;
                $rw++;
        }

         $a = "SELECT * FROM ( SELECT a.TransNo,a.AccountDebet,a.AccountKredit,a.Debet,a.Kredit,a.Amount,Date,a.TransactionType,a.RefNumber,a.Note,a.Message,a.UserID,a.ApprovedBy,a.TransID,c.KodeGroup,c.status, ROW_NUMBER() OVER (ORDER BY a.Date asc) as row FROM [dbo].[TransList] a inner join[dbo].[GroupAcc] c on SUBSTRING('$_GET[acc3]',1,1)= SUBSTRING(c.KodeGroup,1,1) where a.TransNo is not null and a.Debet = '$_GET[acc3]' and a.Date between '$_GET[from]' and '$_GET[to]' or a.Kredit = '$_GET[acc3]' and a.Date between '$_GET[from]' and '$_GET[to]') a";
                          //echo $a;
                        $b = sqlsrv_query($conn, $a);
                        $c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC);
                            if($c[15] == 0) {
                                $amountdebit = "SELECT sum (amount) FROM ( SELECT a.TransNo,a.AccountDebet,a.AccountKredit,a.Debet,a.Kredit,a.Amount,Date,a.TransactionType,a.RefNumber,a.Note,a.Message,a.UserID,a.ApprovedBy,a.TransID,c.KodeGroup,c.status, ROW_NUMBER() OVER (ORDER BY a.Date asc) as row FROM [dbo].[TransList] a inner join[dbo].[GroupAcc] c on SUBSTRING('$_GET[acc3]',1,1)= SUBSTRING(c.KodeGroup,1,1) where a.TransNo is not null and a.Kredit = '$_GET[acc3]' and a.Date between '$_GET[from]' and '$_GET[to]') a";
                                //echo $amountdebit;
                                $prosesdebit = sqlsrv_query($conn, $amountdebit);
                                $hasilDebit1 = sqlsrv_fetch_array($prosesdebit, SQLSRV_FETCH_NUMERIC); 
                                $hasilDebit = $hasilDebit1[0];

                                $amountKredit = "SELECT sum (amount) FROM ( SELECT a.TransNo,a.AccountDebet,a.AccountKredit,a.Debet,a.Kredit,a.Amount,Date,a.TransactionType,a.RefNumber,a.Note,a.Message,a.UserID,a.ApprovedBy,a.TransID,c.KodeGroup,c.status, ROW_NUMBER() OVER (ORDER BY a.Date asc) as row FROM [dbo].[TransList] a inner join[dbo].[GroupAcc] c on SUBSTRING('$_GET[acc3]',1,1)= SUBSTRING(c.KodeGroup,1,1) where a.TransNo is not null and a.Debet = '$_GET[acc3]' and a.Date between '$_GET[from]' and '$_GET[to]') a";
                                //echo $amountKredit;
                                $prosesKredit = sqlsrv_query($conn, $amountKredit);
                                $hasilKredit1 = sqlsrv_fetch_array($prosesKredit, SQLSRV_FETCH_NUMERIC); 
                                $hasilKredit = $hasilKredit1[0];

                                } else if ($c[15] == 1) {
                                $amountdebit = "SELECT sum (amount) FROM ( SELECT a.TransNo,a.AccountDebet,a.AccountKredit,a.Debet,a.Kredit,a.Amount,Date,a.TransactionType,a.RefNumber,a.Note,a.Message,a.UserID,a.ApprovedBy,a.TransID,c.KodeGroup,c.status, ROW_NUMBER() OVER (ORDER BY a.Date asc) as row FROM [dbo].[TransList] a inner join[dbo].[GroupAcc] c on SUBSTRING('$_GET[acc3]',1,1)= SUBSTRING(c.KodeGroup,1,1) where a.TransNo is not null and a.Kredit = '$_GET[acc3]' and a.Date between '$_GET[from]' and '$_GET[to]') a";
                                //echo $amountdebit;
                                $prosesdebit = sqlsrv_query($conn, $amountdebit);
                                $hasilDebit1 = sqlsrv_fetch_array($prosesdebit, SQLSRV_FETCH_NUMERIC); 
                                $hasilDebit = $hasilDebit1[0];

                                $amountKredit = "SELECT sum (amount) FROM ( SELECT a.TransNo,a.AccountDebet,a.AccountKredit,a.Debet,a.Kredit,a.Amount,Date,a.TransactionType,a.RefNumber,a.Note,a.Message,a.UserID,a.ApprovedBy,a.TransID,c.KodeGroup,c.status, ROW_NUMBER() OVER (ORDER BY a.Date asc) as row FROM [dbo].[TransList] a inner join[dbo].[GroupAcc] c on SUBSTRING('$_GET[acc3]',1,1)= SUBSTRING(c.KodeGroup,1,1) where a.TransNo is not null and a.Debet = '$_GET[acc3]' and a.Date between '$_GET[from]' and '$_GET[to]') a";
                                //echo $amountKredit;
                                $prosesKredit = sqlsrv_query($conn, $amountKredit);
                                $hasilKredit1 = sqlsrv_fetch_array($prosesKredit, SQLSRV_FETCH_NUMERIC); 
                                $hasilKredit = $hasilKredit1[0];
                                
                            }

            $objWorkSheet->getStyle('A5:G' .$rw)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objWorkSheet->getStyle('A'.$rw,'Saldo Akhir')->getFont()->setBold(true);
            $objWorkSheet->SetCellValue('A'.$rw,'Saldo Akhir');
            $objWorkSheet->getStyle('E'.$rw, number_format($hasilKredit))->getFont()->setBold(true);
            $objWorkSheet->SetCellValue("E".$rw, number_format($hasilKredit), PHPExcel_Cell_DataType::TYPE_STRING);
            $objWorkSheet->getStyle('F'.$rw, number_format($hasilDebit))->getFont()->setBold(true);
            $objWorkSheet->SetCellValue("F".$rw, number_format($hasilDebit), PHPExcel_Cell_DataType::TYPE_STRING);
            $objWorkSheet->getStyle('G'.$rw, number_format($ctotal))->getFont()->setBold(true);
            $objWorkSheet->SetCellValue("G".$rw, number_format($ctotal), PHPExcel_Cell_DataType::TYPE_STRING);
        }
 
//exit;
    $objWorkSheet->setTitle('Drep Neraca History');

    $fileName = 'NeracaHIS'.'_'.strtotime(date('Y-m-d H:i:s')).'.xls';
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');

// download ke client
    header('Content-type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename="'.$fileName.'"');
    $objWriter->save('php://output');

    return $filePath.'/'.$fileName;

?>
