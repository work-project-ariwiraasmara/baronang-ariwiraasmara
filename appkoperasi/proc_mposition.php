<?php
session_start();
error_reporting(0);

include "connect.php";

$page			= $_GET['page'];
$edit			= $_GET['edit'];
$delete			= $_GET['delete'];

$name			= $_POST['name'];
$description	= $_POST['description'];
$pokok			= $_POST['pokok'];
$wajib			= $_POST['wajib'];
$sukarela		= $_POST['sukarela'];

if(isset($_POST['belanja'])){
    $belanja = str_replace(',', '', $_POST['belanja']);
}

if(isset($_POST['pinjaman'])){
    $pinjaman = str_replace(',', '', $_POST['pinjaman']);
}

if(isset($_POST['gabungan'])){
    $b = str_replace(',', '', $_POST['gabungan']) / 2;
    $belanja = $b;
    $pinjaman = $b;
}

if(!empty($delete)){
	$dpultsql = "delete from [dbo].[JabatanDetail] where KodeJabatan='$delete'";
	$dpulstmt = sqlsrv_query($conn, $dpultsql);
	if($dpulstmt){
        messageAlert(lang('Berhasil menghapus data dari database'),'success');
        header('Location: mposition.php');
	}
	else{
        messageAlert(lang('Gagal menghapus data dari database'),'danger');
        header('Location: mposition.php');
	}
}
else{
	if($name == "" || $description == "" || $belanja == "" || $pinjaman == "" || $pokok == "" || $wajib == "" || $sukarela == ""){
        messageAlert(lang('Harap isi seluruh kolom'),'info');
        header('Location: mposition.php');
	}
	else{
		if(!empty($edit)){
            $ppdsql = "exec [dbo].[ProsesJabatanDetail] '$edit', '$name', '$description', '$belanja', '$pinjaman', '$wajib', '$pokok', '$sukarela'";
            $ppdstmt = sqlsrv_query($conn, $ppdsql);
            if($ppdstmt){
                messageAlert(lang('Berhasil memperbaharui data ke database'),'success');
                header('Location: mposition.php');
            }
            else{
                messageAlert(lang('Gagal memperbaharui data ke database'),'danger');
                header('Location: mposition.php');
            }
		}
		else{
            //kode position
            $a = "select [dbo].[getKodePosition]()";
            $b = sqlsrv_query($conn, $a);
            $c = sqlsrv_fetch_array( $b, SQLSRV_FETCH_NUMERIC);
            $code = $c[0];

            $ppdsql = "exec [dbo].[ProsesJabatanDetail] '$code', '$name', '$description', '$belanja', '$pinjaman', '$wajib', '$pokok', '$sukarela'";
            $ppdstmt = sqlsrv_query($conn, $ppdsql);
            if($ppdstmt){
                messageAlert(lang('Berhasil menyimpan ke database'),'success');
                header('Location: mposition.php');
            }
            else{
                messageAlert(lang('Gagal menyimpan ke database'),'danger');
                header('Location: mposition.php');
            }
		}
	}
}
?>