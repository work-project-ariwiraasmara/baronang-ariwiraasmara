<?php
session_start();
error_reporting(0);
include "connect.php";
require("lib/PHPMailer_5.2.0/class.phpmailer.php");

$page = $_GET['page'];
$edit = $_GET['edit'];
$delete = $_GET['delete'];

//resend email
if(isset($_GET['resend']) and isset($_GET['member']) and isset($_GET['kod']) and isset($_GET['produk'])){
    $a = "select * from [dbo].[MemberList] where MemberID='$_GET[member]'";
    $b = sqlsrv_query($conn, $a);
    $c = sqlsrv_fetch_array( $b, SQLSRV_FETCH_NUMERIC);

    $aa = "select * from [dbo].[LoanPenjaminView] where email='$_GET[resend]' and LoanAppNum='$_GET[kod]'";
    $bb = sqlsrv_query($conn, $aa);
    $cc = sqlsrv_fetch_array( $bb, SQLSRV_FETCH_NUMERIC);

    $root = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/';
    $linky = $root."koperasi/confmail.php?c=".md5('y')."&p=$_GET[kod]&m=$_GET[member]";
    $linkn = $root."koperasi/confmail.php?c=".md5('n')."&p=$_GET[kod]&m=$_GET[member]";

    $mail = new PHPMailer();
    $mail->IsSMTP();
    $mail->Host = "baronang.com";  // specify main and backup server
    $mail->SMTPAuth = true;     // turn on SMTP authentication
    $mail->Username = "konfirmasi@baronang.com";  // SMTP username
    $mail->Password = "alva7000"; // SMTP password
    $mail->Port = 587;
    $mail->SMTPDebug = 1;

    $mail->From = "konfirmasi@baronang.com";
    $mail->FromName = "Baronang Mail Service";
    $mail->AddAddress($_GET['resend']);                  // name is optional
    $mail->AddReplyTo("konfirmasi@baronang.com", "Baronang Konfirmasi");
    $mail->IsHTML(true);// set email format to HTML

    $mail->Subject = "Konfirmasi Penjamin";
    $mail->Body =
        "Hi! ".$cc[2].",<br><br>
            Anda telah dipilih untuk menjadi penjamin oleh '".$c[2]."' dengan kode member ".$c[1].". Klik link dibawah ini untuk mengkonfirmasi:<br>
            <br>
            ".$linky."
            <br><br>
            Atau jika ada tidak berkenan untuk menjadi penjamin bisa dengan mengklik link dibawah ini:<br>
            <br>
            ".$linkn."
            <br>
            <br>
            <br>
            <br>
            Terima kasih.<br>
            Salam,<br>
            <br>
            <br>
            Baronang";
    $statuskirim = 0;
    if(!$mail->Send())
    {
        echo "Message could not be sent.";
        echo "Mailer Error: " . $mail->ErrorInfo;
    }
    else{
        $statuskirim = 1;
    }

    $ppsql = "exec dbo.ProsesLoanPenjamin '$_GET[kod]','$cc[1]','','','$statuskirim'";
    $ppstmt = sqlsrv_query($conn, $ppsql);
    sqlsrv_execute($ppstmt);

    echo "<script language='javascript'>alert('Berhasil resend email.');document.location='csoaloanapp.php?memberid=".$_GET['member']."&loantype=".$_GET['produk']."';</script>";

}

if(isset($_POST['member']) and isset($_POST['produk']) and isset($_POST['kod']) and isset($_POST['amount'])){
    $member = $_POST['member'];
    $nama = $_POST['nama'];
    $produk = $_POST['produk'];
    $kod = $_POST['kod'];
    $amount = $_POST['amount'];

    $a = "select * from [dbo].[MemberList] where MemberID='$member'";
    $b = sqlsrv_query($conn, $a);
    $c = sqlsrv_fetch_array( $b, SQLSRV_FETCH_NUMERIC);

    //config
    $x = "select * from [dbo].[GeneralSetingConfig] where KID='$_SESSION[KID]' and LoanType='$produk'";
    $y = sqlsrv_query($conn, $x);
    $z = sqlsrv_fetch_array( $y, SQLSRV_FETCH_NUMERIC);

    if(isset($_FILES['filename'])){
        $uploads_dir = 'uploads/';
        foreach ($_FILES["filename"]["error"] as $key => $error) {
            $tmp_name = $_FILES["filename"]["tmp_name"][$key];
            $path = $uploads_dir . basename($_FILES["filename"]["name"][$key]);

            $namedoc = $_POST['namedoc'][$key];

            if(move_uploaded_file($tmp_name, $path)){
                $sql = "exec dbo.ProsesMemberListDocUpload '$member','".$namedoc."','".$path."'";
                $stmt = sqlsrv_query($conn, $sql);
                sqlsrv_execute($stmt);
            }
            else{
                echo "<script language='javascript'>alert('Gagal upload file');document.location='csoaloanapp.php';</script>";
            }
        }
    }

    if(isset($_POST['memberID'])){
        foreach($_POST['memberID'] as $m){
            //send email
            $sqll = "select * from [dbo].[MemberList] where MemberID='$m'";
            $stmtt = sqlsrv_query($conn, $sqll);
            $data = sqlsrv_fetch_array( $stmtt, SQLSRV_FETCH_NUMERIC);

            $root = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/';
            $linky = $root."koperasi/confmail.php?c=".md5('y')."&p=$kod&m=$m";
            $linkn = $root."koperasi/confmail.php?c=".md5('n')."&p=$kod&m=$m";

            $mail = new PHPMailer();
            $mail->IsSMTP();
            $mail->Host = "baronang.com";  // specify main and backup server
            $mail->SMTPAuth = true;     // turn on SMTP authentication
            $mail->Username = "konfirmasi@baronang.com";  // SMTP username
            $mail->Password = "alva7000"; // SMTP password
            $mail->Port = 587;
            $mail->SMTPDebug = 1;

            $mail->From = "konfirmasi@baronang.com";
            $mail->FromName = "Baronang Mail Service";
            $mail->AddAddress($data[5]);                  // name is optional
            $mail->AddReplyTo("konfirmasi@baronang.com", "Baronang Konfirmasi");
            $mail->IsHTML(true);// set email format to HTML

            $mail->Subject = "Konfirmasi Penjamin";
            $mail->Body =
                "Hi! ".$data[2].",<br><br>
            Anda telah dipilih untuk menjadi penjamin oleh '".$c[2]."' dengan kode member ".$c[1].". Klik link dibawah ini untuk mengkonfirmasi:<br>
            <br>
            ".$linky."
            <br><br>
            Atau jika ada tidak berkenan untuk menjadi penjamin bisa dengan mengklik link dibawah ini:<br>
            <br>
            ".$linkn."
            <br>
            <br>
            <br>
            <br>
            Terima kasih.<br>
            Salam,<br>
            <br>
            <br>
            Baronang";

            $statuskirim = 0;
            if(!$mail->Send())
            {
                echo "Message could not be sent.";
                echo "Mailer Error: " . $mail->ErrorInfo;
            }
            else{
                $statuskirim = 1;
            }

            $ppsql = "exec dbo.ProsesLoanPenjamin '$kod','$m',0,'','$statuskirim'";
            $ppstmt = sqlsrv_query($conn, $ppsql);
            sqlsrv_execute($ppstmt);
        }

        $sqll = "select * from [dbo].[MemberListDocUpload] where MemberID='$member'";
        $stmtt = sqlsrv_query($conn, $sqll);
        while($data = sqlsrv_fetch_array( $stmtt, SQLSRV_FETCH_NUMERIC)){
            $sql = "exec dbo.ProsesLoanDocUpload '$kod','".$data[1]."','".$data[2]."',''";
            $stmt = sqlsrv_query($conn, $sql);
            sqlsrv_execute($stmt);
        }
    }

    $status = 0;
    //cek dokumen upload
    $noo = 0;
    $p = "select * from [dbo].[LoanDocUpload] where LoanAppNum='$kod'";
    $l = sqlsrv_query($conn, $p);
    while($m = sqlsrv_fetch_array( $l, SQLSRV_FETCH_NUMERIC)){
        $noo++;
    }

    //cek penjamin
    $nooo = 0;
    $pp = "select * from [dbo].[LoanPenjamin] where LoanAppNum='$kod'";
    $ll = sqlsrv_query($conn, $pp);
    while($mm = sqlsrv_fetch_array( $ll, SQLSRV_FETCH_NUMERIC)){
        $nooo++;
    }

    if($noo == $z[5] and $nooo == $z[3]){
        $status = 1;
    }

    $ss = "exec dbo.ProsesLoanApplicationList '$member','$kod','$produk','$amount','$_SESSION[UserID]','$status'";
    $pp = sqlsrv_query($conn, $ss);
    sqlsrv_execute($pp);

    echo "<script language='javascript'>alert('Berhasil! Pinjaman anda akan segera diproses lebih lanjut,');document.location='csoaloanapp.php';</script>";
}

if(isset($_GET['memberid']) and isset($_GET['loantype']) and isset($_GET['amount']) and isset($_GET['delete'])){
    $member = $_GET['memberid'];
    $type = $_GET['loantype'];
    $amount = $_GET['amount'];
    $kod = $_GET['delete'];

    $ppsql = "delete from dbo.MemberListDocUpload where MemberID = '$member' and NameDoc = '$delete'";
    $ppstmt = sqlsrv_query($conn, $ppsql);
    sqlsrv_execute($ppstmt);

    echo "<script language='javascript'>document.location='csoaloanapp.php?memberid=$member&loantype=$type&amount=$amount';</script>";

}
?>