<?php
/**
 *
 */
class CronController extends Controller
{
    public function actionDailyBalanceByDate($date, $kid){
        $response = array(
            'status'=>FALSE,
            'message'=>'',
            'errorCode'=>'',
        );

        $tanggal = date($date, strtotime('-1 day'));

        $sql = "select * from KoneksiKoperasiBaronang.dbo.ListKoperasi where KID='".$kid."'";
        $datas =  Yii::app()->db->createCommand($sql)->queryAll();
        foreach($datas as $data){
            $s = "exec ".$data['DatabaseName'].".dbo.ProsesCalculateDailyBalance '".$tanggal."'";
            $e = Yii::app()->db->createCommand($s);
            if($e->execute()){
                $response['status'] = true;
                $response['message'] = 'Cron daily balance berhasil dijalankan '.$data['KID'];
            }
            else{
                $response['message'] = 'Cron daily balance gagal dijalankan '.$data['KID'];
                $response['errorCode'] = '(erX4522)';
            }
        }

        echo json_encode($response);
    }

    public function actionDailyBalance($kid = null){
        $response = array(
            'status'=>FALSE,
            'message'=>'',
            'errorCode'=>'',
        );

        $tanggal = date('Y-m-d', strtotime('-1 day'));

        if($kid != null){
          $sql = "select * from KoneksiKoperasiBaronang.dbo.ListKoperasi where KID='".$kid."'";
        }
        else{
          $sql = "select * from KoneksiKoperasiBaronang.dbo.ListKoperasi where Status = 1";
        }
        $datas =  Yii::app()->db->createCommand($sql)->queryAll();
        foreach($datas as $data){
            $s = "exec ".$data['DatabaseName'].".dbo.ProsesCalculateDailyBalance '".$tanggal."'";
            $e = Yii::app()->db->createCommand($s);
            if($e->execute()){
                $response['status'] = true;
                $response['message'] = 'Cron daily balance berhasil dijalankan '.$data['KID'];
            }
            else{
                $response['message'] = 'Cron daily balance gagal dijalankan '.$data['KID'];
                $response['errorCode'] = '(erX4522)';
            }
        }

        echo json_encode($response);
    }

    public function actionDailyInterest($kid = null){
      $response = array(
          'status'=>FALSE,
          'messageRS'=>'',
          'messageTD'=>'',
          'errorCode'=>'',
      );

      if($kid != null){
        $sql = "select * from KoneksiKoperasiBaronang.dbo.ListKoperasi where KID='".$kid."'";
      }
      else{
        $sql = "select * from KoneksiKoperasiBaronang.dbo.ListKoperasi where Status = 1";
      }
      $datas =  Yii::app()->db->createCommand($sql)->queryAll();
      foreach($datas as $data){

        $sql3 = "select * from ".$data['DatabaseName'].".dbo.TimeDepositAccount";
        $datas3 =  Yii::app()->db->createCommand($sql3)->queryAll();
        foreach($datas3 as $data3){
          $s = "exec ".$data['DatabaseName'].".dbo.ProsesCalculateInterestTimeDeposit '".$data3['AccountNumber']."'";
          $e = Yii::app()->db->createCommand($s);
          if($e->execute()){
              $response['status'] = true;
              $response['messageTD'] = 'Cron berhasil dijalankan '.$data['KID'];
          }
          else{
              $response['messageTD'] = 'Cron TD harian gagal dijalankan '.$data3['AccountNumber'];
              $response['errorCode'] = '(erX00001)';
          }
        }

        $sql2 = "select * from ".$data['DatabaseName'].".dbo.RegularSavingAcc";
        $datas2 =  Yii::app()->db->createCommand($sql2)->queryAll();
        foreach($datas2 as $data2){
          $tanggal = date('Y-m-d');
          //bunga harian
          $sql = "exec ".$data['DatabaseName'].".dbo.ProsesCalculateInterestDaily '".$data2['AccountNo']."'";
          $exec = Yii::app()->db->createCommand($sql);
          if($exec->execute()){
              //bunga bulanan
              $sql2 = "exec ".$data['DatabaseName'].".dbo.ProsesCalculateInterestMonthly '".$data2['AccountNo']."'";
              $exec2 = Yii::app()->db->createCommand($sql2);
              if($exec2->execute()){
                  $response['status'] = true;
                  $response['messageRS'] = 'Cron berhasil dijalankan '.$tanggal;
              }
              else{
                  $response['messageRS'] = 'Cron RS bulanan gagal dijalankan '.$data['KID'];
                  $response['errorCode'] = '(erX00023)';
              }
          }
          else{
              $response['messageRS'] = 'Cron RS harian gagal dijalankan '.$data2['AccountNo'];
              $response['errorCode'] = '(erX00001)';
          }
        }
      }

      echo json_encode($response);
    }

    public function actionLoanBilling($kid = null){
      $response = array(
          'status'=>FALSE,
          'message'=>'',
          'errorCode'=>'',
      );

      if($kid != null){
        $sql = "select * from KoneksiKoperasiBaronang.dbo.ListKoperasi where KID='".$kid."'";
      }
      else{
        $sql = "select * from KoneksiKoperasiBaronang.dbo.ListKoperasi where Status = 1";
      }
      $datas =  Yii::app()->db->createCommand($sql)->queryAll();
      foreach($datas as $data){

          $sql2 = "select * from ".$data['DatabaseName'].".dbo.LoanList
            where [LoanAppNumber] not in (Select [LoanAppNum] from ".$data['DatabaseName'].".[dbo].[LoanClose]) order by LoanAppNumber asc";
          $datas2 =  Yii::app()->db->createCommand($sql2)->queryAll();
          foreach($datas2 as $data2){
              $tanggal = date('Y-m-d');
              $sql3 = "exec ".$data['DatabaseName'].".dbo.ProsesCalculateLoanBilling '".$data2['LoanAppNumber']."'";
              $exec2 = Yii::app()->db->createCommand($sql3);
              $exec2->execute();

              $response['status'] = true;
              $response['message'] = 'Cron loan billing berhasil dijalankan '.$tanggal;

              /*if($exec2->execute()){
                  $response['status'] = true;
                  $response['message'] = 'Cron loan billing berhasil dijalankan '.$tanggal;
              }
              else{
                  $response['message'] = $sql3;
                  $response['errorCode'] = '(erX00001)';
              }*/
          }
      }

      echo json_encode($response);
    }

    public function actionBasicSavingBilling($kid = null){
      $response = array(
          'status'=>FALSE,
          'message'=>'',
          'errorCode'=>'',
      );

      if($kid != null){
        $sql = "select * from KoneksiKoperasiBaronang.dbo.ListKoperasi where KID='".$kid."'";
      }
      else{
        $sql = "select * from KoneksiKoperasiBaronang.dbo.ListKoperasi where Status = 1";
      }
      $datas =  Yii::app()->db->createCommand($sql)->queryAll();
      foreach($datas as $data){

          $tanggal = date('Y-m-d');
          $sql2 = "exec ".$data['DatabaseName'].".dbo.ProsesCalculateBasicSavingBilling";
          $exec2 = Yii::app()->db->createCommand($sql2);
          if($exec2->execute()){
              $response['status'] = true;
              $response['message'] = 'Cron basic saving billing berhasil dijalankan '.$tanggal;
          }
          else{
              $response['message'] = 'Cron basic saving billing gagal dijalankan '.$data['KID'];
              $response['errorCode'] = '(erX00001)';
          }
      }

      echo json_encode($response);
    }

    public function actionTicketingUser(){
      $response = array(
          'status'=>FALSE,
          'message'=>'',
          'errorCode'=>'',
      );

      $tanggal = date('Y-m-d');
      $sql = "exec PaymentGateway.dbo.ProsesDeleteTicketingUser";
      $exec = Yii::app()->db->createCommand($sql);
      if($exec->execute()){
          $response['status'] = true;
          $response['message'] = 'Cron berhasil dijalankan '.$tanggal;
      }
      else{
          $response['message'] = 'Cron gagal dijalankan '.$data['KID'];
          $response['errorCode'] = '(erX00001)';
      }

      echo json_encode($response);
    }
}
?>
