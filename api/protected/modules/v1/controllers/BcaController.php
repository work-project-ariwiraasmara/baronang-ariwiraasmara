<?php
// Set header response
header('Content-Type: application/json');

class BcaController extends Controller {

    public function actionIndex(){

    }

    public function getDateAll(){
        $t = microtime(true);
        $micro = sprintf("%06d",($t - floor($t)) * 1000000);
        $d = new DateTime( date('Y-m-d H:i:s.'.$micro, $t) );

        return $d->format("Y-m-d H:i:s.u");
    }

    public function getTransactionNumber(){
      $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyz';
      $random =  strtoupper(substr(str_shuffle($permitted_chars), 0, 8));
      if($random != ''){
          return $random;
      }
    }

    public function actionInquiry(){
        $response = array(
            'status'=>FALSE,
            'message'=>'',
            'statusCode'=>'',
        );

        $sprint = new Sprint();
        $channelID = $sprint->channelID;
        $secretKey = $sprint->secretKey;
        $kid = $sprint->kid;
        $bankCode = $sprint->bankCode;

        /*$param = implode(',', $_REQUEST);*/
        $param = '';

        //save log api
        $id = '';
        $qwe = "insert into [PaymentGateway].[dbo].[LogHitAPI](KID, Action, DateHit, Param, Status) values('".$kid."','".Yii::app()->controller->action->id."','".$this->getDateAll()."','".$param."',0)";
        $asd =  Yii::app()->db->createCommand($qwe);
        if($asd->execute()){
            $id = Yii::app()->db->getLastInsertID();
        }

        if(isset($_REQUEST['currency']) and isset($_REQUEST['transactionDate']) and isset($_REQUEST['channelType']) and isset($_REQUEST['customerAccount']) and isset($_REQUEST['inquiryReffId']) and isset($_REQUEST['authCode'])){
            $currency = $_REQUEST['currency'];
            $transactionDate = $_REQUEST['transactionDate'];
            $channelType = $_REQUEST['channelType'];
            $customerAccount = $_REQUEST['customerAccount'];
            $inquiryReffId = $_REQUEST['inquiryReffId'];
            $authCode = $_REQUEST['authCode'];

            $a = "select* from KoneksiKoperasiBaronang.dbo.ListKoperasi where KID = '$kid' and Status = 1";
            $b =  Yii::app()->db->createCommand($a)->queryAll();
            if($b != null){
                $date = date('Y-m-d');
                $c = "select* from ".$b[0]['DatabaseName'].".dbo.VaMember where VaNumber = '$customerAccount' and BankCode = '$bankCode' and Status = 1";
                $d =  Yii::app()->db->createCommand($c)->queryAll();
                if($d != null){
                    $transno = $this->getTransactionNumber();
                    $amount = 0;
                    $transdate = date('Y-m-d H:i:s');
                    $transexpired = date('Y-m-d H:i:s', strtotime($transdate.'+ 1 day'));
                    $cardno = $d[0]['CardNo'];

                    $authCodeGen = hash("sha256", $customerAccount.$inquiryReffId.$secretKey);

                    if($authCode == $authCodeGen){
                        $sql = "exec ".$b[0]['DatabaseName'].".[dbo].[ProsesRequestVa] '$transno','$customerAccount', '$cardno', '$bankCode', '$amount'";
                        $exec =  Yii::app()->db->createCommand($sql);
                        if($exec->execute()){
                            $response['status'] = TRUE;
                            $response['statusCode'] = '000';
                            $response['message'] = 'VA aktif';

                            $response['channelId'] = $channelID;
                            $response['currency'] = $currency;
                            $response['transactionNo'] = $transno;
                            $response['transactionAmount'] = "$amount";
                            $response['transactionDate'] = $transdate;
                            $response['transactionExpire'] = $transexpired;
                            $response['description'] = 'Top up VA with card number '.$cardno;
                            $response['customerAccount'] = $customerAccount;
                            $response['customerName'] = $cardno;
                            $response['inquiryReffId'] = $inquiryReffId;
                        }
                        else{
                            $response['message'] = 'Error API';
                            $response['statusCode'] = '200';

                            $response['channelId'] = '';
                            $response['currency'] = '';
                            $response['transactionNo'] = '';
                            $response['transactionAmount'] = '';
                            $response['transactionDate'] = '';
                            $response['transactionExpire'] = '';
                            $response['description'] = '';
                            $response['customerAccount'] = '';
                            $response['customerName'] = '';
                            $response['inquiryReffId'] = '';
                        }
                    }
                    else{
                        $response['message'] = 'AuthCode invalid';
                        $response['statusCode'] = '200';

                        $response['channelId'] = '';
                        $response['currency'] = '';
                        $response['transactionNo'] = '';
                        $response['transactionAmount'] = '';
                        $response['transactionDate'] = '';
                        $response['transactionExpire'] = '';
                        $response['description'] = '';
                        $response['customerAccount'] = '';
                        $response['customerName'] = '';
                        $response['inquiryReffId'] = '';
                    }
                }
                else{
                  $response['message'] = 'VA not found';
                  $response['statusCode'] = '200';

                  $response['channelId'] = '';
                  $response['currency'] = '';
                  $response['transactionNo'] = '';
                  $response['transactionAmount'] = '';
                  $response['transactionDate'] = '';
                  $response['transactionExpire'] = '';
                  $response['description'] = '';
                  $response['customerAccount'] = '';
                  $response['customerName'] = '';
                  $response['inquiryReffId'] = '';
                }
            }
            else{
                $response['message'] = 'KID not found';
                $response['statusCode'] = '200';
            }
        }
        else{
            $response['message'] = 'Invalid require data';
            $response['statusCode'] = '500';
        }

        if($id != ''){
            /*$msg = json_encode($response);*/
            $msg = '';
            //save log api
            $qwe = "update [PaymentGateway].[dbo].[LogHitAPI] set Status = 1, DateProcess = '".$this->getDateAll()."',Message = '".$msg."' where ID = '".$id."'";
            $asd =  Yii::app()->db->createCommand($qwe);
            $asd->execute();
        }

        echo json_encode($response);
    }

    public function actionPayment(){
        $response = array(
        );

        $sprint = new Sprint();
        $cid = $sprint->channelID;
        $secretKey = $sprint->secretKey;
        $kid = $sprint->kid;

        /*$param = implode(',', $_REQUEST);*/
        $param = '';

        //save log api
        $id = '';
        $qwe = "insert into [PaymentGateway].[dbo].[LogHitAPI](KID, Action, DateHit, Param, Status) values('".$kid."','".Yii::app()->controller->action->id."','".$this->getDateAll()."','".$param."',0)";
        $asd =  Yii::app()->db->createCommand($qwe);
        if($asd->execute()){
            $id = Yii::app()->db->getLastInsertID();
        }

        if(isset($_REQUEST['channelId']) and isset($_REQUEST['currency']) and isset($_REQUEST['transactionNo']) and isset($_REQUEST['transactionAmount']) and isset($_REQUEST['channelType']) and isset($_REQUEST['transactionStatus']) and isset($_REQUEST['transactionMessage'])
            and isset($_REQUEST['customerAccount']) and isset($_REQUEST['flagType']) and isset($_REQUEST['insertId']) and isset($_REQUEST['authCode']) and isset($_REQUEST['paymentReffId'])){

            $channelId = $_REQUEST['channelId'];
            $currency = $_REQUEST['currency'];
            $transactionNo = $_REQUEST['transactionNo'];
            $transactionAmount = $_REQUEST['transactionAmount'];
            $transactionDate = $_REQUEST['transactionDate'];
            $channelType = $_REQUEST['channelType'];
            $transactionStatus = $_REQUEST['transactionStatus'];
            $transactionMessage = $_REQUEST['transactionMessage'];
            $customerAccount = $_REQUEST['customerAccount'];
            $flagType = $_REQUEST['flagType'];
            $insertId = $_REQUEST['insertId'];
            $authCode = $_REQUEST['authCode'];
            $paymentReffId = $_REQUEST['paymentReffId'];

            $tanggal = date('Y-m-d H:i:s');
            $a = "select* from KoneksiKoperasiBaronang.dbo.ListKoperasi where KID = '$kid' and Status = 1";
            $b =  Yii::app()->db->createCommand($a)->queryAll();
            if($b != null){
                $c = "select* from ".$b[0]['DatabaseName'].".dbo.VaRequest where VaNumber = '$customerAccount' and RequestID = '$transactionNo' and Status = 1";
                $d =  Yii::app()->db->createCommand($c)->queryAll();
                if($d != null){
                    $cardno = $d[0]['CardNo'];
                    $bankCode = $d[0]['BankCode'];
                    $requestID = $d[0]['RequestID'];
                    $expiredDate = date('Y-m-d H:i:s', strtotime($d[0]['ExpiredDate']));
                    $status = $d[0]['Status'];

                    if($expiredDate >= $tanggal){
                        if($status == 1){
                            if($channelId == $cid){
                                if($transactionNo == $requestID){
                                    if($currency == 'IDR'){
                                        // Generate authCode
                                        $authCodeGen = hash("sha256", $transactionNo.$transactionAmount.$channelId.$transactionStatus.$insertId.$secretKey);
                                        if($authCodeGen == $authCode){
                                            if($transactionStatus == '00'){
                                                $sql = "exec ".$b[0]['DatabaseName'].".[dbo].[ProsesUploadVa] '$transactionNo', '$transactionDate', '$bankCode', '$paymentReffId', '$customerAccount', '$transactionAmount'";
                                                $exec =  Yii::app()->db->createCommand($sql);
                                                if($exec->execute()){
                                                    $response['channelId'] = $channelId;
                                                    $response['currency'] = $currency;
                                                    $response['paymentStatus'] = '00';
                                                    $response['paymentMessage'] = 'Success';
                                                    $response['flagType'] = $flagType;
                                                    $response['paymentReffId'] = $paymentReffId;
                                                }
                                                else{
                                                    $response['message'] = 'Error API';
                                                    $response['statusCode'] = '200';
                                                }
                                            }
                                            else{
                                              $response['channelId'] = $channelId;
                                              $response['currency'] = $currency;
                                              $response['paymentStatus'] = '01';
                                              $response['paymentMessage'] = 'Invalid transactionStatus';
                                              $response['flagType'] = $flagType;
                                              $response['paymentReffId'] = $paymentReffId;
                                            }
                                        }
                                        else{
                                            $response['channelId'] = $channelId;
                                            $response['currency'] = $currency;
                                            $response['paymentStatus'] = '01';
                                            $response['paymentMessage'] = 'Invalid authCode';
                                            $response['flagType'] = $flagType;
                                            $response['paymentReffId'] = $paymentReffId;
                                        }
                                    }
                                    else{
                                      $response['channelId'] = $channelId;
                                      $response['currency'] = $currency;
                                      $response['paymentStatus'] = '01';
                                      $response['paymentMessage'] = 'Invalid currency';
                                      $response['flagType'] = $flagType;
                                      $response['paymentReffId'] = $paymentReffId;
                                    }
                                }
                                else{
                                  $response['channelId'] = $channelId;
                                  $response['currency'] = $currency;
                                  $response['paymentStatus'] = '01';
                                  $response['paymentMessage'] = 'Invalid transactionNo';
                                  $response['flagType'] = $flagType;
                                  $response['paymentReffId'] = $paymentReffId;
                                }
                            }
                            else{
                              $response['channelId'] = $channelId;
                              $response['currency'] = $currency;
                              $response['paymentStatus'] = '01';
                              $response['paymentMessage'] = 'Invalid channelId';
                              $response['flagType'] = $flagType;
                              $response['paymentReffId'] = $paymentReffId;
                            }
                        }
                        else if($status == 2){
                          $response['channelId'] = $channelId;
                          $response['currency'] = $currency;
                          $response['paymentStatus'] = '02';
                          $response['paymentMessage'] = 'Transaction had been paid';
                          $response['flagType'] = $flagType;
                          $response['paymentReffId'] = $paymentReffId;
                        }
                        else if($status == 0){
                          $response['channelId'] = $channelId;
                          $response['currency'] = $currency;
                          $response['paymentStatus'] = '05';
                          $response['paymentMessage'] = 'Transaction had been canceled';
                          $response['flagType'] = $flagType;
                          $response['paymentReffId'] = $paymentReffId;

                        }
                        else{
                          $response['message'] = 'Invalid status VA';
                          $response['statusCode'] = '200';
                        }
                    }
                    else{
                      $response['channelId'] = $channelId;
                      $response['currency'] = $currency;
                      $response['paymentStatus'] = '04';
                      $response['paymentMessage'] = 'Expired transaction';
                      $response['flagType'] = $flagType;
                      $response['paymentReffId'] = $paymentReffId;

                    }
                }
                else{
                  $response['channelId'] = $channelId;
                  $response['currency'] = $currency;
                  $response['paymentStatus'] = '01';
                  $response['paymentMessage'] = 'Invalid VA number';
                  $response['flagType'] = $flagType;
                  $response['paymentReffId'] = $paymentReffId;
                }
            }
            else{
              $response['message'] = 'KID not found';
              $response['statusCode'] = '200';
            }
        }
        else{
            $response['message'] = 'Invalid require data';
            $response['statusCode'] = '500';
        }

        if($id != ''){
            $msg = json_encode($response);
            //save log api
            $qwe = "update [PaymentGateway].[dbo].[LogHitAPI] set Status = 1, DateProcess = '".$this->getDateAll()."',Message = '".$msg."' where ID = '".$id."'";
            $asd =  Yii::app()->db->createCommand($qwe);
            $asd->execute();
        }

        echo json_encode($response);
    }
}
?>
