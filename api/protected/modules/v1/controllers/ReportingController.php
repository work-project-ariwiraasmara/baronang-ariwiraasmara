<?php

class ReportingController extends Controller {

    public function getDateAll(){
        $t = microtime(true);
        $micro = sprintf("%06d",($t - floor($t)) * 1000000);
        $d = new DateTime( date('Y-m-d H:i:s.'.$micro, $t) );

        return $d->format("Y-m-d H:i:s.u");
    }

    public function actionGetTopUpData(){
      $response = array(
          'Status'=>false,
          'Message'=>'',
          'StatusCode'=>'',
          'Data'=>array(),
      );

      if(isset($_REQUEST['KID']) and isset($_REQUEST['Date'])){
          $kid = $_REQUEST['KID'];
          $date = $_REQUEST['Date'];
          $param = implode(',', $_REQUEST);

          $qwe = "insert into [PaymentGateway].[dbo].[LogHitAPI](KID, Action, DateHit, Param, Status) values('".$kid."','".Yii::app()->controller->action->id."','".$this->getDateAll()."','".$param."',0)";
          $asd =  Yii::app()->db->createCommand($qwe);
          if($asd->execute()){
              $id = Yii::app()->db->getLastInsertID();
          }

          $y = "select* from KoneksiKoperasiBaronang.dbo.ListKoperasi where KID = '$kid' and Status = 1";
          $z =  Yii::app()->db->createCommand($y)->queryAll();
          if($z != null){
              $yy = "select* from ".$z[0]['DatabaseName'].".[dbo].[TopUpPointData] where convert(date, TimeStam) = '".$date."'";
              $zz =  Yii::app()->db->createCommand($yy)->queryAll();
              if($zz != null){
                  foreach ($zz as $z){
                      $data = array(
                          'TransactionCode'=>$z['TransactionNumber'],
                          'DateTopUp'=>$z['TimeStam'],
                          'CardNo'=>$z['CardNo'],
                          'TopUpPoint'=>$z['TopUpPoint'],
                          'AmountTopUp'=>$z['AmountTopUp'],
                          'Name'=>$z['Name'],
                          'Phone'=>$z['Phone'],
                          'Email'=>$z['Email'],
                          'KTP'=>$z['KTP']
                      );

                      array_push($response['Data'], $data);
                  }

                  $response['Message'] = 'Data Found';
                  $response['StatusCode'] = '000';
                  $response['Status'] = true;
              }
              else{
                $response['Message'] = 'Kosong';
                $response['StatusCode'] = '500';
              }
          }
          else{
            $response['Message'] = 'KID Tidak Ditemukan';
            $response['StatusCode'] = '404';
          }

          if($id != ''){
              $msg = json_encode($response);

              //save log api
              $qwe = "update [PaymentGateway].[dbo].[LogHitAPI] set Status = 1, DateProcess = '".$this->getDateAll()."',Message = '".$response['Message']."' where ID = '".$id."'";
              $asd =  Yii::app()->db->createCommand($qwe);
              $asd->execute();
          }
      }
      else{
        $response['Message'] = 'Invalid Require Data';
        $response['StatusCode'] = '400';
      }

      echo json_encode($response);
    }

    public function actionGetB2BData(){
        $response = array(
            'Status'=>false,
            'Message'=>'',
            'StatusCode'=>'',
            'Data'=>array(),
        );

        if(isset($_REQUEST['KID']) and isset($_REQUEST['Date'])){
            $kid = $_REQUEST['KID'];
            $date = $_REQUEST['Date'];
            $param = implode(',', $_REQUEST);

            $qwe = "insert into [PaymentGateway].[dbo].[LogHitAPI](KID, Action, DateHit, Param, Status) values('".$kid."','".Yii::app()->controller->action->id."','".$this->getDateAll()."','".$param."',0)";
            $asd =  Yii::app()->db->createCommand($qwe);
            if($asd->execute()){
                $id = Yii::app()->db->getLastInsertID();
            }

            $y = "select* from KoneksiKoperasiBaronang.dbo.ListKoperasi where KID = '$kid' and Status = 1";
            $z =  Yii::app()->db->createCommand($y)->queryAll();
            if($z != null){
                $yy = "select* from ".$z[0]['DatabaseName'].".[dbo].[B2BData] where convert(date, DateReceipt) = '".$date."' or convert(date, DateRegister) = '".$date."'";
                $zz =  Yii::app()->db->createCommand($yy)->queryAll();
                if($zz != null){
                    foreach ($zz as $z){
                        $data = array(
                            'RegistrationID'=>$z['RegistrationID'],
                            'ExpiredDate'=>$z['ExpireDate'],
                            'DateReceipt'=>$z['DateReceipt'],
                            'DateRegister'=>$z['DateRegister'],
                            'Amount'=>$z['Amount'],
                            'LocationName'=>$z['LocationName'],
                            'VehicleType'=>$z['VehicleType'],
                            'VehicleCode'=>$z['VehicleCode']
                        );

                        array_push($response['Data'], $data);
                    }

                    $response['Message'] = 'Data Found';
                    $response['StatusCode'] = '000';
                    $response['Status'] = true;
                }
                else{
                  $response['Message'] = 'Kosong';
                  $response['StatusCode'] = '500';
                }
            }
            else{
              $response['Message'] = 'KID Tidak Ditemukan';
              $response['StatusCode'] = '404';
            }

            if($id != ''){
                $msg = json_encode($response);

                //save log api
                $qwe = "update [PaymentGateway].[dbo].[LogHitAPI] set Status = 1, DateProcess = '".$this->getDateAll()."',Message = '".$response['Message']."' where ID = '".$id."'";
                $asd =  Yii::app()->db->createCommand($qwe);
                $asd->execute();
            }
        }
        else{
          $response['Message'] = 'Invalid Require Data';
          $response['StatusCode'] = '400';
        }

        echo json_encode($response);
    }

    public function actionGetMembershipData(){
        $response = array(
            'Status'=>false,
            'Message'=>'',
            'StatusCode'=>'',
            'Data'=>array(),
        );

        if(isset($_REQUEST['KID']) and isset($_REQUEST['Date'])){
            $kid = $_REQUEST['KID'];
            $date = $_REQUEST['Date'];
            $param = implode(',', $_REQUEST);

            $qwe = "insert into [PaymentGateway].[dbo].[LogHitAPI](KID, Action, DateHit, Param, Status) values('".$kid."','".Yii::app()->controller->action->id."','".$this->getDateAll()."','".$param."',0)";
            $asd =  Yii::app()->db->createCommand($qwe);
            if($asd->execute()){
                $id = Yii::app()->db->getLastInsertID();
            }

            $y = "select* from KoneksiKoperasiBaronang.dbo.ListKoperasi where KID = '$kid' and Status = 1";
            $z =  Yii::app()->db->createCommand($y)->queryAll();
            if($z != null){
                $yy = "select* from ".$z[0]['DatabaseName'].".[dbo].[BuyMembershipData] where convert(date, TimeStam) = '".$date."'";
                $zz =  Yii::app()->db->createCommand($yy)->queryAll();
                if($zz != null){
                    foreach ($zz as $z){
                        $data = array(
                            'TransactionCode'=>$z['TransactionNumber'],
                            'DateTopUp'=>$z['TimeStam'],
                            'CardNo'=>$z['CardNo'],
                            'PointDeduct'=>$z['ProductPoint'],
                            'Note'=>$z['Note'],
                            'Price'=>$z['Price'],
                            'Name'=>$z['Name'],
                            'Phone'=>$z['Phone'],
                            'Email'=>$z['Email'],
                            'KTP'=>$z['KTP']
                        );

                        array_push($response['Data'], $data);
                    }

                    $response['Message'] = 'Data Found';
                    $response['StatusCode'] = '000';
                    $response['Status'] = true;
                }
                else{
                  $response['Message'] = 'Kosong';
                  $response['StatusCode'] = '500';
                }
            }
            else{
              $response['Message'] = 'KID Tidak Ditemukan';
              $response['StatusCode'] = '404';
            }

            if($id != ''){
                $msg = json_encode($response);

                //save log api
                $qwe = "update [PaymentGateway].[dbo].[LogHitAPI] set Status = 1, DateProcess = '".$this->getDateAll()."',Message = '".$response['Message']."' where ID = '".$id."'";
                $asd =  Yii::app()->db->createCommand($qwe);
                $asd->execute();
            }
        }
        else{
          $response['Message'] = 'Invalid Require Data';
          $response['StatusCode'] = '400';
        }

        echo json_encode($response);
    }
}

?>
