<?php

class DevController extends Controller {

    public function getDateAll(){
        $t = microtime(true);
        $micro = sprintf("%06d",($t - floor($t)) * 1000000);
        $d = new DateTime( date('Y-m-d H:i:s.'.$micro, $t) );

        return $d->format("Y-m-d H:i:s.u");
    }

    public function actionGateIn(){
        $response = array(
            'Status'=>false,
            'Message'=>'',
            'StatusCode'=>'',
        );

        if(isset($_REQUEST['KID']) and isset($_REQUEST['CardID']) and isset($_REQUEST['LocationID']) and isset($_REQUEST['VehicleType']) and isset($_REQUEST['MinimumPoint'])){
            $kid = $_REQUEST['KID'];
            $card_id = str_replace(' ','+',$_REQUEST['CardID']);
            $minimum_point = $_REQUEST['MinimumPoint'];
            $locationid = $_REQUEST['LocationID'];
            $vehicletype = $_REQUEST['VehicleType'];

            $parking = new Parking();
            $cardid = $parking->decrypt($card_id);

            $param = implode(',', $_REQUEST);

            //save log api
            $id = '';
            $qwe = "insert into [PaymentGateway].[dbo].[LogHitAPI](KID, Action, DateHit, Param, Status) values('".$kid."','".Yii::app()->controller->action->id."','".$this->getDateAll()."','".$param."',0)";
            $asd =  Yii::app()->db->createCommand($qwe);
            if($asd->execute()){
                $id = Yii::app()->db->getLastInsertID();
            }

            $yy = "select* from KoneksiKoperasiBaronang.dbo.ListKoperasi where KID = '$kid' and Status = 1";
            $zz =  Yii::app()->db->createCommand($yy)->queryAll();
            if($zz != null){
                //cek location
                $sql2 = "select* from ".$zz[0]['DatabaseName'].".dbo.LocationMerchant where LocationID = '".$locationid."' and Status = 1";
                $exec2 =  Yii::app()->db->createCommand($sql2)->queryAll();
                if($exec2 != null){
                    $isOnlyMember = $exec2[0]['isOnlyMember'];

                    //cek vehicle
                    $s2 = "select* from ".$zz[0]['DatabaseName'].".dbo.VehicleType where Code = '".$vehicletype."' and Status = 1";
                    $e2 =  Yii::app()->db->createCommand($s2)->queryAll();
                    if($e2 != null){
                        $vehicle = $e2[0]['VehicleID'];

                        //cek balance
                        $sql = "select* from ".$zz[0]['DatabaseName'].".dbo.MasterCard where CardNo = '".$cardid."' and Status = 1 or RFIDNo = '".$cardid."' and Status = 1";
                        $exec =  Yii::app()->db->createCommand($sql)->queryAll();
                        if($exec != null){
                            $cardno = $exec[0]['CardNo'];

                            $response['CardID'] = $cardid;

                            //cek balance
                            $sql3 = "select* from ".$zz[0]['DatabaseName'].".dbo.ParkingList where CardNo = '".$cardno."' and Status = 0";
                            $exec3 =  Yii::app()->db->createCommand($sql3)->queryAll();
                            if($exec3 == null){

                                $sql2 = "select* from ".$zz[0]['DatabaseName'].".dbo.MemberCard where CardNo = '".$cardno."' and Status = 1";
                                $exec2 =  Yii::app()->db->createCommand($sql2)->queryAll();
                                if($exec != null){
                                    $status = 0;
                                    $member = 0;
                                    $gp = 0;
                                    $year = date('Y');
                                    $month = date('m');
                                    $sq2 = "select top 1 * from ".$zz[0]['DatabaseName'].".dbo.MemberLocationMonth where CardNo = '".$cardno."' and LocationID ='".$locationid."' and VehicleID = '".$vehicle."' and Year = '".$year."' and Bulan = '".$month."' and Status = 1";
                                    $exe2 =  Yii::app()->db->createCommand($sq2)->queryAll();
                                    if($exe2 != null){
                                        $member = 1;
                                        $response['Member'] = true;
                                    }
                                    else{
                                        $member = 0;
                                        $response['Member'] = false;
                                    }

                                    $sq3 = "select top 1 * from ".$zz[0]['DatabaseName'].".dbo.MemberLocationGP where CardNo = '".$cardno."' and LocationID ='".$locationid."' and VehicleID = '".$vehicle."' and Status = 1";
                                    $exe3 =  Yii::app()->db->createCommand($sq3)->queryAll();
                                    if($exe3 != null){
                                        $gp = 1;
                                        $response['GP'] = true;
                                    }
                                    else{
                                        $gp = 0;
                                        $response['GP'] = false;
                                    }

                                    $tin = date('Y-m-d H:i:s');
                                    $reff = '';
                                    if(isset($_REQUEST['TimeIn']) and isset($_REQUEST['ReffNo'])){
                                        $tin = $_REQUEST['TimeIn'];
                                        $reff = $_REQUEST['ReffNo'];
                                    }

                                    if($isOnlyMember == 1){
                                        if($member == 1){
                                            $qwee = "exec ".$zz[0]['DatabaseName'].".[dbo].[ProsesGateIn] '$cardno','$locationid','$vehicletype',$member";
                                            $asde =  Yii::app()->db->createCommand($qwee);
                                            if($asde->execute()){
                                                $response['Status'] = true;
                                                $response['Message'] = 'Berhasil masuk sebagai member';
                                                $response['StatusCode'] = '002';
                                            }
                                            else{
                                                $response['Message'] = 'Failed In';
                                                $response['StatusCode'] = '200';
                                            }
                                        }
                                        else{
                                            $response['Message'] = 'Kartu Tidak Terdaftar Sebagai Member';
                                            $response['StatusCode'] = '207';
                                        }
                                    }
                                    else{
                                        if($member == 0){
                                            $minimum = 0;
                                            $afg = "select top 1 * from ".$zz[0]['DatabaseName'].".dbo.ParkingSetting";
                                            $zcv =  Yii::app()->db->createCommand($afg)->queryAll();
                                            if($zcv != null){
                                                $value = $zcv[0]['ValueConversion'];

                                                $minimum = round($minimum_point/$value);
                                            }

                                            if($minimum <= $exec2[0]['Point'] || $gp == 1){
                                                $qwee = "exec ".$zz[0]['DatabaseName'].".[dbo].[ProsesGateIn] '$cardno','$locationid','$vehicletype',$member";
                                                $asde =  Yii::app()->db->createCommand($qwee);
                                                if($asde->execute()){
                                                    $response['Status'] = true;
                                                    $response['StatusCode'] = '001';
                                                    $response['Message'] = 'Berhasil masuk sebagai non member';
                                                }
                                                else{
                                                    $response['Message'] = 'Failed In';
                                                    $response['StatusCode'] = '200';
                                                }
                                            }
                                            else{
                                                $response['Message'] = 'Saldo Tidak Cukup';
                                                $response['StatusCode'] = '201';
                                            }
                                        }
                                        else if($member == 1){
                                            $qwee = "exec ".$zz[0]['DatabaseName'].".[dbo].[ProsesGateIn] '$cardno','$locationid','$vehicletype',$member";
                                            $asde =  Yii::app()->db->createCommand($qwee);
                                            if($asde->execute()){
                                                $response['Status'] = true;
                                                $response['Message'] = 'Berhasil masuk sebagai member';
                                                $response['StatusCode'] = '002';
                                            }
                                            else{
                                                $response['Message'] = 'Failed In';
                                                $response['StatusCode'] = '200';
                                            }
                                        }
                                        else{
                                            $response['Message'] = 'Kartu Tidak Terdaftar Sebagai Member';
                                            $response['StatusCode'] = '207';
                                        }
                                    }
                                }
                                else{
                                  $response['Message'] = 'Kartu Tidak Valid';
                                  $response['StatusCode'] = '202';
                                }
                            }
                            else{
                              $response['Message'] = 'Kartu Sedang Digunakan';
                              $response['StatusCode'] = '203';
                            }
                        }
                        else{
                            $response['Message'] = 'Kartu Tidak Ditemukan';
                            $response['StatusCode'] = '202';
                        }
                    }
                    else{
                        $response['Message'] = 'Vehicle Not Valid';
                        $response['StatusCode'] = '204';
                    }
                }
                else{
                  $response['Message'] = 'Location Not Found';
                  $response['StatusCode'] = '205';
                }
            }
            else{
                $response['Message'] = 'KID Tidak Ditemukan';
                $response['StatusCode'] = '404';
            }

            if($id != ''){
                $msg = json_encode($response);
                //save log api
                $qwe = "update [PaymentGateway].[dbo].[LogHitAPI] set Status = 1, DateProcess = '".$this->getDateAll()."',Message = '".$msg."' where ID = '".$id."'";
                $asd =  Yii::app()->db->createCommand($qwe);
                $asd->execute();
            }
        }
        else{
            $response['Message'] = 'Invalid Require Data';
            $response['StatusCode'] = '400';
        }

        echo json_encode($response);
    }

    public function actionCheckIn(){
        $response = array(
            'Status'=>false,
            'Message'=>'',
            'StatusCode'=>'',
        );

        if(isset($_REQUEST['KID']) and isset($_REQUEST['CardID']) and isset($_REQUEST['LocationID']) and isset($_REQUEST['VehicleType']) and isset($_REQUEST['MinimumPoint'])){
            $kid = $_REQUEST['KID'];
            $card_id = str_replace(' ','+',$_REQUEST['CardID']);
            $minimum_point = $_REQUEST['MinimumPoint'];
            $locationid = $_REQUEST['LocationID'];
            $vehicletype = $_REQUEST['VehicleType'];

            $parking = new Parking();
            $cardid = $parking->decrypt($card_id);

            $param = implode(',', $_REQUEST);

            //save log api
            $id = '';
            $qwe = "insert into [PaymentGateway].[dbo].[LogHitAPI](KID, Action, DateHit, Param, Status) values('".$kid."','".Yii::app()->controller->action->id."','".$this->getDateAll()."','".$param."',0)";
            $asd =  Yii::app()->db->createCommand($qwe);
            if($asd->execute()){
                $id = Yii::app()->db->getLastInsertID();
            }

            $yy = "select* from KoneksiKoperasiBaronang.dbo.ListKoperasi where KID = '$kid' and Status = 1";
            $zz =  Yii::app()->db->createCommand($yy)->queryAll();
            if($zz != null){

                //cek location
                $sql2 = "select* from ".$zz[0]['DatabaseName'].".dbo.LocationMerchant where LocationID = '".$locationid."' and Status = 1";
                $exec2 =  Yii::app()->db->createCommand($sql2)->queryAll();
                if($exec2 != null){
                    $isOnlyMember = $exec2[0]['isOnlyMember'];

                    //cek vehicle
                    $s2 = "select* from ".$zz[0]['DatabaseName'].".dbo.VehicleType where Code = '".$vehicletype."' and Status = 1";
                    $e2 =  Yii::app()->db->createCommand($s2)->queryAll();
                    if($e2 != null){
                        $vehicle = $e2[0]['VehicleID'];

                        //cek balance
                        $sql = "select* from ".$zz[0]['DatabaseName'].".dbo.MasterCard where CardNo = '".$cardid."' and Status = 1 or RFIDNo = '".$cardid."' and Status = 1";
                        $exec =  Yii::app()->db->createCommand($sql)->queryAll();
                        if($exec != null){
                            $cardno = $exec[0]['CardNo'];

                            $response['CardID'] = $cardid;

                            //cek balance
                            $sql3 = "select* from ".$zz[0]['DatabaseName'].".dbo.ParkingList where CardNo = '".$cardno."' and Status = 0";
                            $exec3 =  Yii::app()->db->createCommand($sql3)->queryAll();
                            if($exec3 == null){

                                $sql2 = "select* from ".$zz[0]['DatabaseName'].".dbo.MemberCard where CardNo = '".$cardno."' and Status = 1";
                                $exec2 =  Yii::app()->db->createCommand($sql2)->queryAll();
                                if($exec != null){
                                    $status = 0;
                                    $member = 0;
                                    $year = date('Y');
                                    $month = date('m');
                                    $sq2 = "select top 1 * from ".$zz[0]['DatabaseName'].".dbo.MemberLocationMonth where CardNo = '".$cardno."' and LocationID ='".$locationid."' and VehicleID = '".$vehicle."' and Year = '".$year."' and Bulan = '".$month."' and Status = 1";
                                    $exe2 =  Yii::app()->db->createCommand($sq2)->queryAll();
                                    if($exe2 != null){
                                        $member = 1;
                                        $response['Member'] = true;
                                    }
                                    else{
                                        $member = 0;
                                        $response['Member'] = false;
                                    }

                                    if($isOnlyMember == 1){
                                        if($member == 1){
                                            $response['Status'] = true;
                                            $response['Message'] = 'Berhasil masuk sebagai member';
                                            $response['StatusCode'] = '002';
                                            $response['CardID'] = $cardid;
                                        }
                                        else{
                                            $response['Message'] = 'Kartu Tidak Valid';
                                            $response['StatusCode'] = '202';
                                        }
                                    }
                                    else{
                                        if($member == 0){
                                            $minimum = 0;
                                            $afg = "select top 1 * from ".$zz[0]['DatabaseName'].".dbo.ParkingSetting";
                                            $zcv =  Yii::app()->db->createCommand($afg)->queryAll();
                                            if($zcv != null){
                                                $value = $zcv[0]['ValueConversion'];

                                                $minimum = round($minimum_point/$value);
                                            }

                                            if($minimum <= $exec2[0]['Point']){
                                                $response['Status'] = true;
                                                $response['StatusCode'] = '001';
                                                $response['Message'] = 'Berhasil masuk sebagai non member';
                                                $response['CardID'] = $cardid;
                                            }
                                            else{
                                                $response['Message'] = 'Saldo Tidak Cukup';
                                                $response['StatusCode'] = '201';
                                            }
                                        }
                                        else if($member == 1){
                                            $response['Status'] = true;
                                            $response['Message'] = 'Berhasil masuk sebagai member';
                                            $response['StatusCode'] = '002';
                                            $response['CardID'] = $cardid;
                                        }
                                        else{
                                            $response['Message'] = 'Kartu Tidak Valid';
                                            $response['StatusCode'] = '202';
                                        }
                                    }
                                }
                                else{
                                  $response['Message'] = 'Kartu Tidak Valid';
                                  $response['StatusCode'] = '202';
                                }
                            }
                            else{
                              $response['Message'] = 'Kartu Sedang Digunakan';
                              $response['StatusCode'] = '203';
                            }
                        }
                        else{
                            $response['Message'] = 'Kartu Tidak Ditemukan';
                            $response['StatusCode'] = '202';
                        }
                    }
                    else{
                        $response['Message'] = 'Vehicle Not Valid';
                        $response['StatusCode'] = '204';
                    }
                }
                else{
                  $response['Message'] = 'Location Not Found';
                  $response['StatusCode'] = '205';
                }
            }
            else{
                $response['Message'] = 'KID Tidak Ditemukan';
                $response['StatusCode'] = '404';
            }

            if($id != ''){
                $msg = json_encode($response);
                //save log api
                $qwe = "update [PaymentGateway].[dbo].[LogHitAPI] set Status = 1, DateProcess = '".$this->getDateAll()."',Message = '".$msg."' where ID = '".$id."'";
                $asd =  Yii::app()->db->createCommand($qwe);
                $asd->execute();
            }
        }
        else{
            $response['Message'] = 'Invalid Require Data';
            $response['StatusCode'] = '400';
        }

        echo json_encode($response);
    }

    public function actionTransitIn(){
        $response = array(
            'Status'=>false,
            'StatusCode'=>'',
            'Message'=>'',
        );

        if(isset($_REQUEST['KID']) and isset($_REQUEST['LocationID']) and isset($_REQUEST['CardID']) and isset($_REQUEST['VehicleType'])){
            $kid = $_REQUEST['KID'];
            $card_id = str_replace(' ','+',$_REQUEST['CardID']);
            $locationid = $_REQUEST['LocationID'];
            $vehicletype = $_REQUEST['VehicleType'];

            $parking = new Parking();
            $cardid = $parking->decrypt($card_id);

            $param = implode(',', $_REQUEST);

            //save log api
            $id = '';
            $qwe = "insert into [PaymentGateway].[dbo].[LogHitAPI](KID, Action, DateHit, Param, Status) values('".$kid."','".Yii::app()->controller->action->id."','".$this->getDateAll()."','".$param."',0)";
            $asd =  Yii::app()->db->createCommand($qwe);
            if($asd->execute()){
                $id = Yii::app()->db->getLastInsertID();
            }

            $yy = "select* from KoneksiKoperasiBaronang.dbo.ListKoperasi where KID = '$kid' and Status = 1";
            $zz =  Yii::app()->db->createCommand($yy)->queryAll();
            if($zz != null){
                //cek location
                $a = "select* from ".$zz[0]['DatabaseName'].".dbo.LocationMerchant where LocationID = '".$locationid."' and Status = 1";
                $b =  Yii::app()->db->createCommand($a)->queryAll();
                if($b != null){

                    $c = "select* from ".$zz[0]['DatabaseName'].".dbo.MasterCard where CardNo = '".$cardid."' and Status = 1 or RFIDNo = '".$cardid."' and Status = 1";
                    $d =  Yii::app()->db->createCommand($c)->queryAll();
                    if($d != null){
                        $cardno = $d[0]['CardNo'];

                        $e = "select* from ".$zz[0]['DatabaseName'].".dbo.MemberCard where CardNo = '".$cardno."' and Status = 1";
                        $f =  Yii::app()->db->createCommand($e)->queryAll();
                        if($f != null){
                            $g = "select* from ".$zz[0]['DatabaseName'].".dbo.VehicleType where Code = '".$vehicletype."' and Status = 1";
                            $h =  Yii::app()->db->createCommand($g)->queryAll();
                            if($h != null){
                                $vehicleid = $h[0]['VehicleID'];

                                $m = "select top 1 * from ".$zz[0]['DatabaseName'].".dbo.MemberLocationMonth where LocationID = '".$locationid."' and CardNo = '".$cardno."' and VehicleID = '".$vehicleid."' and Bulan = '".date('m')."' and Year = '".date('Y')."'";
                                $n =  Yii::app()->db->createCommand($m)->queryAll();
                                if($n != null){
                                    //cek lokasi sebelumnya harus masuk
                                    $i = "select top 1 * from ".$zz[0]['DatabaseName'].".dbo.ParkingList where LocationIn = '".$locationid."' and CardNo = '".$cardno."' and Status = 0 order by TimeIn desc";
                                    $j =  Yii::app()->db->createCommand($i)->queryAll();
                                    if($j != null){
                                        $vold = $j[0]['VehicleType'];

                                        $k = "select top 1 * from ".$zz[0]['DatabaseName'].".dbo.LocationTransit where LocationID = '".$locationid."' and VehicleTypeFrom = '".$vold."' and VehicleTypeTo = '".$vehicletype."'";
                                        $l =  Yii::app()->db->createCommand($k)->queryAll();
                                        if($l != null){

                                            $aa = "exec ".$zz[0]['DatabaseName'].".dbo.ProsesTransitIn '".$locationid."','".$cardno."','".$vold."'";
                                            $bb =  Yii::app()->db->createCommand($aa);
                                            if($bb->execute()){
                                                $response['Status'] = true;
                                                $response['Message'] = 'Berhasil masuk sebagai member';
                                                $response['StatusCode'] = '002';
                                            }
                                            else{
                                                $response['Message'] = 'Gagal Transit';
                                                $response['StatusCode'] = '202';
                                            }
                                        }
                                        else{
                                            $response['Message'] = 'Tidak Dapat Transit';
                                            $response['StatusCode'] = '202';
                                        }
                                    }
                                    else{
                                        $response['Message'] = 'Kartu Tidak Dapat Transit';
                                        $response['StatusCode'] = '202';
                                    }
                                }
                                else{
                                    $response['Message'] = 'Kartu Bukan Member';
                                    $response['StatusCode'] = '202';
                                }
                            }
                            else{
                                $response['Message'] = 'Tipe Kendaraan Tidak Sesuai';
                                $response['StatusCode'] = '202';
                            }
                        }
                        else{
                            $response['Message'] = 'Kartu Tidak Valid';
                            $response['StatusCode'] = '202';
                        }
                    }
                    else{
                        $response['Message'] = 'Kartu Tidak Ditemukan';
                        $response['StatusCode'] = '202';
                    }
                }
                else{
                    $response['Message'] = 'Location Not Found';
                    $response['StatusCode'] = '205';
                }
            }
            else{
                $response['Message'] = 'KID Tidak Ditemukan';
                $response['StatusCode'] = '404';
            }

            if($id != ''){
                $msg = json_encode($response);
                //save log api
                $qwe = "update [PaymentGateway].[dbo].[LogHitAPI] set Status = 1, DateProcess = '".$this->getDateAll()."',Message = '".$msg."' where ID = '".$id."'";
                $asd =  Yii::app()->db->createCommand($qwe);
                $asd->execute();
            }
        }
        else{
            $response['Message'] = 'Invalid Require Data';
            $response['StatusCode'] = '400';
        }

        echo json_encode($response);
    }

    public function actionTransitOut(){
        $response = array(
            'Status'=>false,
            'StatusCode'=>'',
            'Message'=>'',
        );

        if(isset($_REQUEST['KID']) and isset($_REQUEST['LocationID']) and isset($_REQUEST['CardID']) and isset($_REQUEST['VehicleType'])){
            $kid = $_REQUEST['KID'];
            $card_id = str_replace(' ','+',$_REQUEST['CardID']);
            $locationid = $_REQUEST['LocationID'];
            $vehicletype = $_REQUEST['VehicleType'];

            $parking = new Parking();
            $cardid = $parking->decrypt($card_id);

            $param = implode(',', $_REQUEST);

            //save log api
            $id = '';
            $qwe = "insert into [PaymentGateway].[dbo].[LogHitAPI](KID, Action, DateHit, Param, Status) values('".$kid."','".Yii::app()->controller->action->id."','".$this->getDateAll()."','".$param."',0)";
            $asd =  Yii::app()->db->createCommand($qwe);
            if($asd->execute()){
                $id = Yii::app()->db->getLastInsertID();
            }

            $yy = "select* from KoneksiKoperasiBaronang.dbo.ListKoperasi where KID = '$kid' and Status = 1";
            $zz =  Yii::app()->db->createCommand($yy)->queryAll();
            if($zz != null){
                //cek location
                $a = "select* from ".$zz[0]['DatabaseName'].".dbo.LocationMerchant where LocationID = '".$locationid."' and Status = 1";
                $b =  Yii::app()->db->createCommand($a)->queryAll();
                if($b != null){

                    $c = "select* from ".$zz[0]['DatabaseName'].".dbo.MasterCard where CardNo = '".$cardid."' and Status = 1 or RFIDNo = '".$cardid."' and Status = 1";
                    $d =  Yii::app()->db->createCommand($c)->queryAll();
                    if($d != null){
                        $cardno = $d[0]['CardNo'];

                        $e = "select* from ".$zz[0]['DatabaseName'].".dbo.MemberCard where CardNo = '".$cardno."' and Status = 1";
                        $f =  Yii::app()->db->createCommand($e)->queryAll();
                        if($f != null){
                            $g = "select* from ".$zz[0]['DatabaseName'].".dbo.VehicleType where Code = '".$vehicletype."' and Status = 1";
                            $h =  Yii::app()->db->createCommand($g)->queryAll();
                            if($h != null){
                                $vehicleid = $h[0]['VehicleID'];

                                $k = "select top 1 * from ".$zz[0]['DatabaseName'].".dbo.LocationTransit where LocationID = '".$locationid."' and VehicleTypeTo = '".$vehicletype."'";
                                $l =  Yii::app()->db->createCommand($k)->queryAll();
                                if($l != null){
                                    $vehicletypefrom = $l[0]['VehicleTypeFrom'];

                                    //cek lokasi transit harus masuk
                                    $i = "select top 1 * from ".$zz[0]['DatabaseName'].".dbo.LogTransitCard where LocationID = '".$locationid."' and CardNo = '".$cardno."' and VehicleTypeIn = '".$vehicletypefrom."' and Status = 0";
                                    $j =  Yii::app()->db->createCommand($i)->queryAll();
                                    if($j != null){

                                        $aa = "exec ".$zz[0]['DatabaseName'].".dbo.ProsesTransitOut '".$locationid."','".$cardno."','".$vehicletype."','".$vehicletypefrom."'";
                                        $bb =  Yii::app()->db->createCommand($aa);
                                        if($bb->execute()){
                                            $response['Status'] = true;
                                            $response['Message'] = 'Transaksi Berhasil Dilakukan';
                                            $response['StatusCode'] = '003';
                                        }
                                        else{
                                            $response['Message'] = 'Gagal Transit';
                                            $response['StatusCode'] = '202';
                                        }
                                    }
                                    else{
                                        $response['Message'] = 'Kartu Tidak Sesuai';
                                        $response['StatusCode'] = '202';
                                    }
                                }
                                else{
                                    $response['Message'] = 'Tidak Dapat Transit';
                                    $response['StatusCode'] = '202';
                                }
                            }
                            else{
                                $response['Message'] = 'Tipe Kendaraan Tidak Sesuai';
                                $response['StatusCode'] = '202';
                            }
                        }
                        else{
                            $response['Message'] = 'Kartu Tidak Valid';
                            $response['StatusCode'] = '202';
                        }
                    }
                    else{
                        $response['Message'] = 'Kartu Tidak Ditemukan';
                        $response['StatusCode'] = '202';
                    }
                }
                else{
                    $response['Message'] = 'Location Not Found';
                    $response['StatusCode'] = '205';
                }
            }
            else{
                $response['Message'] = 'KID Tidak Ditemukan';
                $response['StatusCode'] = '404';
            }

            if($id != ''){
                $msg = json_encode($response);
                //save log api
                $qwe = "update [PaymentGateway].[dbo].[LogHitAPI] set Status = 1, DateProcess = '".$this->getDateAll()."',Message = '".$msg."' where ID = '".$id."'";
                $asd =  Yii::app()->db->createCommand($qwe);
                $asd->execute();
            }
        }
        else{
            $response['Message'] = 'Invalid Require Data';
            $response['StatusCode'] = '400';
        }

        echo json_encode($response);
    }

    public function actionGateOut(){
        $response = array(
            'Status'=>false,
            'StatusCode'=>'',
            'Message'=>'',
        );

        if(isset($_REQUEST['KID']) and isset($_REQUEST['CardID']) and isset($_REQUEST['Point']) and isset($_REQUEST['Duration']) and isset($_REQUEST['LocationID'])){
            $kid = $_REQUEST['KID'];
            $card_id = str_replace(' ','+',$_REQUEST['CardID']);
            $point = $_REQUEST['Point'];
            $duration = $_REQUEST['Duration'];
            $locationid = $_REQUEST['LocationID'];

            $parking = new Parking();
            $cardid = $parking->decrypt($card_id);

            $param = implode(',', $_REQUEST);

            //save log api
            $id = '';
            $qwe = "insert into [PaymentGateway].[dbo].[LogHitAPI](KID, Action, DateHit, Param, Status) values('".$kid."','".Yii::app()->controller->action->id."','".$this->getDateAll()."','".$param."',0)";
            $asd =  Yii::app()->db->createCommand($qwe);
            if($asd->execute()){
                $id = Yii::app()->db->getLastInsertID();
            }

            $yy = "select* from KoneksiKoperasiBaronang.dbo.ListKoperasi where KID = '$kid' and Status = 1";
            $zz =  Yii::app()->db->createCommand($yy)->queryAll();
            if($zz != null){

                    //cek location
                    $sql2 = "select* from ".$zz[0]['DatabaseName'].".dbo.LocationMerchant where LocationID = '".$locationid."' and Status = 1";
                    $exec2 =  Yii::app()->db->createCommand($sql2)->queryAll();
                    if($exec2 != null){

                        //cek balance
                        $sql = "select* from ".$zz[0]['DatabaseName'].".dbo.MasterCard where CardNo = '".$cardid."' and Status = 1 or RFIDNo = '".$cardid."' and Status = 1";
                        $exec =  Yii::app()->db->createCommand($sql)->queryAll();
                        if($exec != null){
                            $cardno = $exec[0]['CardNo'];

                            $sqla = "select top 1 * from ".$zz[0]['DatabaseName'].".dbo.ParkingList where CardNo = '".$cardno."' and LocationIn = '".$locationid."' and Status = 0 order by TimeIn desc";
                            $execa =  Yii::app()->db->createCommand($sqla)->queryAll();
                            if($execa != null){
                                $transid = $execa[0]['TransID'];
                                $vehicletype = $execa[0]['VehicleType'];
                                $timein = $execa[0]['TimeIn'];

                                $sqly = "select* from ".$zz[0]['DatabaseName'].".dbo.VehicleType where Code = '".$vehicletype."'";
                                $execy =  Yii::app()->db->createCommand($sqly)->queryAll();
                                if($execy != null){
                                    $vehicleid = $execy[0]['VehicleID'];

                                    $sql2 = "select* from ".$zz[0]['DatabaseName'].".dbo.MemberCard where CardNo = '".$cardno."' and Status = 1";
                                    $exec2 =  Yii::app()->db->createCommand($sql2)->queryAll();
                                    if($exec2 != null){
                                        $member = 0;
                                        $gp = 0;
                                        $year = date('Y');
                                        $month = date('m');
                                        $sq2 = "select* from ".$zz[0]['DatabaseName'].".dbo.MemberLocationMonth where CardNo = '".$cardno."' and LocationID = '".$locationid."' and VehicleID = '".$vehicleid."' and Year = '".$year."' and Bulan = '".$month."' and Status = 1";
                                        $exe2 =  Yii::app()->db->createCommand($sq2)->queryAll();
                                        if($exe2 != null){
                                            $point = 0;
                                            $member = 1;
                                            $response['Member'] = true;
                                        }
                                        else{
                                            $member = 0;
                                            $response['Member'] = false;

                                            //jika kartu transit
                                            $sqr = "select top 1 * from ".$zz[0]['DatabaseName'].".dbo.LocationTransit where LocationID = '".$locationid."' and VehicleTypeFrom = '".$vehicletype."' and Status = 1";
                                            $exer =  Yii::app()->db->createCommand($sqr)->queryAll();
                                            if($exer != null){
                                                $vto = $exer[0]['VehicleTypeTo'];

                                                $sqlh = "select* from ".$zz[0]['DatabaseName'].".dbo.VehicleType where Code = '".$vto."'";
                                                $exech =  Yii::app()->db->createCommand($sqlh)->queryAll();
                                                if($exech != null){
                                                    $vhid = $exech[0]['VehicleID'];

                                                    $sqt2 = "select* from ".$zz[0]['DatabaseName'].".dbo.MemberLocationMonth where CardNo = '".$cardno."' and LocationID = '".$locationid."' and VehicleID = '".$vhid."' and Year = '".$year."' and Bulan = '".$month."' and Status = 1";
                                                    $exet2 =  Yii::app()->db->createCommand($sqt2)->queryAll();
                                                    if($exet2 != null){
                                                        $point = 0;
                                                    }
                                                }
                                            }
                                        }

                                        $sq3 = "select* from ".$zz[0]['DatabaseName'].".dbo.MemberLocationGP where CardNo = '".$cardno."' and LocationID = '".$locationid."' and VehicleID = '".$vehicleid."' and Status = 1";
                                        $exe3 =  Yii::app()->db->createCommand($sq3)->queryAll();
                                        if($exe3 != null){
                                            $maxgp = 0;
                                            $sq4 = "select* from ".$zz[0]['DatabaseName'].".dbo.LocationGP where LocationID = '".$locationid."' and VehicleID = '".$vehicleid."' and Status = 1";
                                            $exe4 =  Yii::app()->db->createCommand($sq4)->queryAll();
                                            if($exe4 != null){
                                                $maxgp = $exe4[0]['GPMinutes'];
                                            }

                                            $sq5 = "select DATEDIFF(minute, TimeIn, getDate()) as LamaWaktu from ".$zz[0]['DatabaseName'].".dbo.ParkingList where CardNo='".$cardno."' and Status = 0";
                                            $exe5 =  Yii::app()->db->createCommand($sq5)->queryAll();
                                            if($exe5 != null){
                                                if($exe5[0]['LamaWaktu'] <= $maxgp){
                                                  $point = 0;
                                                  $gp = 1;
                                                  $response['GP'] = true;
                                                }
                                            }
                                        }
                                        else{
                                            $gp = 0;
                                            $response['GP'] = false;
                                        }

                                        $finalpoint = 0;
                                        $afg = "select top 1 * from ".$zz[0]['DatabaseName'].".dbo.ParkingSetting";
                                        $zcv =  Yii::app()->db->createCommand($afg)->queryAll();
                                        if($zcv != null){
                                            $value = $zcv[0]['ValueConversion'];

                                            $finalpoint = round($point/$value);
                                        }


                                        if($finalpoint <= $exec2[0]['Point'] || $gp == 1){
                                            //debet emoney
                                            $s = "exec ".$zz[0]['DatabaseName'].".[dbo].[ProsesDeductCard] '".$cardno."','".$finalpoint."','".$duration."','".$locationid."'";
                                            $e =  Yii::app()->db->createCommand($s);
                                            if($e->execute()){
                                                $response['Status'] = true;
                                                $response['StatusCode'] = '003';
                                                $response['CardID'] = $cardid;
                                                $response['PaidDate'] = date('Y-m-d H:i:s');
                                                $response['Balance'] = number_format($exec2[0]['Point'] - $finalpoint);
                                                $response['Point'] = number_format($finalpoint);
                                                $response['Rupiah'] = $point;
                                                $response['Message'] = 'Transaksi Berhasil Dilakukan';
                                            }
                                            else{
                                                $response['Message'] = 'Transaksi Tidak Dapat Dilakukan';
                                                $response['StatusCode'] = '200';
                                            }
                                        }
                                        else{
                                            $response['Message'] = 'Saldo Tidak Cukup';
                                            $response['StatusCode'] = '201';
                                        }
                                    }
                                    else{
                                        $response['Message'] = 'Kartu Tidak Valid';
                                        $response['StatusCode'] = '202';
                                    }
                                }
                                else{
                                    $response['Message'] = 'Kendaraan Tidak Sesuai';
                                    $response['StatusCode'] = '204';
                                }
                            }
                            else{
                                $response['Message'] = 'Kartu Yang Digunakan Tidak Benar';
                                $response['StatusCode'] = '206';
                            }
                        }
                        else{
                            $response['Message'] = 'Kartu Tidak Ditemukan';
                            $response['StatusCode'] = '202';
                        }
                    }
                    else{
                        $response['Message'] = 'Location Not Found';
                        $response['StatusCode'] = '205';
                    }

            }
            else{
                $response['Message'] = 'KID Tidak Ditemukan';
                $response['StatusCode'] = '404';
            }

            if($id != ''){
                $msg = json_encode($response);
                //save log api
                $qwe = "update [PaymentGateway].[dbo].[LogHitAPI] set Status = 1, DateProcess = '".$this->getDateAll()."',Message = '".$msg."' where ID = '".$id."'";
                $asd =  Yii::app()->db->createCommand($qwe);
                $asd->execute();
            }
        }
        else{
            $response['Message'] = 'Invalid Require Data';
            $response['StatusCode'] = '400';
        }

        echo json_encode($response);
    }

    public function actionCheckOut(){
        $response = array(
            'Status'=>false,
            'StatusCode'=>'',
            'Message'=>'',
        );

        if(isset($_REQUEST['KID']) and isset($_REQUEST['CardID']) and isset($_REQUEST['Point']) and isset($_REQUEST['LocationID'])){
            $kid = $_REQUEST['KID'];
            $card_id = str_replace(' ','+',$_REQUEST['CardID']);
            $point = $_REQUEST['Point'];
            $locationid = $_REQUEST['LocationID'];

            $parking = new Parking();
            $cardid = $parking->decrypt($card_id);

            $param = implode(',', $_REQUEST);

            //save log api
            $id = '';
            $qwe = "insert into [PaymentGateway].[dbo].[LogHitAPI](KID, Action, DateHit, Param, Status) values('".$kid."','".Yii::app()->controller->action->id."','".$this->getDateAll()."','".$param."',0)";
            $asd =  Yii::app()->db->createCommand($qwe);
            if($asd->execute()){
                $id = Yii::app()->db->getLastInsertID();
            }

            $yy = "select* from KoneksiKoperasiBaronang.dbo.ListKoperasi where KID = '$kid' and Status = 1";
            $zz =  Yii::app()->db->createCommand($yy)->queryAll();
            if($zz != null){

                    //cek location
                    $sql2 = "select* from ".$zz[0]['DatabaseName'].".dbo.LocationMerchant where LocationID = '".$locationid."' and Status = 1";
                    $exec2 =  Yii::app()->db->createCommand($sql2)->queryAll();
                    if($exec2 != null){

                        //cek balance
                        $sql = "select* from ".$zz[0]['DatabaseName'].".dbo.MasterCard where CardNo = '".$cardid."' and Status = 1 or RFIDNo = '".$cardid."' and Status = 1";
                        $exec =  Yii::app()->db->createCommand($sql)->queryAll();
                        if($exec != null){
                            $cardno = $exec[0]['CardNo'];

                            $response['CardID'] = $cardid;

                            $sqla = "select* from ".$zz[0]['DatabaseName'].".dbo.ParkingList where CardNo = '".$cardno."' and LocationIn = '".$locationid."' and Status = 0";
                            $execa =  Yii::app()->db->createCommand($sqla)->queryAll();
                            if($execa != null){
                                $transid = $execa[0]['TransID'];
                                $vehicletype = $execa[0]['VehicleType'];
                                $timein = date('Y-m-d H:i:s', strtotime($execa[0]['TimeIn']));

                                $sqly = "select* from ".$zz[0]['DatabaseName'].".dbo.VehicleType where Code = '".$vehicletype."'";
                                $execy =  Yii::app()->db->createCommand($sqly)->queryAll();
                                if($execy != null){
                                    $vehicleid = $execy[0]['VehicleID'];

                                    $sql2 = "select* from ".$zz[0]['DatabaseName'].".dbo.MemberCard where CardNo = '".$cardno."' and Status = 1";
                                    $exec2 =  Yii::app()->db->createCommand($sql2)->queryAll();
                                    if($exec2 != null){
                                        $member = 0;
                                        $year = date('Y');
                                        $month = date('m');
                                        $sq2 = "select* from ".$zz[0]['DatabaseName'].".dbo.MemberLocationMonth where CardNo = '".$cardno."' and LocationID = '".$locationid."' and VehicleID = '".$vehicleid."' and Year = '".$year."' and Bulan = '".$month."' and Status = 1";
                                        $exe2 =  Yii::app()->db->createCommand($sq2)->queryAll();
                                        if($exe2 != null){
                                            $point = 0;
                                            $member = 1;
                                            $response['Member'] = true;
                                        }
                                        else{
                                            $member = 0;
                                            $response['Member'] = false;

                                            //jika kartu transit
                                            $sqr = "select top 1 * from ".$zz[0]['DatabaseName'].".dbo.LocationTransit where LocationID = '".$locationid."' and VehicleTypeFrom = '".$vehicletype."' and Status = 1";
                                            $exer =  Yii::app()->db->createCommand($sqr)->queryAll();
                                            if($exer != null){
                                                $vto = $exer[0]['VehicleTypeTo'];

                                                $sqlh = "select* from ".$zz[0]['DatabaseName'].".dbo.VehicleType where Code = '".$vto."'";
                                                $exech =  Yii::app()->db->createCommand($sqlh)->queryAll();
                                                if($exech != null){
                                                    $vhid = $exech[0]['VehicleID'];

                                                    $sqt2 = "select* from ".$zz[0]['DatabaseName'].".dbo.MemberLocationMonth where CardNo = '".$cardno."' and LocationID = '".$locationid."' and VehicleID = '".$vhid."' and Year = '".$year."' and Bulan = '".$month."' and Status = 1";
                                                    $exet2 =  Yii::app()->db->createCommand($sqt2)->queryAll();
                                                    if($exet2 != null){
                                                        $point = 0;
                                                    }
                                                }
                                            }
                                        }

                                        $sq3 = "select* from ".$zz[0]['DatabaseName'].".dbo.MemberLocationGP where CardNo = '".$cardno."' and LocationID = '".$locationid."' and VehicleID = '".$vehicleid."' and Status = 1";
                                        $exe3 =  Yii::app()->db->createCommand($sq3)->queryAll();
                                        if($exe3 != null){
                                            $maxgp = 0;
                                            $sq4 = "select* from ".$zz[0]['DatabaseName'].".dbo.LocationGP where LocationID = '".$locationid."' and VehicleID = '".$vehicleid."' and Status = 1";
                                            $exe4 =  Yii::app()->db->createCommand($sq4)->queryAll();
                                            if($exe4 != null){
                                                $maxgp = $exe4[0]['GPMinutes'];
                                            }

                                            $sq5 = "select DATEDIFF(minute, TimeIn, getDate()) as LamaWaktu from ".$zz[0]['DatabaseName'].".dbo.ParkingList where CardNo='".$cardno."' and Status = 0";
                                            $exe5 =  Yii::app()->db->createCommand($sq5)->queryAll();
                                            if($exe5 != null){
                                                if($exe5[0]['LamaWaktu'] <= $maxgp){
                                                  $point = 0;
                                                  $gp = 1;
                                                  $response['GP'] = true;
                                                }
                                            }
                                        }
                                        else{
                                            $gp = 0;
                                            $response['GP'] = false;
                                        }

                                        $finalpoint = 0;
                                        $afg = "select top 1 * from ".$zz[0]['DatabaseName'].".dbo.ParkingSetting";
                                        $zcv =  Yii::app()->db->createCommand($afg)->queryAll();
                                        if($zcv != null){
                                            $value = $zcv[0]['ValueConversion'];

                                            $finalpoint = round($point/$value);
                                        }

                                        if($finalpoint <= $exec2[0]['Point'] || $gp == 1){
                                            if(isset($_REQUEST['ReffNo']) and isset($_REQUEST['TimeIn'])){
                                                $reff = $_REQUEST['ReffNo'];
                                                $timein = $_REQUEST['TimeIn'];

                                                $abb = "select * from ".$zz[0]['DatabaseName'].".dbo.ParkingReff where TransID = '".$transid."' and ReffNo = '".$reff."'";
                                                $zbb =  Yii::app()->db->createCommand($abb)->queryAll();
                                                if($zbb == null){
                                                    $aww = "insert into ".$zz[0]['DatabaseName'].".dbo.ParkingReff(TransID, ReffNo, DateReceive, Status) values('".$transid."','".$reff."','".$timein."',1)";
                                                    $zww =  Yii::app()->db->createCommand($aww);
                                                    $zww->execute();
                                                }
                                            }

                                            $response['Status'] = true;
                                            $response['StatusCode'] = '000';
                                            $response['CardID'] = $cardid;
                                            $response['VehicleType'] = $vehicletype;
                                            $response['TimeIn'] = $timein;
                                            $response['Message'] = 'Data ditemukan';
                                        }
                                        else{
                                            $response['Message'] = 'Saldo Tidak Cukup';
                                            $response['StatusCode'] = '201';
                                        }
                                    }
                                    else{
                                        $response['Message'] = 'Kartu Tidak Valid';
                                        $response['StatusCode'] = '202';
                                    }
                                }
                                else{
                                    $response['Message'] = 'Kendaraan Tidak Sesuai';
                                    $response['StatusCode'] = '204';
                                }
                            }
                            else{
                                $response['Message'] = 'Kartu Yang Digunakan Tidak Benar';
                                $response['StatusCode'] = '206';
                            }
                        }
                        else{
                            $response['Message'] = 'Kartu Tidak Ditemukan';
                            $response['StatusCode'] = '202';
                        }
                    }
                    else{
                        $response['Message'] = 'Location Not Found';
                        $response['StatusCode'] = '205';
                    }

            }
            else{
                $response['Message'] = 'KID Tidak Ditemukan';
                $response['StatusCode'] = '404';
            }

            if($id != ''){
                $msg = json_encode($response);
                //save log api
                $qwe = "update [PaymentGateway].[dbo].[LogHitAPI] set Status = 1, DateProcess = '".$this->getDateAll()."',Message = '".$msg."' where ID = '".$id."'";
                $asd =  Yii::app()->db->createCommand($qwe);
                $asd->execute();
            }
        }
        else{
            $response['Message'] = 'Invalid Require Data';
            $response['StatusCode'] = '400';
        }

        echo json_encode($response);
    }
}

?>
