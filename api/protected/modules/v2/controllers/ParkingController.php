<?php

class ParkingController extends Controller {

    public function getDateAll(){
        $t = microtime(true);
        $micro = sprintf("%06d",($t - floor($t)) * 1000000);
        $d = new DateTime( date('Y-m-d H:i:s.'.$micro, $t) );

        return $d->format("Y-m-d H:i:s.u");
    }

    public function actionGateIn(){
        $response = array(
            'Status'=>false,
            'Message'=>'',
            'StatusCode'=>'',
        );

        if(isset($_REQUEST['KID']) and isset($_REQUEST['CardID']) and isset($_REQUEST['GateID']) and isset($_REQUEST['MinimumPoint'])){
            $kid = $_REQUEST['KID'];
            $card_id = str_replace(' ','+',$_REQUEST['CardID']);
            $minimum_point = $_REQUEST['MinimumPoint'];
            $gateid = $_REQUEST['GateID'];

            $param = implode(',', $_REQUEST);

            //save log api
            $id = '';
            $qwe = "insert into [PaymentGateway].[dbo].[LogHitAPI](KID, Action, DateHit, Param, Status) values('".$kid."','".Yii::app()->controller->action->id."','".$this->getDateAll()."','".$param."',0)";
            $asd =  Yii::app()->db->createCommand($qwe);
            if($asd->execute()){
                $id = Yii::app()->db->getLastInsertID();
            }

            $yy = "select* from KoneksiKoperasiBaronang.dbo.ListKoperasi where KID = '$kid' and Status = 1";
            $zz =  Yii::app()->db->createCommand($yy)->queryAll();
            if($zz != null){

                //cek gate
                $sql1 = "select* from ".$zz[0]['DatabaseName'].".dbo.LocationGate where LocationGateID = '".$gateid."' and Status = 1";
                $exec1 =  Yii::app()->db->createCommand($sql1)->queryAll();
                if($exec1 != null){
                    $locationid = $exec1[0]['LocationID'];

                    $sql5 = "select* from ".$zz[0]['DatabaseName'].".dbo.LocationGateDetail where LocationGateID = '".$gateid."' and Status = 1";
                    $exec5 =  Yii::app()->db->createCommand($sql5)->queryAll();
                    if($exec5 != null){

                        //cek location
                        $sql2 = "select* from ".$zz[0]['DatabaseName'].".dbo.LocationMerchant where LocationID = '".$locationid."' and Status = 1";
                        $exec2 =  Yii::app()->db->createCommand($sql2)->queryAll();
                        if($exec2 != null){
                            $isOnlyMember = $exec2[0]['isOnlyMember'];

                            //cek balance
                            $sql = "select* from ".$zz[0]['DatabaseName'].".dbo.MasterCard where Barcode = '".$card_id."'";
                            $exec =  Yii::app()->db->createCommand($sql)->queryAll();
                            if($exec != null){
                                $cardno = $exec[0]['CardNo'];

                                $sql3 = "select* from ".$zz[0]['DatabaseName'].".dbo.MemberCard where CardNo = '".$cardno."' and Status = 1";
                                $exec3 =  Yii::app()->db->createCommand($sql3)->queryAll();
                                if($exec3 != null){
                                    $balance = $exec3[0]['Point'];

                                    //MinimumPoint
                                    $minimum = 0;
                                    $afg = "select top 1 * from ".$zz[0]['DatabaseName'].".dbo.ParkingSetting";
                                    $zcv =  Yii::app()->db->createCommand($afg)->queryAll();
                                    if($zcv != null){
                                        $value = $zcv[0]['ValueConversion'];

                                        $minimum = round($minimum_point/$value);
                                    }

                                    //cek member
                                    $year = date('Y');
                                    $month = date('m');
                                    $sq2 = "select * from ".$zz[0]['DatabaseName'].".dbo.MemberLocationMonth where CardNo = '".$cardno."' and LocationID ='".$locationid."' and Year = '".$year."' and Bulan = '".$month."' and Status = 1
                                            and VehicleID in(
                                              select VehicleID from ".$zz[0]['DatabaseName'].".dbo.LocationGateDetail where LocationGateID = '".$gateid."' and Status = 1
                                            )";
                                    $exe2 =  Yii::app()->db->createCommand($sq2)->queryAll();
                                    if($exe2 != null){
                                        $member = 1;
                                        $response['Member'] = true;
                                    }
                                    else{
                                        $member = 0;
                                        $response['Member'] = false;
                                    }

                                    if($isOnlyMember == 1){
                                        if($member == 1){
                                            $response['Status'] = true;
                                            $response['Message'] = 'Saldo Mencukupi';
                                            $response['StatusCode'] = '002';
                                            $response['CardID'] = $cardno;

                                            $qwee = "exec ".$zz[0]['DatabaseName'].".[dbo].[ProsesGateIn] '$cardno','$locationid','$gateid',$member";
                                            $asde =  Yii::app()->db->createCommand($qwee);
                                            if($asde->execute()){
                                                $response['Status'] = true;
                                                $response['Message'] = 'Saldo Mencukupi';
                                                $response['StatusCode'] = '002';
                                                $response['CardID'] = $cardno;
                                            }
                                            else{
                                                $response['Message'] = 'Failed In';
                                                $response['StatusCode'] = '200';
                                            }
                                        }
                                        else{
                                            $response['Message'] = 'Anda belum menjadi member dilokasi ini';
                                            $response['StatusCode'] = '207';
                                        }
                                    }
                                    else{
                                        if($member == 1){
                                            $response['Status'] = true;
                                            $response['Message'] = 'Saldo Mencukupi';
                                            $response['StatusCode'] = '002';
                                            $response['CardID'] = $cardno;

                                            $qwee = "exec ".$zz[0]['DatabaseName'].".[dbo].[ProsesGateIn] '$cardno','$locationid','$gateid',$member";
                                            $asde =  Yii::app()->db->createCommand($qwee);
                                            if($asde->execute()){
                                                $response['Status'] = true;
                                                $response['Message'] = 'Saldo Mencukupi';
                                                $response['StatusCode'] = '002';
                                                $response['CardID'] = $cardno;
                                            }
                                            else{
                                                $response['Message'] = 'Failed In';
                                                $response['StatusCode'] = '200';
                                            }
                                        }
                                        else if($member == 0){
                                            if($minimum <= $balance){
                                                $response['Status'] = true;
                                                $response['StatusCode'] = '001';
                                                $response['Message'] = 'Saldo Mencukupi';
                                                $response['CardID'] = $cardno;

                                                $qwee = "exec ".$zz[0]['DatabaseName'].".[dbo].[ProsesGateIn] '$cardno','$locationid','$gateid',$member";
                                                $asde =  Yii::app()->db->createCommand($qwee);
                                                if($asde->execute()){
                                                    $response['Status'] = true;
                                                    $response['StatusCode'] = '001';
                                                    $response['Message'] = 'Saldo Mencukupi';
                                                    $response['CardID'] = $cardno;
                                                }
                                                else{
                                                    $response['Message'] = 'Failed In';
                                                    $response['StatusCode'] = '200';
                                                }
                                            }
                                            else{
                                                $response['Message'] = 'Saldo Tidak Cukup';
                                                $response['StatusCode'] = '201';
                                            }
                                        }
                                        else{
                                            $response['Message'] = 'Failed In';
                                            $response['StatusCode'] = '200';
                                        }
                                    }
                                }
                                else{
                                    $response['Message'] = 'Kartu Tidak Ditemukan';
                                    $response['StatusCode'] = '202';
                                }
                            }
                            else{
                                $response['Message'] = 'Kartu Tidak Ditemukan';
                                $response['StatusCode'] = '202';
                            }
                        }
                        else{
                            $response['Message'] = 'Location Not Found';
                            $response['StatusCode'] = '205';
                        }
                    }
                    else{
                        $response['Message'] = 'Gate Not Valid';
                        $response['StatusCode'] = '206';
                    }
                }
                else{
                    $response['Message'] = 'Gate Not Valid';
                    $response['StatusCode'] = '206';
                }
            }
            else{
                $response['Message'] = 'KID Not Found';
                $response['StatusCode'] = '404';
            }

            if($id != ''){
                $msg = 'OK';

                //save log api
                $qwe = "update [PaymentGateway].[dbo].[LogHitAPI] set Status = 1, DateProcess = '".$this->getDateAll()."',Message = '".$msg."' where ID = '".$id."'";
                $asd =  Yii::app()->db->createCommand($qwe);
                $asd->execute();
            }
        }
        else{
            $response['Message'] = 'Invalid Require Data';
            $response['StatusCode'] = '400';
        }

        echo json_encode($response);
    }

    public function actionCekBayar(){
        $response = array(
            'Status'=>false,
            'Message'=>'',
            'TicketNumber'=>'',
            'DateIn'=>'',
            'DateOut'=>'',
            'Point'=>'',
            'Duration'=>'',
            'VehicleType'=>'',
        );

        if(isset($_REQUEST['KID']) and isset($_REQUEST['TicketNumber'])){
            $kid = $_REQUEST['KID'];
            $ticket = $_REQUEST['TicketNumber'];

            //save log api
            $qwe = "insert into [PaymentGateway].[dbo].[LogAPI](Tanggal, Action, KID, Tipe) values('".$this->getDateAll()."','".Yii::app()->controller->action->id."','".$kid."',0)";
            $asd =  Yii::app()->db->createCommand($qwe);
            $asd->execute();

            $card = array('1000100010001001','1000100010001002','1000100010001003','1000100010001004','1000100010001006');
            if(in_array($ticket, $card)){
                  $response['Status'] = true;
                  $response['Point'] = rand(10,20);
                  $response['DateIn'] = date('Y-m-d H:i:s', strtotime('-3 hour'));
                  $response['DateOut'] = date('Y-m-d H:i:s');
                  $response['Duration'] = 3;
                  $response['VehicleType'] = 'Motor';
            }
            else{
                $response['Message'] = 'Ticket Number Not Found';
            }

            $response['TicketNumber'] = $ticket;

            //save log api
            $qwe = "insert into [PaymentGateway].[dbo].[LogAPI](Tanggal, Action, KID, Tipe) values('".$this->getDateAll()."','".Yii::app()->controller->action->id."','".$kid."',1)";
            $asd =  Yii::app()->db->createCommand($qwe);
            $asd->execute();
        }
        else{
            $response['Message'] = 'Invalid Require Data';
        }

        echo json_encode($response);
    }

    public function actionLockIn(){
        $response = array(
            'Status'=>false,
            'StatusCode'=>'',
            'Message'=>'',
            'CardID'=>'',
            'DateIn'=>'',
        );

        if(isset($_REQUEST['KID']) and isset($_REQUEST['CardID']) and isset($_REQUEST['LocationID'])){
            $kid = $_REQUEST['KID'];
            $cardid = str_replace(' ','+',$_REQUEST['CardID']);
            $locationid = $_REQUEST['LocationID'];

            $param = implode(',', $_REQUEST);

            //save log api
            $qwe = "insert into [PaymentGateway].[dbo].[LogAPI](Tanggal, Action, KID, Tipe, Param) values('".$this->getDateAll()."','".Yii::app()->controller->action->id."','".$kid."',0,'".$param."')";
            $asd =  Yii::app()->db->createCommand($qwe);
            $asd->execute();

            $yy = "select* from KoneksiKoperasiBaronang.dbo.ListKoperasi where KID = '$kid' and Status = 1";
            $zz =  Yii::app()->db->createCommand($yy)->queryAll();
            if($zz != null){
                $sql = "select* from ".$zz[0]['DatabaseName'].".dbo.MasterCard where Barcode = '".$cardid."' and Status = 1";
                $exec =  Yii::app()->db->createCommand($sql)->queryAll();
                if($exec != null){
                    $cardno = $exec[0]['CardNo'];

                    $sq = "select* from ".$zz[0]['DatabaseName'].".dbo.MemberCard where CardNo = '".$cardno."' and Status = 1";
                    $exe =  Yii::app()->db->createCommand($sq)->queryAll();
                    if($exe != null){
                        $tanggal = date('Y-m-d H:i:s');

                        $qwee = "insert into ".$zz[0]['DatabaseName'].".[dbo].[GateLog](CardNo, LocationID, Status, DateIn) values('$cardno','$locationid',0,'$tanggal')";
                        $asde =  Yii::app()->db->createCommand($qwee);
                        if($asde->execute()){
                            $response['Status'] = true;
                            $response['StatusCode'] = '000';
                            $response['Message'] = 'Berhasil dilakukan';
                            $response['CardID'] = $cardno;
                            $response['DateIn'] = $tanggal;
                        }
                        else{
                            $response['Message'] = 'Failed In';
                            $response['StatusCode'] = '200';
                        }
                    }
                    else{
                      $response['StatusCode'] = '202';
                      $response['Message'] = 'Card not valid';
                    }
                }
                else{
                  $response['StatusCode'] = '202';
                  $response['Message'] = 'Card not registered';
                }
            }
            else{
              $response['StatusCode'] = '404';
              $response['Message'] = 'KID not valid';
            }

            $msg = json_encode($response);

            //save log api
            $qwe = "insert into [PaymentGateway].[dbo].[LogAPI](Tanggal, Action, KID, Tipe, Message) values('".$this->getDateAll()."','".Yii::app()->controller->action->id."','".$kid."',1,'".$msg."')";
            $asd =  Yii::app()->db->createCommand($qwe);
            $asd->execute();        }
        else{
            $response['Message'] = 'Invalid Require Data';
        }

        echo json_encode($response);
    }

    public function actionLockOut(){
        $response = array(
            'Status'=>false,
            'StatusCode'=>'',
            'Message'=>'',
            'CardID'=>'',
            'DateOut'=>'',
        );

        if(isset($_REQUEST['KID']) and isset($_REQUEST['CardID']) and isset($_REQUEST['LocationID'])){
            $kid = $_REQUEST['KID'];
            $cardid = str_replace(' ','+',$_REQUEST['CardID']);
            $locationid = $_REQUEST['LocationID'];

            $param = implode(',', $_REQUEST);

            //save log api
            $qwe = "insert into [PaymentGateway].[dbo].[LogAPI](Tanggal, Action, KID, Tipe, Param) values('".$this->getDateAll()."','".Yii::app()->controller->action->id."','".$kid."',0,'".$param."')";
            $asd =  Yii::app()->db->createCommand($qwe);
            $asd->execute();

            $yy = "select* from KoneksiKoperasiBaronang.dbo.ListKoperasi where KID = '$kid' and Status = 1";
            $zz =  Yii::app()->db->createCommand($yy)->queryAll();
            if($zz != null){
                $sql = "select* from ".$zz[0]['DatabaseName'].".dbo.MasterCard where Barcode = '".$cardid."' and Status = 1";
                $exec =  Yii::app()->db->createCommand($sql)->queryAll();
                if($exec != null){
                    $cardno = $exec[0]['CardNo'];

                    $sq = "select* from ".$zz[0]['DatabaseName'].".dbo.MemberCard where CardNo = '".$cardno."' and Status = 1";
                    $exe =  Yii::app()->db->createCommand($sq)->queryAll();
                    if($exe != null){
                        $tanggal = date('Y-m-d H:i:s');

                        $qwee = "update into ".$zz[0]['DatabaseName'].".[dbo].[GateLog] set Status = 1 where CardNo= '$cardno' and LocationID = '$locationid' and Status = 0)";
                        $asde =  Yii::app()->db->createCommand($qwee);
                        if($asde->execute()){
                            $response['Status'] = true;
                            $response['StatusCode'] = '000';
                            $response['Message'] = 'Berhasil dilakukan';
                            $response['CardID'] = $cardno;
                            $response['DateOut'] = $tanggal;
                        }
                        else{
                            $response['Message'] = 'Failed In';
                            $response['StatusCode'] = '200';
                        }
                    }
                    else{
                      $response['StatusCode'] = '202';
                      $response['Message'] = 'Card not valid';
                    }
                }
                else{
                  $response['StatusCode'] = '202';
                  $response['Message'] = 'Card not registered';
                }
            }
            else{
              $response['StatusCode'] = '404';
              $response['Message'] = 'KID not valid';
            }

            $msg = json_encode($response);

            //save log api
            $qwe = "insert into [PaymentGateway].[dbo].[LogAPI](Tanggal, Action, KID, Tipe, Message) values('".$this->getDateAll()."','".Yii::app()->controller->action->id."','".$kid."',1,'".$msg."')";
            $asd =  Yii::app()->db->createCommand($qwe);
            $asd->execute();
        }
        else{
            $response['Message'] = 'Invalid Require Data';
        }

        echo json_encode($response);
    }
}

?>
