<h1>Create <?php echo $this->class2name($this->modelClass); ?></h1>
<hr />

<?php echo "<?php\n"; ?>$this->widget('ext.widgets.form.FormLinkButtons', array(
	'linkButtons'=>array(
		array('label'=>'List', 'url'=>array('index'), 'access'=>array('<?php echo $this->modelClass; ?>Index')),
		array('label'=>'Create', 'url'=>array('create'), 'access'=>array('<?php echo $this->modelClass; ?>Create')),
	),
)); ?>

<?php echo "<?php echo \$this->renderPartial('_form', array(
	'model'=>\$model,
)); ?>"; ?>