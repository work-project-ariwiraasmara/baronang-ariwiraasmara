<?php

class GridViewHelper
{
    /**
     * Create Date in the gridview
     *
     * @param string $field
     * @param null|string $format
     * @return array
     */
    public static function formatDate($field, $format=null)
    {
        return array(
            'name'=>$field,
            'value'=>'DateHelper::formatDate($data->'. $field .', "'. $format .'")',
        );
    }

    /**
     * Create Date Time in the gridview
     *
     * @param string $field
     * @param null|string $format
     * @return array
     */
    public static function formatDateTime($field, $format=null)
    {
        return array(
            'name'=>$field,
            'value'=>'DateHelper::formatDateTime($data->'. $field .', "'. $format .'")',
        );
    }

	/**
	 * @param $field
	 * @param $enum
	 * @param bool $useFilter
	 * @return array
	 */
	public static function enumLabel($field, $enum, $useFilter=true)
    {
        return array(
            'name'=>$field,
            'filter'=>$useFilter ? $enum::getList() : false,
        );
    }

	/**
	 * @param $field
	 * @param $constants
	 * @param bool $useFilter
	 * @return array
	 */
	public static function constantLabel($field, $constants, $useFilter=true)
	{
		return array(
			'name'=>$field,
			'filter'=>$useFilter ? $constants : false,
		);
	}

    /**
     * Create Username in the gridview
     *
     * @param string $field
     * @return array
     */
    public static function username($field)
    {
        return array(
            'name'=>$field,
            'value'=>'Pengguna::model()->findByPk($data->'. $field .') === null ? null : Pengguna::model()->findByPk($data->'. $field .')->username',
        );
    }
}