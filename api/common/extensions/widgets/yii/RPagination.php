<?php
/**
 * RPagination class file.
 *
 * @link http://www.yiiframework.com/
 * @copyright Copyright &copy; 2008-2011 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

class RPagination extends CPagination
{
	/**
	 * The default page size.
	 */
	const DEFAULT_PAGE_SIZE=20;
}