<?php

class LookupCustomer extends Lookup
{
	public function init()
	{
		parent::init();		
		$this->initData();
		$this->title = 'Lookup Customer';				
	}
	
	public function initData()
	{
		$model = new CustomerSearch('search');
		$model->unsetAttributes(); // clear any default values
		if(isset($_GET['CustomerSearch']))
			$model->attributes=$_GET['CustomerSearch'];
		
		$this->defaultGridViewConfig = array(
			'id'=>'customer-grid',
			'cssFile' => $this->cssFile,
			'dataProvider'=>$model->searchLookup(),
			'filter'=>$model,
			'columns'=>array(
                            'name',
                            'phone',
                            'total_point',
			),
		);
	}
}