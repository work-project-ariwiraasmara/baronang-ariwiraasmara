<?php

class LookupScheduleForBooking extends Lookup
{
	public function init()
	{
		parent::init();		
		$this->initData();
		$this->title = 'Lookup Schedule';				
	}
	
	public function initData()
	{
		$model = new Schedule('search');
		$model->unsetAttributes(); // clear any default values
		if(isset($_GET['Schedule']))
			$model->attributes=$_GET['Schedule'];
		
		$this->defaultGridViewConfig = array(
			'id'=>'schedule-grid',
			'cssFile' => $this->cssFile,
			'dataProvider'=>$model->searchForBooking(),
			'filter'=>$model,
			'columns'=>array(
                            array(
                                'name'=> 'route_name',
                                'value'=>'$data->route->name',
                            ),
                            'departure_date',
                            'start_time',
                            array(
                                'header'=>'Kursi tersedia',
                                'value'=>' $data->vehicleType->seat_capacity - $data->bookedSeatCount',
                            ),
			),
		);
	}
}