<?php require('header.php');?>

<?php include("connect.inc");?>

<div class="row">
    <div class="col-sm-12">
        <?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
            <div class="alert alert-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-warning ?>"></i> <?php echo ucfirst($_SESSION['error-type']); ?></h4>
                <?php echo $_SESSION['error-message']; ?>
            </div>
        <?php } ?>

        <?php if(isset($_GET['view'])){ ?>
            <?php
            $x = "select* from [dbo].[ScheduleTraining] where Tanggal = '$_GET[view]'";
            $y = sqlsrv_query($conn, $x);
            $z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC);
            if($z == null){
                $_SESSION['error-message'] = 'Jadwal tidak ditemukan';
                $_SESSION['error-type'] = 'warning';
                $_SESSION['error-time'] = time()+5;
                echo "<script>window.location.href='mtraining.php'</script>";
            }
            else{
            ?>
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Daftar Peserta Tanggal <?php echo $_GET['view']; ?></h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <table class="table table-bordered table-striped">
                                <tr>
                                    <th>Nama Koperasi</th>
                                    <th>Email Koperasi</th>
                                    <th>Tanggal Training</th>
                                    <th>Status</th>
                                    <th>Tanggal Request</th>
                                    <th>Peserta</th>
                                    <th></th>
                                </tr>
                                <?php
                                $xx = "select* from [dbo].[TrainingRequest] where Tanggal = '$_GET[view]'";
                                $yy = sqlsrv_query($conn, $xx);
                                while($zz = sqlsrv_fetch_array($yy, SQLSRV_FETCH_NUMERIC)){
                                ?>
                                    <tr>
                                        <td><?php echo $zz[1]; ?></td>
                                        <td><?php echo $zz[0]; ?></td>
                                        <td><?php echo $zz[2]; ?></td>
                                        <td><?php echo $zz[3]; ?></td>
                                        <td><?php echo $zz[4]->format('Y-m-d H:i:s'); ?></td>
                                        <td>
                                            <ul>
                                            <?php
                                            $tanggal = $zz[2];
                                            $x = "select* from [dbo].[TrainingRequestDetail] where Tanggal = '$tanggal' and EmailKoperasi='$zz[0]'";
                                            $y = sqlsrv_query($conn, $x);
                                            while($z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC)){
                                            ?>
                                                <li><?php echo $z[0].' - '.$z[1]; ?></li>
                                            <?php } ?>
                                            </ul>
                                        </td>
                                        <td>
                                            <?php if($zz[3] == 0){ ?>
                                                <a href="proctraining.php?confirm=<?php echo $zz[0]; ?>&tgl=<?php echo $zz[2]; ?>"><button type="button" class="btn btn-success btn-xs"><i class="fa fa-reply"></i> Confirm</button></a>
                                                <a href="proctraining.php?del=<?php echo $zz[0]; ?>&tgl=<?php echo $zz[2]; ?>"><button type="button" class="btn btn-warning btn-xs"><i class="fa fa-times"></i> Reject</button></a>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        <?php }
        } ?>

        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Jadwal Training</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">

                        <table class="table table-bordered table-striped">
                            <tr>
                                <th>Tanggal</th>
                                <th>Jam</th>
                                <th>Kapasitas</th>
                                <th>Sisa</th>
                                <th></th>
                            </tr>
                            <?php
                            $tanggal = date('Y-m-d');
                            $a = "select* from [dbo].[ScheduleTraining] where Tanggal >= '$tanggal' order by Tanggal asc";
                            $b = sqlsrv_query($conn, $a);
                            while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                            ?>
                            <tr>
                                <td><?php echo $c[2]->format('Y-m-d'); ?></td>
                                <td><?php echo $c[0].' - '.$c[1]; ?></td>
                                <td><?php echo $c[3]; ?></td>
                                <td><?php echo $c[4]; ?></td>
                                <td>
                                    <a href="?view=<?php echo $c[2]->format('Y-m-d'); ?>"><button type="button" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i> View</button> </a>
                                </td>
                            </tr>
                            <?php } ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php require('footer.php');?>