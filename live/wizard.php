<?php require('header.php');?>

<?php include("connect.inc");?>

    <div class="row">
        <div class="col-md-12">
        <?php if(isset($_GET['edit'])){ ?>
            <div class="box box-primary">
                <form action="procwizard.php" method="post">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Wizard</h3>
                    </div>
                    <div class="box-body">
                        <?php
                        $step = $_GET['edit'];

                        $name = '';
                        $status = '';
                        $link = '';
                        $hint = '';
                        $x = "select* from [dbo].[WizSeting] where Step='$step'";
                        $y = sqlsrv_query($conn, $x);
                        $z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC);
                        if($z != null){
                            $name = $z[1];
                            $status = $z[2];
                            $link = $z[3];
                            $hint = $z[4];
                        }
                        ?>
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-3 control-label">Step</label>
                                <div class="col-sm-9">
                                    <?php echo $step; ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-3 control-label">Name</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" value="<?php echo $name; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-3 control-label">Status</label>
                                <div class="col-sm-4">
                                    <select name="status" class="form-control">
                                        <option value="0">Sekali Input</option>
                                        <option value="1">Berulang</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-3 control-label">Link</label>
                                <div class="col-sm-9">
                                    <?php echo $link; ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-3 control-label">Hint</label>
                                <div class="col-sm-9">
                                    <textarea name="hint" id="editor">

                                    </textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        <?php } else { ?>
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Wizard List</h3>
                </div>
                <div class="box-body">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>Step</th>
                            <th>Name</th>
                            <th>Status</th>
                            <th>Link</th>
                            <th></th>
                        </tr>
                        <?php
                        $a = "select* from [dbo].[WizSeting] order by Step asc";
                        $b = sqlsrv_query($conn, $a);
                        while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                            ?>
                            <tr>
                                <td><?php echo $c[0]; ?></td>
                                <td><?php echo $c[1]; ?></td>
                                <td>
                                    <?php
                                    if($c[2] == 0){
                                        echo 'Sekali Input';
                                    }
                                    else{
                                        echo 'Berulang';
                                    }
                                    ?>
                                </td>
                                <td><?php echo $c[3]; ?></td>
                                <td>
                                    <a href="?edit=<?php echo $c[0]; ?>"><button type="button" class="btn btn-primary btn-xs"><i class="fa fa-edit"></i> Edit</button></a>
                                </td>
                            </tr>
                        <?php } ?>
                    </table>
                </div>
            </div>
        </div>
        <?php } ?>
    </div>

<?php require('footer.php');?>