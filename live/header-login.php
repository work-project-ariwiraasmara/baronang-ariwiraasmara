<?php
session_start();
error_reporting(0);
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Baronang</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Google -->
    <meta name="google-site-verification" content="myMQsdKAV6knfGnFRzsZgh1DGvZ8VNDsX65hf60JEm4" />
    <!-- ICON -->
    <link href="static/images/Logo-fish-icon.png" rel="shortcut icon">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="static/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Morris chart -->
    <link rel="stylesheet" href="static/plugins/morris/morris.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="static/plugins/datepicker/datepicker3.css">
    <!-- daterange picker -->
    <link rel="stylesheet" href="static/plugins/daterangepicker/daterangepicker.css">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="static/plugins/iCheck/all.css">
    <!-- Bootstrap time Picker -->
    <link rel="stylesheet" href="static/plugins/timepicker/bootstrap-timepicker.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="static/plugins/select2/select2.min.css">
    <!-- Pace style -->
    <link rel="stylesheet" href="static/plugins/pace/pace.min.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="static/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="static/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="static/dist/css/skins/_all-skins.min.css">

    <!-- jQuery 2.1.4 -->
    <script src="static/plugins/jQuery/jQuery-2.1.4.min.js"></script>

    <style>
        .footer {
            text-align: center;
            position: fixed;
            left: 0;
            bottom: 0;
            width: 100%;
        }

        .back {
            background: url("assets/baronang.png") no-repeat center center fixed;
            background-size: 50% 70%;
            min-height: 650px;
        }
    </style>
</head>
<body class="hold-transition skin-blue layout-top-nav fixed">
<?php
if(isset($_SESSION['error-time'])){
    if($_SESSION['error-time'] <= time()){
        unset($_SESSION['error-time']);
        unset($_SESSION['error-type']);
        unset($_SESSION['error-message']);
    }
}
?>
<div class="">
    <header class="main-header">
        <nav class="navbar navbar-static-top" role="navigation" style="padding-left: 5%;">
            <a href="#" class="pull-left">
                <img src ="static/images/Logo_White.png" class="img-responsive">
            </a>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown notifications-menu">
                        <a href="index.php">
                            Home
                        </a>
                    </li>
                </ul>
                <ul class="nav navbar-nav">
                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown notifications-menu">
                        <a href="contactus.php">
                            Support
                        </a>
                    </li>
                </ul>
                <ul class="nav navbar-nav">
                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown notifications-menu">
                        <a href="caradaftar.php">
                            Cara Pendaftaran?
                        </a>
                    </li>
                </ul>
                <ul class="nav navbar-nav">
                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown notifications-menu">
                        <a href="video.php">
                            Video
                        </a>
                    </li>
                </ul>
                <ul class="nav navbar-nav">
                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown notifications-menu">
                        <a href="training.php">
                            Training Koperasi
                        </a>
                    </li>
                </ul>
                <ul class="nav navbar-nav">
                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown notifications-menu">
                        <a href="https://baronang.com/koperasi/login.php">
                            Login Akun Koperasi
                        </a>
                    </li>
                </ul>
                <ul class="nav navbar-nav">
                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown notifications-menu">
                        <a href="karir.php">
                            Karir
                        </a>
                    </li>
                </ul>
                <ul class="nav navbar-nav">
                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown notifications-menu">
                        <a href="https://baronang.com/koperasi/register.php">
                            <button type="button" class="btn btn-block btn-default btn-xs">Daftar Akun Koperasi</button>
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
