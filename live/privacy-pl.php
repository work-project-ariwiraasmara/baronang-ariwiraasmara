<?php require('header-main.php');?>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary col-md-6">
                <div class="box-header with-border">
                    <h3 class="box-title">Kebijakan Privasi</h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <b>BARONANG</b> menghormati hal-hal yang berkenaan dengan perlindungan data dan privasi Anda.
                            <br>
                            Oleh karena itu, Kami hanya akan menggunakan nama Anda dan data lainnya yang berhubungan dengan Anda sesuai dengan kebijakan privasi ini.
                            Kami hanya mengumpulkan Data yang penting bagi Kami dan Kami hanya akan mengumpulkan Data yang dibutuhkan untuk segala sesuatu yang berhubungan dengan Anda. Kami hanya akan menyimpan Data Anda selama dibutuhkan untuk memenuhi kewajiban hukum atau selama Data tersebut berhubungan dengan tujuan-tujuan yang ada saat Data dikumpulkan. Kebijakan privasi Kami mengikuti kebijakan perundang-undangan yang berlaku untuk menjaga kerahasiaan data kecuali kami diwajibkan oleh hukum yang berlaku untuk membuka data kepada pihak ketiga yang memiliki kewenangan seperti pemerintah atau lembaga pemerintah lainnya, hanya jika ada perintah yang sah.
                            <br>
                            <h3>Pengumpulan Informasi Personal</h3>
                            Data pribadi adalah data yang dapat digunakan untuk mengenali atau menghubungi seseorang. Saat Anda membuat Akun pengguna <b>BARONANG</b>, Anda akan diminta untuk memberikan Data pribadi yang kami butuhkan termasuk namun tidak terbatas pada:
                            <br>
                            <ul style="list-style-type:disc">
                                <li>Nama Lengkap</li>
                                <li>Alamat Surel (E-mail)</li>
                                <li>Nomor Telepon Genggam</li>
                                <li>Tanggal Lahir</li>
                                <li>Nomor Kartu Tanda Penduduk</li>
                            </ul>
                            <br>
                            Data yang kami peroleh langsung dari Anda melalui pengisian di Baronang App. Data yang dikumpulkan ketika Anda menggunakan Baronang App yang mungkin Kami gunakan untuk mengidentifikasi Anda dan mengesahkan pengguna Baronang App dengan tujuan pencegahan kehilangan dan penipuan.
                            Lebih dari itu, Kami akan menggunakan Data yang Anda berikan untuk urusan administrasi Akun Anda dengan Kami untuk keperluan verifikasi dan mengelola transaksi yang berhubungan dengan Baronang App, melakukan riset mengenai data demografis pengguna Baronang App, mengembangkan Baronang App, mengirimkan Anda informasi yang Kami anggap berguna untuk Anda termasuk informasi tentang layanan dari Kami setelah Anda memberikan persetujuan kepada Kami bahwa Anda tidak keberatan dihubungi mengenai layanan Kami.
                            Kami dapat menggunakan informasi pribadi Anda, termasuk tanggal lahir untuk memverifikasi identitas, membantu pengguna, menentukan Layanan yang tepat untuk Anda dan bermanfaat kepada Anda serta untuk memahami bagian yang paling menarik dari Baronang App.
                            Transaksi yang Anda lakukan melalui Baronang App diproses oleh PT Baronang Asia Pasifik. Anda harus memberikan Data yang akurat dan tidak menyesatkan kepada Kami. Anda harus memperbaharui dan memberitahukan kepada Kami apabila ada perubahan Data yang terkait dengan Anda. Rincian transaksi Anda akan Kami simpan untuk alasan keamanan Kami. Anda dapat mengakses informasi tersebut dengan cara melakukan masuk ke Akun Anda melalui Baronang App.
                            Anda tidak akan membiarkan pihak ketiga untuk dapat mengakses Data pribadi Anda. Kami tidak bertanggung jawab atas penyalahgunaan password dan/atau PIN. Anda wajib memberitahukan kepada Kami apabila Anda yakin bahwa password dan/atau PIN Anda disalahgunakan oleh pihak lain.
                            <br>
                            <h3>Pengungkapan kepada Pihak Ketiga</h3>
                            Terkadang Baronang App dapat memberikan informasi pribadi tertentu kepada merchant strategis yang bekerja sama dengan Baronang App untuk menyediakan Layanan. Baronang App tidak akan menyewakan, menjual atau menyebarluaskan Data pribadi Anda kepada orang lain atau perusahaan-perusahaan yang tidak berafiliasi dengan Baronang App.
                            Kami dapat membagikan informasi pribadi kepada perusahaan induk, afiliasi-afiliasi dan anak perusahaan afiliasi, dan perusahaan-perusahaan yang sudah bekerjasama dengan Kami melalui perjanjian kerahasiaan Kami dan ketika Kami memiliki izin dan persetujuan dari Anda atau keadaan-keadaan yang berkaitan dengan hal-hal berikut ini:
                            <br>
                            <ul style="list-style-type:disc">
                                <li>Baronang App menyediakan informasi kepada rekanan yang Anda percayakan bekerja untuk dan atas nama Baronang App berdasarkan perjanjian-perjanjian kerahasiaan. Perusahaan-perusahaan ini dapat menggunakan identitas pribadi Anda untuk membantu Baronang App berkomunikasi dengan Anda tentang penawaran-penawaran dari Baronang App dan rekanan-rekanan Layanan Kami. Akan tetapi, perusahaan-perusahaan ini tidak berhak untuk memakai informasi dan/atau Data pribadi Anda.</li>
                                <li>Kami merespons panggilan dari pengadilan, permintaan pengadilan atau proses hukum atau untuk menerapkan hak-hak hukum Kami atau mempertahankan hak Kami terhadap tuntutan hukum.</li>
                                <li>Kami yakin bahwa ini sangatlah penting untuk membagi informasi dan/atau Data dalam rangka penyelidikan, pencegahan, atau pengambilan tindakan terhadap kegiatan yang tidak sah, dugaan penipuan, situasi-situasi yang dapat mengakibatkan keselamatan fisik dari orang lain, pelanggaran atas penggunaan Baronang App dan/atau kewajiban-kewajiban hukum lainnya.</li>
                                <li>Baronang App bekerjasama dengan vendor, rekanan-rekanan, dan penyedia jasa lainnya di industri dan kategori bisnis yang berbeda. KEMAMPUAN UNTUK MENGUBAH DAN MENGHAPUS AKUN INFORMASI ANDA Anda dapat mengubah Data pada Akun <b>BARONANG</b> Anda setiap saat.</li>
                            </ul>
                            <h3>Keamanan Informasi Pribadi</h3>
                            Kami memastikan bahwa informasi dan/atau Data yang dikumpulkan akan disimpan dengan aman. Kami menyimpan informasi dan/atau Data pribadi Anda selama diperlukan untuk memenuhi tujuan yang dijelaskan dalam kebijakan privasi ini.
                            <br>
                            <h3>Perubahan Dalam Kebijakan Privasi</h3>
                            Baronang App memiliki hak untuk mengubah kebijakan privasi Kami sesuai dengan Syarat & Ketentuan <b>BARONANG</b> dari waktu ke waktu.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php require('footer-main.php');?>