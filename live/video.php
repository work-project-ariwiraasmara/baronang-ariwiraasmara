<?php require('header-login.php');?>

<div class="row" style="padding: 20px; margin-top: 30px;">
    <section class="content">

    <div class="col-6 col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
        <div class="box box-solid" style="padding: 5px;">
            <div class="box-title">
                <h3>Baronang App Promotion</h3>
            </div>
            <div class="box-body">
                <div class="">
                    <iframe src="https://www.youtube.com/embed/2FzWGLa10No" frameborder="0" encrypted-media width="100%" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>

    <div class="col-6 col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
        <div class="box box-solid" style="padding: 5px;">
            <div class="box-title">
                <h3>Promo Baronang EDC</h3>
            </div>
            <div class="box-body">
                <div class="">
                    <iframe src="https://www.youtube.com/embed/5FMTk9pELD0" width="100%" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>

    <div class="col-6 col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
        <div class="box box-solid" style="padding: 5px;">
            <div class="box-title">
                <h3>Cara Registrasi Baronang App</h3>
            </div>
            <div class="box-body">
                <div class="">
                    <iframe src="https://www.youtube.com/embed/AJCNb27HnBY" width="100%" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>

    <div class="col-6 col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
        <div class="box box-solid" style="padding: 5px;">
            <div class="box-title">
                <h3>Cara Ganti Profile</h3>
            </div>
            <div class="box-body">
                <div class="">
                    <iframe  src="https://www.youtube.com/embed/k3_2sSZcXL0" width="100%" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>

    <div class="col-6 col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
        <div class="box box-solid" style="padding: 5px;">
            <div class="box-title">
                <h3>Cara Daftar Akun Koperasi</h3>
            </div>
            <div class="box-body">
                <div class="">
                    <iframe  src="https://www.youtube.com/embed/3M0LcxoVmLA" width="100%" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>

    <div class="col-6 col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
        <div class="box box-solid" style="padding: 5px;">
            <div class="box-title">
                <h3>Cara Setor Tunai Di Koperasi</h3>
            </div>
            <div class="box-body">
                <div class="">
                    <iframe  src="https://www.youtube.com/embed/1roSSR18fZI" width="100%" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>

    <div class="col-6 col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
        <div class="box box-solid" style="padding: 5px;">
            <div class="box-title">
                <h3>Cara Tarik Tunai Di Koperasi</h3>
            </div>
            <div class="box-body">
                <div class="">
                    <iframe  src="https://www.youtube.com/embed/c1xp3v8jtW8" width="100%" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>

    <div class="col-6 col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
        <div class="box box-solid" style="padding: 5px;">
            <div class="box-title">
                <h3>Cara Transfer Tunai Dengan Baronang App</h3>
            </div>
            <div class="box-body">
                <div class="">
                    <iframe  src="https://www.youtube.com/embed/20PkEqcBka8" width="100%" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>

    <div class="col-6 col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
        <div class="box box-solid" style="padding: 5px;">
            <div class="box-title">
                <h3>Cara Buka Tabungan Di Baronang App</h3>
            </div>
            <div class="box-body">
                <div class="">
                    <iframe  src="https://www.youtube.com/embed/XSY4N_vwFPk" width="100%" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>

    <div class="col-6 col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
        <div class="box box-solid" style="padding: 5px;">
            <div class="box-title">
                <h3>Cara Buka Pinjaman Di Baronang App</h3>
            </div>
            <div class="box-body">
                <div class="">
                    <iframe  src="https://www.youtube.com/embed/GmXMLXuSfxI" width="100%" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>

    <div class="col-6 col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
        <div class="box box-solid" style="padding: 5px;">
            <div class="box-title">
                <h3>Cara Koneksi Baronang App dengan Koperasi</h3>
            </div>
            <div class="box-body">
                <div class="">
                    <iframe  src="https://www.youtube.com/embed/GW14CD0nlTw" width="100%" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>

    </section>
</div>

<?php require('footer-login.php');?>
