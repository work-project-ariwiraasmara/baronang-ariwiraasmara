<?php require('header-login.php');?>

<div class="row" style="padding: 20px; margin-top: 50px;">
    <section class="content">

        <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div style="text-align: center;">
                <img src="static/images/Logo_Black.png" width="25%" />
            </div>
        </div>

        <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div class="box box-solid">
                <div class="box-title" style="margin-left: 8px;">
                    <h2>Deskripsi Pekerjaan</h2>
                </div>
                <div class="box-body">

                    <table class="table">
                        <tr>
                            <td>
                                <b>Electrical Engineer</b>
                                <ul>
                                    <li>Pendidikan min. STM</li>
                                    <li>Bisa mapping & membaca gambar (wire diagram)</li>
                                    <li>Mampu mengerjakan panel elektronik</li>
                                    <li>Menguasai semua jenis material elektrikal</li>
                                    <li>Mampu menghitung volume</li>
                                    <li>Dapat menggunakan alat instrumen</li>
                                </ul>
                            </td>

                            <td>
                                <b>Programmer PHP / Android</b>
                                <ul>
                                    <li>Pendidikan D3 Management Informatika / S1 Sistem Informasi</li>
                                    <li>Menguasai PHP, HTML, Bootstrap/CSS, Java (for Android)</li>
                                    <li>Pernah menggunakan framework Ci / Yii / Laravel (nilai lebih)</li>
                                    <li>Menguasai pemrograman Android (for Android)</li>
                                    <li>Familiar dengan Git</li>
                                    <li>Menguasai database MySQL / MS SQL / Postgre SQL</li>
                                </ul>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <b>Marketing Staff</b>
                                <ul>
                                    <li>Memiliki kemampuan komunikasi, presentasi & negosiasi yang baik</li>
                                    <li>Berpenampilan menarik dan sopan</li>
                                    <li>Bersedia bekerja dengan target</li>
                                    <li>Bersedia melakukan perjalanan dinas</li>
                                    <li>Memiliki pemahaman tentang teknologi digital</li>
                                </ul>
                            </td>

                            <td>
                                <b>Drafter 3D</b>
                                <ul>
                                    <li>Pendidikan min. D3 Teknik Sipil / Arsitektur</li>
                                    <li>Menguasai software SketchUp / 3DMax</li>
                                    <li>Mengerti detail gambar kerja</li>
                                    <li>Memiliki kemampuan mengerjakan desain bangunan</li>
                                    <li>Mampu bekerja dengan cepat dan teliti</li>
                                </ul>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <b>Mechanical Engineer</b>
                                <ul>
                                    <li>Pendidikan STM / D3 Teknik Mesin</li>
                                    <li>Mengerti prinsip kerja mesin</li>
                                    <li>Memiliki kemampuan perbaikan & perawatan mesin</li>
                                </ul>
                            </td>

                            <td>
                                <b>Persyaratan Umum</b>
                                <ul>
                                    <li>Mampu bekerja secara individu maupun dalam team</li>
                                    <li>Mampu bekerja dengan target</li>
                                    <li>Jujur, disiplin & sopan</li>
                                </ul>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2">
                                Jika Anda merasa salah satu kriteria diatas sesuai dengan Anda, kirimkan CV terbaru Anda ke <b>career@baronang.com</b> dengan subjek email sesuai dengan posisi yang Anda inginkan.
                            </td>
                        </tr>

                    </table>

                </div>
            </div>
        </div>

        <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div class="box box-solid">
                <div class="box-title" style="margin-left: 8px;">
                    <h2>Informasi Perusahaan</h2>
                </div>
                <div class="box-body">
                    PT Baronang Asia Pasifik adalah sebuah perusahaan IT yang berdomisili di Jl. Imam Bonjol 88 Blok. A No. 3 Rt. 003 Rw. 001
                </div>
            </div>
        </div>

        <div class="col-6 col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
            <div class="box box-solid">
                <div class="box-title" style="margin-left: 8px;">
                    <h2>Gambaran Perusahaan</h2>
                </div>
                <div class="box-body">
                    <b>Industri</b> <br>
                    Komputer / Teknik Informatika (Perangkat Lunak)
                    <br><br>
                    <b>Situs</b> <br>
                    http://www.baronang.com
                    <br><br>
                    <b>Waktu Bekerja</b> <br>
                    Waktu reguler, Senin-Jum'at
                    <br><br>
                    <b>Gaya Berpakaian</b> <br>
                    Bisnis (Contoh: Kemeja, Casual)
                    <br><br>
                    <b>Bahasa yang Digunakan</b> <br>
                    Bahasa Indonesia
                </div>
            </div>
        </div>



        <div class="col-6 col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
            <div class="box box-solid">
                <div class="box-title" style="margin-left: 8px;">
                    <h2>Lokasi Kerja</h2>
                </div>
                <div class="box-body">
                    <b>Alamat</b> <br>
                    Jl. Imam Bonjol 88, Ruko Plaza Sentra Indoraya Blok. A No. 3 Rt. 003 Rw. 001 Karawaci, Tangerang <br> <br>

                    <iframe width="100%" height="350px" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.4782907393383!2d106.61866431476895!3d-6.2004571955112935!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x3feea099f68ce9ab!2sPT.+Baronang+Asia+Pasifik!5e0!3m2!1sen!2suk!4v1522141418467" frameborder="0" style="border: none;" allowfullscreen></iframe>
                </div>
            </div>
        </div>



    </section>
</div>

<?php require('footer-login.php');?>
