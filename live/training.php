<?php require('header-login.php');?>

<?php include("connect.inc");?>

<div class="row">
    <section class="content">

    <div class="col-sm-12" style="margin-top: 60px;">
        <?php if($_SESSION['error-type'] != '' and $_SESSION['error-message'] != '' and $_SESSION['error-time'] != ''){ ?>
            <div class="alert alert-<?php echo $_SESSION['error-type']; ?> alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-warning ?>"></i> <?php echo ucfirst($_SESSION['error-type']); ?></h4>
                <?php echo $_SESSION['error-message']; ?>
            </div>
        <?php } ?>

        <div class="box no-border">
            <div class="login-logo">
                <a href="" class="logo">
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><img src = "static/images/Logo_Black.png"></span>
                </a>
            </div><!-- /.login-logo -->
            <div class="box-body">
                <form action="proctraining.php" method="post">
                    <p class="login-box-msg">Training Register</p>
                    <div class="col-sm-6">
                        <div class="form-group has-feedback">
                            <input type="text" name="namekop" class="form-control" placeholder="Nama Koperasi" required="">
                            <span class="fa fa-users form-control-feedback" aria-hidden="true"></span>
                        </div>
                        *Pastikan email yang anda isi aktif. Akan digunakan untuk konfirmasi selanjutnya.
                        <div class="form-group has-feedback">
                            <input type="email" name="email" class="form-control" placeholder="Email" required="">
                            <span class="fa fa-envelope-o form-control-feedback" aria-hidden="true"></span>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <p>Jadwal</p>
                        <select name="jadwal" class="form-control" id="jadwal" required="">
                            <option value="">Pilih jadwal</option>
                        <?php
                        $tgl = date('Y-m-d');
                        $sql = "select * from dbo.ScheduleTraining where Tanggal > '$tgl' and Sisa <> 0";
                        $stmt = sqlsrv_query($conn, $sql);
                        while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_NUMERIC)){
                            ?>
                            <option value="<?php echo $row[2]->format('Y-m-d');?>"><?php echo $row[2]->format('l, d F Y');?></option>
                        <?php } ?>
                        </select>
                        <div id="isi">
                            <table class="table table-bordered">

                            </table>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <p>
                            <button id="add" type="button" class="btn btn-xs btn-primary" title="Tambah baris" disabled><i class="fa fa-plus"></i> Tambah Data Peserta</button>
                        </p>

                        <table class="table" id="data">

                        </table>
                    </div>
                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-flat btn-primary">Simpan</button>
                        <a href="index.php"><button type="button" class="btn btn-flat btn-default">Batal</button></a>
                    </div>
                </form>
            </div><!-- /.login-box-body -->
        </div><!-- /.login-box -->
    </div>
    </section>
    </div>
    <script type="text/javascript">
        $('#jadwal').change(function(){
            var jadwal = $('#jadwal :selected').val();

            if(jadwal != ''){
                $.ajax({
                    url : "ajax_training.php",
                    type : 'POST',
                    data: { jadwal: jadwal},
                    success : function(data) {
                        $("#isi").html(data);

                        $('#add').prop('disabled', false);
                        $('#data').html('');
                        $('#data').append(
                            "<tr>" +
                                "<td><input type='text' name='name[]' class='form-control name' placeholder='Nama Peserta' required=''></td>" +
                                "<td><input type='text' name='telp[]' class='form-control telp' placeholder='Telepon' required=''></td>" +
                                "</tr>"
                        );

                    },
                    error : function(){
                        alert('Silahkan coba lagi');
                        return false;
                    }
                });
            }
            else{
                $("#isi").html('');
                $('#add').prop('disabled', true);
                $('#data').html('');
            }
        });

        $('#add').click(function(){
            var rowCount = $('#data tr').length;
            var maxSeat = $('#sisa').val();

            if(rowCount == maxSeat){
                alert('Maksimal '+maxSeat+' kursi');
                return false;
            }
            else{
                $('#data').append(
                    "<tr>" +
                        "<td><input type='text' name='name[]' class='form-control name' placeholder='Nama Peserta' required=''></td>" +
                        "<td><input type='text' name='telp[]' class='form-control telp' placeholder='Telepon' required=''></td>" +
                        "</tr>"
                );
            }
        });

    </script>

<?php require('footer-login.php');?>