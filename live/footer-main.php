        </section>
    </div>
</div>
<footer class="main-footer text-center">
    <strong><a href="index.php">BARONANG</a> &copy; 2018 </strong>
</footer>

<!-- Bootstrap 3.3.5 -->
<script src="static/bootstrap/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="static/plugins/select2/select2.full.min.js"></script>
<!-- InputMask -->
<script src="static/plugins/input-mask/jquery.inputmask.js"></script>
<script src="static/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="static/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src="static/plugins/daterangepicker/daterangepicker.js"></script>
<!-- bootstrap date picker -->
<script src="static/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- bootstrap time picker -->
<script src="static/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="static/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="static/plugins/fastclick/fastclick.min.js"></script>
<!-- AdminLTE App -->
<script src="static/dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="static/dist/js/demo.js"></script>
<!-- Page script -->

<?php require('content-footer.php');?>

</body>
</html>
