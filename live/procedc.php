<?php
require("lib/PHPMailer_5.2.0/class.phpmailer.php");

include("connect.inc");

$kid = $_GET['kid'];
$kba = $_POST['kba'];

if(isset($_GET['change'])){
    $sn = $_GET['change'];

    $a = "select* from [Gateway].[dbo].[EDCList] where SerialNumber = '$sn'";
    $b = sqlsrv_query($conn, $a);
    $c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC);
    if($c != null){
        $key = getKey($c[0]);

        $sql = "update [Gateway].[dbo].[EDCList] set PrivateKey='$key' where SerialNumber='$sn'";
        $stmt = sqlsrv_query($conn, $sql);
        if($stmt){
            messageAlert('Berhasil memperbaharui key','success');
            header('Location: edc.php?add='.$c[1]);
        }
        else{
            messageAlert('Gagal memperbaharui key','danger');
            header('Location: edc.php?add='.$c[1]);
        }
    }
    else{
        messageAlert('Serial number tidak ditemukan','warning');
        header('Location: edc.php');
    }
}

if(isset($_GET['send'])){
    $sn = $_GET['change'];

    $a = "select* from [Gateway].[dbo].[EDCList] where SerialNumber = '$sn'";
    $b = sqlsrv_query($conn, $a);
    $c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC);
    if($c != null){
        $kid = $c[1];
        $sn = $c[0];
        $key = $c[4];
        $kba = $c[2];
        $aa = "select* from [dbo].[UserRegister] where KID = '$kid' and Status = 1";
        $bb = sqlsrv_query($conn, $aa);
        $cc = sqlsrv_fetch_array($bb, SQLSRV_FETCH_NUMERIC);
        if($cc != null){
            $email = $cc[2];
            $nama = $cc[5];
            sendEmail($email, $nama, $kba, $kid, $sn, $key);
        }
        else{
            messageAlert('Koperasi tidak ditemukan','warning');
            header('Location: edc.php');
        }
    }
    else{
        messageAlert('Serial number tidak ditemukan','warning');
        header('Location: edc.php');
    }
}

if(isset($_GET['kid'])){
    if($kba == "" || $kid == ""){
        messageAlert('Harap isi seluruh kolom','info');
        header('Location: edc.php');
    }
    else{
        $x = "select* from [dbo].[ListKoperasi] where KID = '$kid' and Status = 1";
        $y = sqlsrv_query($conn, $x);
        $z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC);
        if($z != null){
            $param = $z[0].date('ymdHisms');
            $key = getKey($param);

            $sql = "exec $z[3].[dbo].[ProsesEDCList] '$kid', '$kba','$key'";
            $stmt = sqlsrv_query($conn, $sql);
            if($stmt){
                messageAlert('Berhasil menambahkan EDC','success');
                header('Location: edc.php');
            }
            else{
                messageAlert('Gagal menambahkan EDC','danger');
                header('Location: edc.php');
            }
        }
        else{
            messageAlert('Koperasi tidak ditemukan','warning');
            header('Location: edc.php');
        }
    }
}

function getKey($param){
    $options = [
        'cost' => 11,
        'salt' => mcrypt_create_iv(22, MCRYPT_DEV_URANDOM),
    ];
    $key = password_hash($param, PASSWORD_BCRYPT, $options);

    return $key;
}

function sendEmail($email, $nama, $kba, $kid, $sn, $key){
    $mail = new PHPMailer();
    $mail->IsSMTP();
    $mail->Host = "baronang.com";  // specify main and backup server
    $mail->SMTPAuth = true;     // turn on SMTP authentication
    $mail->Username = "konfirmasi@baronang.com";  // SMTP username
    $mail->Password = "alva7000"; // SMTP password
    $mail->Port = 587;
    $mail->SMTPDebug = 1;

    $mail->From = "konfirmasi@baronang.com";
    $mail->FromName = "Baronang Mail Service";
    $mail->AddAddress($email);
    $mail->AddReplyTo("konfirmasi@baronang.com", "Baronang Konfirmasi");
    $mail->IsHTML(true);// set email format to HTML

    $mail->Subject = "Email Verifikasi Koperasi";
    $mail->Body =
        "Hi! ".$nama.",<br><br>
            Selamat! EDC anda untuk koperasi ".$kid." anda telah aktif dan siap untuk digunakan.<br>
            <br>
            Kode bank yang digunakan adalah ".$kba."<br>
            Anda harus memasukan <b>Serial Number</b> dan <b>Private Key</b> dibawah ini saat mensetting POS Application.
            <br>
            <b>Serial Number</b> anda adalah ".$sn.".<br>
            <b>Private Key</b> anda adalah ".$key.".<br>
            <br>
            Untuk POS Application dapat anda download melalui link : klik <a href='http://baronang.com'>disini</a>.<br>
            <br>
            Terima kasih telah menggunakan EDC kami.
            <br>
            <br>
            <br>
            <br>
            Salam,<br>
            <br>
            <br>
            Baronang";
    if(!$mail->Send())
    {
        echo "Message could not be sent.";
        echo "Mailer Error: " . $mail->ErrorInfo;
    }
}
?>