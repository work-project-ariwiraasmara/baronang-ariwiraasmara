    <div class="row">
        <footer class="main-footer" style="background-image: url(static/images/bg.png); -webkit-background-size:cover ;background-size:cover ;">

            <div class="form-group">
                <a href="contactus.php" style="color: white; padding-left: 20px;"><b>Hubungi Kami</b></a>
            </div>
            <div class="pull-right" style="color: white; padding-right: 20px">
                <b>Unduh Baronang App sekarang!</b>
                <a href="https://play.google.com/store/apps/details?id=com.baronang.koperasibaronang&hl=en" target="_blank">
                    <img class="img-responsive" src="static/images/googleplay.png" alt="Photo" style="margin-top:10px; margin-left: 30px;width: 150px;">
                </a>
            </div>
            <div class="form-group">
                <a href="#" style="color: white; padding-left: 20px"><b>FAQ</b></a>
            </div>
            <div class="form-group">
                <a href="tnc.php" style="color: white; padding-left: 20px"><b>Syarat dan Ketentuan</b></a>
            </div>
            <div class="form-group">
                <a href="#" style="color: white; padding-left: 20px"><b>Harga</b></a>
            </div>
            <div class="form-group" style="padding-left: 20px;color: white;">
                <strong>Copyright &copy; 2018 <a href="http://baronang.com" style="color: white;"><u>Baronang </u></a>.</strong>
            </div>

        </footer>
    </div>

<!-- Bootstrap 3.3.5 -->
<script src="static/bootstrap/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="static/plugins/select2/select2.full.min.js"></script>
<!-- InputMask -->
<script src="static/plugins/input-mask/jquery.inputmask.js"></script>
<script src="static/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="static/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src="static/plugins/daterangepicker/daterangepicker.js"></script>
<!-- bootstrap date picker -->
<script src="static/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- bootstrap time picker -->
<script src="static/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="static/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="static/plugins/fastclick/fastclick.min.js"></script>
<!-- AdminLTE App -->
<script src="static/dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="static/dist/js/demo.js"></script>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/5a8f8d3c4b401e45400d2482/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
        checkboxClass: 'icheckbox_minimal-blue',
        radioClass   : 'iradio_minimal-blue'
    })
</script>
</body>
</html>
