<?php
require('util/koneksi.php');
if( isset($_GET[$fun->Enlink('link')]) ) {
	$link = $fun->getIDParam('link');
}
else {
	$link = 'index.php';
}
?>

<!DOCTYPE html>
<html>
	<head>
		<title>
			<?php 
			if( isset($_GET[$fun->Enlink('title')]) ) { 
				echo $fun->getIDParam('title'); 
			} 
			else { 
				echo 'Error!'; 
			} 
			?>
		</title>
		<meta content="IE=edge" http-equiv="x-ua-compatible">
		<meta content="initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" name="viewport">
		<meta content="yes" name="apple-mobile-web-app-capable">
		<meta content="yes" name="apple-touch-fullscreen">

		<link href='images/icon/Logo-fish-icon.png' type='image/x-icon'/>
		<link href='images/icon/Logo-fish-icon.png' rel='shortcut icon'/>

		<!-- Fonts -->
		<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
		    
		<!-- Icons -->
		<link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" media="all" rel="stylesheet" type="text/css">
		    
		<!-- Styles -->
		<link href="css/keyframes.css" rel="stylesheet" type="text/css">
		<link href="css/materialize.min.css" rel="stylesheet" type="text/css">
		<link href="css/swiper.css" rel="stylesheet" type="text/css">
		<link href="css/swipebox.min.css" rel="stylesheet" type="text/css">
		<link href="css/style.css" rel="stylesheet" type="text/css">
		<link href="css/custom.css" rel="stylesheet" type="text/css">

		<script type="text/javascript" src="js/jquery-2.1.0.min.js"></script>
	</head>

	<body>
		<div class="m-scene" id="main">
			<div id="content" class="page txt-black">

				<div id="toolbar" class="primary-color">
					<a href="<?php echo $link; ?>" class="open-left">
	                    <i class="ion-android-arrow-back <?php echo $co_head; ?>"></i>
	                </a>

				    <span class="title bold txt-white" id="title">
				    	<?php 
						if( isset($_GET[$fun->Enlink('title')]) ) { 
							echo $fun->getIDParam('title'); 
						} 
						else { 
							echo 'Error!'; 
						} 
						?>
				    </span>
				</div>

				<div class="animated fadeinup delay-1">
					<div class="page-content txt-black center">

						<h1 class="txt-black">
							<?php 
							if( isset($_SESSION[$fun->ENID('error_content')]) ) {
								echo $fun->getSession('error_content', 2);
							}
							else {
								echo 'Error!';
							}
							?>
						</h1>

					</div>
				</div>

			</div>
		</div>

		<script src="js/jquery.swipebox.min.js"></script>
		<script src="js/materialize.min.js"></script>
		<script src="js/swiper.min.js"></script>
		<script src="js/jquery.mixitup.min.js"></script>
		<script src="js/masonry.min.js"></script>
		<script src="js/chart.min.js"></script>
		<script src="js/functions.js"></script>
	</body>
</html>