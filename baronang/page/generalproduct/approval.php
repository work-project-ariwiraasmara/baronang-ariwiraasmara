<div class="animated fadeinup delay-1">
	<div class="page-content">

		<h1 class="title txt-black" id="title"></h1>

		<form action="process/42capprovalproduct.php" method="post">
			<div class="table-responsive m-t-30">
				
				<table>
					<thead>
						<tr>
							<th class="center">Product</th>
							<th class="center">Expired</th>
							<th class="center">Price (Rp)</th>
							<th class="center">Allowance</th>
							<th class="center">Information</th>
							<th class="center">Approve?</th>
						</tr>
					</thead>

					<tbody>
						<?php
						$no = 1; 

						$sapp1 = "SELECT * from 21masterlocation order by locationname";
						$qapp1 = mysqli_query($fun->getConnection(), $sapp1) or die(mysqli_error($fun->getConnection()));
						while($dapp1 = mysqli_fetch_array($qapp1)) { 
							$title = 1;
							?>
							<tr>
								<td class="center hide" id="<?php echo $fun->Enlink($dapp1['masterlocationid']); ?>" colspan="5"><h4 class="bold txt-black"><?php echo $dapp1['locationname']; ?></h4></td>
							</tr>

							<?php
							$sapp2 = "SELECT * from 22amasterproduct where active='2' and locationid='".$dapp1['masterlocationid']."'";
							$qapp2 = mysqli_query($fun->getConnection(), $sapp2) or die(mysqli_error($fun->getConnection()));
							$rapp2 = mysqli_num_rows($qapp2);
							
							if($rapp2 > 0) {
								while($dapp2 = mysqli_fetch_array($qapp2)) { ?>
									<tr>
										<td><?php echo $dapp2['productdescription']; ?></td>
										<td style="text-align: right;"><?php echo $fun->FormatNumber($dapp2['validity_days'])." ".$dapp2['unit']; ?></td>
										<td style="text-align: right;"><?php echo $fun->FormatRupiah($dapp2['productprice']); ?></td>
										<td style="text-align: right;"><?php echo $fun->FormatNumber($dapp2['allowance']); ?></td>
										<td>
											New
											<input type="hidden" name="<?php echo $fun->Enlink('ket').'['.$no.']'; ?>" id="<?php echo $fun->Enlink('ket').'-'.$no; ?>" value="<?php echo $fun->Enval('new'); ?>">
											<input type="hidden" name="<?php echo $fun->Enlink('productid').'['.$no.']'; ?>" id="<?php echo $fun->Enlink('productid').'-'.$no; ?>" value="<?php echo $fun->Enval($dapp2['masterproductid']); ?>">
										</td>
										<td>
											<input type="radio" name="<?php echo $fun->Enlink('approval').'['.$no.']'; ?>" id="<?php echo $fun->Enlink('approval_yes').'-'.$no; ?>" value="<?php echo $fun->Enval('1'); ?>">
											<label for="<?php echo $fun->Enlink('approval_yes').'-'.$no; ?>" class="txt-black">Ya</label>
											<br>
											<input type="radio" name="<?php echo $fun->Enlink('approval').'['.$no.']'; ?>" id="<?php echo $fun->Enlink('approval_no').'-'.$no; ?>" value="<?php echo $fun->Enval('3'); ?>">
											<label for="<?php echo $fun->Enlink('approval_no').'-'.$no; ?>" class="txt-black">Tidak</label>
										</td>
									</tr>
									<?php
									$no++;
								}
								$title = $title + 1;
							}
							else {
								$title = $title - 1;
							}

							
							$sapp3 = "SELECT distinct 	c.masterhistoryproduct as masterhistoryproduct,
													b.productdescription as productdescription, 
													c.validity_days as validity_days,
													c.price as productprice
										from 22amasterproduct as b
										inner join 22chistoryproduct as c
											on b.masterproductid = c.productid
										where c.status='2' and b.locationid='".$dapp1['masterlocationid']."'";
							$qapp3 = mysqli_query($fun->getConnection(), $sapp3) or die(mysqli_error($fun->getConnection()));
							$rapp3 = mysqli_num_rows($qapp3);

							if($rapp3 > 0) {
								while($dapp3 = mysqli_fetch_array($qapp3)) { ?>
									<tr>
										<td><?php echo $dapp3['productdescription']; ?></td>
										<td style="text-align: right;"><?php echo $fun->FormatNumber($dapp3['validity_days']); ?></td>
										<td style="text-align: right;"><?php echo $fun->FormatRupiah($dapp3['productprice']); ?></td>
										<td>
											Update
											<input type="hidden" name="<?php echo $fun->Enlink('productid').'['.$no.']'; ?>" id="<?php echo $fun->Enlink('productid').'-'.$no; ?>" value="<?php echo $fun->Enval($dapp3['masterhistoryproduct']); ?>">
											<input type="hidden" name="<?php echo $fun->Enlink('ket').'['.$no.']'; ?>" id="<?php echo $fun->Enlink('ket').'-'.$no; ?>" value="<?php echo $fun->Enval('update'); ?>">
										</td>
										<td>
											<input type="radio" name="<?php echo $fun->Enlink('approval').'['.$no.']'; ?>" id="<?php echo $fun->Enlink('approval_yes').'-'.$no; ?>" value="<?php echo $fun->Enval('1'); ?>">
											<label for="<?php echo $fun->Enlink('approval_yes').'-'.$no; ?>" class="txt-black">Ya</label>
											<br>
											<input type="radio" name="<?php echo $fun->Enlink('approval').'['.$no.']'; ?>" id="<?php echo $fun->Enlink('approval_no').'-'.$no; ?>" value="<?php echo $fun->Enval('3'); ?>">
											<label for="<?php echo $fun->Enlink('approval_no').'-'.$no; ?>" class="txt-black">Tidak</label>
										</td>
									</tr>
									<?php
									$no++;
								}
								$title = $title + 1;
							}
							else {
								$title = $title - 1;
							}
							?>

							<tr>
								<td colspan="5" class="m-b-30"></td>
							</tr>

							<?php
							if($title > 0) { ?>
								<script type="text/javascript">
									$("<?php echo '#'.$fun->Enlink($dapp1['masterlocationid']); ?>").removeClass("hide");
									console.log("<?php echo '#'.$dapp1['masterlocationid'].': '.$title; ?>");
								</script>
								<?php
							}
							
						}
						?>
					</tbody>

					<tfoot>
						<tr>
							<td colspan="6">
								<button type="submit" name="btnok" id="btnok" class="btn btn-large width-100 primary-color waves-effect waves-light borad-20">Approve</button>
							</td>
						</tr>
					</tfoot>

				</table>

			</div>

			<input type="hidden" name="<?php echo $fun->Enlink('jml_no'); ?>" id="<?php echo $fun->Enlink('jml_no'); ?>" value="<?php echo $fun->Enval($no); ?>">

		</form>

	</div>
</div>

<script type="text/javascript">
	$('#toBack').click(function() {
		window.location.href = "<?php echo '?#'.$fun->Enlink('skymanagement'); ?>";
	});

	$("<?php echo '#'.$fun->Enlink('btnlokasi'); ?>").click(function(){
		var lokasi = $("<?php echo '#'.$fun->Enlink('lokasi'); ?>").val();

		window.location.href = "<?php echo '?pg=product&sb=approval&'.$fun->Enlink('lokasi').'='; ?>" + lokasi;
	});
</script>
