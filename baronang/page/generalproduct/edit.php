<?php
$sf1 = "SELECT * from 22amasterproduct where masterproductid='".$fun->getIDParam('ID')."'";
$qf1 = mysqli_query($fun->getConnection(), $sf1) or die(mysqli_error($fun->getConnection()));
$df1 = mysqli_fetch_array($qf1);
?>

<div class="animated fadeinup delay-1">
	<div class="page-content">

		<h1 class="title uppercase txt-black" id="title"></h1>

		<form action="<?php echo 'process/42bupdateproduct.php?'.$fun->setIDParam('ID', $fun->getIDParam('ID')); ?>" method="post">

			<div class="input-field">
				<?php
				$s1 = "SELECT locationname from 21masterlocation where masterlocationid='".$fun->getIDParam('lokasi')."'";
				$q1 = mysqli_query($fun->getConnection(), $s1) or die(mysqli_error($fun->getConnection()));
				$d1 = mysqli_fetch_array($q1);
				$fun->inputField('text', 'lokasi', 'lokasi', '', 'validate hide', 'txt-black', $fun->getIDParam('lokasi'), 'required');
				?>
				<h3 class="bold txt-black"><?php echo 'Location : '.$d1['locationname']; ?></h3>
			</div>

			<?php /*
			<div class="input-field m-t-30">
				<?php
				$fun->inputField('text', 'deskripsi', 'deskripsi', 'Deskripsi', 'validate', 'txt-black', $df1['productdescription'], 'required');
				?>
			</div>

			<div class="m-t-10">
				<?php
				$arrtipe = array('Motor', 'Mobil Kecil', 'Mobil Besar');
				$fun->selectField('tipe', 'tipe', 'Tipe', 'browser-default', 'txt-black', $arrtipe, $arrtipe, $df1['vehicletype'], 'required');
				?>
			</div>

			<div class="m-t-10">
				<span>Status</span> : <br>
				<select class="browser-default" name="<?php echo $fun->Enlink('status'); ?>" id="<?php echo $fun->Enlink('status'); ?>">
					<option value="<?php echo $fun->Enval('0'); ?>" <?php if($df1['active'] == '0') echo 'selected'; ?> >Tidak Aktif</option>
					<option value="<?php echo $fun->Enval('1'); ?>" <?php if($df1['active'] == '1') echo 'selected'; ?> >Aktif</option>
				</select>
			</div>
			*/ ?>

			<div class="input-field">
				<?php
				$fun->inputField('number', 'valid', 'valid', 'Quantity', 'validate', 'txt-black', $df1['validity_days'], 'required');
				?>
			</div>

			<div class="input-field">
				<?php
				$fun->inputField('number', 'harga', 'harga', 'Price', 'validate', 'txt-black', $df1['productprice']);
				?>
			</div>

			

			<?php
			$fun->buttonField('submit', 'confirm', 'confirm', 'btn btn-large width-100 waves-effect waves-light primary-color m-t-30 borad-20', 'OK');
			?>

		</form>

	</div>
</div>

<script type="text/javascript">
	$('#toBack').click(function() {
		window.location.href = "<?php echo '?pg=generalproduct'; ?>";
	});
</script>
