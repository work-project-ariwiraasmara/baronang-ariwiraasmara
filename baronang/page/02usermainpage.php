<?php
$session = $fun->getValookie('session');
//$session = "member";
$id = $fun->getValookie('field_id');

if($session == 'member') {
	$s1 = "SELECT * from 22bmastermember where mastermemberid='".$fun->getValookie('id')."'";
}
else {
	$s1 = "SELECT * from 23dmastertenant where mastertenantid='".$fun->getValookie('id')."'";
}
$q1 = mysqli_query($fun->getConnection(), $s1) or die(mysqli_error($fun->getConnection()));
$d1 = mysqli_fetch_array($q1);

$s2 = "SELECT * from 23amastercard where memberid='".$fun->getValookie('id')."'";
$q2 = mysqli_query($fun->getConnection(), $s2) or die(mysqli_error($fun->getConnection()));
$d2 = mysqli_fetch_array($q2);

$s3 = "SELECT * from 21masterlocation where masterlocationid='".$d1['locationid']."'";
$q3 = mysqli_query($fun->getConnection(), $s3) or die(mysqli_error($fun->getConnection()));
$d3 = mysqli_fetch_array($q3);
?>

<div class="animated fadeinup delay-1 nowrap">

	<div class="page-content txt-black">

		<?php
	    if($session == 'member') { ?>
	    	<ul class="tabs txt-link-dark bg-0">
		    	<?php
		    	if( $d1['tab4'] == 1 ) { ?>
			        <li class="tab"><a class="" href="<?php echo '#'.$fun->Enlink('personal'); ?>">Personal</a></li>
			        <?php
			    }

			    if( $d1['tab3'] == 1 ) { ?>
			        <li class="tab"><a class="" href="<?php echo '#'.$fun->Enlink('tenantmanagement'); ?>">Tenant</a></li>
			        <?php
			    }

			    if( $d1['tab2'] == 1 ) { ?>
			        <li class="tab"><a class="" href="<?php echo '#'.$fun->Enlink('skymanagement'); ?>">Skyparking</a></li>
			        <?php
			    }

			    if( $d1['tab1'] == 1 ) { ?>
			        <li class="tab"><a class="" href="<?php echo '#'.$fun->Enlink('baronang'); ?>">Baronang</a></li>
			        <?php
			    } 
		        ?>
	        </ul>
	        <?php
	    }

        if($session == 'member') { 
        	if( $d1['tab4'] == '1' ) { require('page/02dtab4_personal.php'); }  

        	if( $d1['tab2'] == '1' ) { require('page/02btab2_skymanagement.php'); }     

        	if( $d1['tab1'] == '1' ) { require('page/02atab1_baronang.php'); }	

        	if( $d1['tab3'] == '1' ) { require('page/02ctab3_tenantmanagement.php'); } 
        }
        else { 
        	require('page/02dtab4_personal.php');
        }
		?>
	</div>

</div>

<?php
$scmpy = "SELECT distinct 	a.id_prov as id_prov, 
							a.nama_prov as nama_prov, 
							c.nama_kabkot as nama_kabkot,
							b.companyid as companyid, 
							b.companyname as companyname,
							b.companylogo as companylogo, 
							b.companyemail as companyemail,
							b.companyphone as companyphone, 
							b.companyaddress as companyaddress
		from 11amasterprovince as a 
		inner join 12mastercompany as b 
			on a.id_prov = b.province
		inner join 11bmastercity as c 
			on b.city = c.id_kabkot
		order by nama_prov ASC";
$qcmpy = mysqli_query($fun->getConnection(), $scmpy) or die(mysqli_error($fun->getConnection()));
$rcmpy = mysqli_num_rows($qcmpy);

if($rcmpy > 0) {
	while($dcmpy = mysqli_fetch_array($qcmpy)) { ?>
		<div class="modal borad-20" id="<?php echo $fun->Enlink($dcmpy['companyid']); ?>">
			<div class="modal-content choose-date txt-black">
				<?php
				if( ($dcmpy['companylogo'] != '') || !empty($dcmpy['companylogo']) ) { ?>
					<p class="center">
						<img src="<?php echo 'images/company/'.strtolower($dcmpy['companyid'].$dcmpy['companyname']).'/'.$dcmpy['companylogo']; ?>">
					</p>
					<?php
				}
				?>
				<p><h1 class="txt-black"><?php echo $dcmpy['companyname']; ?></h1></p>

				<p><span class="bold">Address : </span><br><?php echo $dcmpy['companyaddress'].', '.$dcmpy['nama_kabkot'].', '.$dcmpy['nama_prov']; ?></p>
				<p><span class="bold">Phone Number : </span><?php echo $dcmpy['companyphone'] ?></p>
				<p><span class="bold">Email : </span><?php echo $dcmpy['companyemail']; ?></p>
				<p class="m-t-10 txt-white">
					<a href="<?php echo '?pg=company&sb=edit&'.$fun->setIDParam('idcmpy', $dcmpy['companyid']); ?>" class="btn btn-large width-100 primary-color waves-effect waves-light borad-20"><i class="ion-android-create"></i></a>
				</p>
			</div>
		</div>
		<?php
	}
}

$fun->setOneSession('error_content', '', 2);
?>