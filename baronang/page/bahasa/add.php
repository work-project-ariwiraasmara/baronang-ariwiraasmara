<?php
$lang = $fun->getIDParam('lang');
if($lang == 'en') {
	$langtext = 'English';
}
else {
	$langtext = 'Bahasa Indonesia';
}
?>

<div class="animated fadeinup delay-1">
	<div class="page-content">

		<h1 class="title uppercase txt-black" id="title"></h1>

		<form action="<?php echo 'process/61aaddlang.php?'.$fun->setIDParam('lang', $fun->getIDParam('lang')).'&'.$fun->setIDParam('ID', $fun->getIDParam('ID')); ?>" method="post">
			
			<div class="input-field">
				<?php
				$fun->inputField('text', 'item', 'item', 'Item', 'validate', 'txt-black', null, 'required');
				?>
			</div>

			<div class="input-field">
				<?php
				$fun->inputField('text', 'lang', 'lang', $langtext, 'validate', 'txt-black', null, 'required');
				?>
			</div>

			<?php
			$fun->buttonField('submit', 'confirm', 'confirm', 'btn btn-large width-100 waves-effect waves-light primary-color m-t-30 borad-20', 'Save');
			?>

		</form>

	</div>
</div>

<script type="text/javascript">
	$('#toBack').click(function() {
		window.location.href = "?pg=lang";
	});
</script>