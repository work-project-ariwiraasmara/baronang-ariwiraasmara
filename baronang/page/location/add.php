<div class="animated fadeinup delay-1">
	<div class="page-content">

		<h1 class="title uppercase txt-black" id="title"></h1>

		<form action="process/41aaddlocation.php" method="post" enctype="multipart/form-data">
				
			<div class="input-field">
				<?php
				$fun->inputField('text', 'nama', 'nama', 'Name', 'validate', 'txt-black', null, 'required');
				?>
			</div>

			<div class="input-field">
				<?php
				$fun->inputField('email', 'email', 'email', 'Email', 'validate', 'txt-black', null, 'required');
				?>
			</div>

			<div class="input-field">
				<?php
				$fun->inputField('number', 'tlp', 'tlp', 'Phone Number', 'validate', 'txt-black');
				?>
			</div>

			<div class="input-field">
				<?php
				$fun->inputField('text', 'alamat', 'alamat', 'Address', 'validate', 'txt-black');
				?>
			</div>

			<div class="row">
				<div class="col s6">
					<span class="bold">Province</span> : <br>
					<select class="browser-default txt-black" name="<?php echo $fun->Enlink('prov'); ?>" id="<?php echo $fun->Enlink('prov'); ?>" required>
						<?php
						$s3 = "SELECT * from 11amasterprovince order by id_prov";
						$q3 = mysqli_query($fun->getConnection(), $s3) or die(mysqli_error($fun->getConnection()));
						while($d3 = mysqli_fetch_array($q3)) { ?>
							<option value="<?php echo $fun->Enval($d3['id_prov']); ?>"><?php echo $d3['nama_prov']; ?></option>
							<?php
						}
						?>
					</select>
				</div>

				<div class="col s6">
					<span class="bold">Area</span> : <br>
					<select class="browser-default txt-black" name="<?php echo $fun->Enlink('kabkot'); ?>" id="<?php echo $fun->Enlink('kabkot'); ?>" required>
						<option value="">Choose province first...</option>
					</select>
				</div>
			</div>

			<div class="input-field">
				<?php
				$fun->fileField('gambar', 'gambar', 'Image', 'validate', 'txt-black');
				?>
			</div>

			<?php
			$fun->buttonField('submit', 'confirm', 'confirm', 'btn btn-large width-100 waves-effect waves-light primary-color m-t-30 borad-20', 'Save');
			?>

		</form>

	</div>
</div>

<script type="text/javascript">
	$('#toBack').click(function() {
		window.location.href = "?pg=location";
	});
</script>
