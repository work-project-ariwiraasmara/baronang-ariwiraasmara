<div class="animated fadeinup delay-1">
	<div class="page-content">

		<h1 class="title uppercase txt-black" id="title"></h1>

		<div class="m-t-30 txt-black">
			<ul id="myUL" class="txt-black">
				<?php
				$s1 = "SELECT distinct 	a.id_prov as id_prov, a.nama_prov as nama_prov
						from 11amasterprovince as a 
						inner join 21masterlocation as b 
							on a.id_prov = b.locationprovince
						order by nama_prov ASC";
				$q1 = mysqli_query($fun->getConnection(), $s1) or die(mysqli_error($fun->getConnection()));
				while($d1 = mysqli_fetch_array($q1)) { ?>
					<li class="m-b-10 txt-black"><span class="caret"><?php echo $d1['nama_prov']; ?></span>
						<ul class="nested txt-black">
							<?php
							$s2 = "SELECT * from 21masterlocation where active = '1' and locationprovince='".$d1['id_prov']."' order by locationname ASC";
							$q2 = mysqli_query($fun->getConnection(), $s2) or die(mysqli_error($fun->getConnection()));
							$r2 = mysqli_num_rows($q2);
							if($r2 > 0) {
								while($d2 = mysqli_fetch_array($q2)) { ?>
									<li class="m-l-30 txt-black m-b-10" style=""><a href="<?php echo '#'.$fun->Enlink($d2['masterlocationid']); ?>" class="modal-trigger"><?php echo $d2['locationname']; ?></a></li>
									<?php
								}
							}
							else { ?>
								<li class="m-l-30 italic">[Data Not Available]</li>
								<?php
							}
							?>
						</ul>
					</li>
					<?php
				}
				?>
			</ul>
		</div>

		<div class="floating-button page-fab animated bouncein delay-3" style="z-index: 1;">
			<a class="btn-floating btn-large waves-effect waves-light primary-color btn z-depth-1" href="?pg=location&sb=add">
				<i class="ion-android-add"></i>
			</a>
		</div>

	</div>
</div>

<script type="text/javascript">
	$('#toBack').click(function() {
		window.location.href = "<?php echo '?#'.$fun->Enlink('skymanagement'); ?>";
	});
</script>

<?php
$s1 = "SELECT 	a.masterlocationid as masterlocationid,
				a.locationname as locationname, 
				a.locationphone as locationphone,
				a.locationaddress as locationaddress,
				a.image as image,
				b.nama_prov as nama_prov,
				c.nama_kabkot as nama_kabkot
		from 21masterlocation as a 
		inner join 11amasterprovince as b 
			on a.locationprovince = b.id_prov 
		inner join 11bmastercity as c 
			on a.locationcity = c.id_kabkot";
$q1 = mysqli_query($fun->getConnection(), $s1) or die(mysqli_error($fun->getConnection()));
while($d1 = mysqli_fetch_array($q1)) { ?>
	<div class="modal borad-10" id="<?php echo $fun->Enlink($d1['masterlocationid']); ?>">
		<div class="modal-content choose-date txt-black">
			<p><h1 class="txt-black bold"><?php echo $d1['locationname'] ?></h1></p>

			<?php
			if( !($d1['image'] == '') || !empty($d1['image']) ) { ?>
				<p><img src="<?php echo 'image/location/'.$d1['image']; ?>"></p>
				<?php
			}
			?>

			<p><span class="bold">Address : </span><br><?php echo $d1['locationaddress'].', '.$d1['nama_kabkot'].', '.$d1['nama_prov']; ?></p>
			<p><span class="bold">Phone Number : </span><?php echo $d1['locationphone'] ?></p>
			
			<p class="txt-white">
				<a href="<?php echo '?pg=location&sb=edit&'.$fun->setIDParam('ID', $d1['masterlocationid']) ?>" class="btn waves-light waves-light primary-color borad-20 width-100">
					<i class="ion-android-create" style=""></i>
				</a>
			</p>

			<div class="row center m-t-10" style="border-bottom: 1px solid #ddd; border-top: 1px solid #ccc;">
				<div class="col s4 m-t-10 m-b-10">
					<a href="<?php echo '?pg=product&'.$fun->setIDParam('lokasi', $d1['masterlocationid']); ?>" class="waves-effect waves-light modal-trigger">
						<i class="ion-card"></i><br>
						Product
					</a>
				</div>

				<div class="col s4 m-t-10 m-b-10">
					<a href="<?php echo '?pg=compliment&'.$fun->setIDParam('lokasi', $d1['masterlocationid']); ?>" class="waves-effect waves-light modal-trigger">
						<i class="ion-card"></i><br>
						Complimentary Product
					</a>
				</div>

				<div class="col s4 m-t-10 m-b-10">
					<a href="<?php echo '?pg=tenant&'.$fun->setIDParam('lokasi', $d1['masterlocationid']); ?>" class="waves-effect waves-light modal-trigger">
						<i class="ion-android-people"></i><br>
						Tenant
					</a>
				</div>
			</div>
		</div>
	</div>
	<?php

}
?>
