<?php
$sf1 = "SELECT * from 21masterlocation where masterlocationid='".$fun->getIDParam('ID')."'";
$qf1 = mysqli_query($fun->getConnection(), $sf1) or die(mysqli_error($fun->getConnection()));
$df1 = mysqli_fetch_array($qf1);

$sf2 = "SELECT a.nama_prov as nama_prov, b.nama_kabkot as nama_kabkot
		from 11amasterprovince as a 
		inner join 11bmastercity as b 
			on a.id_prov = b.id_prov 
		where a.id_prov='".$df1['locationprovince']."' and b.id_kabkot='".$df1['locationcity']."'";
$qf2 = mysqli_query($fun->getConnection(), $sf2) or die(mysqli_error($fun->getConnection()));
$df2 = mysqli_fetch_array($qf2);
?>

<div class="animated fadeinup delay-1">
	<div class="page-content">

		<h1 class="title uppercase txt-black" id="title"></h1>

		<form action="<?php echo 'process/41bupdatelocation.php?'.$fun->setIDParam('ID', $fun->getIDParam('ID')); ?>" method="post" enctype="multipart/form-data">
				
			<div class="input-field">
				<?php
				$fun->inputField('text', 'nama', 'nama', 'Name', 'validate', 'txt-black', $df1['locationname'], 'required');
				?>
			</div>

			<div class="input-field">
				<?php
				$fun->inputField('email', 'email', 'email', 'Email', 'validate', 'txt-black', $df1['locationemail'], 'required');
				?>
			</div>

			<div class="input-field">
				<?php
				$fun->inputField('number', 'tlp', 'tlp', 'Phone Number', 'validate', 'txt-black', $df1['locationphone']);
				?>
			</div>

			<div class="input-field">
				<?php
				$fun->inputField('text', 'alamat', 'alamat', 'Address', 'validate', 'txt-black', $df1['locationaddress']);
				?>
			</div>

			<div class="row">
				<div class="col s6">
					<span class="bold">Province</span> :<br>
					<span class="italic"><?php echo '['.$df2['nama_prov'].']'; ?></span> <br>
					<select class="browser-default txt-black" name="<?php echo $fun->Enlink('prov'); ?>" id="<?php echo $fun->Enlink('prov'); ?>" required>
						<?php
						$s3 = "SELECT * from 11amasterprovince order by id_prov";
						$q3 = mysqli_query($fun->getConnection(), $s3) or die(mysqli_error($fun->getConnection()));
						while($d3 = mysqli_fetch_array($q3)) { ?>
							<option value="<?php echo $fun->Enval($d3['id_prov']); ?>"><?php echo $d3['nama_prov']; ?></option>
							<?php
						}
						?>
					</select>
				</div>

				<div class="col s6">
					<span class="bold">Area</span> : <br>
					<span class="italic"><?php echo '['.$df2['nama_kabkot'].']'; ?></span> <br>
					<select class="browser-default txt-black" name="<?php echo $fun->Enlink('kabkot'); ?>" id="<?php echo $fun->Enlink('kabkot'); ?>" required>
						<option value="">Choose province first...</option>
					</select>
				</div>
			</div>

			<div class="m-t-30">
				<span>Status</span> : <br>
				<select class="browser-default" name="<?php echo $fun->Enlink('status'); ?>" id="<?php echo $fun->Enlink('status'); ?>">
					<option value="<?php echo $fun->Enval('0'); ?>" <?php if($df1['active'] == '0') echo 'selected'; ?> >Not Active</option>
					<option value="<?php echo $fun->Enval('1'); ?>" <?php if($df1['active'] == '1') echo 'selected'; ?> >Active</option>
				</select>
			</div>

			<div class="input-field">
				<?php
				$fun->fileField('gambar', 'gambar', 'Image', 'validate', 'txt-black');
				?>
			</div>

			<?php
			$fun->buttonField('submit', 'confirm', 'confirm', 'btn btn-large width-100 waves-effect waves-light primary-color m-t-30 borad-20', 'Update');
			?>

		</form>

	</div>
</div>

<script type="text/javascript">
	$('#toBack').click(function() {
		window.location.href = "?pg=location";
	});
</script>
