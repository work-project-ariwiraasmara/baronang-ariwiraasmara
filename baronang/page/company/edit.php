<?php
$scmpy = "SELECT * from 12mastercompany where companyid='".$fun->getIDParam('idcmpy')."'";
$qcmpy = mysqli_query($fun->getConnection(), $scmpy) or die(mysqli_error($fun->getConnection()));
$dcmpy = mysqli_fetch_array($qcmpy);

$sloccmpy = "SELECT a.nama_prov as nama_prov, b.nama_kabkot as nama_kabkot
		from 11amasterprovince as a 
		inner join 11bmastercity as b 
			on a.id_prov = b.id_prov 
		where a.id_prov='".$dcmpy['province']."' and b.id_kabkot='".$dcmpy['city']."'";
$qloccmpy = mysqli_query($fun->getConnection(), $sloccmpy) or die(mysqli_error($fun->getConnection()));
$dloccmpy = mysqli_fetch_array($qloccmpy);
?>

<div class="animated fadeinup delay-1">
	<div class="page-content">

		<h1 class="title uppercase txt-black" id="title"></h1>

		<form action="<?php echo 'process/05beditcompany.php?'.$fun->setIDParam('idcmpy', $fun->getIDParam('idcmpy') ); ?>" method="post" enctype="multipart/form-data">
			
			<div class="input-field">
				<?php
				$fun->inputField('text', 'nama', 'nama', 'Name', 'validate', 'txt-black', $dcmpy['companyname'], 'required');
				?>
			</div>

			<div class="input-field">
				<?php
				$fun->inputField('text', 'email', 'email', 'Email', 'validate', 'txt-black', $dcmpy['companyemail']);
				?>
			</div>

			<div class="input-field">
				<?php
				$fun->inputField('text', 'tlp', 'tlp', 'Phone Number', 'validate', 'txt-black', $dcmpy['companyphone']);
				?>
			</div>

			<div class="input-field">
				<?php
				$fun->inputField('text', 'label1', 'label1', 'Label 1', 'validate', 'txt-black', $dcmpy['labeltier1']);
				?>
			</div>

			<div class="input-field">
				<?php
				$fun->inputField('text', 'label2', 'label2', 'Label 2', 'validate', 'txt-black', $dcmpy['labeltier2']);
				?>
			</div>

			<div class="input-field">
				<?php
				$fun->inputField('text', 'label3', 'label3', 'Label 3', 'validate', 'txt-black', $dcmpy['labeltier3']);
				?>
			</div>

			<div class="input-field">
				<?php
				$fun->inputField('text', 'label4', 'label4', 'Label 4', 'validate', 'txt-black', $dcmpy['labeltier4']);
				?>
			</div>

			<div class="input-field">
				<?php
				$fun->inputField('text', 'label5', 'label5', 'Label 5', 'validate', 'txt-black', $dcmpy['labeltier5']);
				?>
			</div>

			<div class="input-field">
				<?php
				$fun->inputField('text', 'labellokasi', 'labellokasi', 'Location\'s Label', 'validate', 'txt-black', $dcmpy['labellocation']);
				?>
			</div>

			<div class="input-field">
				<?php
				$fun->inputField('text', 'labeltenant', 'labeltenant', 'Tenant\'s Label', 'validate', 'txt-black', $dcmpy['labeltenant']);
				?>
			</div>

			<div class="input-field">
				<?php
				$fun->inputField('text', 'labelvehicle', 'labelvehicle', 'Vehicle\'s Label', 'validate', 'txt-black', $dcmpy['labelvehicle']);
				?>
			</div>

			<div class="input-field">
				<?php
				$fun->fileField('gambar', 'gambar', 'Image', 'validate', 'txt-black');
				?>
			</div>

			<div class="row m-t-30">
				<div class="col s12 input-field">
					<?php
					$fun->inputField('text', 'alamat', 'alamat', 'Address', 'validate', 'txt-black', $dcmpy['companyaddress']);
					?>
				</div>

				<div class="col s6">
					<span class="bold">Province</span> : <br>
					<span class="italic"><?php echo '['.$dloccmpy['nama_prov'].']'; ?></span><br>
					<select class="browser-default txt-black" name="<?php echo $fun->Enlink('prov'); ?>" id="<?php echo $fun->Enlink('prov'); ?>" required>
						<?php
						$sprov = "SELECT * from 11amasterprovince order by id_prov";
						$qprov = mysqli_query($fun->getConnection(), $sprov) or die(mysqli_error($fun->getConnection()));
						while($dprov = mysqli_fetch_array($qprov)) { ?>
							<option value="<?php echo $fun->Enval($dprov['id_prov']); ?>"><?php echo $dprov['nama_prov']; ?></option>
							<?php
						}
						?>
					</select>
				</div>

				<div class="col s6">
					<span class="bold">Area</span> : <br>
					<span class="italic"><?php echo '['.$dloccmpy['nama_kabkot'].']'; ?></span><br>
					<select class="browser-default txt-black" name="<?php echo $fun->Enlink('kabkot'); ?>" id="<?php echo $fun->Enlink('kabkot'); ?>" required>
						<option value="">Choose province first...</option>
					</select>
				</div>
			</div>

			<?php
			$fun->buttonField('submit', 'confirm', 'confirm', 'btn btn-large width-100 waves-effect waves-light primary-color m-t-30 borad-20', 'Update');
			?>

		</form>

	</div>
</div>

<script type="text/javascript">
	$('#toBack').click(function() {
		window.location.href = "?pg=company";
	});
</script>