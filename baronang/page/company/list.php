<div class="animated fadeinup delay-1">
	<div class="page-content">

		<h1 class="title uppercase txt-black" id="title"></h1>

		<?php
		$sbprov = "SELECT distinct 	a.id_prov as id_prov, a.nama_prov as nama_prov
				from 11amasterprovince as a 
				inner join 12mastercompany as b 
					on a.id_prov = b.province
				order by nama_prov ASC";
		$qbprov = mysqli_query($fun->getConnection(), $sbprov) or die(mysqli_error($fun->getConnection()));
		$rbprov = mysqli_num_rows($qbprov);

		if($rbprov > 0) { ?>
			<ul id="myUL" class="txt-black">
				<?php
				while($dbprov = mysqli_fetch_array($qbprov)) { ?>
					<li class="m-b-10 txt-black"><span class="caret"><?php echo $dbprov['nama_prov']; ?></span>
						<ul class="nested txt-black">
							<?php
							$s2 = "SELECT * from 12mastercompany where province='".$dbprov['id_prov']."' order by companyname ASC";
							$q2 = mysqli_query($fun->getConnection(), $s2) or die(mysqli_error($fun->getConnection()));
							$r2 = mysqli_num_rows($q2);
							if($r2 > 0) {
								while($d2 = mysqli_fetch_array($q2)) { ?>
									<li class="m-l-30 txt-black m-b-10" style=""><a href="<?php echo '#'.$fun->Enlink($d2['companyid']); ?>" class="modal-trigger"><?php echo $d2['companyname']; ?></a></li>
									<?php
								}
							}
							else { ?>
								<li class="m-l-30 italic">[Data not available]</li>
								<?php
							}
							?>
						</ul>
					</li>
					<?php
				} ?>
			</ul>
			<?php
		}
		else { ?>
			<h1 class="uppercase bold center">[Data not available]</h1>
			<?php
		}
		?>

		<div class="floating-button page-fab animated bouncein delay-3" style="z-index: 1; margin-bottom: 75px;">
			<a class="btn-floating btn-large waves-effect waves-light primary-color btn z-depth-1" href="?pg=company&sb=add">
				<i class="ion-android-add"></i>
			</a>
		</div>
	
	</div>
</div>

<script type="text/javascript">
	$('#toBack').click(function() {
		window.location.href = "<?php echo '?#'.$fun->Enlink('baronang'); ?>";
	});
</script>

<?php
$sbprov = "SELECT distinct 	b.id_prov as id_prov, c.id_kabkot as id_kabkot,
				 			b.nama_prov as nama_prov, c.nama_kabkot as nama_kabkot
				from 12mastercompany as a 
				inner join 11amasterprovince as b 
					on a.province = b.id_prov 
				inner join 11bmastercity as c 
					on a.city = c.id_kabkot
				order by nama_prov ASC";
$qbprov = mysqli_query($fun->getConnection(), $sbprov) or die(mysqli_error($fun->getConnection()));
$rbprov = mysqli_num_rows($qbprov);
if($rbprov > 0) {
	while($dbprov = mysqli_fetch_array($qbprov)) {
		$s2 = "SELECT * from 12mastercompany where province='".$dbprov['id_prov']."' order by companyname ASC";
		$q2 = mysqli_query($fun->getConnection(), $s2) or die(mysqli_error($fun->getConnection()));
		$r2 = mysqli_num_rows($q2);
		if($r2 > 0) {
			while($d2 = mysqli_fetch_array($q2)) { ?>
				<div class="modal borad-20" id="<?php echo $fun->Enlink($d2['companyid']); ?>">
					<div class="modal-content choose-date txt-black">
						
						<?php
						if($d2['companylogo'] != '' || !empty($d2['companylogo']) ) { ?>
							<p class="center"><img src="<?php echo 'images/company/'.$d2['companylogo']; ?>"></p>
							<?php
						}
						?>

						<p><h1 class="txt-black bold"><?php echo $d2['companyname']; ?></h1></p>
						<p><span class="bold">Email</span> : <?php echo $d2['companyemail']; ?></p>
						<p><span class="bold">Phone Number</span> : <?php echo $d2['companyphone']; ?></p>
						<p><span class="bold">Address</span> : <?php echo $d2['companyaddress'].', '.$dbprov['nama_kabkot'].', '.$dbprov['nama_prov']; ?></p>

						<p class="txt-white"><a href="<?php echo '?pg=company&sb=edit&'.$fun->setIDParam('idcmpy', $d2['companyid']); ?>" class="btn btn-large primary-color width-100 waves-effect waves-light borad-20"><i class="ion-android-create"></i></a></p>
					</div>
				</div>
				<?php
			}
		}
	}
}
?>