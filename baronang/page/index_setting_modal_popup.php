<?php
if( $logexpire > $exptime ) { ?>
	<div class="modal borad-20" id="<?php echo $fun->Enlink('logout'); ?>">
		<div class="modal-content choose-date">
			<p><h1 class="txt-black center">Your login session has expired!<br>Please, sign in again!</h1></p>
			<p class="txt-white"><a href="logout.php" class="btn btn-large width-100 primary-color waves-light waves-effect borad-20 m-t-30">OK</a></p>
		</div>
	</div>
	<?php
}
?>

<div class="modal borad-20" id="<?php echo $fun->Enlink('options'); ?>">
	<div class="modal-content choose-date txt-black">
		<?php
		if($session == 'member') { ?>
			<p><a href="?pg=profil" id="<?php echo $fun->Enlink('menuprofil'); ?>" class="modal-close"><i class="ion-android-person"></i> Profile</a></p>
			<p><a href="<?php echo '#'.$fun->Enlink('applycard'); ?>" id="<?php echo $fun->Enlink('link_card'); ?>" class="modal-trigger"><i class="ion-card"></i> Apply Card</a></p>
			<?php
		}
		?>
									
		<p><a href="?pg=shop"><i class="ion-android-cart"></i> Cart</p>
		<p><a href="?pg=shop&sb=invoice"><i class="ion-android-document"></i> Invoice</p>
		<p><a href="<?php echo '#'.$fun->Enlink('setlang'); ?>" id="<?php echo $fun->Enlink('link_lang'); ?>" class="modal-trigger"><i class="ion-android-globe"></i> Language</p>
		<p><a href="logout.php"><i class="ion-log-out"></i> Logout</a></p>
	</div>
</div>

<div class="modal borad-20" id="<?php echo $fun->Enlink('setlang'); ?>">
	<div class="modal-content choose-date txt-black">
		<p>
			<h2 class="txt-black center bold">Choose Language</h2>
			<div class="center" id="google_translate_element"></div>
		</p>
	</div>
</div>

<a href="<?php echo '#'.$fun->Enlink('logout') ?>" id="<?php echo $fun->Enlink('toLogoutForced'); ?>" class="hide modal-trigger">Logout</a>
						
<div class="modal borad-20" id="<?php echo $fun->Enlink('kamera'); ?>">
	<div class="modal-content choose-date txt-black">
		<div class="row">
									
			<div class="col s12 center" id="frame_webcodecam">
				<canvas id="webcodecam-canvas" class="width-100"></canvas>
				<div class="scanner-laser laser-rightBottom" style="opacity: 0.5;"></div>
				<div class="scanner-laser laser-rightTop" style="opacity: 0.5;"></div>
				<div class="scanner-laser laser-leftBottom" style="opacity: 0.5;"></div>
				<div class="scanner-laser laser-leftTop" style="opacity: 0.5;"></div>
			</div>

			<div class="col s12 center">
				<div id="result">
					<img id="scanned-img" src="">
					<div class="center">
						<p class="hide" id="scanned-QR"></p>
					</div>
				</div>
			</div>

			<div class="col s12">
				<div id="<?php echo $fun->Enlink('qrdetail'); ?>" class="txt-black m-t-30"></div>
			</div>

			<div class="col s6">
				<button id="<?php echo $fun->Enlink('btn_checkqr'); ?>" class="borad-20 waves-effect waves-light btn-large primary-color width-100 m-t-30 hide">Detail</button>
			</div>

			<div class="col s6">
				<button id="<?php echo $fun->Enlink('btn_submit'); ?>" class="borad-20 waves-effect waves-light btn-large primary-color width-100 m-t-30 hide">OK</button>
			</div>

			<div class="col s6 hide">
				<button title="Mulai Pindai" class="borad-20 waves-effect waves-light btn-large primary-color width-100 m-t-30" id="play" type="button"><i class="ion-play"></i></button>
			</div>

			<div class="col s6 hide">
				<button title="Hentikan Streaming" class="borad-20 waves-effect waves-light btn-large primary-color width-100 m-t-30" id="stop" type="button" disabled><i class="ion-stop"></i></button>
			</div>

			<div class="col s12 hide">
				<select class="browser-default" id="camera-select"></select>
				<input type="text"name="uid" id="result-code" value="">
			</div>								

		</div>

	</div>
</div>

<div class="modal borad-20" id="<?php echo $fun->Enlink('confirm-pin'); ?>">
	<div class="modal-content choose-date txt-black">

		<p><h1 class="title txt-black center">pin confirmation</h1></p>

		<p class="justify" id="<?php echo $fun->Enlink('confirm-pin-msg'); ?>"></p>
		
		<div class="input-field">
			<?php
			$fun->inputField('password', 'cfmpin', 'cfmpin', 'PIN', 'validate', 'txt-black', null, 'required');
			$fun->inputField('text', 'idttrigger', 'idttrigger', '', 'hide', 'txt-black', null, 'required');
			?>
		</div>

		<p><h4 class="title txt-black center" id="<?php $fun->Enlink('pinres'); ?>"></h4></p>

		<button id="<?php echo $fun->Enlink('btn_pinconfirm'); ?>" class="borad-20 waves-effect waves-light btn-large primary-color width-100 m-t-10">OK</button>

	</div>
</div>

<?php
$fun->linkTo('#'.$fun->Enlink('confirm-pin'), 'to Confirm Pin', 'modal-trigger hide', 'toConfirmPIN');
?>