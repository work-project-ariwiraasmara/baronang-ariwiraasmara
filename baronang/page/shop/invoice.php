<div class="animated fadeinup delay-1">
	<div class="page-content">

		<h1 class="title uppercase txt-black" id="title"></h1>

		<div class="m-t-10 txt-black">
			<?php
			if(isset($_GET[$fun->Enlink('detail')])) { 
				$smember = "SELECT * from 22bmastermember where mastermemberid='".$fun->getValookie('id')."'";
				$qmember = mysqli_query($fun->getConnection(), $smember) or die(mysqli_error($fun->getConnection()));
				$dmember = mysqli_fetch_array($qmember);

				$sinvoice = "SELECT * from 23fproductinvoice where invoiceid='".$fun->getIDParam('detail')."' and memberid='".$fun->getValookie('id')."' order by datecreate desc";
				$qinvoice = mysqli_query($fun->getConnection(), $sinvoice) or die(mysqli_error($fun->getConnection()));
				$dinvoice = mysqli_fetch_array($qinvoice);

				$smcp = "SELECT mastercardid, points from 23amastercard where cardtype='SkyCard' and memberid='".$fun->getValookie('id')."'";
				$qmcp = mysqli_query($fun->getConnection(), $smcp) or die(mysqli_error($fun->getConnection()));
				$dmcp = mysqli_fetch_array($qmcp);

				if($dinvoice['method_payment'] == 'skypoint') { 
					$cardtext = "Card Number";
					$cardnumber = $dmcp['mastercardid'];
					$mp = 'Sky Point'; 
				}
				else { 
					$cardtext = "VA BCA";
					$cardnumber = "5278".$dmember['memberphone'];
					$mp = 'VA BCA'; 
				}

				if($dinvoice['status'] == '2') {
					$action = "process/42econfirmationpaymentproductshopnonskypoint.php?".$fun->setIDParam('ID', $fun->getIDParam('detail'));
					$method = "post"; 
				}
				else {
					$action = "return false;";
					$method = "return false;"; 
				}

				$amount = (int)$dinvoice['total'] - (int)$dinvoice['total_paid'];
				?>
				<form action="<?php echo $action; ?>" method="<?php echo $method; ?>">
					<h2 class="txt-black bold m-b-30"><?php echo $fun->getIDParam('detail'); ?></h2>
					<p><span class="bold">Total Purchase</span> : Rp. <?php echo $fun->FormatRupiah($dinvoice['total']); ?></p>

					<?php
					if($dinvoice['status'] == '2') { 
						$intdatenow = strtotime(date('Y-m-d H:i:s'));

						if($dinvoice['dateinvalid'] == '' || empty($dinvoice['dateinvalid'])) { 
							$dateinvalid = date('Y-m-d H:i:s', strtotime($dinvoice['datecreate'] . ' +3 hours'));
							$intdatevalid = strtotime($dinvoice['datecreate'] . ' +3 hours');
						}
						else {
							$dateinvalid = $dinvoice['dateinvalid'];
							$intdatevalid =  strtotime($dinvoice['dateinvalid']);
						}
						?>
						<p><span class="bold">Date Invalid</span> : <?php echo $dateinvalid; ?></p>
						<?php
					}
					?>

					<p><span class="bold">Date Shop</span> : <?php echo $dinvoice['datecreate']; ?></p>
					<p><span class="bold">Method Payment</span> : <?php echo $mp; ?></p>
					<p><span class="bold"><?php echo $cardtext; ?></span> : <?php echo $cardnumber; ?></p>
					<p><span class="bold">Total Paid</span> : Rp. <?php echo $fun->FormatRupiah($dinvoice['total_paid']); ?></p>
					<p><span class="bold">Amount</span> : Rp. <?php echo $fun->FormatRupiah($amount); ?></p>
					<p><span class="bold">Products</span> : </p>
					<?php
					$s2 = "SELECT * from 23eproductshop where invoiceid='".$fun->getIDParam('detail')."' and status='1' order by datecreated desc";
					$q2 = mysqli_query($fun->getConnection(), $s2) or die(mysqli_error($fun->getConnection()));
					while($d2 = mysqli_fetch_array($q2)) { 

						$s3 = "SELECT * from 22amasterproduct where masterproductid='".$d2['productid']."'";
						$q3 = mysqli_query($fun->getConnection(), $s3) or die(mysqli_error($fun->getConnection()));
						$d3 = mysqli_fetch_array($q3);

						$subtotal = (int)$d2['qty'] * (int)$d3['productprice'];

						?>
						<div class="single-news animated fadeinright delay-3 width-100 m-t-10">
							<p class="bold"><?php echo $d3['productdescription']; ?></p>
							<p><?php echo $d3['vehicletype']; ?></p>
							<p><?php echo $d3['validity_days'].' '.$d3['unit']; ?></p>
							<p><?php echo 'Rp. '.$fun->FormatRupiah($d3['productprice']); ?><span class="bold"><?php echo ' x'.$d2['qty']; ?></span></p>
							<p><span class="bold">Subtotal</span> : Rp. <?php echo $fun->FormatRupiah($subtotal); ?></p>
						</div>
						<?php
					}

					if($dinvoice['status'] == '2') { 
						if($intdatevalid > $intdatenow) { ?>
							<div class="center m-t-30 bold" id="timer_countdown" style="font-size: 20px;">Time Countdown</div>
							<?php

							if($dinvoice['method_payment'] == 'skypoint') {
								$fun->buttonField('button', 'btn_splitpoint', 'btn_splitpoint', 'btn btn-large width-100 waves-effect waves-light primary-color m-t-10 borad-20', 'Split Point');
							}

							$fun->buttonField('submit', 'confirm', 'confirm', 'btn btn-large width-100 waves-effect waves-light primary-color m-t-10 borad-20', 'Confirm Payment');
						}
						else { ?>
							<div class="m-t-30">
								<h1 class="center uppercase txt-black bold">this transaction has expired!</h1>
							</div>
							<?php
						}
					}
					?>
				</form>
				<?php
			}
			else {
				$sinvoice = "SELECT * from 23fproductinvoice where memberid='".$fun->getValookie('id')."' order by datecreate desc";
				$qinvoice = mysqli_query($fun->getConnection(), $sinvoice) or die(mysqli_error($fun->getConnection()));
				$rinvoice = mysqli_num_rows($qinvoice);

				if($rinvoice > 0) {
					$intdatenow = strtotime(date('Y-m-d H:i:s'));
					while($dinvoice = mysqli_fetch_array($qinvoice)) { 
						$expired = 0;

						if($dinvoice['dateinvalid'] == '' || empty($dinvoice['dateinvalid'])) {
							$intdatevalid = strtotime($dinvoice['datecreate'] . ' +3 hours');
						}
						else {
							$intdatevalid =  strtotime($dinvoice['dateinvalid']);
						}

						if($dinvoice['status'] == '2') { 
							

							if($intdatevalid > $intdatenow) {
								$br 	= "<br>";
								$status = 'waiting for payment..'; 
								$class 	= "bold italic";
							}
							else {
								$br 	= "";
								$status = 'expired'; 
								$class 	= "bold italic right";
							}
						}
						else { 
							$br 	= "";
							$class 	= "bold right";
							$status = 'paid'; 
						}

						?>
						<a href="<?php echo '?pg=shop&sb=invoice&'.$fun->setIDParam('detail', $dinvoice['invoiceid']); ?>" class="">
							<div class="single-news animated fadeinright delay-3 width-100 m-t-10 waves-effect waves-light">
								<span class="left"><?php echo $dinvoice['invoiceid']; ?></span><?php echo $br; ?>
								<span class="<?php echo $class; ?>"><?php echo $status; ?></span>
							</div>
						</a>
						<?php
					}
				}
				else { ?>
					<div class="single-news animated fadeinright delay-3 width-100 m-t-20">
						<h3 class="uppercase bold txt-black center">There are no transaction!</h3>
					</div>
					<?php
				}
			}
			?>
		</div>

	</div>
</div>

<script type="text/javascript">
	$('#toBack').click(function() {

		<?php
		if(isset($_GET[$fun->Enlink('detail')])) { ?>
			window.location.href = "?pg=shop&sb=invoice";
			<?php
		}
		else { ?>
			window.location.href = "?";
			<?php
		}
		?>

	});

	<?php
	if($dinvoice['status'] == '2') { ?>
		var countDownDate = new Date("<?php echo date('M d, Y H:i:s', strtotime($dateinvalid)); ?>").getTime();
		console.log(countDownDate);

		// Update the count down every 1 second
		var x = setInterval(function() {

			// Get today's date and time
		  	var now = new Date().getTime();
		    
		  	// Find the distance between now and the count down date
		  	var distance = countDownDate - now;
		    
		  	// Time calculations for days, hours, minutes and seconds
		  	var days = Math.floor(distance / (1000 * 60 * 60 * 24));
		  	var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
		  	var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
		  	var seconds = Math.floor((distance % (1000 * 60)) / 1000);
		    
		  	// Output the result in an element with id="demo"
		  	document.getElementById("timer_countdown").innerHTML = days + "d " + hours + "h " + minutes + "m " + seconds + "s ";
		    
		  	// If the count down is over, write some text 
		  	if (distance < 0) {
		    	clearInterval(x);
		    	document.getElementById("timer_countdown").innerHTML = "EXPIRED";
		  	}
		}, 1000);
		<?php
	}
	?>
</script>