<div class="animated fadeinup delay-1">
	<div class="page-content">

		<h1 class="title uppercase txt-black" id="title"></h1>

		<div class="m-t-30">
			<form action="?pg=shop&sb=payment" method="post">
				<div id="<?php echo $fun->Enlink('product-shop'); ?>">
					<?php
					$card = "";
					$btn_purchase = 0;
					$scart = "SELECT * from 23eproductshop where status='2' and memberid='".$fun->getValookie('id')."' order by datecreated asc";
					$qcart = mysqli_query($fun->getConnection(), $scart) or die(mysqli_error($fun->getConnection()));
					$rcart = mysqli_num_rows($qcart);

					if($rcart > 0) {
						$total = 0;
						$no = 1;
						while($dcart = mysqli_fetch_array($qcart)) {
							$card = $dcart['cardid'];

							if($dcart['qty'] == '' || empty($dcart['qty'])) {
								$qty = 1;
							}
							else {
								$qty = $dcart['qty'];
							}

							$s2 = "SELECT 	a.masterproductid as masterproductid,
											a.productdescription as productdescription,
											a.vehicletype as vehicletype,
											a.validity_days as validity_days,
											a.unit as unit,
											a.productprice as productprice,
											b.locationname as locationname  
									from 22amasterproduct as a 
									inner join 21masterlocation as b 
										on a.locationid = b.masterlocationid
									where a.masterproductid='".$dcart['productid']."'";
							$q2 = mysqli_query($fun->getConnection(), $s2) or die(mysqli_error($fun->getConnection()));
							$d2 = mysqli_fetch_array($q2);

							$subtotal = (int)$d2['productprice'] * (int)$qty;
							?>
							<?php //SHOP ID  ?><input type="text" name="<?php echo $fun->Enlink('id').'['.$no.']'; ?>" id="<?php echo $fun->Enlink('id').'-'.$no; ?>" value="<?php echo $fun->Enval($dcart['productshopid']); ?>" class="hide" readonly>
							<input type="text" name="<?php echo $fun->Enlink('price').'['.$no.']'; ?>" id="<?php echo $fun->Enlink('price').'-'.$no; ?>" value="<?php echo $d2['productprice']; ?>" class="hide" readonly>
							<input type="text" name="<?php echo $fun->Enlink('subtotal').'['.$no.']'; ?>" id="<?php echo $fun->Enlink('subtotal-'.$no); ?>" value="<?php echo $subtotal; ?>" class="hide" readonly>
							<div class="single-news animated fadeinright delay-3 width-100 m-t-10">
								<p class="bold"><?php echo $d2['productdescription']; ?></p>
								<p><span class="bold">Location</span> : <?php echo $d2['locationname']; ?></p>
								<p><span class="bold">Vehicle Type</span> : <?php echo $d2['vehicletype']; ?></p>
								<p><span class="bold">Valid Day(s)</span> : <?php echo $d2['validity_days'].' '.$d2['unit']; ?></p>
								<p><span class="bold">Start At</span> : <?php echo date('d F Y', strtotime($dcart['datestart'])); ?></p>
								<p><span class="bold">Price</span> : Rp. <?php echo number_format($d2['productprice'],0,',','.'); ?></p>
								<p><span class="bold">Sub Total</span> : Rp. <span id="<?php echo $fun->Enlink('subtotal_label-'.$no); ?>"><?php echo number_format($subtotal,0,',','.'); ?></p></span>
								<div class="m-t-10" style="margin-right: -30px;">
									<div class="left" style="margin-right: 50px;">
										<input type='button' class='qtyminus' id="<?php echo $fun->Enlink('qtyminus-'.$no); ?>" value='-' field="quantity" />
								    
										<span id="<?php echo $fun->Enlink('qtylabel-'.$no); ?>"><?php echo $qty; ?></span>
										<?php //QUANTITY ?><input type='number' name='<?php echo $fun->Enlink('qty').'['.$no.']'; ?>' id="<?php echo $fun->Enlink('qty-'.$no); ?>" value='<?php echo $qty; ?>' class='hide'>
										    
										<input type='button' class='qtyplus' id="<?php echo $fun->Enlink('qtyplus-'.$no); ?>" value='+' field="quantity" />
									</div>
									
									<div class="right" style="margin-top: 3px;">
										<input type="checkbox" name="<?php echo $fun->Enlink('cbproduct').'['.$no.']'; ?>" id="<?php echo $fun->Enlink('cbproduct-'.$no); ?>" value="<?php echo $fun->Enval(1); ?>" checked>
										<label for="<?php echo $fun->Enlink('cbproduct-'.$no); ?>"></label>
									</div><br>
								</div>
							</div>

							<script type="text/javascript">
								$("<?php echo '#'.$fun->Enlink('qtylabel-'.$no); ?>").click(function(){
									//$("<?php echo '#'.$fun->Enlink('qty-'.$no); ?>").removeClass("hide");
									//$("<?php echo '#'.$fun->Enlink('qtylabel-'.$no); ?>").addClass("hide");
								});

								$("<?php echo '#'.$fun->Enlink('cbproduct-'.$no); ?>").click(function(){
									var sbt = $("<?php echo '#'.$fun->Enlink('subtotal-'.$no); ?>").val();
									
									if( $("<?php echo '#'.$fun->Enlink('cbproduct-'.$no); ?>").is(':checked') ) {
										$("<?php echo '#'.$fun->Enlink('cbproduct-'.$no); ?>").val("<?php echo $fun->Enval(1); ?>");
										total = parseInt(total) + parseInt(sbt);
									}
									else {
										$("<?php echo '#'.$fun->Enlink('cbproduct-'.$no); ?>").val("<?php echo $fun->Enval(2); ?>");
										total = parseInt(total) - parseInt(sbt);
									}

									var ftotal = new Number(total.toFixed(0)).toLocaleString("id-ID");
									$("<?php echo '#'.$fun->Enlink('total'); ?>").val(total);
									$("<?php echo '#'.$fun->Enlink('total-label'); ?>").html(ftotal);
								});

								$("<?php echo '#'.$fun->Enlink('qtyminus-'.$no); ?>").click(function(){
									var prc = $("<?php echo '#'.$fun->Enlink('price').'-'.$no; ?>").val();
									var qty = $("<?php echo '#'.$fun->Enlink('qty-'.$no); ?>").val();
									var sbt = $("<?php echo '#'.$fun->Enlink('subtotal-'.$no); ?>").val();
									var res = parseInt(qty) - 1;
									if(res < 1) {
										//res = 1;
										var id = $("<?php echo '#'.$fun->Enlink('id').'-'.$no; ?>").val();
										$.ajax({
									    	url : "process/ajax/ajax_delete_product_shop.php",
									    	type : 'POST',
									    	data: { 
									        	productid: id
									    	},
									    	success : function(data) {
									    		//console.log(data);
									    		window.location.reload();
									    	},
									    	error : function(){
									        	alert('Terjadi kesalahan dalam jaringan!\nTidak dapat mengambil data Product!\nSilahkan cek koneksi jaringan dan coba lagi!');
									        	return false;
									    	}
									    });
									}
									else {
										sbt = parseInt(sbt) - parseInt(prc);
										total = parseInt(total) - parseInt(prc);
									}

									var ftotal = new Number(total.toFixed(0)).toLocaleString("id-ID");
									var fsbt = new Number(sbt.toFixed(0)).toLocaleString("id-ID");
									$("<?php echo '#'.$fun->Enlink('subtotal-'.$no); ?>").val(sbt);
									$("<?php echo '#'.$fun->Enlink('subtotal_label-'.$no); ?>").html(fsbt);
									$("<?php echo '#'.$fun->Enlink('total'); ?>").val(total);
									$("<?php echo '#'.$fun->Enlink('total-label'); ?>").html(ftotal);
									$("<?php echo '#'.$fun->Enlink('qty-'.$no); ?>").val(res);
									$("<?php echo '#'.$fun->Enlink('qtylabel-'.$no); ?>").html(res);
								});

								$("<?php echo '#'.$fun->Enlink('qtyplus-'.$no); ?>").click(function(){
									var prc = $("<?php echo '#'.$fun->Enlink('price').'-'.$no; ?>").val();
									var qty = $("<?php echo '#'.$fun->Enlink('qty-'.$no); ?>").val();
									var sbt = $("<?php echo '#'.$fun->Enlink('subtotal-'.$no); ?>").val();
									var res = parseInt(qty) + 1;
									total = parseInt(total) + parseInt(prc);
									sbt = parseInt(sbt) + parseInt(prc);

									var ftotal = new Number(total.toFixed(0)).toLocaleString("id-ID");
									var fsbt = new Number(sbt.toFixed(0)).toLocaleString("id-ID");
									$("<?php echo '#'.$fun->Enlink('subtotal-'.$no); ?>").val(sbt);
									$("<?php echo '#'.$fun->Enlink('subtotal_label-'.$no); ?>").html(fsbt);
									$("<?php echo '#'.$fun->Enlink('total'); ?>").val(total);
									$("<?php echo '#'.$fun->Enlink('total-label'); ?>").html(ftotal);
									$("<?php echo '#'.$fun->Enlink('qty-'.$no); ?>").val(res);
									$("<?php echo '#'.$fun->Enlink('qtylabel-'.$no); ?>").html(res);
								});
							</script>
							<?php
							$btn_purchase++; $no++;
							$total = $total + $subtotal;
						}
					}
					else { ?>
						<div class="single-news animated fadeinright delay-3 waves-effect waves-light width-100">
							<h1 class="center bold txt-black">You're cart is empty</h1>
						</div>
						<?php
					}

					if($btn_purchase > 0) { ?>
						<div class="m-t-30 row">
							<div class="col s6 bold">
								<h5 class="txt-black right bold">
									Total : Rp.
								</h5>
							</div>

							<div class="col s6">
								<h5 class="txt-black bold" id="<?php echo $fun->Enlink('total-label'); ?>">
									<?php
									echo number_format($total,0,',','.');
									?>
								</h5>
							</div>
						</div>

						<?php
						// CARD ID
						$fun->inputField('text', 'card', 'card', '', 'validate hide', 'txt-black', $fun->Enval($card), 'required', 'readonly');

						// TOTAl BELANJA
						$fun->inputField('number', 'total', 'total', '', 'validate hide', 'txt-black', $total, 'required', 'readonly');

						$fun->buttonField('submit', 'confirm', 'confirm', 'btn btn-large width-100 waves-effect waves-light primary-color m-t-10 borad-20', 'Purchase');
					}
					?>
				</div>
			</form>
		</div>

	</div>
</div>

<script type="text/javascript">
	$('#toBack').click(function() {
		window.location.href = "?";
	});

	<?php
	if($rcart > 0) { ?>
		var total = "<?php echo $total; ?>";
		<?php
	}
	?>
</script>