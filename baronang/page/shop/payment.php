<div class="animated fadeinup delay-1">
	<div class="page-content">

		<h1 class="title uppercase txt-black">Choose Method Payment</h1>

		<div class="m-t-30">
			
			<form action="process/42dproductshop.php" method="post">

				<?php
				$total = @$_POST[$fun->Enlink('total')] / 1000;
				$smcp = "SELECT mastercardid, points from 23amastercard where cardtype='SkyCard' and memberid='".$fun->getValookie('id')."'";
				$qmcp = mysqli_query($fun->getConnection(), $smcp) or die(mysqli_error($fun->getConnection()));
				$dmcp = mysqli_fetch_array($qmcp);

				if($dmcp['points'] <= $total) {
					if($dmcp['points'] != 0 || $dmcp['points'] != '' || empty($dmcp['points'])) {
						$totalva = (int)@$_POST[$fun->Enlink('total')] - ((int)$dmcp['points'] * 1000);
					}
					else {
						$totalva = (int)@$_POST[$fun->Enlink('total')] - ((int)$dmcp['points'] * 1000);
					}
				}
				else {
					$totalva = (int)@$_POST[$fun->Enlink('total')];
				}


				$rbsky_chk = "";
				if($dmcp['points'] != 0 || $dmcp['points'] != '' || !empty($dmcp['points']) ) { $rbsky_chk = "checked"; }
				else { $rbsky_chk = ""; }

				$rbvabca_chk = "";
				if($dmcp['points'] <= $total) { $rbvabca_chk = "checked"; }
				else { $rbvabca_chk = ""; }


				$paymentto_val = "";
				if( ($rbsky_chk == "checked") && ($rbvabca_chk == "checked") ) {
					$paymentto_val = "skypoint-vabca";
				}
				else { 
					if($rbsky_chk == "checked") {
						$paymentto_val = "skypoint";
					}
					else {
						if($rbvabca_chk == "checked") {
							$paymentto_val = "vabca";
						}
						else {
							$paymentto_val = ""; 
						}
					}
					
				}
				
				$resskypoint = (int)$dmcp['points'] - (int)$total;
				?>

				<div class="single-news animated fadeinright delay-3 width-100 m-t-10">
					<span class="bold">Total Shopping : </span> Rp. <?php echo $fun->FormatNumber(@$_POST[$fun->Enlink('total')]); ?>
				</div>

				<div class="single-news animated fadeinright delay-3 width-100 m-t-10">
					<div class="left">
						<input type="checkbox" name="<?php echo $fun->Enlink('payment'); ?>" id="<?php echo $fun->Enlink('skypoint') ?>" value="skypoint" <?php echo $rbsky_chk; ?> >
						<label for="<?php echo $fun->Enlink('skypoint') ?>" class="txt-black">Sky Point</label>
					</div>

					<div class="right bold italic" style="margin-top: 2px;">
						<?php echo $fun->FormatNumber($dmcp['points']).' Points'; ?>
					</div><br>
				</div>

				<div class="single-news animated fadeinright delay-3 width-100 m-t-10">
					<div class="left">
						<input type="checkbox" name="<?php echo $fun->Enlink('payment'); ?>" id="<?php echo $fun->Enlink('vabca'); ?>" value="vabca" <?php echo $rbvabca_chk; ?> >
						<label for="<?php echo $fun->Enlink('vabca'); ?>" class="txt-black">Virtual Account BCA</label>
					</div>

					<div class="right bold italic" style="margin-top: 2px;">
						<span id="<?php echo $fun->Enlink('vaamount'); ?>"><?php echo 'Rp. '.$fun->FormatNumber($totalva); ?></span>
					</div><br>
				</div>

				<?php //METODE PEMBAYARAN ?><input type="text" name="<?php echo $fun->Enlink('paymentto'); ?>" id="<?php echo $fun->Enlink('paymentto'); ?>" value="<?php echo $paymentto_val; ?>" class="hide" readonly>
				<?php //CARD ID ?><input type="text" name="<?php echo $fun->Enlink('card'); ?>" id="<?php echo $fun->Enlink('card'); ?>" value="<?php echo @$_POST[$fun->Enlink('card')]; ?>" class="hide" readonly>
				<?php //TOTAL BELANJA ?><input type="text" name="<?php echo $fun->Enlink('total'); ?>" id="<?php echo $fun->Enlink('total'); ?>" value="<?php echo $fun->Enval(@$_POST[$fun->Enlink('total')]); ?>" class="hide" readonly>
				<?php
				$fun->inputField('text', 'pointtxt', 'pointtxt', '', 'validate hide', 'txt-black', $dmcp['points'], '', 'readonly');
				
				$fun->buttonField('button', 'toconfirm', 'toconfirm', 'btn btn-large width-100 waves-effect waves-light primary-color m-t-30 borad-20', 'Pay');

				$fun->buttonField('submit', 'submit_payment_confirm', 'submit_payment_confirm', 'btn btn-large width-100 waves-effect waves-light primary-color m-t-10 borad-20 hide', 'to Process');

				$no = 1;
				foreach (@$_POST[$fun->Enlink('id')] as $key) { 

					$cb = $fun->Denval(@$_POST[$fun->Enlink('cbproduct')][$no]);

					if($cb == 1) { ?>
						<?php //SHOP ID & SHOP QUANTITY ?><input type="text" name="<?php echo $fun->Enlink('id').'['.$no.']';  ?>" id="<?php echo $fun->Enlink('id-'.$no);  ?>" value="<?php echo '--[ID]--'.$key.'--[QTY]--'.$fun->Enval(@$_POST[$fun->Enlink('qty')][$no]); ?>" class="hide">
						<br>
						<?php
					}

					$no++;
				}
				?>
			</form>

		</div>

	</div>
</div>

<?php
$fun->linkTo('#'.$fun->Enlink('notenough_skypoint_warning'), 'to Warning not enough skypoint', 'hide modal-trigger', 'toWarningSkypoint');
?>

<div class="modal borad-20" id="<?php echo $fun->Enlink('notenough_skypoint_warning'); ?>">
	<div class="modal-content choose-date txt-black justify">
		<p class="bold italic">Your Skypoint is not enough for the transaction!</p>
		<p>Your current Skypoint : <?php echo $fun->FormatNumber($dmcp['points']); ?> points</p>
		<p>Your total purchase : Rp. <?php echo $fun->FormatNumber(@$_POST[$fun->Enlink('total')]); ?> </p>
		<p>Please choose another payment method</p>
		
		<?php
		$fun->buttonField('button', 'confirm_warning_skypoint', 'confirm_warning_skypoint', 'btn btn-large width-100 waves-effect waves-light primary-color m-t-30 borad-20 modal-close', 'OK');
		?>

	</div>
</div>

<script type="text/javascript">
	var pay = "";
	var method = "";
	var skypoint = 0;
	var vabca = 0;
	var msg_payment = "";
	var msg_payment_amount = "";

	$('#toBack').click(function() {
		window.location.href = "?pg=shop";
	});

	//
	if( $("<?php echo '#'.$fun->Enlink('vabca'); ?>").is(':checked') ) {
		if( $("<?php echo '#'.$fun->Enlink('skypoint'); ?>").is(':checked') ) {
			msg_payment = "Skypoint and BCA Virtual Account";
			msg_payment_amount = "<?php echo $fun->FormatNumber($dmcp['points']).' Points and Rp. '.$fun->FormatNumber($totalva); ?>";
		}
		else {
			msg_payment = "BCA Virtual Account";
			msg_payment_amount = "<?php echo 'Rp. '.$fun->FormatNumber($totalva); ?>";
		}
	}
	else {
		if( $("<?php echo '#'.$fun->Enlink('skypoint'); ?>").is(':checked') ) {
			msg_payment = "Skypoint";
			msg_payment_amount = "<?php echo $fun->FormatNumber($dmcp['points']).' Points'; ?>";
		}
		else {
			msg_payment = "Nothing";
			msg_payment_amount = "";
		}
	}

	//
	$("<?php echo '#'.$fun->Enlink('skypoint'); ?>").click(function(){
		var total   = 0;
		var amount  = "<?php echo @$_POST[$fun->Enlink('total')]; ?>";
		var point   = $("<?php echo '#'.$fun->Enlink('pointtxt'); ?>").val();
		var payment = "";
		var ftotal  = 0;

		if( $("<?php echo '#'.$fun->Enlink('vabca'); ?>").is(':checked') ) {
			pay = $("<?php echo '#'.$fun->Enlink('vabca'); ?>").val();
			method = "vabca";
			vabca = 1;

			if( $("<?php echo '#'.$fun->Enlink('skypoint'); ?>").is(':checked') ) {
				skypoint = 1;
				total = parseInt(amount) - (parseInt(point) * 1000);
				if(total < 0) {
					total = 0;
				}

				ftotal = new Number(total.toFixed(0)).toLocaleString("id-ID");
				payment = "skypoint-vabca";
				msg_payment = "Skypoint and BCA Virtual Account";
				msg_payment_amount = "<?php echo $fun->FormatNumber($dmcp['points']).' Points and Rp. '; ?>" + ftotal;
			}
			else {
				skypoint = 0;
				total = parseInt(amount);
				ftotal = new Number(total.toFixed(0)).toLocaleString("id-ID");

				payment = "vabca";
				msg_payment = "BCA Virtual Account";
				msg_payment_amount = "Rp. " + ftotal;
			}
		}
		else {
			pay = "";
			method = "";
			vabca = 0;

			total = parseInt(amount);
			ftotal = new Number(total.toFixed(0)).toLocaleString("id-ID");

			if( $("<?php echo '#'.$fun->Enlink('skypoint'); ?>").is(':checked') ) {
				payment = "skypoint";
				msg_payment = "Skypoint";
				msg_payment_amount = "<?php echo $fun->FormatNumber($dmcp['points']).' Points'; ?>";
			}
			else {
				payment = "";
				msg_payment = "Nothing";
				msg_payment_amount = "";
			}
		}

		//console.log(payment);
		ftotal = new Number(total.toFixed(0)).toLocaleString("id-ID");
		$("<?php echo '#'.$fun->Enlink('vaamount'); ?>").html("Rp. " + ftotal);
		$("<?php echo '#'.$fun->Enlink('paymentto'); ?>").val(payment);
	});

	$("<?php echo '#'.$fun->Enlink('vabca'); ?>").click(function(){
		var total   = 0;
		var amount  = "<?php echo @$_POST[$fun->Enlink('total')]; ?>";
		var point   = $("<?php echo '#'.$fun->Enlink('pointtxt'); ?>").val();
		var payment = "";
		var ftotal;

		if( $("<?php echo '#'.$fun->Enlink('vabca'); ?>").is(':checked') ) {
			pay = $("<?php echo '#'.$fun->Enlink('vabca'); ?>").val();
			method = "vabca";
			vabca = 1;

			if( $("<?php echo '#'.$fun->Enlink('skypoint'); ?>").is(':checked') ) {
				skypoint = 1;
				total = parseInt(amount) - (parseInt(point) * 1000);
				if(total < 0) {
					total = 0;
				}

				ftotal = new Number(total.toFixed(0)).toLocaleString("id-ID");
				payment = "skypoint-vabca";
				msg_payment = "Skypoint and BCA Virtual Account";
				msg_payment_amount = "<?php echo $fun->FormatNumber($dmcp['points']).' Points and Rp. '; ?>" + ftotal;
			}
			else {
				skypoint = 0;
				total = parseInt(amount);
				ftotal = new Number(total.toFixed(0)).toLocaleString("id-ID");

				payment = "vabca";
				msg_payment = "BCA Virtual Account";
				msg_payment_amount = "Rp. " + ftotal;
			}
		}
		else {
			pay = "";
			method = "";
			vabca = 0;

			total = parseInt(amount);
			ftotal = new Number(total.toFixed(0)).toLocaleString("id-ID");

			if( $("<?php echo '#'.$fun->Enlink('skypoint'); ?>").is(':checked') ) {
				payment = "skypoint";
				msg_payment = "Skypoint";
				msg_payment_amount = "<?php echo $fun->FormatNumber($dmcp['points']).' Points'; ?>";
			}
			else {
				payment = "";
				msg_payment = "Nothing";
				msg_payment_amount = "";
			}
		}

		//console.log(payment);
		ftotal = new Number(total.toFixed(0)).toLocaleString("id-ID");
		$("<?php echo '#'.$fun->Enlink('vaamount'); ?>").html("Rp. " + ftotal);
		$("<?php echo '#'.$fun->Enlink('paymentto'); ?>").val(payment);
	});


	// 
	function toConfirmPayment(pay, msg) {
		if( pay == '' || pay == null ) {
				alert('Method Payment can\'t be empty!');
		}
		else {
			$(document).ready(function() {
				//$("<?php echo '#'.$fun->Enlink('paymentto'); ?>").val(method);
				$("<?php echo '#'.$fun->Enlink('confirm-pin-msg'); ?>").html(msg);
				$("<?php echo '#'.$fun->Enlink('toConfirmPIN'); ?>").click();
				$("<?php echo '#'.$fun->Enlink('idttrigger'); ?>").val("<?php echo $fun->Enlink('submit_payment_confirm'); ?>");
			});
				
		}
	}

	function toWarningNotEnoughSkyPoint() {
		$(document).ready(function() {
			$("<?php echo '#'.$fun->Enlink('toWarningSkypoint'); ?>").click();
		});
	}

	$("<?php echo '#'.$fun->Enlink('toconfirm'); ?>").click(function(){
		var msg = "You will make a payment using " + msg_payment + " " + msg_payment_amount;
		var total_purchase = parseInt("<?php echo $total; ?>");
		var skypoint_purchase = parseInt("<?php echo $dmcp['points']; ?>");
		console.log("skypoint_purchase : " + skypoint_purchase);
		console.log("total_purchase : " + total_purchase);
		
		if( $("<?php echo '#'.$fun->Enlink('skypoint'); ?>").is(':checked') ) {
			if( $("<?php echo '#'.$fun->Enlink('vabca'); ?>").is(':checked') ) {
				pay = "skypoint-vabca";
				toConfirmPayment(pay, msg);
			}
			else {
				pay = "skypoint";

				if(skypoint_purchase >=  total_purchase) {
					toConfirmPayment(pay, msg);
				}
				else {
					toWarningNotEnoughSkyPoint();
				}

			}
		}
		else {
			if( $("<?php echo '#'.$fun->Enlink('vabca'); ?>").is(':checked') ) {
				pay = "vabca";
				toConfirmPayment(pay, msg);
			}
			else {
				pay = "";
				toConfirmPayment(pay, msg);
			}
		}

	});
</script>