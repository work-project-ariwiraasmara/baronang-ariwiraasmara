<?php
$sloc = "SELECT a.locationname as locationname, b.nama_prov as nama_prov, c.nama_kabkot as nama_kabkot
		from 21masterlocation as a 
		inner join 11amasterprovince as b 
			on a.locationprovince = b.id_prov
		inner join 11bmastercity as c
			on a.locationcity = c.id_kabkot
		where a.masterlocationid='".$fun->getIDParam('lokasi')."'";
$qloc = mysqli_query($fun->getConnection(), $sloc) or die(mysqli_error($fun->getConnection()));
$dloc = mysqli_fetch_array($qloc);
?>

<div class="animated fadeinup delay-1">
	<div class="page-content">

		<h1 class="title uppercase txt-black" id="title"></h1>
		<h3 class="txt-black"><?php echo 'Location : '.$dloc['locationname']; ?></h3>

		<div class="m-t-30 txt-black">
			<?php
			$s1 = "SELECT * from 22amasterproduct where active='1' and productprice > 0 and locationid='".$fun->getIDParam('lokasi')."' order by productdescription ASC";
			$q1 = mysqli_query($fun->getConnection(), $s1) or die(mysqli_error($fun->getConnection()));
			while($d1 = mysqli_fetch_array($q1)) { ?>
				<a href="<?php echo '#'.$fun->Enlink($d1['masterproductid']); ?>" class="modal-trigger single-news animated fadeinright delay-3 waves-effect waves-light width-100">
					<?php
					echo $d1['additionaldesc'].'-'.$d1['productdescription'];
					?>
				</a>
				<?php
			}
			?>
		</div>

		<div class="floating-button page-fab animated bouncein delay-3" style="z-index: 1;">
			<a class="btn-floating btn-large waves-effect waves-light primary-color btn z-depth-1" href="<?php echo '?pg=product&sb=add&'.$fun->setIDParam('lokasi', $fun->getIDParam('lokasi')); ?>">
				<i class="ion-android-add"></i>
			</a>
		</div>

	</div>
</div>

<script type="text/javascript">
	$('#toBack').click(function() {
		window.location.href = "?pg=location";
	});
</script>

<?php
$s1 = "SELECT 	a.masterproductid as masterproductid,
				a.productdescription as productdescription, 
				a.vehicletype as vehicletype,
				a.validity_days as validity_days,
				a.productprice as productprice,
				a.unit as unit,
				b.locationaddress as locationaddress,
				c.nama_kabkot as nama_kabkot,
				d.nama_prov as nama_prov
		from 22amasterproduct as a 
		inner join 21masterlocation as b 
			on a.locationid = b.masterlocationid 
		inner join 11amasterprovince as d 
			on b.locationprovince = d.id_prov  
		inner join 11bmastercity as c 
			on b.locationcity = c.id_kabkot
		where a.active='1' and a.locationid='".$fun->getIDParam('lokasi')."'";
$q1 = mysqli_query($fun->getConnection(), $s1) or die(mysqli_error($fun->getConnection()));
while($d1 = mysqli_fetch_array($q1)) { ?>
	<div class="modal borad-10" id="<?php echo $fun->Enlink($d1['masterproductid']); ?>">
		<div class="modal-content choose-date txt-black">
			<p><h1 class="txt-black bold"><?php echo $d1['productdescription']; ?></h1></p>
			<p><span class="bold">Type : </span><?php echo $d1['vehicletype']; ?></p>
			<p><span class="bold">Quantity : </span><?php echo $fun->FormatNumber($d1['validity_days']).'&nbsp;'.$d1['unit']; ?></p>
			<p><span class="bold">Price : Rp. </span><?php echo $fun->FormatRupiah($d1['productprice']); ?></p>
			<p class="txt-white"><a href="<?php echo '?pg=product&sb=edit&'.$fun->setIDParam('ID', $d1['masterproductid']).'&'.$fun->setIDParam('lokasi', $fun->getIDParam('lokasi')); ?>" class="btn btn-large waves-light waves-light width-100 primary-color borad-20"><i class="ion-android-create"></i></a></p>
		</div>
	</div>
	<?php
}
?>
