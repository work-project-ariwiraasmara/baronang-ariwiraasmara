<div class="animated fadeinup delay-1">
	<div class="page-content">

		<h1 class="title uppercase txt-black" id="title"></h1>

		<form action="process/42aaddproduct.php" method="post">

			<div class="input-field">
				<?php
				$s1 = "SELECT locationname from 21masterlocation where masterlocationid='".$fun->getIDParam('lokasi')."'";
				$q1 = mysqli_query($fun->getConnection(), $s1) or die(mysqli_error($fun->getConnection()));
				$d1 = mysqli_fetch_array($q1);
				$fun->inputField('text', 'lokasi', 'lokasi', '', 'validate hide', 'txt-black', $fun->getIDParam('lokasi'), 'required');
				?>
				<h3 class="bold txt-black"><?php echo 'Location : '.$d1['locationname']; ?></h3>
			</div>

			<div class="m-t-10">
				<span>Product Category</span> : <br>
				<select class="browser-default" name="<?php echo $fun->Enlink('catprod'); ?>" id="<?php echo $fun->Enlink('catprod'); ?>" required>
					<?php
					$scatprod = "SELECT * from 22dproductcategory where categorytype = '100'";
					$qcatprod = mysqli_query($fun->getConnection(), $scatprod) or die(mysqli_error($fun->getConnection()));
					while($dcatprod = mysqli_fetch_array($qcatprod)) { ?>
						<option value="<?php echo $fun->Enval($dcatprod['productcategoryid']) ?>"><?php echo $dcatprod['productcategoryname']; ?></option>
						<?php
					}
					/*
					$arrtipe = array('Membership for Motorcycle', 'Membership for Small Car', 'Membership for Big Car');
					$fun->selectField('tipe', 'tipe', 'Product Category', 'browser-default', 'txt-black', $arrtipe, $arrtipe, '', 'required');
					*/
					?>
				</select>
			</div>
			
			<div class="m-t-30">
				<span>Product Type</span> : <br>
				<select class="browser-default" name="<?php echo $fun->Enlink('tiprod'); ?>" id="<?php echo $fun->Enlink('tiprod'); ?>" required>
					<option value="">Choose category first..</option>
					<?php
					/*
					$stiprod = "SELECT * from 22eproducttype";
					$qtiprod = mysqli_query($fun->getConnection(), $stiprod) or die(mysqli_error($fun->getConnection()));
					while($dtiprod = mysqli_fetch_array($qtiprod)) { ?>
						<option value="<?php echo $fun->Enval($dtiprod['producttypeid']) ?>"><?php echo $dtiprod['producttypename']; ?></option>
						<?php
					}
					<option value="<?php echo $fun->Enval('1'); ?>">Motorcycle</option>
					<option value="<?php echo $fun->Enval('2'); ?>">Small Car</option>
					<option value="<?php echo $fun->Enval('3'); ?>">Big Car</option>
					*/
					?>
				</select>
			</div>

			<div class="row">
				<div class="col s6 input-field m-t-50">
					<?php
					$fun->inputField('number', 'qty', 'qty', 'Quantity', 'validate', 'txt-black');
					?>
				</div>

				<div class="col s6 m-t-30">
					<span>Unit</span> : <br>
					<select class="browser-default" name="<?php echo $fun->Enlink('prodit'); ?>" id="<?php echo $fun->Enlink('prodit'); ?>" required>
						<option value="">Choose category first..</option>
					</select>
				</div>
			</div>
			
			
			
			

			<div class="input-field m-t-30">
				<?php
				$fun->inputField('number', 'harga', 'harga', 'Price', 'validate', 'txt-black');
				?>
			</div>
			
			<div class="input-field m-t-30">
				<?php
				$fun->inputField('number', 'quota', 'quota', 'Quota', 'validate', 'txt-black');
				?>
			</div>
			
			<div class="input-field m-t-30">
				<?php
				$fun->inputField('text', 'deskripsi', 'deskripsi', 'Additional Description', 'validate', 'txt-black', null, 'required');
				?>
			</div>

			<?php /*
			<div class="input-field m-t-30" id="<?php echo $fun->Enlink('harian'); ?>">
				<?php
				$fun->inputField('number', 'valid', 'valid', 'Quantity', 'validate', 'txt-black', null, 'required');
				?>
			</div>
			
			<div class="row m-t-30 hide" id="<?php echo $fun->Enlink('bulanan'); ?>">
				<div class="col s6">
					<span>Year</span>: <br>
					<select class="browser-default" name="<?php echo $fun->Enlink('bulanan_tahun') ?>" id="<?php echo $fun->Enlink('bulanan_tahun') ?>">
						<?php
						for($x = date('Y'); $x < 2026; $x++) { ?>
							<option value="<?php echo $fun->Enval($x); ?>"><?php echo $x; ?></option>
							<?php
						}
						?>
					</select>
				</div>

				<div class="col s6">
					<span>Month</span>: <br>
					<select class="browser-default" name="<?php echo $fun->Enlink('bulanan_bulan') ?>" id="<?php echo $fun->Enlink('bulanan_bulan') ?>">
						<?php
						for($x = 1; $x < 13; $x++) { ?>
							<option value="<?php echo $fun->Enval($x); ?>"><?php echo date('F', strtotime(date('Y').'-'.$x.'-1')); ?></option>
							<?php
						}
						?>
					</select>
				</div>
			</div>
			*/ ?>

			<?php
			$fun->inputField('hidden', 'process', 'process', '', 'validate', 'txt-black', 'product');
			$fun->buttonField('submit', 'confirm', 'confirm', 'btn btn-large width-100 waves-effect waves-light primary-color m-t-30 borad-20', 'Save');
			?>

		</form>

	</div>
</div>

<script type="text/javascript">
	$('#toBack').click(function() {
		window.location.href = "<?php echo '?pg=product&'.$fun->setIDParam('lokasi', $fun->getIDParam('lokasi')); ?>";
	});

	$("<?php echo '#'.$fun->Enlink('catprod'); ?>").click(function(){
		var countunit = $("<?php echo '#'.$fun->Enlink('prodit'); ?> option").length;
		var category = $("<?php echo '#'.$fun->Enlink('catprod'); ?> :selected").val();
		//alert(countunit);

		$.ajax({
			url : "process/ajax/ajax_category_type.php",
			type : 'POST',
			data: { 
			    id: category
			},
			success : function(data) {
				$("<?php echo '#'.$fun->Enlink('tiprod'); ?>").html(data);
			},
			error : function(){
				alert('Terjadi kesalahan dalam jaringan!\nGagal Link Kartu!\nSilahkan cek koneksi jaringan dan coba lagi!');
			    return false;
			}
		});

		$.ajax({
			url : "process/ajax/ajax_category_unit.php",
			type : 'POST',
			data: { 
			    id: category
			},
			success : function(data) {
				$("<?php echo '#'.$fun->Enlink('prodit'); ?>").html(data);
			},
			error : function(){
				alert('Terjadi kesalahan dalam jaringan!\nGagal Link Kartu!\nSilahkan cek koneksi jaringan dan coba lagi!');
			    return false;
			}
		});


	});
</script>
