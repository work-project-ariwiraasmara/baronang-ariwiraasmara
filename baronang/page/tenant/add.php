<?php
$scmpy = "SELECT * from 12mastercompany where companyid='71'";
$qcmpy = mysqli_query($fun->getConnection(), $scmpy) or die(mysqli_error($fun->getConnection()));
$dcmpy = mysqli_fetch_array($qcmpy);
?>

<div class="animated fadeinup delay-1">
	<div class="page-content">

		<h1 class="title uppercase txt-black" id="title"></h1>

		<form action="process/43aaddmember.php" method="post">

			<div class="input-field">
				<?php
				$s1 = "SELECT locationname from 21masterlocation where masterlocationid='".$fun->getIDParam('lokasi')."'";
				$q1 = mysqli_query($fun->getConnection(), $s1) or die(mysqli_error($fun->getConnection()));
				$d1 = mysqli_fetch_array($q1);
				$fun->inputField('text', 'lokasi', 'lokasi', '', 'validate hide', 'txt-black', $fun->getIDParam('lokasi'), 'required');
				?>
				<h3 class="bold txt-black"><?php echo 'Location : '.$d1['locationname']; ?></h3>
			</div>

			<div class="input-field">
				<?php
				$fun->inputField('text', 'nama', 'nama', 'Name', 'validate', 'txt-black', null, 'required');
				?>
			</div>

			<?php
			if( ($dcmpy['labeltier1'] != '') || !empty($dcmpy['labeltier1']) ) { ?>
				<div class="input-field">
					<?php
					$fun->inputField('text', 'tier1', 'tier1', $dcmpy['labeltier1'], 'validate', 'txt-black', null, 'required');
					?>
				</div>
				<?php
			}

			if( ($dcmpy['labeltier2'] != '') || !empty($dcmpy['labeltier2']) ) { ?>
				<div class="input-field">
					<?php
					$fun->inputField('text', 'tier2', 'tier2', $dcmpy['labeltier2'], 'validate', 'txt-black', null, 'required');
					?>
				</div>
				<?php
			}

			if( ($dcmpy['labeltier3'] != '') || !empty($dcmpy['labeltier3']) ) { ?>
				<div class="input-field">
					<?php
					$fun->inputField('text', 'tier3', 'tier3', $dcmpy['labeltier3'], 'validate', 'txt-black', null, 'required');
					?>
				</div>
				<?php
			}

			if( ($dcmpy['labeltier4'] != '') || !empty($dcmpy['labeltier4']) ) { ?>
				<div class="input-field">
					<?php
					$fun->inputField('text', 'tier4', 'tier4', $dcmpy['labeltier4'], 'validate', 'txt-black', null, 'required');
					?>
				</div>
				<?php
			}

			if( ($dcmpy['labeltier5'] != '') || !empty($dcmpy['labeltier5']) ) { ?>
				<div class="input-field">
					<?php
					$fun->inputField('text', 'tier5', 'tier5', $dcmpy['labeltier5'], 'validate', 'txt-black', null, 'required');
					?>
				</div>
				<?php
			}
			?>

			<div class="input-field">
				<?php
				$fun->inputField('number', 'tlp', 'tlp', 'Phone Number', 'validate', 'txt-black');
				?>
			</div>

			<?php /*
			<div class="col s6 m-t-10">
				<?php
				$fun->inputField('number', 'qtymotor', 'qtymotor', 'Jumlah Motor', 'validate', 'txt-black');
				?>
			</div>

			<div class="col s6 m-t-10">
				<?php
				$fun->inputField('number', 'qtymobil1', 'qtymobil1', 'Jumlah Mobil Kecil', 'validate', 'txt-black');
				?>
			</div>

			<div class="col s6 m-t-10">
				<?php
				$fun->inputField('number', 'qtymobil2', 'qtymobil2', 'Jumlah Mobil Besar', 'validate', 'txt-black');
				?>
			</div>
			*/ ?>

			<?php
			$fun->buttonField('submit', 'confirm', 'confirm', 'btn btn-large width-100 waves-effect waves-light primary-color m-t-30 borad-20', 'Save');
			?>

		</form>

	</div>
</div>

<script type="text/javascript">
	$('#toBack').click(function() {
		window.location.href = "<?php echo '?pg=tenant&'.$fun->setIDParam('lokasi', $fun->getIDParam('lokasi')); ?>";
	});
</script>