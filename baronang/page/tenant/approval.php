<style type="text/css">
	#sum_motor, #sum_mobil1, #sum_mobil2 {
		border: none !important;
		text-align: right;
	}
	#sum_check {
		border: none !important;
	}

	/* Chrome, Safari, Edge, Opera */
	input::-webkit-outer-spin-button,
	input::-webkit-inner-spin-button {
	  -webkit-appearance: none;
	  margin: 0;
	}

	/* Firefox */
	input[type=number] {
	  -moz-appearance: textfield;
	}
</style>

<script type="text/javascript">
	var jml_check = 0;
	var jml_motor = 0;
	var jml_mobil1 = 0;
	var jml_mobil2 = 0;
	// toLocaleString("id-ID");
</script>

<div class="animated fadeinup delay-1">
	<div class="page-content">

		<h1 class="title txt-black" id="title"></h1>

		<?php
		if( isset($_GET[$fun->Enlink('lokasi')]) ) {
			$link_form = 'process/43dapprovalmember.php?'.$fun->setIDParam('lokasi', $fun->getIDParam('lokasi'));
		
			$sloc = "SELECT locationname from 21masterlocation where masterlocationid='".$fun->getIDParam('lokasi')."'";
			$qloc = mysqli_query($fun->getConnection(), $sloc) or die(mysqli_error($fun->getConnection()));
			$dloc = mysqli_fetch_array($qloc);

			echo '<h3 class="title txt-black">'.$dloc['locationname'].'</h3>';

		}
		else {
			$link_form = 'process/43eapprovalmembergrup.php';
		}
		?>

		<form action="<?php echo $link_form; ?>" method="post">
			<div class="table-responsive m-t-50">
			
				<table class="table">
					<thead>
						<tr>
							<th class="center">
							<?php
							if( isset($_GET[$fun->Enlink('lokasi')]) ) echo 'Name';
							else echo 'Location';
							?>
							</th>

							<th class="center">Number of Motorcycles</th>
							<th class="center">Number of Small Cars</th>
							<th class="center">Number of Big Cars</th>

							<?php
							if( !isset($_GET[$fun->Enlink('lokasi')]) ) { ?>
								<th class="center">Number of Tenant</th>
								<?php
							}
							?>

							<th class="center">
								<input type="checkbox" id="<?php echo $fun->Enlink('tenant-all'); ?>"  /> 
		            			<label for="<?php echo $fun->Enlink('tenant-all'); ?>" class="txt-black"></label>
							</th>
						</tr>
					</thead>

					<tbody id="tbl_content">
					<?php
					$no = 1;
					if( isset($_GET[$fun->Enlink('lokasi')]) ) {
						$lokasi = $fun->GET('lokasi', 2);

						$jml_motor = 0; $jml_mobil1 = 0; $jml_mobil2 = 0;

						// COMPANY
						$scmpy = "SELECT * from 12mastercompany where companyid='71'";
						$qcmpy = mysqli_query($fun->getConnection(), $scmpy) or die(mysqli_error($fun->getConnection()));
						$dcmpy = mysqli_fetch_array($qcmpy);

						$s1 = "SELECT 	b.masterlocationid as masterlocationid, 
										b.locationname as locationname,
										a.mastertenantid as mastertenantid,
										a.locationid as locationid,
										a.tenantname as tenantname,
										a.allowancemotorcycle as allowancemotorcycle,
										a.allowancecarsmall as allowancecarsmall,
										a.allowancecarbig as allowancecarbig,
										a.tier1 as tier1, a.tier2 as tier2, a.tier3 as tier3,
										a.tier4 as tier4, a.tier5 as tier5
								from 23dmastertenant as a 
								inner join 21masterlocation as b 
									on a.locationid = b.masterlocationid
								where a.status = '3' and b.masterlocationid='$lokasi'";
						$q1 = mysqli_query($fun->getConnection(), $s1) or die(mysqli_error($fun->getConnection()));
						while($d1 = mysqli_fetch_array($q1)) { 

							$passa = $fun->randNumber(2);
							$passb = $fun->randNumber(2);
							$password = $passa.$passb.$passa.$passb;

							$sprodmotor = "SELECT masterproductid, validity_days from 22amasterproduct where locationid='".$d1['masterlocationid']."' and vehicletype='Motorcycle' and productprice='0'";
							$qprodmotor = mysqli_query($fun->getConnection(), $sprodmotor) or die(mysqli_error($fun->getConnection()));
							$dprodmotor = mysqli_fetch_array($qprodmotor);
							
							$sprodmobil1 = "SELECT masterproductid, validity_days from 22amasterproduct where locationid='".$d1['masterlocationid']."' and vehicletype='Small Car' and productprice='0'";
							$qprodmobil1 = mysqli_query($fun->getConnection(), $sprodmobil1) or die(mysqli_error($fun->getConnection()));
							$dprodmobil1 = mysqli_fetch_array($qprodmobil1);

							$sprodmobil2 = "SELECT masterproductid, validity_days from 22amasterproduct where locationid='".$d1['masterlocationid']."' and vehicletype='Big Car' and productprice='0'";
							$qprodmobil2 = mysqli_query($fun->getConnection(), $sprodmobil2) or die(mysqli_error($fun->getConnection()));
							$dprodmobil2 = mysqli_fetch_array($qprodmobil2);

							?>
							<tr>
								<?php 
								if( ($d1['tier1'] == '') || empty($d1['tier1']) ) {
									$f3tier1 = '';
								}
								else {
									$f1tier1 = str_replace(' ', '', $d1['tier1']);
									$f2tier1 = str_replace('.', '', $f1tier1);
									$f3tier1 = str_replace(',', '', $f2tier1);
								}


								if( ($d1['tier2'] == '') || empty($d1['tier2']) ) {
									$f3tier2 = '';
								}
								else {
									$f1tier2 = str_replace(' ', '', $d1['tier2']);
									$f2tier2 = str_replace('.', '', $f1tier2);
									$f3tier2 = str_replace(',', '', $f2tier2);
								}


								if( ($d1['tier3'] == '') || empty($d1['tier3']) ) {
									$f3tier3 = '';
								}
								else {
									$f1tier3 = str_replace(' ', '', $d1['tier3']);
									$f2tier3 = str_replace('.', '', $f1tier3);
									$f3tier3 = str_replace(',', '', $f2tier3);
								}


								if( ($d1['tier4'] == '') || empty($d1['tier4']) ) {
									$f3tier4 = '';
								}
								else {
									$f1tier4 = str_replace(' ', '', $d1['tier4']);
									$f2tier4 = str_replace('.', '', $f1tier4);
									$f3tier4 = str_replace(',', '', $f2tier4);
								}


								if( ($d1['tier5'] == '') || empty($d1['tier5']) ) {
									$f3tier5 = '';
								}
								else {
									$f1tier5 = str_replace(' ', '', $d1['tier5']);
									$f2tier5 = str_replace('.', '', $f1tier5);
									$f3tier5 = str_replace(',', '', $f2tier5);
								}

								$email = strtolower($f3tier1).strtolower($f3tier2).strtolower($f3tier3).strtolower($f3tier4).strtolower($f3tier5).'@'.strtolower(str_replace(' ', '', $d1['locationname'])).'.'.strtolower(str_replace(' ', '', $dcmpy['companyname'])).'.baronang.com';
								?>
								<td>
									<?php echo $d1['tenantname']; ?>
									<input type="hidden" name="<?php echo $fun->Enlink('tid').'['.$no.']';   ?>" id="<?php echo $fun->Enlink('tid').'-'.$no;   ?>" value="<?php echo $fun->Enval($d1['mastertenantid']); ?>">
									<input type="hidden" name="<?php echo $fun->Enlink('email').'['.$no.']'; ?>" id="<?php echo $fun->Enlink('email').'-'.$no; ?>" value="<?php echo $fun->Enval($email); ?>">
									<input type="hidden" name="<?php echo $fun->Enlink('pass').'['.$no.']';  ?>" id="<?php echo $fun->Enlink('pass').'-'.$no;  ?>" value="<?php echo $fun->Enval($password); ?>">
								</td>

								<td>
									<input type="number" class="validate right txt_motor" name="<?php echo $fun->Enlink('qtymotor').'['.$no.']'; ?>" id="<?php echo $fun->Enlink('qtymotor').'-'.$no; ?>" value="<?php echo $d1['allowancemotorcycle']; ?>" readonly>
								</td>

								<td>
									<input type="number" class="validate right txt_mobil1" name="<?php echo $fun->Enlink('qtymobil1').'['.$no.']'; ?>" id="<?php echo $fun->Enlink('qtymobil1').'-'.$no; ?>" value="<?php echo $d1['allowancecarsmall']; ?>" readonly>
								</td>

								<td>
									<input type="number" class="validate right txt_mobil2" name="<?php echo $fun->Enlink('qtymobil2').'['.$no.']'; ?>" id="<?php echo $fun->Enlink('qtymobil2').'-'.$no; ?>" value="<?php echo $d1['allowancecarbig']; ?>" readonly>
								</td>

								<th class="center">
									<input type="checkbox" name="<?php echo $fun->Enlink('tenant_approval').'['.$no.']'; ?>" id="<?php echo $fun->Enlink('tenant_approval').'-'.$no; ?>" value="<?php echo $fun->Enval('4'); ?>"  /> 
		            				<label for="<?php echo $fun->Enlink('tenant_approval').'-'.$no; ?>" class="txt-black"></label>
								</th>
							</tr>

							<script type="text/javascript">
								$(document).ready(function() {
								    //this calculates values automatically 
								    //calculateSum();
								    sumMotor();
								    sumMobil1();
								    sumMobil2();

								    $("<?php echo '#'.$fun->Enlink('qtymotor').'-'.$no; ?>").on("keydown keyup", function() {
										sumMotor();
									});

									$("<?php echo '#'.$fun->Enlink('qtymobil1').'-'.$no; ?>").on("keydown keyup", function() {
										sumMobil1();
									});

									$("<?php echo '#'.$fun->Enlink('qtymobil2').'-'.$no; ?>").on("keydown keyup", function() {
										sumMobil2();
									});
								});

								$("<?php echo '#'.$fun->Enlink('tenant_approval').'-'.$no; ?>").click(function() {
									if( $("<?php echo '#'.$fun->Enlink('tenant_approval').'-'.$no; ?>").is(':checked') ) {
										$("<?php echo '#'.$fun->Enlink('tenant_approval').'-'.$no; ?>").val("<?php echo $fun->Enval('2'); ?>");
										jml_check = jml_check + 1;
									}
									else {
										$("<?php echo '#'.$fun->Enlink('tenant_approval').'-'.$no; ?>").val("<?php echo $fun->Enval('4'); ?>");
										jml_check = jml_check - 1;
									}
									$("#sum_check").val(jml_check);
								});
							</script>
							<?php
							$no++; 
							$jml_motor  = $jml_motor + $d1['allowancemotorcycle'];
							$jml_mobil1 = $jml_mobil1 + $d1['allowancecarsmall'];
							$jml_mobil2 = $jml_mobil2 + $d1['allowancecarbig'];
							$password++;
						}
						?>

						<tr>
							<th colspan=""></th>
							<th><input type="text" name="input" id="sum_motor"  value="<?php echo $jml_motor; ?>" readonly /></th>
							<th><input type="text" name="input" id="sum_mobil1" value="<?php echo $jml_mobil1; ?>" readonly /></th>
							<th><input type="text" name="input" id="sum_mobil2" value="<?php echo $jml_mobil2; ?>" readonly /></th>
							<th><input type="text" name="input" id="sum_check"  value="0" class="center" readonly /></th>
						</tr>
						<?php
					}
					else {

						$s1 = "SELECT distinct 	a.masterlocationid as masterlocationid, 
												a.locationname as locationname,
												(SELECT Count(mastertenantid)    from 23dmastertenant where status='3' and locationid = masterlocationid) as tenant,
												(SELECT SUM(allowancemotorcycle) from 23dmastertenant where status='3' and locationid = masterlocationid) as allowancemotorcycle,
												(SELECT SUM(allowancecarsmall)   from 23dmastertenant where status='3' and locationid = masterlocationid) as allowancecarsmall,
												(SELECT SUM(allowancecarbig)     from 23dmastertenant where status='3' and locationid = masterlocationid) as allowancecarbig
								from 21masterlocation as a
								inner join 23dmastertenant as b 
									on a.masterlocationid = b.locationid
								where b.status = '3'
								order by tenant DESC";
						$q1 = mysqli_query($fun->getConnection(), $s1) or die(mysqli_error($fun->getConnection()));
						while($d1 = mysqli_fetch_array($q1)) { 

							if($d1['tenant'] > 0) { ?> 
								<tr>
									<td class="txt-link">
										<input type="hidden" name="<?php echo $fun->Enlink('lokasi').'['.$no.']'; ?>" id="<?php echo $fun->Enlink('lokasi'); ?>" value="<?php echo $fun->Enval($d1['masterlocationid']); ?>">
										
										<a href="<?php echo '?pg=tenant&sb=approval&'.$fun->setIDParam('lokasi', $d1['masterlocationid']); ?>">
											<span style=""><?php echo $d1['locationname']; ?></span>
										</a>
									</td>

									<td style="text-align: right;">
										<?php echo $fun->FormatNumber($d1['allowancemotorcycle']); ?>
									</td>
									
									<td style="text-align: right;">
										<?php echo $fun->FormatNumber($d1['allowancecarsmall']); ?>
									</td>
									
									<td style="text-align: right;">
										<?php echo $fun->FormatNumber($d1['allowancecarbig']); ?>
									</td>
									
									<td style="text-align: right;">
										<?php echo $fun->FormatNumber($d1['tenant']); ?>
									</td>
								
									<th class="center">
										<input type="checkbox" name="<?php echo $fun->Enlink('tenant_approval').'['.$no.']'; ?>" id="<?php echo $fun->Enlink('tenant_approval').'-'.$no; ?>" value="<?php echo $fun->Enval('4'); ?>"  /> 
			            				<label for="<?php echo $fun->Enlink('tenant_approval').'-'.$no; ?>" class="txt-black"></label>
									</th>
								</tr>

								<script type="text/javascript">
									$("<?php echo '#'.$fun->Enlink('tenant_approval').'-'.$no; ?>").click(function() {
										if( $("<?php echo '#'.$fun->Enlink('tenant_approval').'-'.$no; ?>").is(':checked') ) {
											$("<?php echo '#'.$fun->Enlink('tenant_approval').'-'.$no; ?>").val("<?php echo $fun->Enval('2'); ?>");
											jml_check = jml_check + 1;
										}
										else {
											$("<?php echo '#'.$fun->Enlink('tenant_approval').'-'.$no; ?>").val("<?php echo $fun->Enval('4'); ?>");
											jml_check = jml_check - 1;
										}
										$("#sum_check").val(jml_check);
									});
								</script>
								<?php
							}
							$no++;
						}

					}
					?>
					</tbody>

					<tfoot>
						<tr>
							<th <?php if( isset($_GET[$fun->Enlink('lokasi')]) ) { echo 'colspan="5"'; } else { echo 'colspan="6"'; } ?> >
								<?php
								$fun->inputField('hidden', 'jml_no', 'jml_no', null, 'hide', 'txt-black', $no); 
								?>
								<button type="submit" name="confirm" id="confirm" class="btn btn-large width-100 waves-effect waves-light primary-color m-t-10 borad-20">Approve</button>
							</th>
						</tr>
					</tfoot>

				</table>

			</div>

		</form>

	</div>
</div>

<script type="text/javascript">
	$('#toBack').click(function() {
		window.location.href = "<?php echo '?#'.$fun->Enlink('skymanagement'); ?>";
	});

	function sumMotor() {
	    var sum = 0;
	    //iterate through each textboxes and add the values
	    $(".txt_motor").each(function() {
	        //add only if the value is number
	        if (!isNaN(this.value) && this.value.length != 0) {
	            sum += parseFloat(this.value);
	            //$(this).css("background-color", "#FEFFB0");
	        }
	        else if (this.value.length != 0){
	            //$(this).css("background-color", "red");
	        }
	    });
	 
	 	var finsum = new Number(sum.toFixed(0)).toLocaleString("id-ID");
		$("input#sum_motor").val(finsum);
	}

	function sumMobil1() {
	    var sum = 0;
	    //iterate through each textboxes and add the values
	    $(".txt_mobil1").each(function() {
	        //add only if the value is number
	        if (!isNaN(this.value) && this.value.length != 0) {
	            sum += parseFloat(this.value);
	            //$(this).css("background-color", "#FEFFB0");
	        }
	        else if (this.value.length != 0){
	            //$(this).css("background-color", "red");
	        }
	    });
	 
	 	var finsum = new Number(sum.toFixed(0)).toLocaleString("id-ID");
		$("input#sum_mobil1").val(finsum);
	}

	function sumMobil2() {
	    var sum = 0;
	    //iterate through each textboxes and add the values
	    $(".txt_mobil2").each(function() {
	        //add only if the value is number
	        if (!isNaN(this.value) && this.value.length != 0) {
	            sum += parseFloat(this.value);
	            //$(this).css("background-color", "#FEFFB0");
	        }
	        else if (this.value.length != 0){
	            //$(this).css("background-color", "red");
	        }
	    });
	 
	    var finsum = new Number(sum.toFixed(0)).toLocaleString("id-ID");
		$("input#sum_mobil2").val(finsum);
	}

	$("<?php echo '#'.$fun->Enlink('tenant-all'); ?>").click(function(){

		$("input:checkbox").not(this).prop("checked", this.checked);
		if( $("<?php echo '#'.$fun->Enlink('tenant-all'); ?>").is(':checked') ) {
			$("input:checkbox").val("<?php echo $fun->Enval('2'); ?>");
		}
		else {
			$("input:checkbox").val("<?php echo $fun->Enval('4'); ?>");
		}

		if( $("<?php echo '#'.$fun->Enlink('tenant-all'); ?>").is(':checked') ) {
			jml_check = "<?php echo $no-1; ?>";
			$("#sum_check").val("<?php echo $no-1; ?>");
		}
		else {
			jml_check = 0;
			$("#sum_check").val("0");
		}

	});
</script>
