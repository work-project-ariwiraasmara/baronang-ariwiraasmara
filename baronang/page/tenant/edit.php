<?php
$scmpy = "SELECT * from 12mastercompany where companyid='71'";
$qcmpy = mysqli_query($fun->getConnection(), $scmpy) or die(mysqli_error($fun->getConnection()));
$dcmpy = mysqli_fetch_array($qcmpy);

$sf1 = "SELECT * from 23dmastertenant where mastertenantid='".$fun->getIDParam('ID')."'";
$qf1 = mysqli_query($fun->getConnection(), $sf1) or die(mysqli_error($fun->getConnection()));
$df1 = mysqli_fetch_array($qf1);
?>

<div class="animated fadeinup delay-1">
	<div class="page-content">

		<h1 class="title uppercase txt-black" id="title"></h1>

		<form action="<?php echo 'process/43aeditmember.php&'.$fun->setIDParam('ID', $fun->getIDParam('ID')).'&'.$fun->setIDParam('lokasi', $fun->getIDParam('lokasi')); ?>" method="post">

			<div class="input-field">
				<?php
				$s1 = "SELECT locationname from 21masterlocation where masterlocationid='".$fun->getIDParam('lokasi')."'";
				$q1 = mysqli_query($fun->getConnection(), $s1) or die(mysqli_error($fun->getConnection()));
				$d1 = mysqli_fetch_array($q1);
				?>
				<h3 class="bold txt-black"><?php echo 'Lokasi : '.$d1['locationname']; ?></h3>
				<span class="m-t-30"><?php echo $df1['tenantname']; ?></span>
			</div>

			<div class="input-field m-t-50">
				<?php
				$fun->inputField('number', 'tlp', 'tlp', 'Phone Number', 'validate', 'txt-black', $df1['tenantphone']);
				?>
			</div>

			<div class="row">
				<div class="col s12 input-field">
					<?php
					$fun->inputField('text', 'alamat', 'alamat', 'Address', 'validate', 'txt-black', null, 'required');
					?>
				</div>

				<div class="col s6">
					<span class="bold">Province</span> : <br>
					<select class="browser-default txt-black" name="<?php echo $fun->Enlink('prov'); ?>" id="<?php echo $fun->Enlink('prov'); ?>" required>
						<?php
						$s3 = "SELECT * from 11amasterprovince order by id_prov";
						$q3 = mysqli_query($fun->getConnection(), $s3) or die(mysqli_error($fun->getConnection()));
						while($d3 = mysqli_fetch_array($q3)) { ?>
							<option value="<?php echo $fun->Enval($d3['id_prov']); ?>"><?php echo $d3['nama_prov']; ?></option>
							<?php
						}
						?>
					</select>
				</div>

				<div class="col s6">
					<span class="bold">Area</span> : <br>
					<select class="browser-default txt-black" name="<?php echo $fun->Enlink('kabkot'); ?>" id="<?php echo $fun->Enlink('kabkot'); ?>" required>
						<option value="">Choose Province..</option>
					</select>
				</div>
			</div>

			<?php
			$fun->buttonField('submit', 'confirm', 'confirm', 'btn btn-large width-100 waves-effect waves-light primary-color m-t-30 borad-20', 'Update');
			?>

		</form>

	</div>
</div>

<script type="text/javascript">
	$('#toBack').click(function() {
		window.location.href = "<?php echo '?pg=tenant&'.$fun->setIDParam('lokasi', $fun->getIDParam('lokasi')); ?>";
	});
</script>