<?php
if( isset($_GET[$fun->Enlink('lokasi')]) ) {

	$s1 = "SELECT * from 12mastercompany where companyid='71'";
	$q1 = mysqli_query($fun->getConnection(), $s1) or die(mysqli_error($fun->getConnection()));
	$d1 = mysqli_fetch_array($q1);

	$s2 = "SELECT * from 21masterlocation where masterlocationid='".$fun->getIDParam('lokasi')."'";
	$q2 = mysqli_query($fun->getConnection(), $s2) or die(mysqli_error($fun->getConnection()));
	$d2 = mysqli_fetch_array($q2);

	// HEADER { 
		$objPHPExcel->getActiveSheet()
                        ->getStyle('A')
                        ->getNumberFormat()
                        ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);


        $objPHPExcel->getActiveSheet()
                        ->getStyle('B')
                        ->getNumberFormat()
                        ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

        $objPHPExcel->getActiveSheet()
                        ->getStyle('C')
                        ->getNumberFormat()
                        ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

        $objPHPExcel->getActiveSheet()
                        ->getStyle('D')
                        ->getNumberFormat()
                        ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

        $objPHPExcel->getActiveSheet()
                        ->getStyle('E')
                        ->getNumberFormat()
                        ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

        $objPHPExcel->getActiveSheet()
                        ->getStyle('F')
                        ->getNumberFormat()
                        ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);


        $objPHPExcel->getActiveSheet()
                        ->getStyle('G')
                        ->getNumberFormat()
                        ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

        $objPHPExcel->getActiveSheet()
                        ->getStyle('H')
                        ->getNumberFormat()
                        ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

        $objPHPExcel->getActiveSheet()
                        ->getStyle('I')
                        ->getNumberFormat()
                        ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

        $objPHPExcel->getActiveSheet()
                        ->getStyle('J')
                        ->getNumberFormat()
                        ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

		$objPHPExcel->getActiveSheet()
                        ->getStyle('K')
                        ->getNumberFormat()
                        ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

        // TITLE
        $objWorkSheet->getStyle('A1')->getFont()->setSize(20);
		$objWorkSheet->getStyle('A1')->getFont()->setBold(true);
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:K1');
		$objWorkSheet->SetCellValue('A1', 'Tenant '.$d2['locationname']);
		$objWorkSheet->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);  


        // NOMOR URUT
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(8);
		$objWorkSheet->getStyle('A3')->getFont()->setBold(true);
		$objWorkSheet->SetCellValue('A3', 'No.');
		$objWorkSheet->getStyle('A3')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objWorkSheet->getStyle('A3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);


		// NAMA
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(40);
		$objWorkSheet->getStyle('B3')->getFont()->setBold(true);
		$objWorkSheet->SetCellValue('B3', 'Name');
		$objWorkSheet->getStyle('B3')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objWorkSheet->getStyle('B3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	

		// LABEL TIER 1
		if( $d1['labeltier1'] != '' || !empty($d1['labeltier1']) ) {
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
			$objWorkSheet->getStyle('C3')->getFont()->setBold(true);
			$objWorkSheet->SetCellValue('C3', $d1['labeltier1']);
			$objWorkSheet->getStyle('C3')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objWorkSheet->getStyle('C3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		}
		else {
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(0);
			$objWorkSheet->SetCellValue('C3', '');
		}
		

		// LABEL TIER 2
		if( $d1['labeltier2'] != '' || !empty($d1['labeltier2']) ) {
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
			$objWorkSheet->getStyle('D3')->getFont()->setBold(true);
			$objWorkSheet->SetCellValue('D3', $d1['labeltier2']);
			$objWorkSheet->getStyle('D3')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objWorkSheet->getStyle('D3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		}
		else {
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(0);
			$objWorkSheet->SetCellValue('D3', '');
		}


		// LABEL TIER 3
		if( $d1['labeltier3'] != '' || !empty($d1['labeltier3']) ) {
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
			$objWorkSheet->getStyle('E3')->gEtFont()->setBold(true);
			$objWorkSheet->SetCellValue('E3', $d1['labeltier3']);
			$objWorkSheet->getStyle('E3')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objWorkSheet->getStyle('E3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		}
		else {
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(0);
			$objWorkSheet->SetCellValue('E3', '');
		}


		// LABEL TIER 4
		if( $d1['labeltier4'] != '' || !empty($d1['labeltier4']) ) {
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
			$objWorkSheet->getStyle('F3')->getFont()->setBold(true);
			$objWorkSheet->SetCellValue('F3', $d1['labeltier4']);
			$objWorkSheet->getStyle('F3')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objWorkSheet->getStyle('F3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		}
		else {
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(0);
			$objWorkSheet->SetCellValue('F3', '');
		}


		// LABEL TIER 5
		if( $d1['labeltier5'] != '' || !empty($d1['labeltier5']) ) {
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(30);
			$objWorkSheet->getStyle('G3')->getFont()->setBold(true);
			$objWorkSheet->SetCellValue('G3', $d1['labeltier5']);
			$objWorkSheet->getStyle('G3')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objWorkSheet->getStyle('G3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		}
		else {
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(0);
			$objWorkSheet->SetCellValue('G3', '');
		}


		// NO. TLP
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(17);
		$objWorkSheet->getStyle('H3')->getFont()->setBold(true);
		$objWorkSheet->SetCellValue('H3', 'Phone Number');
		$objWorkSheet->getStyle('H3')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objWorkSheet->getStyle('H3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);


		// JUMLAH MOTOR
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
		$objWorkSheet->getStyle('I3')->getFont()->setBold(true);
		$objWorkSheet->SetCellValue('I3', 'Number of Motorcycles');
		$objWorkSheet->getStyle('I3')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objWorkSheet->getStyle('I3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);


		// JUMLAH MOBIL KECIL
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(18);
		$objWorkSheet->getStyle('J3')->getFont()->setBold(true);
		$objWorkSheet->SetCellValue('J3', 'Number of Small Cars');
		$objWorkSheet->getStyle('J3')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objWorkSheet->getStyle('J3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);


		// JUMLAH MOBIL BESAR
		$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(18);
		$objWorkSheet->getStyle('K3')->getFont()->setBold(true);
		$objWorkSheet->SetCellValue('K3', 'Number of Big Cars');
		$objWorkSheet->getStyle('K3')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objWorkSheet->getStyle('K3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	
		//

	// }


	// CONTENT {
		$no = 1;
		$row = 4;

		$s3 = "SELECT * from 23dmastertenant where locationid='".$fun->getIDParam('lokasi')."' order by mastertenantid";
		$q3 = mysqli_query($fun->getConnection(), $s3) or die(mysqli_error($fun->getConnection()));
		while($d3 = mysqli_fetch_array($q3)) {

			// NOMOR URUT
			$objWorkSheet->SetCellValue('A'.$row, $fun->FormatNumber($no));
            $objWorkSheet->getStyle('A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

            // NAMA
            $objWorkSheet->SetCellValue('B'.$row, $d3['tenantname']);

			// LABEL TIER 1
			if( $d1['labeltier1'] != '' || !empty($d1['labeltier1']) ) {
				$objWorkSheet->SetCellValue('C'.$row, $d3['tier1'].' ');
			}

			// LABEL TIER 2
			if( $d1['labeltier2'] != '' || !empty($d1['labeltier2']) ) {
				$objWorkSheet->SetCellValue('D'.$row, $d3['tier2'].' ');
			}

			// LABEL TIER 3
			if( $d1['labeltier3'] != '' || !empty($d1['labeltier3']) ) {
				$objWorkSheet->SetCellValue('E'.$row, $d3['tier3'].' ');
			}

			// LABEL TIER 4
			if( $d1['labeltier4'] != '' || !empty($d1['labeltier4']) ) {
				$objWorkSheet->SetCellValue('F'.$row, $d3['tier4'].' ');
			}

			// LABEL TIER 5
			if( $d1['labeltier5'] != '' || !empty($d1['labeltier5']) ) {
				$objWorkSheet->SetCellValue('G'.$row, $d3['tier5'].' ');
			}

			// NO. TLP
            $objWorkSheet->SetCellValue('H'.$row, ' '.$d3['tenantphone'].' ');

			// JUMLAH MOTOR
            $objWorkSheet->SetCellValue('I'.$row, ' '.$d3['allowancemotorcycle']);
            $objWorkSheet->getStyle('I'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

			// JUMLAH MOBIL KECIL
            $objWorkSheet->SetCellValue('J'.$row, ' '.$d3['allowancecarsmall']);
            $objWorkSheet->getStyle('J'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

			// JUMLAH MOBIL BESAR
			$objWorkSheet->SetCellValue('K'.$row, ' '.$d3['allowancecarbig']);
			$objWorkSheet->getStyle('K'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

			$row++; $no++;
		}
	// }


	//exit;
	$objWorkSheet->setTitle('Tenant '.$d2['locationname']);
	$fileName = 'Tenant '.$d2['locationname'].' '.date('d F Y').'.xls';

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');
}
else {
	echo 'process download..';
	//header('location: ?pg=location');
}
?>