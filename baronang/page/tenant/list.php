<?php
$sloc = "SELECT a.locationname as locationname, 
				b.nama_prov as nama_prov, 
				c.nama_kabkot as nama_kabkot
		from 21masterlocation as a 
		inner join 11amasterprovince as b 
			on a.locationprovince = b.id_prov
		inner join 11bmastercity as c
			on a.locationcity = c.id_kabkot
		where a.masterlocationid='".$fun->getIDParam('lokasi')."'";
$qloc = mysqli_query($fun->getConnection(), $sloc) or die(mysqli_error($fun->getConnection()));
$dloc = mysqli_fetch_array($qloc);
?>

<div class="animated fadeinup delay-1">
	<div class="page-content">

		<h1 class="title uppercase txt-black" id="title"></h1>
		<h3 class="txt-black"><?php echo 'Location : '.$dloc['locationname']; ?></h3>

		<div class="m-t-30 txt-black">
			<?php
			$s1 = "SELECT * from 23dmastertenant where locationid='".$fun->getIDParam('lokasi')."' order by mastertenantid";
			//echo $s1;
			$q1 = mysqli_query($fun->getConnection(), $s1) or die(mysqli_error($fun->getConnection()));
			while($d1 = mysqli_fetch_array($q1)) { ?>
				<a href="<?php echo '#'.$fun->Enlink($d1['mastertenantid']); ?>" class="modal-trigger single-news animated fadeinright delay-3 waves-effect waves-light width-100">
					<?php echo $d1['tenantname']; ?>
				</a>
				<?php
			}
			?>
		</div>

		<div class="floating-button page-fab animated bouncein delay-3" style="z-index: 1;">
			<a class="btn-floating btn-large waves-effect waves-light primary-color btn z-depth-1 modal-trigger" href="<?php echo '#'.$fun->Enlink('modal_act'); ?>">
				<i class="ion-android-add"></i>
			</a>
		</div>

	</div>
</div>

<div class="modal bottom-sheet" id="<?php echo $fun->Enlink('modal_act'); ?>">
	<div class="modal-content choose-date txt-black">
		<p><a href="#import_tenant" class="modal-trigger"><i class="ion-ios-cloud-upload"></i> Import</a></p>
		<p><a href="<?php echo '?pg=tenant&sb=template&download=excel&'.$fun->setIDParam('lokasi', $fun->getIDParam('lokasi')); ?>"><i class="ion-ios-cloud-download"></i> Download Template</a></p>
		<p><a href="<?php echo '?pg=tenant&sb=add&'.$fun->setIDParam('lokasi', $fun->getIDParam('lokasi')); ?>"><i class="ion-android-create"></i> New Data</a></p>
	</div>
</div>

<div class="modal borad-20" id="import_tenant">
	<div class="modal-content choose-date txt-black">
		<form action="<?php echo 'process/43cimportmember.php?'.$fun->setIDParam('lokasi', $fun->getIDParam('lokasi')); ?>" method="post" enctype="multipart/form-data">
			
			<p>
				Choose File : <br>
				<input type="file" name="filetenant" id="filetenant" class="validate">
			</p>
			
			<?php
			$fun->buttonField('submit', 'ok', 'ok', 'borad-10 m-t-30 btn btn-large width-100 waves-effect waves-light primary-color', 'Import');
			?>

		</form>
	</div>
</div>

<script type="text/javascript">
	$('#toBack').click(function() {
		window.location.href = "?pg=location";
	});
</script>

<?php
$s1 = "SELECT * from 23dmastertenant where locationid='".$fun->getIDParam('lokasi')."'";
$q1 = mysqli_query($fun->getConnection(), $s1) or die(mysqli_error($fun->getConnection()));
while($d1 = mysqli_fetch_array($q1)) { 

	$s2 = "SELECT b.nama_prov as nama_prov, c.nama_kabkot as nama_kabkot
			from 11amasterprovince as b 
			inner join 11bmastercity as c
				on b.id_prov = c.id_prov";
	$q2 = mysqli_query($fun->getConnection(), $s2) or die(mysqli_error($fun->getConnection()));
	$d2 = mysqli_fetch_array($q2);

	?>
	<div class="modal borad-10" id="<?php echo $fun->Enlink($d1['mastertenantid']); ?>">
		<div class="modal-content choose-date txt-black">
			<p><h1 class="txt-black bold"><?php echo $d1['tenantname']; ?></h1></p>
			<p><span class="bold">Email : </span><?php echo $d1['tenantemail']; ?></p>
			<p><span class="bold">Address : </span><br><?php echo $d1['tenantaddess'].', '.$d2['nama_kabkot'].', '.$d2['nama_prov']; ?></p>
			<p><span class="bold">Phone Number : </span><?php echo $d1['tenantphone'] ?></p>
			<p class="txt-white"><a href="<?php echo '?pg=tenant&sb=edit&'.$fun->setIDParam('lokasi', $fun->getIDParam('lokasi')).'&'.$fun->setIDParam('ID', $d1['mastertenantid']); ?>" class="btn btn-large waves-light waves-light width-100 primary-color borad-20"><i class="ion-android-create"></i></a></p>
		</div>
	</div>
	<?php
}
?>
