<div class="row menubottom">
	<div class="col s6" style="margin-top: 0px; text-align: right;">
		<a href="<?php echo '#'.$fun->Enlink('kamera'); ?>" class="z-depth-1 btn btn-floating btn-large waves-effect waves-light primary-color floating-button animated bouncein delay-3 modal-trigger" id="<?php echo $fun->Enlink('menukamera'); ?>" style="margin-right: 25px;">
			<i class="ion-qr-scanner"></i>
		</a>
	</div>

	<div class="col s6 txt-white" style="margin-top: 0px; text-align: left;">
		<a href="<?php echo '#'.$fun->Enlink('qr'); ?>" class="z-depth-1 btn btn-floating btn-large waves-effect waves-light primary-color floating-button animated bouncein delay-3 modal-trigger" style="margin-left: 25px;">
			<img src="images/qrcodesquare4.png" style="width: 30px; height: 30px; margin-top: 13px; margin-left: 13px;">
		</a>
	</div>
</div>