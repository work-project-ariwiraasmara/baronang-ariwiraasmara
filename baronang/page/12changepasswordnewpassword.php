<?php
if( isset($_GET[$fun->Enlink('ideukey')]) ) { 

	if( $fun->getIDParam('ideukey') == '' || empty($fun->getIDParam('ideukey')) || is_null($fun->getIDParam('ideukey')) ) {
		echo "Nilai Paramater ideukey kosong";
		header('location:?');
	}
	else {
		$s1 = "SELECT * from 23cOutgoingEmail where ukey='".$fun->getIDParam('ideukey')."'";
		$q1 = mysqli_query($fun->getConnection(), $s1) or die(mysqli_error($fun->getConnection()));
		$d1 = mysqli_fetch_array($q1);

		if( count($d1) > 0 ) {

			if($d1['status'] > 0) { 

				if( strtotime(date('Y-m-d H:i:s')) < strtotime($d1['datevalid']) ) { ?>
					<div id="content" class="grey-blue login">

						<div id="toolbar" class="tool-login primary-color animated fadeindown">

						</div>

						<div class="login-form animated fadeinup delay-2 z-depth-1 txt-black">
							<div class="form-inputs">
								<h1 class="uppercase txt-black">ganti password baru</h1>

								<form action="<?php echo 'process/12updatepassword.php?'.$fun->setIDParam('ideukey', $fun->getIDParam('ideukey')); ?>" method="post">
									
									<div class="input-field">
										<?php
										$fun->inputField('password', 'pass', 'pass', 'Password', 'validate', 'txt-black', null, 'required');
										?>
									</div>

									<div class="input-field">
										<?php
										$fun->inputField('password', 'passconf', 'passconf', 'Password Confirmation', 'validate', 'txt-black', null, 'required');
										?>
									</div>

									<?php
									$fun->buttonField('submit', 'confirm', 'confirm', 'btn btn-large width-100 waves-effect waves-light primary-color m-t-30 borad-20', 'Change');
									?>	

								</form>

							</div>

							<?php
							require('footer.php');
							?>
						</div>
						
					</div>
					<?php
				}
				else { 
					$s2 = "DELETE from 23cOutgoingEmail where ukey='".$fun->getIDParam('ideukey')."'";
					$q2 = mysqli_query($fun->getConnection(), $s2) or die(mysqli_error($fun->getConnection()));

					//$fun->setOneSession('error_content', 'Sudah Tidak Bisa Merubah Password!<br><br>Sudah Lewat Masa Tenggat!<br><br>Silahkan Buat Permintaan Baru Lagi!', 2);
					//header('location:ei.php?'.$fun->setIDParam('title', 'Error Send Request!').'&'.$fun->setIDParam('link', 'index.php'));
					header('location: ../index.php?'.$fun->setIDParam('status', 'You can\'t change your password!<br><br>The deadline has passed!<br><br>Please make a new request again!'));
				}
			}
			else {
				//echo "Paramete ideukey ada tapi status = 0, maka data di hapus";
				$s2 = "DELETE from 23cOutgoingEmail where ukey='".$fun->getIDParam('ideukey')."'";
				$q2 = mysqli_query($fun->getConnection(), $s2) or die(mysqli_error($fun->getConnection()));

				$fun->setOneSession('error_content', '', 2);
				header('location:?');
			}

		}
		else {
			//echo "Parameter ideukey ada tapi di data tidak ada";
			$fun->setOneSession('error_content', '', 2);
			header('location:?');
		}
	}

}
else {
	//echo "Tidak Ada Paramater ideukey";
	$fun->setOneSession('error_content', '', 2);
	header('location:?');
}
?>