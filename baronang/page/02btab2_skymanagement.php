<div class="row txt-black center m-t-10" id="<?php echo $fun->Enlink('skymanagement'); ?>">
	<div class="col s4">
		<a href="?pg=location" class="waves-effect waves-light">
			<i class="ion-android-car"></i><br>
			Location
		</a>
	</div>
	
	<div class="col s4">
		<a href="?pg=generalproduct" class="waves-effect waves-light">
			<i class="ion-android-car"></i><br>
			Product
		</a>
	</div>

	<div class="col s4">
		<a href="?pg=product&sb=approval" class="waves-effect waves-light">
			<i class="ion-android-checkmark-circle"></i><br>
			Product<br>Approval
		</a>
	</div>
	
	<div class="col s4">
		<a href="?pg=tenant&sb=approval" class="waves-effect waves-light">
			<i class="ion-android-checkmark-circle"></i><br>
			Tenant<br>Approval
		</a>
	</div>
	
	<div class="col s4">
		<a href="?pg=report" class="waves-effect waves-light">
			<i class="ion-android-checkmark-circle"></i><br>
			Reports
		</a>
	</div>
	
	<?php /*
	<div class="col s4">
		<a href="?pg=tenant" class="waves-effect waves-light">
			<i class="ion-android-people"></i><br>
			Tenant
		</a>
	</div>
	*/ ?>
</div>
