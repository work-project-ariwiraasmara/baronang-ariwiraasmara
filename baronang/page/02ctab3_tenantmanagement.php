<div class="txt-black m-t-10" id="<?php echo $fun->Enlink('tenantmanagement'); ?>">
	
	<?php
	$s1 = "SELECT distinct 	a.masterlocationid as masterlocationid,
							a.locationname as locationname
		from 21masterlocation as a 
		inner join 23dmastertenant as b
			on a.masterlocationid = b.locationid
		where b.memberid='".$fun->getValookie('id')."'";
	$q1 = mysqli_query($fun->getConnection(), $s1) or die(mysqli_error($fun->getConnection()));
	while($d1 = mysqli_fetch_array($q1)) { 
		$tenantname = '';
		?>

		<div id="<?php echo $fun->Enlink('loc-'.$d1['locationname']); ?>"><h3 class="txt-black uppercase"><?php echo $d1['locationname']; ?></h3>
		<div class="row m-b-30" style="border: 1px solid #aaa; border-radius: 10px;">

			<?php
			$tohidecard1 = 0;
			// KARTU 7100
			$scard1 = "SELECT 	a.mastertenantid as mastertenantid,
								a.tenantname as tenantname,
								b.mastercardid as mastercardid
						from 23dmastertenant as a 
						inner join 23amastercard as b 
							on a.mastertenantid = b.memberid
						where b.mastercardid like '7100%' and a.memberid='".$fun->getValookie('id')."' and a.locationid='".$d1['masterlocationid']."'";
			//echo $s2.'<br><br>';
			$qcard1 = mysqli_query($fun->getConnection(), $scard1) or die(mysqli_error($fun->getConnection()));
			$rcard1 = mysqli_num_rows($qcard1);

			if($rcard1 > 0) {
				$tohidecard1 = 1;
				while($dcard1 = mysqli_fetch_array($qcard1)) { 

					$s2 = "SELECT * from 31bmastercarduser where activationstatus='1' and cardid='".$dcard1['mastercardid']."'";
					$q2 = mysqli_query($fun->getConnection(), $s2) or die(mysqli_error($fun->getConnection()));
					$r2 = mysqli_num_rows($q2);

					if($r2 > 0) {
						$d2 = mysqli_fetch_array($q2);

						$s3 = "SELECT membername from 22bmastermember where mastermemberid='".$d2['useridactivated']."'";
						$q3 = mysqli_query($fun->getConnection(), $s3) or die(mysqli_error($fun->getConnection()));
						$d3 = mysqli_fetch_array($q3);

						$tenantname = $d3['membername'];
					}
					else {
						$tenantname = $dcard1['tenantname'];
					}

					$svhl = "SELECT b.vehicletype as vehicle, b.productimage as productimage
							from 31amastercardproductowned as a 
							inner join 22amasterproduct as b 
								on a.productowned = b.masterproductid
							where a.cardid='".$dcard1['mastercardid']."'";
					$qvhl = mysqli_query($fun->getConnection(), $svhl) or die(mysqli_error($fun->getConnection()));
					$dvhl = mysqli_fetch_array($qvhl);

					$locid = substr($dcard1['mastercardid'],0,9).'0000000';
					$sloc = "SELECT * from 21masterlocation where masterlocationid = '".$locid."'";
					$qloc = mysqli_query($fun->getConnection(), $sloc) or die(mysqli_error($fun->getConnection()));
					$dloc = mysqli_fetch_array($qloc);
					?>
					<div class="col s6 m-b-30 m-t-10 txt-white">
						<a href="<?php echo '#'.$fun->Enlink($dcard1['mastercardid']); ?>" class="modal-trigger">
							<div class="card_tenant_container">
								<div class="card_sky_tenant">
									<img src="images/logo skyparking 1.png" style="width: 20px; height: 20px;">
								</div>

								<div class="card_company_tenant">
									<?php
									if($dloc['image'] == '' || empty($dloc['image'])) {
										echo $dloc['locationname'];
									}
									else { ?>
										<img src="<?php echo 'images/location/'.$dloc['image']; ?>" style="width: 20px; height: 20px;">
										<?php
									}
									?>
								</div>

								<img src="images/2.png" id="imgcard">

								<div class="card_identity_tenant" style="font-size: 9px;">
									<?php
									echo $tenantname.'<br>';
									echo $dcard1['mastercardid'];
									?>
								</div>

								<div class="card_logo_tenant" style="margin-bottom: 1px;">
									<?php
									if($dvhl['productimage'] == '' || empty($dvhl['productimage'])) {?>
										<div style="margin-bottom: 10px;"><?php echo $dvhl['vehicle']; ?></div>
										<?php
									}
									else { ?>
										<div style="margin-bottom: 10px;">
											<img src="<?php echo 'images/icon/'.$dvhl['productimage']; ?>" style="width: 20px; height: 15px;">
										</div>
										<?php
									}
									?>

									<img src="images/icon/Logo-whitefish-icon.png" style="width: 20px; height: 10px;">
								</div>
							</div>
						</a>
					</div>
					<?php
				}
			}
			else {
				$tohidecard1 = 0;
			}

			// KARTU 7101
			$tohidecard2 = 0;
			$scard2 = "SELECT 	a.mastertenantid as mastertenantid,
								a.tenantname as tenantname,
								b.mastercardid as mastercardid
						from 23dmastertenant as a 
						inner join 23amastercard as b 
							on a.mastertenantid = b.memberid
						where b.mastercardid like '7101%' and a.memberid='".$fun->getValookie('id')."' and a.locationid='".$d1['masterlocationid']."'";
			//echo $s2.'<br><br>';
			$qcard2 = mysqli_query($fun->getConnection(), $scard2) or die(mysqli_error($fun->getConnection()));
			$rcard2 = mysqli_num_rows($qcard2);

			if($rcard2 > 0) {
				$tohidecard2 = 1;
				while($dcard2 = mysqli_fetch_array($qcard2)) { 

					$s2 = "SELECT * from 31bmastercarduser where activationstatus='1' and cardid='".$dcard2['mastercardid']."'";
					$q2 = mysqli_query($fun->getConnection(), $s2) or die(mysqli_error($fun->getConnection()));
					$r2 = mysqli_num_rows($q2);

					if($r2 > 0) {
						$d2 = mysqli_fetch_array($q2);

						$s3 = "SELECT * from 22bmastermember where mastermemberid='".$d2['useridactivated']."'";
						$q3 = mysqli_query($fun->getConnection(), $s3) or die(mysqli_error($fun->getConnection()));
						$d3 = mysqli_fetch_array($q3);

						$tenantname = $d3['membername'];
					}
					else {
						$tenantname = $dcard2['tenantname'];
					}

					$svhl = "SELECT b.vehicletype as vehicle, b.productimage as productimage
							from 31amastercardproductowned as a 
							inner join 22amasterproduct as b 
								on a.productowned = b.masterproductid
							where a.cardid='".$dcard2['mastercardid']."'";
					$qvhl = mysqli_query($fun->getConnection(), $svhl) or die(mysqli_error($fun->getConnection()));
					$dvhl = mysqli_fetch_array($qvhl);

					$locid = substr($dcard2['mastercardid'],0,9).'0000000';
					$sloc = "SELECT * from 21masterlocation where masterlocationid = '".$locid."'";
					$qloc = mysqli_query($fun->getConnection(), $sloc) or die(mysqli_error($fun->getConnection()));
					$dloc = mysqli_fetch_array($qloc);
					?>
					<div class="col s6 m-b-30 m-t-10 txt-white">
						<a href="<?php echo '#'.$fun->Enlink($dcard2['mastercardid']); ?>" class="modal-trigger">
							<div class="card_tenant_container">
								<div class="card_sky_tenant">
									<img src="images/logo skyparking 1.png" style="width: 20px; height: 20px;">
								</div>

								<div class="card_company_tenant">
									<?php
									if($dloc['image'] == '' || empty($dloc['image'])) {
										echo $dloc['locationname'];
									}
									else { ?>
										<img src="<?php echo 'images/location/'.$dloc['image']; ?>" style="width: 20px; height: 20px;">
										<?php
									}
									?>
								</div>

								<img src="images/3.png" id="imgcard">

								<div class="card_identity_tenant" style="font-size: 9px;">
									<?php
									echo $tenantname.'<br>';
									echo $dcard2['mastercardid'];
									?>
								</div>

								<div class="card_logo_tenant" style="margin-bottom: 1px;">
									<?php
									if($dvhl['productimage'] == '' || empty($dvhl['productimage'])) {?>
										<div style="margin-bottom: 10px;"><?php echo $dvhl['vehicle']; ?></div>
										<?php
									}
									else { ?>
										<div style="margin-bottom: 10px;">
											<img src="<?php echo 'images/icon/'.$dvhl['productimage']; ?>" style="width: 20px; height: 10px;">
										</div>
										<?php
									}
									?>

									<img src="images/icon/Logo-whitefish-icon.png" style="width: 20px; height: 10px;">
								</div>
							</div>
						</a>
					</div>
					<?php
				}
			}
			else {
				$tohidecard2 = 0;
			}


			// KARTU 7104
			$tohidecard3 = 0;
			$scard3 = "SELECT 	a.mastertenantid as mastertenantid,
								a.tenantname as tenantname,
								b.mastercardid as mastercardid
						from 23dmastertenant as a 
						inner join 23amastercard as b 
							on a.mastertenantid = b.memberid
						where b.mastercardid like '7104%' and a.memberid='".$fun->getValookie('id')."' and a.locationid='".$d1['masterlocationid']."'";
			//echo $s2.'<br><br>';
			$qcard3 = mysqli_query($fun->getConnection(), $scard3) or die(mysqli_error($fun->getConnection()));
			$rcard3 = mysqli_num_rows($qcard3);

			if($rcard3 > 0) {
				$tohidecard3 = 1;
				while($dcard3 = mysqli_fetch_array($qcard3)) { 

					$s2 = "SELECT * from 31bmastercarduser where activationstatus='1' and cardid='".$dcard3['mastercardid']."'";
					$q2 = mysqli_query($fun->getConnection(), $s2) or die(mysqli_error($fun->getConnection()));
					$r2 = mysqli_num_rows($q2);

					if($r2 > 0) {
						$d2 = mysqli_fetch_array($q2);

						$s3 = "SELECT * from 22bmastermember where mastermemberid='".$d2['useridactivated']."'";
						$q3 = mysqli_query($fun->getConnection(), $s3) or die(mysqli_error($fun->getConnection()));
						$d3 = mysqli_fetch_array($q3);

						$tenantname = $d3['membername'];
					}
					else {
						$tenantname = $dcard3['tenantname'];
					}

					$svhl = "SELECT b.vehicletype as vehicle, b.productimage as productimage
							from 31amastercardproductowned as a 
							inner join 22amasterproduct as b 
								on a.productowned = b.masterproductid
							where a.cardid='".$dcard3['mastercardid']."'";
					$qvhl = mysqli_query($fun->getConnection(), $svhl) or die(mysqli_error($fun->getConnection()));
					$dvhl = mysqli_fetch_array($qvhl);

					$locid = '7101'.substr($dcard3['mastercardid'],4,5).'0000000';
					$sloc = "SELECT * from 21masterlocation where masterlocationid = '".$locid."'";
					$qloc = mysqli_query($fun->getConnection(), $sloc) or die(mysqli_error($fun->getConnection()));
					$dloc = mysqli_fetch_array($qloc);
					?>
					<div class="col s6 m-b-30 m-t-10 txt-white">
						<a href="<?php echo '#'.$fun->Enlink($dcard3['mastercardid']); ?>" class="modal-trigger">
							<div class="card_tenant_container">
								<div class="card_sky_tenant">
									<img src="images/logo skyparking 1.png" style="width: 20px; height: 20px;">
								</div>

								<div class="card_company_tenant">
									<?php
									if($dloc['image'] == '' || empty($dloc['image'])) {
										echo $dloc['locationname'];
									}
									else { ?>
										<img src="<?php echo 'images/location/'.$dloc['image']; ?>" style="width: 20px; height: 20px;">
										<?php
									}
									?>
								</div>

								<img src="images/4.png" id="imgcard">

								<div class="card_identity_tenant" style="font-size: 9px;">
									<?php
									echo $tenantname.'<br>';
									echo $dcard3['mastercardid'];
									?>
								</div>

								<div class="card_logo_tenant" style="margin-bottom: 1px;">
									<?php
									if($dvhl['productimage'] == '' || empty($dvhl['productimage'])) {?>
										<div style="margin-bottom: 10px;"><?php echo $dvhl['vehicle']; ?></div>
										<?php
									}
									else { ?>
										<div style="margin-bottom: 10px;">
											<img src="<?php echo 'images/icon/'.$dvhl['productimage']; ?>" style="width: 20px; height: 15px;">
										</div>
										<?php
									}
									?>

									<img src="images/icon/Logo-whitefish-icon.png" style="width: 20px; height: 10px;">
								</div>
							</div>
						</a>
					</div>
					<?php
				}
			}
			else {
				$tohidecard3 == 0;
			}

			if( ($tohidecard1 == 0) && ($tohidecard2 == 0) && ($tohidecard3 == 0)) { ?>
				<script type="text/javascript">
					$("<?php echo '#'.$fun->Enlink('loc-'.$d1['locationname']) ?>").addClass("hide");
				</script>
				<?php
			}
			?>

		</div></div>
		<?php
	}
	?>
	
</div>
