<div class="animated fadeinup delay-1">
	<div class="page-content">

		<h1 class="title uppercase txt-black" id="title"></h1>
		<h3 class="title uppercase txt-black"><?php echo 'Card Number : '.$fun->getIDParam('cardid'); ?></h3>

		<div class="m-t-30">
			<?php
			$sact = "SELECT a.activitylogid as activitylogid,
							a.cardid as cardid, a.datecreated as datecreated,
							a.activitydescription as activitydescription,
							a.debet as debet, a.credit as credit,
							a.contraaccount as contraaccount,
							b.locationname as locationname
					from 31cmastercardactivity as a 
					inner join 21masterlocation as b 
						on a.locationid = b.masterlocationid
					where activitycode like '8%' and activitymemberid='".$fun->getValookie('id')."' and cardid='".$fun->getIDParam('cardid')."' order by datecreated desc limit 10";
			$qact = mysqli_query($fun->getConnection(), $sact) or die(mysqli_error($fun->getConnection()));
			$ract = mysqli_num_rows($qact);
			if($ract > 0) {
				while($dact = mysqli_fetch_array($qact)) { 
					if($dact['debet'] == '' || empty($dact['debet'])) {
						$dk = 'CR';
					}

					if($dact['credit'] == '' || empty($dact['credit'])) {
						$dk = 'DB';
					}

					?>
					<div class="m-t-10 single-news animated fadeinright delay-3 txt-black">
						<div class="row">
							<div class="col s6">
								<?php echo date('d F Y', strtotime($dact['datecreated'])) ?>
							</div>
							
							<div class="col s6 right">
								<?php echo ''; ?>
							</div>

							<div class="col s6">
								<?php echo $dact['activitydescription']; ?>
							</div>
							
							<div class="col s6 right">
								<?php echo $dact['locationname']; ?>
							</div>

						</div>
					</div>
					<?php
				}
			}
			else { ?>
				<div class="center single-news animated fadeinright delay-3 txt-black">
					<h2 class="bold italic txt-black">Data is Empty</h2> 
				</div>
				<?php
			}
			?>
		</div>

	</div>
</div>

<script type="text/javascript">
	$('#toBack').click(function() {
		window.location.href = "?";
	});
</script>
