<div class="animated fadeinup delay-1">
	<div class="page-content">

		<h1 class="title uppercase txt-black" id="title"></h1>

		<div class="m-t-30">
			<?php
			$like = '7102'.date('y').'100';
			$lokasi = $fun->getIDParam('lokasi');

			$sloc = "SELECT locationname from 21masterlocation where masterlocationid='$lokasi'";
			$qloc = mysqli_query($fun->getConnection(), $sloc) or die(mysqli_error($fun->getConnection()));
			$dloc = mysqli_fetch_array($qloc);

			if($lokasi == '' || empty($lokasi)) {
				$stopup = "SELECT * from 22amasterproduct where masterproductid like '$like%' and active = '1'";
			}
			else {
				$stopup = "SELECT * from 22amasterproduct where masterproductid like '$like%' and locationid='$lokasi' and active = '1'";
			}

			$qtopup = mysqli_query($fun->getConnection(), $stopup) or die(mysqli_error($fun->getConnection()));
			while($dtopup = mysqli_fetch_array($qtopup)) { ?>
				<a href="<?php echo '#'.$fun->Enlink($dtopup['masterproductid']); ?>" class="modal-trigger single-news animated fadeinright delay-3 waves-effect waves-light width-100">
					<?php
					echo $dtopup['productdescription'];
					?>
				</a>
				<?php
			}
			?>
		</div>

	</div>
</div>

<?php
$no = 1;
$qtopup = mysqli_query($fun->getConnection(), $stopup) or die(mysqli_error($fun->getConnection()));
while($dtopup = mysqli_fetch_array($qtopup)) { ?>
	<div class="modal borad-20" id="<?php echo $fun->Enlink($dtopup['masterproductid']); ?>">
		<div class="modal-content choose-date txt-black">
			<p><span class="bold">Product Name</span> : <?php echo $dtopup['productdescription']; ?></p>
			<p><span class="bold">Get Point(s)</span> : <?php echo $fun->FormatNumber($dtopup['validity_days']); ?></p>
			<p><span class="bold">Price</span> : Rp. <?php echo $fun->FormatRupiah($dtopup['productprice']); ?></p>
			
			<form action="<?php echo 'process/71cardtopup.php?'.$fun->setIDParam('cardid', $fun->getIDParam('cardid')).'&'.$fun->setIDParam('company', '71').'&'.$fun->setIDParam('productid', $dtopup['masterproductid']).'&'.$fun->setIDParam('lokasi', $lokasi); ?>" method="post">
			
				<?php
				$fun->inputField('number', 'amount-'.$no, 'amount', '', 'validate hide', 'txt-black', $dtopup['validity_days'], 'required');
				
				$fun->buttonField('button', 'btn_topup_confirm-'.$no, 'btn_topup_confirm-'.$no, 'btn btn-large width-100 primary-color m-t-10 borad-20 waves-effect waves-light', 'Top Up');
			
				$fun->buttonField('submit', 'submit_topup_confirm-'.$no, 'submit_topup_confirm-'.$no, 'btn btn-large width-100 waves-effect waves-light primary-color m-t-30 borad-20 hide', 'to Process');
				?>
			</form>
			
		</div>
	</div>

	<script type="text/javascript">
		$("<?php echo '#'.$fun->Enlink('btn_topup_confirm-'.$no); ?>").click(function(){
			var amount = $("<?php echo '#'.$fun->Enlink('amount-'.$no); ?>").val();

			if(amount == '' || amount == null) {
				alert('Amount can\'t be empty!');
			}
			else {
				$(document).ready(function() {
					$("<?php echo '#'.$fun->Enlink('toConfirmPIN'); ?>").click();
					$("<?php echo '#'.$fun->Enlink('idttrigger'); ?>").val("<?php echo $fun->Enlink('submit_topup_confirm-'.$no); ?>");
				});
			}
			
		});
	</script>
	<?php
	$no++;
}
?>

<script type="text/javascript">
	$('#toBack').click(function() {
		window.location.href = "?";
	});
</script>
