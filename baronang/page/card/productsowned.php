<div class="animated fadeinup delay-1">
	<div class="page-content">

		<h1 class="title uppercase txt-black" id="title"></h1>
		<h3 class="title uppercase txt-black"><?php echo 'Card Number : '.$fun->getIDParam('cardid'); ?></h3>

		<div class="m-t-30">
			<?php
			$date = date('Y-m-d');
			$sact = "SELECT a.productowned as productowned,
							a.cardid as cardid, a.startdate as startdate,
							a.expirationdate as expirationdate,
							b.productdescription as productdescription, 
							b.additionaldesc as additionaldesc,
							b.vehicletype as vehicletype,
							b.validity_days as qty,
							b.unit as unit,
							c.locationname as locationname
					from 31amastercardproductowned as a 
					inner join 22amasterproduct as b 
						on a.productowned = b.masterproductid
					inner join 21masterlocation as c 
						on b.locationid = c.masterlocationid
					where cardid='".$fun->getIDParam('cardid')."' and a.expirationdate > '".$date."'
					order by a.expirationdate";
			$qact = mysqli_query($fun->getConnection(), $sact) or die(mysqli_error($fun->getConnection()));
			$ract = mysqli_num_rows($qact);
			if($ract > 0) { ?>
				<div class="m-t-10 single-news animated fadeinright delay-3 txt-black">
				<div class="col s6 right">
						<span class="bold underline"><?php echo 'Expiration Date'; ?></span>
				</div>
				</div>
				<?php
				while($dact = mysqli_fetch_array($qact)) { 
					?>
					<div class="m-t-10 single-news animated fadeinright delay-3 txt-black">
						<div class="row">
							
							<div class="col s6">
								<?php echo $dact['locationname']; ?>
							</div>
							
							<div class="col s6 right">
								<?php echo date('d F Y', strtotime($dact['expirationdate'])) ?>
							</div>
							
							<div class="col s6">
								<?php echo $dact['vehicletype']; ?>
							</div>
							
						</div>
					</div>
					<?php
				}
			}
			else { ?>
				<div class="center single-news animated fadeinright delay-3 txt-black">
					<h2 class="bold italic txt-black">Data is Empty</h2> 
				</div>
				<?php
			}
			?>
		</div>

	</div>
</div>

<script type="text/javascript">
	$('#toBack').click(function() {
		window.location.href = "?";
	});
</script>
