<div class="animated fadeinup delay-1">
	<div class="page-content">

		<h1 class="title uppercase txt-black" id="title"></h1>
		<h3 class="title uppercase txt-black"><?php echo 'Card Number : '.$fun->getIDParam('cardid'); ?></h3>
		
		<div class="m-t-30 table-responsive">
			<table class="table">
				<tbody>
					<?php
					$sfh = "SELECT * from 31cmastercardactivity 
							where 	activitycode not like '8%' and 
									activitymemberid='".$fun->getValookie('id')."' 
									and cardid='".$fun->getIDParam('cardid')."' order by datecreated desc limit 10";
					//echo $sfh;
					$qfh = mysqli_query($fun->getConnection(), $sfh) or die(mysqli_error($fun->getConnection()));
					$rfh = mysqli_num_rows($qfh);
					if($rfh > 0) {
						while($dfh = mysqli_fetch_array($qfh)) { 
							if($dfh['debet'] == 0 || empty($dfh['debet'])) {
								$dk = 'CR';
								$nominal = $dfh['credit'];
								$dp = 'Points CR';
								$point = $dfh['pointcredit'];
							}

							if($dfh['credit'] == 0 || empty($dfh['credit'])) {
								$dk = 'DB';
								$nominal = $dfh['debet'];
								$dp = 'Points DB';
								$point = $dfh['pointdebet'];
							}
							
							$s2 = "SELECT * from 22amasterproduct where masterproductid='".$dfh['contraaccount']."'";
							$q2 = mysqli_query($fun->getConnection(), $s2) or die(mysqli_error($fun->getConnection()));
							$d2 =  mysqli_fetch_array($q2);

							?>
							<tr class="m-t-10 animated fadeinright delay-3 txt-black">
								<td>
									<?php 
									echo date('d F Y', strtotime($dfh['datecreated'])).'<br>';
									echo $dfh['activitydescription'];
									?>
								</td>

								<td>
									<span class="bold underline right"><?php echo $dp; ?></span><br>
									<span class="right"><?php echo $fun->FormatNumber($point); ?></span>
								</td>

								<td class="right">
									<span class="bold underline"><?php echo $dk; ?></span><br>
									<?php echo $fun->FormatRupiah($nominal); ?>
								</td>
							</tr>

							<tr class="m-t-10 animated fadeinright delay-3 txt-black single-news">
								<td colspan="3"><?php echo $d2['productdescription']; ?></td>
							</tr>
							<?php
						}
					}
					else { ?>
						<tr class="center single-news animated fadeinright delay-3 txt-black">
							<td><h2 class="bold italic txt-black">Data is Empty</h2></td> 
						</tr>
						<?php
					}
					?>
				</tbody>
			</table>
		</div>

	</div>
</div>

<script type="text/javascript">
	$('#toBack').click(function() {
		window.location.href = "?";
	});
</script>
