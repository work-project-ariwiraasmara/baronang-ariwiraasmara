<div class="animated fadeinup delay-1">
	<div class="page-content">

		<h1 class="title uppercase txt-black" id="title"></h1>

		<?php
		if( isset($_GET[$fun->Enlink('lokasi')]) ) {
			$sloc = "SELECT a.locationname as locationname, b.nama_prov as nama_prov, c.nama_kabkot as nama_kabkot
					from 21masterlocation as a 
					inner join 11amasterprovince as b 
						on a.locationprovince = b.id_prov
					inner join 11bmastercity as c
						on a.locationcity = c.id_kabkot
					where a.active = '1' and a.masterlocationid='".$fun->getIDParam('lokasi')."'";
			$qloc = mysqli_query($fun->getConnection(), $sloc) or die(mysqli_error($fun->getConnection()));
			$dloc = mysqli_fetch_array($qloc);
			?>
			<h3 class="txt-black"><?php echo 'Location : '.$dloc['locationname']; ?></h3>

			<div class="m-t-30 txt-black">
			<?php
				$s1 = "SELECT * from 22amasterproduct where active='1' and productprice > 0 and locationid='".$fun->getIDParam('lokasi')."' order by productdescription ASC";
				$q1 = mysqli_query($fun->getConnection(), $s1) or die(mysqli_error($fun->getConnection()));
				while($d1 = mysqli_fetch_array($q1)) { ?>
					<a href="<?php echo '#'.$fun->Enlink($d1['masterproductid']); ?>" class="modal-trigger single-news animated fadeinright delay-3 waves-effect waves-light width-100">
						<?php
						echo $d1['additionaldesc'].'-'.$d1['productdescription'];
						?>
					</a>
					<?php
				}
				?>
			</div>
			<?php
		}
		else { ?>

			<div class="m-t-30 txt-black">
				<ul id="myUL" class="txt-black">
					<?php
					$s1 = "SELECT distinct 	a.id_prov as id_prov, a.nama_prov as nama_prov
							from 11amasterprovince as a 
							inner join 21masterlocation as b 
								on a.id_prov = b.locationprovince
							order by nama_prov ASC";
					$q1 = mysqli_query($fun->getConnection(), $s1) or die(mysqli_error($fun->getConnection()));
					while($d1 = mysqli_fetch_array($q1)) { ?>
						<li class="m-b-10 txt-black"><span class="caret"><?php echo $d1['nama_prov']; ?></span>
							<ul class="nested txt-black">
								<?php
								$s2 = "SELECT * from 21masterlocation where active = '1' and locationprovince='".$d1['id_prov']."' order by locationname ASC";
								$q2 = mysqli_query($fun->getConnection(), $s2) or die(mysqli_error($fun->getConnection()));
								$r2 = mysqli_num_rows($q2);
								if($r2 > 0) {
									while($d2 = mysqli_fetch_array($q2)) { ?>
										<li class="m-l-30 txt-black m-b-10" style=""><a href="<?php echo '?pg=card&sb=product&'.$fun->setIDParam('cardid', $fun->getIDParam('cardid')).'&'.$fun->setIDParam('lokasi', $d2['masterlocationid']); ?>"><?php echo $d2['locationname']; ?></a></li>
										<?php
									}
								}
								else { ?>
									<li class="m-l-30 italic">[Data Not Available]</li>
									<?php
								}
								?>
							</ul>
						</li>
						<?php
					}
					?>
				</ul>
			</div>

			<?php
		}
		?>

		

	</div>
</div>

<script type="text/javascript">
	$('#toBack').click(function() {
		<?php
		if( isset($_GET[$fun->Enlink('lokasi')]) ) { ?>
			window.location.href = "<?php echo '?pg=card&sb=product&'.$fun->setIDParam('cardid', $fun->getIDParam('cardid')); ?>";
			<?php
		}
		else { ?>
			window.location.href = "?";
			<?php
		}
		?>
		
	});
</script>

<?php
if( isset($_GET[$fun->Enlink('lokasi')]) ) {
	$no = 1;
	$s1 = "SELECT * from 22amasterproduct where active='1' and productprice > 0 and locationid='".$fun->getIDParam('lokasi')."' order by productdescription ASC";
	$q1 = mysqli_query($fun->getConnection(), $s1) or die(mysqli_error($fun->getConnection()));
	while($d1 = mysqli_fetch_array($q1)) { ?>
		<div class="modal borad-20" id="<?php echo $fun->Enlink($d1['masterproductid']); ?>">
			<div class="modal-content choose-date txt-black">
				<p><h1 class="txt-black bold"><?php echo $d1['additionaldesc'].'-'.$d1['productdescription']; ?></h1></p>
				<p><span class="bold">Type : </span><?php echo $d1['vehicletype']; ?></p>
				<p><span class="bold">Quantity : </span><?php echo $fun->FormatNumber($d1['validity_days']).'&nbsp;'.$d1['unit']; ?></p>
				<p><span class="bold">Price : Rp. </span><?php echo $fun->FormatRupiah($d1['productprice']); ?></p>	
				<form action="<?php echo 'process/72addcardproduct.php?'.$fun->setIDParam('cardid', $fun->getIDParam('cardid')).'&'.$fun->setIDParam('company', '71').'&'.$fun->setIDParam('lokasi', $fun->getIDParam('lokasi')).'&'.$fun->setIDParam('productid', $d1['masterproductid']); ?>" method="post">
					<p class="m-t-10">
						<span class="bold">Quantity :</span>
						<input type='button' class='qtyminus' id="<?php echo $fun->Enlink('qtyminus-'.$no); ?>" value='-' field="quantity" />
								    
						<span id="<?php echo $fun->Enlink('qtylabel-'.$no); ?>">1</span>
						<input type='text' name='<?php echo $fun->Enlink('qty'); ?>' value='1' id="<?php echo $fun->Enlink('qty-'.$no); ?>" class='hide' />
								    
						<input type='button' class='qtyplus' id="<?php echo $fun->Enlink('qtyplus-'.$no); ?>" value='+' field="quantity" />
					</p>

					<p class="m-t-5">
						<?php
						$fun->dtField('date', 'startat', 'startat-'.$no, 'Start At : ', 'validate', 'txt-black bold');
						?>
					</p>

					<?php
					$fun->inputField('text', 'productid-'.$no, 'productid', '','validate hide', 'txt-black bold', $fun->Enval($d1['masterproductid']));

					$fun->buttonField('button', 'btn_cart-'.$no, 'btn_cart-'.$no, 'btn btn-large width-100 primary-color m-t-10 borad-20 waves-effect waves-light', 'Add to Cart');

					$fun->buttonField('button', 'btn_product_confirm-'.$no, 'btn_product_confirm-'.$no, 'btn btn-large width-100 primary-color m-t-10 borad-20 waves-effect waves-light', 'Purchase');

					$fun->buttonField('submit', 'confirm-'.$no, 'confirm-'.$no, 'btn btn-large width-100 waves-effect waves-light primary-color m-t-10 borad-20 hide', 'to Process');
					?>
				</form>
			</div>
		</div>

		<script type="text/javascript">
			$("<?php echo '#'.$fun->Enlink('btn_product_confirm-'.$no); ?>").click(function(){
				var date = $("<?php echo '#'.$fun->Enlink('startat-'.$no); ?>").val();
				
				if(date == '' || date == null) {
					alert('Date can\'t be empty!');
				}
				else {
					$(document).ready(function() {
						$("<?php echo '#'.$fun->Enlink('toConfirmPIN'); ?>").click();
						$("<?php echo '#'.$fun->Enlink('idttrigger'); ?>").val("<?php echo $fun->Enlink('confirm-'.$no); ?>");
					});
				}
				
			});

			$("<?php echo '#'.$fun->Enlink('btn_cart-'.$no); ?>").click(function(){
				var product = $("<?php echo '#'.$fun->Enlink('productid-'.$no); ?>").val();
				var card 	= "<?php echo $fun->Enval($fun->getIDParam('cardid')); ?>";
				var qty 	= $("<?php echo '#'.$fun->Enlink('qty-'.$no); ?>").val();
				var date 	= $("<?php echo '#'.$fun->Enlink('startat-'.$no); ?>").val();

				$.ajax({
			    	url : "process/ajax/ajax_add_product_shop.php",
			    	type : 'POST',
			    	data: { 
			        	productid: product,
			        	qty: qty,
			        	date: date,
			        	cardid: card
			    	},
			    	success : function(data) {
			    		//console.log(data);
			    		window.location.href = "<?php echo '?pg=card&sb=product&'.$fun->setIDParam('cardid', $fun->getIDParam('cardid')).'&'.$fun->setIDParam('status', 'The product has been added to the basket'); ?>"
			    	},
			    	error : function(){
			        	alert('Terjadi kesalahan dalam jaringan!\nTidak dapat mengambil data Product!\nSilahkan cek koneksi jaringan dan coba lagi!');
			        	return false;
			    	}
			    });
			});


			$("<?php echo '#'.$fun->Enlink('qtyminus-'.$no); ?>").click(function(){
				var prc = $("<?php echo '#'.$fun->Enlink('price').'-'.$no; ?>").val();
				var qty = $("<?php echo '#'.$fun->Enlink('qty-'.$no); ?>").val();
				var res = parseInt(qty) - 1;
				if(res < 1) {
					res = 1;
				}

				$("<?php echo '#'.$fun->Enlink('qty-'.$no); ?>").val(res);
				$("<?php echo '#'.$fun->Enlink('qtylabel-'.$no); ?>").html(res);
			});

			$("<?php echo '#'.$fun->Enlink('qtyplus-'.$no); ?>").click(function(){
				var prc = $("<?php echo '#'.$fun->Enlink('price').'-'.$no; ?>").val();
				var qty = $("<?php echo '#'.$fun->Enlink('qty-'.$no); ?>").val();
				var res = parseInt(qty) + 1;

				$("<?php echo '#'.$fun->Enlink('qty-'.$no); ?>").val(res);
				$("<?php echo '#'.$fun->Enlink('qtylabel-'.$no); ?>").html(res);
			});
		</script>
		<?php
		$no++;
	}
}
?>
