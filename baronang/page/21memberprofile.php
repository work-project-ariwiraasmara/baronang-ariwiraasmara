<?php
$session = $fun->getValookie('session');

if($session == 'member') {
	$s1 = "SELECT * from 22bmastermember where mastermemberid='".$fun->getValookie('id')."'";
	$name = "membername";
	$tlp = "memberphone";
	$alamat = "memberaddress";
	$prov = "memberprovince";
	$kabkot = "membercity";
	$aktif = "active";
	$btn_lbl = "Save";

	$q1 = mysqli_query($fun->getConnection(), $s1) or die(mysqli_error($fun->getConnection()));
	$d1 = mysqli_fetch_array($q1);

	if($d1['active'] == 2) {
		$link_form = "process/21bupdateprofile.php?".$fun->setIDParam('status', 2);
	}
	else {
		$link_form = "process/21bupdateprofile.php?";
	}

}
else {
	$s1 = "SELECT * from 23dmastertenant where mastertenantid='".$fun->getValookie('id')."'";
	$name = "tenantname";
	$tlp = "tenantphone";
	$alamat = "tenantaddess";
	$prov = "province";
	$kabkot = "city";
	$aktif = "status";
	$btn_lbl = "OK";

	$q1 = mysqli_query($fun->getConnection(), $s1) or die(mysqli_error($fun->getConnection()));
	$d1 = mysqli_fetch_array($q1);

	$link_form = "process/21clinkmember.php?".$fun->setIDParam('ID', $d1['mastertenantid']);
}


$s2 = "SELECT a.id_prov, b.id_kabkot, a.nama_prov, b.nama_kabkot from 11amasterprovince as a 
		inner join 11bmastercity as b 
			on a.id_prov = b.id_prov
		where a.id_prov='".$d1[$prov]."' and b.id_kabkot='".$d1[$kabkot]."'";
$q2 = mysqli_query($fun->getConnection(), $s2) or die(mysqli_error($fun->getConnection()));
$d2 = mysqli_fetch_array($q2);
?>

<div class="animated fadeinup delay-1">
	<div class="page-content txt-black">
		<?php /*
		<span class="bold italic txt-black"><?php echo $d1['membername']; ?></span> 
		*/ ?>
		<h1 class="title bold txt-black" id="title"></h1>

		<div class="" id="<?php echo $fun->Enlink('form_updateprofil'); ?>">

			<form action="<?php echo $link_form; ?>" method="post">

				<?php
				if($session == 'member') { 

					if($d1[$aktif] == 2) { ?>
						<div class="input-field">
							<?php
							$fun->inputField('text', 'nama', 'nama', 'Name', 'validate', 'txt-black bold', $d1[$name], 'required');
							?>
						</div>

						<div class="input-field">
							<?php
							$fun->inputField('text', 'tlp', 'tlp', 'Phone Number', 'validate', 'txt-black bold', '', 'required');
							?>
						</div>

						<div class="input-field">
							<?php
							$fun->inputField('password', 'pin', 'pin', 'PIN', 'validate', 'txt-black bold', null, 'required');
							?>
						</div>

						<div class="input-field">
							<?php
							$fun->inputField('password', 'pin2', 'pin2', 'PIN Confirmation', 'validate', 'txt-black bold', null, 'required');
							?>
						</div>
						<?php
						$fun->buttonField('button', 'toconfirm', 'toconfirm', 'btn btn-large width-100 waves-effect waves-light primary-color m-t-30 borad-20', $btn_lbl);

						$fun->buttonField('submit', 'confirm', 'confirm', 'btn btn-large width-100 waves-effect waves-light primary-color m-t-30 borad-20 hide', 'to Process');
					}
					else { ?>
						<div class="input-field">
							<?php
							$fun->inputField('text', 'nama', 'nama', 'Name', 'validate', 'txt-black bold', $d1[$name], 'required');
							?>
						</div>

						<div class="input-field">
							<?php
							$fun->inputField('text', 'ktp', 'ktp', 'Card Identification Number', 'validate', 'txt-black bold', $d1['ktp'], 'required');
							?>
						</div>

						<div class="input-field">
							<?php
							$fun->inputField('number', 'tlp', 'tlp', 'Phone Number', 'validate', 'txt-black bold', $d1[$tlp]);
							?>
						</div>

						<div class="row">
							<div class="col s12 input-field">
								<?php
								$fun->inputField('text', 'alamat', 'alamat', 'Address', 'validate', 'txt-black bold', $d1[$alamat]);
								?>
							</div>

							<div class="col s6 input-field">
								<span class="bold">Province</span> : <br>
								<span><?php echo $d2['nama_prov']; ?></span> <span class="bold underline txt-link" id="<?php echo $fun->Enlink('showprovinsi') ?>">[choose]</span>
								<select class="browser-default txt-black hide" name="<?php echo $fun->Enlink('prov'); ?>" id="<?php echo $fun->Enlink('prov'); ?>" required>
									<?php
									$s3 = "SELECT * from 11amasterprovince order by id_prov";
									$q3 = mysqli_query($fun->getConnection(), $s3) or die(mysqli_error($fun->getConnection()));
									while($d3 = mysqli_fetch_array($q3)) { ?>
										<option value="<?php echo $fun->Enval($d3['id_prov']); ?>" <?php //if($d3['id_prov'] == $d2['id_prov']) echo 'selected'; ?> ><?php echo $d3['nama_prov']; ?></option>
										<?php
									}
									?>
								</select>
							</div>

							<div class="col s6 input-field">
								<span class="bold">Area</span><br>
								<span><?php echo $d2['nama_kabkot']; ?></span> <span class="bold underline txt-link" id="<?php echo $fun->Enlink('showkabkot') ?>">[choose]</span>
								<select class="browser-default txt-black hide" name="<?php echo $fun->Enlink('kabkot'); ?>" id="<?php echo $fun->Enlink('kabkot'); ?>" required>
									<option value="">Choose province first...</option>
								</select>
							</div>
						</div>
						<?php

						$fun->buttonField('submit', 'confirm', 'confirm', 'btn btn-large width-100 waves-effect waves-light primary-color m-t-30 borad-20', $btn_lbl);
					}
				}
				else {
					$fun->inputField('email', 'email', 'email', 'Email', 'validate', 'txt-black bold');

					$fun->buttonField('submit', 'confirm', 'confirm', 'btn btn-large width-100 waves-effect waves-light primary-color m-t-30 borad-20', $btn_lbl);
				}
				?>

			</form>
		</div>

		<?php
		if($d1[$aktif] != 2) { ?>
			<div class="row m-t-50 center">
			
				<?php /* <div class="col s12 m-t-30">
					<h3 class="uppercase txt-black">Ubah : </h3>
				</div>*/ ?>

				<div class="col s6 txt-white">
					<?php
					$fun->buttonField('button', 'btnpass', 'btnpass', 'btn btn-large width-100 waves-effect waves-light primary-color m-t-30 borad-20', 'Change Password');
					?>
				</div>

				<div class="col s6 txt-white">
					<?php
					$fun->buttonField('button', 'btnpin', 'btnpin', 'btn btn-large width-100 waves-effect waves-light primary-color m-t-30 borad-20', 'Change PIN');
					?>
				</div>

			</div>

			<div class="form-inputs hide" id="<?php echo $fun->Enlink('form_changepass'); ?>">
				<form action="<?php echo 'process/12updatepassword.php?'.$fun->setIDParam('tipe', 'chp1'); ?>" method="post">
					
					<div class="input-field">
						<?php
						$fun->inputField('password', 'pass', 'pass', 'Password', 'validate', 'txt-black', null, 'required');
						?>
					</div>

					<div class="input-field">
						<?php
						$fun->inputField('password', 'passconf', 'passconf', 'Password Confirmation', 'validate', 'txt-black', null, 'required');
						?>
					</div>

					<?php
					$fun->buttonField('submit', 'confirm', 'confirm', 'btn btn-large width-100 waves-effect waves-light primary-color m-t-30 borad-20', 'Update Password');
					?>

				</form>
			</div>

			<div class="form-inputs hide" id="<?php echo $fun->Enlink('form_changepin'); ?>">
				<form action="<?php echo 'process/12updatepassword.php?'.$fun->setIDParam('tipe', 'chp2'); ?>" method="post">

					<div class="input-field">
						<?php
						$fun->inputField('password', 'pin', 'pass', 'PIN', 'validate', 'txt-black', null, 'required');
						?>
					</div>

					<div class="input-field">
						<?php
						$fun->inputField('password', 'pin2', 'passconf', 'PIN Confirmation', 'validate', 'txt-black', null, 'required');
						?>
					</div>

					<?php
					$fun->buttonField('submit', 'confirm', 'confirm', 'btn btn-large width-100 waves-effect waves-light primary-color m-t-30 borad-20', 'Update PIN');
					?>

				</form>
			</div>
			<?php
		}
		?>

		<?php /* 
		<div class="txt-white m-t-10">
			<?php
			$fun->linkTo('logout.php', 'Keluar', 'btn btn-large width-100 waves-effect waves-light primary-color borad-20');
			?>
		</div>
		*/ ?>

	</div>

</div>

<script type="text/javascript">
	$('#toBack').click(function() {
		window.location.href = "?";
	});

	<?php
	if($session == 'member') {
		if($d1[$aktif] == 2) { ?>

			$("<?php echo '#'.$fun->Enlink('toconfirm') ?>").click(function() {
				var pin1 = $("<?php echo '#'.$fun->Enlink('pin') ?>").val();
				var pin2 = $("<?php echo '#'.$fun->Enlink('pin2') ?>").val();

				if(pin1 == pin2) {
					$(document).ready(function() {
						$("<?php echo '#'.$fun->Enlink('confirm') ?>").click();
					});
				}
				else {
					window.location.href = "<?php echo '?'.$fun->setIDParam('status', 'PIN and Confirmation are not match!'); ?>"
				}

			});
			<?php
		}
	}
	?>

	$("<?php echo '#'.$fun->Enlink('pin') ?>").keyup(function(){
		var pin = $("<?php echo '#'.$fun->Enlink('pin') ?>").val();

		if(pin.length > 6 || pin.length < 0) {
			alert('PIN length allowed is only 6!');
			$("<?php echo '#'.$fun->Enlink('pin') ?>").val('');
		}
	});

	$("<?php echo '#'.$fun->Enlink('pin') ?>").keydown(function(){
		var pin = $("<?php echo '#'.$fun->Enlink('pin') ?>").val();

		if(pin.length > 6 || pin.length < 0) {
			alert('PIN length allowed is only 6!');
			$("<?php echo '#'.$fun->Enlink('pin') ?>").val('');
		}
	});

	$("<?php echo '#'.$fun->Enlink('pin2') ?>").keyup(function(){
		var pin = $("<?php echo '#'.$fun->Enlink('pin2') ?>").val();

		if(pin.length > 6 || pin.length < 0) {
			alert('PIN length allowed is only 6!');
			$("<?php echo '#'.$fun->Enlink('pin2') ?>").val('');
		}
	});

	$("<?php echo '#'.$fun->Enlink('pin2') ?>").keydown(function(){
		var pin = $("<?php echo '#'.$fun->Enlink('pin2') ?>").val();

		if(pin.length > 6 || pin.length < 0) {
			alert('PIN length allowed is only 6!');
			$("<?php echo '#'.$fun->Enlink('pin2') ?>").val('');
		}
	});

	$("<?php echo '#'.$fun->Enlink('showprovinsi') ?>").click(function() {
		if($("<?php echo '#'.$fun->Enlink('prov') ?>").hasClass("hide")) {
			$("<?php echo '#'.$fun->Enlink('prov') ?>").removeClass("hide");
		}
		else {
			$("<?php echo '#'.$fun->Enlink('prov') ?>").addClass("hide");
		}
	});

	$("<?php echo '#'.$fun->Enlink('showkabkot') ?>").click(function() {
		if($("<?php echo '#'.$fun->Enlink('kabkot') ?>").hasClass("hide")) {
			$("<?php echo '#'.$fun->Enlink('kabkot') ?>").removeClass("hide");
		}
		else {
			$("<?php echo '#'.$fun->Enlink('kabkot') ?>").addClass("hide");
		}
	});

	$("<?php echo '#'.$fun->Enlink('btnpass') ?>").click(function() {
		if($("<?php echo '#'.$fun->Enlink('form_changepass') ?>").hasClass("hide")) {
			$("<?php echo '#'.$fun->Enlink('form_changepass') ?>").removeClass("hide");
			$("<?php echo '#'.$fun->Enlink('form_changepin') ?>").addClass("hide");
		}
		else {
			$("<?php echo '#'.$fun->Enlink('form_changepass') ?>").addClass("hide");
		}
	});

	$("<?php echo '#'.$fun->Enlink('btnpin') ?>").click(function() {
		if($("<?php echo '#'.$fun->Enlink('form_changepin') ?>").hasClass("hide")) {
			$("<?php echo '#'.$fun->Enlink('form_changepin') ?>").removeClass("hide");
			$("<?php echo '#'.$fun->Enlink('form_changepass') ?>").addClass("hide");
		}
		else {
			$("<?php echo '#'.$fun->Enlink('form_changepin') ?>").addClass("hide");
		}
	});
</script>

<?php
$fun->setOneSession('error_content', '', 2);
?>
