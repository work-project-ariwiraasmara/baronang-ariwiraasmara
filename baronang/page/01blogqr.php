<div id="content" class="grey-blue login">

	<div id="toolbar" class="tool-login primary-color animated fadeindown">

	</div>

	<div class="login-form animated fadeinup delay-2 z-depth-1 txt-black">
		<div class="form-inputs">

			<h1 class="uppercase txt-black">log in</h1>

				<div class="row">

	                <div class="col s12 center">
	                	<canvas id="webcodecam-canvas" class="width-100"></canvas>
				        <div class="scanner-laser laser-rightBottom" style="opacity: 0.5;"></div>
				        <div class="scanner-laser laser-rightTop" style="opacity: 0.5;"></div>
				        <div class="scanner-laser laser-leftBottom" style="opacity: 0.5;"></div>
				        <div class="scanner-laser laser-leftTop" style="opacity: 0.5;"></div>
	                </div>

	                <div class="col s12 center">
			        	<div id="result">
			               	<img id="scanned-img" src="">
			                <div class="center">
			                    <p class="hide" id="scanned-QR"></p>
			                </div>
			            </div>
	                </div>

	                <div class="col s6">
	                	<button title="Mulai Pindai" class="borad-20 waves-effect waves-light btn-large primary-color width-100 m-t-30" id="play" type="button"><i class="ion-play"></i></button>
	                </div>

	                <div class="col s6">
	                	<button title="Hentikan Streaming" class="borad-20 waves-effect waves-light btn-large primary-color width-100 m-t-30" id="stop" type="button" disabled><i class="ion-stop"></i></button>
	                </div>

	                <div class="col s12 hide">
	                	<select class="browser-default" id="camera-select"></select>
	                	<input type="text" name="code" id="result-code" value="">
	                </div>

		    	</div>

		    	<button type="button" class="borad-20 waves-effect waves-light btn-large primary-color width-100 m-t-30" name="<?php echo $fun->Enlink('ok'); ?>" id="<?php echo $fun->Enlink('ok'); ?>">OK</button>

		    	<div class="txt-white">
		    		<?php // echo $fun->Enval('7000200000000004'); 4e7a41774d4449774d4441774d4441774d4441774e413d3d ?>
		    		<a href="?" class="borad-20 waves-effect waves-light btn-large primary-color width-100 m-t-10"><i class="ion-android-arrow-back"></i></a>
		    	</div>
		    	
		</div>
	</div>

</div>

<script type="text/javascript">
	$("<?php echo '#'.$fun->Enlink('ok'); ?>").click(function() {
		qr = $("#result-code").val();
		//alert(qr);

		window.location.href = "<?php echo 'process/01bqrprocess.php?qr'; ?>="+qr;
	});

	$('#play').click(function(){
    	$('#result-code').val();
    	$('#stop').attr('disabled', false);
    });

    $('#stop').click(function(){
        $('#form-camera').addClass('hide');
    });
</script>