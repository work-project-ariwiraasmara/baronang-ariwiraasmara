<?php
$session = $fun->getValookie('session');

if($session == 'member') {
	$s1 = "SELECT * from 22bmastermember where mastermemberid='".$fun->getValookie('id')."'";
	$name = "membername";
}
else {
	$s1 = "SELECT * from 23dmastertenant where mastertenantid='".$fun->getValookie('id')."'";
	$name = "tenantname";
}

$q1 = mysqli_query($fun->getConnection(), $s1) or die(mysqli_error($fun->getConnection()));
$d1 = mysqli_fetch_array($q1);

$s3 = "SELECT * from 21masterlocation where masterlocationid='".$d1['locationid']."'";
$q3 = mysqli_query($fun->getConnection(), $s3) or die(mysqli_error($fun->getConnection()));
$r3 = mysqli_num_rows($q3);

if($r3 > 0) { 
	$d3 = mysqli_fetch_array($q3); 
	$locname = $d3['locationname'];
}
else { 
	$locname = 'null';
}

$s2 = "SELECT * from 31bmastercarduser where cardid like '7000%' and  useridactivated='".$fun->getValookie('id')."' and activationstatus = '1' limit 1";
$q2 = mysqli_query($fun->getConnection(), $s2) or die(mysqli_error($fun->getConnection()));
$r2 = mysqli_num_rows($q2);

if($r2 > 0) { 
	$d2 = mysqli_fetch_array($q2); 
	$cardid = $d2['cardid'];
	$spoint = "SELECT points from 23amastercard where mastercardid='".$cardid."'";
	$qpoint = mysqli_query($fun->getConnection(), $spoint) or die(mysqli_error($fun->getConnection()));
	$dpoint = mysqli_fetch_array($qpoint);
}
else { 
	$cardid = 0;
}
?>
<div class="txt-white m-t-30" id="<?php echo $fun->Enlink('personal'); ?>" style="margin-bottom: -65px;">
	
	<a href="<?php echo '#'.$fun->Enlink('card1-'.$cardid); ?>" class="modal-trigger">
		<div class="card_container">
			
			<div class="card_company">
					<?php 
					$scomp = "SELECT companyname from 12mastercompany where companyid='70' and status = '1'";
					$qcomp = mysqli_query($fun->getConnection(), $scomp) or die(mysqli_error($fun->getConnection()));
					$dcomp = mysqli_fetch_array($qcomp);

					echo strtoupper($dcomp['companyname']); ?>
				</div>

			<img src="images/1.png" id="imgcard">

			<div class="card_identity">
				<?php
				echo $d1[$name].'<br>';
				echo $locname.'<br>';
				if ($dpoint['points'] > 0){
					echo $cardid.'<br>';
					echo $dpoint['points'].' Points';
				}
				else {
					echo $cardid;
				}
				?>
			</div>

			<div class="card_logo">
				<img src="images/icon/Logo-whitefish-icon.png" style="width: 40px; height: 20px;">
			</div>

		</div>
	</a>
	
	<?php
	//7100 [COKLAT]
	$scard1 = "SELECT * from 31bmastercarduser where cardid like '7100%' and useridactivated='".$fun->getValookie('id')."' and activationstatus = '1'";
	$qcard1 = mysqli_query($fun->getConnection(), $scard1) or die(mysqli_error($fun->getConnection()));
	while($dcard1 = mysqli_fetch_array($qcard1)) { 

		$sctype = "SELECT cardtype from 23amastercard where mastercardid='".$dcard1['cardid']."'";
		$qctype = mysqli_query($fun->getConnection(), $sctype) or die(mysqli_error($fun->getConnection()));
		$dctype = mysqli_fetch_array($qctype);

		$sloc = "SELECT * from 21masterlocation where masterlocationid='".substr($dcard1['cardid'],0,9).'0000000'."'";
		$qloc = mysqli_query($fun->getConnection(), $sloc) or die(mysqli_error($fun->getConnection()));
		$dloc = mysqli_fetch_array($qloc);
		
		$spoint = "SELECT points from 23amastercard where mastercardid='".$dcard1['cardid']."'";
		$qpoint = mysqli_query($fun->getConnection(), $spoint) or die(mysqli_error($fun->getConnection()));
		$dpoint = mysqli_fetch_array($qpoint);
		
		$scomp = "SELECT companyname from 12mastercompany where companyid='71' and status = '1'";
		$qcomp = mysqli_query($fun->getConnection(), $scomp) or die(mysqli_error($fun->getConnection()));
		$dcomp = mysqli_fetch_array($qcomp);

		?>
		<a href="<?php echo '#'.$fun->Enlink('card2-'.$dcard1['cardid']); ?>" class="modal-trigger">
			<div class="m-t-30 card_container">
				<div class="card_sky">
					<img src="images/logo skyparking 1.png" style="width: 45px; height: 45px;">
				</div>

				<div class="card_company">
					<?php echo strtoupper($dcomp['companyname']); ?>
				</div>

				<img src="images/2.png" id="imgcard">

				<div class="card_identity">
					<?php
					echo $d1[$name].'<br>';
					echo $dctype['cardtype'].'<br>';
					if ($dpoint['points'] > 0){
						echo $dcard1['cardid'].'<br>';
						echo $fun->FormatNumber($dpoint['points']).' Points';
					}
					else {
						echo $dcard1['cardid'];
					}
					?>
				</div>
					
				<div class="card_logo">
					<img src="images/icon/Logo-whitefish-icon.png" style="width: 40px; height: 20px;">
				</div>
			</div>
		</a>
		<?php
	}
	
	//7100 [MERAH] Card Product Owned 
	$scard5 = "SELECT * from 31bmastercarduser where cardid like '7100%' and useridactivated='".$fun->getValookie('id')."' and activationstatus = '1'";
	$qcard5 = mysqli_query($fun->getConnection(), $scard5) or die(mysqli_error($fun->getConnection()));
	while($dcard5 = mysqli_fetch_array($qcard5)) {

		$scard4 = "SELECT * from 31amastercardproductowned where cardid ='".$dcard5['cardid']."' and active = '1'";
		$qcard4 = mysqli_query($fun->getConnection(), $scard4) or die(mysqli_error($fun->getConnection()));
		while($dcard4 = mysqli_fetch_array($qcard4)) {

			$expdate = new datetime($dcard4['expirationdate']);
			$svhl = "SELECT b.vehicletype as vehicle, b.productimage as productimage,
						b.locationid as locationid 
						from 31amastercardproductowned as a 
						inner join 22amasterproduct as b 
							on a.productowned = b.masterproductid
						where a.serialnumber='".$dcard4['serialnumber']."'";
			//echo $svhl.'<br><br>';
			$qvhl = mysqli_query($fun->getConnection(), $svhl) or die(mysqli_error($fun->getConnection()));
			$dvhl = mysqli_fetch_array($qvhl);
			
			$locid = $dvhl['locationid'];
			$sloc = "SELECT * from 21masterlocation where masterlocationid = '".$locid."'";
			$qloc = mysqli_query($fun->getConnection(), $sloc) or die(mysqli_error($fun->getConnection()));
			$dloc = mysqli_fetch_array($qloc);
		?>
		<a href="<?php echo '#'.$fun->Enlink('card5-'.$dcard4['serialnumber']); ?>" class="modal-trigger">
			<div class="card_container m-t-10">
				<div class="card_sky">
					<img src="images/logo skyparking 1.png" style="width: 45px; height: 45px;">
				</div>

				<div class="card_company">
					<?php
					if($dloc['image'] == '' || empty($dloc['image'])) {
						echo strtoupper($dloc['locationname']);
					}
					else { ?>
						<img src="<?php echo 'images/location/'.$dloc['image']; ?>" style="width: 45px; height: 45px;">
						<?php
					}
					?>
				</div>

				<img src="images/5.png" id="imgcard">
				
				<div class="card_identity">
					<?php
					echo $d1[$name].'<br>';
					echo $dcard4['serialnumber'].'<br>';
					echo date_format($expdate,'d/m/Y');
					?>
				</div>

				<div class="card_logo">
					<?php
					if($dvhl['productimage'] == '' || empty($dvhl['productimage'])) {?>
						<div class="bold m-b-10"><?php echo $dvhl['vehicle']; ?></div>
						<?php
					}
					else { ?>
						<div style="margin-bottom: 10px;">
							<img src="<?php echo 'images/icon/'.$dvhl['productimage']; ?>" style="width: 40px; height: 25px;">
						</div>
						<?php
					}
					?>

					<img src="images/icon/Logo-whitefish-icon.png" style="width: 40px; height: 20px;">
				</div>
			</div>
		</a>
		<?php
		}
	}


	$scard2 = "SELECT * from 31bmastercarduser where cardid like '7101%' and useridactivated='".$fun->getValookie('id')."' and activationstatus = '1'";
	$qcard2 = mysqli_query($fun->getConnection(), $scard2) or die(mysqli_error($fun->getConnection()));
	while($dcard2 = mysqli_fetch_array($qcard2)) { 

		$svhl = "SELECT b.vehicletype as vehicle, b.productimage as productimage
					from 31amastercardproductowned as a 
					inner join 22amasterproduct as b 
						on a.productowned = b.masterproductid
					where a.cardid='".$dcard2['cardid']."'";
		$qvhl = mysqli_query($fun->getConnection(), $svhl) or die(mysqli_error($fun->getConnection()));
		$dvhl = mysqli_fetch_array($qvhl);
		
		$locid = substr($dcard2['cardid'],0,9).'0000000';
		$sloc = "SELECT * from 21masterlocation where masterlocationid = '".$locid."'";
		$qloc = mysqli_query($fun->getConnection(), $sloc) or die(mysqli_error($fun->getConnection()));
		$dloc = mysqli_fetch_array($qloc);
		?>
		<a href="<?php echo '#'.$fun->Enlink('card3-'.$dcard2['cardid']); ?>" class="modal-trigger">
			<div class="card_container m-t-10">
				<div class="card_sky">
					<img src="images/logo skyparking 1.png" style="width: 45px; height: 45px;">
				</div>

				<div class="card_company">
					<?php
					if($dloc['image'] == '' || empty($dloc['image'])) {
						echo strtoupper($dloc['locationname']);
					}
					else { ?>
						<img src="<?php echo 'images/location/'.$dloc['image']; ?>" style="width: 45px; height: 45px;">
						<?php
					}
					?>
				</div>

				<img src="images/3.png" id="imgcard">
				
				<div class="card_identity">
					<?php
					echo $d1[$name].'<br>';
					echo $dcard2['cardid'];
					?>
				</div>

				<div class="card_logo">
					<?php
					if($dvhl['productimage'] == '' || empty($dvhl['productimage'])) {?>
						<div class="bold m-b-10"><?php echo $dvhl['vehicle']; ?></div>
						<?php
					}
					else { ?>
						<div style="margin-bottom: 10px;">
							<img src="<?php echo 'images/icon/'.$dvhl['productimage']; ?>" style="width: 40px; height: 25px;">
						</div>
						<?php
					}
					?>

					<img src="images/icon/Logo-whitefish-icon.png" style="width: 40px; height: 20px;">
				</div>
			</div>
		</a>
		<?php
	}


	$scard3 = "SELECT * from 31bmastercarduser where cardid like '7104%' and useridactivated='".$fun->getValookie('id')."' and activationstatus = '1'";
	$qcard3 = mysqli_query($fun->getConnection(), $scard3) or die(mysqli_error($fun->getConnection()));
	while($dcard3 = mysqli_fetch_array($qcard3)) {

		$svhl = "SELECT b.vehicletype as vehicle, b.productimage as productimage
					from 31amastercardproductowned as a 
					inner join 22amasterproduct as b 
						on a.productowned = b.masterproductid
					where a.cardid='".$dcard3['cardid']."'";
		$qvhl = mysqli_query($fun->getConnection(), $svhl) or die(mysqli_error($fun->getConnection()));
		$dvhl = mysqli_fetch_array($qvhl);
		
		$locid = '7101'.substr($dcard3['cardid'],4,5).'0000000';
		$sloc = "SELECT * from 21masterlocation where masterlocationid = '".$locid."'";
		$qloc = mysqli_query($fun->getConnection(), $sloc) or die(mysqli_error($fun->getConnection()));
		$dloc = mysqli_fetch_array($qloc);
		
		$srfid = "SELECT rfid from 31dmastercardrfid where cardid = '".$dcard3['cardid']."'";
		$qrfid = mysqli_query($fun->getConnection(), $srfid) or die(mysqli_error($fun->getConnection()));
		$drfid = mysqli_fetch_array($qrfid);
		?>
		<a href="<?php echo '#'.$fun->Enlink('card4-'.$dcard3['cardid']); ?>" class="modal-trigger">
			<div class="card_container m-t-10">
				<div class="card_sky">
					<img src="images/logo skyparking 1.png" style="width: 45px; height: 45px;">
				</div>

				<div class="card_company">
					<?php
					if($dloc['image'] == '' || empty($dloc['image'])) {
						echo strtoupper($dloc['locationname']);
					}
					else { ?>
						<img src="<?php echo 'images/location/'.$dloc['image']; ?>" style="width: 45px; height: 45px;">
						<?php
					}
					?>
				</div>

				<img src="images/4.png" id="imgcard">
				
				<div class="card_identity">
					<?php
					echo $d1[$name].'<br>';
					echo $dcard3['cardid'].'<br>';
					echo $drfid['rfid'];
					?>
				</div>

				<div class="card_logo">
					<?php
					if($dvhl['productimage'] == '' || empty($dvhl['productimage'])) {?>
						<div class="bold m-b-10"><?php echo $dvhl['vehicle']; ?></div>
						<?php
					}
					else { ?>
						<div style="margin-bottom: 10px;">
							<img src="<?php echo 'images/icon/'.$dvhl['productimage']; ?>" style="width: 40px; height: 25px;">
						</div>
						<?php
					}
					?>

					<img src="images/icon/Logo-whitefish-icon.png" style="width: 40px; height: 20px;">
				</div>
			</div>
		</a>
		<?php
	}
	?>
</div>
