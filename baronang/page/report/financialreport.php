<?php
//if(isset($_GET['b'])) { $b = $funct->Denval(@$_GET['b']); } else { $b = date('m'); }
//if(isset($_GET['y'])) { $y = $funct->Denval(@$_GET['y']); } else { $y = date('Y'); }
$d = date('d');
$m = date('m');
$y = date('Y');
$numdays = date('t');
$x = 1;
?>

<div class="animated fadeinup delay-1">
	<div class="page-content">

		<h1 class="title txt-black" id="title"></h1>
		<h3 class="title txt-black"><?php echo date('F Y'); ?></h3>

		
			<div class="table-responsive m-t-30">
				
				<table>
					<thead>
						<tr>
							<th class="center" rowspan="2" >Date</th>
							<th class="center" colspan="2">Top Up</th>
							<th class="center">Purchase Product</th>
						</tr>
						<tr>
							<th class="center">Rp.</th>
							<th class="center">Points</th>
							<th class="center">Points</th>
						</tr>
					</thead>

					<tbody>
						<?php
						$totalcredit = 0;
						$totaldebet = 0;
						$totalpointcr = 0;
						$totalpointdb = 0;
						
						while ($x <= $d) {
							$day = str_pad($x, 2, "0", STR_PAD_LEFT);
							$date = $day.'-'.$m.'-'.$y;
							$tgl = $y.'-'.$m.'-'.$day;
							$stopup = "Select sum(credit) as credit, sum(pointcredit) as pointcredit 
							from 31cmastercardactivity where activitycode like '1%' and datecreated between
							'$tgl 00:00:00' and '$tgl 23:59:59'";
							$qtopup = mysqli_query($fun->getConnection(), $stopup) or die(mysqli_error($fun->getConnection()));
							$dtopup = mysqli_fetch_array($qtopup);
							
							$spurchase = "Select sum(debet) as debet, sum(pointdebet) as pointdebet 
							from 31cmastercardactivity where activitycode like '2%' and datecreated between
							'$tgl 00:00:00' and '$tgl 23:59:59'";
							$qpurchase = mysqli_query($fun->getConnection(), $spurchase) or die(mysqli_error($fun->getConnection()));
							$dpurchase = mysqli_fetch_array($qpurchase);
							?>
							<tr>
								<td><?php echo $date; ?></td>
								<td style="text-align: right;"><?php echo $fun->FormatRupiah($dtopup['credit']); ?></td>
								<td style="text-align: right;"><?php echo $fun->FormatNumber($dtopup['pointcredit']); ?></td>
								<td style="text-align: right;"><?php echo $fun->FormatNumber($dpurchase['pointdebet']); ?></td>
							</tr>
						<?php 
							$totalpointcr = $totalpointcr + $dtopup['pointcredit'];
							$totalcredit = $totalcredit + $dtopup['credit'];
							$totalpointdb = $totalpointdb + $dpurchase['pointdebet'];
							$totaldebet = $totaldebet + $dpurchase['debet'];
							$x++;
							}
							?>
						
					</tbody>

					<tfoot>
						<tr>
							<th style="text-align: right;">Total</th>
							<th style="text-align: right;"><?php echo $fun->FormatRupiah($totalcredit); ?></th>
							<th style="text-align: right;"><?php echo $fun->FormatNumber($totalpointcr); ?></th>
							<th style="text-align: right;"><?php echo $fun->FormatNumber($totalpointdb); ?></th>
						</tr>
					</tfoot>

				</table>

			</div>

	</div>
</div>

<script type="text/javascript">
	$('#toBack').click(function() {
		window.location.href = "<?php echo '?#'.$fun->Enlink('skymanagement'); ?>";
	});
</script>
