<div id="content" class="grey-blue login">

	<div id="toolbar" class="tool-login primary-color animated fadeindown">

	</div>

	<div class="login-form animated fadeinup delay-2 z-depth-1 txt-black">
		<div class="form-inputs">
			<h1 class="uppercase txt-black">Forgot Password</h1>

			<form action="process/11checkmemberemailregistered.php" method="post">
				
				<div class="input-field">
					<?php
					$fun->inputField('email', 'email', 'email', 'Email', 'validate', 'txt-black', null, 'required');
					?>
				</div>

				<div class="row">
					<div class="col s6 txt-white">
						<?php
						$fun->linkTo('?', '<i class="ion-android-arrow-back"></i>', 'btn btn-large width-100 waves-effect waves-light primary-color m-t-30 borad-20');
						?>
					</div>

					<div class="col s6">
						<?php
						$fun->buttonField('submit', 'confirm', 'confirm', 'btn btn-large width-100 waves-effect waves-light primary-color m-t-30 borad-20', 'OK');
						?>
					</div>
				</div>

			</form>
		</div>

		<?php
		require('footer.php');
		?>
	</div>

	<?php
	if( isset($_GET[$fun->Enlink('status')]) ) {
		$status = $fun->getIDParam('status');

		$fun->Notrifger('toStatus', 'statusNotif', 'Hasil Status Success / Fail');

		if($status == 'fail') {
			$fun->notifSnackBar('toSnackbar', 'SnackBar()', $fun->getSession('error_content', 2));
			/*$fun->Notif('statusNotif', $fun->getSession('error_content', 2), 'warn');
			?>
			<script type="text/javascript">
				$(document).ready(function() {
					$("<?php echo '#'.$fun->Enlink('toStatus'); ?>").click();
				});
			</script>
			<?php
			*/
		}
		//echo 'error_content : '.$fun->ENID('status');
	}
	?>
</div>

<?php
$fun->setOneSession('error_content', '', 2);
?>