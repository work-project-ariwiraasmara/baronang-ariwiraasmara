<!-- Left Sidebar -->
<ul id="slide-out-left" class="side-nav collapsible">

    <li>
        <div class="sidenav-header primary-color">

            <div class="nav-avatar">
                <img class="circle avatar" src="images/icon/No-Image-Icon.png" alt="">
                <div class="avatar-body">
                    <a href="?pg=profil"><b style="color: white;"><?php echo $fun->getValookie('membername'); ?></b></a>
                    <p>Online</p>
                </div>
            </div>

        </div>
    </li>

    <li><a href="?" class="no-child txt-black"><i class="ion-android-home"></i> Home</a></li>
    <li><a href="?pg=profil" class="no-child txt-black"><i class="ion-android-person"></i> Profil</a></li>

    <?php
    /*
    <li><a href="?pg=location" class="no-child txt-black"><i class="ion-document-text"></i> Location</a></li>
    <li><a href="?pg=product" class="no-child txt-black"><i class="ion-document-text"></i> Product</a></li>
    */ ?>

    <li><a href="logout.php" class="no-child txt-black"><i class="ion-log-out"></i> Logout</a></li>

    <li class="m-t-30"><?php require('footer.php'); ?></li>

</ul>
<!-- End of Sidebars -->
