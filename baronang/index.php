<?php
require('util/koneksi.php');
if(isset($_GET['download'])) {

	if( isset($_COOKIE[$fun->ENID('login')]) ) {
		$filePath = "download/";

		// DOWNLOAD EXCEL {
			if( @$_GET['download'] == 'excel' ) {
				require('plugins/phpexcel/Classes/PHPExcel.php');

				$pg = @$_GET['pg']; 
				$sb = @$_GET['sb'];

				$objPHPExcel = new PHPExcel();
			    $objPHPExcel->getProperties()->setCreator("Baronang");
			    //$objPHPExcel->getProperties()->setTitle("Laporan Sky Parking");

			    // set autowidth
			    for($col = 'A'; $col !== 'Z'; $col++) {
			        $objPHPExcel->getActiveSheet()
			            ->getColumnDimension($col)
			            ->setAutoSize(true);
			    }

			    //sheet 2
			    $objWorkSheet = $objPHPExcel->createSheet(0);


			    if(strtolower($pg) == 'location' || strtolower($pg) == 'lokasi') {

				}
				
				else if(strtolower($pg) == 'product' || strtolower($pg) == 'produk') {

				}

				else if(strtolower($pg) == 'tenant') {
					if(strtolower($sb) == 'list' || strtolower($sb) == 'index' || strtolower($sb) == '' || is_null($sb) || empty($sb) ) {
						require('page/tenant/download_list.php');
					}

					else if(strtolower($sb) == 'template') {
						require('page/tenant/download_template.php');
					}

					else {
						header('location: ?pg=tenant');
					}

					// download ke client
				    header('Content-type: application/vnd.ms-excel');
				    header('Content-Disposition: attachment; filename="'.$GLOBALS['fileName'].'"');
				    $objWriter->save('php://output');

				    return $filePath.$GLOBALS['fileName'];
				}

				else if(strtolower($pg) == 'company' || strtolower($pg) == 'perusahaan') {

				}

				else {
					header('location: www.baronang.com'.$_SERVER['PHP_SELF']);
				}

			}
		// }


		// DOWNLOAD PDF {
			else if( @$_GET['download'] == 'pdf' ) {
				$pg = @$_GET['pg']; 
				$sb = @$_GET['sb'];

				if(strtolower($pg) == 'location' || strtolower($pg) == 'lokasi') {

				}

				else if(strtolower($pg) == 'product' || strtolower($pg) == 'produk') {

				}

				else if(strtolower($pg) == 'tenant') {
					if(strtolower($sb) == 'member') {
						require('page/tenant/download_pdf_akun_member.php');
					}

					else {
						header('location: ?pg=tenant&'.$fun->setIDParam('lokasi', $fun->getIDParam('lokasi')));
					}
				}

				else if(strtolower($pg) == 'company' || strtolower($pg) == 'perusahaan') {

				}

				else {
					header('location: ?');
				}

			}
		// }

		//
		else {
			echo 'tidak ada pilihan download..';
			header('location: www.baronang.com'.$_SERVER['PHP_SELF']);
		}
	}
	else {
		echo 'tidak ada sesi login..<br>';
		//echo $fun->getValookie('login');
		header('location:?');
	}

}
else { ?>
	<!DOCTYPE html>
	<html lang="id-ID">
		<head>
			<?php
			if(isset($_COOKIE['login'])) { echo "<title>Baronang Login</title>"; }
			else { echo "<title>Baronang</title>"; }
			?>
			<meta content="IE=edge" http-equiv="x-ua-compatible">
			<meta content="initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" name="viewport">
			<meta content="yes" name="apple-mobile-web-app-capable">
			<meta content="yes" name="apple-touch-fullscreen">

			<link href='images/icon/Logo-fish-icon.png' type='image/x-icon'/>
			<link href='images/icon/Logo-fish-icon.png' rel='shortcut icon'/>

			<!-- Fonts -->
			<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
		    <link href="https://fonts.googleapis.com/css2?family=Philosopher&display=swap" rel="stylesheet">
		    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100&display=swap" rel="stylesheet">
		    <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@300&display=swap" rel="stylesheet">
		    <link href="https://fonts.googleapis.com/css2?family=Playfair+Display&display=swap" rel="stylesheet">
		    <link href="https://fonts.googleapis.com/css2?family=Manrope&display=swap" rel="stylesheet">

			<!-- Icons -->
			<link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" media="all" rel="stylesheet" type="text/css">
			
			<!-- Styles -->
			<link href="css/keyframes.css" rel="stylesheet" type="text/css">
			<link href="css/materialize.min.css" rel="stylesheet" type="text/css">
			<link href="css/swiper.css" rel="stylesheet" type="text/css">
			<link href="css/swipebox.min.css" rel="stylesheet" type="text/css">
			<link href="css/style.css" rel="stylesheet" type="text/css">
			<link href="css/custom.css" rel="stylesheet" type="text/css">

			<style type="text/css">
				.goog-te-gadget-simple a {text-decoration: none !important;}
				.goog-te-gadget-icon, .goog-te-banner-frame.skiptranslate, .goog-te-gadget-simple img { display: none !important; }
				body { top: 0px !important; }
			</style>

		   <script type="text/javascript" src="js/jquery-2.1.0.min.js"></script>
		</head>
		<body>
			<div class="m-scene" id="main">
				<?php
				if( isset($_COOKIE[$fun->ENID('login')]) ) {

					$logexpire = time();
					$exptime = $fun->getValookie('exptime');

					$session = $fun->getValookie('session');
					//$session = "member";
					$id = $fun->getValookie('field_id');

					if($session == 'member') {
						$s1 = "SELECT * from 22bmastermember where mastermemberid='".$fun->getValookie('id')."'";
						$aktif = 'active';
					}
					else {
						$s1 = "SELECT * from 23dmastertenant where mastertenantid='".$fun->getValookie('id')."'";
						$aktif = 'status';
					}

					//$s1 = "SELECT * from 22bmastermember where mastermemberid='".$fun->getValookie('id')."'";;
					$q1 = mysqli_query($fun->getConnection(), $s1) or die(mysqli_error($fun->getConnection()));
					$d1 = mysqli_fetch_array($q1);

					require('plugins/qrcode/qrlib.php');

					//barcode
					include('plugins/barcode128/BarcodeGenerator.php');
					include('plugins/barcode128/BarcodeGeneratorPNG.php');
					include('plugins/barcode128/BarcodeGeneratorSVG.php');
					include('plugins/barcode128/BarcodeGeneratorJPG.php');
					include('plugins/barcode128/BarcodeGeneratorHTML.php');

					function QRCode($path, $kode, $cp, $matrixPointSize) {
						//QR code

						$PNG_TEMP_DIR = dirname(__FILE__).DIRECTORY_SEPARATOR.'qrcard'.DIRECTORY_SEPARATOR;
						//$PNG_WEB_DIR = 'qrcard/'.$memberid.'/';

						$errorCorrectionLevel = 'H';
						//$matrixPointSize = 500;

						//$kode = '1000100010001004';
						$filename = $path.$kode.'.png';
						if (!file_exists($filename)){
							QRcode::png($cp, $filename, $errorCorrectionLevel, $matrixPointSize, 2);
						}

						$generator = new Picqer\Barcode\BarcodeGeneratorPNG();
						$resource = 'data:image/png;base64,'.base64_encode($generator->getBarcode($kode, $generator::TYPE_CODE_128));
					}

					function cipher($cardno) {
						$ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
						$iv = openssl_random_pseudo_bytes($ivlen);
						$ciphertext_raw = openssl_encrypt($cardno, $cipher, '70007000', $options=OPENSSL_RAW_DATA, $iv);
						$hmac = hash_hmac('sha256', $ciphertext_raw, '70007000', $as_binary=true);
						$ciphertext = base64_encode( $iv.$hmac.$ciphertext_raw );
						return $ciphertext;
					}

					if(!file_exists('qrcard/'.$fun->getValookie('id').'/'))  { 
						mkdir('qrcard/'.$fun->getValookie('id').'/');
						QRCode('qrcard/'.$fun->getValookie('id').'/', $fun->getValookie('id'), $fun->encrypt($fun->getValookie('id'), $fun->getK()), 5);
					}

					if( !is_dir('process/qrtenant_'.$fun->getValookie('id')) ) { 
						mkdir('process/qrtenant_'.$fun->getValookie('id'));
					}
					?>

					<div id="content" class="page txt-black">
						<?php
						$pg = $fun->GET('pg');
						$sb = $fun->GET('sb');
						?>

						<div id="toolbar" class="primary-color">
							<?php
							if( @$_GET['pg'] != '' || !empty(@$_GET['pg']) || !is_null(@$_GET['pg']) ) { ?>
								<a href="#" class="open-left" id="toBack">
						        	<i class="ion-android-arrow-back"></i>
						        </a>
								<?php
							}
							?>

							<span>
								<a href="?"><img src="images/icon/Logo-White.png" class="m-l-10 m-t-10" style="width: 200px; height: 40px; z-index: -1; float: left;"></a>
							</span>
							
							<a href="<?php echo '#'.$fun->Enlink('options'); ?>" class="open-right modal-trigger">
								<i class="ion-android-more-vertical"></i>
							</a>

						</div>

						<?php
						if($d1[$aktif] > 1) { ?>
							<div class="" id="<?php echo $fun->Enlink('profil') ?>">
								<?php require('page/21memberprofile.php'); ?>
							</div>

							<?php
							//if( !isset($_GET[$fun->Enlink('status')]) ) { ?>
								<a href="<?php echo '#'.$fun->Enlink('firsttime'); ?>" id="<?php echo $fun->Enlink('toFirstTime'); ?>" class="hide modal-trigger">to First Time</a>

								<div class="modal borad-20" id="<?php echo $fun->Enlink('firsttime'); ?>">
									<div class="modal-content choose-date center txt-black bold">
										<?php
										if($session == 'member') { ?>
											Welcome to Baronang<br><br>
											To be able to use this application, please fill in your personal information first
											<?php
										}
										else { ?>
											Welcome to Baronang<br><br>
											To be able to use this application, link this account with your personal member account
											<?php 
										}
										?>
										
										<?php
										$fun->buttonField('button', 'ok', 'ok', 'btn btn-large borad-20 m-t-30 primary-color waves-effect waves-light width-100 modal-close', 'OK');
										?>
									</div>
								</div>
								<?php
							//}

						}
						else { 

							if($session == 'member') {

								if(strtolower($pg) == '' || strtolower($pg) == 'home' || strtolower($pg) == 'beranda') { ?>
									<div style="margin-bottom: 100px;">
										<div class="" id="<?php echo $fun->Enlink('home') ?>">
											<?php require('page/02usermainpage.php'); ?>
										</div>

										<div class="hide" id="<?php echo $fun->Enlink('profil') ?>">
											<?php require('page/21memberprofile.php'); ?>
										</div>
									</div>
									
									<?php
									require('page/menu-bottom.php');

									//$s2 = "SELECT * from 23amastercard where memberid='".$d1[$id]."'";
									//$q2 = mysqli_query($fun->getConnection(), $s2) or die(mysqli_error($fun->getConnection()));
									//$d2 = mysqli_fetch_array($q2);
									?>

									<div class="modal borad-20" id="<?php echo $fun->Enlink('qr'); ?>">
										<div class="modal-content choose-date txt-black modal-close">
												
											<img src="<?php echo 'qrcard/'.$fun->getValookie('id').'/'.$fun->getValookie('id').'.png'; ?>">

											<div class="center m-t-30">
												<?php echo $fun->getValookie('id'); ?>
											</div>

										</div>
									</div>
									<?php
								}

								else if(strtolower($pg) == 'location' || strtolower($pg) == 'lokasi') {
									if(strtolower($sb) == 'list' || strtolower($sb) == 'index' || strtolower($sb) == '' || is_null($sb) || empty($sb) ) {
										require('page/location/list.php');
										$fun->setTitle('Location');
										$fun->setDocTitle();
									}
									else if(strtolower($sb) == 'add') {
										require('page/location/add.php');
										$fun->setTitle('Add Location');
										$fun->setDocTitle();
									}
									else if(strtolower($sb) == 'edit') {
										require('page/location/edit.php');
										$fun->setTitle('Edit Location');
										$fun->setDocTitle();
									}

									else {
										require('404.php');
									}
								}

								else if(strtolower($pg) == 'product' || strtolower($pg) == 'produk') {
									if(strtolower($sb) == 'list' || strtolower($sb) == 'index' || strtolower($sb) == '' || is_null($sb) || empty($sb) ) {
										require('page/product/list.php');
										$fun->setTitle('Product');
										$fun->setDocTitle();
									}
									else if(strtolower($sb) == 'approval') {
										require('page/product/approval.php');
										$fun->setTitle('Product Approval');
										$fun->setDocTitle();
									}
									else if(strtolower($sb) == 'add') {
										require('page/product/add.php');
										$fun->setTitle('Add Product');
										$fun->setDocTitle();
									}
									else if(strtolower($sb) == 'edit') {
										require('page/product/edit.php');
										$fun->setTitle('Edit Product');
										$fun->setDocTitle();
									}

									else {
										require('404.php');
									}
								}
								
								else if(strtolower($pg) == 'generalproduct' || strtolower($pg) == 'generalproduk') {
									if(strtolower($sb) == 'list' || strtolower($sb) == 'index' || strtolower($sb) == '' || is_null($sb) || empty($sb) ) {
										require('page/generalproduct/list.php');
										$fun->setTitle('Product');
										$fun->setDocTitle();
									}
									else if(strtolower($sb) == 'approval') {
										require('page/generalproduct/approval.php');
										$fun->setTitle('Product Approval');
										$fun->setDocTitle();
									}
									else if(strtolower($sb) == 'add') {
										require('page/generalproduct/add.php');
										$fun->setTitle('Add Product');
										$fun->setDocTitle();
									}
									else if(strtolower($sb) == 'edit') {
										require('page/generalproduct/edit.php');
										$fun->setTitle('Edit Product');
										$fun->setDocTitle();
									}

									else {
										require('404.php');
									}
								}

								else if(strtolower($pg) == 'compliment') {
									if(strtolower($sb) == 'list' || strtolower($sb) == 'index' || strtolower($sb) == '' || is_null($sb) || empty($sb) ) {
										require('page/compliment/list.php');
										$fun->setTitle('Complimentary Product');
										$fun->setDocTitle();
									}
									else if(strtolower($sb) == 'add') {
										require('page/compliment/add.php');
										$fun->setTitle('Add Complimentary Product');
										$fun->setDocTitle();
									}
									else if(strtolower($sb) == 'edit') {
										require('page/compliment/edit.php');
										$fun->setTitle('Edit Complimentary Product');
										$fun->setDocTitle();
									}

									else {
										require('404.php');
									}
								}

								else if(strtolower($pg) == 'tenant') {
									if(strtolower($sb) == 'list' || strtolower($sb) == 'index' || strtolower($sb) == '' || is_null($sb) || empty($sb) ) {
										require('page/tenant/list.php');
										$fun->setTitle('Tenant');
										$fun->setDocTitle();
									}
									else if(strtolower($sb) == 'approval') {
										require('page/tenant/approval.php');
										$fun->setTitle('Tenant Approval');
										$fun->setDocTitle();
									}
									else if(strtolower($sb) == 'add') {
										require('page/tenant/add.php');
										$fun->setTitle('Add Tenant');
										$fun->setDocTitle();
									}
									else if(strtolower($sb) == 'edit') {
										require('page/tenant/edit.php');
										$fun->setTitle('Edit Tenant');
										$fun->setDocTitle();
									}

									else {
										require('404.php');
									}
								}
								
								else if(strtolower($pg) == 'report') {
									if(strtolower($sb) == 'list' || strtolower($sb) == 'index' || strtolower($sb) == '' || is_null($sb) || empty($sb) ) {
										require('page/report/list.php');
										$fun->setTitle('Reports');
										$fun->setDocTitle();
									}
									else if(strtolower($sb) == 'financial') {
										require('page/report/financialreport.php');
										$fun->setTitle('Financial Report');
										$fun->setDocTitle();
									}
									else if(strtolower($sb) == 'sales') {
										require('page/report/sales.php');
										$fun->setTitle('Membership Sales');
										$fun->setDocTitle();
									}
									else if(strtolower($sb) == 'edit') {
										require('page/report/edit.php');
										$fun->setTitle('Edit Tenant');
										$fun->setDocTitle();
									}

									else {
										require('404.php');
									}
								}

								else if(strtolower($pg) == 'company' || strtolower($pg) == 'perusahaan') {
									if(strtolower($sb) == 'list' || strtolower($sb) == 'index' || strtolower($sb) == '' || is_null($sb) || empty($sb) ) {
										//header('location: ?#'.$fun->Enlink('baronang') );
										require('page/company/list.php');
										$fun->setTitle('Company');
										$fun->setDocTitle();
									}
									else if(strtolower($sb) == 'add') {
										require('page/company/add.php');
										$fun->setTitle('Add Company');
										$fun->setDocTitle();
									}
									else if(strtolower($sb) == 'edit') {
										require('page/company/edit.php');
										$fun->setTitle('Edit Company');
										$fun->setDocTitle();
									}

									else {
										require('404.php');
									}
								}

								else if(strtolower($pg) == 'profil' || strtolower($pg) == 'profile') {
									if(strtolower($sb) == 'list' || strtolower($sb) == 'index' || strtolower($sb) == '' || is_null($sb) || empty($sb) ) {
										require('page/21memberprofile.php');
									}

									else {
										require('404.php');
									}
								}

								else if(strtolower($pg) == 'lang' || strtolower($pg) == 'language' || strtolower($pg) == 'bahasa' || strtolower($pg) == 'bhs') {
									if(strtolower($sb) == 'list' || strtolower($sb) == 'index' || strtolower($sb) == '' || is_null($sb) || empty($sb) ) {
										require('page/bahasa/list.php');
										$fun->setTitle('Language');
										$fun->setDocTitle();
									}
									else if(strtolower($sb) == 'add') {
										require('page/bahasa/add.php');
										$fun->setTitle('Add Language');
										$fun->setDocTitle();
									}
									else if(strtolower($sb) == 'edit') {
										require('page/bahasa/edit.php');
										$fun->setTitle('Edit Language');
										$fun->setDocTitle();
									}

									else {
										require('404.php');
									}
								}

								else if(strtolower($pg) == 'card' || strtolower($pg) == 'kartu') {
									if(strtolower($sb) == 'productsowned' || strtolower($sb) == 'products' || strtolower($sb) == 'products_owned') {
										require('page/card/productsowned.php');
										$fun->setTitle('Products Owned');
										$fun->setDocTitle();
									}
									
									else if(strtolower($sb) == 'list' || strtolower($sb) == 'index' || strtolower($sb) == 'activity' || strtolower($sb) == 'cardactivity' || strtolower($sb) == 'card_activity' || strtolower($sb) == '' || is_null($sb) || empty($sb) ) {
										require('page/card/activity.php');
										$fun->setTitle('Card\'s Activity');
										$fun->setDocTitle();
									}

									else if(strtolower($sb) == 'financial' || strtolower($sb) == 'history' || strtolower($sb) == 'financialhistory' || strtolower($sb) == 'financial_history') {
										require('page/card/financialhistory.php');
										$fun->setTitle('Financial History');
										$fun->setDocTitle();
									}

									else if(strtolower($sb) == 'purchase' || strtolower($sb) == 'product' || strtolower($sb) == 'purchaseproduct' || strtolower($sb) == 'purchase_product') {
										require('page/card/product.php');
										$fun->setTitle('Card\'s Products');
										$fun->setDocTitle();
									}

									else if(strtolower($sb) == 'topup' || strtolower($sb) == 'top_up') {
										require('page/card/topup.php');
										$fun->setTitle('Top Up');
										$fun->setDocTitle();
									}

									else {
										require('404.php');
									}
								}

								else if(strtolower($pg) == 'pin' || strtolower($pg) == 'pinconfirm' || strtolower($pg) == 'pin_confirm' || strtolower($pg) == 'pin_confirmation' || strtolower($pg) == 'pinconfirmation') {
									require('page/01cpinconfirmation.php');
									$fun->setTitle('Top Up');
									$fun->setDocTitle();
								}

								else if(strtolower($pg) == 'shop' || strtolower($pg) == 'belanja' || strtolower($pg) == 'shopping') {
									if(strtolower($sb) == 'list' || strtolower($sb) == 'index' || strtolower($sb) == '' || is_null($sb) || empty($sb) ) {
										require('page/shop/list.php');
										$fun->setTitle('Shopping Cart');
										$fun->setDocTitle();
									}
									else if(strtolower($sb) == 'payment' || strtolower($sb) == 'method_payment' || strtolower($sb) == 'methodpayment' || strtolower($sb) == 'pembayaran' || strtolower($sb) == 'metode_pembayaran' || strtolower($sb) == 'metodepembayaran' ) {
										require('page/shop/payment.php');
										$fun->setTitle('Payment');
										$fun->setDocTitle();
									}
									else if(strtolower($sb) == 'invoice' || strtolower($sb) == 'strukbelanja' || strtolower($sb) == 'struk') {
										require('page/shop/invoice.php');
										$fun->setTitle('Invoice');
										$fun->setDocTitle();
									}

									else {
										require('404.php');
									}
								}

								else {
									require('404.php');
								}
							
							}
							else { ?>
								<div class="animated fadeinup delay-1">
									<div class="page-content txt-black">
										<h1 class="uppercase bold txt-black center">Please sign in with personal account!</h1>
									</div>
								</div>
								<?php
							}

							?>
							
							<div class="modal borad-20" id="<?php echo $fun->Enlink('applycard'); ?>">
								<div class="modal-content choose-date txt-black">
									<p><h1 class="txt-black center bold">Apply Card</h1></p>
									<p>Choose Company
										<select class="browser-default" id="applycard_company" name="applycard_company">
											<?php
											$scomp = "SELECT * from 12mastercompany where status = '1'";
											$qcomp = mysqli_query($fun->getConnection(), $scomp) or die(mysqli_error($fun->getConnection()));
											while($dcomp = mysqli_fetch_array($qcomp)) { ?>
												<option value="<?php echo $dcomp['companyid']; ?>"><?php echo $dcomp['companyname']; ?></option>
												<?php 
											} 
											?>
										</select>
									</p>
									<p>
										<button class="btn btn-large width-100 primary-color waves-effect waves-light borad-20" id="btnapplyok">OK</button>
									</p>
								</div>
							</div>
							
							<?php

						}

						$fun->linkTo('#'.$fun->Enlink('logout'), 'toLogoutForced', 'modal-trigger hide', 'toLogoutForced');

						require('page/index_setting_modal_popup.php');
						require('page/02ctab4_personal_modal_popup.php');
						require('page/02ctab3_tenantmanagement_modal_popup.php');
						?>

					</div>

					<!-- Google Translator -->
					<script type="text/javascript">
						<?php
						if($session == 'member') { ?>
							function googleTranslateElementInit() {
							 	new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'en,ar,zh-CN,zh-TW,de,hi,id,ja,ko,ru,es', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
							}
							<?php
						}
						else { ?>
							function googleTranslateElementInit() {
							 	new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
							}
							<?php
						}
						?>
					
					</script>
					
					<script type="text/javascript">
						var cardid_tenantmanagement = 0;
						var cardid_shareaccess = 0;

						$("<?php echo '#'.$fun->Enlink('btnformlink_ok'); ?>").click(function() {
							//alert(cardid_tenantmanagement);
							var email = $("<?php echo '#'.$fun->Enlink('emaillinkcard'); ?>").val();
							
							$.ajax({
			                    url : "process/ajax/ajax_link_cardemail.php",
			                    type : 'POST',
			                    data: { 
			                        id: cardid_tenantmanagement,
			                        email: email 
			                    },
			                    success : function(data) {
			                    	cardid_tenantmanagement = 0;
			                    	$("<?php echo '#'.$fun->Enlink('emaillinkcard'); ?>").val('');
			                    	//console.log(data);
			                    	window.location.href = "<?php echo '?'.$fun->setIDParam('status', 'Successful Link Card!'); ?>";
			                    },
			                    error : function(){
			                        alert('Terjadi kesalahan dalam jaringan!\nGagal Link Kartu!\nSilahkan cek koneksi jaringan dan coba lagi!');
			                        return false;
			                    }
			                });
						});

						$("<?php echo '#'.$fun->Enlink('btnformshareaccess_ok'); ?>").click(function(){
							var email = $("<?php echo '#'.$fun->Enlink('emailshareaccess'); ?>").val();

							$.ajax({
			                    url : "process/ajax/ajax_link_shareaccess_email.php",
			                    type : 'POST',
			                    data: { 
			                        id: cardid_shareaccess,
			                        email: email 
			                    },
			                    success : function(data) {
			                    	cardid_shareaccess = 0;
			                    	$("<?php echo '#'.$fun->Enlink('emailshareaccess'); ?>").val('');
			                    	//console.log(data);
			                    	window.location.href = "<?php echo '?'.$fun->setIDParam('status', 'Successful Share Access!'); ?>";
			                    },
			                    error : function(){
			                        alert('Terjadi kesalahan dalam jaringan!\nGagal Link Kartu!\nSilahkan cek koneksi jaringan dan coba lagi!');
			                        return false;
			                    }
			                });
						});

						$("#btnapplyok").click(function() {
							var cmpy = $("#applycard_company :selected").val();
							//alert(company);
							window.location.href = "process/14applycard.php?cmpy="+cmpy;
						});

						function confirmDelete(name, tbl, fld, val, url) {
							if(confirm("Anda yakin ingin menghapus data ini? [" + name + "]")) {
								$.ajax({
			                        url : "process/ajax/ajax_deletedata.php",
			                        type : 'POST',
			                        data: { 
			                        	tbl: tbl,
			                        	fld: fld,
			                        	val: val
			                        },
			                        success : function(data) {
			                            //alert("Data: " + name + " dengan:\nTabel: " + tbl + "\nField: " + fld + "\nValue: " + val);
			                        	window.location.href = url;
			                        	//console.log(data);
			                        },
			                        error : function(){
			                            alert('Terjadi kesalahan dalam jaringan!\nGagal Menghapus Data!\nSilahkan cek koneksi jaringan dan coba lagi!');
			                            return false;
			                        }
			                    });
							}
							else {
								alert("Data Tidak Jadi Dihapus!");
							}
						}

						<?php
						if($logexpire > $exptime) { ?>
							$(document).ready(function() {
								$("<?php echo '#'.$fun->Enlink('toLogoutForced'); ?>").click();
							});
							<?php
						}
						?>

						$("<?php echo '#'.$fun->Enlink('prov'); ?>").change(function() {
							var id = $("<?php echo '#'.$fun->Enlink('prov'); ?> :selected").val();
							//alert(id);

							$.ajax({
			                    url : "process/ajax/ajax_kabkot.php",
			                    type : 'POST',
			                    data: { 
			                    	id: id
			                    },
			                    success : function(data) {
			                    	$("<?php echo '#'.$fun->Enlink('kabkot'); ?>").html(data);
			                    },
			                    error : function(){
			                        alert('Terjadi kesalahan dalam jaringan!\nTidak dapat mengambil data Kabupaten / Kota!\nSilahkan cek koneksi jaringan dan coba lagi!');
			                       	return false;
			                    }
			                });
						});

						$("<?php echo '#'.$fun->Enlink('lokasi'); ?>").change(function() {
							var id = $("<?php echo '#'.$fun->Enlink('lokasi'); ?> :selected").val(); 

							$.ajax({
			                    url : "process/ajax/ajax_lokasi_product.php",
			                    type : 'POST',
			                    data: { 
			                    	id: id
			                    },
			                    success : function(data) {
			                    	$("<?php echo '#'.$fun->Enlink('product'); ?>").html(data);
			                    },
			                    error : function(){
			                        alert('Terjadi kesalahan dalam jaringan!\nTidak dapat mengambil data Product!\nSilahkan cek koneksi jaringan dan coba lagi!');
			                       	return false;
			                    }
			                });
						});

						<?php 
						if($d1[$aktif] > 1) { ?>
							$(document).ready(function() {
								$("<?php echo '#'.$fun->Enlink('toFirstTime'); ?>").click();
							});
							<?php
						}
						else { ?>
							var home = 1;
							$("<?php echo '#'.$fun->Enlink('menuhome') ?>").click(function(){
								home = 1;
								if(home == 1) {
									profil = 0;
									$("<?php echo '#'.$fun->Enlink('home') ?>").removeClass("hide");
									$("<?php echo '#'.$fun->Enlink('profil') ?>").addClass("hide");
								}
								else {
									$("<?php echo '#'.$fun->Enlink('home') ?>").addClass("hide");
								}
							});

							var profil = 0;
							$("<?php echo '#'.$fun->Enlink('menuprofil') ?>").click(function(){
								profil = 1;
								if(profil == 1) {
									home = 0;
									$("<?php echo '#'.$fun->Enlink('profil') ?>").removeClass("hide");
									$("<?php echo '#'.$fun->Enlink('home') ?>").addClass("hide");
								}
								else {
									$("<?php echo '#'.$fun->Enlink('profil') ?>").addClass("hide");
								}
							});
							<?php
						}
						?>

						$("<?php echo '#'.$fun->Enlink('menukamera'); ?>").click(function(){
							$(document).ready(function() {
								$("#play").click();
								$("<?php echo '#'.$fun->Enlink('qrdetail'); ?>").val('');
							});
						});

						$("#play").click(function() {
							$("#stop").prop('disabled', false);
							$("<?php echo '#'.$fun->Enlink('btn_checkqr'); ?>").removeClass('hide');
							$("<?php echo '#'.$fun->Enlink('btn_submit'); ?>").removeClass('hide');
						});

						$(document).ready(function() {
							$("#result-code").keyup(function(){
								var res = $("#result-code").val();

								if(res != '' || res != null) {
									$("#frame_webcodecam").addClass("hide");
								}
							});

							$("#result-code").change(function(){
								var res = $("#result-code").val();

								if(res != '' || res != null) {
									$("#frame_webcodecam").addClass("hide");
								}
							});
						});
						
						$("<?php echo '#'.$fun->Enlink('btn_checkqr'); ?>").click(function() {
							var qr   = $("#result-code").val();
							var type = "detail";

							if(qr == '' || qr == null) {
								alert('QR Can\'t be empty! Please, scan the qr code first!');
							}
							else {
								$(document).ready(function() {
									$("#stop").click();
								});

								$.ajax({
				                    url : "process/01bqrprocess.php",
				                    type : 'POST',
				                    data: { 
				                    	qr: qr,
				                    	type: type
				                    },
				                    success : function(data) {
				                    	$("<?php echo '#'.$fun->Enlink('qrdetail'); ?>").removeClass('hide');
				                    	$("<?php echo '#'.$fun->Enlink('qrdetail'); ?>").html(data);
				                    },
				                    error : function(){
				                        alert('Terjadi kesalahan dalam jaringan!\nTidak dapat mengambil data Product!\nSilahkan cek koneksi jaringan dan coba lagi!');
				                       	return false;
				                    }
				                });

							}
						});

						$("<?php echo '#'.$fun->Enlink('btn_submit'); ?>").click(function() {
							var qr = $("#result-code").val();
							window.location.href = "process/01bqrprocess.php?type=link&qr="+qr;
						});
					</script>

					<?php
				}

				else {

					$pg = $fun->GET('pg');
					$sb = $fun->GET('sb');

					if(strtolower($pg) == '' || strtolower($pg) == 'home' || strtolower($pg) == 'beranda' || strtolower($pg) == 'login') {
						require('page/01baronang.php');
						$fun->setTitle('Baronang');
						$fun->setDocTitle();
					}

					else if(strtolower($pg) == 'forgotpass' || strtolower($pg) == 'forgotpassword' || strtolower($pg) == 'forgot' ||
					   strtolower($pg) == 'changepass' || strtolower($pg) == 'changepassword' || strtolower($pg) == 'change' || 
					   strtolower($pg) == 'gantipass'  || strtolower($pg) == 'gantipassword'  || strtolower($pg) == 'ganti'  ||
					   strtolower($pg) == 'lupapass'   || strtolower($pg) == 'lupapassword'   || strtolower($pg) == 'lupa') {
						require('page/11changepasswordenteremail.php');
						$fun->setTitle('Forgot Password');
						$fun->setDocTitle();
					}

					else if(strtolower($pg) == 'updatepass' || strtolower($pg) == 'updatepassword' || strtolower($pg) == 'ubahpass' || strtolower($pg) == 'ubahpassword') {
						require('page/12changepasswordnewpassword.php');
						$fun->setTitle('Update Password');
						$fun->setDocTitle();
					}

					else if(strtolower($pg) == 'register' || strtolower($pg) == 'daftar') {
						require('page/13memberregister.php');
						$fun->setTitle('Register');
						$fun->setDocTitle();
					}

					else if(strtolower($pg) == 'aktivasi' || strtolower($pg) == 'activation' || strtolower($pg) == 'aktif' || strtolower($pg) == 'activ' || strtolower($pg) == 'activate' || strtolower($pg) == 'verfikasi' || strtolower($pg) == 'verification') {
						require('process/13bconfirmregister.php');
						$fun->setTitle('Register');
						$fun->setDocTitle();
					}

					else if(strtolower($pg) == 'logqr' || strtolower($pg) == 'qr' || strtolower($pg) == 'loginqr') {
						require('page/01blogqr.php');
						$fun->setTitle('Sign In');
						$fun->setDocTitle();
					}

					else {
						require('404.php');
						$fun->setTitle('Cloud Parking - Error: 404!');
						$fun->setDocTitle();
					}

					?>
					<script type="text/javascript">
					function googleTranslateElementInit() {
					 	new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'en,ar,zh-CN,zh-TW,de,hi,id,ja,ko,ru,es', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
					 	//new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'en,ar,zh-CN,zh-TW,nl,fr,de,hi,id,ja,ko,ru,es', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
					}
					</script>
					<?php

				}

				if( isset($_GET[$fun->Enlink('status')]) ) {
					$fun->notifSnackBar('toSnackbar', 'SnackBar()', $fun->getIDParam('status', 2));
				}
				?>

			</div>

			<?php
			require('pinconfirmation.php');
			?>

			<!-- Google Translator -->
			<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

			<!--  -->
			<script type="text/javascript">
				// TOAST
				<?php
				if( isset($_GET[$fun->Enlink('status')]) ) { ?>
					$(document).ready(function() {
						$("<?php echo '#'.$fun->Enlink('toSnackbar'); ?>").click();
					});
					<?php
				} 
				?>

				function SnackBar() {
					var x = document.getElementById("snackbar");
					x.className = "show";
					setTimeout(function() { 
				  		x.className = x.className.replace("show", ""); 
				  	}, 3000);
				}

				// TREEVIEW
				var toggler = document.getElementsByClassName("caret");
				var i;

				for (i = 0; i < toggler.length; i++) {
				  toggler[i].addEventListener("click", function() {
				    this.parentElement.querySelector(".nested").classList.toggle("active");
				    this.classList.toggle("caret-down");
				  });
				}

				//
				//console.log("<?php //echo 'PHP_SELF : '.$_SERVER['PHP_SELF']; ?>")
			</script>

			<script src="js/jquery.swipebox.min.js"></script>
			<script src="js/materialize.min.js"></script>
			<script src="js/swiper.min.js"></script>
			<script src="js/jquery.mixitup.min.js"></script>
			<script src="js/masonry.min.js"></script>
			<script src="js/chart.min.js"></script>
			<script src="js/functions.js"></script>

			<!-- QR Barcode Reader -->
			<script type="text/javascript" src="plugins/webCodeCamJs/js/qrcodelib.js"></script>
			<script type="text/javascript" src="plugins/webCodeCamJs/js/webcodecamjquery.js"></script>
			<script type="text/javascript" src="plugins/webCodeCamJs/js/mainjquery.js"></script>
		</body>
	</html>
	<?php
}
?>
