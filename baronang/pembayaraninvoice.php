<!DOCTYPE html>
<html>
<head>
	<title>Pembayaran Invoice</title>
</head>
<body>

	<form action="" method="post">
		<input type="text" name="invoice" id="invoice" placeholder="Nomor Invoice.."><br>
		<input type="submit" name="ok" id="ok" value="OK">
	</form>

	<?php
	include('util/koneksi.php');
	if(isset($_POST['ok'])) {
		echo '<br>';
		$invoice = @$_POST['invoice'];
		$s1 = "SELECT 	a.invoiceid as invoice,
						a.datecreate as datecreate,
						a.memberid as memberid,
						a.method_payment as method_payment,
						a.vanumber as vanumber,
						a.total as total,
						a.status as status,
						Sum(b.qty) as qty
		 		from 23fproductinvoice as a 
		 		inner join 23eproductshop as b
		 			on a.invoiceid = b.invoiceid
		 		where invoiceid='$invoice'";
		$q1 = mysqli_query($fun->getConnection(), $s1) or die(mysqli_error($fun->getConnection()));
		$d1 = mysqli_fetch_array($q1);

		echo 'Tanggal Dibuat : '.$d1['datecreate'].'<br>';
		echo 'Metode Pembayaran : '.$d1['method_payment'].'<br>';
		echo 'VA Number : '.$d1['vanumber'].'<br>';
		echo 'Jumlah Total : '.$d1['qty'].'<br>';
		echo 'Total : '.number_format($d1['total'],0,',','.').'<br><br>';

		$s2 = "SELECT * from 23eproductshop where invoiceid='$invoice'";
		$q2 = mysqli_query($fun->getConnection(), $s2) or die(mysqli_error($fun->getConnection()));
		while($d2 = mysqli_fetch_array($q2)) {
			echo 'Produk ID : '.$d2['productid'].'<br>';
			echo 'Jumlah : '.$d2['qty'].'<br>';
			echo 'Tanggal Mulai : '.$d2['datestart'].'<br><br>';
			
			//Product Owned
			$ProductID = $d2['productid'];
			$CardID = $d2['cardid'];
			$Start = $d2['datestart'];
			$StartDate = new datetime($d2['datestart']);
			$StartFrom = strtotime($d2['datestart']);
			$sproduct = "SELECT * from 22amasterproduct where masterproductid = '".$ProductID."' and active = '1'";
			$qproduct = mysqli_query($fun->getConnection(), $sproduct) or die(mysqli_error($fun->getConnection()));
			$dproduct = mysqli_fetch_array($qproduct);
			$prodcate = $dproduct['category'];
			$qty = $dproduct['validity_days'];
			Switch ($prodcate){
				case '01':
					$interval = $qty.' days';
					$NewDate = date_add($StartDate, date_interval_create_from_date_string($interval));
					$ExpDate = date_format(date_time_set($NewDate,23,59,59), 'Y-m-d H:i:s');
					break;
					  
				case '02':
					$d = date("Y-m-d H:i:s", strtotime("Sunday"));
					if (($d > $StartDate) and ($qty == 1)){
						$NewDate = new datetime($d);
						$ExpDate = date_format(date_time_set($NewDate,23,59,59), 'Y-m-d H:i:s');
					}
					elseif (($d < $StartDate) and ($qty == 1)){
						$week = date("W", $StartFrom);
						$year = date("Y", $StartFrom);
						$ExpDate = date("Y-m-d 23:59:59", strtotime($year.'W'.str_pad($week, 2, 0, STR_PAD_LEFT).'7'));
					}
					else {
						$interval = ($qty-1).' weeks';
						$EndDay = date_add($StartDate, date_interval_create_from_date_string($interval));
						$Start = strtotime(date_format($EndDay, "Y-m-d"));
						$week = date("W", $Start);
						$year = date("Y", $Start);
						$ExpDate = date("Y-m-d 23:59:59", strtotime($year.'W'.str_pad($week, 2, 0, STR_PAD_LEFT).'7'));
					}
					break;
					  
				case '03':
					$interval = $qty.' weeks';
					$NewDate = date_add($StartDate, date_interval_create_from_date_string($interval));
					$NewDate2 = date_add($NewDate, date_interval_create_from_date_string("-1 days"));
					$ExpDate = date_format(date_time_set($NewDate2,23,59,59), 'Y-m-d H:i:s');
					break;
					  
				case '04':
					$m = date("m", $StartFrom);
					if (($m == date('m')) and ($qty == 1)){
						$EndDate = date_format(date_time_set($StartDate,23,59,59), 'Y-m-d H:i:s');
						$ExpDate = date("Y-m-t H:i:s", strtotime($EndDate));
					}
					else {
						$interval = ($qty-1).' months';
						$EndDay = date_add($StartDate, date_interval_create_from_date_string($interval));
						$EndDate = date_format(date_time_set($EndDay,23,59,59), 'Y-m-d H:i:s');
						$ExpDate = date("Y-m-t H:i:s", strtotime($EndDate));
					}	
					break;
					  
				case '05':
					$interval = $qty.' months';
					$NewDate = date_add($StartDate, date_interval_create_from_date_string($interval));
					$NewDate2 = date_add($NewDate, date_interval_create_from_date_string("-1 days"));
					$ExpDate = date_format(date_time_set($NewDate2,23,59,59), 'Y-m-d H:i:s');
					break;
					  
			}
			$quota = $dproduct['quota'];
			$counter = $dproduct['counter'] + 1;
			//$newcounter = $counter + 1;
			$serialnumber = substr($ProductID, 0, 14).str_pad($counter, 6, "0", STR_PAD_LEFT);
			
			echo 'Counter : '.$counter.'<br>';
			echo 'Serial Number : '.$serialnumber.'<br>';
			
			$date = date('Y-m-d H:i:s');
			
			$sserial = "INSERT into 22fproductserialnumber values('$serialnumber', '$ProductID', '$date',1)";
			$qserial = mysqli_query($fun->getConnection(), $sserial) or die(mysqli_error($fun->getConnection()));
			
			$sprodowned = "INSERT into 31amastercardproductowned values
			('','$date', '$CardID', '$ProductID', '$Start', '$ExpDate','$serialnumber',1,0,0)";
			$qprodowned = mysqli_query($fun->getConnection(), $sprodowned) or die(mysqli_error($fun->getConnection()));
			//$dprodowned = mysqli_fetch_array($qprodowned);
			
			if ($quota == $counter){
				$scounterprod = "Update 22amasterproduct set counter = '".$counter."', active = 2 where masterproductid = '".$ProductID."'";
				}
			else {
				$scounterprod = "Update 22amasterproduct set counter = '".$counter."' where masterproductid = '".$ProductID."'";
				}
			$qcounterprod = mysqli_query($fun->getConnection(), $scounterprod) or die(mysqli_error($fun->getConnection()));	
				
			//CardActivity
			$locid = $dproduct['locationid'];
			$MemberID = $d2['memberid'];
			$price = $dproduct['productprice'];
			$pricepoint = $price / 1000;
			$cardActivity = '7105'.date('y').'205'.date('m');
			$scardact1 = "SELECT count(*) as jmlactivity from 31cmastercardactivity where activitylogid like '".$cardActivity."%'";
			$qcardact1 = mysqli_query($fun->getConnection(), $scardact1) or die(mysqli_error($fun->getConnection()));
			$dcardact1 = mysqli_fetch_array($qcardact1);
			$rcardact1 = $dcardact1['jmlactivity'];
				  
			$scardact2 = "SELECT activitylogid from 31cmastercardactivity where activitylogid like '".$cardActivity."%' order by activitylogid desc limit 1";
			$qcardact2 = mysqli_query($fun->getConnection(), $scardact2) or die(mysqli_error($fun->getConnection()));
			$dcardact2 = mysqli_fetch_array($qcardact2);
						  
			if($rcardact1 > 0) {
				$noactivity = (int)substr($dcardact2['activitylogid'], -5) + 1;
				}
			else {
				$noactivity = '00001';
				}
			$activityID = substr($cardActivity, 0, 11).str_pad($noactivity, 5, "0", STR_PAD_LEFT);
			$sactivity = "SELECT description from 15activitycode where activitycodeid = '205'";
			$qactivity = mysqli_query($fun->getConnection(), $sactivity) or die(mysqli_error($fun->getConnection()));
			$dactivity = mysqli_fetch_array($qactivity);
			$description = $dactivity['description'];
			$scardact3 = "INSERT into 31cmastercardactivity values
			('$activityID', '$CardID', '$date', '$description', $price, 0, $pricepoint, 0, '205', 
			'$ProductID', '$MemberID', '$locid', '-')";
			$qcardact3 = mysqli_query($fun->getConnection(), $scardact3) or die(mysqli_error($fun->getConnection()));
			
		}
		
	}
	?>
	
</body>
</html>
