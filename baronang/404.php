<div id="toolbar" class="primary-color">
    <a href="javascript:history.back()" class="open-left">
        <i class="ion-android-arrow-back <?php echo $co_primary_teks; ?>"></i>
    </a>
    <span class="title bold txt-white">Error: 404!</span>
</div>

<div class="animated fadeinup delay-1">
	<div class="page-content">
		<h1 class="uppercase bold italic center txt-black m-t-50">404!<br>Halaman Tidak Ditemukan!</h1>

		<?php
		require('footer.php');
		?>
	</div>
</div>