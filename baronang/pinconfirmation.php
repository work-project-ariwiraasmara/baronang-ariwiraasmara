<script type="text/javascript">
	$("<?php echo '#'.$fun->Enlink('btn_pinconfirm') ?>").click(function(){
		var id  = $("<?php echo '#'.$fun->Enlink('idttrigger') ?>").val();
		var pin = $("<?php echo '#'.$fun->Enlink('cfmpin') ?>").val();

		if(pin == '' || pin == null) {
			$("<?php echo '#'.$fun->Enlink('pinres') ?>").html('PIN can\'t be empty!');
		}
		else {

			$.ajax({
				url : "process/ajax/ajax_checkpin.php",
				type : 'POST',
				data: { 
					pin: pin
				},
				success : function(data) {
					if(data == 'pin1') {
						$(document).ready(function() {
							$("#"+id).click();
						});
					}
					else {
						$("<?php echo '#'.$fun->Enlink('pinres') ?>").html('PIN is invalid!');
					}

				},
				error : function(){
					alert('Terjadi kesalahan dalam jaringan!\nTidak dapat mengambil data Product!\nSilahkan cek koneksi jaringan dan coba lagi!');
					return false;
				}
			});

		}

	});
</script>