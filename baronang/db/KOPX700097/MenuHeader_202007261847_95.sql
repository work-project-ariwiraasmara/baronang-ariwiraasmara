INSERT INTO KOPX700097.dbo.MenuHeader (HeaderNo,Nama,Status,IconStyle) VALUES 
(1,'Master',1,'fa fa-dashboard')
,(2,'Produk',0,'fa fa-list')
,(4,'Pelayanan Pelanggan',0,'fa fa-user')
,(5,'Teller',1,'fa fa-laptop')
,(8,'Pinjaman',0,'fa fa-money')
,(11,'Laporan',0,'fa fa-book')
,(3,'Import',0,'fa fa-download')
,(10,'Akuntansi',0,'fa fa-newspaper-o')
,(6,'Kasir',0,'fa fa-laptop')
,(7,'Bank',0,'fa fa-money')
;
INSERT INTO KOPX700097.dbo.MenuHeader (HeaderNo,Nama,Status,IconStyle) VALUES 
(12,'Parkir',1,'fa fa-list')
,(9,'Potongan Gaji',0,'fa fa-money')
,(14,'Grafik Parkir',1,'fa fa-list')
,(13,'Laporan Parkir',1,'fa fa-list')
;