INSERT INTO KOPX700097.dbo.LaporanLabaRugiKeuangan (KodeAccount,NamaAccount,TypeID,NamaType,Header,GroupID,NamaGroup,Nilai,Bulan,Tahun,Status,Username) VALUES 
('4.1.01.00.000','Pendapatan Simpan Pinjaman','4.1.00.00.000','Pendapatan Usaha','0','4.0.00.00.000','Pendapatan',0.0000,12,'2018',1,'admin')
,('4.1.01.01.000','Pendapatan Bunga Pinjaman','4.1.00.00.000','Pendapatan Usaha','1','4.0.00.00.000','Pendapatan',0.0000,12,'2018',1,'admin')
,('4.1.01.02.000','Pendapatan Administrasi Pinjaman','4.1.00.00.000','Pendapatan Usaha','1','4.0.00.00.000','Pendapatan',0.0000,12,'2018',1,'admin')
,('4.1.01.03.000','Pendapatan Administrasi Tabungan','4.1.00.00.000','Pendapatan Usaha','1','4.0.00.00.000','Pendapatan',0.0000,12,'2018',1,'admin')
,('4.1.01.04.000','Pendapatan Administrasi Deposito','4.1.00.00.000','Pendapatan Usaha','1','4.0.00.00.000','Pendapatan',0.0000,12,'2018',1,'admin')
,('4.1.01.05.000','Pendapatan Billing','4.1.00.00.000','Pendapatan Usaha','1','4.0.00.00.000','Pendapatan',0.0000,12,'2018',1,'admin')
,('4.1.03.00.000','Pendapatan Lain-lain','4.1.00.00.000','Pendapatan Usaha','0','4.0.00.00.000','Pendapatan',0.0000,12,'2018',1,'admin')
,('4.1.03.01.000','Pendapatan Lain-lain','4.1.00.00.000','Pendapatan Usaha','1','4.0.00.00.000','Pendapatan',0.0000,12,'2018',1,'admin')
,('4.1.03.01.001','Pendapatan bunga bank','4.1.00.00.000','Pendapatan Usaha','2','4.0.00.00.000','Pendapatan',0.0000,12,'2018',1,'admin')
,('4.1.03.01.002','Pendapatan lain-lain','4.1.00.00.000','Pendapatan Usaha','2','4.0.00.00.000','Pendapatan',0.0000,12,'2018',1,'admin')
;
INSERT INTO KOPX700097.dbo.LaporanLabaRugiKeuangan (KodeAccount,NamaAccount,TypeID,NamaType,Header,GroupID,NamaGroup,Nilai,Bulan,Tahun,Status,Username) VALUES 
('6.1.01.00.000','Biaya Bunga','6.1.00.00.000','Biaya Simpan Pinjam','0','6.0.00.00.000','Biaya',0.0000,12,'2018',0,'admin')
,('6.1.01.01.000','Biaya Bunga Tabungan','6.1.00.00.000','Biaya Simpan Pinjam','1','6.0.00.00.000','Biaya',0.0000,12,'2018',0,'admin')
,('6.1.01.02.000','Biaya Bunga Deposito','6.1.00.00.000','Biaya Simpan Pinjam','1','6.0.00.00.000','Biaya',0.0000,12,'2018',0,'admin')
,('6.2.01.00.000','Biaya Operasional','6.2.00.00.000','Biaya Umum','0','6.0.00.00.000','Biaya',0.0000,12,'2018',0,'admin')
,('6.2.01.01.000','Biaya Operasional','6.2.00.00.000','Biaya Umum','0','6.0.00.00.000','Biaya',0.0000,12,'2018',0,'admin')
,('6.2.01.01.001','Biaya Gaji','6.2.00.00.000','Biaya Umum','0','6.0.00.00.000','Biaya',0.0000,12,'2018',0,'admin')
,('6.2.01.01.002','Honor Pengurus','6.2.00.00.000','Biaya Umum','2','6.0.00.00.000','Biaya',0.0000,12,'2018',0,'admin')
,('6.2.01.01.003','Biaya Perjalanan Dinas','6.2.00.00.000','Biaya Umum','2','6.0.00.00.000','Biaya',0.0000,12,'2018',0,'admin')
,('6.2.01.01.004','Biaya Operasional','6.2.00.00.000','Biaya Umum','2','6.0.00.00.000','Biaya',0.0000,12,'2018',0,'admin')
,('6.2.01.01.005','Biaya Perlengkapan - ATK','6.2.00.00.000','Biaya Umum','2','6.0.00.00.000','Biaya',0.0000,12,'2018',0,'admin')
;
INSERT INTO KOPX700097.dbo.LaporanLabaRugiKeuangan (KodeAccount,NamaAccount,TypeID,NamaType,Header,GroupID,NamaGroup,Nilai,Bulan,Tahun,Status,Username) VALUES 
('6.2.01.01.006','Biaya Rapat - RAT','6.2.00.00.000','Biaya Umum','2','6.0.00.00.000','Biaya',0.0000,12,'2018',0,'admin')
,('6.3.01.00.000','Biaya Lain-lain','6.3.00.00.000','Biaya Lain-lain','0','6.0.00.00.000','Biaya',0.0000,12,'2018',0,'admin')
,('6.3.01.01.000','Biaya Lain-lain','6.3.00.00.000','Biaya Lain-lain','1','6.0.00.00.000','Biaya',0.0000,12,'2018',0,'admin')
,('6.3.01.01.001','Selisih Kas','6.3.00.00.000','Biaya Lain-lain','2','6.0.00.00.000','Biaya',0.0000,12,'2018',0,'admin')
,('6.3.01.01.002','Biaya Lain-lain','6.3.00.00.000','Biaya Lain-lain','2','6.0.00.00.000','Biaya',0.0000,12,'2018',0,'admin')
,('4.1.03.01.003','Pendapatan biaya penutupan Tabungan','4.1.00.00.000','Pendapatan Usaha','2','4.0.00.00.000','Pendapatan',0.0000,12,'2018',1,'admin')
;