INSERT INTO KOPX700097.dbo.LocationMerchant (LocationID,ReferenceID,Nama,Alamat,Telepon,Status,Long,Lat,Deskripsi,Gambar,Code,Balance,isOnlyMember) VALUES 
('700097LOC0000001','','MMatahari-UPH','Jl. MH. Thamrin Boulevard 1100, Klp. Dua, Karawaci, Tangerang, Banten 15811','(021) 5460901',1,'','','Pelita Harapan University is a private Christian university in Lippo Village','appkoperasi/img/parking/universitas pelita ahrapan.jpg','UPH',279928000.0000,0)
,('700097LOC0000002','','Permata Sport Club','Jalan Permata Bunda Blok C3 No.18, Binong, Curug, Binong, Curug, Tangerang, Banten 15810','',1,'','','Villa Permata','','SMK',76181000.0000,0)
,('700097LOC0000004',NULL,'Helipad','Helipad Lippo Karawaci',' ',1,NULL,NULL,'',NULL,'HLP',1306430000.0000,1)
,('700097LOC0000008',NULL,'Menara Matahari','Karawaci',' ',2,NULL,NULL,NULL,NULL,'MMA',0.0000,1)
,('700097LOC0000009',NULL,'Cyberpark','Karawaci Tangerang',' ',1,NULL,NULL,'',NULL,'CBP',1167694000.0000,0)
,('700097LOC0000010',NULL,'Pallais De Europe','Karawaci',' ',1,NULL,NULL,'',NULL,'PLS',163888000.0000,0)
,('700097LOC0000011',NULL,'FK MRIN','Lippo Karawaci',' ',1,NULL,NULL,NULL,NULL,'FKM',99000.0000,0)
,('700097LOC0000012',NULL,'MRIN','Lippo Karawaci',NULL,2,NULL,NULL,NULL,NULL,'MRN',0.0000,0)
,('700097LOC0000014',NULL,'Depok Town Square','Jl. Margonda Raya No. 1, Pondok Cina, Beji, Depok 16424, Jawa Barat, Indonesia',' ',1,NULL,NULL,'Depok Town Square',NULL,'DETOS',1000.0000,0)
,('700097LOC0000015',NULL,'Siloam TB Simatupang','Jl. RA. Kartini No. 8 Cilandak RT.10/RW.4 Cilandak Barat Kec. Cilandak Kota Jakarta Selatan Daerah Khusus Ibukota Jakarta 12430','(021) 29531900',1,NULL,NULL,NULL,NULL,'SHTB',41940000.0000,1)
;
INSERT INTO KOPX700097.dbo.LocationMerchant (LocationID,ReferenceID,Nama,Alamat,Telepon,Status,Long,Lat,Deskripsi,Gambar,Code,Balance,isOnlyMember) VALUES 
('700097LOC0000016',NULL,'Hypermart Lippo Mall Puri','Jl. Puri Permai No.82, RT.2/RW.2, Kembangan Sel., Kec. Kembangan, Kota Jakarta Barat, Daerah Khusus Ibukota Jakarta 11610','(021) 58357426',2,NULL,NULL,' ',NULL,'HPI',700000.0000,1)
,('700097LOC0000017',NULL,'OT Building','Jl. Lkr. Luar Barat No.Kav. 35-36, RT.1/RW.3, Rw. Buaya, Kecamatan Cengkareng, Kota Jakarta Barat, Daerah Khusus Ibukota Jakarta 11740','(021) 58397777',2,NULL,NULL,' ',NULL,'OTB',0.0000,0)
,('700097LOC0000018',NULL,'RS. Mayapada Modernland','Jl. Honoris Raya No.6, RT.001/RW.006, Modernland, Kota Tangerang','',1,NULL,NULL,'','appkoperasi/img/parking/','MHTG',4716000.0000,NULL)
,('700097LOC0000003',NULL,'Pluit Village','Pluit',' 081777',2,NULL,NULL,'',NULL,'PLV',297000.0000,0)
,('700097LOC0000005',NULL,'Menara Asia','Karawaci',' ',1,NULL,NULL,NULL,NULL,'MNA',619220000.0000,1)
,('700097LOC0000006',NULL,'Benton Junction','Lippo Karawaci',' ',2,NULL,NULL,'Boulevard Palem Raya No. 38',NULL,'BTJ',2000.0000,0)
,('700097LOC0000007',NULL,'Karawaci Office Park','Karawaci',' ',1,NULL,NULL,'',NULL,'KOP',1395486000.0000,0)
,('700097LOC0000013',NULL,'Shelter','Lippo Karawaci',NULL,2,NULL,NULL,NULL,NULL,'SHT',0.0000,1)
;