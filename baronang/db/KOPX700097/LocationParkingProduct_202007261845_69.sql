INSERT INTO KOPX700097.dbo.LocationParkingProduct (LocationID,ProductID,Nama,Deskripsi,Status,JumlahPoinDeduct,Jumlah,Harga,Gambar,VehicleID,Tipe,Keterangan) VALUES 
('700097LOC0000002','700097PROD000001','Member 1 Bulan Motor','Berlangganan 1 bulan',1,100,1,100000.0000,'','700097VEHICLE001',1,'')
,('700097LOC0000002','700097PROD000002','Member 6 Bulan Motor','Berlangganan 6 bulan',1,540,6,600000.0000,'','700097VEHICLE001',1,'')
,('700097LOC0000002','700097PROD000003','Member 12 Bulan Motor','Berlangganan 12 bulan',1,960,12,1200000.0000,'','700097VEHICLE001',1,'')
,('700097LOC0000002','700097PROD000004','Member 1 Bulan Mobil','Berlangganan 1 bulan',1,200,1,200000.0000,'','700097VEHICLE002',1,'')
,('700097LOC0000002','700097PROD000005','Member 6 Bulan Mobil','Berlangganan 6 bulan',1,1200,6,1200000.0000,'','700097VEHICLE002',1,'')
,('700097LOC0000002','700097PROD000006','Member 12 Bulan Mobil','Berlangganan 12 bulan',1,2400,12,2400000.0000,'','700097VEHICLE002',1,'')
,('700097LOC0000001','700097PROD000007','Member 3 Bulan Mobil','Berlangganan 3 bulan ',0,1500,3,1500000.0000,NULL,'700097VEHICLE002',1,NULL)
,('700097LOC0000001','700097PROD000008','Member 3 Bulan Motor','Berlangganan 3 bulan',0,540,3,540000.0000,NULL,'700097VEHICLE001',1,NULL)
,('700097LOC0000001','700097PROD000009','Member Mobil 1 bulan','Berlangganan 1 bulan',0,500,1,500000.0000,NULL,'700097VEHICLE002',1,'')
,('700097LOC0000001','700097PROD000010','Member 1 bulan Motor','Berlangganan 1 bulan',0,180,1,180000.0000,NULL,'700097VEHICLE001',1,'')
;
INSERT INTO KOPX700097.dbo.LocationParkingProduct (LocationID,ProductID,Nama,Deskripsi,Status,JumlahPoinDeduct,Jumlah,Harga,Gambar,VehicleID,Tipe,Keterangan) VALUES 
('700097LOC0000004','700097PROD000011','Member 1 Bulan Motor','Berlangganan 1 bulan ',1,100,1,100000.0000,NULL,'700097VEHICLE001',1,NULL)
,('700097LOC0000004','700097PROD000012','Member 3 Bulan Motor','Berlangganan 3 bulan',1,300,3,300000.0000,NULL,'700097VEHICLE001',1,NULL)
,('700097LOC0000004','700097PROD000013','Member 6 Bulan Motor','Berlangganan 6 bulan, diskon 10% senilai 60 poin',1,540,6,540000.0000,NULL,'700097VEHICLE001',1,NULL)
,('700097LOC0000004','700097PROD000014','Member 12 Bulan Motor','Berlangganan 12 bulan, diskon 20% senilai 240 poin',1,960,12,960000.0000,NULL,'700097VEHICLE001',1,NULL)
,('700097LOC0000004','700097PROD000015','Member 1 Bulan Mobil','Berlangganan 1 bulan',1,200,1,200000.0000,NULL,'700097VEHICLE002',1,NULL)
,('700097LOC0000004','700097PROD000016','Member 3 Bulan Mobil','Berlangganan 3 bulan',1,600,3,600000.0000,NULL,'700097VEHICLE002',1,NULL)
,('700097LOC0000004','700097PROD000017','Member 6 Bulan Mobil','Berlangganan 6 bulan, diskon 10% senilai 120 poin',1,1080,6,1080000.0000,NULL,'700097VEHICLE002',1,NULL)
,('700097LOC0000004','700097PROD000018','Member 12 Bulan Mobil','Berlangganan 12 bulan, diskon 20% senilai 480 poin',1,1920,12,1920000.0000,NULL,'700097VEHICLE002',1,NULL)
,('700097LOC0000005','700097PROD000019','Member 1 Bulan Motor','Berlangganan 1 bulan',1,180,1,180000.0000,NULL,'700097VEHICLE001',1,NULL)
,('700097LOC0000005','700097PROD000020','Member 3 Bulan Motor','Berlangganan 3 bulan ',1,540,3,540000.0000,NULL,'700097VEHICLE001',1,NULL)
;
INSERT INTO KOPX700097.dbo.LocationParkingProduct (LocationID,ProductID,Nama,Deskripsi,Status,JumlahPoinDeduct,Jumlah,Harga,Gambar,VehicleID,Tipe,Keterangan) VALUES 
('700097LOC0000005','700097PROD000021','Member 6 Bulan Motor','Berlangganan 6 bulan',1,1080,6,1080000.0000,NULL,'700097VEHICLE001',1,NULL)
,('700097LOC0000005','700097PROD000022','Member 12 Bulan Motor','Berlangganan 12 bulan',1,2160,12,2160000.0000,NULL,'700097VEHICLE001',1,NULL)
,('700097LOC0000005','700097PROD000023','Member 1 Bulan Mobil','Berlangganan 1 bulan',1,800,1,800000.0000,NULL,'700097VEHICLE002',1,NULL)
,('700097LOC0000005','700097PROD000024','Member 3 Bulan Mobil','Berlangganan 3 bulan ',1,2400,3,2400000.0000,NULL,'700097VEHICLE002',1,NULL)
,('700097LOC0000005','700097PROD000025','Member 6 Bulan Mobil','Berlangganan 6 bulan',1,4800,6,4800000.0000,NULL,'700097VEHICLE002',1,NULL)
,('700097LOC0000005','700097PROD000026','Member 12 Bulan Mobil','Berlangganan 12 bulan',1,9600,12,9600000.0000,NULL,'700097VEHICLE002',1,NULL)
,('700097LOC0000009','700097PROD000027','Member 1 Bulan Motor','Berlangganan 1 bulan',1,100,1,100000.0000,NULL,'700097VEHICLE001',1,NULL)
,('700097LOC0000009','700097PROD000028','Member 3 Bulan Motor','Berlangganan 3 bulan',1,300,3,300000.0000,NULL,'700097VEHICLE001',1,NULL)
,('700097LOC0000009','700097PROD000029','Member 6 Bulan Motor','Berlangganan 6 bulan',1,600,6,600000.0000,NULL,'700097VEHICLE001',1,NULL)
,('700097LOC0000009','700097PROD000030','Member 12 Bulan Motor','Berlangganan 12 bulan',1,1200,12,1200000.0000,NULL,'700097VEHICLE001',1,NULL)
;
INSERT INTO KOPX700097.dbo.LocationParkingProduct (LocationID,ProductID,Nama,Deskripsi,Status,JumlahPoinDeduct,Jumlah,Harga,Gambar,VehicleID,Tipe,Keterangan) VALUES 
('700097LOC0000009','700097PROD000031','Member 1 Bulan Mobil','Berlangganan 1 bulan',1,330,1,330000.0000,NULL,'700097VEHICLE002',1,NULL)
,('700097LOC0000009','700097PROD000032','Member 3 Bulan Mobil','Berlangganan 3 bulan',1,990,3,990000.0000,NULL,'700097VEHICLE002',1,NULL)
,('700097LOC0000009','700097PROD000033','Member 6 Bulan Mobil','Berlangganan 6 bulan',1,1980,6,1980000.0000,NULL,'700097VEHICLE002',1,NULL)
,('700097LOC0000009','700097PROD000034','Member 12 Bulan Mobil','Berlangganan 12 bulan',1,3960,12,3960000.0000,NULL,'700097VEHICLE002',1,NULL)
,('700097LOC0000007','700097PROD000035','Member 1 Bulan Motor','Berlangganan 1 bulan',1,100,1,100000.0000,NULL,'700097VEHICLE001',1,NULL)
,('700097LOC0000007','700097PROD000036','Member 3 Bulan Motor','Berlangganan 3 bulan',1,300,3,300000.0000,NULL,'700097VEHICLE001',1,NULL)
,('700097LOC0000007','700097PROD000037','Member 6 Bulan Motor','Berlangganan 6 bulan',1,600,6,600000.0000,NULL,'700097VEHICLE001',1,NULL)
,('700097LOC0000007','700097PROD000038','Member 12 Bulan Motor','Berlangganan 12 bulan',1,1200,12,1200000.0000,NULL,'700097VEHICLE001',1,NULL)
,('700097LOC0000007','700097PROD000039','Member 1 Bulan Mobil','Berlangganan 1 bulan',1,330,1,330000.0000,NULL,'700097VEHICLE004',1,NULL)
,('700097LOC0000007','700097PROD000040','Member 3 Bulan Mobil','Berlangganan 3 bulan',1,990,3,990000.0000,NULL,'700097VEHICLE004',1,NULL)
;
INSERT INTO KOPX700097.dbo.LocationParkingProduct (LocationID,ProductID,Nama,Deskripsi,Status,JumlahPoinDeduct,Jumlah,Harga,Gambar,VehicleID,Tipe,Keterangan) VALUES 
('700097LOC0000007','700097PROD000041','Member 6 Bulan Mobil','Berlangganan 6 bulan',1,1980,6,1980000.0000,NULL,'700097VEHICLE004',1,NULL)
,('700097LOC0000007','700097PROD000042','Member 12 Bulan Mobil','Berlangganan 12 bulan',1,3960,12,3960000.0000,NULL,'700097VEHICLE004',1,NULL)
,('700097LOC0000009','700097PROD000043','Member 1 Bulan VIP','Berlangganan 1 bulan ',1,500,1,500000.0000,NULL,'700097VEHICLE004',1,NULL)
,('700097LOC0000007','700097PROD000044','Member 1 Bulan VIP Motor','Berlangganan 1 bulan, lokasi parkir didalam area ruko',2,200,1,200000.0000,NULL,'700097VEHICLE006',1,NULL)
,('700097LOC0000007','700097PROD000045','Member 3 Bulan VIP Motor','Berlangganan 3 bulan, lokasi parkir didalam area ruko',2,600,3,600000.0000,NULL,'700097VEHICLE006',1,NULL)
,('700097LOC0000007','700097PROD000046','Member 6 Bulan VIP Motor','Berlangganan 6 bulan, lokasi parkir didalam area ruko',2,1200,6,1200000.0000,NULL,'700097VEHICLE006',1,NULL)
,('700097LOC0000007','700097PROD000047','Member 12 Bulan VIP Motor','Berlangganan 12 bulan, lokasi parkir didalam area ruko',2,2400,12,2400000.0000,NULL,'700097VEHICLE006',1,NULL)
,('700097LOC0000014','700097PROD000052','Member 1 Bulan Motor','Berlangganan 1 bulan',1,100,1,100000.0000,NULL,'700097VEHICLE001',1,NULL)
,('700097LOC0000014','700097PROD000053','Member 3 Bulan Motor','Berlangganan 3 bulan',1,300,3,300000.0000,NULL,'700097VEHICLE001',1,NULL)
,('700097LOC0000014','700097PROD000054','Member 6 Bulan Motor','Berlangganan 6 bulan',1,600,6,600000.0000,NULL,'700097VEHICLE001',1,NULL)
;
INSERT INTO KOPX700097.dbo.LocationParkingProduct (LocationID,ProductID,Nama,Deskripsi,Status,JumlahPoinDeduct,Jumlah,Harga,Gambar,VehicleID,Tipe,Keterangan) VALUES 
('700097LOC0000014','700097PROD000055','Member 12 Bulan Motor','Berlangganan 12 bulan',1,1200,12,1200000.0000,NULL,'700097VEHICLE001',1,NULL)
,('700097LOC0000014','700097PROD000056','Member 1 Bulan Mobil','Berlangganan 1 bulan',1,200,1,200000.0000,NULL,'700097VEHICLE004',1,NULL)
,('700097LOC0000014','700097PROD000057','Member 3 Bulan Mobil','Berlangganan 3 bulan',1,600,3,600000.0000,NULL,'700097VEHICLE004',1,NULL)
,('700097LOC0000014','700097PROD000058','Member 6 Bulan Mobil','Berlangganan 6 bulan',1,1200,6,1200000.0000,NULL,'700097VEHICLE004',1,NULL)
,('700097LOC0000014','700097PROD000059','Member 12 Bulan Mobil','Berlangganan 12 bulan',1,2400,12,2400000.0000,NULL,'700097VEHICLE004',1,NULL)
,('700097LOC0000012','700097PROD000060','Member 1 Bulan Motor','Berlangganan 1 bulan',1,180,1,180000.0000,NULL,'700097VEHICLE001',1,NULL)
,('700097LOC0000012','700097PROD000061','Member 3 Bulan Motor','Berlangganan 3 bulan',1,540,3,540000.0000,NULL,'700097VEHICLE001',1,NULL)
,('700097LOC0000012','700097PROD000062','Member 6 Bulan Mobil','Berlangganan 6 bulan',1,1080,6,1080000.0000,NULL,'700097VEHICLE001',1,NULL)
,('700097LOC0000012','700097PROD000063','Member 12 Bulan Mobil','Berlangganan 12 bulan',1,2160,12,2160000.0000,NULL,'700097VEHICLE001',1,NULL)
,('700097LOC0000012','700097PROD000064','Member 1 Bulan Mobil','Berlangganan 1 bulan',1,300,1,300000.0000,NULL,'700097VEHICLE002',1,NULL)
;
INSERT INTO KOPX700097.dbo.LocationParkingProduct (LocationID,ProductID,Nama,Deskripsi,Status,JumlahPoinDeduct,Jumlah,Harga,Gambar,VehicleID,Tipe,Keterangan) VALUES 
('700097LOC0000012','700097PROD000065','Member 3 Bulan Mobil','Berlangganan 3 bulan',1,900,3,900000.0000,NULL,'700097VEHICLE002',1,NULL)
,('700097LOC0000012','700097PROD000066','Member 6 Bulan Mobil','Berlangganan 6 bulan',1,1800,6,1800000.0000,NULL,'700097VEHICLE002',1,NULL)
,('700097LOC0000012','700097PROD000067','Member 12 Bulan Mobil','Berlangganan 12 bulan',1,3600,12,3600000.0000,NULL,'700097VEHICLE002',1,NULL)
,('700097LOC0000015','700097PROD000068','Member 1 Bulan Motor','Berlangganan 1 bulan',1,180,1,180000.0000,NULL,'700097VEHICLE001',1,NULL)
,('700097LOC0000015','700097PROD000069','Member 3 Bulan Motor','Berlangganan 3 bulan',1,540,3,540000.0000,NULL,'700097VEHICLE001',1,NULL)
,('700097LOC0000015','700097PROD000070','Member 6 Bulan Motor','Berlangganan 6 bulan',1,1080,6,1080000.0000,NULL,'700097VEHICLE001',1,NULL)
,('700097LOC0000015','700097PROD000071','Member 12 Bulan Motor','Berlangganan 12 bulan',1,2160,12,2160000.0000,NULL,'700097VEHICLE001',1,NULL)
,('700097LOC0000015','700097PROD000072','Member 1 Bulan Mobil','Berlangganan 1 bulan',1,800,1,800000.0000,NULL,'700097VEHICLE002',1,NULL)
,('700097LOC0000015','700097PROD000073','Member 3 Bulan Mobil','Berlangganan 3 bulan',1,2400,3,2400000.0000,NULL,'700097VEHICLE002',1,NULL)
,('700097LOC0000015','700097PROD000074','Member 6 Bulan Mobil','Berlangganan 6 bulan',1,4800,6,4800000.0000,NULL,'700097VEHICLE002',1,NULL)
;
INSERT INTO KOPX700097.dbo.LocationParkingProduct (LocationID,ProductID,Nama,Deskripsi,Status,JumlahPoinDeduct,Jumlah,Harga,Gambar,VehicleID,Tipe,Keterangan) VALUES 
('700097LOC0000015','700097PROD000075','Member 12 Bulan Mobil','Berlangganan 12 bulan',1,11600,12,11600000.0000,NULL,'700097VEHICLE002',1,NULL)
,('700097LOC0000016','700097PROD000076','Member 1 Bulan Motor','Gratis 1 bulan member mobil',1,0,1,0.0000,NULL,'700097VEHICLE002',1,NULL)
,('700097LOC0000016','700097PROD000077','Member 1 Hari Motor','Gratis 1 hari member motor',1,0,1,0.0000,NULL,'700097VEHICLE001',0,NULL)
,('700097LOC0000001','700097PROD000078','Member Motor 3 Bulan','Berlangganan 3 bulan motor MM-UPH',1,210,3,210000.0000,NULL,'700097VEHICLE001',1,'')
,('700097LOC0000001','700097PROD000079','Member Mobil 3 Bulan','Berlangganan 3 bulan mobil',1,820,3,820000.0000,NULL,'700097VEHICLE002',1,NULL)
,('700097LOC0000003','700097PROD000080','Member Mobil 6 Bulan','Member Mobil Pluit Village',1,500,6,500000.0000,'','700097VEHICLE002',1,'')
,('700097LOC0000003','700097PROD000081','Member Mobil 12 Bulan','Member Mobil 12 Bulan',1,900,12,900000.0000,NULL,'700097VEHICLE002',1,'')
,('700097LOC0000003','700097PROD000082','Member Mobil 1 Bulan','Member Mobil 1 Bulan Pluit Village',1,1000,1,1000000.0000,'','700097VEHICLE002',1,'')
,('700097LOC0000018','700097PROD000083','Member Motor 1 Bulan','Member Motor 1 Bulan',1,100,1,100000.0000,NULL,'700097VEHICLE001',1,'')
,('700097LOC0000018','700097PROD000084','Member Mobil 1 Bulan','Member Mobil 1 Bulan',1,300,1,300000.0000,NULL,'700097VEHICLE002',1,'')
;
INSERT INTO KOPX700097.dbo.LocationParkingProduct (LocationID,ProductID,Nama,Deskripsi,Status,JumlahPoinDeduct,Jumlah,Harga,Gambar,VehicleID,Tipe,Keterangan) VALUES 
('700097LOC0000010','PRODC001','Member 1 Bulan VIP','Berlangganan 1 bulan',1,200,1,200000.0000,NULL,'700097VEHICLE004',1,NULL)
,('700097LOC0000010','PRODC002','Member 3 Bulan VIP','Berlangganan 3 bulan',1,600,3,600000.0000,NULL,'700097VEHICLE004',1,NULL)
,('700097LOC0000010','PRODC003','Member 6 Bulan VIP','Berlangganan 6 bulan',1,1200,6,1200000.0000,NULL,'700097VEHICLE004',1,NULL)
,('700097LOC0000010','PRODC004','Member 12 Bulan VIP','Berlangganan 12 bulan',1,2400,12,2400000.0000,NULL,'700097VEHICLE004',1,NULL)
;