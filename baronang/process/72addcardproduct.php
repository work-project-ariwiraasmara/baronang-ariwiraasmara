<?php
require('../util/koneksi.php');

$tgl = $fun->POST('startat', 1);
$cpy = $fun->getIDParam('company');
//$cpy = "71";

if($cpy == '' || empty($cpy) || $tgl == '' || empty($tgl)) {
	header('location: ../index.php?pg=card&sb=product&'.$fun->setIDParam('cardid', $fun->getIDParam('cardid')).'&'.$fun->setIDParam('lokasi', $fun->getIDParam('lokasi')).'&'.$fun->setIDParam('status', 'Company / Date can\'t be empty!<br>Try Again!'));
}
else {

	$cardid = $fun->getIDParam('cardid');
	$product = $fun->getIDParam('productid');

	$post = "CID=$cpy&CardID=$cardid&ProductID=$product&StartDate=$tgl";
	
	$curl = curl_init();
	curl_setopt_array($curl, array(
		CURLOPT_URL => "https://baronang.com/apibap/api/baronang/PurchaseProduct?",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "POST",
		CURLOPT_POSTFIELDS => $post,
	));
	$output = curl_exec($curl);
	$err = curl_error($curl);
	curl_close($curl);
	$result = json_decode($output, true);

	if($result['statusCode'] == '101') {
		header('location: ../index.php?pg=card&sb=product&'.$fun->setIDParam('cardid', $cardid).'&'.$fun->setIDParam('company', 71).'&'.$fun->setIDParam('lokasi', $fun->getIDParam('lokasi')).'&'.$fun->setIDParam('status', 'Success Purchasing Product!'));
	}
	else {
		header('location: ../index.php?pg=card&sb=product&'.$fun->setIDParam('cardid', $cardid).'&'.$fun->setIDParam('company', 71).'&'.$fun->setIDParam('lokasi', $fun->getIDParam('lokasi')).'&'.$fun->setIDParam('status', $result['message']));
	}

}
?>
