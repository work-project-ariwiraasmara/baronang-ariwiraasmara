<?php
ob_start();
require('../util/koneksi.php');
require_once('../plugins/excel_reader2.php');
require('../plugins/TCPDF-master/tcpdf.php');

$target = basename($_FILES['filetenant']['name']) ;
move_uploaded_file($_FILES['filetenant']['tmp_name'], $target);
chmod($_FILES['filetenant']['name'],0777);

$data = new Spreadsheet_Excel_Reader($_FILES['filetenant']['name'],false);
$jumlah_baris = $data->rowcount($sheet_index=0);

$lokasi = $fun->getIDParam('lokasi');
$date = date('Y-m-d H:i:s');

// COMPANY
$s1 = "SELECT * from 12mastercompany where companyid='71'";
$q1 = mysqli_query($fun->getConnection(), $s1) or die(mysqli_error($fun->getConnection()));
$d1 = mysqli_fetch_array($q1);


// LOKASI
$sloc = "SELECT * from 21masterlocation where masterlocationid='$lokasi'";
$qloc = mysqli_query($fun->getConnection(), $sloc) or die(mysqli_error($fun->getConnection()));
$dloc = mysqli_fetch_array($qloc);
//echo $dloc['locationname'].'<br><br>';

//$qtymotor 	= $dloc['allowancemotorcycle'];
//$qtymobil1 	= $dloc['allowancesmallcar'];
//$qtymobil2 	= $dloc['allowancebigcar'];

// PRODUCT
	$sprodmotor = "SELECT * from 22amasterproduct where locationid='$lokasi' and vehicletype='Motorcycle' and productprice='0' and active = '1'";
	$qprodmotor = mysqli_query($fun->getConnection(), $sprodmotor) or die(mysqli_error($fun->getConnection()));
	$dprodmotor = mysqli_fetch_array($qprodmotor);
	if ($dprodmotor != '' || !empty($dprodmotor)) {
		$qtymotor = $dprodmotor['allowance'];
	}
	else {
		$qtymotor = 0;
	}
		
	$sprodmobil1 = "SELECT * from 22amasterproduct where locationid='$lokasi' and vehicletype='Small Car' and productprice='0' and active = '1'";
	$qprodmobil1 = mysqli_query($fun->getConnection(), $sprodmobil1) or die(mysqli_error($fun->getConnection()));
	if ($qprodmobil1) {
		$dprodmobil1 = mysqli_fetch_array($qprodmobil1);
		$qtymobil1 = $dprodmobil1['allowance'];
	}
	else {
		$qtymobil1 = 0;
	}
		
	$sprodmobil2 = "SELECT * from 22amasterproduct where locationid='$lokasi' and vehicletype='Big Car' and productprice='0' and active = '1'";
	$qprodmobil2 = mysqli_query($fun->getConnection(), $sprodmobil2) or die(mysqli_error($fun->getConnection()));
	if ($qprodmobil2) {
		$dprodmobil2 = mysqli_fetch_array($qprodmobil2);
		$qtymobil2 = $dprodmobil2['allowance'];
	}
	else {
		$qtymobil2 = 0;
	}

$spkk = "SELECT a.nama_prov as nama_prov, b.nama_kabkot as nama_kabkot 
			from 11amasterprovince as a 
			inner join 11bmastercity as b 
				on a.id_prov = b.id_prov 
			where a.id_prov='".$dloc['locationprovince']."' and b.id_kabkot='".$dloc['locationcity']."'";
$qpkk = mysqli_query($fun->getConnection(), $spkk) or die(mysqli_error($fun->getConnection()));
$dpkk = mysqli_fetch_array($qpkk);


$col_nama = 0;
for($bs = 1; $bs < 21; $bs++) {
	if($data->val(3, $bs) == 'Nama' || $data->val(3, $bs) == 'Name' || $data->val(3, $bs) == 'MemberName' || $data->val(3, $bs) == 'Member Name' || $data->val(3, $bs) == 'Nama Member' || $data->val(3, $bs) == 'NamaMember' ) {
		$col_nama = $bs;
	}
}

$col_tlp = 0;
for($bs = 1; $bs < 21; $bs++) {
	if($data->val(3, $bs) == 'Tlp' || $data->val(3, $bs) == 'No. Tlp' || $data->val(3, $bs) == 'Nomor Telepon' || $data->val(3, $bs) == 'NomorTelepon' || $data->val(3, $bs) == 'No.Tlp') {
		$col_tlp = $bs;
	}
}

$col_motor = 0;
for($bs = 1; $bs < 21; $bs++) {
	if($data->val(3, $bs) == 'Motor' || $data->val(3, $bs) == 'Motorcycle' || $data->val(3, $bs) == 'Jumlah Motor' || $data->val(3, $bs) == 'JumlahMotor' || $data->val(3, $bs) == 'QTY Motor' || $data->val(3, $bs) == 'QtyMotor' ) {
		$col_motor = $bs;
	}
}

$col_mobil1 = 0;
for($bs = 1; $bs < 21; $bs++) {
	if($data->val(3, $bs) == 'Mobil Kecil' || $data->val(3, $bs) == 'MobilKecil' || $data->val(3, $bs) == 'Car Small' || $data->val(3, $bs) == 'CarSmall' || $data->val(3, $bs) == 'Jumlah Mobil Kecil' || $data->val(3, $bs) == 'JumlahMobilKecil' || $data->val(3, $bs) == 'QTY Mobil Kecil' || $data->val(3, $bs) == 'QTYMobilKecil' ) {
		$col_mobil1 = $bs;
	}
}

$col_mobil2 = 0;
for($bs = 1; $bs < 21; $bs++) {
	if($data->val(3, $bs) == 'Mobil Besar' || $data->val(3, $bs) == 'MobilBesar' || $data->val(3, $bs) == 'Car Big' || $data->val(3, $bs) == 'CarBig' || $data->val(3, $bs) == 'Jumlah Mobil Besar' || $data->val(3, $bs) == 'JumlahMobilBesar' || $data->val(3, $bs) == 'QTY Mobil Besar' || $data->val(3, $bs) == 'QTYMobilBesar' ) {
		$col_mobil2 = $bs;
	}
}


$col_tier1 = 0;
for($bs = 1; $bs < 21; $bs++) {
	if($data->val(3, $bs) == $d1['labeltier1']) {
		$col_tier1 = $bs;
	}
}
//echo 'labeltier1 : col-'.$col_tier1.'<br>';
$f1tier1 = str_replace(' ', '', $col_tier1);
$f2tier1 = str_replace('.', '', $f1tier1);
$f3tier1 = str_replace(',', '', $f2tier1);


$col_tier2 = 0;
for($bs = 1; $bs < 21; $bs++) {
	if($data->val(3, $bs) == $d1['labeltier2']) {
		$col_tier2 = $bs;
	}
}
//echo 'labeltier2 : col-'.$col_tier2.'<br>';
$f1tier2 = str_replace(' ', '', $col_tier2);
$f2tier2 = str_replace('.', '', $f1tier2);
$f3tier2 = str_replace(',', '', $f2tier2);


$col_tier3 = 0;
for($bs = 1; $bs < 21; $bs++) {
	if($data->val(3, $bs) == $d1['labeltier3']) {
		$col_tier3 = $bs;
	}
}
//echo 'labeltier3 : col-'.$col_tier3.'<br>';
$f1tier3 = str_replace(' ', '', $col_tier3);
$f2tier3 = str_replace('.', '', $f1tier3);
$f3tier3 = str_replace(',', '', $f2tier3);


$col_tier4 = 0;
for($bs = 1; $bs < 21; $bs++) {
	if($data->val(3, $bs) == $d1['labeltier4']) {
		$col_tier4 = $bs;
	}
}
//echo 'labeltier4 : col-'.$col_tier4.'<br>';
$f1tier4 = str_replace(' ', '', $col_tier4);
$f2tier4 = str_replace('.', '', $f1tier4);
$f3tier4 = str_replace(',', '', $f2tier4);


$col_tier5 = 0;
for($bs = 1; $bs < 21; $bs++) {
	if($data->val(3, $bs) == $d1['labeltier5']) {
		$col_tier5 = $bs;
	}
}
//echo 'labeltier5 : col-'.$col_tier5.'<br><br>';
$f1tier5 = str_replace(' ', '', $col_tier5);
$f2tier5 = str_replace('.', '', $f1tier5);
$f3tier5 = str_replace(',', '', $f2tier5);

$filetxt = fopen("Akun Member ".$dloc['locationname'].".html", "w") or die("Unable to open file!");
$pdf_content = '';
$jst = 1;

// GET LAST ID TENANT
$s2 = "SELECT mastertenantid from 23dmastertenant where mastertenantid like '".substr($dloc['masterlocationid'], 0, 9)."%' order by mastertenantid desc limit 1";
//echo $s2.'<br><br>';
$q2 = mysqli_query($fun->getConnection(), $s2) or die(mysqli_error($fun->getConnection()));
$r2 = mysqli_num_rows($q2);

if($r2 > 0) {
	echo 'last id tenant ada!<br><br>';
	$d2 = mysqli_fetch_array($q2);
	$id1 = substr($d2['mastertenantid'], -7, 4) + 1;
	$id2 = str_pad($id1, 4,"0", STR_PAD_LEFT);
	//$tid = '7101'.substr($dloc['masterlocationid'], 4, 3).date('y').$id2.'000';
	//$tid = '7101'.date('y').substr($dloc['masterlocationid'], 6, 3).$id2.'000';
}
else {
	echo 'last id tenant tidak ada!<br><br>';
	$id2 = 1001;
	//$tid = '7101'.substr($dloc['masterlocationid'], 4, 3).date('y').'1001000';
	//$tid = '7101'.date('y').substr($dloc['masterlocationid'], 4, 3).'1001000';
	//$tid = '7101'.date('y').substr($dloc['masterlocationid'], 6, 3).$id2.'000';
}

for ($i = 4; $i <= $jumlah_baris; $i++) {

	$nama 		= $data->val($i, $col_nama);
	$tier1 		= $data->val($i, $col_tier1);
	$tier2 		= $data->val($i, $col_tier2);
	$tier3 		= $data->val($i, $col_tier3);
	$tier4 		= $data->val($i, $col_tier4);
	$tier5 		= $data->val($i, $col_tier5);
	$tlp 		= $data->val($i, $col_tlp);
	//$qtymotor 	= $data->val($i, $col_motor);
	//$qtymobil1 	= $data->val($i, $col_mobil1);
	//$qtymobil2 	= $data->val($i, $col_mobil2);

	$f1tier1 = str_replace(' ', '', $tier1);
	$f2tier1 = str_replace('.', '', $f1tier1);
	$f3tier1 = str_replace(',', '', $f2tier1);

	$f1tier2 = str_replace(' ', '', $tier2);
	$f2tier2 = str_replace('.', '', $f1tier2);
	$f3tier2 = str_replace(',', '', $f2tier2);

	$f1tier3 = str_replace(' ', '', $tier3);
	$f2tier3 = str_replace('.', '', $f1tier3);
	$f3tier3 = str_replace(',', '', $f2tier3);

	$f1tier4 = str_replace(' ', '', $tier4);
	$f2tier4 = str_replace('.', '', $f1tier4);
	$f3tier4 = str_replace(',', '', $f2tier4);

	$f1tier5 = str_replace(' ', '', $tier5);
	$f2tier5 = str_replace('.', '', $f1tier5);
	$f3tier5 = str_replace(',', '', $f2tier5);

	$tid = '7101'.date('y').substr($dloc['masterlocationid'], 6, 3).$id2.'000';
	//echo $tid.'<br><br>';

	//$email = strtolower($f3tier1).strtolower($f3tier2).strtolower($f3tier3).strtolower($f3tier4).strtolower($f3tier5).'@'.strtolower(str_replace(' ', '', $dloc['locationname'])).'.'.strtolower(str_replace(' ', '', $d1['companyname'])).'.baronang.com';
	//$pass = $fun->randChar('10');
	//$enpass = $fun->ENID($pass);

	//$pdf_content .= "#[".$jst."]<br>";
	//$pdf_content .= "Email : ".$email."<br>";
	//$pdf_content .= "Password : ".$pass."<br>";
	//$pdf_content .= "========================================<br><br>";

	//$s3 = "INSERT into 23dmastertenant values('$tid', '', '$date', '', '$lokasi', '$email', '$enpass', '$nama', '$tlp', '', '', '', '$qtymobil1', '$qtymobil2', '$qtymotor', '3', '$tier1', '$tier2', '$tier3', '$tier4', '$tier5');";
	$locname = $dloc['locationname'];
	if (!empty($f1tier1)) {
		$TenantName = $locname.'-'.$f1tier1.'-'.$f1tier2.'-'.$f1tier3.'-'.$f1tier4.'-'.$f1tier5;
	}
	else if (!empty($f1tier2)) {
		$TenantName = $locname.'-'.$f1tier2.'-'.$f1tier3.'-'.$f1tier4.'-'.$f1tier5;
	}
	else {
		$TenantName = $locname.'-'.$f1tier3.'-'.$f1tier4.'-'.$f1tier5;
	}
	
	$s3 = "INSERT into 23dmastertenant values('$tid', '', '$date', null, '$lokasi', null, null, '$TenantName', '$tlp', '', '0', '0', '$qtymobil1', '$qtymobil2', '$qtymotor', '3', '$tier1', '$tier2', '$tier3', '$tier4', '$tier5')";
	$q3 = mysqli_query($fun->getConnection(), $s3) or die(mysqli_error($fun->getConnection()));

	/*
	if($q3) {

		$s4 = "SELECT mastercardid from 23amastercard where memberid='$tid' order by mastercardid DESC limit 1";
		//echo $s4.'<br>';
		$q4 = mysqli_query($fun->getConnection(), $s4) or die(mysqli_error($fun->getConnection()));
		$r4 = mysqli_num_rows($q4);

		if($r4 > 0) {
			$d4 = mysqli_fetch_array($q4);
			$no = str_pad( ((int)substr($d4['mastercardid'], -3) + 1), 3, "0", STR_PAD_LEFT);
		}
		else {
			$no = str_pad(1, 3, "0", STR_PAD_LEFT);
		}

		// GET LAST ID PRODUCT OWN
		$scpo = "SELECT activitylogid from 31amastercardproductowned order by activitylogid DESC";
		$qcpo = mysqli_query($fun->getConnection(), $scpo) or die(mysqli_error($fun->getConnection()));
		$dcpo = mysqli_fetch_array($qcpo);
		$idcpo = (int)$dcpo['activitylogid'] + 1;
		echo 'Last activitylogid: '.$dcpo['activitylogid'].' ==> '.$idcpo.'<br><br>';

		$sprodmotor = "SELECT masterproductid from 22amasterproduct where locationid='$lokasi' and vehicletype='Motor' and productprice='0'";
		$qprodmotor = mysqli_query($fun->getConnection(), $sprodmotor) or die(mysqli_error($fun->getConnection()));
		$dprodmotor = mysqli_fetch_array($qprodmotor);
		$prodmotor = $dprodmotor['masterproductid'];

		if( $prodmotor != '' || !empty($prodmotor) ) {
			if($qtymotor > 0) {
				for($x = 1; $x <= $qtymotor; $x++) {
					$date = date('Y-m-d H:i:s');

					$motorid = substr($tid, 0, 13).$no;
					
					$scmotor1 = "INSERT into 23amastercard values('$motorid', '$tid', '$date', 'Motor', '0')";
					echo 'Motor #'.$x.') .'.$scmotor1.'<br>';
					$qcmotor1 = mysqli_query($fun->getConnection(), $scmotor1) or die(mysqli_error($fun->getConnection()));
					
					$spmotor = "SELECT validity_days from 22amasterproduct where masterproductid='$prodmotor'";
					$qpmotor = mysqli_query($fun->getConnection(), $spmotor) or die(mysqli_error($fun->getConnection()));
					$dpmotor = mysqli_fetch_array($qpmotor);

					$expvalid = date('Y-m-d H:i:s', (time() + ($dpmotor['validity_days'] * 24 * 60 * 60)) );

					$scmotor2 = "INSERT into 31amastercardproductowned values('$idcpo', '$date', '$motorid', '$prodmotor', '$expvalid')";
					echo 'Motor #'.$x.') .'.$scmotor2.'<br><br>';
					$qcmotor2 = mysqli_query($fun->getConnection(), $scmotor2) or die(mysqli_error($fun->getConnection()));

					$no++;
					$no = str_pad($no, 3, "0", STR_PAD_LEFT);
					$idcpo++;
				}
			}
		}
		
		echo '=========================================================================<br><br>';

		$sprodmobil1 = "SELECT masterproductid from 22amasterproduct where locationid='$lokasi' and vehicletype='Mobil Kecil' and productprice='0'";
		$qprodmobil1 = mysqli_query($fun->getConnection(), $sprodmobil1) or die(mysqli_error($fun->getConnection()));
		$dprodmobil1 = mysqli_fetch_array($qprodmobil1);
		$prodmobil1 = $dprodmobil1['masterproductid'];

		if( $prodmobil1 != '' || !empty($prodmobil1) ) {
			if($qtymobil1 > 0) {
				for($x = 1; $x <= $qtymobil1; $x++) {
					$date = date('Y-m-d H:i:s');
					$mobil1id = substr($tid, 0, 13).$no;
					
					$scmobil11 = "INSERT into 23amastercard values('$mobil1id', '$tid', '$date', 'Mobil Kecil', '0')";
					echo 'Mobil 1 #'.$x.'). '.$scmobil11.'<br>';
					$qcmobil11 = mysqli_query($fun->getConnection(), $scmobil11) or die(mysqli_error($fun->getConnection()));
					
					$spmobil11 = "SELECT validity_days from 22amasterproduct where masterproductid='$prodmobil1'";
					$qpmobil11 = mysqli_query($fun->getConnection(), $spmobil11) or die(mysqli_error($fun->getConnection()));
					$dpmobil11 = mysqli_fetch_array($qpmobil11);

					$expvalid = date('Y-m-d H:i:s', (time() + ($dpmobil11['validity_days'] * 24 * 60 * 60)) );

					$scmobil12 = "INSERT into 31amastercardproductowned values('$idcpo', '$date', '$mobil1id', '$prodmobil1', '$expvalid')";
					echo 'Mobil 1 #'.$x.'). '.$scmobil12.'<br><br>';
					$qcmobil12 = mysqli_query($fun->getConnection(), $scmobil12) or die(mysqli_error($fun->getConnection()));

					$no++;
					$no = str_pad($no, 3, "0", STR_PAD_LEFT);
					$idcpo++;
				}
			}
		}
		

		echo '=========================================================================<br><br>';

		$sprodmobil2 = "SELECT masterproductid from 22amasterproduct where locationid='$lokasi' and vehicletype='Mobil Besar' and productprice='0'";
		$qprodmobil2 = mysqli_query($fun->getConnection(), $sprodmobil2) or die(mysqli_error($fun->getConnection()));
		$dprodmobil2 = mysqli_fetch_array($qprodmobil2);
		$prodmobil2 = $dprodmobil2['masterproductid'];

		if( $prodmobil2 != '' || !empty($prodmobil2) ) {
			if($qtymobil2 > 0) {
				for($x = 1; $x <= $qtymobil2; $x++) {
					$date = date('Y-m-d H:i:s');
					$mobil2id = substr($tid, 0, 13).$no;
					
					$scmobil21 = "INSERT into 23amastercard values('$mobil2id', '$tid', '$date', 'Mobil Besar', '0')";
					echo 'Mobil 2 #'.$x.'). '.$scmobil21.'<br>';
					$qcmobil21 = mysqli_query($fun->getConnection(), $scmobil21) or die(mysqli_error($fun->getConnection()));
					
					$spmobil21 = "SELECT validity_days from 22amasterproduct where masterproductid='$prodmobil2'";
					$qpmobil21 = mysqli_query($fun->getConnection(), $spmobil21) or die(mysqli_error($fun->getConnection()));
					$dpmobil21 = mysqli_fetch_array($qpmobil21);

					$expvalid = date('Y-m-d H:i:s', (time() + ($dpmobil21['validity_days'] * 24 * 60 * 60)) );

					$scmobil22 = "INSERT into 31amastercardproductowned values('$idcpo', '$date', '$mobil2id', '$prodmobil2', '$expvalid')";
					echo 'Mobil 2 #'.$x.'). '.$scmobil22.'<br><br>';
					$qcmobil22 = mysqli_query($fun->getConnection(), $scmobil22) or die(mysqli_error($fun->getConnection()));

					$no++;
					$no = str_pad($no, 3, "0", STR_PAD_LEFT);
					$idcpo++;
				}
			}
		}
		
	}
	*/

	//echo $s3.'<br><br>';
	$jst++; $id2++;
}

/*
$pdf_content .= '<center>';
	$pdf_content .= '<div style="margin-top: 100px;">';
		$pdf_content .= 'Halaman akan redirect dalam waktu 60 detik, atau klik tombol kembali<br>';
		$pdf_content .= '<button id="btn_print" onclick="window.print();">Print</button>';
		$pdf_content .= '<a href="../?pg=tenant&'.$fun->setIDParam('lokasi', $fun->getIDParam('lokasi')).'"><button>Kembali</button></a>';
	$pdf_content .= '</div>';
$pdf_content .= '</center>';

fwrite($filetxt, $pdf_content);
fclose($filetxt);
*/

?>
<script type="text/javascript">
	//window.location.href = "<?php //echo 'Akun Member '.$dloc['locationname'].'.html'; ?>";
	
	//setTimeout(function(){
		window.location.href = "<?php echo '../?pg=tenant&'.$fun->setIDParam('lokasi', $lokasi).'&'.$fun->setIDParam('status', 'Successful add tenant!'); ?>"
	//}, 60000);
</script>
