<?php
require('../util/koneksi.php');

$nama 		= $fun->POST('nama', 1);
$email 		= $fun->POST('email', 1);
$tlp 		= $fun->POST('tlp', 1);
$alamat 	= $fun->POST('alamat', 1);
$prov 		= $fun->POST('prov', 2);
$kabkot 	= $fun->POST('kabkot', 2);
$motor 		= $fun->POST('motor', 1);
$mobil1		= $fun->POST('mobil1', 1);
$mobil2		= $fun->POST('mobil2', 1);


$img 		= $fun->FILE('gambar', 'name', 1);
$src_img 	= $fun->FILE('gambar', 'tmp_name', 1);

if( $nama == '' || empty($nama) ||
	$email == '' || empty($email) ) {
	//$fun->setOneSession('error_content', 'Nama/Email Tidak Boleh Kosong!<br>Silahkan Coba Lagi!', 2);
	//header('location:../ei.php?'.$fun->setIDParam('title', 'Error Login!').'&'.$fun->setIDParam('link', 'index.php'));
	header('location: ../index.php?'.$fun->setIDParam('status', 'Name/Email can\'t be empty!<br>Try again!'));
}
else {

	$s1 = "SELECT masterlocationid from 21masterlocation order by masterlocationid desc limit 1";
	$q1 = mysqli_query($fun->getConnection(), $s1) or die(mysqli_error($fun->getConnection()));
	$r1 = mysqli_num_rows($q1);
	$d1 = mysqli_fetch_array($q1);

	if($r1 > 0) {
		//$locid = '7101'.((int)substr($d1['masterlocationid'], 4, 3) + 1).date('y').'0000000';
		$locid = '7101'.date('y').((int)substr($d1['masterlocationid'], 6, 3) + 1).'0000000';
	}
	else {
		$locid = '7101'.date('y').'001'.'0000000';
	}

	$date = date('Y-m-d H:i:s');
	$s2 = "INSERT into 21masterlocation values('$locid', '$date', '$nama', '$alamat', '$kabkot', '$prov', '$tlp', '$email', '1', '$img',0,0,0)";
	echo $s2;
	$q2 = mysqli_query($fun->getConnection(), $s2) or die(mysqli_error($fun->getConnection()));

	if($q2) {
		move_uploaded_file($src_img, '../images/location/'.$img);

		// 7101.lokasi.date('y').01.(0 -> free, 1 -> paid).(1->motor, 2->mobil kecil, 3->mobil besar).001
		// 7101.101.20.01.0.1.001 -> motor free -> [FIXED] 7101101200101001
		// 7101.101.20.01.0.2.001 -> mobil kecil free -> [FIXED] 7101101200102001
		// 7101.101.20.01.0.3.001 -> mobil besar free -> [FIXED] 7101101200103001
		$desk1 = $nama.' Compliment Motor';
		$desk2 = $nama.' Compliment Mobil Kecil';
		$desk3 = $nama.' Compliment Mobil Besar';
		
		//$idprod1 = "7102".substr($locid, 4, 3).date('y')."0101001";
		//$idprod2 = "7102".substr($locid, 4, 3).date('y')."0102001";
		//$idprod3 = "7102".substr($locid, 4, 3).date('y')."0103001";

		$idprod1 = "7102".date('y').substr($locid, 6, 3)."0101001";
		$idprod2 = "7102".date('y').substr($locid, 6, 3)."0102001";
		$idprod3 = "7102".date('y').substr($locid, 6, 3)."0103001";

		//$s3 = "INSERT into 22amasterproduct values
		//		('$idprod1', '$date', '$locid', '$desk1', 'Motor', '3650', '0', '2'),
		//		('$idprod2', '$date', '$locid', '$desk2', 'Mobil Kecil ', '3650', '0', '2'),
		//		('$idprod3', '$date', '$locid', '$desk3', 'Mobil Besar', '3650', '0', '2')";
		//echo $s3;
		//$q3 = mysqli_query($fun->getConnection(), $s3) or die(mysqli_error($fun->getConnection()));

		if($q2) {
			header('location: ../index.php?pg=location&'.$fun->setIDParam('status', 'Successfull add location!'));
		}
		else {
			header('location: ../index.php?pg=location&'.$fun->setIDParam('status', 'Fail to add location!<br>Check connection!'));
		}

	}
	else {
		header('location: ../index.php?pg=location&'.$fun->setIDParam('status', 'Fail to add location!<br>Check connection!'));
	}

}
?>
