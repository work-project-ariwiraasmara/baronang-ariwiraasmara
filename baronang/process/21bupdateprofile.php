<?php
require('../util/koneksi.php');

$nama 	= $fun->POST('nama', 1);

if($nama == '' || empty($nama) || is_null($nama) ) {

	//$fun->setOneSession('error_content', 'Nama / KTP Tidak Boleh Kosong!', 2);
	//header('location:../ei.php?'.$fun->setIDParam('title', 'Field Kosong!').'&'.$fun->setIDParam('link', '?'));
	header('location: ../index.php?'.$fun->setIDParam('status', 'Name can\'t be empty!'));
}
else {
	$ktp 	= $fun->POST('ktp', 1);
	$tlp 	= $fun->POST('tlp', 1);
	$alamat	= $fun->POST('alamat', 1);
	$prov 	= $fun->POST('prov', 2);
	$kabkot = $fun->POST('kabkot', 2);

	$session = $fun->getValookie('session');
	$status = $fun->getIDParam('status');
	if($status == 2) {
		$pin = $fun->POST('pin', 1);

		//$wsc = "Berhasil menyimpan data diri!";
		$wsc = "Successful save personal information!";
		$wst = "Success!";

		//$wfc = "Gagal menyimpan data diri!<br>Silahkan Coba lagi!";
		$wfc = "Fail to save personal information!<br>Try Again!";
		$wft = "Fail!";
		$link = "index.php?";

		if($session == 'member') {
			$s1 = "UPDATE 22bmastermember set membername = '$nama',
											memberphone = '$tlp',
										  	pin = '$pin',
										  	active = '1'
					where mastermemberid='".$fun->getValookie('id')."'";
		}
		else {
			$s1 = "UPDATE 23dmastertenant set tenantname = '$nama',
											tenantphone = '$tlp',
											tenantaddess = '$alamat',
											province = '$prov',
											city = '$kabkot',
											status = '1'
					where mastertenantid='".$fun->getValookie('id')."'";

		}
		
	}
	else {
		//$wsc = "Profil Berhasil Diperbaharui!";
		$wsc = "Successful update profile!";
		$wst = "Profil Updated!";

		//$wfc = "Update Profil Gagal!<br>Silahkan Coba Lagi!";
		$wfc = "Failt to update profile!<br>Try Again!";
		$wft = "Update Fail!";

		//$link = "index.php?pg=profil";
		$link = "index.php?";

		if($session == 'member') {
			$s1 = "UPDATE 22bmastermember set membername='$nama',
											ktp='$ktp', 
											memberphone='$tlp', 
											memberaddress='$alamat', 
											memberprovince='$prov', 
											membercity='$kabkot'
					where mastermemberid='".$fun->getValookie('id')."'";
		}
		else {
			$s1 = "UPDATE 23dmastertenant set tenantname = '$nama',
											tenantphone = '$tlp',
											tenantaddess = '$alamat',
											province = '$prov',
											city = '$kabkot'
					where mastertenantid='".$fun->getValookie('id')."'";
		}

	}

	$q1 = mysqli_query($fun->getConnection(), $s1) or die(mysqli_error($fun->getConnection()));

	if($q1) {
		//header('location: ../'.$link.$fun->setIDParam('status', $wsc));
		header('location: 14applycard.php?'.$fun->setIDParam('status_message', $wsc).'&'.$fun->setIDParam('status_member', 2));
	}
	else {
		header('location: ../'.$link.$fun->setIDParam('status', $wft));
	}

}
?>