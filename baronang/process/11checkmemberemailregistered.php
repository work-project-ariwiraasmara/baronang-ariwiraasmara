<?php
require('../util/koneksi.php');
$email = $fun->POST('email', 1);

if($email == '' || empty($email) || is_null($email) ) {
	header('location: ../index.php?'.$fun->setIDParam('status', 'Email can\'t be empty!'));
}
else {

	$s1 = "SELECT * from 22bmastermember where memberemail='$email'";
	$q1 = mysqli_query($fun->getConnection(), $s1) or die(mysqli_error($fun->getConnection()));
	$d1 = mysqli_fetch_array($q1);

	if(count($d1) > 0) {

		$s2 = "SELECT * from 23cOutgoingEmail where email='$email'";
		$q2 = mysqli_query($fun->getConnection(), $s2) or die(mysqli_error($fun->getConnection()));
		$d2 = mysqli_fetch_array($q2);

		if(count($d2) > 0) {
			if( strtotime(date('Y-m-d H:i:s')) < strtotime($d2['datevalid']) ) {
				header('location: ../index.php?pg=changepass&'.$fun->setIDParam('status', 'You made request before and still valid<br>Check email!'));
			}
			else {
				$s3 = "DELETE from 23cOutgoingEmail where email='$email'";
				$q3 = mysqli_query($fun->getConnection(), $s3) or die(mysqli_error($fun->getConnection()));

				header('location: ../index.php?pg=changepass&'.$fun->setIDParam('status', 'You made request before but it\'s already no longer valid!'));
			}
		}
		else {
			$datenow = date('Y-m-d H:i:s');
			$datevalid = date('Y-m-d H:i:s', time() + (30 * 60) );

			$sukey = "SELECT ukey from 23cOutgoingEmail order by ukey DESC";
			$qukey = mysqli_query($fun->getConnection(), $sukey) or die(mysqli_error($fun->getConnection()));
			$dukey = mysqli_fetch_array($qukey);

			$ukey = "@".$fun->Enlink($email).":#".date('ymdHis').'$#'.rand(1,9)."$#".rand(10,99)."$#".rand(100,999)."$#".$fun->randChar(5).'$#7000pass';
			
			$s3 = "INSERT into 23cOutgoingEmail values('', '$datenow', '$datevalid', '$email', '$ukey', '1')";
			$q3 = mysqli_query($fun->getConnection(), $s3) or die(mysqli_error($fun->getConnection()));

			if($q3) {
				$subject = "Change Password Confirmation";
				$link = 'baronang.com/baronang/?pg=updatepass%26'.$fun->setIDParam('ideukey', $ukey).'%26'.$fun->setIDParam('tipe', 'chp1');
				//baronang.com/baronang/?pg=updatepass%26".$fun->setIDParam('ideukey', $ukey)."<br>
				$txt = 	"Kepada Anggota Baronang Terhormat,<br>
						Saudara/i ".$d1['membername']."<br>
						<br>
						Anda telah melakukan permintaan lupa password.<br>
						Kami perlu memastikan bahwa anda yang melakukan permintaan. Harap mengkonfirmasi dengan mengklik link dibawah ini.<br>
						<br>
						<a href='".$link."'>Konfirmasi</a><br>
						<br>
						Harap segera mengganti password anda dalam tenggat waktu 30 menit setelah permintaan dibuat.<br>
						Abaikan pesan ini jika anda tidak melakukan permintaan merubah password.<br>
						<br>
	            		<br>
	            		<br>
	            		<br>
	            		Terima kasih.<br>
	            		Salam,<br>
	            		<br>
	            		<br>
	            		Sky Parking
	            		<br>
	            		<i>Powered by Baronang</i>";
				
				//$headers = "MIME-Version: 1.0" . "\r\n";
				//$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
				//$headers .= 'From: <noreply@baronang.com>' . "\r\n";

				//mail($email, $subject, $txt, $headers);
				$curl = curl_init();
                    $post = "toaddr=$email&subject=$subject&message=$txt";
                    curl_setopt_array($curl, array(
                        CURLOPT_URL => "http://interzircon.com/phpmail.php",
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => "",
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 30,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => "POST",
                        CURLOPT_POSTFIELDS => $post,
                    ));
                    $output = curl_exec($curl);
                    $err = curl_error($curl);
                    curl_close($curl);
                    $result = json_decode($output, true);

				//$fun->setOneSession('error_content', 'Permintaan lupa password berhasil!<br>Silahkan cek email Anda!', 2);
				//header('location:../ei.php?'.$fun->setIDParam('title', 'Request Change Password!').'&'.$fun->setIDParam('link', 'index.php'));
				header('location: ../index.php?'.$fun->setIDParam('status', 'Request forgot password succesfully!<br>Check email!'));
			}
			else {
				//$fun->setOneSession('error_content', 'Gagal mengirim permintaan lupa password!', 2);
				//header('location:../ei.php?'.$fun->setIDParam('title', 'Error Send Request!').'&'.$fun->setIDParam('link', '?pg=changepass'));
				header('location: ../index.php?pg=changepass&'.$fun->setIDParam('status', 'Fail to send request forgot password!'));
			}

			
		}

		
	}
	else {
		//$fun->setOneSession('error_content', 'Email ini belum terdaftar sebagai anggota!', 2);
		//header('location:../ei.php?'.$fun->setIDParam('title', 'Not Found!').'&'.$fun->setIDParam('link', '?pg=changepass'));
		header('location: ../index.php?pg=changepass&'.$fun->setIDParam('status', 'This Email is not registered as a member!'));
	}

}
?>
