<?php
require('../util/koneksi.php');
$email 	= $fun->POST('email', 1);
$pass 	= $fun->POST('pass', 1);

if($email == '' || empty($email) || is_null($email) || 
	$pass == '' || empty($pass) || is_null($pass) ) {
	$fun->setOneSession('error_content', 'Email / Password Tidak Boleh Kosong!<br><br>Silahkan Coba Lagi!', 2);
	header('location:../ei.php?'.$fun->setIDParam('title', 'Error Register!').'&'.$fun->setIDParam('link', 'index.php'));
}
else {

	$s1 = "SELECT * from 22bmastermember where memberemail='$email'";
	$q1 = mysqli_query($fun->getConnection(), $s1) or die(mysqli_error($fun->getConnection()));
	$d1 = mysqli_fetch_array($q1);

	if( count($d1) > 0 ) {
		$fun->setOneSession('error_content', 'Email ini sudah terdaftar!<br><br>Tidak bisa mendaftarkan keanggotaan dengan email yang sama!', 2);
		header('location:../ei.php?'.$fun->setIDParam('title', 'Error Register!').'&'.$fun->setIDParam('link', '?pg=register'));
	}
	else {

		$created = date('Y-m-d H:i:s');
		$ukey = "@".$fun->Enlink($email).":#".rand(1,9)."0$#".rand(10,99)."00$#".rand(100,999)."000$#".$fun->randChar(5).'$#7000reg';
		$s2 = "INSERT into 23cOutgoingEmail values('', '$created', '', '$email', '$ukey', '2', '$pass')";
		$q2 = mysqli_query($fun->getConnection(), $s2) or die(mysqli_error($fun->getConnection()));
		$link = 'baronang.com/baronang/?pg=aktif%26'.$fun->setIDParam('activate', $ukey);

		if($q2) {
			$subject = "Verifikasi Email Register Confirmation";
			$txt = "Hi!<br><br>
                    Terima kasih telah bergabung dengan Sky Parking.<br>
                   	Kami perlu memastikan bahwa email anda valid. Harap mengkonfirmasi email anda dengan mengklik link dibawah ini.<br>
                    <br>
                    <a href='".$link."'>Konfirmasi Pendaftaran</a>
                    <br>
                    <br>
                    Terima kasih.<br>
                    Salam,<br>
                    <br>
                    <br>
                    Sky Parking
                    <br>
                    <i>Powered by Baronang</i>
                    </p>";

			$curl = curl_init();
            $post = "toaddr=$email&subject=$subject&message=$txt";
            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://interzircon.com/phpmail.php",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => $post,
            ));
            $output = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
            $result = json_decode($output, true);

			$fun->setOneSession('error_content', 'Berhasil mendaftar!<br><br>Cek email untuk verfikasi', 2);
			header('location:../ei.php?'.$fun->setIDParam('title', 'Register Success!').'&'.$fun->setIDParam('link', 'index.php'));
		}
		else {
			$fun->setOneSession('error_content', 'Tidak bisa mendaftarkan!<br><br>Terjadi kesalahan dalam koneksi!<br><br>Silahkan coba lagi!', 2);
			header('location:../ei.php?'.$fun->setIDParam('title', 'Error Register!').'&'.$fun->setIDParam('link', 'index.php'));
		}

	}

}
?>
