<?php
require('../util/koneksi.php');

if(isset($_GET[$fun->Enlink('qr201')])) {
	$email = $fun->getIDParam('email');
	$pass  = $fun->getIDParam('pass');
}
else {
	$email = $fun->POST('email', 1);
	$pass  = $fun->ENID($fun->POST('pass', 1));
}


if($email == '' || empty($email) || is_null($email) || $pass == '' || empty($pass) || is_null($pass) ) {
	//$fun->setOneSession('error_content', 'Email/Password Tidak Boleh Kosong!<br>Silahkan Coba Lagi!', 2);
	//header('location:../ei.php?'.$fun->setIDParam('title', 'Error Login!').'&'.$fun->setIDParam('link', 'index.php'));
	header('location: ../index.php?'.$fun->setIDParam('status', 'Email/Password can\' be empty!<br>Try Again!'));
}
else {

	$checkemail = substr($email, -13);
	if($checkemail == '.baronang.com') {
		$s1 = "SELECT * from 23dmastertenant where tenantemail='$email' and tenantpassword='$pass'";
		$id = "mastertenantid";
		$email = "tenantemail";
		$name = "tenantname";
		$session = "tenant";
		$aktif = "status";
	}
	else {
		$s1 = "SELECT * from 22bmastermember where memberemail='$email' and memberpassword='$pass'";
		$id = "mastermemberid";
		$email = "memberemail";
		$name = "membername";
		$session = "member";
		$aktif = "active";
	}
	$q1 = mysqli_query($fun->getConnection(), $s1) or die(mysqli_error($fun->getConnection()));
	$d1 = mysqli_fetch_array($q1);

	if(count($d1) > 0) { 

		if($d1[$aktif] > 0) {

			if($d1[$aktif] == 3) {
				header('location: ../index.php?'.$fun->setIDParam('status', 'Approve tenant first..'));
			}
			else {

				if($session == "member") {
					$fun->setCookie(array("login" => "@syslog:[OK]=>@".date('YmdHis').'%'.rand(1,9).'#'.$fun->randChar('3').'%'.rand(10,99).'#'.$fun->randChar('5').'%'.rand(100,999).'#'.$fun->randChar('7'),
							  	  "id" 			=> $d1[$id],
							  	  "field_id" 	=> $id,
							  	  "locationid" 	=> $d1['locationid'],
							  	  "email" 		=> $d1[$email],
							  	  "field_email" => $email,
							  	  "name" 		=> $d1[$name],
								  "field_name" 	=> $name,
								  "session"		=> $session,
								  "exptime" 	=> (time()+(2*60*60))
								), 1, 2);
					header('location:../');
				}
				else {

					if($d1[$aktif] == 1) {
						header('location: ../index.php?'.$fun->setIDParam('status', 'Please signin with registered personal account'));
					}
					else {
						
						$fun->setCookie(array("login" => "@syslog:[OK]=>@".date('YmdHis').'%'.rand(1,9).'#'.$fun->randChar('3').'%'.rand(10,99).'#'.$fun->randChar('5').'%'.rand(100,999).'#'.$fun->randChar('7'),
							  	  "id" 			=> $d1[$id],
							  	  "field_id" 	=> $id,
							  	  "locationid" 	=> $d1['locationid'],
							  	  "email" 		=> $d1[$email],
							  	  "field_email" => $email,
							  	  "name" 		=> $d1[$name],
								  "field_name" 	=> $name,
								  "session"		=> $session,
								  "exptime" 	=> (time()+(2*60*60))
								), 1, 2);
						header('location:../');

					}

				}

			}

		}
		else {
			//$fun->setOneSession('error_content', 'Email ini belum diverifikasi!<br>Cek Email!', 2);
			//header('location:../ei.php?'.$fun->setIDParam('title', 'Error Login!').'&'.$fun->setIDParam('link', 'index.php'));
			header('location: ../index.php?'.$fun->setIDParam('status', 'This email is not verified yet!<br>Check email!'));
			//echo 'Email ini belum diverifikasi!<br>Cek Email!';
		}

	}
	else { 
		//$_SESSION['error_content'] = 'Username / Password Salah! Silahkan Coba Lagi!';
		//$fun->setOneSession('error_content', 'Email/Password Salah!<br>Silahkan Coba Lagi!', 2);
		//header('location:../ei.php?'.$fun->setIDParam('title', 'Error Login!').'&'.$fun->setIDParam('link', 'index.php'));
		header('location: ../index.php?'.$fun->setIDParam('status', 'Email/Password is wrong!<br>Try again!'));
		//echo 'Email/Password Salah!<br>Silahkan Coba Lagi!';
	}
}

?>

