<?php
require('../util/koneksi.php');
$email 	= $fun->POST('email', 1);
$pass 	= $fun->ENID($fun->POST('pass', 1));

if($email == '' || empty($email) || is_null($email) || 
	$pass == '' || empty($pass) || is_null($pass) ) {
	//$fun->setOneSession('error_content', 'Email/Password Tidak Boleh Kosong!<br>Silahkan Coba Lagi!', 2);
	//header('location:../ei.php?'.$fun->setIDParam('title', 'Error Register!').'&'.$fun->setIDParam('link', 'index.php'));
    header('location: ../index.php?'.$fun->setIDParam('status', 'Email/Password can\'t be empty!<br>Try again!'));
}
else {

	$s1 = "SELECT * from 22bmastermember where memberemail='$email'";
	$q1 = mysqli_query($fun->getConnection(), $s1) or die(mysqli_error($fun->getConnection()));
	$d1 = mysqli_fetch_array($q1);

	if( count($d1) > 0 ) {
	   	//$fun->setOneSession('error_content', 'Email ini sudah terdaftar!', 2);
	   	//header('location:../ei.php?'.$fun->setIDParam('title', 'Error Register!').'&'.$fun->setIDParam('link', '?pg=register'));
	   	header('location: ../index.php?pg=register&'.$fun->setIDParam('status', 'This Email is already registered!'));
    }
	else {

		$created = date('Y-m-d H:i:s');
		//$ukey = "@".$fun->Enlink($email).":#".date('ymdHis').'$#'.rand(1,9)."$#".rand(10,99)."$#".rand(100,999)."$#".$fun->randChar(5).'$#7000new';
		$ukey = "@".$fun->Enlink($email).":#".date('ymdHis').'$#7000new';
		$s2 = "INSERT into 23cOutgoingEmail values('', '$created', '', '$email', '$ukey', '2', '$pass')";
		$q2 = mysqli_query($fun->getConnection(), $s2) or die(mysqli_error($fun->getConnection()));
		$link = 'baronang.com/baronang/?pg=aktif%26'.$fun->setIDParam('activate', $ukey);
		//$link = 'baronang.com/baronang';
		//<a href='".$link."'>Konfirmasi Pendaftaran</a>

		if($q2) {
			$subject = "Verifikasi Email Register Confirmation";
			$txt = "Hi!<br><br>
                    Terima kasih telah bergabung dengan Sky Parking.<br>
                   	Kami perlu memastikan bahwa email anda valid. Harap mengkonfirmasi email anda dengan mengklik link dibawah ini.<br>
                    <br>
                    <a href=".$link.">Konfirmasi</a>
                    <br>
                    <br>
                    Terima kasih.<br>
                    Salam,<br>
                    <br>
                    <br>
                    Sky Parking
                    <br>
                    <i>Powered by Baronang</i>
                    </p>";

			$curl = curl_init();
            $post = "toaddr=$email&subject=$subject&message=$txt";
            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://interzircon.com/phpmail.php",
                //CURLOPT_URL => "https://dodo.web.id/baronang/phpmailer-bap.php",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => $post,
            ));
            $output = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
            $result = json_decode($output, true);

			//$fun->setOneSession('error_content', 'Berhasil mendaftar!<br>Cek email untuk verfikasi!', 2);
			//header('location:../ei.php?'.$fun->setIDParam('title', 'Register Success!').'&'.$fun->setIDParam('link', 'index.php'));
			header('location: ../index.php?'.$fun->setIDParam('status', 'Success registering!<br>Check Email for verification!'));
        }
		else {
			//$fun->setOneSession('error_content', 'Terjadi kesalahan dalam koneksi!<br>Silahkan coba lagi!', 2);
			//header('location:../ei.php?'.$fun->setIDParam('title', 'Error Register!').'&'.$fun->setIDParam('link', 'index.php'));
			header('location: ../index.php?'.$fun->setIDParam('status', 'An error occurred in the connection!<br>Check connection!'));
		}

	}

}
?>
