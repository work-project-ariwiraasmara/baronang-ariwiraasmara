<?php
require('../util/koneksi.php');

$total 	 	= $fun->POST('total', 2); // TOTAL BELANJA

if($total == '' || empty($total)) {
	header('location:../?pg=shop&'.$fun->setIDParam('status', 'Amount of shopping can\'t be empty!'));
}
else {

	$memberid 	= $fun->getValookie('id');
	$payment 	= $fun->POST('paymentto', 1); // METODE PEMBAYARAN
	$card 		= $fun->POST('card', 2); // CARD ID

	$date = date('Y-m-d H:i:s');
	$dateinvalid = date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s') . ' +3 hours'));
	$like = '7107'.date('ym');
	
	$totalpoint = (int)$total/1000;
	$splittotal = 0;

	$smember = "SELECT * from 22bmastermember where mastermemberid='$memberid'";
	$qmember = mysqli_query($fun->getConnection(), $smember) or die(mysqli_error($fun->getConnection()));
	$dmember = mysqli_fetch_array($qmember);

	// OWN CARD'S POINTS
	$s10 = "SELECT points from 23amastercard where mastercardid='$card'";;
	$q10 = mysqli_query($fun->getConnection(), $s10) or die(mysqli_error($fun->getConnection()));
	$d10 = mysqli_fetch_array($q10);

	//
	$s1 = "SELECT invoiceid from 23fproductinvoice where invoiceid like '$like%' order by invoiceid desc limit 1";
	$q1 = mysqli_query($fun->getConnection(), $s1) or die(mysqli_error($fun->getConnection()));
	$r1 = mysqli_num_rows($q1);

	if($r1 > 0) {
		$d1 = mysqli_fetch_array($q1);
		$id = (int)substr($d1['invoiceid'], -8) + 1;
	}
	else {
		$id = 1;
	}

	if($payment == 'skypoint-vabca') {
		if($d10['points'] > $totalpoint) {
			$payment = 'skypoint';
		}
	}


	$id = str_pad($id, 8, '0', STR_PAD_LEFT);
	$invoice = $like.$id;

	$no = 1;

	if(isset($_POST[$fun->Enlink('id')])) {

		$s2 = "SELECT activitylogid from 31amastercardproductowned order by activitylogid desc limit 1";
		$q2 = mysqli_query($fun->getConnection(), $s2) or die(mysqli_error($fun->getConnection()));
		$d2 = mysqli_fetch_array($q2);
		$activitylogid = (int)$d2['activitylogid'] + 1;

		$s6 = "INSERT into 31amastercardproductowned values"; 

		foreach(@$_POST[$fun->Enlink('id')] as $key) { //$key => SHOP ID & QUANTITY
			$idpos1		= strpos($key,"ID")+5; // FILTER: PROSES DAPAT SHOP ID 1
			$idpos2		= strpos($key,"QTY")-3; // FILTER: PROSES DAPAT SHOP ID 2
			$id  		= $fun->Denval(substr($key, $idpos1, ($idpos2-$idpos1))); // FILTER: DAPAT SHOP ID

			$qtypos		= strpos($key,"QTY")+6; // FILTER: PROSES DAPAT QUANTITY
			$jml		= $fun->Denval(substr($key, $qtypos)); // FILTER: DAPAT QUANTITY
			// qty = '$qty',
			//echo 'STR: '.$key.'<br>';
			//echo 'ID: '.$id.'<br>';
			//echo 'QTY: '.$qty.'<br>';

			$s3 = "UPDATE 23eproductshop set dateshop = '$date', 
											 invoiceid = '$invoice',
											 qty = '$jml',
											 status = '1'
					where productshopid='$id'";
			//echo $s2.'<br><br>';
			$q3 = mysqli_query($fun->getConnection(), $s3) or die(mysqli_error($fun->getConnection()));

			if( $payment == 'skypoint' ) {

				//Product Owned
				$s4 = "SELECT * from 23eproductshop where productshopid='$id'";
				$q4 = mysqli_query($fun->getConnection(), $s4) or die(mysqli_error($fun->getConnection()));
				$d4 = mysqli_fetch_array($q4);


				$ProductID = $d4['productid'];
				$CardID = $d4['cardid'];
				$Start = $d4['datestart'];
				$StartDate = new datetime($d4['datestart']);
				$StartFrom = strtotime($d4['datestart']);


				$s5 = "SELECT * from 22amasterproduct where masterproductid='$ProductID' and active='1'";
				$q5 = mysqli_query($fun->getConnection(), $s5) or die(mysqli_error($fun->getConnection()));
				$d5 = mysqli_fetch_array($q5);

				$prodcate = $d5['category'];
				$qty = $d5['validity_days'];


				Switch ($prodcate){
					case '01':
						$interval = $qty.' days';
						$NewDate = date_add($StartDate, date_interval_create_from_date_string($interval));
						$ExpDate = date_format(date_time_set($NewDate,23,59,59), 'Y-m-d H:i:s');
						break;
						  
					case '02':
						$d = date("Y-m-d H:i:s", strtotime("Sunday"));
						if (($d > $StartDate) and ($qty == 1)){
							$NewDate = new datetime($d);
							$ExpDate = date_format(date_time_set($NewDate,23,59,59), 'Y-m-d H:i:s');
						}
						elseif (($d < $StartDate) and ($qty == 1)){
							$week = date("W", $StartFrom);
							$year = date("Y", $StartFrom);
							$ExpDate = date("Y-m-d 23:59:59", strtotime($year.'W'.str_pad($week, 2, 0, STR_PAD_LEFT).'7'));
						}
						else {
							$interval = ($qty-1).' weeks';
							$EndDay = date_add($StartDate, date_interval_create_from_date_string($interval));
							$Start = strtotime(date_format($EndDay, "Y-m-d"));
							$week = date("W", $Start);
							$year = date("Y", $Start);
							$ExpDate = date("Y-m-d 23:59:59", strtotime($year.'W'.str_pad($week, 2, 0, STR_PAD_LEFT).'7'));
						}
						break;
						  
					case '03':
						$interval = $qty.' weeks';
						$NewDate = date_add($StartDate, date_interval_create_from_date_string($interval));
						$NewDate2 = date_add($NewDate, date_interval_create_from_date_string("-1 days"));
						$ExpDate = date_format(date_time_set($NewDate2,23,59,59), 'Y-m-d H:i:s');
						break;
						  
					case '04':
						$m = date("m", $StartFrom);
						if (($m == date('m')) and ($qty == 1)){
							$EndDate = date_format(date_time_set($StartDate,23,59,59), 'Y-m-d H:i:s');
							$ExpDate = date("Y-m-t H:i:s", strtotime($EndDate));
						}
						else {
							$interval = ($qty-1).' months';
							$EndDay = date_add($StartDate, date_interval_create_from_date_string($interval));
							$EndDate = date_format(date_time_set($EndDay,23,59,59), 'Y-m-d H:i:s');
							$ExpDate = date("Y-m-t H:i:s", strtotime($EndDate));
						}	
						break;
						  
					case '05':
						$interval = $qty.' months';
						$NewDate = date_add($StartDate, date_interval_create_from_date_string($interval));
						$NewDate2 = date_add($NewDate, date_interval_create_from_date_string("-1 days"));
						$ExpDate = date_format(date_time_set($NewDate2,23,59,59), 'Y-m-d H:i:s');
						break;
						  
				}
				$quota = $d5['quota'];
				$counter = $d5['counter'] + 1;
				//$newcounter = $counter + 1;
				$serialnumber = substr($ProductID, 0, 14).str_pad($counter, 6, "0", STR_PAD_LEFT);

				$sserial = "INSERT into 22fproductserialnumber values('$serialnumber', '$ProductID', '$date',1)";
				$qserial = mysqli_query($fun->getConnection(), $sserial) or die(mysqli_error($fun->getConnection()));

				if ($quota == $counter){
					$scounterprod = "Update 22amasterproduct set counter = '$counter', active = 2 where masterproductid = '$ProductID'";
				}
				else {
					$scounterprod = "Update 22amasterproduct set counter = '$counter' where masterproductid = '$ProductID'";
				}
				$qcounterprod = mysqli_query($fun->getConnection(), $scounterprod) or die(mysqli_error($fun->getConnection()));	

				for($po = 0; $po < $qty; $po++) {
					$s6 .= "('$activitylogid', '$date', '$CardID', '$ProductID', '$Start', '$ExpDate', '$serialnumber', '1', '0', '0'),";
					$activitylogid++;
				}


				//CardActivity
				$locid = $d5['locationid'];
				$MemberID = $d4['memberid'];
				$price = $d5['productprice'];
				$pricepoint = $price / 1000;
				$cardActivity = '7105'.date('y').'205'.date('m');
				$scardact1 = "SELECT count(*) as jmlactivity from 31cmastercardactivity where activitylogid like '".$cardActivity."%'";
				$qcardact1 = mysqli_query($fun->getConnection(), $scardact1) or die(mysqli_error($fun->getConnection()));
				$dcardact1 = mysqli_fetch_array($qcardact1);
				$rcardact1 = $dcardact1['jmlactivity'];
					  
				$scardact2 = "SELECT activitylogid from 31cmastercardactivity where activitylogid like '".$cardActivity."%' order by activitylogid desc limit 1";
				$qcardact2 = mysqli_query($fun->getConnection(), $scardact2) or die(mysqli_error($fun->getConnection()));
				$dcardact2 = mysqli_fetch_array($qcardact2);
							  
				if($rcardact1 > 0) {
					$noactivity = (int)substr($dcardact2['activitylogid'], -5) + 1;
				}
				else {
					$noactivity = '00001';
				}

				$activityID = substr($cardActivity, 0, 11).str_pad($noactivity, 5, "0", STR_PAD_LEFT);
				$sactivity = "SELECT description from 15activitycode where activitycodeid = '205'";
				$qactivity = mysqli_query($fun->getConnection(), $sactivity) or die(mysqli_error($fun->getConnection()));
				$dactivity = mysqli_fetch_array($qactivity);
				$description = $dactivity['description'];
				$scardact3 = "INSERT into 31cmastercardactivity values
				('$activityID', '$CardID', '$date', '$description', $price, 0, $pricepoint, 0, '205', 
				'$ProductID', '$MemberID', '$locid', '-')";
				$qcardact3 = mysqli_query($fun->getConnection(), $scardact3) or die(mysqli_error($fun->getConnection()));

			}

		}

		if($payment == 'skypoint') {
			$length_s6 = (int)strlen($s6);
			$s6 = substr($s6, 0, $length_s6-1).';';
			//echo $s6;
			$q6 = mysqli_query($fun->getConnection(), $s6) or die(mysqli_error($fun->getConnection()));

			$statusinvoice = 1;
			$cardinvoice = $card;

			if( $d10['points'] >= $totalpoint ) {
				$totalpaid = (int)$total;
				$splittotal = 0;
				$sisa = (int)$d10['points'] - (int)$totalpoint;
			}
			else {
				$totalpaid  = (int)$d10['points'] * 1000;
				$splittotal = 0;
				$sisa = 0;
			}

			$paymentsplit = 'skypoint';
		}
		else if($payment == 'vabca') {
			$statusinvoice = 2;
			$cardinvoice = "5278".$dmember['memberphone'];

			$totalpaid = (int)$total;
			$splittotal = 0;

			$paymentsplit = 'vabca';
		}
		else {
			$length_s6 = (int)strlen($s6);
			$s6 = substr($s6, 0, $length_s6-1).';';
			//echo $s6;
			//$q6 = mysqli_query($fun->getConnection(), $s6) or die(mysqli_error($fun->getConnection()));

			$statusinvoice = 2;
			$cardinvoice = "5278".$dmember['memberphone'];

			$splittotal = (int)$d10['points'] * 1000;
			$totalpaid  = $splittotal;
			$sisa = 0;
			
			$paymentsplit = 'skypoint';
		}
		
		$s7 = "INSERT into 23fproductinvoice values('$invoice', '$date', '$memberid', '$payment', '$cardinvoice', '$total', '$statusinvoice', '$dateinvalid', '$totalpaid')";
		$q7 = mysqli_query($fun->getConnection(), $s7) or die(mysqli_error($fun->getConnection()));

		$s8 = "SELECT paymentid from 23gpaymentproduct where invoiceid='$invoice' order by paymentid desc limit 1";
		$q8 = mysqli_query($fun->getConnection(), $s8) or die(mysqli_error($fun->getConnection()));
		$r8 = mysqli_num_rows($q8);
		if($r8 > 0) {
			$d8 = mysqli_fetch_array($q8);
			$pid = (int)substr($d8['paymentid'], -5) + 1;
			$paymentid = $invoice.str_pad($pid, 5, "0", STR_PAD_LEFT);
		}
		else {
			$paymentid = $invoice.str_pad(1, 5, "0", STR_PAD_LEFT);
		}

		$s9 = "INSERT into 23gpaymentproduct values('$paymentid', '$date', '$invoice', '$memberid', '$paymentsplit', '$splittotal')";
		$q9 = mysqli_query($fun->getConnection(), $s9) or die(mysqli_error($fun->getConnection()));

		if($q9) {

			if($payment == 'skypoint' || $payment == 'skypoint-vabca') {
				$s11 = "UPDATE 23amastercard set points='$sisa' where mastercardid='$card' and memberid='$memberid'";
				$q11 = mysqli_query($fun->getConnection(), $s11) or die(mysqli_error($fun->getConnection()));
			}

			//echo $payment.' : '.$fun->POST('paymentto', 1);

			header('location:../?pg=shop&sb=invoice&'.$fun->setIDParam('status', 'Succesful on transaction!'));
		}
		else {
			header('location:../?pg=shop&sb=invoice&'.$fun->setIDParam('status', 'Transaction Fail!<br>Try Again!'));
		}
	}
	else {
		header('location:../?'.$fun->setIDParam('status', 'There is nothing to process on transaction!'));
	}

}
?>
