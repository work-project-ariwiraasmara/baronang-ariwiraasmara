<?php
$postdata = file_get_contents("php://input");
$data = json_decode($postdata, true);
if($data != null){
    if(isset($data['Base64']) and isset($data['Filename'])){
        $base64 = $data['Base64'];
        $filename = $data['Filename'];

        $output_file = 'images/'.$filename.'.png';
        // open the output file for writing
        $ifp = fopen($output_file, 'wb');

        // split the string on commas
        // $data[ 0 ] == "data:image/png;base64"
        // $data[ 1 ] == <actual base64 string>
        //$data = explode( ',', $base64_string );

        // we could add validation here with ensuring count( $data ) > 1
        fwrite($ifp, base64_decode($base64));

        // clean up the file resource
        fclose($ifp);

        echo 'Success save to '.$output_file;
    }
    else{
        echo 'Invalid parameter';
    }
}
?>
