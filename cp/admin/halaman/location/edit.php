<?php
$sp1 = "SELECT * from ".$funct->getValookie('koneksi').".location where LocationID='".$funct->getIDParam('id')."'";
$qp1 = mysqli_query($funct->getConnection(), $sp1) or die(mysqli_error($funct->getConnection()));
$dp1 = mysqli_fetch_array($qp1);
?>

<div id="toolbar" class="primary-color">
    <a href="?pg=location&sb=index" class="open-left">
        <i class="ion-android-arrow-back"></i>
    </a>
    <span class="title bold txt-white" id="title"></span>
</div>

<div class="animated fadeinup delay-1">
	<div class="page-content">

		<div class="form-inputs">
			<form action="" method="post">

				<div class="input-field">
					<input type="text" name="nama" id="nama" class="validate" value="<?php echo $dp1['Nama']; ?>">
					<label for="nama">Nama</label>
				</div>

				<div class="input-field">
					<input type="text" name="alamat" id="alamat" class="validate" value="<?php echo $dp1['Alamat']; ?>">
					<label for="alamat">Alamat</label>
				</div>

				<div class="input-field">
					<input type="tel" name="tlp" id="tlp" class="validate" value="<?php echo $dp1['Telepon']; ?>">
					<label for="tlp">No. Tlp</label>
				</div>

				<div class="input-field">
					<input type="text" name="desk" id="desk" class="validate" value="<?php echo $dp1['Deskripsi']; ?>">
					<label for="desk">Deskripsi</label>
				</div>

				<div class="input-field">
					<input type="number" name="code" id="code" class="validate" value="<?php echo $dp1['Code']; ?>">
					<label for="code">Code</label>
				</div>

				<div class="input-field">
					<input type="number" name="bal" id="bal" class="validate" value="<?php echo $dp1['Balance']; ?>">
					<label for="bal">Balance</label>
				</div>

				<div class="input-field">
					<input type="number" name="gpin" id="gpin" class="validate" value="<?php echo $dp1['GPIn']; ?>">
					<label for="gpin">GP In</label>
				</div>

				<div class="input-field">
					<input type="number" name="gpout" id="gpout" class="validate" value="<?php echo $dp1['GPOut']; ?>">
					<label for="gpout">GP Out</label>
				</div>

				<div class="input-field">
					<input type="text" name="tiket" id="tiket" class="validate" value="<?php echo $dp1['TicketCode']; ?>">
					<label for="tiket">Ticket Code</label>
				</div>

				<div class="input-field">
					<input type="text" name="ovolocid" id="ovolocid" class="validate" value="<?php echo $dp1['OvoLocationID']; ?>">
					<label for="ovolocid">OVO Location ID</label>
				</div>

				<div class="input-field">
					<input type="text" name="ovococo" id="ovococo" class="validate" value="<?php echo $dp1['OvoCompanyCode']; ?>">
					<label for="ovococo">OVO Company Code</label>
				</div>

				<button type="submit" name="btnok" class="btn btn-large width-100 waves-effect waves-large primary-color borad-30 m-t-30">
					OK
				</button>
			</form>

			<?php
			if(isset($_POST['btnok'])) {
				$nama 		= @$_POST['nama'];
				$alamat 	= @$_POST['alamat'];
				$tlp 		= @$_POST['tlp'];
				$desk 		= @$_POST['desk'];
				$code 		= @$_POST['code'];
				$bal 		= @$_POST['bal'];
				$gpin 		= @$_POST['gpin'];
				$gpout 		= @$_POST['gpout'];
				$tiket 		= @$_POST['tiket'];
				$ovolocid 	= @$_POST['ovolocid'];
				$ovococo 	= @$_POST['ovococo'];

				$s2 = "UPDATE ".$funct->getValookie('koneksi').".location set Nama='$nama', Alamat='$alamat', Telepon='$tlp', Deskripsi='$desk', Code='$code', Balance='$bal', GPIn='$gpin', GPOut='$gpout', TicketCode='$tiket', OvoLocationID='$ovolocid', OvoCompanyCode='$ovococo' where LocationID='".$funct->getIDParam('id')."'";
				$q2 = mysqli_query($funct->getConnection(), $s2) or die(mysqli_error($funct->getConnection()));
				if($q2) { ?>
					<script type="text/javascript">
						alert('Update Data Location Berhasil!');
						window.location.href = "?pg=location&sb=index";
					</script>
					<?php
				}
			}
			?>

		</div>

	</div>
</div>