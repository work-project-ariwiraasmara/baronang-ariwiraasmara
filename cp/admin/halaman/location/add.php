<div id="toolbar" class="primary-color">
    <a href="?pg=location&sb=index" class="open-left">
        <i class="ion-android-arrow-back"></i>
    </a>
    <span class="title bold txt-white" id="title"></span>
</div>

<div class="animated fadeinup delay-1">
	<div class="page-content">

		<div class="form-inputs">
			<form action="" method="post">

				<div class="input-field">
					<input type="text" name="nama" id="nama" class="validate">
					<label for="nama">Nama</label>
				</div>

				<div class="input-field">
					<input type="text" name="alamat" id="alamat" class="validate">
					<label for="alamat">Alamat</label>
				</div>

				<div class="input-field">
					<input type="tel" name="tlp" id="tlp" class="validate">
					<label for="tlp">No. Tlp</label>
				</div>

				<div class="input-field">
					<input type="text" name="desk" id="desk" class="validate">
					<label for="desk">Deskripsi</label>
				</div>

				<div class="input-field">
					<input type="number" name="code" id="code" class="validate">
					<label for="code">Code</label>
				</div>

				<div class="input-field">
					<input type="number" name="bal" id="bal" class="validate">
					<label for="bal">Balance</label>
				</div>

				<div class="input-field">
					<input type="number" name="gpin" id="gpin" class="validate">
					<label for="gpin">GP In</label>
				</div>

				<div class="input-field">
					<input type="number" name="gpout" id="gpout" class="validate">
					<label for="gpout">GP Out</label>
				</div>

				<div class="input-field">
					<input type="text" name="tiket" id="tiket" class="validate">
					<label for="tiket">Ticket Code</label>
				</div>

				<div class="input-field">
					<input type="text" name="ovolocid" id="ovolocid" class="validate">
					<label for="ovolocid">OVO Location ID</label>
				</div>

				<div class="input-field">
					<input type="text" name="ovococo" id="ovococo" class="validate">
					<label for="ovococo">OVO Company Code</label>
				</div>

				<button type="submit" name="btnok" class="btn btn-large width-100 waves-effect waves-large primary-color borad-30 m-t-30">
					OK
				</button>
			</form>

			<?php
			if(isset($_POST['btnok'])) {
				$nama 		= @$_POST['nama'];
				$alamat 	= @$_POST['alamat'];
				$tlp 		= @$_POST['tlp'];
				$desk 		= @$_POST['desk'];
				$code 		= @$_POST['code'];
				$bal 		= @$_POST['bal'];
				$gpin 		= @$_POST['gpin'];
				$gpout 		= @$_POST['gpout'];
				$tiket 		= @$_POST['tiket'];
				$ovolocid 	= @$_POST['ovolocid'];
				$ovococo 	= @$_POST['ovococo'];

				$s1 = "SELECT LocationID from ".$funct->getValookie('koneksi').".location order by LocationID DESC limit 1";
				$q1 = mysqli_query($funct->getConnection(), $s1) or die(mysqli_error($funct->getConnection()));
				$d1 = mysqli_fetch_array($q1);

				$graid = substr($d1['LocationID'], -3);
                $gp = (int)$graid + 1;
                $id = 'LOC'.str_pad($gp, 3, '0', STR_PAD_LEFT);

				$s2 = "INSERT into ".$funct->getValookie('koneksi').".location values('$id', '', '$nama', '$alamat', '$tlp', '1', null, null, '$desk', null, '$code', '$bal', '$gpin', '$gpout', '0', '0', '$tiket', '$ovolocid', '$ovococo')";
				$q2 = mysqli_query($funct->getConnection(), $s2) or die(mysqli_error($funct->getConnection()));
				if($q2) { ?>
					<script type="text/javascript">
						alert('Tambah Data Location Berhasil!');
						window.location.href = "?pg=location&sb=index";
					</script>
					<?php
				}
			}
			?>

		</div>

	</div>
</div>