<div id="toolbar" class="primary-color">
    <div class="open-left" id="open-left" data-activates="slide-out-left">
        <i class="ion-android-menu"></i>
    </div>
    <span class="title bold txt-white" id="title"></span>
    <div class="open-right">
        <a href="?pg=location&sb=add">
        	<i class="ion-android-add txt-white"></i>
        </a>
    </div>
</div>

<div class="animated fadeinup delay-1">
	<div class="page-content">
		<div class="">

			<table class="table">
				<thead>
					<tr>
						<th class="nowrap">Location ID</th>
						<th class="nowrap">Reference ID</th>
						<th class="nowrap">Nama</th>
						<th class="nowrap">Alamat</th>
						<th class="nowrap">Telepon</th>
						<th class="nowrap">Status</th>
						<th class="nowrap">Lon</th>
						<th class="nowrap">Lat</th>
						<th class="nowrap">Deskripsi</th>
						<th class="nowrap">Gambar</th>
						<th class="nowrap">Code</th>
						<th class="nowrap">Balance</th>
						<th class="nowrap">GPIn</th>
						<th class="nowrap">GPout</th>
						<th class="nowrap">Is Only Member?</th>
						<th class="nowrap">Is Free?</th>
						<th class="nowrap">Ticket Code</th>
						<th class="nowrap">Ovo Location ID</th>
						<th class="nowrap">Ovo Company Code</th>
						<th colspan="3"></th>
					</tr>
				</thead>

				<tbody>
					<?php
					$page = isset($_GET['hlm']) ? intval($_GET['hlm']) : 1; // untuk nomor halaman
					$adjacents = isset($_GET['adjacents']) ? intval($_GET['adjacents']) : 0; // khusus style pagination 2 dan 3
					$rpp = 10; // jumlah record per halaman

					$sql = "SELECT * from ".$funct->getValookie('koneksi').".location";
					$qry = mysqli_query($funct->getConnection(), $sql) or die(mysqli_error($funct->getConnection()));

					$tcount = mysqli_num_rows($qry); // jumlah total baris
					$tpages = isset($tcount) ? ceil($tcount / $rpp) : 1; // jumlah total halaman
					$count = 0; // untuk paginasi
					$i = ($page - 1) * $rpp; // batas paginasi
					$no_urut = ($page - 1) * $rpp; // nomor urut
					$reload = "?pg=location&sb=index&amp;adjacents=" . $adjacents; // untuk link ke halaman lain
					
					//while($data = mysqli_fetch_array($qry)) { 
					while ( ($count < $rpp) && ($i < $tcount) ) {
						mysqli_data_seek($qry, $i);
						$data = mysqli_fetch_array($qry);
						?>
						<tr>
							<td class="nowrap"><?php echo $data['LocationID']; ?></td>
							<td class="nowrap"><?php echo $data['ReferenceID']; ?></td>
							<td class="nowrap"><?php echo $data['Nama']; ?></td>
							<td class="nowrap"><?php echo $data['Alamat']; ?></td>
							<td class="nowrap"><?php echo $data['Telepon']; ?></td>
							<td class="nowrap"><?php echo $data['Status']; ?></td>
							<td class="nowrap"><?php echo $data['Lon']; ?></td>
							<td class="nowrap"><?php echo $data['Lat']; ?></td>
							<td class="nowrap"><?php echo $data['Deskripsi']; ?></td>
							<td class="nowrap"><?php echo $data['Gambar']; ?></td>
							<td class="nowrap"><?php echo $data['Code']; ?></td>
							<td class="nowrap"><?php echo $data['Balance']; ?></td>
							<td class="nowrap"><?php echo $data['GPIn']; ?></td>
							<td class="nowrap"><?php echo $data['GPOut']; ?></td>
							<td class="nowrap"><?php echo $data['isOnlyMember']; ?></td>
							<td class="nowrap"><?php echo $data['isFree']; ?></td>
							<td class="nowrap"><?php echo $data['TicketCode']; ?></td>
							<td class="nowrap"><?php echo $data['OvoLocationID']; ?></td>
							<td class="nowrap"><?php echo $data['OvoCompanyCode']; ?></td>
							<td>
								<a href="<?php echo '?pg=location&sb=edit&'.$funct->Enlink('id').'='.$funct->Enval($data['LocationID']); ?>" class="waves-effect waves-light"><i class="ion-android-create"></i></a>
							</td>
							<td>
								<a href="#" id="<?php echo 'delete'.$data['LocationID']; ?>" class="waves-effect waves-light"><i class="ion-android-delete"></i></a>
							</td>
							<td style="color: transparent;">--</td>

							<script type="text/javascript">
								$('<?php echo '#delete'.$data['LocationID']; ?>').click(function(){
									confirmDelete('location', 'LocationID', '<?php echo $data['LocationID'] ?>', '?pg=location&sb=index');
								});
							</script>
						</tr>
						<?php
						$i++;
                    	$count++;
					}
					?>
				</tbody>
			</table>

			<div class="m-t-30">
				<?php echo $funct->paginate_one($reload, $page, $tpages, $adjacents); ?>
			</div>

		</div>
	</div>
</div>