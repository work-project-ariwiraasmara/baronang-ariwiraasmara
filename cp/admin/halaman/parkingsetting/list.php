<div id="toolbar" class="primary-color">
    <div class="open-left" id="open-left" data-activates="slide-out-left">
        <i class="ion-android-menu"></i>
    </div>
    <span class="title bold txt-white" id="title"></span>
    <div class="open-right">
        <a href="?pg=parkingsetting&sb=add">
        	<i class="ion-android-add txt-white"></i>
        </a>
    </div>
</div>

<div class="animated fadeinup delay-1">
	<div class="page-content">
		<div class="">
			<table class="table">
				<thead>
					<tr>
						<th>Point Conversion</th>
						<th>Value Conversion</th>
						<th>Max Top Up EDC</th>
						<th>Max Point User</th>
						<th colspan="2"></th>
					</tr>
				</thead>

				<tbody>
					<?php
					$page = isset($_GET['hlm']) ? intval($_GET['hlm']) : 1; // untuk nomor halaman
					$adjacents = isset($_GET['adjacents']) ? intval($_GET['adjacents']) : 0; // khusus style pagination 2 dan 3
					$rpp = 10; // jumlah record per halaman

					$sql = "SELECT * from ".$funct->getValookie('koneksi').".parkingsetting";
					$qry = mysqli_query($funct->getConnection(), $sql) or die(mysqli_error($funct->getConnection()));

					$tcount = mysqli_num_rows($qry); // jumlah total baris
					$tpages = isset($tcount) ? ceil($tcount / $rpp) : 1; // jumlah total halaman
					$count = 0; // untuk paginasi
					$i = ($page - 1) * $rpp; // batas paginasi
					$no_urut = ($page - 1) * $rpp; // nomor urut
					$reload = "?pg=parkingsetting&sb=index&amp;adjacents=" . $adjacents; // untuk link ke halaman lain
					
					//while($data = mysqli_fetch_array($qry)) { 
					while ( ($count < $rpp) && ($i < $tcount) ) {
						mysqli_data_seek($qry, $i);
						$data = mysqli_fetch_array($qry);
						?>
						<tr>
							<td><?php echo $funct->FormatNumber($data['PointConversion']); ?></td>
							<td><?php echo $funct->FormatNumber($data['ValueConversion']); ?></td>
							<td><?php echo $funct->FormatNumber($data['MaxTopUpEDC']); ?></td>
							<td><?php echo $funct->FormatNumber($data['MaxPointUser']); ?></td>
							<td>
								<a href="<?php echo '?pg=parkingsetting&sb=edit&'.$funct->setIDParam('id', $data['ID']); ?>" class="waves-effect waves-light"><i class="ion-android-create"></i></a>
							</td>
							<td>
								<a href="#" id="<?php echo 'delete'.$data['ID']; ?>" class="waves-effect waves-light"><i class="ion-android-delete"></i></a>
							</td>

							<script type="text/javascript">
								$('<?php echo '#delete'.$data['ID']; ?>').click(function(){
									confirmDelete('vehicletype', 'ID', '<?php echo $data['ID'] ?>', '?pg=parkingsetting&sb=index');
								});
							</script>
						</tr>
						<?php
						$i++;
                    	$count++;
					}
					?>
				</tbody>
			</table>

			<div class="m-t-30">
				<?php echo $funct->paginate_one($reload, $page, $tpages, $adjacents); ?>
			</div>

		</div>
	</div>
</div>