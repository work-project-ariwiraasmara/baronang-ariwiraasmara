<div id="toolbar" class="primary-color">
    <a href="?pg=scheme&sb=index" class="open-left">
        <i class="ion-android-arrow-back"></i>
    </a>
    <span class="title bold txt-white" id="title"></span>
</div>

<div class="animated fadeinup delay-1">
	<div class="page-content">

		<div class="form-inputs">
			<form action="" method="post">
				<div class="input-field">
					<input type="number" name="max" id="max" class="validate">
					<label for="max">Max</label>
				</div>

				<div class="input-field">
					<input type="number" name="interval" id="interval" class="validate">
					<label for="interval">Interval</label>
				</div>

				<button type="submit" name="btnok" class="btn btn-large width-100 waves-effect waves-large primary-color borad-30 m-t-30">
					OK
				</button>
			</form>

			<?php
			if(isset($_POST['btnok'])) {
				$max 		= @$_POST['max']; 
				$interval 	= @$_POST['interval'];

				$s1 = "SELECT PricingSchemeID from ".$funct->getValookie('koneksi').".pricingscheme order by PricingSchemeID DESC limit 1";
				$q1 = mysqli_query($funct->getConnection(), $s1) or die(mysqli_error($funct->getConnection()));
				$d1 = mysqli_fetch_array($q1);

				$graid = substr($d1['PricingSchemeID'], -2);
                $gp = (int)$graid + 1;
                $id = 'scheme'.str_pad($gp, 2, '0', STR_PAD_LEFT);

				$s2 = "INSERT into ".$funct->getValookie('koneksi').".pricingscheme values('$id', '$max', '$interval')";
				$q2 = mysqli_query($funct->getConnection(), $s2) or die(mysqli_error($funct->getConnection()));
				if($q2) { ?>
					<script type="text/javascript">
						alert('Tambah Data Scheme Berhasil!');
						window.location.href = "?pg=scheme&sb=index";
					</script>
					<?php
				}
			}
			?>

		</div>

	</div>
</div>