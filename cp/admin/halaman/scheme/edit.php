<?php
$sp1 = "SELECT * from ".$funct->getValookie('koneksi').".pricingscheme where PricingSchemeID='".$funct->getIDParam('id')."'";
$qp1 = mysqli_query($funct->getConnection(), $sp1) or die(mysqli_error($funct->getConnection()));
$dp1 = mysqli_fetch_array($qp1);
?>

<div id="toolbar" class="primary-color">
    <a href="?pg=scheme&sb=index" class="open-left">
        <i class="ion-android-arrow-back"></i>
    </a>
    <span class="title bold txt-white" id="title"></span>
</div>

<div class="animated fadeinup delay-1">
	<div class="page-content">

		<div class="form-inputs">
			<form action="" method="post">
				<div class="input-field">
					<input type="number" name="max" id="max" class="validate" value="<?php echo $dp1['Max']; ?>">
					<label for="max">Max</label>
				</div>

				<div class="input-field">
					<input type="number" name="interval" id="interval" class="validate" value="<?php echo $dp1['ParkInterval']; ?>">
					<label for="interval">Interval</label>
				</div>

				<button type="submit" name="btnok" class="btn btn-large width-100 waves-effect waves-large primary-color borad-30 m-t-30">
					OK
				</button>
			</form>

			<?php
			if(isset($_POST['btnok'])) {
				$max 		= @$_POST['max']; 
				$interval 	= @$_POST['interval'];

				$s2 = "UPDATE ".$funct->getValookie('koneksi').".pricingscheme SET Max='$max', ParkInterval='$interval' where PricingSchemeID='".$funct->getIDParam('id')."'";
				$q2 = mysqli_query($funct->getConnection(), $s2) or die(mysqli_error($funct->getConnection()));
				if($q2) { ?>
					<script type="text/javascript">
						alert('Update Data Scheme Berhasil!');
						window.location.href = "?pg=scheme&sb=index";
					</script>
					<?php
				}
			}
			?>

		</div>

	</div>
</div>