<?php
if(isset($_GET['b'])) { $b = $funct->Denval(@$_GET['b']); } else { $b = date('m'); }
if(isset($_GET['y'])) { $y = $funct->Denval(@$_GET['y']); } else { $y = date('Y'); }

if(isset($_GET['detail'])) {
	if(isset($_GET['type'])) {
		// HEADER { 
			$objWorkSheet->getStyle('A1')->getFont()->setSize(20);
			$objWorkSheet->getStyle('A1')->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:E1');
			$objWorkSheet->SetCellValue('A1', 'Laporan Casual');
			$objWorkSheet->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		
			$objWorkSheet->getStyle('A2')->getFont()->setSize(16);
			$objWorkSheet->getStyle('A2')->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:E2');
			$objWorkSheet->SetCellValue('A2', date('F Y', strtotime($y.'-'.$b.'-1')));
			$objWorkSheet->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		
			// NOMOR { 
				$objPHPExcel->getActiveSheet()
	                        ->getStyle('A')
	                        ->getNumberFormat()
	                        ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

	            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(8);
				$objWorkSheet->getStyle('A4')->getFont()->setBold(true);
				$objWorkSheet->SetCellValue('A4', 'No.');
				$objWorkSheet->getStyle('A4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				$objWorkSheet->getStyle('A4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			// }

			// TANGGAL { 
				$objPHPExcel->getActiveSheet()
	                        ->getStyle('B')
	                        ->getNumberFormat()
	                        ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

	            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
				$objWorkSheet->getStyle('B4')->getFont()->setBold(true);
				$objWorkSheet->SetCellValue('B4', 'CardNo');
				$objWorkSheet->getStyle('B4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				$objWorkSheet->getStyle('B4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			// }

			// JAM MASUK / KELUAR { 
				$objPHPExcel->getActiveSheet()
	                        ->getStyle('C')
	                        ->getNumberFormat()
	                        ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

	            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
				$objWorkSheet->getStyle('C4')->getFont()->setBold(true);

				if($funct->Denval(@$_GET['type']) == 'time in') {
					$objWorkSheet->SetCellValue('C4', 'Jam Masuk');
				}
				else {
					$objWorkSheet->SetCellValue('C4', 'Jam Keluar');
				}

				$objWorkSheet->getStyle('C4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				$objWorkSheet->getStyle('C4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			// }

			// TOTAL { 
				$objPHPExcel->getActiveSheet()
	                        ->getStyle('D')
	                        ->getNumberFormat()
	                        ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

	            $objPHPExcel->getActiveSheet()
	                        ->getStyle('E')
	                        ->getNumberFormat()
	                        ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

	            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(4);
	            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
				$objWorkSheet->getStyle('D4')->getFont()->setBold(true);
				$objPHPExcel->setActiveSheetIndex(0)->mergeCells('D4:E4');
				$objWorkSheet->SetCellValue('D4', 'Total');
				$objWorkSheet->getStyle('D4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				$objWorkSheet->getStyle('D4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			// }
		// }

		// CONTENT { 
			$no = 1; $row = 5;
			$dt = date('Y-m-d', strtotime($y.'-'.$b.'-'.$funct->Denval(@$_GET['d'])));

			if($funct->Denval(@$_GET['type']) == 'time in') {
				$daty = "TimeIn"; 

				$sql = "SELECT TransID, CardNo, TimeIn from ".$funct->getValookie('koneksi').".parkinglist where VehicleType='".$funct->Denval(@$_GET['vhl'])."' and isMember = '0' and TimeIn between '$dt 00:00:00' and '$dt 23:59:59'";
				/*
				$sql = "SELECT 	a.CardNo as CardNo, 
								a.TimeIn as TimeIn, 
								b.PaidAmount as TotalPaid
                        from ".$funct->getValookie('koneksi').".parkinglist as a 
                        inner join ".$funct->getValookie('koneksi').".parkinglistpayment as b 
                        	on a.TransID = b.TransID 
                        where 	a.isMember='0' 
                        	and a.VehicleType = '".$funct->Denval(@$_GET['vhl'])."' 
                        	and a.TimeIn between '$dt 00:00:00' and '$dt 23:59:59'";
				*/
			}
			else { 
				$daty = "TimeOut"; 

				$sql = "SELECT TransID, CardNo, TimeOut from ".$funct->getValookie('koneksi').".parkinglist where VehicleType='".$funct->Denval(@$_GET['vhl'])."' and isMember = '0' and TimeOut between '$dt 00:00:00' and '$dt 23:59:59'";
				/*
				$sql = "SELECT 	a.CardNo as CardNo, 
								a.TimeOut as TimeOut, 
								b.PaidAmount as TotalPaid
                        from ".$funct->getValookie('koneksi').".parkinglist as a 
                        inner join ".$funct->getValookie('koneksi').".parkinglistpayment as b 
                        	on a.TransID = b.TransID 
                        where 	a.isMember='0' 
                            and a.VehicleType = '".$funct->Denval(@$_GET['vhl'])."' 
                           	and a.TimeOut between '$dt 00:00:00' and '$dt 23:59:59'";
				*/
			}


			// $sql = "SELECT distinct CardNo, TimeIn, TimeOut, TotalPaid from parkinglist where isMember='0' and VehicleType='".$funct->Denval(@$_GET['vhl'])."' and Month(TimeIn)='".$funct->Denval(@$_GET['b'])."' and Year(TimeIn)='".$funct->Denval(@$_GET['y'])."' order by TimeIn DESC";
			//echo $sql;
			$qry = mysqli_query($funct->getConnection(), $sql) or die(mysqli_error($funct->getConnection()));
			while($data = mysqli_fetch_array($qry)) {

				$sql2 = "SELECT Sum(PaidAmount) as TotalPaid from ".$funct->getValookie('koneksi').".parkinglistpayment where TransID='".$data['TransID']."' and PaidDate between '$dt 00:00:00' and '$dt 23:59:59'";
				$qry2 = mysqli_query($funct->getConnection(), $sql2) or die(mysqli_error($funct->getConnection()));
				$data2 = mysqli_fetch_array($qry2);

				// NOMOR
				$objWorkSheet->SetCellValue('A'.$row, $no.' ');
            	$objWorkSheet->getStyle('A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

            	// TANGGAL
            	$objWorkSheet->SetCellValue('B'.$row, $data['CardNo'].' ');
	           	$objWorkSheet->getStyle('B'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
				
	           	// JAM MASUK / KELUAR
	           	$objWorkSheet->SetCellValue('C'.$row, date('H:i:s', strtotime($data[$daty])) );
	           	$objWorkSheet->getStyle('C'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

	           	// TOTAL
	           	$objWorkSheet->SetCellValue('D'.$row, 'Rp.');

	           	$objWorkSheet->SetCellValue('E'.$row, $funct->FormatRupiah($data2['TotalPaid']) );
	           	$objWorkSheet->getStyle('E'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

				$no++; $row++;
				$total = $total + $data2['TotalPaid']; 
			}
		// }

		$objWorkSheet->getStyle('A'.$row)->getFont()->setBold(true);
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$row.':C'.$row);
		$objWorkSheet->SetCellValue('A'.$row, 'Total');
		$objWorkSheet->getStyle('A'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objWorkSheet->getStyle('A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

		$objWorkSheet->SetCellValue('D'.$row, 'Rp.');
	    $objWorkSheet->SetCellValue('E'.$row, ' '.$funct->FormatRupiah($total));
	    $objWorkSheet->getStyle('E'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

		//exit;
		$objWorkSheet->setTitle('Laporan Casual');

		$fileName = 'Laporan Casual '.date('d F Y', strtotime($y.'-'.$b.'-'.$funct->Denval(@$_GET['d']))).'.xls';
	}
	else {
		// HEADER { 
			$objWorkSheet->getStyle('A1')->getFont()->setSize(20);
			$objWorkSheet->getStyle('A1')->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:E1');
			$objWorkSheet->SetCellValue('A1', 'Laporan Casual');
			$objWorkSheet->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		
			$objWorkSheet->getStyle('A2')->getFont()->setSize(16);
			$objWorkSheet->getStyle('A2')->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:E2');
			$objWorkSheet->SetCellValue('A2', date('F Y', strtotime($y.'-'.$b.'-1')));
			$objWorkSheet->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			// TANGGAL { 
				$objPHPExcel->getActiveSheet()
	                        ->getStyle('A')
	                        ->getNumberFormat()
	                        ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

	            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(7);
				$objWorkSheet->getStyle('A4')->getFont()->setBold(true);
				$objWorkSheet->SetCellValue('A4', 'Tanggal');
				$objWorkSheet->getStyle('A4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				$objWorkSheet->getStyle('A4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			// }

			// TOTAL IN { 
				$objPHPExcel->getActiveSheet()
	                        ->getStyle('B')
	                        ->getNumberFormat()
	                        ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

	            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
				$objWorkSheet->getStyle('B4')->getFont()->setBold(true);
				$objWorkSheet->SetCellValue('B4', 'Total In');
				$objWorkSheet->getStyle('B4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				$objWorkSheet->getStyle('B4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			// }

			// TOTAL OUT { 
				$objPHPExcel->getActiveSheet()
	                        ->getStyle('C')
	                        ->getNumberFormat()
	                        ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

	            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
				$objWorkSheet->getStyle('C4')->getFont()->setBold(true);
				$objWorkSheet->SetCellValue('C4', 'Total Out');
				$objWorkSheet->getStyle('C4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				$objWorkSheet->getStyle('C4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			// }

			// TOTAL { 
				$objPHPExcel->getActiveSheet()
	                        ->getStyle('D')
	                        ->getNumberFormat()
	                        ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

	            $objPHPExcel->getActiveSheet()
	                        ->getStyle('E')
	                        ->getNumberFormat()
	                        ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

	            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(4);
	            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);

				$objWorkSheet->getStyle('D4')->getFont()->setBold(true);
				$objPHPExcel->setActiveSheetIndex(0)->mergeCells('D4:E4');
				$objWorkSheet->SetCellValue('D4', 'Total');
				$objWorkSheet->getStyle('D4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
				$objWorkSheet->getStyle('D4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			// }
		// }

		// CONTENT { 

			$row = 5;
			$total = 0;
			$t = date('t', strtotime($y.'-'.$b.'-1'));
			
			$arr = 0;
			$artio = json_decode($funct->Denval(@$_COOKIE['totio']));
			$atio = array();

			for($d = 1; $d <= $t; $d++) {
				$dt = date('Y-m-d', strtotime($y.'-'.$b.'-'.$d));
				$totio = 0; $toti = 0; $toto = 0;

				
				$s11 = "SELECT TransID, CardNo from ".$funct->getValookie('koneksi').".parkinglist where VehicleType='".$funct->Denval(@$_GET['vhl'])."' and isMember = '0' and TimeIn between '$dt 00:00:00' and '$dt 23:59:59'";
                $q11 = mysqli_query($funct->getConnection(), $s11) or die(mysqli_error($funct->getConnection()));
                while($d11 = mysqli_fetch_array($q11)) {
                    $s12 = "SELECT Sum(PaidAmount) as TotalPaid from ".$funct->getValookie('koneksi').".parkinglistpayment where TransID='".$d11['TransID']."' and PaidDate between '$dt 00:00:00' and '$dt 23:59:59'";
	                $q12 = mysqli_query($funct->getConnection(), $s12) or die(mysqli_error($funct->getConnection()));
	                $d12 = mysqli_fetch_array($q12);

	                $toti = $toti + $d12['TotalPaid'];
                }

                $s21 = "SELECT TransID, CardNo from ".$funct->getValookie('koneksi').".parkinglist where VehicleType='".$funct->Denval(@$_GET['vhl'])."' and isMember = '0' and TimeOut between '$dt 00:00:00' and '$dt 23:59:59'";
                $q21 = mysqli_query($funct->getConnection(), $s21) or die(mysqli_error($funct->getConnection()));
                while($d21 = mysqli_fetch_array($q21)) {
                    $s22 = "SELECT Sum(PaidAmount) as TotalPaid from ".$funct->getValookie('koneksi').".parkinglistpayment where TransID='".$d21['TransID']."' and PaidDate between '$dt 00:00:00' and '$dt 23:59:59'";
	                $q22 = mysqli_query($funct->getConnection(), $s22) or die(mysqli_error($funct->getConnection()));
	                $d22 = mysqli_fetch_array($q22);

	                $toto = $toto + $d22['TotalPaid'];
                }

				/*
                $sql = "SELECT * from (
							select count(TimeIn)  as TimeIn  from ".$funct->getValookie('koneksi').".parkinglist where VehicleType='".$funct->Denval(@$_GET['vhl'])."' and isMember = '0' and TimeIn  between '$dt 00:00:00' and '$dt 23:59:59') as TimeIn, 
							(select count(TimeOut) as TimeOut from ".$funct->getValookie('koneksi').".parkinglist where VehicleType='".$funct->Denval(@$_GET['vhl'])."' and isMember = '0' and TimeOut between '$dt 00:00:00' and '$dt 23:59:59') as TimeOut
							(select sum(b.PaidAmount) as Total from ".$funct->getValookie('koneksi').".parkinglist as a 
			                inner join ".$funct->getValookie('koneksi').".parkinglistpayment as b 
			                    on a.TransID = b.TransID 
							where a.isMember = '0' and a.VehicleType='".$funct->Denval(@$_GET['vhl'])."' and PaidDate between '$dt 00:00:00' and '$dt 23:59:59') as Total
						)";
				

				//echo $d.'). '.$sql.'<br><br>';
				//$qry = mysqli_query($funct->getConnection(), $sql) or die(mysqli_error($funct->getConnection()));
				//while($data = mysqli_fetch_array($qry)) { 
				//$data = mysqli_fetch_array($qry);

				
				$s11 = "SELECT Sum(b.PaidAmount) as Total
                        from ".$funct->getValookie('koneksi').".parkinglist as a 
                        inner join ".$funct->getValookie('koneksi').".parkinglistpayment as b 
                            on a.TransID = b.TransID 
                        where a.isMember='0' and a.VehicleType = '".$funct->Denval(@$_GET['vhl'])."' and a.TimeIn between '$dt 00:00:00' and '$dt 23:59:59'";
                $q11 = mysqli_query($funct->getConnection(), $s11) or die(mysqli_error($funct->getConnection()));
                $d11 = mysqli_fetch_array($q11);
                $toti = $toti + $d11['Total'];

                $s21 = "SELECT Sum(b.PaidAmount) as Total
                        from ".$funct->getValookie('koneksi').".parkinglist as a 
                        inner join ".$funct->getValookie('koneksi').".parkinglistpayment as b 
                            on a.TransID = b.TransID 
                        where a.isMember='0' and a.VehicleType = '".$funct->Denval(@$_GET['vhl'])."' and a.TimeOut between '$dt 00:00:00' and '$dt 23:59:59'";
                $q21 = mysqli_query($funct->getConnection(), $s11) or die(mysqli_error($funct->getConnection()));
                $d21 = mysqli_fetch_array($q11);
                $toto = $toto+ $d21['Total'];

                
                */
                $totio = $totio + $toti + $toto;

				$s1 = "select count(TimeIn)  as TimeIn  from ".$funct->getValookie('koneksi').".parkinglist where VehicleType='".$funct->Denval(@$_GET['vhl'])."' and isMember = '0' and TimeIn  between '$dt 00:00:00' and '$dt 23:59:59'";
				$q1 = mysqli_query($funct->getConnection(), $s1) or die(mysqli_error($funct->getConnection()));
				$d1 = mysqli_fetch_array($q1);

				$s2 = "select count(TimeOut) as TimeOut from ".$funct->getValookie('koneksi').".parkinglist where VehicleType='".$funct->Denval(@$_GET['vhl'])."' and isMember = '0' and TimeOut between '$dt 00:00:00' and '$dt 23:59:59'";
				$q2 = mysqli_query($funct->getConnection(), $s2) or die(mysqli_error($funct->getConnection()));
				$d2 = mysqli_fetch_array($q2);

					// TANGGAL
					$objWorkSheet->SetCellValue('A'.$row, ' '.$d);
            		$objWorkSheet->getStyle('A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            		$objWorkSheet->getStyle('A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

            		// TOTAL IN
            		$objWorkSheet->SetCellValue('B'.$row, ' '.$funct->FormatNumber($d1['TimeIn']));
	           		$objWorkSheet->getStyle('B'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				
	           		// TOTAL OUT
	           		$objWorkSheet->SetCellValue('C'.$row, ' '.$funct->FormatNumber($d2['TimeOut']));
	           		$objWorkSheet->getStyle('C'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

	           		// TOTAL
	           		$objWorkSheet->SetCellValue('D'.$row, 'Rp.');
	           		$objWorkSheet->getStyle('D'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

	           		//$objWorkSheet->SetCellValue('E'.$row, ' '.$funct->FormatRupiah($artio[$arr]);
	           		$objWorkSheet->SetCellValue('E'.$row, ' '.$funct->FormatRupiah($totio));
	           		$objWorkSheet->getStyle('E'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

					//$total = $total + $data['Total'];
					$total = $total + $totio;
				
				$row++; $arr++;
			}
			
		// }

		$objWorkSheet->getStyle('A'.$row)->getFont()->setBold(true);
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$row.':C'.$row);
		$objWorkSheet->SetCellValue('A'.$row, 'Total');
		$objWorkSheet->getStyle('A'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objWorkSheet->getStyle('A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

		$objWorkSheet->SetCellValue('D'.$row, 'Rp.');
	    $objWorkSheet->getStyle('D'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

	    $objWorkSheet->SetCellValue('E'.$row, ' '.$funct->FormatRupiah($total));
	    $objWorkSheet->getStyle('E'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

	    //$objWorkSheet->SetCellValue('A'.($row+1), $funct->getArray());

		//exit;
		$objWorkSheet->setTitle('Laporan Casual');

		$fileName = 'Laporan Casual '.date('F Y', strtotime($funct->Denval(@$_GET['y']).'-'.$funct->Denval(@$_GET['b']).'-1')).'.xls';
	
		//var_dump($funct->getArray());
	}
}
else {
	// HEADER { 
		$objWorkSheet->getStyle('A1')->getFont()->setSize(20);
		$objWorkSheet->getStyle('A1')->getFont()->setBold(true);
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:E1');
		$objWorkSheet->SetCellValue('A1', 'Laporan Casual');
		$objWorkSheet->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	
		$objWorkSheet->getStyle('A2')->getFont()->setSize(16);
		$objWorkSheet->getStyle('A2')->getFont()->setBold(true);
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:E2');
		$objWorkSheet->SetCellValue('A2', date('F Y', strtotime(@$_GET['y'].'-'.@$_GET['b'].'-1')));
		$objWorkSheet->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	
		// KENDARAAN { 
			$objPHPExcel->getActiveSheet()
                        ->getStyle('A')
                        ->getNumberFormat()
                        ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
			$objWorkSheet->getStyle('A4')->getFont()->setBold(true);
			$objWorkSheet->SetCellValue('A4', 'Kendaraan');
			$objWorkSheet->getStyle('A4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objWorkSheet->getStyle('A4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		// }

		// TOTAL IN { 
			$objPHPExcel->getActiveSheet()
                        ->getStyle('B')
                        ->getNumberFormat()
                        ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
			$objWorkSheet->getStyle('B4')->getFont()->setBold(true);
			$objWorkSheet->SetCellValue('B4', 'Total In');
			$objWorkSheet->getStyle('B4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objWorkSheet->getStyle('B4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		// }

		// TOTAL OUT { 
			$objPHPExcel->getActiveSheet()
                        ->getStyle('C')
                        ->getNumberFormat()
                        ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
			$objWorkSheet->getStyle('C4')->getFont()->setBold(true);
			$objWorkSheet->SetCellValue('C4', 'Total Out');
			$objWorkSheet->getStyle('C4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objWorkSheet->getStyle('C4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		// }

		// TOTAL { 
			$objPHPExcel->getActiveSheet()
                        ->getStyle('D')
                        ->getNumberFormat()
                        ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

            $objPHPExcel->getActiveSheet()
                        ->getStyle('E')
                        ->getNumberFormat()
                        ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(4);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);

			$objWorkSheet->getStyle('D4')->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('D4:E4');
			$objWorkSheet->SetCellValue('D4', 'Total');
			$objWorkSheet->getStyle('D4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objWorkSheet->getStyle('D4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		// }
	// }

	// CONTENT { 
		/*
		$smotor = 	"SELECT * from (
						(select count(TimeIn) as TimeIn    from ".$funct->getValookie('koneksi').".parkinglist where VehicleType = 'C' and isMember = '0' and Month(TimeIn)='$b' and Year(TimeIn)='$y') as TimeIn, 
						(select count(TimeOut) as TimeOut  from ".$funct->getValookie('koneksi').".parkinglist where VehicleType = 'C' and isMember = '0' and Month(TimeIn)='$b' and Year(TimeIn)='$y') as TimeOut,
						(select sum(b.PaidAmount) as Total from ".$funct->getValookie('koneksi').".parkinglist as a 
                         inner join ".$funct->getValookie('koneksi').".parkinglistpayment as b 
                            on a.TransID = b.TransID 
						 where a.VehicleType = 'C' and Month(b.PaidDate)='$b' and Year(b.PaidDate)='$y') as Total
					)";
		*/

		$smotor = 	"SELECT * from (
					 (select count(TimeIn) as TimeIn    from ".$funct->getValookie('koneksi').".parkinglist where VehicleType = 'C' and isMember = '0' and Month(TimeIn)='$b' and Year(TimeIn)='$y') as TimeIn, 
					 (select count(TimeOut) as TimeOut  from ".$funct->getValookie('koneksi').".parkinglist where VehicleType = 'C' and isMember = '0' and Month(TimeIn)='$b' and Year(TimeIn)='$y') as TimeOut,
					 (select sum(b.PaidAmount) as Total from ".$funct->getValookie('koneksi').".parkinglist as a 
                	 inner join ".$funct->getValookie('koneksi').".parkinglistpayment as b 
                    	on a.TransID = b.TransID 
					 where isMember = '0' and a.VehicleType = 'C' and Month(b.PaidDate)='$b' and Year(b.PaidDate)='$y') as Total
					)";
					
		$qmotor = mysqli_query($funct->getConnection(), $smotor) or die(mysqli_error($funct->getConnection()));
		$motor = mysqli_fetch_array($qmotor);

		/*
		$smobil = 	"SELECT * from (
						(select count(TimeIn)  as TimeIn   from ".$funct->getValookie('koneksi').".parkinglist where VehicleType = 'B' and isMember = '0' and Month(TimeIn)='$b' and Year(TimeIn)='$y') as TimeIn, 
						(select count(TimeOut) as TimeOut  from ".$funct->getValookie('koneksi').".parkinglist where VehicleType = 'B' and isMember = '0' and Month(TimeIn)='$b' and Year(TimeIn)='$y') as TimeOut,
						(select sum(b.PaidAmount) as Total from ".$funct->getValookie('koneksi').".parkinglist as a 
                         inner join ".$funct->getValookie('koneksi').".parkinglistpayment as b 
                            on a.TransID = b.TransID 
						 where a.VehicleType = 'B' and Month(b.PaidDate)='$b' and Year(b.PaidDate)='$y') as Total
					)";
		*/

		$smobil = 	"SELECT * from (
					 (select count(TimeIn)  as TimeIn   from ".$funct->getValookie('koneksi').".parkinglist where VehicleType = 'B' and isMember = '0' and Month(TimeIn)='$b' and Year(TimeIn)='$y') as TimeIn, 
					 (select count(TimeOut) as TimeOut  from ".$funct->getValookie('koneksi').".parkinglist where VehicleType = 'B' and isMember = '0' and Month(TimeIn)='$b' and Year(TimeIn)='$y') as TimeOut,
					 (select sum(b.PaidAmount) as Total from ".$funct->getValookie('koneksi').".parkinglist as a 
                      inner join ".$funct->getValookie('koneksi').".parkinglistpayment as b 
                        on a.TransID = b.TransID 
					  where isMember = '0' and a.VehicleType = 'B' and Month(b.PaidDate)='$b' and Year(b.PaidDate)='$y') as Total
					)";

		$qmobil = mysqli_query($funct->getConnection(), $smobil) or die(mysqli_error($funct->getConnection()));
		$mobil = mysqli_fetch_array($qmobil);

		/*
		$struk = 	"SELECT * from (
						(select count(TimeIn) as TimeIn    from ".$funct->getValookie('koneksi').".parkinglist where VehicleType = 'T' and isMember = '0' and Month(TimeIn)='$b' and Year(TimeIn)='$y') as TimeIn, 
						(select count(TimeOut) as TimeOut  from ".$funct->getValookie('koneksi').".parkinglist where VehicleType = 'T' and isMember = '0' and Month(TimeIn)='$b' and Year(TimeIn)='$y') as TimeOut,
						(select sum(b.PaidAmount) as Total from ".$funct->getValookie('koneksi').".parkinglist as a 
                         inner join ".$funct->getValookie('koneksi').".parkinglistpayment as b 
                            on a.TransID = b.TransID 
						 where a.VehicleType = 'T' and Month(b.PaidDate)='$b' and Year(b.PaidDate)='$y') as Total
					)";
		*/

		$struk = 	"SELECT * from (
					 (select count(TimeIn) as TimeIn    from ".$funct->getValookie('koneksi').".parkinglist where VehicleType = 'T' and isMember = '0' and Month(TimeIn)='$b' and Year(TimeIn)='$y') as TimeIn, 
					 (select count(TimeOut) as TimeOut  from ".$funct->getValookie('koneksi').".parkinglist where VehicleType = 'T' and isMember = '0' and Month(TimeIn)='$b' and Year(TimeIn)='$y') as TimeOut,
					 (select sum(b.PaidAmount) as Total from ".$funct->getValookie('koneksi').".parkinglist as a 
                      inner join ".$funct->getValookie('koneksi').".parkinglistpayment as b 
                    	on a.TransID = b.TransID 
					  where isMember = '0' and a.VehicleType = 'T' and Month(b.PaidDate)='$b' and Year(b.PaidDate)='$y') as Total
					)";

		$qtruk = mysqli_query($funct->getConnection(), $struk) or die(mysqli_error($funct->getConnection()));
		$truk = mysqli_fetch_array($qtruk);

		/*
		$svalet = 	"SELECT * from (
						(select count(TimeIn) as TimeIn    from ".$funct->getValookie('koneksi').".parkinglist where VehicleType = 'P' and isMember = '0' and Month(TimeIn)='$b' and Year(TimeIn)='$y') as TimeIn, 
						(select count(TimeOut) as TimeOut  from ".$funct->getValookie('koneksi').".parkinglist where VehicleType = 'P' and isMember = '0' and Month(TimeIn)='$b' and Year(TimeIn)='$y') as TimeOut,
						(select sum(b.PaidAmount) as Total from ".$funct->getValookie('koneksi').".parkinglist as a 
                         inner join ".$funct->getValookie('koneksi').".parkinglistpayment as b 
                            on a.TransID = b.TransID 
						 where a.VehicleType = 'P' and Month(b.PaidDate)='$b' and Year(b.PaidDate)='$y') as Total
					)";
		*/

		$svalet = 	"SELECT * from (
					 (select count(TimeIn) as TimeIn    from ".$funct->getValookie('koneksi').".parkinglist where VehicleType = 'P' and isMember = '0' and Month(TimeIn)='$b' and Year(TimeIn)='$y') as TimeIn, 
					 (select count(TimeOut) as TimeOut  from ".$funct->getValookie('koneksi').".parkinglist where VehicleType = 'P' and isMember = '0' and Month(TimeIn)='$b' and Year(TimeIn)='$y') as TimeOut,
					 (select sum(b.PaidAmount) as Total from ".$funct->getValookie('koneksi').".parkinglist as a 
                      inner join ".$funct->getValookie('koneksi').".parkinglistpayment as b 
                    	on a.TransID = b.TransID 
					  where isMember = '0' and a.VehicleType = 'P' and Month(b.PaidDate)='$b' and Year(b.PaidDate)='$y') as Total
					)";

		$qvalet = mysqli_query($funct->getConnection(), $svalet) or die(mysqli_error($funct->getConnection()));
		$valet = mysqli_fetch_array($qvalet);

		/*
		$svipmotor = 	"SELECT * from (
							(select count(TimeIn) as TimeIn    from ".$funct->getValookie('koneksi').".parkinglist where VehicleType = 'Z' and isMember = '0' and Month(TimeIn)='$b' and Year(TimeIn)='$y') as TimeIn, 
							(select count(TimeOut) as TimeOut  from ".$funct->getValookie('koneksi').".parkinglist where VehicleType = 'Z' and isMember = '0' and Month(TimeIn)='$b' and Year(TimeIn)='$y') as TimeOut,
							(select sum(b.PaidAmount) as Total from ".$funct->getValookie('koneksi').".parkinglist as a 
                         	 inner join ".$funct->getValookie('koneksi').".parkinglistpayment as b 
                             	on a.TransID = b.TransID 
						 	 where a.VehicleType = 'Z' and Month(b.PaidDate)='$b' and Year(b.PaidDate)='$y') as Total
						)";
		*/

		$svipmotor = 	"SELECT * from (
						 (select count(TimeIn) as TimeIn    from ".$funct->getValookie('koneksi').".parkinglist where VehicleType = 'Z' and isMember = '0' and Month(TimeIn)='$b' and Year(TimeIn)='$y') as TimeIn, 
						 (select count(TimeOut) as TimeOut  from ".$funct->getValookie('koneksi').".parkinglist where VehicleType = 'Z' and isMember = '0' and Month(TimeIn)='$b' and Year(TimeIn)='$y') as TimeOut,
						 (select sum(b.PaidAmount) as Total from ".$funct->getValookie('koneksi').".parkinglist as a 
                          inner join ".$funct->getValookie('koneksi').".parkinglistpayment as b 
                        	on a.TransID = b.TransID 
						  where isMember = '0' and a.VehicleType = 'Z' and Month(b.PaidDate)='$b' and Year(b.PaidDate)='$y') as Total
						)";

		$qvipmotor = mysqli_query($funct->getConnection(), $svipmotor) or die(mysqli_error($funct->getConnection()));
		$vipmotor = mysqli_fetch_array($qvipmotor);

		/*
		$svipmobil = 	"SELECT * from (
							(select count(TimeIn) as TimeIn    from ".$funct->getValookie('koneksi').".parkinglist where VehicleType = 'V' and isMember = '0' and Month(TimeIn)='$b' and Year(TimeIn)='$y') as TimeIn, 
							(select count(TimeOut) as TimeOut  from ".$funct->getValookie('koneksi').".parkinglist where VehicleType = 'V' and isMember = '0' and Month(TimeIn)='$b' and Year(TimeIn)='$y') as TimeOut,
							(select sum(b.PaidAmount) as Total from ".$funct->getValookie('koneksi').".parkinglist as a 
                             inner join ".$funct->getValookie('koneksi').".parkinglistpayment as b 
                            	on a.TransID = b.TransID 
							where a.VehicleType = 'V' and Month(b.PaidDate)='$b' and Year(b.PaidDate)='$y') as Total
						)";
		*/

		$svipmobil = 	"SELECT * from (
						 (select count(TimeIn) as TimeIn    from ".$funct->getValookie('koneksi').".parkinglist where VehicleType = 'V' and isMember = '0' and Month(TimeIn)='$b' and Year(TimeIn)='$y') as TimeIn, 
						 (select count(TimeOut) as TimeOut  from ".$funct->getValookie('koneksi').".parkinglist where VehicleType = 'V' and isMember = '0' and Month(TimeIn)='$b' and Year(TimeIn)='$y') as TimeOut,
						 (select sum(b.PaidAmount) as Total from ".$funct->getValookie('koneksi').".parkinglist as a 
                          inner join ".$funct->getValookie('koneksi').".parkinglistpayment as b 
                        	on a.TransID = b.TransID 
						  where isMember = '0' and a.VehicleType = 'V' and Month(b.PaidDate)='$b' and Year(b.PaidDate)='$y') as Total
						)";

		$qvipmobil = mysqli_query($funct->getConnection(), $svipmobil) or die(mysqli_error($funct->getConnection()));
		$vipmobil = mysqli_fetch_array($qvipmobil);

		// ISI KOLOM { 

			// KENDARAAN { 
				$objWorkSheet->SetCellValue('A5', 'Motor');
            	$objWorkSheet->getStyle('A5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            	$objWorkSheet->SetCellValue('A6', 'Mobil');
            	$objWorkSheet->getStyle('A6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            	$objWorkSheet->SetCellValue('A7', 'VIP Motor');
            	$objWorkSheet->getStyle('A7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            	$objWorkSheet->SetCellValue('A8', 'VIP Mobil');
            	$objWorkSheet->getStyle('A8')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            	$objWorkSheet->SetCellValue('A9', 'Truk');
            	$objWorkSheet->getStyle('A9')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            	$objWorkSheet->SetCellValue('A10', 'Valet');
            	$objWorkSheet->getStyle('A10')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			// }

			// TOTAL IN { 
            	$objWorkSheet->SetCellValue('B5', ' '.$funct->FormatNumber($motor['TimeIn']));
	           	$objWorkSheet->getStyle('B5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

	           	$objWorkSheet->SetCellValue('B6', ' '.$funct->FormatNumber($mobil['TimeIn']));
	           	$objWorkSheet->getStyle('B6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

	           	$objWorkSheet->SetCellValue('B7', ' '.$funct->FormatNumber($vipmotor['TimeIn']));
	           	$objWorkSheet->getStyle('B7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

	           	$objWorkSheet->SetCellValue('B8', ' '.$funct->FormatNumber($vipmobil['TimeIn']));
	           	$objWorkSheet->getStyle('B8')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

	           	$objWorkSheet->SetCellValue('B9', ' '.$funct->FormatNumber($truk['TimeIn']));
	           	$objWorkSheet->getStyle('B9')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

	           	$objWorkSheet->SetCellValue('B10', ' '.$funct->FormatNumber($valet['TimeIn']));
	           	$objWorkSheet->getStyle('B10')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			// }

			// TOTAL OUT { 
	           	$objWorkSheet->SetCellValue('C5', ' '.$funct->FormatNumber($motor['TimeOut']));
	           	$objWorkSheet->getStyle('C5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

	           	$objWorkSheet->SetCellValue('C6', ' '.$funct->FormatNumber($mobil['TimeOut']));
	           	$objWorkSheet->getStyle('C6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

	           	$objWorkSheet->SetCellValue('C7', ' '.$funct->FormatNumber($vipmotor['TimeOut']));
	           	$objWorkSheet->getStyle('C7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

	           	$objWorkSheet->SetCellValue('C8', ' '.$funct->FormatNumber($vipmobil['TimeOut']));
	           	$objWorkSheet->getStyle('C8')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

	           	$objWorkSheet->SetCellValue('C9', ' '.$funct->FormatNumber($truk['TimeOut']));
	           	$objWorkSheet->getStyle('C9')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

	           	$objWorkSheet->SetCellValue('C10', ' '.$funct->FormatNumber($valet['TimeOut']));
	           	$objWorkSheet->getStyle('C10')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			// }

			// TOTAL { 
				$objWorkSheet->SetCellValue('D5', 'Rp.');
	           	$objWorkSheet->SetCellValue('E5', ' '.$funct->FormatRupiah($motor['Total']));
	           	$objWorkSheet->getStyle('E5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

	           	$objWorkSheet->SetCellValue('D6', 'Rp.');
	           	$objWorkSheet->SetCellValue('E6', ' '.$funct->FormatRupiah($mobil['Total']));
	           	$objWorkSheet->getStyle('E6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

	           	$objWorkSheet->SetCellValue('D7', 'Rp.');
	           	$objWorkSheet->SetCellValue('E7', ' '.$funct->FormatRupiah($vipmotor['Total']));
	           	$objWorkSheet->getStyle('E7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

	           	$objWorkSheet->SetCellValue('D8', 'Rp.');
	           	$objWorkSheet->SetCellValue('E8', ' '.$funct->FormatRupiah($vipmobil['Total']));
	           	$objWorkSheet->getStyle('E8')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

	           	$objWorkSheet->SetCellValue('D9', 'Rp.');
	           	$objWorkSheet->SetCellValue('E9', ' '.$funct->FormatRupiah($truk['Total']));
	           	$objWorkSheet->getStyle('E9')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

	           	$objWorkSheet->SetCellValue('D10', 'Rp.');
	           	$objWorkSheet->SetCellValue('E10', ' '.$funct->FormatRupiah($valet['Total']));
	           	$objWorkSheet->getStyle('E10')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			// }

		// }
	// }

	//exit;
	$objWorkSheet->setTitle('Laporan Casual');

	$fileName = 'Laporan Casual '.date('F Y', strtotime($y.'-'.$b.'-1')).'.xls';
}

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');
?>