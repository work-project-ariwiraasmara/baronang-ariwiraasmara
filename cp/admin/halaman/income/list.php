<?php
if(isset($_GET['b'])) { $b = $funct->Denval(@$_GET['b']); } else { $b = date('m'); }
if(isset($_GET['y'])) { $y = $funct->Denval(@$_GET['y']); } else { $y = date('Y'); }
?>

<div id="toolbar" class="primary-color">
    <div class="open-left" id="open-left" data-activates="slide-out-left">
	    <i class="ion-android-menu"></i>
	</div>
    
    <span class="title bold txt-white" id="title"></span>

	<div class="open-right">
	    <a href="<?php echo 'index_download_excel.php?pg=income&download=1&b='.$funct->Enval($b).'&y='.$funct->Enval($y); ?>" id="download">
	    	<i class="ion-android-download txt-white"></i>
	    </a>
	</div>

	<div class="open-right">
	    <a class="modal-trigger" href="#cari">
	    	<i class="ion-android-search txt-white"></i>
	    </a>
	</div>
</div>

<div class="animated fadeinup delay-1">
	<div class="page-content">

		<h1 class="uppercase txt-black bold"><?php echo date('F Y', strtotime($y.'-'.$b.'-1')) ?></h1>

		<div class="m-t-50">
			<table class="table">
				<thead>
					<tr>
						<th rowspan="3"  style="text-align: center; border: 1px solid #000;">No.</th>
						<th rowspan="3"  style="text-align: center; border: 1px solid #000;">Jenis Kendaraan</th>
						<th colspan="2"  style="text-align: center; border: 1px solid #000;">Total Pendapatan</th>
						<th colspan="10" style="text-align: center; border: 1px solid #000;">Jenis Pembayaran</th>
					</tr>

					<tr>
						<th rowspan="2" style="text-align: center; border: 1px solid #000;">QTY</th>
						<th rowspan="2" style="text-align: center; border: 1px solid #000;">Nilai</th>
						<th colspan="2" style="text-align: center; border: 1px solid #000;">Tunai</th>
						<th colspan="2" style="text-align: center; border: 1px solid #000;">Ovo</th>
						<th colspan="2" style="text-align: center; border: 1px solid #000;">Member</th>
						<th colspan="2" style="text-align: center; border: 1px solid #000;">Denda</th>
						<th colspan="2" style="text-align: center; border: 1px solid #000;">Emoney</th>
					</tr>

					<tr>
						<th style="text-align: center; border: 1px solid #000;">QTY</th>
						<th style="text-align: center; border: 1px solid #000;">Nilai</th>
						<th style="text-align: center; border: 1px solid #000;">QTY</th>
						<th style="text-align: center; border: 1px solid #000;">Nilai</th>
						<th style="text-align: center; border: 1px solid #000;">QTY</th>
						<th style="text-align: center; border: 1px solid #000;">Nilai</th>
						<th style="text-align: center; border: 1px solid #000;">QTY</th>
						<th style="text-align: center; border: 1px solid #000;">Nilai</th>
						<th style="text-align: center; border: 1px solid #000;">QTY</th>
						<th style="text-align: center; border: 1px solid #000;">Nilai</th>
					</tr>
				</thead>

				<tbody>
					<?php
						$no = 1;
						
						$s1 = "SELECT VehicleID, Code, Name from ".$funct->getValookie('koneksi').".vehicletype order by VehicleID ASC";
						$q1 = mysqli_query($funct->getConnection(), $s1) or die(mysqli_error($funct->getConnection()));
						while($data = mysqli_fetch_array($q1)) {

							$totqty = 0;
							$totnil = 0;

							$s2 = "SELECT distinct TransID from ".$funct->getValookie('koneksi').".parkinglist where VehicleType='$data[Code]'";
							$q2 = mysqli_query($funct->getConnection(), $s2) or die(mysqli_error($funct->getConnection()));
							$d2 = mysqli_fetch_array($q2);
							//echo $s2.'<br>';

							// get Tunai
							$s3 = "SELECT count(b.PaidDate) as QTY, sum(b.PaidAmount) as Nilai
								   from parkinglist as a inner join ".$funct->getValookie('koneksi').".parkinglistpayment as b on a.TransID = b.TransID where a.VehicleType='$data[Code]' and b.Method='CASH' and Month(b.PaidDate)='$b' and Year(b.PaidDate)='$y'";
							$q3 = mysqli_query($funct->getConnection(), $s3) or die(mysqli_error($funct->getConnection()));
							$d3 = mysqli_fetch_array($q3);

							// get Ovo
							$s4 = "SELECT count(b.PaidDate) as QTY, sum(b.PaidAmount) as Nilai
								   from parkinglist as a inner join ".$funct->getValookie('koneksi').".parkinglistpayment as b on a.TransID = b.TransID where a.VehicleType='$data[Code]' and b.Method='OVO' and Month(b.PaidDate)='$b' and Year(b.PaidDate)='$y'";
							$q4 = mysqli_query($funct->getConnection(), $s4) or die(mysqli_error($funct->getConnection()));
							$d4 = mysqli_fetch_array($q4);


							// get Member
							$s5 = "SELECT ProductID from ".$funct->getValookie('koneksi').".locationparkingproduct where VehicleID='$data[VehicleID]'";
							$q5 = mysqli_query($funct->getConnection(), $s5) or die(mysqli_error($funct->getConnection()));
							$d5 = mysqli_fetch_array($q5);

							$s6 = "select Count(TransDate) as QTY, Sum(Amount) as Nilai from ".$funct->getValookie('koneksi').".translist where TransactionType='MBRS' and Kredit='$d5[ProductID]' and Month(TransDate)='$b' and Year(TransDate)='$y' order by Kredit ASC";
							$q6 = mysqli_query($funct->getConnection(), $s6) or die(mysqli_error($funct->getConnection()));
							$d6 = mysqli_fetch_array($q6);

							// get Denda



							// get Emoney

							?>
							<tr style="border: 1px solid #000;">
								<td style="border: 1px solid #000; text-align: right;"><?php echo $no; ?>. </td>
								<td><?php echo $data['Name']; ?></td>

								<?php //Total Pendapatan ?>
								<td id="<?php echo 'Total_QTY_'.$data['VehicleID']; ?>" style="text-align: right;"></td>
								<td id="<?php echo 'Total_Nilai_'.$data['VehicleID']; ?>" style="text-align: right;"></td>

								<?php //Tunai ?>
								<td class="txt-link" style="text-align: right;">
									<?php
									if($d3['QTY'] =='' || empty($d3['QTY']) || is_null($d3['QTY'])) { 
										echo $d3['QTY'];
									}
									else { ?>
										<a href="<?php  echo '?pg=income&sb=cash&'.$funct->setIDParam('detail', $data['Code']).'&b='.$funct->Enval($b).'&y='.$funct->Enval($y); ?>">
											<?php echo $d3['QTY']; ?>
										</a>
										<?php
									}
									?>
								</td>
								<td style="text-align: right;"><?php echo $funct->FormatRupiah($d3['Nilai']); ?></td>

								<?php //Ovo ?>
								<td class="txt-link" style="text-align: right;">
									<?php
									if($d4['QTY'] =='' || empty($d4['QTY']) || is_null($d4['QTY'])) { 
										echo $d4['QTY'];
									}
									else { ?>
										<a href="<?php echo '?pg=income&sb=ovo&'.$funct->setIDParam('detail', $data['Code']).'&b='.$funct->Enval($b).'&y='.$funct->Enval($y); ?>">
											<?php echo $d4['QTY']; ?>
										</a>
										<?php
									}
									?>
								</td>
								<td style="text-align: right;"><?php echo $funct->FormatRupiah($d4['Nilai']); ?></td>

								<?php //Member ?>
								<td class="txt-link" style="text-align: right;">
									<?php
									if($d6['QTY'] == '' || empty($d6['QTY']) || is_null($d6['QTY'])) {
										echo $d6['QTY'];
									}
									else { ?>
										<a href="<?php echo '?pg=income&sb=member&'.$funct->setIDParam('detail', $data['VehicleID']).'&b='.$funct->Enval($b).'&y='.$funct->Enval($y); ?>">
											<?php echo $d6['QTY']; ?>
										</a>
										<?php
									}
									?>
								</td>
								<td style="text-align: right;"><?php echo $funct->FormatRupiah($d6['Nilai']); ?></td>

								<?php //Denda ?>
								<td style="text-align: right;"></td>
								<td style="text-align: right;"></td>

								<?php //Emoney ?>
								<td style="text-align: right;"></td>
								<td style="text-align: right;"></td>
							</tr>
							<?php
							$no++;
							$totqty = $d3['QTY']   + $d4['QTY'] + $d6['QTY'];
							$totnil = $d3['Nilai'] + $d4['Nilai'] + $d6['Nilai'];
							?>
							<script type="text/javascript">
								$("<?php echo '#Total_QTY_'.$data['VehicleID']; ?>").html("<?php echo $totqty; ?>");
								$("<?php echo '#Total_Nilai_'.$data['VehicleID']; ?>").html("<?php echo $funct->FormatRupiah($totnil); ?>");
							</script>
							<?php
						
					}
					?>
				</tbody>
			</table>

		</div>
	</div>
</div>

<div class="modal borad-30" id="cari">
	<div class="modal-content choose-date p-20">
		<div class="row">
			<div class="col s6">
				<span class="bold">Bulan</span>
				<select class="browser-default" id="bulan">
					<?php
					for($x = 1; $x < 13; $x++) { ?>
						<option value="<?php echo $funct->Enval($x); ?>"><?php echo date('F', strtotime( date('Y').'-'.$x.'-1' )); ?></option>
						<?php
					}
					?>
				</select>
			</div>

			<div class="col s6">
				<span class="bold">Tahun</span>
				<select class="browser-default" id="tahun">
					<?php
					for($x = date('Y'); $x > 2015; $x--) { ?>
						<option value="<?php echo $funct->Enval($x); ?>"><?php echo $x; ?></option>
						<?php
					}
					?>
				</select>
			</div>

			<div class="col s12 m-t-30">
				<button id="btnok" class="btn btn-large width-100 primary-color waves-effect waves-light">Cari</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$('#btnok').click(function(){
		var bulan = $('#bulan :selected').val();
		var tahun = $('#tahun :selected').val();

		window.location.href = "?pg=income&b="+bulan+"&y="+tahun;
	});

	/*
	$('#download').click(function(){
		//window.location.href = "<?php //echo 'halaman/income/download_list.php?pg=income&download=1&b='.$b.'&y='.$y; ?>";
		window.location.href = "<?php //echo 'index_download_excel.php?pg=income&download=1&b='.$b.'&y='.$y; ?>";
	});
	*/
</script>