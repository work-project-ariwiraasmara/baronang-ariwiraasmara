<?php
if(isset($_GET['b'])) { $b = $funct->Denval(@$_GET['b']); } else { $b = date('m'); }
if(isset($_GET['y'])) { $y = $funct->Denval(@$_GET['y']); } else { $y = date('Y'); }

$s1 = "SELECT VehicleID, Code, Name from ".$funct->getValookie('koneksi').".vehicletype where Code='".$funct->getIDParam('detail')."' order by VehicleID ASC";
$q1 = mysqli_query($funct->getConnection(), $s1) or die(mysqli_error($funct->getConnection()));
$d1 = mysqli_fetch_array($q1);

$total = 0;

if(isset($_GET['d'])) {

	// HEADER { 
		$objPHPExcel->getActiveSheet()
                        ->getStyle('A')
                        ->getNumberFormat()
                        ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

        $objPHPExcel->getActiveSheet()
                        ->getStyle('B')
                        ->getNumberFormat()
                        ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

        $objPHPExcel->getActiveSheet()
                        ->getStyle('C')
                        ->getNumberFormat()
                        ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

        $objPHPExcel->getActiveSheet()
                        ->getStyle('D')
                        ->getNumberFormat()
                        ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

        $objPHPExcel->getActiveSheet()
                        ->getStyle('F')
                        ->getNumberFormat()
                        ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);


        $objWorkSheet->getStyle('A1')->getFont()->setSize(20);
		$objWorkSheet->getStyle('A1')->getFont()->setBold(true);
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:F1');
		$objWorkSheet->SetCellValue('A1', 'Laporan Income Rincian Detil Cash '.$d1['Name']);
		$objWorkSheet->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$objWorkSheet->getStyle('A2')->getFont()->setSize(16);
		$objWorkSheet->getStyle('A2')->getFont()->setBold(true);
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:F2');
		$objWorkSheet->SetCellValue('A2', date('d F Y', strtotime($y.'-'.$b.'-'.$funct->Denval(@$_GET['d']))));
		$objWorkSheet->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);


        // NOMOR URUT
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
		$objWorkSheet->getStyle('A4')->getFont()->setBold(true);
		$objWorkSheet->SetCellValue('A4', 'No.');
		$objWorkSheet->getStyle('A4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objWorkSheet->getStyle('A4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		// TRANS ID
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
		$objWorkSheet->getStyle('B4')->getFont()->setBold(true);
		$objWorkSheet->SetCellValue('B4', 'Trans ID');
		$objWorkSheet->getStyle('B4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objWorkSheet->getStyle('B4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	
		// CARD NO
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
		$objWorkSheet->getStyle('C4')->getFont()->setBold(true);
		$objWorkSheet->SetCellValue('C4', 'Card No');
		$objWorkSheet->getStyle('C4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objWorkSheet->getStyle('C4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	
		// PAID DATE
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
		$objWorkSheet->getStyle('D4')->getFont()->setBold(true);
		$objWorkSheet->SetCellValue('D4', 'Paid Date');
		$objWorkSheet->getStyle('D4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objWorkSheet->getStyle('D4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	
		// PAID AMOUNT
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(5);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);

		$objWorkSheet->getStyle('E4')->getFont()->setBold(true);
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('E4:F4');
		$objWorkSheet->SetCellValue('E4', 'Paid Date');
		$objWorkSheet->getStyle('E4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objWorkSheet->getStyle('E4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	// }


	// CONTENT { 
		$no = 1;
		$row = 5;
        $tgl = $y.'-'.$b.'-'.$funct->Denval(@$_GET['d']);

        $s2 = "SELECT a.TransID, a.CardNo, b.method, b.PaidDate, b.PaidAmount
            	from ".$funct->getValookie('koneksi').".parkinglist as a 
                inner join ".$funct->getValookie('koneksi').".parkinglistpayment as b 
                	on a.TransID = b.TransID 
                where a.VehicleType = '".$funct->getIDParam('detail')."' and b.PaidDate between '$tgl 00:00:00' and '$tgl 23:59:59' and method='".ucfirst(@$_GET['sb'])."' order by b.PaidDate ASC";
        $q2 = mysqli_query($funct->getConnection(), $s2) or die(mysqli_error($funct->getConnection()));
        while($d2 = mysqli_fetch_array($q2)) {

        	$objWorkSheet->SetCellValue('A'.$row, ' '.$funct->FormatNumber($no));
            $objWorkSheet->getStyle('A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

            $objWorkSheet->SetCellValue('B'.$row, ' '.$d2['TransID']);
            $objWorkSheet->getStyle('B'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            $objWorkSheet->SetCellValue('C'.$row, ' '.$d2['CardNo']);
            $objWorkSheet->getStyle('C'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            $objWorkSheet->SetCellValue('D'.$row, date('H:i:s', strtotime($d2['PaidDate'])) );
            $objWorkSheet->getStyle('D'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            $objWorkSheet->SetCellValue('E'.$row, 'Rp. ');
            $objWorkSheet->getStyle('E'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            $objWorkSheet->SetCellValue('F'.$row, ' '.$funct->FormatRupiah($d2['PaidAmount']));
            $objWorkSheet->getStyle('F'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

        	$no++; $row++;
        	$total = $total + $d2['PaidAmount'];
        }

        $objWorkSheet->getStyle('A'.$row)->getFont()->setBold(true);
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$row.':D'.$row);
		$objWorkSheet->SetCellValue('A'.$row, 'Total ');
		$objWorkSheet->getStyle('A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

		$objWorkSheet->getStyle('E'.$row)->getFont()->setBold(true);
        $objWorkSheet->SetCellValue('E'.$row, 'Rp. ');
        $objWorkSheet->getStyle('E'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

        $objWorkSheet->getStyle('F'.$row)->getFont()->setBold(true);
        $objWorkSheet->SetCellValue('F'.$row, ' '.$funct->FormatRupiah($total));
        $objWorkSheet->getStyle('F'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
	// }


	//exit;
	$objWorkSheet->setTitle('LaporanIncome Rinci DetailCash');
	$fileName = 'Laporan Income Rincian Detail Cash '.date('d F Y', strtotime($y.'-'.$b.'-'.$funct->Denval(@$_GET['d']))).'.xls';
}
else {

	// HEADER { 
		$objWorkSheet->getStyle('A1')->getFont()->setSize(15);
		$objWorkSheet->getStyle('A1')->getFont()->setBold(true);
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:C1');
		$objWorkSheet->SetCellValue('A1', 'Laporan Income Detil Cash '.$d1['Name']);
		$objWorkSheet->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$objWorkSheet->getStyle('A2')->getFont()->setSize(13);
		$objWorkSheet->getStyle('A2')->getFont()->setBold(true);
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:C2');
		$objWorkSheet->SetCellValue('A2', date('F Y', strtotime($y.'-'.$b.'-1')));
		$objWorkSheet->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	

		// TANGGAL
		$objPHPExcel->getActiveSheet()
                        ->getStyle('A')
                        ->getNumberFormat()
                        ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(8);
		$objWorkSheet->getStyle('A4')->getFont()->setBold(true);
		$objWorkSheet->SetCellValue('A4', 'Tanggal');
		$objWorkSheet->getStyle('A4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objWorkSheet->getStyle('A4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		// JUMLAH
		$objPHPExcel->getActiveSheet()
                        ->getStyle('B')
                        ->getNumberFormat()
                        ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

        $objPHPExcel->getActiveSheet()
                        ->getStyle('C')
                        ->getNumberFormat()
                        ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(5);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);

		$objWorkSheet->getStyle('B4')->getFont()->setBold(true);
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('B4:C4');
		$objWorkSheet->SetCellValue('B4', 'Jumlah');
		$objWorkSheet->getStyle('B4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objWorkSheet->getStyle('B4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	// }


	// CONTENT {
		$row = 5;
		$t = date('t', strtotime($y.'-'.$b.'-'.$funct->Denval(@$_GET['d'])));
		for($dd = 1; $dd <= $t; $dd++) {
			$tgl = $y.'-'.$b.'-'.$dd;

			$s2 = "SELECT b.PaidDate, Sum(b.PaidAmount) as PaidAmount 
					from ".$funct->getValookie('koneksi').".parkinglist as a 
                    inner join ".$funct->getValookie('koneksi').".parkinglistpayment as b 
                    	on a.TransID = b.TransID 
                    where a.VehicleType = '".$funct->getIDParam('detail')."' and b.PaidDate between '$tgl 00:00:00' and '$tgl 23:59:59' and method='".ucfirst(@$_GET['sb'])."' order by b.PaidDate ASC";
            $q2 = mysqli_query($funct->getConnection(), $s2) or die(mysqli_error($funct->getConnection()));
            while($d2 = mysqli_fetch_array($q2)) {
            	if( !($d2['PaidAmount'] == '') || !empty($d2['PaidAmount']) || !is_null($d2['PaidAmount'])) {

            		$objWorkSheet->SetCellValue('A'.$row, ' '.date('d', strtotime($d2['PaidDate'])));
            		$objWorkSheet->getStyle('A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

            		$objWorkSheet->SetCellValue('B'.$row, 'Rp. ');
            		$objWorkSheet->getStyle('B'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            		$objWorkSheet->SetCellValue('C'.$row, ' '.$funct->FormatRupiah($d2['PaidAmount']));
            		$objWorkSheet->getStyle('C'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

            		$total = $total + $d2['PaidAmount'];
            		$row++;
            	}
            }
		}

		$objWorkSheet->getStyle('A'.$row)->getFont()->setBold(true);
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$row.':B'.$row);
		$objWorkSheet->SetCellValue('A'.$row, 'Total Rp. ');
		$objWorkSheet->getStyle('A'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objWorkSheet->getStyle('A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
	
		$objWorkSheet->getStyle('C'.$row)->getFont()->setBold(true);
		$objWorkSheet->SetCellValue('C'.$row, ' '.$funct->FormatRupiah($total));
        $objWorkSheet->getStyle('C'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
	// }


	//exit;
	$objWorkSheet->setTitle('Laporan Income Detail Cash');
	$fileName = 'Laporan Income Detail Cash '.$d1['Name'].' '.date('F Y', strtotime($y.'-'.$b.'-1')).'.xls';
}


$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');
?>