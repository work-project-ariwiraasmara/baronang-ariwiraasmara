<?php
if(isset($_GET['b'])) { $b = $funct->Denval(@$_GET['b']); } else { $b = date('m'); }
if(isset($_GET['y'])) { $y = $funct->Denval(@$_GET['y']); } else { $y = date('Y'); }

$s1 = "SELECT VehicleID, Code, Name from ".$funct->getValookie('koneksi').".vehicletype where VehicleID='".$funct->getIDParam('detail')."' order by VehicleID ASC";
$q1 = mysqli_query($funct->getConnection(), $s1) or die(mysqli_error($funct->getConnection()));
$d1 = mysqli_fetch_array($q1);

$s2 = "SELECT ProductID from ".$funct->getValookie('koneksi').".locationparkingproduct where VehicleID='$d1[VehicleID]'";
$q2 = mysqli_query($funct->getConnection(), $s2) or die(mysqli_error($funct->getConnection()));
$d2 = mysqli_fetch_array($q2);
?>
<div id="toolbar" class="primary-color">
    <div class="open-left">
        <a class="modal-trigger" href="?pg=income">
            <i class="ion-android-arrow-back txt-white"></i>
        </a>
    </div>

    <span class="title bold txt-white" id="title"></span>

    <div class="open-right">
        <a href="#download" id="<?php if(isset($_GET['d'])) { echo 'download_rincian'; } else { echo 'download'; } ?>">
            <i class="ion-android-download txt-white"></i>
        </a>
    </div>
</div>

<div class="animated fadeinup delay-1">
    <div class="page-content">

        <h1 class="uppercase bold txt-black italic">
            <?php 
            if(isset($_GET['d'])) {
                echo 'Rincian Detail '.@$_GET['sb'].' '.$d1['Name'];
            }
            else {
                echo 'Detail '.@$_GET['sb'].' '.$d1['Name']; 
            }
            ?>    
        </h1>

        <h3 class="uppercase bold txt-black italic">
            <?php 
            if(isset($_GET['d'])) {
                echo date('d F Y', strtotime($y.'-'.$b.'-'.$funct->Denval(@$_GET['d'])));
            }
            else {
                echo date('F Y', strtotime($y.'-'.$b.'-1'));
            }
            ?>  
        </h3>

        <div class="m-t-50">
            <table>
                <thead>
                    <tr>
                        <?php
                        if(isset($_GET['d'])) { ?>
                            <th>No.</th>
                            <th>TransNo</th>
                            <th>No. Card</th>
                            <th>TransDate</th>
                            <th colspan="2" style="text-align: center;">Amount</th>
                            <?php
                        }
                        else { ?>
                            <th>Tanggal</th>
                            <th colspan="2" style="text-align: center;">Jumlah</th>
                            <?php
                        }
                        ?>
                    </tr>
                </thead>

                <tbody>
                    <?php
                    $total = 0;

                    if(isset($_GET['d'])) {
                        $no = 1;
                        $tgl = $y.'-'.$b.'-'.$funct->Denval(@$_GET['d']);

                        $s3 = "SELECT TransNo, Debet, Kredit, TransactionType, TransDate, Amount from ".$funct->getValookie('koneksi').".translist where TransactionType='MBRS' and Kredit='$d2[ProductID]' and TransDate between '$tgl 00:00:00' and '$tgl 23:59:59' order by TransDate ASC";
                        $q3 = mysqli_query($funct->getConnection(), $s3) or die(mysqli_error($funct->getConnection()));
                        while($d3 = mysqli_fetch_array($q3)) { ?>
                            <tr>
                                <td><?php echo $no; ?></td>
                                <td><?php echo $d3['TransNo']; ?></td>
                                <td><?php echo $d3['Debet']; ?></td>
                                <td><?php echo date('H:i:s', strtotime($d3['TransDate'])); ?></td>
                                <td>Rp.</td>
                                <td style="text-align: right;"><?php echo $funct->FormatRupiah($d3['Amount']); ?></td>
                            </tr>
                            <?php
                            $total = $total + $d3['Amount'];
                            $no++;
                        }
                    }
                    else {
                        $t = date('t', strtotime($y.'-'.$b.'-1'));

                        for($dd = 1; $dd <= $t; $dd++) {
                            $tgl = $y.'-'.$b.'-'.$dd;

                            $s3 = "SELECT TransDate, Sum(Amount) as Amount from ".$funct->getValookie('koneksi').".translist where TransactionType='MBRS' and Kredit='$d2[ProductID]' and TransDate between '$tgl 00:00:00' and '$tgl 23:59:59'";
                            $q3 = mysqli_query($funct->getConnection(), $s3) or die(mysqli_error($funct->getConnection()));
                            while($d3 = mysqli_fetch_array($q3)) { 
                                if( !($d3['Amount'] == '') || !empty($d3['Amount']) || !is_null($d3['Amount'])) { ?>
                                    <tr>
                                        <td class="txt-link"><a href="<?php echo '?pg=income&sb=member&'.$funct->setIDParam('detail', $d1['VehicleID']).'&d='.$funct->Enval($dd).'&b='.$funct->Enval($b).'&y='.$funct->Enval($y); ?>"><?php echo $dd; ?></a></td>
                                        <td>Rp.</td>
                                        <td style="text-align: right;"><?php echo $funct->FormatRupiah($d3['Amount']); ?></td>
                                    </tr>
                                    <?php 
                                    $total = $total + $d3['Amount'];
                                }
                            }
                        }
                    }
                    ?>
                </tbody>

                <tfoot>
                    <tr>
                        <?php
                        if(isset($_GET['d'])) { ?>
                            <th colspan="4" style="text-align: right;">Total</th>
                            <?php
                        }
                        else { ?>
                            <th style="text-align: right;">Total</th>
                            <?php
                        }
                        ?>
                        <th>Rp.</th>
                        <th style="text-align: right;"><?php echo $funct->FormatRupiah($total); ?></th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('#download').click(function(){
        window.location.href = "<?php echo 'index_download_excel.php?pg=income&sb=member&download=1&'.$funct->setIDParam('detail', $d1['VehicleID']).'&b='.$funct->Enval($b).'&y='.$funct->Enval($y); ?>";
    });

    $('#download_rincian').click(function(){
        window.location.href = "<?php echo 'index_download_excel.php?pg=income&sb=member&download=1&'.$funct->setIDParam('detail', $d1['VehicleID']).'&b='.$funct->Enval($b).'&y='.$funct->Enval($y).'&d='.@$_GET['d']; ?>";
    });
</script>