<?php
if(isset($_GET['b'])) { $b = $funct->Denval(@$_GET['b']); } else { $b = date('m'); }
if(isset($_GET['y'])) { $y = $funct->Denval(@$_GET['y']); } else { $y = date('Y'); }

$s1 = "SELECT VehicleID, Code, Name from ".$funct->getValookie('koneksi').".vehicletype where Code='".$funct->getIDParam('detail')."' order by VehicleID ASC";
$q1 = mysqli_query($funct->getConnection(), $s1) or die(mysqli_error($funct->getConnection()));
$d1 = mysqli_fetch_array($q1);
?>
<div id="toolbar" class="primary-color">
	<div class="open-left">
        <a class="modal-trigger" href="?pg=income">
            <i class="ion-android-arrow-back txt-white"></i>
        </a>
    </div>

    <span class="title bold txt-white" id="title"></span>

    <div class="open-right">
        <a href="#download" id="<?php if(isset($_GET['d'])) { echo 'download_rincian'; } else { echo 'download'; } ?>">
            <i class="ion-android-download txt-white"></i>
        </a>
    </div>
</div>

<div class="animated fadeinup delay-1">
    <div class="page-content txt-black">
        
        <h1 class="uppercase bold txt-black italic">
            <?php 
            if(isset($_GET['d'])) {
                echo 'Rincian Detail '.@$_GET['sb'].' '.$d1['Name']; 
            }
            else {
                echo 'Detail '.@$_GET['sb'].' '.$d1['Name']; 
            }
            ?>
         </h1>
        
        <h3 class="uppercase bold txt-black italic">
            <?php 
            if(isset($_GET['d'])) {
                echo date('d F Y', strtotime($y.'-'.$b.'-'.$funct->Denval(@$_GET['d'])));
            }
            else {
                echo date('F Y', strtotime($y.'-'.$b.'-1'));
            }
            ?>
        </h3>

        <div class="m-t-50">
            <table class="table">
                <thead>
                    <tr>
                        <?php
                        if(!isset($_GET['d'])) { ?>
                            <th>Tanggal</th>
                            <th colspan="2" style="text-align: center;">Jumlah</th>
                            <?php
                        }
                        else { ?>
                            <th>No.</th>
                            <th>Trans ID</th>
                            <th>Card No</th>
                            <th>Paid Date</th>
                            <th colspan="2" style="text-align: center;">Paid Amount</th>
                            <?php
                        }
                        ?>
                    </tr>
                </thead>

                <tbody>
                    <?php
                    $total = 0;

                    if(isset($_GET['d'])) {
                        $no = 1;
                        $tgl = $y.'-'.$b.'-'.$funct->Denval(@$_GET['d']);

                        $s2 = "SELECT a.TransID, a.CardNo, b.method, b.PaidDate, b.PaidAmount
                            from ".$funct->getValookie('koneksi').".parkinglist as a 
                            inner join ".$funct->getValookie('koneksi').".parkinglistpayment as b 
                                on a.TransID = b.TransID 
                            where a.VehicleType = '".$funct->getIDParam('detail')."' and b.PaidDate between '$tgl 00:00:00' and '$tgl 23:59:59' and method='".ucfirst(@$_GET['sb'])."' order by b.PaidDate ASC";
                        $q2 = mysqli_query($funct->getConnection(), $s2) or die(mysqli_error($funct->getConnection()));
                        while($d2 = mysqli_fetch_array($q2)) { ?>
                            <tr>
                                <td><?php echo $no; ?></td>
                                <td><?php echo $d2['TransID']; ?></td>
                                <td><?php echo $d2['CardNo']; ?></td>
                                <td><?php echo date('d-m-Y H:i:s', strtotime($d2['PaidDate'])); ?></td>
                                <td>Rp.</td>
                                <td style="text-align: right;"><?php echo $funct->FormatRupiah($d2['PaidAmount']); ?></td>
                            </tr>
                            <?php
                            $no++;
                            $total = $total + $d2['PaidAmount'];
                        }
                    }
                    else {
                        $t = date('t', strtotime($y.'-'.$b.'-'.$funct->Denval(@$_GET['d'])));
                        for($dd = 1; $dd <= $t; $dd++) {
                            $tgl = $y.'-'.$b.'-'.$dd;

                            $s2 = "SELECT b.PaidDate, Sum(b.PaidAmount) as PaidAmount
                                    from ".$funct->getValookie('koneksi').".parkinglist as a 
                                    inner join ".$funct->getValookie('koneksi').".parkinglistpayment as b 
                                        on a.TransID = b.TransID 
                                    where a.VehicleType = '".$funct->getIDParam('detail')."' and b.PaidDate between '$tgl 00:00:00' and '$tgl 23:59:59' and method='".ucfirst(@$_GET['sb'])."' order by b.PaidDate ASC";
                            $q2 = mysqli_query($funct->getConnection(), $s2) or die(mysqli_error($funct->getConnection()));
                            while($d2 = mysqli_fetch_array($q2)) { 
                                if( !($d2['PaidAmount'] == '') || !empty($d2['PaidAmount']) || !is_null($d2['PaidAmount'])) { ?>
                                    <tr>
                                        <td><a href="<?php echo '?pg=income&sb=cash&'.$funct->setIDParam('detail', $d1['Code']).'&d='.$funct->Enval($dd).'&b='.$funct->Enval($b).'&y='.$funct->Enval($y); ?>"><?php echo date('d', strtotime($d2['PaidDate'])); ?></a></td>
                                        <td>Rp.</td>
                                        <td style="text-align: right;"><?php echo $funct->FormatRupiah($d2['PaidAmount']); ?></td>
                                    </tr>
                                    <?php
                                }
                                
                                $total = $total + $d2['PaidAmount'];
                            }
                        }
                    }
                    ?>
                </tbody>

                <tfoot>
                    <tr>
                        <?php
                        if(isset($_GET['d'])) { ?>
                            <th colspan="4" style="text-align: right;">Total</th>
                            <?php
                        }
                        else { ?>
                            <th style="text-align: right;">Total</th>
                            <?php
                        }
                        ?>
                        <th>Rp.</th>
                        <th style="text-align: right;"><?php echo $funct->FormatRupiah($total); ?></th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('#download').click(function(){
        window.location.href = "<?php echo 'index_download_excel.php?pg=income&sb=cash&download=1&'.$funct->setIDParam('detail', $d1['Code']).'&b='.$funct->Enval($b).'&y='.$funct->Enval($y); ?>";
    });

    $('#download_rincian').click(function(){
        window.location.href = "<?php echo 'index_download_excel.php?pg=income&sb=cash&download=1&'.$funct->setIDParam('detail', $d1['Code']).'&b='.$funct->Enval($b).'&y='.$funct->Enval($y).'&d='.@$_GET['d']; ?>";
    });
</script>