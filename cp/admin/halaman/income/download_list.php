<?php
if(isset($_GET['b'])) { $b = $funct->Denval(@$_GET['b']); } else { $b = date('m'); }
if(isset($_GET['y'])) { $y = $funct->Denval(@$_GET['y']); } else { $y = date('Y'); }

// HEADER {
	$objWorkSheet->getStyle('A1')->getFont()->setSize(20);
	$objWorkSheet->getStyle('A1')->getFont()->setBold(true);
	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:N1');
	$objWorkSheet->SetCellValue('A1', 'Laporan Income');
	$objWorkSheet->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

	$objWorkSheet->getStyle('A2')->getFont()->setSize(16);
	$objWorkSheet->getStyle('A2')->getFont()->setBold(true);
	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:N2');
	$objWorkSheet->SetCellValue('A2', date('F Y', strtotime($y.'-'.$b.'-1')));
	$objWorkSheet->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);


	// NOMOR URUT {
		$objPHPExcel->getActiveSheet()
                        ->getStyle('A')
                        ->getNumberFormat()
                        ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
		$objWorkSheet->getStyle('A4')->getFont()->setBold(true);
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A4:A6');
		$objWorkSheet->SetCellValue('A4', 'No.');
		$objWorkSheet->getStyle('A4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objWorkSheet->getStyle('A4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	// }



	// JENIS KENDARAAN {
		$objPHPExcel->getActiveSheet()
                        ->getStyle('B')
                        ->getNumberFormat()
                        ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(18);
		$objWorkSheet->getStyle('B4')->getFont()->setBold(true);
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('B4:B6');
		$objWorkSheet->SetCellValue('B4', 'Jenis Kendaraan');
		$objWorkSheet->getStyle('B4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objWorkSheet->getStyle('B4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	// }



	// TOTAL PENDAPATAN {
		$objPHPExcel->getActiveSheet()
                        ->getStyle('C')
                        ->getNumberFormat()
                        ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(17);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(22);

		$objWorkSheet->getStyle('C4')->getFont()->setBold(true);
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('C4:D4');
		$objWorkSheet->SetCellValue('C4', 'Total Pendapatan');
		$objWorkSheet->getStyle('C4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objWorkSheet->getStyle('C4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$objWorkSheet->getStyle('C5')->getFont()->setBold(true);
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('C5:C6');
		$objWorkSheet->SetCellValue('C5', 'QTY');
		$objWorkSheet->getStyle('C5')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objWorkSheet->getStyle('C5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$objWorkSheet->getStyle('D5')->getFont()->setBold(true);
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('D5:D6');
		$objWorkSheet->SetCellValue('D5', 'Nilai');
		$objWorkSheet->getStyle('D5')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objWorkSheet->getStyle('D5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	// }


	// JENIS PEMBAYARAN {

		$objPHPExcel->getActiveSheet()
                        ->getStyle('E')
                        ->getNumberFormat()
                        ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

		$objWorkSheet->getStyle('E4')->getFont()->setBold(true);
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('E4:N4');
		$objWorkSheet->SetCellValue('E4', 'Jenis Pembayaran');
		$objWorkSheet->getStyle('E4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objWorkSheet->getStyle('E4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		

		// TUNAI {
			$objWorkSheet->getStyle('E5')->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('E5:F5');
			$objWorkSheet->SetCellValue('E5', 'Tunai');
			$objWorkSheet->getStyle('E5')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objWorkSheet->getStyle('E5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(17);
			$objWorkSheet->getStyle('E6')->getFont()->setBold(true);
			$objWorkSheet->SetCellValue('E6', 'QTY');
			$objWorkSheet->getStyle('E6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objWorkSheet->getStyle('E6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(22);
			$objWorkSheet->getStyle('F6')->getFont()->setBold(true);
			$objWorkSheet->SetCellValue('F6', 'Nilai');
			$objWorkSheet->getStyle('F6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objWorkSheet->getStyle('F6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		// }


		
		// OVO {
			$objWorkSheet->getStyle('G5')->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('G5:H5');
			$objWorkSheet->SetCellValue('G5', 'Ovo');
			$objWorkSheet->getStyle('G5')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objWorkSheet->getStyle('G5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(17);
			$objWorkSheet->getStyle('G6')->getFont()->setBold(true);
			$objWorkSheet->SetCellValue('G6', 'QTY');
			$objWorkSheet->getStyle('G6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objWorkSheet->getStyle('G6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(22);
			$objWorkSheet->getStyle('H6')->getFont()->setBold(true);
			$objWorkSheet->SetCellValue('H6', 'Nilai');
			$objWorkSheet->getStyle('H6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objWorkSheet->getStyle('H6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		// }
		

		
		// MEMBER {
			$objWorkSheet->getStyle('I5')->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('I5:J5');
			$objWorkSheet->SetCellValue('I5', 'Member');
			$objWorkSheet->getStyle('I5')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objWorkSheet->getStyle('I5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(17);
			$objWorkSheet->getStyle('I6')->getFont()->setBold(true);
			$objWorkSheet->SetCellValue('I6', 'QTY');
			$objWorkSheet->getStyle('I6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objWorkSheet->getStyle('I6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(22);
			$objWorkSheet->getStyle('J6')->getFont()->setBold(true);
			$objWorkSheet->SetCellValue('J6', 'Nilai');
			$objWorkSheet->getStyle('J6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objWorkSheet->getStyle('J6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		// }
		

		
		// DENDA {
			$objWorkSheet->getStyle('K5')->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('K5:L5');
			$objWorkSheet->SetCellValue('K5', 'Denda');
			$objWorkSheet->getStyle('K5')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objWorkSheet->getStyle('K5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(17);
			$objWorkSheet->getStyle('K6')->getFont()->setBold(true);
			$objWorkSheet->SetCellValue('K6', 'QTY');
			$objWorkSheet->getStyle('K6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objWorkSheet->getStyle('K6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(22);
			$objWorkSheet->getStyle('L6')->getFont()->setBold(true);
			$objWorkSheet->SetCellValue('L6', 'Nilai');
			$objWorkSheet->getStyle('L6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objWorkSheet->getStyle('L6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		// }
		

		
		// EMONEY {
			$objWorkSheet->getStyle('M5')->getFont()->setBold(true);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('M5:N5');
			$objWorkSheet->SetCellValue('M5', 'Emoney');
			$objWorkSheet->getStyle('M5')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objWorkSheet->getStyle('M5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(17);
			$objWorkSheet->getStyle('M6')->getFont()->setBold(true);
			$objWorkSheet->SetCellValue('M6', 'QTY');
			$objWorkSheet->getStyle('M6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objWorkSheet->getStyle('M6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(22);
			$objWorkSheet->getStyle('N6')->getFont()->setBold(true);
			$objWorkSheet->SetCellValue('N6', 'Nilai');
			$objWorkSheet->getStyle('N6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objWorkSheet->getStyle('N6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		// }
		

	// }

// }



// CONTENT {
	
	$row = 7;
	$no = 1;

	$s1 = "SELECT VehicleID, Code, Name from ".$funct->getValookie('koneksi').".vehicletype order by VehicleID ASC";
	$q1 = mysqli_query($funct->getConnection(), $s1) or die(mysqli_error($funct->getConnection()));
	while($data = mysqli_fetch_array($q1)) {

		$totqty = 0;
		$totnil = 0;

		$s2 = "SELECT distinct TransID from ".$funct->getValookie('koneksi').".parkinglist where VehicleType='$data[Code]'";
		$q2 = mysqli_query($funct->getConnection(), $s2) or die(mysqli_error($funct->getConnection()));
		$d2 = mysqli_fetch_array($q2);

		// get Tunai
		$s3 = "SELECT count(b.PaidDate) as QTY, sum(b.PaidAmount) as Nilai
				from parkinglist as a inner join ".$funct->getValookie('koneksi').".parkinglistpayment as b on a.TransID = b.TransID where a.VehicleType='$data[Code]' and b.Method='CASH' and Month(b.PaidDate)='$b' and Year(b.PaidDate)='$y'";
		$q3 = mysqli_query($funct->getConnection(), $s3) or die(mysqli_error($funct->getConnection()));
		$d3 = mysqli_fetch_array($q3);

		// get Ovo
		$s4 = "SELECT count(b.PaidDate) as QTY, sum(b.PaidAmount) as Nilai
				from parkinglist as a inner join ".$funct->getValookie('koneksi').".parkinglistpayment as b on a.TransID = b.TransID where a.VehicleType='$data[Code]' and b.Method='OVO' and Month(b.PaidDate)='$b' and Year(b.PaidDate)='$y'";
		$q4 = mysqli_query($funct->getConnection(), $s4) or die(mysqli_error($funct->getConnection()));
		$d4 = mysqli_fetch_array($q4);

		// get Member
		$s5 = "SELECT ProductID from ".$funct->getValookie('koneksi').".locationparkingproduct where VehicleID='$data[VehicleID]'";
		$q5 = mysqli_query($funct->getConnection(), $s5) or die(mysqli_error($funct->getConnection()));
		$d5 = mysqli_fetch_array($q5);

		$s6 = "select Count(TransDate) as QTY, Sum(Amount) as Nilai from ".$funct->getValookie('koneksi').".translist where TransactionType='MBRS' and Kredit='$d5[ProductID]' and Month(TransDate)='$b' and Year(TransDate)='$y' order by Kredit ASC";
		$q6 = mysqli_query($funct->getConnection(), $s6) or die(mysqli_error($funct->getConnection()));
		$d6 = mysqli_fetch_array($q6);

		// get Denda
		// get Emoney

		// ISI KOLOM {

			// NOMOR
			$objWorkSheet->SetCellValue('A'.$row, $no);
            $objWorkSheet->getStyle('A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

            // JENIS KENDARAAN
            $objWorkSheet->SetCellValue('B'.$row, $data['Name']);

            // JENIS PEMBAYARAN { 
            	// TUNAI { 
            		// QTY
	            	$objWorkSheet->SetCellValue('E'.$row, ' '.$funct->FormatNumber($d3['QTY']));
	            	$objWorkSheet->getStyle('E'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

	            	// NILAI
	            	$objWorkSheet->SetCellValue('F'.$row, ' '.$funct->FormatRupiah($d3['Nilai']));
	            	$objWorkSheet->getStyle('F'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            	// }

            	// OVO { 
            		// QTY
	            	$objWorkSheet->SetCellValue('G'.$row, ' '.$funct->FormatNumber($d4['QTY']));
	            	$objWorkSheet->getStyle('G'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

	            	// NILAI
	            	$objWorkSheet->SetCellValue('H'.$row, ' '.$funct->FormatRupiah($d4['Nilai']));
	            	$objWorkSheet->getStyle('H'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            	// }

            	// MEMBER { 
            		// QTY
	            	$objWorkSheet->SetCellValue('I'.$row, ' '.$funct->FormatNumber($d6['QTY']));
	            	$objWorkSheet->getStyle('I'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

	            	// NILAI
	            	$objWorkSheet->SetCellValue('J'.$row, ' '.$funct->FormatRupiah($d6['Nilai']));
	            	$objWorkSheet->getStyle('J'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            	// }

            	// DENDA { 
            		
            	// }

            	// EMONEY { 
            		
            	// }
            // }

            // TOTAL {
            	$totqty = $d3['QTY']   + $d4['QTY'] + $d6['QTY'];
				$totnil = $d3['Nilai'] + $d4['Nilai'] + $d6['Nilai'];

            	// QTY
            	$objWorkSheet->SetCellValue('C'.$row, ' '.$funct->FormatNumber($totqty));
            	$objWorkSheet->getStyle('C'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

            	// NILAI
            	$objWorkSheet->SetCellValue('D'.$row, ' '.$funct->FormatRupiah($totnil));
            	$objWorkSheet->getStyle('D'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            // }

		// }

		
		$no++; $row++;
	}
//  }


//exit;
$objWorkSheet->setTitle('Laporan Income');

$fileName = 'Laporan Income '.date('F Y', strtotime($y.'-'.$b.'-1')).'.xls';
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');
?>