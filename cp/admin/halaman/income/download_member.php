<?php
if(isset($_GET['b'])) { $b = $funct->Denval(@$_GET['b']); } else { $b = date('m'); }
if(isset($_GET['y'])) { $y = $funct->Denval(@$_GET['y']); } else { $y = date('Y'); }

$s1 = "SELECT VehicleID, Code, Name from ".$funct->getValookie('koneksi').".vehicletype where VehicleID='".$funct->getIDParam('detail')."' order by VehicleID ASC";
$q1 = mysqli_query($funct->getConnection(), $s1) or die(mysqli_error($funct->getConnection()));
$d1 = mysqli_fetch_array($q1);

$s2 = "SELECT ProductID from ".$funct->getValookie('koneksi').".locationparkingproduct where VehicleID='$d1[VehicleID]'";
$q2 = mysqli_query($funct->getConnection(), $s2) or die(mysqli_error($funct->getConnection()));
$d2 = mysqli_fetch_array($q2);

$total = 0;

if(isset($_GET['d'])) {

	// HEADER { 

		$objPHPExcel->getActiveSheet()
                        ->getStyle('A')
                        ->getNumberFormat()
                        ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

        $objPHPExcel->getActiveSheet()
                        ->getStyle('B')
                        ->getNumberFormat()
                        ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

        $objPHPExcel->getActiveSheet()
                        ->getStyle('C')
                        ->getNumberFormat()
                        ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

        $objPHPExcel->getActiveSheet()
                        ->getStyle('D')
                        ->getNumberFormat()
                        ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

        $objPHPExcel->getActiveSheet()
                        ->getStyle('F')
                        ->getNumberFormat()
                        ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
		
        $objWorkSheet->getStyle('A1')->getFont()->setSize(20);
		$objWorkSheet->getStyle('A1')->getFont()->setBold(true);
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:F1');
		$objWorkSheet->SetCellValue('A1', 'Laporan Income Rincian Detil Member '.$d1['Name']);
		$objWorkSheet->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$objWorkSheet->getStyle('A2')->getFont()->setSize(16);
		$objWorkSheet->getStyle('A2')->getFont()->setBold(true);
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:F2');
		$objWorkSheet->SetCellValue('A2', date('d F Y', strtotime($y.'-'.$b.'-'.$funct->Denval(@$_GET['d']))));
		$objWorkSheet->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);


        // NOMOR URUT
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
		$objWorkSheet->getStyle('A4')->getFont()->setBold(true);
		$objWorkSheet->SetCellValue('A4', 'No.');
		$objWorkSheet->getStyle('A4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objWorkSheet->getStyle('A4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		// TRANSNO
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
		$objWorkSheet->getStyle('B4')->getFont()->setBold(true);
		$objWorkSheet->SetCellValue('B4', 'TransNo');
		$objWorkSheet->getStyle('B4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objWorkSheet->getStyle('B4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	
		// CARD NO
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
		$objWorkSheet->getStyle('C4')->getFont()->setBold(true);
		$objWorkSheet->SetCellValue('C4', 'Card No');
		$objWorkSheet->getStyle('C4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objWorkSheet->getStyle('C4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	
		// PAID DATE
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
		$objWorkSheet->getStyle('D4')->getFont()->setBold(true);
		$objWorkSheet->SetCellValue('D4', 'Trans Date');
		$objWorkSheet->getStyle('D4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objWorkSheet->getStyle('D4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	
		// PAID AMOUNT
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(5);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);

		$objWorkSheet->getStyle('E4')->getFont()->setBold(true);
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('E4:F4');
		$objWorkSheet->SetCellValue('E4', 'Amount');
		$objWorkSheet->getStyle('E4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objWorkSheet->getStyle('E4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	// }


	// CONTENT { 
		$no = 1;
		$row = 5;
        $tgl = $y.'-'.$b.'-'.$funct->Denval(@$_GET['d']);

        $s3 = "SELECT TransNo, Debet, Kredit, TransactionType, TransDate, Amount from ".$funct->getValookie('koneksi').".translist where TransactionType='MBRS' and Kredit='$d2[ProductID]' and TransDate between '$tgl 00:00:00' and '$tgl 23:59:59' order by TransDate ASC";
        $q3 = mysqli_query($funct->getConnection(), $s3) or die(mysqli_error($funct->getConnection()));
        while($d3 = mysqli_fetch_array($q3)) {

        	$objWorkSheet->SetCellValue('A'.$row, ' '.$funct->FormatNumber($no));
            $objWorkSheet->getStyle('A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

            $objWorkSheet->SetCellValue('B'.$row, ' '.$d3['TransNo']);
            $objWorkSheet->getStyle('B'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            $objWorkSheet->SetCellValue('C'.$row, ' '.$d3['Debet']);
            $objWorkSheet->getStyle('C'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            $objWorkSheet->SetCellValue('D'.$row, date('H:i:s', strtotime($d3['TransDate'])) );
            $objWorkSheet->getStyle('D'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            $objWorkSheet->SetCellValue('E'.$row, 'Rp. ');
            $objWorkSheet->getStyle('E'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            $objWorkSheet->SetCellValue('F'.$row, ' '.$funct->FormatRupiah($d3['Amount']));
            $objWorkSheet->getStyle('F'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

        	$total = $total + $d3['Amount'];
            $no++; $row++;
        }

        $objWorkSheet->getStyle('A'.$row)->getFont()->setBold(true);
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$row.':D'.$row);
		$objWorkSheet->SetCellValue('A'.$row, 'Total ');
		$objWorkSheet->getStyle('A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

		$objWorkSheet->getStyle('E'.$row)->getFont()->setBold(true);
        $objWorkSheet->SetCellValue('E'.$row, 'Rp. ');
        $objWorkSheet->getStyle('E'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

        $objWorkSheet->getStyle('F'.$row)->getFont()->setBold(true);
        $objWorkSheet->SetCellValue('F'.$row, ' '.$funct->FormatRupiah($total));
        $objWorkSheet->getStyle('F'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
	// }


	//exit;
	$objWorkSheet->setTitle('LaporanIncome Rinci DetailMemb');
	$fileName = 'Laporan Income Rincian Detail Member '.$d1['Name'].' '.date('d F Y', strtotime($y.'-'.$b.'-'.$funct->Denval(@$_GET['d']))).'.xls';
}
else {

	// HEADER { 
		$objWorkSheet->getStyle('A1')->getFont()->setSize(13);
		$objWorkSheet->getStyle('A1')->getFont()->setBold(true);
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:C1');
		$objWorkSheet->SetCellValue('A1', 'Laporan Income Detil Member '.$d1['Name']);
		$objWorkSheet->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$objWorkSheet->getStyle('A2')->getFont()->setSize(13);
		$objWorkSheet->getStyle('A2')->getFont()->setBold(true);
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:C2');
		$objWorkSheet->SetCellValue('A2', date('F Y', strtotime($y.'-'.$b.'-1')));
		$objWorkSheet->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	

		// TANGGAL
		$objPHPExcel->getActiveSheet()
                        ->getStyle('A')
                        ->getNumberFormat()
                        ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(8);
		$objWorkSheet->getStyle('A4')->getFont()->setBold(true);
		$objWorkSheet->SetCellValue('A4', 'Tanggal');
		$objWorkSheet->getStyle('A4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objWorkSheet->getStyle('A4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		// JUMLAH
		$objPHPExcel->getActiveSheet()
                        ->getStyle('B')
                        ->getNumberFormat()
                        ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

        $objPHPExcel->getActiveSheet()
                        ->getStyle('C')
                        ->getNumberFormat()
                        ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(5);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);

		$objWorkSheet->getStyle('B4')->getFont()->setBold(true);
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('B4:C4');
		$objWorkSheet->SetCellValue('B4', 'Jumlah');
		$objWorkSheet->getStyle('B4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objWorkSheet->getStyle('B4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	// }


	// CONTENT { 
		$row = 5;
		$t = date('t', strtotime($y.'-'.$b.'-1'));

		for($dd = 1; $dd <= $t; $dd++) {
			$tgl = $y.'-'.$b.'-'.$dd;

			$s3 = "SELECT TransDate, Sum(Amount) as Amount from ".$funct->getValookie('koneksi').".translist where TransactionType='MBRS' and Kredit='$d2[ProductID]' and TransDate between '$tgl 00:00:00' and '$tgl 23:59:59'";
            $q3 = mysqli_query($funct->getConnection(), $s3) or die(mysqli_error($funct->getConnection()));
		
            while($d3 = mysqli_fetch_array($q3)) {
            	if( !($d3['Amount'] == '') || !empty($d3['Amount']) || !is_null($d3['Amount'])) {

            		$objWorkSheet->SetCellValue('A'.$row, ' '.date('d', strtotime($d3['TransDate'])));
            		$objWorkSheet->getStyle('A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

            		$objWorkSheet->SetCellValue('B'.$row, 'Rp. ');
            		$objWorkSheet->getStyle('B'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            		$objWorkSheet->SetCellValue('C'.$row, ' '.$funct->FormatRupiah($d3['Amount']));
            		$objWorkSheet->getStyle('C'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

            		$total = $total + $d3['Amount'];
            		$row++;
            	}
            }
		}

		$objWorkSheet->getStyle('A'.$row)->getFont()->setBold(true);
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$row.':B'.$row);
		$objWorkSheet->SetCellValue('A'.$row, 'Total Rp. ');
		$objWorkSheet->getStyle('A'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$objWorkSheet->getStyle('A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
	
		$objWorkSheet->getStyle('C'.$row)->getFont()->setBold(true);
		$objWorkSheet->SetCellValue('C'.$row, ' '.$funct->FormatRupiah($total));
        $objWorkSheet->getStyle('C'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
	// }


	//exit;
	$objWorkSheet->setTitle('Laporan Income Detail Member');
	$fileName = 'Laporan Income Detail Member '.$d1['Name'].' '.date('F Y', strtotime($y.'-'.$b.'-1')).'.xls';
}

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');
?>