<div id="toolbar" class="primary-color">
    <div class="open-left" id="open-left" data-activates="slide-out-left">
        <i class="ion-android-menu"></i>
    </div>
    <span class="title bold txt-white" id="title"></span>
    <div class="open-right">
        <a href="?pg=locationgate&sb=add">
        	<i class="ion-android-add txt-white"></i>
        </a>
    </div>
</div>

<div class="animated fadeinup delay-1">
	<div class="page-content">

		<div class="table-responsive">
			<table>
				<thead>
					<tr>
						<th></th>
						<?php
						$shead = "SELECT * from ".$funct->getValookie('koneksi').".locationgate";
						$qhead = mysqli_query($funct->getConnection(), $shead) or die(mysqli_error($funct->getConnection()));
						while($dhead = mysqli_fetch_array($qhead)) { ?>
							<th class="center"><?php echo $dhead['Note']; ?></th>
							<?php
						}
						?>
					</tr>
				</thead>

				<tbody>
					<tr style="border-bottom: 1px solid #000;">
						<th>Loop1</th>
						<?php
						$sh1 = "SELECT LocationGateID from ".$funct->getValookie('koneksi').".locationgate";
						$qh1 = mysqli_query($funct->getConnection(), $sh1) or die(mysqli_error($funct->getConnection()));
						while($dh1 = mysqli_fetch_array($qh1)) {
							$color = '';
							$sb1 = "SELECT loop1 from ".$funct->getValookie('koneksi').".loggate where gateid='".$dh1[0]."'";
							$qb1 = mysqli_query($funct->getConnection(), $sb1) or die(mysqli_error($funct->getConnection()));
							while($db1 = mysqli_fetch_array($qb1)) { 
								if($db1[0] == '1') {
									$color = 'style="color: #0f0; font-size: 45px;"';
								}
								else {
									$color = 'style="color: #f00; font-size: 45px;"';
								}
								?>
								<td>
									<div class="p-10 center">
										<h1 class="ion-ios-circle-filled" <?php echo $color; ?>></h1>
									</div>
								</td>
								<?php
							}

						}
						?>
					</tr>

					<tr style="border-bottom: 1px solid #000;">
						<th>Button</th>
						<?php
						$sh1 = "SELECT LocationGateID from ".$funct->getValookie('koneksi').".locationgate";
						$qh1 = mysqli_query($funct->getConnection(), $sh1) or die(mysqli_error($funct->getConnection()));
						while($dh1 = mysqli_fetch_array($qh1)) {
							$color = '';
							$sb1 = "SELECT button from ".$funct->getValookie('koneksi').".loggate where gateid='".$dh1[0]."'";
							$qb1 = mysqli_query($funct->getConnection(), $sb1) or die(mysqli_error($funct->getConnection()));
							while($db1 = mysqli_fetch_array($qb1)) { 
								if($db1[0] == '1') {
									$color = 'style="color: #0f0; font-size: 45px;"';
								}
								else {
									$color = 'style="color: #f00; font-size: 45px;"';
								}
								?>
								<td>
									<div class="p-10 center">
										<h1 class="ion-ios-circle-filled" <?php echo $color; ?>></h1>
									</div>
								</td>
								<?php
							}

						}
						?>
					</tr>

					<tr style="border-bottom: 1px solid #000;">
						<th>API Request</th>
						<?php
						$sh1 = "SELECT LocationGateID from ".$funct->getValookie('koneksi').".locationgate";
						$qh1 = mysqli_query($funct->getConnection(), $sh1) or die(mysqli_error($funct->getConnection()));
						while($dh1 = mysqli_fetch_array($qh1)) {
							$color = '';
							$sb1 = "SELECT apirequest from ".$funct->getValookie('koneksi').".loggate where gateid='".$dh1[0]."'";
							$qb1 = mysqli_query($funct->getConnection(), $sb1) or die(mysqli_error($funct->getConnection()));
							while($db1 = mysqli_fetch_array($qb1)) { 
								if($db1[0] == '1') {
									$color = 'style="color: #0f0; font-size: 45px;"';
								}
								else {
									$color = 'style="color: #f00; font-size: 45px;"';
								}
								?>
								<td>
									<div class="p-10 center">
										<h1 class="ion-ios-circle-filled" <?php echo $color; ?>></h1>
									</div>
								</td>
								<?php
							}

						}
						?>
					</tr>

					<tr style="border-bottom: 1px solid #000;">
						<th>Reply Request</th>
						<?php
						$sh1 = "SELECT LocationGateID from ".$funct->getValookie('koneksi').".locationgate";
						$qh1 = mysqli_query($funct->getConnection(), $sh1) or die(mysqli_error($funct->getConnection()));
						while($dh1 = mysqli_fetch_array($qh1)) {
							$color = '';
							$sb1 = "SELECT replyrequest from ".$funct->getValookie('koneksi').".loggate where gateid='".$dh1[0]."'";
							$qb1 = mysqli_query($funct->getConnection(), $sb1) or die(mysqli_error($funct->getConnection()));
							while($db1 = mysqli_fetch_array($qb1)) { 
								if($db1[0] == '1') {
									$color = 'style="color: #0f0; font-size: 45px;"';
								}
								else {
									$color = 'style="color: #f00; font-size: 45px;"';
								}
								?>
								<td>
									<div class="p-10 center">
										<h1 class="ion-ios-circle-filled" <?php echo $color; ?>></h1>
									</div>
								</td>
								<?php
							}

						}
						?>
					</tr>

					<tr style="border-bottom: 1px solid #000;">
						<th>Print Ticket</th>
						<?php
						$sh1 = "SELECT LocationGateID from ".$funct->getValookie('koneksi').".locationgate";
						$qh1 = mysqli_query($funct->getConnection(), $sh1) or die(mysqli_error($funct->getConnection()));
						while($dh1 = mysqli_fetch_array($qh1)) {
							$color = '';
							$sb1 = "SELECT printticket from ".$funct->getValookie('koneksi').".loggate where gateid='".$dh1[0]."'";
							$qb1 = mysqli_query($funct->getConnection(), $sb1) or die(mysqli_error($funct->getConnection()));
							while($db1 = mysqli_fetch_array($qb1)) { 
								if($db1[0] == '1') {
									$color = 'style="color: #0f0; font-size: 45px;"';
								}
								else {
									$color = 'style="color: #f00; font-size: 45px;"';
								}
								?>
								<td>
									<div class="p-10 center">
										<h1 class="ion-ios-circle-filled" <?php echo $color; ?>></h1>
									</div>
								</td>
								<?php
							}

						}
						?>
					</tr>

					<tr style="border-bottom: 1px solid #000;">
						<th>Gate Open</th>
						<?php
						$sh1 = "SELECT LocationGateID from ".$funct->getValookie('koneksi').".locationgate";
						$qh1 = mysqli_query($funct->getConnection(), $sh1) or die(mysqli_error($funct->getConnection()));
						while($dh1 = mysqli_fetch_array($qh1)) {
							$color = '';
							$sb1 = "SELECT gateopen from ".$funct->getValookie('koneksi').".loggate where gateid='".$dh1[0]."'";
							$qb1 = mysqli_query($funct->getConnection(), $sb1) or die(mysqli_error($funct->getConnection()));
							while($db1 = mysqli_fetch_array($qb1)) { 
								if($db1[0] == '1') {
									$color = 'style="color: #0f0; font-size: 45px;"';
								}
								else {
									$color = 'style="color: #f00; font-size: 45px;"';
								}
								?>
								<td>
									<div class="p-10 center">
										<h1 class="ion-ios-circle-filled" <?php echo $color; ?>></h1>
									</div>
								</td>
								<?php
							}

						}
						?>
					</tr>

					<tr style="border-bottom: 1px solid #000;">
						<th>Loop2</th>
						<?php
						$sh1 = "SELECT LocationGateID from ".$funct->getValookie('koneksi').".locationgate";
						$qh1 = mysqli_query($funct->getConnection(), $sh1) or die(mysqli_error($funct->getConnection()));
						while($dh1 = mysqli_fetch_array($qh1)) {
							$color = '';
							$sb1 = "SELECT loop2 from ".$funct->getValookie('koneksi').".loggate where gateid='".$dh1[0]."'";
							$qb1 = mysqli_query($funct->getConnection(), $sb1) or die(mysqli_error($funct->getConnection()));
							while($db1 = mysqli_fetch_array($qb1)) { 
								if($db1[0] == '1') {
									$color = 'style="color: #0f0; font-size: 45px;"';
								}
								else {
									$color = 'style="color: #f00; font-size: 45px;"';
								}
								?>
								<td>
									<div class="p-10 center">
										<h1 class="ion-ios-circle-filled" <?php echo $color; ?>></h1>
									</div>
								</td>
								<?php
							}

						}
						?>
					</tr>

					<tr style="border-bottom: 1px solid #000;">
						<th>API Gate In</th>
						<?php
						$color = '';
						$sh1 = "SELECT LocationGateID from ".$funct->getValookie('koneksi').".locationgate";
						$qh1 = mysqli_query($funct->getConnection(), $sh1) or die(mysqli_error($funct->getConnection()));
						while($dh1 = mysqli_fetch_array($qh1)) {

							$sb1 = "SELECT apigatein from ".$funct->getValookie('koneksi').".loggate where gateid='".$dh1[0]."'";
							$qb1 = mysqli_query($funct->getConnection(), $sb1) or die(mysqli_error($funct->getConnection()));
							while($db1 = mysqli_fetch_array($qb1)) { 
								if($db1[0] == '1') {
									$color = 'style="color: #0f0; font-size: 45px;"';
								}
								else {
									$color = 'style="color: #f00; font-size: 45px;"';
								}
								?>
								<td>
									<div class="p-10 center">
										<h1 class="ion-ios-circle-filled" <?php echo $color; ?>></h1>
									</div>
								</td>
								<?php
							}

						}
						?>
					</tr>
				</tbody>
				
			</table>
		</div>

	</div>
</div>

<script type="text/javascript">
	window.setTimeout(function () {
		window.location.reload();
	}, (1 * 60 * 1000) );
</script>