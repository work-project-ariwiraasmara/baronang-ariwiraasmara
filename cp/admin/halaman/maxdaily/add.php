<div id="toolbar" class="primary-color">
    <a href="?pg=maxdaily&sb=index" class="open-left">
        <i class="ion-android-arrow-back"></i>
    </a>
    <span class="title bold txt-white" id="title"></span>
</div>

<div class="animated fadeinup delay-1">
	<div class="page-content">

		<div class="form-inputs">
			<form action="" method="post">
				<div class="input-field">
					<span class="bold">Day</span>
					<select class="browser-default" name="day">
						<?php
						$dy = array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Public Holiday");
						for($d=0; $d<count($dy); $d++) { ?>
							<option value="<?php echo $dy[$d]; ?>"><?php echo $dy[$d]; ?></option>
							<?php
						}
						?>
					</select>
				</div>

				<div class="input-field">
					<input type="number" name="price" id="price" class="validate">
					<label for="price">Max Price</label>
				</div>

				<div class="input-field">
					<span class="bold">Vehicle</span>
					<select class="browser-default" name="vhl">
						<?php
						$s1 = "SELECT Code, Name from ina".$funct->getValookie('cmpid')."001.vehicletype";
						$q1 = mysqli_query($funct->getConnection(), $s1) or die(mysqli_error($funct->getConnection()));
						while($d1 = mysqli_fetch_array($q1)) { ?>
							<option value="<?php echo $d1['Code']; ?>"><?php echo $d1['Name']; ?></option>
							<?php
						}
						?>
					</select>
				</div>

				<button type="submit" name="btnok" class="btn btn-large width-100 waves-effect waves-large primary-color borad-30 m-t-30">
					OK
				</button>
			</form>

			<?php
			if(isset($_POST['btnok'])) {
				$day 	= @$_POST['day'];
				$price 	= @$_POST['price'];
				$vhl 	= @$_POST['vhl'];

				$s2 = "SELECT MaxID from ina".$funct->getValookie('cmpid')."001.pricingdailymax order by MaxID DESC limit 1";
				$q2 = mysqli_query($funct->getConnection(), $s2) or die(mysqli_error($funct->getConnection()));
				$d2 = mysqli_fetch_array($q2);

				$graid = substr($d2['MaxID'], -3);
                $gp = (int)$graid + 1;
                $id = 'max'.str_pad($gp, 3, '0', STR_PAD_LEFT);

				$s3 = "INSERT into ina".$funct->getValookie('cmpid')."001.pricingdailymax values('$id', '$day', '$price', '$vhl')";
				$q3 = mysqli_query($funct->getConnection(), $s3) or die(mysqli_error($funct->getConnection()));
				if($q3) { ?>
					<script type="text/javascript">
						alert('Tambah Data Max Daily Berhasil!');
						window.location.href = "?pg=maxdaily&sb=index";
					</script>
					<?php
				}
			}
			?>

		</div>

	</div>
</div>