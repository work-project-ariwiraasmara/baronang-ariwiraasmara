<?php
$sp1 = "SELECT * from ".$funct->getValookie('koneksi').".pricingdailymax where MaxID='".$funct->getIDParam('id')."'";
$qp1 = mysqli_query($funct->getConnection(), $sp1) or die(mysqli_error($funct->getConnection()));
$dp1 = mysqli_fetch_array($qp1);
?>

<div id="toolbar" class="primary-color">
    <a href="?pg=maxdaily&sb=index" class="open-left">
        <i class="ion-android-arrow-back"></i>
    </a>
    <span class="title bold txt-white" id="title"></span>
</div>

<div class="animated fadeinup delay-1">
	<div class="page-content">

		<div class="form-inputs">
			<form action="" method="post">
				<div class="input-field">
					<span class="bold">Day</span>
					<select class="browser-default" name="day">
						<?php
						$dy = array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Public Holiday");
						for($d=0; $d<count($dy); $d++) { ?>
							<option value="<?php echo $dy[$d]; ?>" <?php if($dy[$d] == $dp1['DayName']) { echo 'selected'; } ?> ><?php echo $dy[$d]; ?></option>
							<?php
						}
						?>
					</select>
				</div>

				<div class="input-field">
					<input type="number" name="price" id="price" class="validate" value="<?php echo $dp1['MaxPrice']; ?>">
					<label for="price">Max Price</label>
				</div>

				<div class="input-field">
					<span class="bold">Vehicle</span>
					<select class="browser-default" name="vhl">
						<?php
						$s1 = "SELECT Code, Name from ".$funct->getValookie('koneksi').".vehicletype";
						$q1 = mysqli_query($funct->getConnection(), $s1) or die(mysqli_error($funct->getConnection()));
						while($d1 = mysqli_fetch_array($q1)) { ?>
							<option value="<?php echo $d1['Code']; ?>" <?php if($d1['Code'] == $dp1['VehicleID']) { echo 'selected'; } ?> ><?php echo $d1['Name']; ?></option>
							<?php
						}
						?>
					</select>
				</div>

				<button type="submit" name="btnok" class="btn btn-large width-100 waves-effect waves-large primary-color borad-30 m-t-30">
					OK
				</button>
			</form>

			<?php
			if(isset($_POST['btnok'])) {
				$day 	= @$_POST['day'];
				$price 	= @$_POST['price'];
				$vhl 	= @$_POST['vhl'];

				$s3 = "UPDATE ".$funct->getValookie('koneksi').".pricingdailymax set DayName='$day', MaxPrice='$price', VehicleID='$vhl' where MaxID='".$funct->getIDParam('id')."'";
				$q3 = mysqli_query($funct->getConnection(), $s3) or die(mysqli_error($funct->getConnection()));
				if($q3) { ?>
					<script type="text/javascript">
						alert('Update Data Max Daily Berhasil!');
						window.location.href = "?pg=maxdaily&sb=index";
					</script>
					<?php
				}
			}
			?>

		</div>

	</div>
</div>