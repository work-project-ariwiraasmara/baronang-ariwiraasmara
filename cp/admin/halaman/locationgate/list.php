<div id="toolbar" class="primary-color">
    <div class="open-left" id="open-left" data-activates="slide-out-left">
        <i class="ion-android-menu"></i>
    </div>
    <span class="title bold txt-white" id="title"></span>
    <div class="open-right">
        <a href="?pg=maxdaily&sb=add">
        	<i class="ion-android-add txt-white"></i>
        </a>
    </div>
</div>

<div class="animated fadeinup delay-1">
	<div class="page-content">
		<div class="">
			<table class="table">
				<thead>
					<tr>
						<th>Max ID</th>
						<th>Day Name</th>
						<th colspan="2" style="text-align: center;">Max Price</th>
						<th>Vehicle ID</th>
						<th colspan="2"></th>
					</tr>
				</thead>

				<tbody>
					<?php
					$page = isset($_GET['hlm']) ? intval($_GET['hlm']) : 1; // untuk nomor halaman
					$adjacents = isset($_GET['adjacents']) ? intval($_GET['adjacents']) : 0; // khusus style pagination 2 dan 3
					$rpp = 10; // jumlah record per halaman

					$sql = "SELECT * from ".$funct->getValookie('koneksi').".pricingdailymax";
					$qry = mysqli_query($funct->getConnection(), $sql) or die(mysqli_error($funct->getConnection()));

					$tcount = mysqli_num_rows($qry); // jumlah total baris
					$tpages = isset($tcount) ? ceil($tcount / $rpp) : 1; // jumlah total halaman
					$count = 0; // untuk paginasi
					$i = ($page - 1) * $rpp; // batas paginasi
					$no_urut = ($page - 1) * $rpp; // nomor urut
					$reload = "?pg=maxdaily&sb=index&amp;adjacents=" . $adjacents; // untuk link ke halaman lain
					
					//while($data = mysqli_fetch_array($qry)) { 
					while ( ($count < $rpp) && ($i < $tcount) ) { 
						mysqli_data_seek($qry, $i);
						$data = mysqli_fetch_array($qry);
						?>
						<tr>
							<td><?php echo $data['MaxID']; ?></td>
							<td><?php echo $data['DayName']; ?></td>
							<td>Rp.</td>
							<td style="text-align: right;"><?php echo $funct->FormatRupiah($data['MaxPrice']); ?></td>
							<td><?php echo $data['VehicleID']; ?></td>
							<td>
								<a href="<?php echo '?pg=maxdaily&sb=edit&'.$funct->setIDParam('id', $data['MaxID']); ?>" class="waves-effect waves-light"><i class="ion-android-create"></i></a>
							</td>
							<td>
								<a href="#" id="<?php echo 'delete'.$data['MaxID']; ?>" class="waves-effect waves-light"><i class="ion-android-delete"></i></a>
							</td>

							<script type="text/javascript">
								$('<?php echo '#delete'.$data['MaxID']; ?>').click(function(){
									confirmDelete('pricingdailymax', 'MaxID', '<?php echo $data['MaxID'] ?>', '?pg=maxdaily&sb=index');
								});
							</script>
						</tr>
						<?php
						$i++;
                    	$count++;
					}
					?>
				</tbody>
			</table>

			<div class="m-t-30">
				<?php echo $funct->paginate_one($reload, $page, $tpages, $adjacents); ?>
			</div>

		</div>
	</div>
</div>