<div id="toolbar" class="primary-color">
    <a href="?pg=locationgate&sb=index" class="open-left">
        <i class="ion-android-arrow-back"></i>
    </a>
    <span class="title bold txt-white" id="title"></span>
</div>

<div class="animated fadeinup delay-1">
	<div class="page-content">

		<div class="form-inputs">
			<form action="" method="post">
				<div class="input-field">
					<input type="text" name="gateid" id="gateid" class="validate">
					<label for="gateid">gate id</label>
				</div>

				<div class="input-field">
					<input type="text" name="code" id="code" class="validate">
					<label for="code">Code</label>
				</div>

				<div class="input-field">
					<input type="number" name="balance" id="balance" class="validate">
					<label for="balance">Minimum Balance</label>
				</div>

				<div class="input-field">
					<input type="text" name="note" id="note" class="validate">
					<label for="note">Note</label>
				</div>

				<button type="submit" name="btnok" class="btn btn-large width-100 waves-effect waves-large primary-color borad-30 m-t-30">
					OK
				</button>
			</form>

			<?php
			if(isset($_POST['btnok'])) {
				$gateid = @$_POST['gateid'];
				$locid 	= @$_POST['locid'];
				$code 	= @$_POST['code'];
				$bal 	= @$_POST['balance'];
				$note 	= @$_POST['note'];

				$s1 = "SELECT VehicleID from ".$funct->getValookie('koneksi').".vehicletype order by VehicleID DESC limit 1";
				$q1 = mysqli_query($funct->getConnection(), $s1) or die(mysqli_error($funct->getConnection()));
				$d1 = mysqli_fetch_array($q1);

				$graid = substr($d1['VehicleID'], -3);
                $gp = (int)$graid + 1;
                $id = 'VHC'.str_pad($gp, 3, '0', STR_PAD_LEFT);

				$sp2 = "INSERT into ".$funct->getValookie('koneksi').".vehicletype values('$gateid', '$locid', '1', '$code', '$bal' ,'$note')";
				$qp2 = mysqli_query($funct->getConnection(), $sp2) or die(mysqli_error($funct->getConnection()));
				if($qp2) { ?>
					<script type="text/javascript">
						alert('Tambah Data Location Gate Berhasil!');
						window.location.href = "?pg=locationgate&sb=index";
					</script>
					<?php
				}
			}
			?>

		</div>

	</div>
</div>