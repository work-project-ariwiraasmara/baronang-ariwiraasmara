<?php
if(isset($_GET['b'])) { $b = $funct->Denval(@$_GET['b']); } else { $b = date('m'); }
if(isset($_GET['y'])) { $y = $funct->Denval(@$_GET['y']); } else { $y = date('Y'); }
?>

<div id="toolbar" class="primary-color">
	<?php
    if(isset($_GET['detail'])) { ?>
    	<div class="open-left">
    		<a class="modal-trigger" href="?pg=member">
	        	<i class="ion-android-arrow-back txt-white"></i>
	        </a>
	    </div>
    	<?php
    }
    else { ?>
    	<div class="open-left" id="open-left" data-activates="slide-out-left">
	        <i class="ion-android-menu"></i>
	    </div>
    	<?php
    }
    ?>
    
    <span class="title bold txt-white" id="title"></span>

    <div class="open-right">
    	<?php
    	if(isset($_GET['detail'])) {
    		if(isset($_GET['type'])) { ?>
    			<a href="<?php echo 'index_download_excel.php?pg=member&download=1&detail='.@$_GET['detail'].'&vhl='.@$_GET['vhl'].'&b='.$funct->Enval($b).'&y='.$funct->Enval($y).'&d='.@$_GET['d'].'&type='.@$_GET['type']; ?>" id="download">
    				<i class="ion-android-download txt-white"></i>
    			</a>
    			<?php
    		}
    		else { ?>
    			<a href="<?php echo 'index_download_excel.php?pg=member&download=1&detail='.@$_GET['detail'].'&vhl='.@$_GET['vhl'].'&b='.$funct->Enval($b).'&y='.$funct->Enval($y); ?>" id="download">
    				<i class="ion-android-download txt-white"></i>
    			</a>
    			<?php
    		}
    	}
    	else { ?>
    		<a href="<?php echo 'index_download_excel.php?pg=member&download=1&b='.$funct->Enval($b).'&y='.$funct->Enval($y); ?>" id="download">
    			<i class="ion-android-download txt-white"></i>
    		</a>
    		<?php
    	}
    	?>
    </div>

    <?php
    if(!isset($_GET['detail'])) { ?>
    	<div class="open-right">
	        <a class="modal-trigger" href="#cari">
	        	<i class="ion-android-search txt-white"></i>
	        </a>
	    </div>
    	<?php
    }
    ?>
</div>

<div class="animated fadeinup delay-1">
	<div class="page-content">
		<div class="m-t-0">

			<?php
			$total = 0;

			if(isset($_GET['detail'])) { ?>
				<h1 class="uppercase bold italic"><?php echo 'Detail Kendaraan '.$funct->Denval(@$_GET['detail']); ?></h1>
				<?php
				if(isset($_GET['type'])) { ?>
					<h3 class="uppercase bold"><?php echo date('d F Y', strtotime($y.'-'.$b.'-'.$funct->Denval(@$_GET['d']))); ?></h3>
					<?php
				}
				else { ?>
					<h3 class="uppercase bold"><?php echo date('F Y', strtotime($y.'-'.$b.'-1')); ?></h3>
					<?php
				}
			}
			else { 
				if(isset($_GET['b']) && isset($_GET['y'])) { ?>
					<h1 class="uppercase bold"><?php echo date('F Y', strtotime($y.'-'.$b.'-1')); ?></h1>
					<?php
				}
				else { ?>
					<h1 class="uppercase bold"><?php echo date('F Y'); ?></h1>
					<?php
				}
			}
			?>

			<div class="table-responsive m-t-30">
				<table class="table">
					<thead>
						<tr>
							<?php
							if(isset($_GET['type'])) { ?>
								<th>No.</th>
								<?php
							}
							?>

							<th>
								<?php
								if(isset($_GET['detail'])) { 
									if(isset($_GET['type'])) {
										echo 'CardNo'; 
									}
									else {
										echo 'Tanggal';
									}
								}
								else { echo 'Kendaraan'; }
								?>
							</th>

							<?php
							if(isset($_GET['type'])) { 
								if($funct->Denval(@$_GET['type']) == 'time in') { $th = 'Jam Masuk'; }
								else { $th = 'Jam Keluar'; }
								?>
								<th class="center"><?php echo $th; ?></th>
								<?php
							}
							else { ?>
								<th class="center">Total In</th>
								<th class="center">Total Out</th>
								<?php
							}
							?>

							<th colspan="2" class="center">Total</th>
						</tr>
					</thead>

					<tbody>
						<?php
						if(isset($_GET['detail'])) {
							if(isset($_GET['type'])) {
								$no = 1;
								$dt = date('Y-m-d', strtotime($y.'-'.$b.'-'.$funct->Denval(@$_GET['d'])));

								if($funct->Denval(@$_GET['type']) == 'time in') {
									$daty = "TimeIn"; 

									$sql = "SELECT TransID, CardNo, TimeIn from ".$funct->getValookie('koneksi').".parkinglist where VehicleType='".$funct->Denval(@$_GET['vhl'])."' and isMember = '1' and TimeIn between '$dt 00:00:00' and '$dt 23:59:59'";
									/*
									$sql = "SELECT 	a.CardNo as CardNo, 
													a.TimeIn as TimeIn, 
													b.PaidAmount as TotalPaid
                            				from ".$funct->getValookie('koneksi').".parkinglist as a 
                            				inner join ".$funct->getValookie('koneksi').".parkinglistpayment as b 
                                				on a.TransID = b.TransID 
                            				where 	a.isMember='1' 
                            						and a.VehicleType = '".$funct->Denval(@$_GET['vhl'])."' 
                            						and a.TimeIn between '$dt 00:00:00' and '$dt 23:59:59'";
									*/
								}
								else { 
									$daty = "TimeOut"; 

									$sql = "SELECT TransID, CardNo, TimeOut from ".$funct->getValookie('koneksi').".parkinglist where VehicleType='".$funct->Denval(@$_GET['vhl'])."' and isMember = '1' and TimeOut between '$dt 00:00:00' and '$dt 23:59:59'";
									/*
									$sql = "SELECT 	a.CardNo as CardNo, 
													a.TimeOut as TimeOut, 
													b.PaidAmount as TotalPaid
                            				from ".$funct->getValookie('koneksi').".parkinglist as a 
                            				inner join ".$funct->getValookie('koneksi').".parkinglistpayment as b 
                                				on a.TransID = b.TransID 
                            				where 	a.isMember='1' 
                            						and a.VehicleType = '".$funct->Denval(@$_GET['vhl'])."' 
                            						and a.TimeOut between '$dt 00:00:00' and '$dt 23:59:59'";
									*/
								}

								//echo $sql;
								$qry = mysqli_query($funct->getConnection(), $sql) or die(mysqli_error($funct->getConnection()));
								while($data = mysqli_fetch_array($qry)) { 

									// $sql2 = "SELECT Sum(PaidAmount) as TotalPaid from ".$funct->getValookie('koneksi').".parkinglistpayment where TransID='".$data['TransID']."' and PaidDate between '$dt 00:00:00' and '$dt 23:59:59'";
									// $qry2 = mysqli_query($funct->getConnection(), $sql2) or die(mysqli_error($funct->getConnection()));
									// $data2 = mysqli_fetch_array($qry2);
									?>
									<tr>
										<td><?php echo $no; ?></td>
										<td><?php echo $data['CardNo']; ?></td>
										<td class="center"><?php echo date('H:i:s', strtotime($data[$daty])); ?></td>
										<td>Rp.</td>
										<td style="text-align: right;"><?php echo $funct->FormatRupiah('0'); //$funct->FormatRupiah($data2['TotalPaid']); ?></td>
									</tr>
									<?php
									$no++;
									$total = 0;
									//$total = $total + $data2['TotalPaid']; 
								}
							}
							else {
								$t = date('t', strtotime($y.'-'.$b.'-1'));
								//echo $t.'<br><br>';
								for($d = 1; $d <= $t; $d++) {
									$dt = date('Y-m-d', strtotime($y.'-'.$b.'-'.$d));

									/*
									$sql = "SELECT distinct (SELECT Count(TimeIn)  from ".$funct->getValookie('koneksi').".parkinglist where isMember='1' and VehicleType='".$funct->Denval(@$_GET['vhl'])."' and TimeIn  between '$dt 00:00:00' and '$dt 23:59:59') as TimeIn,
															(SELECT COUNT(TimeOut) from ".$funct->getValookie('koneksi').".parkinglist where isMember='1' and VehicleType='".$funct->Denval(@$_GET['vhl'])."' and TimeOut between '$dt 00:00:00' and '$dt 23:59:59') as TimeOut,
															(SELECT SUM(TotalPaid) from ".$funct->getValookie('koneksi').".parkinglist where isMember='1' and VehicleType='".$funct->Denval(@$_GET['vhl'])."' and TimeIn  between '$dt 00:00:00' and '$dt 23:59:59') as Total
											from ".$funct->getValookie('koneksi').".parkinglist";
									
									$sql = "SELECT Count(a.TimeIn) as TimeIn, Count(a.TimeOut) as TimeOut, Sum(b.PaidAmount) as Total
                            				from ".$funct->getValookie('koneksi').".parkinglist as a 
                            				inner join ".$funct->getValookie('koneksi').".parkinglistpayment as b 
                                				on a.TransID = b.TransID 
                            				where a.isMember='1' and a.VehicleType = '".$funct->Denval(@$_GET['vhl'])."' and b.PaidDate between '$dt 00:00:00' and '$dt 23:59:59'";
									*/
                            		$sql = "SELECT * from (
												(select count(TimeIn) as TimeIn    from ".$funct->getValookie('koneksi').".parkinglist where VehicleType='".$funct->Denval(@$_GET['vhl'])."' and isMember = '1' and TimeIn  between '$dt 00:00:00' and '$dt 23:59:59') as TimeIn, 
												(select count(TimeOut) as TimeOut  from ".$funct->getValookie('koneksi').".parkinglist where VehicleType='".$funct->Denval(@$_GET['vhl'])."' and isMember = '1' and TimeOut between '$dt 00:00:00' and '$dt 23:59:59') as TimeOut,
												(select sum(b.PaidAmount) as Total from ".$funct->getValookie('koneksi').".parkinglist as a 
                                    	 		 inner join ".$funct->getValookie('koneksi').".parkinglistpayment as b 
                                        			on a.TransID = b.TransID 
										 		 where a.isMember = '1' and a.VehicleType='".$funct->Denval(@$_GET['vhl'])."' and PaidDate between '$dt 00:00:00' and '$dt 23:59:59') as Total
											)";
									//echo $d.'). '.$sql.'<br><br>';
									$qry = mysqli_query($funct->getConnection(), $sql) or die(mysqli_error($funct->getConnection()));
									while($data = mysqli_fetch_array($qry)) { ?>
										<tr>
											<?php 
											/*
											<td <?php if( $data['Total'] != 0 || !empty($data['Total']) || !is_null($data['Total']) ) echo 'class="txt-link"'; ?> >
												<?php
												if( $data['Total'] == 0 || empty($data['Total']) || is_null($data['Total']) ) { 
													 echo $d;
												}
												else { ?>
													<a href="<?php echo '?pg=member&detail='.@$_GET['detail'].'&vhl='.@$_GET['vhl'].'&b='.@$_GET['b'].'&y='.@$_GET['y'].'&d='.$funct->Enval($d); ?>">
														<?php echo $d; ?>
													</a>
													<?php
												}
												?>
											</td>
											*/ 
											?>
											<td><?php echo $d; ?></td>

											<?php
											if($data['TimeIn'] == 0) { ?>
												<td class="txt-link" style="text-align: right;">
													<a href="#">
														<?php echo $funct->FormatNumber($data['TimeIn']); ?>	
													</a>
												</td>
												<?php
											}
											else { ?>
												<td class="txt-link" style="text-align: right;">
													<a href="<?php echo '?pg=member&detail='.@$_GET['detail'].'&vhl='.@$_GET['vhl'].'&b='.$funct->Enval($b).'&y='.$funct->Enval($y).'&d='.$funct->Enval($d).'&type='.$funct->Enval('time in'); ?>">
														<?php echo $funct->FormatNumber($data['TimeIn']); ?>	
													</a>
												</td>
												<?php
											}

											if($data['TimeOut'] == 0) { ?>
												<td class="txt-link" style="text-align: right;">
													<a href="#">
														<?php echo $funct->FormatNumber($data['TimeOut']); ?>
													</a>
												</td>
												<?php
											}
											else { ?>
												<td class="txt-link" style="text-align: right;">
													<a href="<?php echo '?pg=member&detail='.@$_GET['detail'].'&vhl='.@$_GET['vhl'].'&b='.$funct->Enval($b).'&y='.$funct->Enval($y).'&d='.$funct->Enval($d).'&type='.$funct->Enval('time out'); ?>">
														<?php echo $funct->FormatNumber($data['TimeOut']); ?>
													</a>
												</td>
												<?php
											}
											?>

											
											
											<td>Rp.</td>
											<td style="text-align: right;"><?php echo $funct->FormatRupiah(0); ?></td>
										</tr>
										<?php
										$total = 0;
										//$total = $total + $data['Total']; 
									}
								}
							}
						}
						else {
							/*
							$sql = "SELECT distinct	TransID,
													(select Count(TimeIn)  from ".$funct->getValookie('koneksi').".parkinglist where isMember='1' and VehicleType='C' and Month(TimeIn)  = '$b' and Year(TimeIn)  = '$y') as TimeIn_Motor,
													(select Count(TimeOut) from ".$funct->getValookie('koneksi').".parkinglist where isMember='1' and VehicleType='C' and Month(TimeOut) = '$b' and Year(TimeOut) = '$y') as TimeOut_Motor,
													(select Sum(TotalPaid) from ".$funct->getValookie('koneksi').".parkinglist where isMember='1' and VehicleType='C' and Month(TimeOut)  = '$b' and Year(TimeOut)  = '$y') as Total_Motor,

													(select Count(TimeIn)  from ".$funct->getValookie('koneksi').".parkinglist where isMember='1' and VehicleType='B' and Month(TimeIn)  = '$b' and Year(TimeIn)  = '$y') as TimeIn_Mobil,
													(select Count(TimeOut) from ".$funct->getValookie('koneksi').".parkinglist where isMember='1' and VehicleType='B' and Month(TimeOut) = '$b' and Year(TimeOut) = '$y') as TimeOut_Mobil,
													(select Sum(TotalPaid) from ".$funct->getValookie('koneksi').".parkinglist where isMember='1' and VehicleType='B' and Month(TimeIn)  = '$b' and Year(TimeIn)  = '$y') as Total_Mobil,

													(select Count(TimeIn)  from ".$funct->getValookie('koneksi').".parkinglist where isMember='1' and VehicleType='T' and Month(TimeIn)  = '$b' and Year(TimeIn)  = '$y') as TimeIn_Truck,
													(select Count(TimeOut) from ".$funct->getValookie('koneksi').".parkinglist where isMember='1' and VehicleType='T' and Month(TimeOut) = '$b' and Year(TimeOut) = '$y') as TimeOut_Truck,
													(select Sum(TotalPaid) from ".$funct->getValookie('koneksi').".parkinglist where isMember='1' and VehicleType='T' and Month(TimeIn)  = '$b' and Year(TimeIn)  = '$y') as Total_Truck,

													(select Count(TimeIn)  from ".$funct->getValookie('koneksi').".parkinglist where isMember='1' and VehicleType='V' and Month(TimeIn)  = '$b' and Year(TimeIn)  = '$y') as TimeIn_VIPMobil,
													(select Count(TimeOut) from ".$funct->getValookie('koneksi').".parkinglist where isMember='1' and VehicleType='V' and Month(TimeOut) = '$b' and Year(TimeOut) = '$y') as TimeOut_VIPMobil,
													(select Sum(TotalPaid) from ".$funct->getValookie('koneksi').".parkinglist where isMember='1' and VehicleType='V' and Month(TimeIn)  = '$b' and Year(TimeIn)  = '$y') as Total_VIPMobil,

													(select Count(TimeIn)  from ".$funct->getValookie('koneksi').".parkinglist where isMember='1' and VehicleType='P' and Month(TimeIn)  = '$b' and Year(TimeIn)  = '$y') as TimeIn_Valet,
													(select Count(TimeOut) from ".$funct->getValookie('koneksi').".parkinglist where isMember='1' and VehicleType='P' and Month(TimeOut) = '$b' and Year(TimeOut) = '$y') as TimeOut_Valet,
													(select Sum(TotalPaid) from ".$funct->getValookie('koneksi').".parkinglist where isMember='1' and VehicleType='P' and Month(TimeIn)  = '$b' and Year(TimeIn)  = '$y') as Total_Valet,

													(select Count(TimeIn)  from ".$funct->getValookie('koneksi').".parkinglist where isMember='1' and VehicleType='Z' and Month(TimeIn)  = '$b' and Year(TimeIn)  = '$y') as TimeIn_VIPMotor,
													(select Count(TimeOut) from ".$funct->getValookie('koneksi').".parkinglist where isMember='1' and VehicleType='Z' and Month(TimeOut) = '$b' and Year(TimeOut) = '$y') as TimeOut_VIPMotor,
													(select Sum(TotalPaid) from ".$funct->getValookie('koneksi').".parkinglist where isMember='1' and VehicleType='Z' and Month(TimeIn)  = '$b' and Year(TimeIn)  = '$y') as Total_VIPMotor
									from ".$funct->getValookie('koneksi').".parkinglist where isMember='1' and Month(TimeIn) = '$b' and Year(TimeIn) = '$y' order by TransID DESC limit 1";
							//echo $sql;
							*/

							//$smotor = "SELECT Count(a.TimeIn) as TimeIn, Count(a.TimeOut) as TimeOut, Sum(b.PaidAmount) as Total
                            //			from ".$funct->getValookie('koneksi').".parkinglist as a 
                            //			inner join ".$funct->getValookie('koneksi').".parkinglistpayment as b 
                            //    			on a.TransID = b.TransID 
                            //			where a.isMember='1' and a.VehicleType='C' and Month(b.PaidDate)='$b' and Year(b.PaidDate)='$y' order by b.PaidDate ASC";
							
							$smotor = 	"SELECT * from (
											(select count(TimeIn) as TimeIn    from ".$funct->getValookie('koneksi').".parkinglist where VehicleType = 'C' and isMember = '1' and Month(TimeIn)='$b' and Year(TimeIn)='$y') as TimeIn, 
											(select count(TimeOut) as TimeOut  from ".$funct->getValookie('koneksi').".parkinglist where VehicleType = 'C' and isMember = '1' and Month(TimeIn)='$b' and Year(TimeIn)='$y') as TimeOut,
											(select sum(b.PaidAmount) as Total from ".$funct->getValookie('koneksi').".parkinglist as a 
                                    	 		inner join ".$funct->getValookie('koneksi').".parkinglistpayment as b 
                                        			on a.TransID = b.TransID 
										 	 where a.isMember= '1' and a.VehicleType = 'C' and Month(b.PaidDate)='$b' and Year(b.PaidDate)='$y') as Total
										)";
							//echo $smotor;
							$qmotor = mysqli_query($funct->getConnection(), $smotor) or die(mysqli_error($funct->getConnection()));
							$motor = mysqli_fetch_array($qmotor);


							//$smobil = "SELECT Count(a.TimeIn) as TimeIn, Count(a.TimeOut) as TimeOut, Sum(b.PaidAmount) as Total
                            //			from ".$funct->getValookie('koneksi').".parkinglist as a 
                            //			inner join ".$funct->getValookie('koneksi').".parkinglistpayment as b 
                            //    			on a.TransID = b.TransID 
                            //			where a.isMember='1' and a.VehicleType = 'B' and Month(b.PaidDate)='$b' and Year(b.PaidDate)='$y' order by b.PaidDate ASC";
							
							$smobil = 	"SELECT * from (
											(select count(TimeIn)  as TimeIn   from ".$funct->getValookie('koneksi').".parkinglist where VehicleType = 'B' and isMember = '1' and Month(TimeIn)='$b' and Year(TimeIn)='$y') as TimeIn, 
											(select count(TimeOut) as TimeOut  from ".$funct->getValookie('koneksi').".parkinglist where VehicleType = 'B' and isMember = '1' and Month(TimeIn)='$b' and Year(TimeIn)='$y') as TimeOut,
											(select sum(b.PaidAmount) as Total from ".$funct->getValookie('koneksi').".parkinglist as a 
                                    	 		inner join ".$funct->getValookie('koneksi').".parkinglistpayment as b 
                                        			on a.TransID = b.TransID 
										 	 where a.isMember= '1' and a.VehicleType = 'B' and Month(b.PaidDate)='$b' and Year(b.PaidDate)='$y') as Total
										)";

							$qmobil = mysqli_query($funct->getConnection(), $smobil) or die(mysqli_error($funct->getConnection()));
							$mobil = mysqli_fetch_array($qmobil);

							//$struk = "SELECT Count(a.TimeIn) as TimeIn, Count(a.TimeOut) as TimeOut, Sum(b.PaidAmount) as Total
                            //			from ".$funct->getValookie('koneksi').".parkinglist as a 
                            //			inner join ".$funct->getValookie('koneksi').".parkinglistpayment as b 
                            //    			on a.TransID = b.TransID 
                            //			where a.isMember='1' and a.VehicleType = 'T' and Month(b.PaidDate)='$b' and Year(b.PaidDate)='$y' order by b.PaidDate ASC";
							//echo $sql;

							$struk = 	"SELECT * from (
											(select count(TimeIn) as TimeIn    from ".$funct->getValookie('koneksi').".parkinglist where VehicleType = 'T' and isMember = '1' and Month(TimeIn)='$b' and Year(TimeIn)='$y') as TimeIn, 
											(select count(TimeOut) as TimeOut  from ".$funct->getValookie('koneksi').".parkinglist where VehicleType = 'T' and isMember = '1' and Month(TimeIn)='$b' and Year(TimeIn)='$y') as TimeOut,
											(select sum(b.PaidAmount) as Total from ".$funct->getValookie('koneksi').".parkinglist as a 
                                    	 		inner join ".$funct->getValookie('koneksi').".parkinglistpayment as b 
                                        			on a.TransID = b.TransID 
										 	 where a.isMember= '1' and a.VehicleType = 'T' and Month(b.PaidDate)='$b' and Year(b.PaidDate)='$y') as Total
										)";

							$qtruk = mysqli_query($funct->getConnection(), $struk) or die(mysqli_error($funct->getConnection()));
							$truk = mysqli_fetch_array($qtruk);

							//$svalet = "SELECT Count(a.TimeIn) as TimeIn, Count(a.TimeOut) as TimeOut, Sum(b.PaidAmount) as Total
                            //			from ".$funct->getValookie('koneksi').".parkinglist as a 
                            //			inner join ".$funct->getValookie('koneksi').".parkinglistpayment as b 
                            //    			on a.TransID = b.TransID 
                            //			where a.isMember='1' and a.VehicleType = 'P' and Month(b.PaidDate)='$b' and Year(b.PaidDate)='$y' order by b.PaidDate ASC";
							//echo $sql;

							$svalet = 	"SELECT * from (
											(select count(TimeIn) as TimeIn    from ".$funct->getValookie('koneksi').".parkinglist where VehicleType = 'P' and isMember = '1' and Month(TimeIn)='$b' and Year(TimeIn)='$y') as TimeIn, 
											(select count(TimeOut) as TimeOut  from ".$funct->getValookie('koneksi').".parkinglist where VehicleType = 'P' and isMember = '1' and Month(TimeIn)='$b' and Year(TimeIn)='$y') as TimeOut,
											(select sum(b.PaidAmount) as Total from ".$funct->getValookie('koneksi').".parkinglist as a 
                                    			inner join ".$funct->getValookie('koneksi').".parkinglistpayment as b 
                                        			on a.TransID = b.TransID 
										 	 where a.isMember= '1' and a.VehicleType = 'P' and Month(b.PaidDate)='$b' and Year(b.PaidDate)='$y') as Total
										)";

							$qvalet = mysqli_query($funct->getConnection(), $svalet) or die(mysqli_error($funct->getConnection()));
							$valet = mysqli_fetch_array($qvalet);

							//$svipmotor = "SELECT Count(a.TimeIn) as TimeIn, Count(a.TimeOut) as TimeOut, Sum(b.PaidAmount) as Total
                            //				from ".$funct->getValookie('koneksi').".parkinglist as a 
                            //				inner join ".$funct->getValookie('koneksi').".parkinglistpayment as b 
                            //    				on a.TransID = b.TransID 
                            //				where a.isMember='1' and a.VehicleType = 'Z' and Month(b.PaidDate)='$b' and Year(b.PaidDate)='$y' order by b.PaidDate ASC";
							//echo $sql;

							$svipmotor = "SELECT * from (
											(select count(TimeIn) as TimeIn    from ".$funct->getValookie('koneksi').".parkinglist where VehicleType = 'Z' and isMember = '1' and Month(TimeIn)='$b' and Year(TimeIn)='$y') as TimeIn, 
											(select count(TimeOut) as TimeOut  from ".$funct->getValookie('koneksi').".parkinglist where VehicleType = 'Z' and isMember = '1' and Month(TimeIn)='$b' and Year(TimeIn)='$y') as TimeOut,
											(select sum(b.PaidAmount) as Total from ".$funct->getValookie('koneksi').".parkinglist as a 
                                    	 		inner join ".$funct->getValookie('koneksi').".parkinglistpayment as b 
                                        			on a.TransID = b.TransID 
										 	where a.isMember= '1' and a.VehicleType = 'Z' and Month(b.PaidDate)='$b' and Year(b.PaidDate)='$y') as Total
										)";

							$qvipmotor = mysqli_query($funct->getConnection(), $svipmotor) or die(mysqli_error($funct->getConnection()));
							$vipmotor = mysqli_fetch_array($qvipmotor);

							//$svipmobil = "SELECT Count(a.TimeIn) as TimeIn, Count(a.TimeOut) as TimeOut, Sum(b.PaidAmount) as Total
                            //				from ".$funct->getValookie('koneksi').".parkinglist as a 
                            //				inner join ".$funct->getValookie('koneksi').".parkinglistpayment as b 
                            //    				on a.TransID = b.TransID 
                            //				where a.isMember='1' and a.VehicleType = 'V' and Month(b.PaidDate)='$b' and Year(b.PaidDate)='$y' order by b.PaidDate ASC";
							//echo $sql;

							$svipmobil = "SELECT * from (
											(select count(TimeIn) as TimeIn    from ".$funct->getValookie('koneksi').".parkinglist where VehicleType = 'V' and isMember = '1' and Month(TimeIn)='$b' and Year(TimeIn)='$y') as TimeIn, 
											(select count(TimeOut) as TimeOut  from ".$funct->getValookie('koneksi').".parkinglist where VehicleType = 'V' and isMember = '1' and Month(TimeIn)='$b' and Year(TimeIn)='$y') as TimeOut,
											(select sum(b.PaidAmount) as Total from ".$funct->getValookie('koneksi').".parkinglist as a 
                                    	 		inner join ".$funct->getValookie('koneksi').".parkinglistpayment as b 
                                        			on a.TransID = b.TransID 
										 	where a.isMember= '1' and a.VehicleType = 'V' and Month(b.PaidDate)='$b' and Year(b.PaidDate)='$y') as Total
										)";

							$qvipmobil = mysqli_query($funct->getConnection(), $svipmobil) or die(mysqli_error($funct->getConnection()));
							$vipmobil = mysqli_fetch_array($qvipmobil);
							?>

							<tr>
								<td class="txt-link"><a href="<?php echo '?pg=member&detail='.$funct->Enval('Motor').'&vhl='.$funct->Enval('C').'&b='.$funct->Enval($b).'&y='.$funct->Enval($y); ?>">Motor</a></td>
								<td style="text-align: right;">
									<?php 
									if($motor['TimeIn'] == '' || is_null($motor['TimeIn']) || empty($motor['TimeIn'])) {
										echo $funct->FormatNumber('0'); 
									}
									else {
										echo $funct->FormatNumber($motor['TimeIn']); 
									}
									?>
								</td>
								<td style="text-align: right;">
									<?php
									if($motor['TimeOut'] == '' || is_null($motor['TimeOut']) || empty($motor['TimeOut'])) {
										echo $funct->FormatNumber('0'); 
									}
									else {
										echo $funct->FormatNumber($motor['TimeOut']); 
									}
									?>
								</td>
								<td>Rp.</td>
								<td style="text-align: right;">
									<?php 
									if($motor['Total'] == '' || is_null($motor['Total']) || empty($motor['Total'])) {
										echo $funct->FormatRupiah('0'); 
									}
									else {
										echo $funct->FormatRupiah($motor['Total']); 
									}
									?>
								</td>
							</tr>

							<tr>
								<td class="txt-link"><a href="<?php echo '?pg=member&detail='.$funct->Enval('Mobil').'&vhl='.$funct->Enval('B').'&b='.$funct->Enval($b).'&y='.$funct->Enval($y); ?>">Mobil</a></td>
								<td style="text-align: right;">
									<?php 
									if($mobil['TimeIn'] == '' || is_null($mobil['TimeIn']) || empty($mobil['TimeIn'])) {
										echo $funct->FormatNumber('0'); 
									}
									else {
										echo $funct->FormatNumber($mobil['TimeIn']); 
									}
									?>
								</td>
								<td style="text-align: right;">
									<?php 
									if($mobil['TimeOut'] == '' || is_null($mobil['TimeOut']) || empty($mobil['TimeOut'])) {
										echo $funct->FormatNumber('0'); 
									}
									else {
										echo $funct->FormatNumber($mobil['TimeOut']); 
									}
									?>
								</td>
								<td>Rp.</td>
								<td style="text-align: right;">
									<?php 
									if($mobil['Total'] == '' || is_null($mobil['Total']) || empty($mobil['Total'])) {
										echo $funct->FormatRupiah('0'); 
									}
									else {
										echo $funct->FormatRupiah($mobil['Total']); 
									}
									?>
								</td>
							</tr>

							<tr>
								<td class="txt-link"><a href="<?php echo '?pg=member&detail='.$funct->Enval('VIP Motor').'&vhl='.$funct->Enval('Z').'&b='.$funct->Enval($b).'&y='.$funct->Enval($y); ?>">VIP Motor</a></td>
								<td style="text-align: right;">
									<?php 
									if($vipmotor['TimeIn'] == '' || is_null($vipmotor['TimeIn']) || empty($vipmotor['TimeIn'])) {
										echo $funct->FormatNumber('0'); 
									}
									else {
										echo $funct->FormatNumber($vipmotor['TimeIn']); 
									}
									?>
								</td>
								<td style="text-align: right;">
									<?php 
									if($vipmotor['TimeOut'] == '' || is_null($vipmotor['TimeOut']) || empty($vipmotor['TimeOut'])) {
										echo $funct->FormatNumber('0'); 
									}
									else {
										echo $funct->FormatNumber($vipmotor['TimeOut']); 
									}
									?>
								</td>
								<td>Rp.</td>
								<td style="text-align: right;">
									<?php 
									if($vipmotor['Total'] == '' || is_null($vipmotor['Total']) || empty($vipmotor['Total'])) {
										echo $funct->FormatRupiah('0'); 
									}
									else {
										echo $funct->FormatRupiah($vipmotor['Total']); 
									}
									?>
								</td>
							</tr>

							<tr>
								<td class="txt-link"><a href="<?php echo '?pg=member&detail='.$funct->Enval('VIP Mobil').'&vhl='.$funct->Enval('V').'&b='.$funct->Enval($b).'&y='.$funct->Enval($y); ?>">VIP Mobil</a></td>
								<td style="text-align: right;">
									<?php
									if($vipmobil['TimeIn'] == '' || is_null($vipmobil['TimeIn']) || empty($vipmobil['TimeIn'])) {
										echo $funct->FormatNumber('0'); 
									}
									else {
										echo $funct->FormatNumber($vipmobil['TimeIn']); 
									}
									?>
								</td>
								<td style="text-align: right;">
									<?php
									if($vipmobil['TimeOut'] == '' || is_null($vipmobil['TimeOut']) || empty($vipmobil['TimeOut'])) {
										echo $funct->FormatNumber('0'); 
									}
									else {
										echo $funct->FormatNumber($vipmobil['TimeOut']); 
									}
									?>
								</td>
								<td>Rp.</td>
								<td style="text-align: right;">
									<?php 
									if($vipmobil['Total'] == '' || is_null($vipmobil['Total']) || empty($vipmobil['Total'])) {
										echo $funct->FormatRupiah('0'); 
									}
									else {
										echo $funct->FormatRupiah($vipmobil['Total']); 
									}
									?>
								</td>
							</tr>

							<tr>
								<td class="txt-link"><a href="<?php echo '?pg=member&detail='.$funct->Enval('Truck').'&vhl='.$funct->Enval('T').'&b='.$funct->Enval($b).'&y='.$funct->Enval($y); ?>">Truck</a></td>
								<td style="text-align: right;">
									<?php 
									if($truk['TimeIn'] == '' || is_null($truk['TimeIn']) || empty($truk['TimeIn'])) {
										echo $funct->FormatNumber('0'); 
									}
									else {
										echo $funct->FormatNumber($truk['TimeIn']); 
									}
									?>
								</td>
								<td style="text-align: right;">
									<?php 
									if($truk['TimeOut'] == '' || is_null($truk['TimeOut']) || empty($truk['TimeOut'])) {
										echo $funct->FormatNumber('0'); 
									}
									else {
										echo $funct->FormatNumber($truk['TimeOut']); 
									}
									?>
								</td>
								<td>Rp.</td>
								<td style="text-align: right;">
									<?php 
									if($truk['Total'] == '' || is_null($truk['Total']) || empty($truk['Total'])) {
										echo $funct->FormatRupiah('0'); 
									}
									else {
										echo $funct->FormatRupiah($truk['Total']); 
									}
									?>
								</td>
							</tr>

							<tr>
								<td class="txt-link"><a href="<?php echo '?pg=member&detail='.$funct->Enval('Valet').'&vhl='.$funct->Enval('P').'&b='.$funct->Enval($b).'&y='.$funct->Enval($y); ?>">Valet</a></td>
								<td style="text-align: right;">
									<?php 
									if($valet['TimeIn'] == '' || is_null($valet['TimeIn']) || empty($valet['TimeIn'])) {
										echo $funct->FormatNumber('0'); 
									}
									else {
										echo $funct->FormatNumber($valet['TimeIn']); 
									}
									?>
								</td>
								<td style="text-align: right;">
									<?php 
									if($valet['TimeOut'] == '' || is_null($valet['TimeOut']) || empty($valet['TimeOut'])) {
										echo $funct->FormatNumber('0'); 
									}
									else {
										echo $funct->FormatNumber($valet['TimeOut']); 
									}
									?>
								</td>
								<td>Rp.</td>
								<td style="text-align: right;">
									<?php 
									if($valet['Total'] == '' || is_null($valet['Total']) || empty($valet['Total'])) {
										echo $funct->FormatRupiah('0'); 
									}
									else {
										echo $funct->FormatRupiah($valet['Total']); 
									}
									?>
								</td>
							</tr>
							<?php
						}
						?>
					</tbody>

					<?php
					if(isset($_GET['detail'])) { ?>
						<tfoot>
							<tr>
								<th <?php if(isset($_GET['d'])) { echo 'colspan="3"'; } else { echo 'colspan="3"'; } ?> style="text-align: right;">Total</th>
								<th>Rp.</th>
								<th style="text-align: right;"><?php echo $funct->FormatRupiah($total); ?></th>
							</tr>
						</tfoot>
						<?php
					}
					?>
				</table>
			</div>
		</div>

	</div>
</div>

<div class="modal borad-30" id="cari">
	<div class="modal-content choose-date txt-black">
		<div class="row">
			<div class="col s4">
				<span class="bold">Bulan</span>
				<select id="bulan" class="browser-default">
					<?php
					for($x = 1; $x < 13; $x++) { ?>
						<option value="<?php echo $funct->Enval($x); ?>"><?php echo date('F', strtotime( date('Y').'-'.$x.'-1' )); ?></option>
						<?php
					}
					?>
				</select>
			</div>

			<div class="col s4">
				<span class="bold">Tahun</span>
				<select id="tahun" class="browser-default">
					<?php
					for($x = date('Y'); $x > 2015; $x--) { ?>
						<option value="<?php echo $funct->Enval($x); ?>"><?php echo $x; ?></option>
						<?php
					}
					?>
				</select>
			</div>

			<div class="col s4 m-t-10">
				<button id="btnok" class="btn btn-large primary-color width-100 waves-effect waves-light">OK</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$('#btnok').click(function(){
		var bulan = $('#bulan :selected').val();
		var tahun = $('#tahun :selected').val();

		window.location.href = "?pg=member&sb=index&b=" + bulan + "&y=" + tahun;
	});
</script>