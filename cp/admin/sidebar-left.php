<!-- Left Sidebar -->
<ul id="slide-out-left" class="side-nav collapsible">

    <li>
        <div class="sidenav-header primary-color">

            <div class="nav-avatar">
                <img class="circle avatar" src="" alt="">
                <div class="avatar-body">
                    <b style="color: white;">ADMIN</b>
                    <p>Online</p>
                </div>
            </div>

        </div>
    </li>

    <li><a href="?" class="no-child txt-black"><i class="ion-android-home"></i> Home</a></li>

    <li>
        <div class="collapsible-header txt-black">
            <i class="ion-android-folder"></i> Master
        </div>

        <div class="collapsible-body txt-black">
            <ul class="collapsible">
                <li class="txt-black">
                    <?php
                    // MENU MASTER
                    $master = array("User Login", "Master Card", "Member List", 
                                    "Vehicle", "Location", "Location Gate", 
                                    "Parking Product", "Parking Product Discount", "Parking Product Top Up", 
                                    "Member Card", "Member Location", "Parking Setting", "Product Member Sold");
                    sort($master);

                    $master_link = array("userlogin", "mastercard", "memberlist", "vehicle", "location", 
                                         "locationgate", "parkingproduct", "parkingproductdiscount", 
                                         "parkingproducttopup", "membercard", "memberlocation",
                                         "parkingsetting", "productmembersold");
                    sort($master_link);

                    for($m = 0; $m < count($master); $m++) { ?>
                        <a href="<?php echo '?pg='.$master_link[$m]; ?>" class="no-child txt-black"><i class="ion-document-text"></i> <?php echo $master[$m]; ?></a>
                        <?php
                    }
                    ?>
                </li>
            </ul>
        </div>
    </li>

    <li>
        <div class="collapsible-header txt-black">
            <i class="ion-android-folder"></i> Pricing
        </div>

        <div class="collapsible-body txt-black">
            <ul class="collapsible">
                <li class="txt-black">
                    <?php
                    // MENU PRICING
                    $pricing = array("Max Daily", "Scheme", "Daily");
                    sort($pricing);

                    $pricing_link = array("maxdaily", "scheme", "daily");
                    sort($pricing_link);

                    for($p = 0; $p < count($pricing); $p++) { ?>
                        <a href="<?php echo '?pg='.$pricing_link[$p]; ?>" class="no-child txt-black"><i class="ion-document-text"></i> <?php echo $pricing[$p]; ?></a>
                        <?php
                    }
                    ?>
                </li>
            </ul>
        </div>
    </li>

    <li>
        <div class="collapsible-header txt-black">
            <i class="ion-android-folder"></i> Laporan
        </div>

        <div class="collapsible-body txt-black">
            <ul class="collapsible">
                <li class="txt-black">
                    <?php
                    // MENU LAPORAN
                    $laporan = array( "Income", "Traffic Casual", "Traffic Member");
                    sort($laporan);

                    $laporan_link = array("income","casual", "member");
                    //sort($laporan_link);

                    for($l = 0; $l < count($laporan); $l++) { ?>
                        <a href="<?php echo '?pg='.$laporan_link[$l]; ?>" class="no-child txt-black"><i class="ion-document-text"></i> <?php echo $laporan[$l]; ?></a>
                        <?php
                    }
                    ?>
                </li>
            </ul>
        </div>
    </li>

    <li><a href="?pg=monitoringgate" class="no-child txt-black"><i class="ion-document-text"></i> Monitoring Gate</a></li>
    <li><a href="logout.php" class="no-child txt-black"><i class="ion-log-out"></i> Logout</a></li>

</ul>
<!-- End of Sidebars -->
