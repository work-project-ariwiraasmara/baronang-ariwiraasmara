<?php
session_start();
require('koneksi.php');
//error_reporting(0);
if(isset($_GET['download'])) {
	require('plugins/phpexcel/Classes/PHPExcel.php');
	$filePath = 'download/';

	$pg = @$_GET['pg']; 
	$sb = @$_GET['sb'];
	
	$sba = "USE ".$funct->getValookie('koneksi');
	$qba = mysqli_query($funct->getConnection(), $sba) or die(mysqli_error($funct->getConnection()));

	$objPHPExcel = new PHPExcel();
    $objPHPExcel->getProperties()->setCreator("Baronang");
    //$objPHPExcel->getProperties()->setTitle("Laporan Sky Parking");

    // set autowidth
    for($col = 'A'; $col !== 'Z'; $col++) {
        $objPHPExcel->getActiveSheet()
            ->getColumnDimension($col)
            ->setAutoSize(true);
    }

    //sheet 2
    $objWorkSheet = $objPHPExcel->createSheet(0);

	if(strtolower($pg) == 'income') {
		if(strtolower($sb) == 'list' || strtolower($sb) == 'index' || strtolower($sb) == '' || is_null($sb) || empty($sb) ) {
			require('halaman/income/download_list.php');
			$objPHPExcel->getProperties()->setTitle("Laporan Income");
		}
		else if(strtolower($sb) == 'cash') {
			require('halaman/income/download_cash.php');
			$objPHPExcel->getProperties()->setTitle("Laporan Income Cash");
		}
		else if(strtolower($sb) == 'ovo') {
			require('halaman/income/download_ovo.php');
			$objPHPExcel->getProperties()->setTitle("Laporan Income Ovo");
		}
		else if(strtolower($sb) == 'member') {
			require('halaman/income/download_member.php');
			$objPHPExcel->getProperties()->setTitle("Laporan Income Member");
		}

		else {
			header('location:index.php?'.$pg);
		}

        // download ke client
	    header('Content-type: application/vnd.ms-excel');
	    header('Content-Disposition: attachment; filename="'.$GLOBALS['fileName'].'"');
	    $objWriter->save('php://output');

	    return $filePath.'/'.$GLOBALS['fileName'];
	}

	else if(strtolower($pg) == 'casual') {
		if(strtolower($sb) == 'list' || strtolower($sb) == 'index' || strtolower($sb) == '' || is_null($sb) || empty($sb) ) {
			require('halaman/laporancasual/download.php');
			$objPHPExcel->getProperties()->setTitle("Laporan Casual");
		}

		else {
			header('location:index.php?'.$pg);
		}

		// download ke client
	    header('Content-type: application/vnd.ms-excel');
	    header('Content-Disposition: attachment; filename="'.$GLOBALS['fileName'].'"');
	    $objWriter->save('php://output');

	    return $filePath.'/'.$GLOBALS['fileName'];
	}

	else if(strtolower($pg) == 'member') {
		if(strtolower($sb) == 'list' || strtolower($sb) == 'index' || strtolower($sb) == '' || is_null($sb) || empty($sb) ) {
			require('halaman/laporanmember/download.php');
			$objPHPExcel->getProperties()->setTitle("Laporan Member");
		}

		else {
			header('location:index.php?'.$pg);
		}

		// download ke client
	    header('Content-type: application/vnd.ms-excel');
	    header('Content-Disposition: attachment; filename="'.$GLOBALS['fileName'].'"');
	    $objWriter->save('php://output');

	    return $filePath.'/'.$GLOBALS['fileName'];
	}

	else {
		header('location:index.php?'.$pg);
	}
}
else { ?>
<!DOCTYPE html>
<html lang="en-US">
	<head>
		<title>Cloud Parking</title>
		<meta content="IE=edge" http-equiv="x-ua-compatible">
		   <meta content="initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" name="viewport">
		   <meta content="yes" name="apple-mobile-web-app-capable">
		   <meta content="yes" name="apple-touch-fullscreen">

		   <link href='static/images/Logo-fish-icon.png' type='image/x-icon'/>
		   <link href='static/images/Logo-fish-icon.png' rel='shortcut icon'/>

		   <!-- Fonts -->
		   <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
		    
		   <!-- Icons -->
		   <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" media="all" rel="stylesheet" type="text/css">
		    
		   <!-- Styles -->
		   <link href="css/keyframes.css" rel="stylesheet" type="text/css">
		   <link href="css/materialize.min.css" rel="stylesheet" type="text/css">
		   <link href="css/swiper.css" rel="stylesheet" type="text/css">
		   <link href="css/swipebox.min.css" rel="stylesheet" type="text/css">
		   <link href="css/style.css" rel="stylesheet" type="text/css">
		   <link href="css/custom.css" rel="stylesheet" type="text/css">

		   <script type="text/javascript" src="js/jquery-2.1.0.min.js"></script>
	</head>

	<body>
		<div class="m-scene" id="main">
		<?php
		if(isset( $_COOKIE['login'] )) { 
			require('sidebar-left.php');
			?>
			<div id="content" class="page txt-black">

				<?php
				$sba = "USE ".$funct->getValookie('koneksi');
				$qba = mysqli_query($funct->getConnection(), $sba) or die(mysqli_error($funct->getConnection()));

				$pg = @$_GET['pg'];
				$sb = @$_GET['sb'];

				if(strtolower($pg) == '' || strtolower($pg) == 'home' || strtolower($pg) == 'beranda') {
					require('home.php');
					$funct->setTitle('Dashboard');
					$funct->setDocTitle();
				}

				else if(strtolower($pg) == 'income') {
					if(strtolower($sb) == 'list' || strtolower($sb) == 'index' || strtolower($sb) == '' || is_null($sb) || empty($sb) ) {
						require('halaman/income/list.php');
						$funct->setTitle('Laporan Income');
						$funct->setDocTitle();
					}
					else if(strtolower($sb) == 'cash') {
						require('halaman/income/cash.php');
						$funct->setTitle('Laporan Income Detil Cash');
						$funct->setDocTitle();
					}
					else if(strtolower($sb) == 'ovo') {
						require('halaman/income/ovo.php');
						$funct->setTitle('Laporan Income Detil Ovo');
						$funct->setDocTitle();
					}
					else if(strtolower($sb) == 'member') {
						require('halaman/income/member.php');
						$funct->setTitle('Laporan Income Detil Member');
						$funct->setDocTitle();
					}
					// else if(strtolower($sb) == 'denda') {

					// }
					// else if(strtolower($sb) == 'emoney') {

					// }

					else {
						require('404.php');
					}
				}

				else if(strtolower($pg) == 'casual') {
					if(strtolower($sb) == 'list' || strtolower($sb) == 'index' || strtolower($sb) == '' || is_null($sb) || empty($sb) ) {
						require('halaman/laporancasual/list.php');
						$funct->setTitle('Laporan Casual');
						$funct->setDocTitle();
					}

					else {
						require('404.php');
					}
				}

				else if(strtolower($pg) == 'member') {
					if(strtolower($sb) == 'list' || strtolower($sb) == 'index' || strtolower($sb) == '' || is_null($sb) || empty($sb) ) {
						require('halaman/laporanmember/list.php');
						$funct->setTitle('Laporan Member');
						$funct->setDocTitle();
					}

					else {
						require('404.php');
					}
				}

				else if(strtolower($pg) == 'location') {
					if(strtolower($sb) == 'list' || strtolower($sb) == 'index' || strtolower($sb) == '' || is_null($sb) || empty($sb) ) {
						require('halaman/location/list.php');
						$funct->setTitle('Location');
						$funct->setDocTitle();
					}
					else if(strtolower($sb) == 'add') {
						require('halaman/location/add.php');
						$funct->setTitle('Add Location');
						$funct->setDocTitle();
					}
					else if(strtolower($sb) == 'edit') {
						require('halaman/location/edit.php');
						$funct->setTitle('Edit Location');
						$funct->setDocTitle();
					}

					else {
						require('404.php');
					}
				}

				else if(strtolower($pg) == 'locationgate') {
					if(strtolower($sb) == 'list' || strtolower($sb) == 'index' || strtolower($sb) == '' || is_null($sb) || empty($sb) ) {
						require('halaman/locationgate/list.php');
						$funct->setTitle('Location Gate');
						$funct->setDocTitle();
					}
					else if(strtolower($sb) == 'add') {
						require('halaman/locationgate/add.php');
						$funct->setTitle('Add Location Gate');
						$funct->setDocTitle();
					}
					else if(strtolower($sb) == 'edit') {
						require('halaman/locationgate/edit.php');
						$funct->setTitle('Edit Location Gate');
						$funct->setDocTitle();
					}

					else {
						require('404.php');
					}
				}

				/*
				else if(strtolower($pg) == 'mastercard') {
					if(strtolower($sb) == 'list' || strtolower($sb) == 'index' || strtolower($sb) == '' || is_null($sb) || empty($sb) ) {
						require('halaman/mastercard/list.php');
						$funct->setTitle('Master Card');
						$funct->setDocTitle();
					}
					else if(strtolower($sb) == 'add') {
						require('halaman/mastercard/add.php');
						$funct->setTitle('Add Master Card');
						$funct->setDocTitle();
					}
					else if(strtolower($sb) == 'edit') {
						require('halaman/mastercard/edit.php');
						$funct->setTitle('Edit Master Card');
						$funct->setDocTitle();
					}

					else {
						require('404.php');
					}
				}
				*/

				
				// table pricingdailymax;
				else if(strtolower($pg) == 'maxdaily') {
					if(strtolower($sb) == 'list' || strtolower($sb) == 'index' || strtolower($sb) == '' || is_null($sb) || empty($sb) ) {
						require('halaman/maxdaily/list.php');
						$funct->setTitle('Max Daily');
						$funct->setDocTitle();
					}
					else if(strtolower($sb) == 'add') {
						require('halaman/maxdaily/add.php');
						$funct->setTitle('Add Max Daily');
						$funct->setDocTitle();
					}
					else if(strtolower($sb) == 'edit') {
						require('halaman/maxdaily/edit.php');
						$funct->setTitle('Edit Max Daily');
						$funct->setDocTitle();
					}

					else {
						require('404.php');
					}
				}
				

				/*
				else if(strtolower($pg) == 'membercard') {
					if(strtolower($sb) == 'list' || strtolower($sb) == 'index' || strtolower($sb) == '' || is_null($sb) || empty($sb) ) {
						require('halaman/membercard/list.php');
						$funct->setTitle('Member Card');
						$funct->setDocTitle();
					}
					else if(strtolower($sb) == 'add') {
						require('halaman/membercard/add.php');
						$funct->setTitle('Add Member Card');
						$funct->setDocTitle();
					}
					else if(strtolower($sb) == 'edit') {
						require('halaman/membercard/edit.php');
						$funct->setTitle('Edit Member Card');
						$funct->setDocTitle();
					}

					else {
						require('404.php');
					}
				}
				*/

				/*
				else if(strtolower($pg) == 'memberlist') {
					if(strtolower($sb) == 'list' || strtolower($sb) == 'index' || strtolower($sb) == '' || is_null($sb) || empty($sb) ) {
						require('halaman/memberlist/list.php');
						$funct->setTitle('Member List');
						$funct->setDocTitle();
					}
					else if(strtolower($sb) == 'add') {
						require('halaman/memberlist/add.php');
						$funct->setTitle('Add Member List');
						$funct->setDocTitle();
					}
					else if(strtolower($sb) == 'edit') {
						require('halaman/memberlist/edit.php');
						$funct->setTitle('Edit Member List');
						$funct->setDocTitle();
					}

					else {
						require('404.php');
					}
				}
				*/

				/*
				else if(strtolower($pg) == 'memberlocation') {
					if(strtolower($sb) == 'list' || strtolower($sb) == 'index' || strtolower($sb) == '' || is_null($sb) || empty($sb) ) {
						require('halaman/memberlocation/list.php');
						$funct->setTitle('Member Location');
						$funct->setDocTitle();
					}
					else if(strtolower($sb) == 'add') {
						require('halaman/memberlocation/add.php');
						$funct->setTitle('Add Member Location');
						$funct->setDocTitle();
					}
					else if(strtolower($sb) == 'edit') {
						require('halaman/memberlocation/edit.php');
						$funct->setTitle('Edit Member Location');
						$funct->setDocTitle();
					}

					else {
						require('404.php');
					}
				}
				*/

				else if(strtolower($pg) == 'monitoringgate') {
					if(strtolower($sb) == 'list' || strtolower($sb) == 'index' || strtolower($sb) == '' || is_null($sb) || empty($sb) ) {
						require('halaman/monitoringgate/list.php');
						$funct->setTitle('Monitoring Gate');
						$funct->setDocTitle();
					}

					else {
						require('404.php');
					}
				}

				/*
				else if(strtolower($pg) == 'parkingproduct') {
					if(strtolower($sb) == 'list' || strtolower($sb) == 'index' || strtolower($sb) == '' || is_null($sb) || empty($sb) ) {
						require('halaman/parkingproduct/list.php');
						$funct->setTitle('Parking Product');
						$funct->setDocTitle();
					}
					else if(strtolower($sb) == 'add') {
						require('halaman/parkingproduct/add.php');
						$funct->setTitle('Add Parking Product');
						$funct->setDocTitle();
					}
					else if(strtolower($sb) == 'edit') {
						require('halaman/parkingproduct/edit.php');
						$funct->setTitle('Edit Parking Product');
						$funct->setDocTitle();
					}

					else {
						require('404.php');
					}
				}
				*/

				/*
				else if(strtolower($pg) == 'parkingproductdiscount') {
					if(strtolower($sb) == 'list' || strtolower($sb) == 'index' || strtolower($sb) == '' || is_null($sb) || empty($sb) ) {
						require('halaman/parkingproductdiscount/list.php');
						$funct->setTitle('Parking Product Discount');
						$funct->setDocTitle();
					}
					else if(strtolower($sb) == 'add') {
						require('halaman/parkingproductdiscount/add.php');
						$funct->setTitle('Add Parking Product Discount');
						$funct->setDocTitle();
					}
					else if(strtolower($sb) == 'edit') {
						require('halaman/parkingproductdiscount/edit.php');
						$funct->setTitle('Edit Parking Product Discount');
						$funct->setDocTitle();
					}

					else {
						require('404.php');
					}
				}
				*/

				/*
				else if(strtolower($pg) == 'parkingproducttop') {
					if(strtolower($sb) == 'list' || strtolower($sb) == 'index' || strtolower($sb) == '' || is_null($sb) || empty($sb) ) {
						require('halaman/parkingproducttop/list.php');
						$funct->setTitle('Parking Product Top');
						$funct->setDocTitle();
					}
					else if(strtolower($sb) == 'add') {
						require('halaman/parkingproducttop/add.php');
						$funct->setTitle('Add Parking Product Top');
						$funct->setDocTitle();
					}
					else if(strtolower($sb) == 'edit') {
						require('halaman/parkingproducttop/edit.php');
						$funct->setTitle('Edit Parking Product Top');
						$funct->setDocTitle();
					}

					else {
						require('404.php');
					}
				}
				*/

				
				else if(strtolower($pg) == 'parkingsetting') {
					if(strtolower($sb) == 'list' || strtolower($sb) == 'index' || strtolower($sb) == '' || is_null($sb) || empty($sb) ) {
						require('halaman/parkingsetting/list.php');
						$funct->setTitle('Parking Setting');
						$funct->setDocTitle();
					}
					// else if(strtolower($sb) == 'add') {
					// 	require('halaman/parkingsetting/add.php');
					// 	$funct->setTitle('Add Parking Setting');
					// 	$funct->setDocTitle();
					// }
					// else if(strtolower($sb) == 'edit') {
					// 	require('halaman/parkingsetting/edit.php');
					// 	$funct->setTitle('Edit Parking Setting');
					// 	$funct->setDocTitle();
					// }

					else {
						require('404.php');
					}
				}
				


				else if(strtolower($pg) == 'scheme') {
					if(strtolower($sb) == 'list' || strtolower($sb) == 'index' || strtolower($sb) == '' || is_null($sb) || empty($sb) ) {
						require('halaman/scheme/list.php');
						$funct->setTitle('Pricing Scheme');
						$funct->setDocTitle();
					}
					else if(strtolower($sb) == 'add') {
						require('halaman/scheme/add.php');
						$funct->setTitle('Add Pricing Scheme');
						$funct->setDocTitle();
					}
					else if(strtolower($sb) == 'edit') {
						require('halaman/scheme/edit.php');
						$funct->setTitle('Edit Pricing Scheme');
						$funct->setDocTitle();
					}

					else {
						require('404.php');
					}
				}
				

				
				else if(strtolower($pg) == 'productmembersold') {
					if(strtolower($sb) == 'list' || strtolower($sb) == 'index' || strtolower($sb) == '' || is_null($sb) || empty($sb) ) {
						require('halaman/productmembersold/list.php');
						$funct->setTitle('Product Member Sold');
						$funct->setDocTitle();
					}
					// else if(strtolower($sb) == 'add') {
					// 	require('halaman/productmembersold/add.php');
					// 	$funct->setTitle('Add Product Member Sold');
					// 	$funct->setDocTitle();
					// }
					// else if(strtolower($sb) == 'edit') {
					// 	require('halaman/productmembersold/edit.php');
					// 	$funct->setTitle('Edit Product Member Sold');
					// 	$funct->setDocTitle();
					// }

					else {
						require('404.php');
					}
				}
				

				

				/*
				else if(strtolower($pg) == 'userlogin') {
					if(strtolower($sb) == 'list' || strtolower($sb) == 'index' || strtolower($sb) == '' || is_null($sb) || empty($sb) ) {
						require('halaman/userlogin/list.php');
						$funct->setTitle('User Login');
						$funct->setDocTitle();
					}
					else if(strtolower($sb) == 'add') {
						require('halaman/userlogin/add.php');
						$funct->setTitle('Add User Login');
						$funct->setDocTitle();
					}
					else if(strtolower($sb) == 'edit') {
						require('halaman/userlogin/edit.php');
						$funct->setTitle('Edit User Login');
						$funct->setDocTitle();
					}

					else {
						require('404.php');
					}
				}
				*/

				
				else if(strtolower($pg) == 'vehicle') {
					if(strtolower($sb) == 'list' || strtolower($sb) == 'index' || strtolower($sb) == '' || is_null($sb) || empty($sb) ) {
						require('halaman/vehicle/list.php');
						$funct->setTitle('Vehicle');
						$funct->setDocTitle();
					}
					// else if(strtolower($sb) == 'add') {
					// 	require('halaman/vehicle/add.php');
					// 	$funct->setTitle('Add Vehicle');
					// 	$funct->setDocTitle();
					// }
					// else if(strtolower($sb) == 'edit') {
					// 	require('halaman/vehicle/edit.php');
					// 	$funct->setTitle('Edit Vehicle');
					// 	$funct->setDocTitle();
					// }

					else {
						require('404.php');
					}
				}
				

				else {
					require('404.php');
				}

				// echo $funct->Denval($_COOKIE[$funct->ENID('userid')]).'<br>';
				// echo $funct->Denval($_COOKIE[$funct->ENID('cmpid')]).'<br>';
				// echo $funct->Denval($_COOKIE[$funct->ENID('username')]).'<br>';
				// echo $funct->Denval($_COOKIE[$funct->ENID('email')]).'<br>';
				// echo $funct->Denval($_COOKIE[$funct->ENID('pin')]).'<br>';
				?>
			</div>

			<script type="text/javascript">
				function confirmDelete(tbl, fld, val, url) {
					if(confirm("Anda yakin ingin menghapus data ini?\n"+fld+": "+val)) {
						$.ajax({
	                        url : "ajax_deletedata.php",
	                        type : 'POST',
	                        data: { 
	                        	tbl: tbl,
	                        	fld: fld,
	                        	val: val
	                        },
	                        success : function(data) {
	                            //alert("Data: " + name + " dengan:\nTabel: " + tbl + "\nField: " + fld + "\nValue: " + val);
	                        	window.location.href = url;
	                        	//console.log(data);
	                        },
	                        error : function(){
	                            alert('Terjadi kesalahan dalam jaringan!\nGagal Menghapus Data!\nSilahkan cek koneksi jaringan dan coba lagi!');
	                            return false;
	                        }
	                    });
					}
					else {
						alert("Data Tidak Jadi Dihapus!");
					}
				}
			</script>

			<?php
		}
		else {
			?><script type="text/javascript">document.title = "Cloud Parking - Login"</script><?php
			require('login.php');
		}
		?>

		<script src="js/jquery.swipebox.min.js"></script>
		<script src="js/materialize.min.js"></script>
		<script src="js/swiper.min.js"></script>
		<script src="js/jquery.mixitup.min.js"></script>
		<script src="js/masonry.min.js"></script>
		<script src="js/chart.min.js"></script>
		<script src="js/functions.js"></script>
	</body>
</html>
<?php
}
?>