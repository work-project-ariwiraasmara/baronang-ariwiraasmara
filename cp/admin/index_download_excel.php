<?php
require('koneksi.php');

class PassArray {

	public $array = array();
	public function setArray($arr) {
		array_push($this->array, $arr);
	}

	function getArray() {
        return $this->array;
    }

    function getArrayPos($pos) {
        return $this->array[$pos];
    }

}

if(isset($_GET['download'])) {
	require('plugins/phpexcel/Classes/PHPExcel.php');
	$filePath = 'download/';

	$pg = @$_GET['pg']; 
	$sb = @$_GET['sb'];

	$myarr = new PassArray();
	
	$sba = "USE ".$funct->getValookie('koneksi');
	$qba = mysqli_query($funct->getConnection(), $sba) or die(mysqli_error($funct->getConnection()));

	$objPHPExcel = new PHPExcel();
    $objPHPExcel->getProperties()->setCreator("Baronang");
    //$objPHPExcel->getProperties()->setTitle("Laporan Sky Parking");

    // set autowidth
    for($col = 'A'; $col !== 'Z'; $col++) {
        $objPHPExcel->getActiveSheet()
            ->getColumnDimension($col)
            ->setAutoSize(true);
    }

    //sheet 2
    $objWorkSheet = $objPHPExcel->createSheet(0);

	if(strtolower($pg) == 'income') {
		if(strtolower($sb) == 'list' || strtolower($sb) == 'index' || strtolower($sb) == '' || is_null($sb) || empty($sb) ) {
			require('halaman/income/download_list.php');
			$objPHPExcel->getProperties()->setTitle("Laporan Income");
		}
		else if(strtolower($sb) == 'cash') {
			require('halaman/income/download_cash.php');
			$objPHPExcel->getProperties()->setTitle("Laporan Income Cash");
		}
		else if(strtolower($sb) == 'ovo') {
			require('halaman/income/download_ovo.php');
			$objPHPExcel->getProperties()->setTitle("Laporan Income Ovo");
		}
		else if(strtolower($sb) == 'member') {
			require('halaman/income/download_member.php');
			$objPHPExcel->getProperties()->setTitle("Laporan Income Member");
		}

		else {
			header('location:index.php?'.$pg);
		}

        // download ke client
	    header('Content-type: application/vnd.ms-excel');
	    header('Content-Disposition: attachment; filename="'.$GLOBALS['fileName'].'"');
	    $objWriter->save('php://output');

	    return $filePath.'/'.$GLOBALS['fileName'];
	}

	else if(strtolower($pg) == 'casual') {
		if(strtolower($sb) == 'list' || strtolower($sb) == 'index' || strtolower($sb) == '' || is_null($sb) || empty($sb) ) {
			require('halaman/laporancasual/download.php');
			$objPHPExcel->getProperties()->setTitle("Laporan Casual");
		}

		else {
			header('location:index.php?'.$pg);
		}

		// download ke client
	    header('Content-type: application/vnd.ms-excel');
	    header('Content-Disposition: attachment; filename="'.$GLOBALS['fileName'].'"');
	    $objWriter->save('php://output');

	    return $filePath.'/'.$GLOBALS['fileName'];
	}

	else if(strtolower($pg) == 'member') {
		if(strtolower($sb) == 'list' || strtolower($sb) == 'index' || strtolower($sb) == '' || is_null($sb) || empty($sb) ) {
			require('halaman/laporanmember/download.php');
			$objPHPExcel->getProperties()->setTitle("Laporan Member");
		}

		else {
			header('location:index.php?'.$pg);
		}

		// download ke client
	    header('Content-type: application/vnd.ms-excel');
	    header('Content-Disposition: attachment; filename="'.$GLOBALS['fileName'].'"');
	    $objWriter->save('php://output');

	    return $filePath.'/'.$GLOBALS['fileName'];
	}

	else {
		header('location:index.php?'.$pg);
	}


}
else {
	//header('location:index.php');
	require('index.php');
}
?>