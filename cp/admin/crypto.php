<?php
class FunctionAdmin {

    public function getS() {
        //$srv = "MOQr5tX3S/fevG5J3fPpBySB41X6CGo7nFq84Bm1gSWkRWyRBRyu8MK4xjaGA4mpgyP90tYeyqmhybuOoxY3pA=="; // => 35.240.183.132
        //$srv = "YpxSNwCk8Avyni3Q+fRcrG8dYKqgpSeoMYfUGhbjOTQNopPTGv7Pr9xAkjNRPh1jjor5bu7oRgMtT6gRWkrvAg=="; // => localhost
        return "YpxSNwCk8Avyni3Q+fRcrG8dYKqgpSeoMYfUGhbjOTQNopPTGv7Pr9xAkjNRPh1jjor5bu7oRgMtT6gRWkrvAg==";
    }

    public function getU() {
        //$uid = 'HIczhNlOQ4ZxAwtD2l9VTfCA9woR8ezWAOW8mXV//VDe480AkvfCmnkcx4keVYNKRHcRe6UjzZjQsZuPk+O8Kw==';
        return "HIczhNlOQ4ZxAwtD2l9VTfCA9woR8ezWAOW8mXV//VDe480AkvfCmnkcx4keVYNKRHcRe6UjzZjQsZuPk+O8Kw==";
    }

    public function getP() {
        //$pwd = 'p5joToJ2y+aiwp5nS2zuDb8VpcQH4rJvmtR0xRpUQhiNKhnsB01ETCP3fgihJSs5D/SUrGSmMOREHKOezgvZJA==';
        //$pwd = 'O8RzFUKS89Xq5BB982vogmVRAyrHEdIn8jfAuwSnl0zbxwx3ZYqRKfFAx+ZvMxMMeHDEStEdqvk0FgyvYwwslA=='; // => kosong
        return "p5joToJ2y+aiwp5nS2zuDb8VpcQH4rJvmtR0xRpUQhiNKhnsB01ETCP3fgihJSs5D/SUrGSmMOREHKOezgvZJA==";
    }

    public function getD() {
        //$dbs = 'Y3lGJjJIkl8ZhTepHCWcYxCjXKm7aUyjKatA0HwqqlSXpEGuNTxFa0oP0SMeF5DDdBIwzInC2dGtamH5qZFi7g==';
        return "Y3lGJjJIkl8ZhTepHCWcYxCjXKm7aUyjKatA0HwqqlSXpEGuNTxFa0oP0SMeF5DDdBIwzInC2dGtamH5qZFi7g==";
    }

    public function getK() {
        //$key = "YDMJ9EeUqmte9U+FqEeSUqD/eL5Dc6fuRQp96seQAxRLWlXmOSIzlpwuESnJp5Z3mNe7qVhqyhJIKKbUbWrOLw==";
        return "YDMJ9EeUqmte9U+FqEeSUqD/eL5Dc6fuRQp96seQAxRLWlXmOSIzlpwuESnJp5Z3mNe7qVhqyhJIKKbUbWrOLw==";
    }

	public function encrypt($code, $key){
        $k = $this->ks($key);

        $plaintext = $code;
        $ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
        $iv = openssl_random_pseudo_bytes($ivlen);
        $ciphertext_raw = openssl_encrypt($plaintext, $cipher, $k, $options=OPENSSL_RAW_DATA, $iv);
        $hmac = hash_hmac('sha256', $ciphertext_raw, $k, $as_binary=true);
        $ciphertext = base64_encode( $iv.$hmac.$ciphertext_raw );

        return $ciphertext;
    }

    public function decrypt($code, $key){
        $k = $this->ks($key);

        $c = base64_decode($code);
        $ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
        $iv = substr($c, 0, $ivlen);
        $hmac = substr($c, $ivlen, $sha2len=32);
        $ciphertext_raw = substr($c, $ivlen+$sha2len);
        $original_plaintext = openssl_decrypt($ciphertext_raw, $cipher, $k, $options=OPENSSL_RAW_DATA, $iv);
        $calcmac = hash_hmac('sha256', $ciphertext_raw, $k, $as_binary=true);
        if (hash_equals($hmac, $calcmac))
        {
            return $original_plaintext;
        }
    }

    private function ks($code){
        $c = base64_decode($code);
        $ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
        $iv = substr($c, 0, $ivlen);
        $hmac = substr($c, $ivlen, $sha2len=32);
        $ciphertext_raw = substr($c, $ivlen+$sha2len);
        $original_plaintext = openssl_decrypt($ciphertext_raw, $cipher, 'c8f6dae75c5e2010d46924649e0c0357', $options=OPENSSL_RAW_DATA, $iv);
        $calcmac = hash_hmac('sha256', $ciphertext_raw, 'c8f6dae75c5e2010d46924649e0c0357', $as_binary=true);
        if (hash_equals($hmac, $calcmac))
        {
            return $original_plaintext;
        }
    }

    public $koneksi;

    function setConnection() {
        $this->koneksi = mysqli_connect($this->decrypt($this->getS(), $this->getK()), 
                                        $this->decrypt($this->getU(), $this->getK()), 
                                        $this->decrypt($this->getP(), $this->getK()), 
                                        $this->decrypt($this->getD(), $this->getK())); 
    }

    function getConnection() {
        return $this->koneksi;
    }


    // SQL QUERY
    function queryAll($table, $order) {
        $sql = "SELECT * from $table $order";
        return mysqli_query($this->getConnection(), $sql) or die(mysqli_error($this->getConnection()));
    }

    function queryAll1($table, $order) {
        $sql = "SELECT * from $table $order limit 1";
        return mysqli_query($this->getConnection(), $sql) or die(mysqli_error($this->getConnection()));
    }

    function queryAllBy($table, $field, $value, $order) {
        $sql = "SELECT * from $table where $field='$value' $order";
        return mysqli_query($this->getConnection(), $sql) or die(mysqli_error($this->getConnection()));
    }

    function queryAllBy1($table, $field, $value, $order) {
        $sql = "SELECT * from $table where $field='$value' $order limit 1";
        return mysqli_query($this->getConnection(), $sql) or die(mysqli_error($this->getConnection()));
    }
    #
    function queryAllF($table, $columns, $order) {
        $sql = "SELECT $columns from $table $order";
        return mysqli_query($this->getConnection(), $sql) or die(mysqli_error($this->getConnection()));
    }

    function queryAllF1($table, $columns, $order) {
        $sql = "SELECT $columns from $table $order limit 1";
        return mysqli_query($this->getConnection(), $sql) or die(mysqli_error($this->getConnection()));
    }

    function queryAllFBy($table, $columns, $field, $value, $order) {
        $sql = "SELECT $columns from $table where $field='$value' $order";
        return mysqli_query($this->getConnection(), $sql) or die(mysqli_error($this->getConnection()));
    }

    function queryAllFBy1($table, $columns, $field, $value, $order) {
        $sql = "SELECT $columns from $table where $field='$value' $order limit 1";
        return mysqli_query($this->getConnection(), $sql) or die(mysqli_error($this->getConnection()));
    }

    
    function ENID($str) {
        return bin2hex(crc32(sha1(md5(crypt($str,'$6$rounds=999999999'), TRUE), TRUE)));
    }

    function Enlink($str) {
        return bin2hex(base64_encode(crc32(sha1( md5($str), TRUE ))));
    }

    function Enval($str) {
        return bin2hex(base64_encode( $str ));
    }

    function Denval($str) {
        return base64_decode(hex2bin( $str ));
    }

    function setIDParam($id, $val) {
        return $this->Enlink($id).'='.$this->Enval($val);
    }

    function getIDParam($id) {
        return $this->Denval(@$_GET[$this->Enlink($id)]);
    }

    function ESC($str) {
        return htmlspecialchars(htmlentities(addslashes($str)));
    }

    function REST($str) {
        return mysqli_real_escape_string($this->getConnection(), $this->ESC($str));
    }

    function READABLE($str) {
        return html_entity_decode(htmlspecialchars_decode($str));
    }

    function FormatRupiah($str) {
        return number_format($str,2,',','.');
    }

    function FormatNumber($str) {
        return number_format($str,0,',','.');
    }

    function Rupiah($str) {
        return 'Rp. '.number_format($str,2,',','.');
    }

    function setOneSession($a, $b, $type=null) {
        if( $type > 1 ) {
            $_SESSION[$this->ENID($a)] = $this->Enval($b);
        }
        else if( $type == 1 ) {
            $_SESSION[$this->ENID($a)] = $b;
        }
        else {
            $_SESSION[$a] = $b;
        }
    }
    
    function setSession($a, $b, $c, $d, $e) {
        $_SESSION[$this->ENID('userid')]    = $this->Enval($a);
        $_SESSION[$this->ENID('cmpid')]     = $this->Enval($b);
        $_SESSION[$this->ENID('username')]  = $this->Enval($c);
        $_SESSION[$this->ENID('email')]     = $this->Enval($d);
        $_SESSION[$this->ENID('pin')]       = $this->Enval($e);
    }

    function setOneCookie($name, $val, $time=1, $type=null) {
        if($type > 1) {
            setcookie($this->ENID($name), $this->Enval($val), time() + ($time * 24 * 60 * 60), "/");
        }
        else if($type == 1) {
            setcookie($this->ENID($name), $val, time() + ($time * 24 * 60 * 60), "/");
        }
        else {
            setcookie($name, $val, time() + ($time * 24 * 60 * 60), "/");
        }
    }

    function setCookie($a, $b, $c, $d, $e, $f, $g, $h) {
        setcookie('login', $this->Enval('1-OK-1'), time() + (7 * 24 * 60 * 60), "/");
        setcookie($this->ENID('userid'),    $this->Enval($a), time() + (7 * 24 * 60 * 60), "/");
        setcookie($this->ENID('cmpid'),     $this->Enval($b), time() + (7 * 24 * 60 * 60), "/");
        setcookie($this->ENID('username'),  $this->Enval($c), time() + (7 * 24 * 60 * 60), "/");
        setcookie($this->ENID('email'),     $this->Enval($d), time() + (7 * 24 * 60 * 60), "/");
        setcookie($this->ENID('pin'),       $this->Enval($e), time() + (7 * 24 * 60 * 60), "/");
        setcookie($this->ENID('koneksi'),   $this->Enval($f), time() + (7 * 24 * 60 * 60), "/");
        setcookie($this->ENID('firstname'), $this->Enval($g), time() + (7 * 24 * 60 * 60), "/");
        setcookie($this->ENID('lastname'),  $this->Enval($h), time() + (7 * 24 * 60 * 60), "/");
    }

    function getSession($str, $type=null) {
        if($type > 1) {
            return $this->Denval($_SESSION[$this->ENID($a)]);
        }
        else if($type == 1) {
            return $_SESSION[$this->ENID($a)];
        }
        else {
            return $_SESSION[$str];
        }
    }

    function getCookie($str) {
        return $_COOKIE[$this->ENID($str)];
    }

    function getValookie($str) {
        return $this->Denval($this->getCookie($str));
    }

    public $titlepage;

    function setTitle($title) {
        $this->titlepage = $title;
    }

    function getTitle() {
        return $this->titlepage;
    }

    function setDocTitle() { ?>
        <script type="text/javascript">
            document.title = "<?php echo 'Cloud Parking - '.$this->getTitle(); ?>";
            $('#title').html("<?php echo $this->getTitle(); ?>")
        </script>
        <?php
    }

    function logoutSystem() {
        setcookie('login', null, time() - (365 * 24 * 60 * 60), "/");
        setcookie($this->ENID('userid'),    null, time() - (365 * 24 * 60 * 60), "/");
        setcookie($this->ENID('cmpid'),     null, time() - (365 * 24 * 60 * 60), "/");
        setcookie($this->ENID('username'),  null, time() - (365 * 24 * 60 * 60), "/");
        setcookie($this->ENID('email'),     null, time() - (365 * 24 * 60 * 60), "/");
        setcookie($this->ENID('pin'),       null, time() - (365 * 24 * 60 * 60), "/");
        setcookie($this->ENID('koneksi'),   null, time() - (365 * 24 * 60 * 60), "/");
        setcookie($this->ENID('firstname'), null, time() - (365 * 24 * 60 * 60), "/");
        setcookie($this->ENID('lastname'),  null, time() - (365 * 24 * 60 * 60), "/");
    }

    function paginate_one($reload, $page, $tpages, $adjacents) {
        $firstlabel = "<b>1</b>";
        $prevlabel  = "<b>Prev</b>";
        $nextlabel  = "<b>Next</b>";
        $lastlabel  = "<b>$tpages</b>";
        
        $out = "<div class=\"m-t-10 m-b-10 center backpaging\"><ul class=\"ownpaging\">\n";
        
        // first
        if($page>1) {
            $out.= "<li><a href=\"" . $reload . "\">" . $firstlabel . "</a></li>\n";
        }
        else {
            $out.= "<li class=\"disabledd\"><a href=\"#\">" . $firstlabel . "</a></li>\n";
        }
        
        // previous
        if($page==1) {
            $out.= "<li class=\"disabledd\"><a href=\"#\">" . $prevlabel . "</a></li>\n";
        }
        elseif($page==2) {
            $out.= "<li><a href=\"" . $reload . "\">" . $prevlabel . "</a></li>\n";
        }
        else {
            $out.= "<li><a href=\"" . $reload . "&amp;hlm=" . ($page-1) . "\">" . $prevlabel . "</a></li>\n";
        }
        
        // current
        $out.= "<li class=\"active\"><a href=\"#\" class=\"active\">Page " . $page . " of " . $tpages . "</a></li>\n";
        
        // next
        if($page<$tpages) {
            $out.= "<li><a href=\"" . $reload . "&amp;hlm=" .($page+1) . "\">" . $nextlabel . "</a></li>\n";
        }
        else {
            $out.= "<li class=\"disabledd\"><a href=\"#\">" . $nextlabel . "</a></li>\n";
        }
        
        // last
        if($page<$tpages) {
            $out.= "<li><a href=\"" . $reload . "&amp;hlm=" . $tpages . "\">" . $lastlabel . "</a></li>\n";
        }
        else {
            $out.= "<li class=\"disabledd\"><a href=\"#\">" . $lastlabel . "</a></li>\n";
        }
        
        $out.= "</ul></div>";
        
        return $out;
    }
    

    public $array = array();
    function setArray($arr) {
        array_push($this->array, $arr);
    }

    function getArray() {
        return $this->array;
    }

    function getArrayPos($pos) {
        return $this->array[$pos];
    }

    public $puval;
    function setVal($val) {
        $this->puval = $val;
    }

    function getVal() {
        return $this->puval;
    }
}
?>