<?php
class FunctionAdmin {

    function ENID($str) {
        return bin2hex(crc32(sha1(md5(crypt($str,'$6$rounds=999999999'), TRUE), TRUE)));
    }

    function READABLE($str) {
        return html_entity_decode(htmlspecialchars_decode($str));
    }

    function Rupiah($str) {
        return 'Rp. '.number_format($angka,2,',','.');
    }
	

    function setSession($a, $b, $c, $d, $e) {
        $_SESSION[$this->ENID('userid')]    = $this->Enval($a);
        $_SESSION[$this->ENID('cmpid')]     = $this->Enval($b);
        $_SESSION[$this->ENID('username')]  = $this->Enval($c);
        $_SESSION[$this->ENID('email')]     = $this->Enval($d);
        $_SESSION[$this->ENID('pin')]       = $this->Enval($e);
    }

    function setCookie($a, $b, $c, $d, $e, $f, $g, $h) {
        setcookie('login', $this->Enval('1-OK-1'), time() + (7 * 24 * 60 * 60), "/");
        setcookie($this->ENID('userid'),    $this->Enval($a), time() + (7 * 24 * 60 * 60), "/");
        setcookie($this->ENID('cmpid'),     $this->Enval($b), time() + (7 * 24 * 60 * 60), "/");
        setcookie($this->ENID('username'),  $this->Enval($c), time() + (7 * 24 * 60 * 60), "/");
        setcookie($this->ENID('email'),     $this->Enval($d), time() + (7 * 24 * 60 * 60), "/");
        setcookie($this->ENID('pin'),       $this->Enval($e), time() + (7 * 24 * 60 * 60), "/");
        setcookie($this->ENID('koneksi'),   $this->Enval($f), time() + (7 * 24 * 60 * 60), "/");
        setcookie($this->ENID('firstname'), $this->Enval($g), time() + (7 * 24 * 60 * 60), "/");
        setcookie($this->ENID('lastname'),  $this->Enval($h), time() + (7 * 24 * 60 * 60), "/");
    }

    function logoutSystem() {
        setcookie('login', null, time() - (365 * 24 * 60 * 60), "/");
        setcookie($this->ENID('userid'),    null, time() - (365 * 24 * 60 * 60), "/");
        setcookie($this->ENID('cmpid'),     null, time() - (365 * 24 * 60 * 60), "/");
        setcookie($this->ENID('username'),  null, time() - (365 * 24 * 60 * 60), "/");
        setcookie($this->ENID('email'),     null, time() - (365 * 24 * 60 * 60), "/");
        setcookie($this->ENID('pin'),       null, time() - (365 * 24 * 60 * 60), "/");
        setcookie($this->ENID('koneksi'),   null, time() - (365 * 24 * 60 * 60), "/");
        setcookie($this->ENID('firstname'), null, time() - (365 * 24 * 60 * 60), "/");
        setcookie($this->ENID('lastname'),  null, time() - (365 * 24 * 60 * 60), "/");
    }
}
?>