<script type="text/javascript">document.title = "Cloud Parking - Error: 404!"</script>

<div id="toolbar" class="primary-color">
    <div class="open-left" id="open-left" data-activates="slide-out-left">
        <i class="ion-android-menu"></i>
    </div>
    <span class="title bold txt-white">Error: 404!</span>
</div>

<div class="animated fadeinup delay-1">
	<div class="page-content">
		<h1 class="uppercase bold italic center txt-black">404!<br>Halaman Tidak Ditemukan!</h1>
	</div>
</div>