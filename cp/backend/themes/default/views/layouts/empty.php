<html>
<head>
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="<?php echo Yii::app()->theme->baseUrl;?>/frontend/images/p5logo.png" type="image/x-icon">

    <link href="<?php echo Yii::app()->theme->baseUrl.'/';?>frontend/css/style.css" rel="stylesheet" type="text/css" media="all" />
    <link href="<?php echo Yii::app()->theme->baseUrl.'/';?>frontend/css/form.css" rel="stylesheet" type="text/css" media="all" />
    <link href="<?php echo Yii::app()->theme->baseUrl.'/';?>backend/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all" />
    <link href="<?php echo Yii::app()->theme->baseUrl.'/';?>backend/css/font-awesome.min.css" rel="stylesheet" />

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>

    <!--Beyond styles-->
    <link href="<?php echo Yii::app()->theme->baseUrl;?>/backend/css/beyond.min.css" rel="stylesheet" />

    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl.'/';?>frontend/js/jquery.min.js"></script>

    <!-- start menu -->
    <link href="<?php echo Yii::app()->theme->baseUrl.'/';?>frontend/css/megamenu.css" rel="stylesheet" type="text/css" media="all" />
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl.'/';?>frontend/js/megamenu.js"></script>
    <script>$(document).ready(function(){$(".megamenu").megamenu();});</script>
    <!-- end menu -->
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl.'/';?>frontend/js/jquery.jscrollpane.min.js"></script>
    <script type="text/javascript" id="sourcecode">
        $(function()
        {
            $('.scroll-pane').jScrollPane();
        });
    </script>
    <!----details-product-slider--->
    <!-- Include the Etalage files -->
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl.'/';?>frontend/css/etalage.css">
    <script src="<?php echo Yii::app()->theme->baseUrl.'/';?>frontend/js/jquery.etalage.min.js"></script>
    <!-- Include the Etalage files -->
    <script>
        jQuery(document).ready(function($){

            $('#etalage').etalage({
                thumb_image_width: 300,
                thumb_image_height: 400,

                show_hint: true,
                click_callback: function(image_anchor, instance_id){
                    alert('Callback example:\nYou clicked on an image with the anchor: "'+image_anchor+'"\n(in Etalage instance: "'+instance_id+'")');
                }
            });
            // This is for the dropdown list example:
            $('.dropdownlist').change(function(){
                etalage_show( $(this).find('option:selected').attr('class') );
            });

        });
    </script>
    <!----//details-product-slider--->
    <!-- top scrolling -->
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl.'/';?>frontend/js/move-top.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl.'/';?>frontend/js/easing.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $(".scroll").click(function(event){
                event.preventDefault();
                $('html,body').animate({scrollTop:$(this.hash).offset().top},1200);
            });
        });
    </script>
</head>
<body style="background-color: white;">
<div class="header-top">
    <div class="wrap">
        <div class="row">
            <div class="col-sm-2 col-xs-12">
                <div class="logo">
                    <a href="<?php echo Yii::app()->baseUrl; ?>"><img width="100" height="60" src="<?php echo Yii::app()->theme->baseUrl.'/';?>frontend/images/p5logo.png" alt=""/></a>
                </div>
            </div>
            <div class="col-sm-6 col-xs-12" style="padding-top: 10px;">
                <div class="input-group input-group-sm">
                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                    <input type="text" class="form-control" id="input-search" placeholder="What are you looking for ?">
                    <span class="input-group-btn">
                      <button type="button" id="search" class="btn btn-info btn-flat">Search</button>
                    </span>
                </div>
            </div>
            <div class="col-sm-4 col-xs-12">
                <div class="cssmenu" style="float: right;">
                    <ul class="icon2 sub-icon2">
                        <li>
                            <a href="#"><img width="30" height="30" src="<?php echo Yii::app()->theme->baseUrl.'/frontend/images/container.png'; ?>"></a>
                            <ul class="sub-icon2 list" style="left: -115px;">
                                <?php
                                if(MyHelper::getOrder() != null){
                                    $data = MyHelper::getOrder();
                                    foreach($data as $d){
                                        ?>
                                        <a href="<?php echo Yii::app()->baseUrl.'/order/id/'.$d['orderID']; ?>"><?php echo $d['orderID'].' - '.$d['NamaToko']; ?></a>
                                    <?php }
                                } else {
                                    echo 'Empty';
                                }?>
                            </ul>
                        </li>
                        <?php if(Yii::app()->user->id == null){ ?>
                            <li><a href="<?php echo Yii::app()->baseUrl; ?>/page/register">Register</a></li>
                            <li><a href="<?php echo Yii::app()->baseUrl; ?>/page/login" class="btn btn-default btn-flat">Login</a></li>
                        <?php } else { ?>
                            <li><a href="<?php echo Yii::app()->baseUrl; ?>/page/logout">Logout</a></li>
                        <?php } ?>
                    </ul>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</div>

<div class="header-bottom" style="height: 45px;">
    <div class="wrap">
        <div id="index-category">

        </div>
    </div>
</div>

<div class="main">
    <div class="wrap">
        <div class="content-top">
            <div class="row">
                <?php
                $a = 0;
                $b = 0;
                if(isset(Yii::app()->user->id)){
                    $a = 2;
                    $b = 10;
                }
                else{
                    $a = 0;
                    $b = 12;
                }
                ?>
                <div class="col-sm-<?php echo $a; ?>">
                    <?php $this->renderPartial('//layouts/menuleft'); ?>
                </div>
                <div class="col-sm-<?php echo $b; ?>">
                    <?php echo $content; ?>
                </div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="content-bottom">

        </div>
    </div>
</div>

<div class="footer">
    <div class="footer-middle">
        <div class="wrap">
            <div class="section group">
                <div class="col-sm-3 col-xs-3">
                    <h3>P5Trade</h3>
                    <ul>
                        <li><a href="#">About Us</a></li>
                        <li><a href="#">Official Store</a></li>
                        <li><a href="#">Blog</a></li>
                    </ul>
                </div>
                <div class="col-sm-3 col-xs-3">
                    <h3>Buy</h3>
                    <ul>
                        <li><a href="#">Buy in P5Trade</a></li>
                        <li><a href="#">How to buy</a></li>
                        <li><a href="#">Payment</a></li>
                    </ul>
                </div>
                <div class="col-sm-3 col-xs-3">
                    <h3>Sell</h3>
                    <ul>
                        <li><a href="#">Buy in P5Trade</a></li>
                        <li><a href="#">How to sell</a></li>
                        <li><a href="#">Open store</a></li>
                        <li><a href="#">Payment</a></li>
                    </ul>
                </div>
                <div class="col-sm-3 col-xs-3">
                    <h3>Help</h3>
                    <ul>
                        <li><a href="#">Privacy Policy</a></li>
                        <li><a href="#">Terms Condition</a></li>
                        <li><a href="#">Contact Us</a></li>
                    </ul>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <div class="clear"></div>
    </div>

    <div class="footer-bottom">
        <div class="wrap">
            <div class="col-sm-12 col-xs-12">
                <div class="pull-left">
                    <img width="100" height="60" src="<?php echo Yii::app()->theme->baseUrl.'/';?>frontend/images/p5logo.png" alt=""/>
                    &copy; 2018, <a href="http://p5trade.com">p5trade.com</a>
                    <br>
                    <br>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl.'/';?>backend/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl.'/';?>backend/js/beyond.min.js"></script>

<!--Bootstrap Date Picker-->
<script src="<?php echo Yii::app()->theme->baseUrl.'/';?>backend/js/datetime/bootstrap-datepicker.js"></script>

<!--Jquery Select2-->
<script src="<?php echo Yii::app()->theme->baseUrl.'/';?>backend/js/select2/select2.js"></script>

<script type="text/javascript">
    $(".select2").select2();

    //--Bootstrap Date Picker--
    $('.date-picker').datepicker();

    $('.search-button').click(function(){
        $('.search-form').show();
    });

    $(document).ready(function() {

        $.ajax({
            type: 'post',
            url: '<?php echo Yii::app()->request->baseUrl ."/home/loadCategory"; ?>',
            data: {},
            success: function(response) {
                $('#index-category').html(response);
            }
        });

        $.ajax({
            type: 'post',
            url: '<?php echo Yii::app()->request->baseUrl ."/home/loadMember"; ?>',
            data: {},
            success: function(response) {
                $('#index-member').html(response);
            }
        });

        var defaults = {
            containerID: 'toTop', // fading element id
            containerHoverID: 'toTopHover', // fading element hover id
            scrollSpeed: 1200,
            easingType: 'linear'
        };


        $().UItoTop({ easingType: 'easeOutQuart' });

    });
</script>
<a href="#" id="toTop" style="display: block;"><span id="toTopHover" style="opacity: 1;"></span></a>
</body>
</html>
