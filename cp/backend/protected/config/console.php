<?php

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'My Web App',

	// autoloading model and component classes
	'import'=>array(
		'application.commands.*',
		'application.components.*',
		'application.models.*',
	),
);
