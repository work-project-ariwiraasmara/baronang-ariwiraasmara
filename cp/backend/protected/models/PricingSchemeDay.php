<?php

/**
 * This is the model class for table "PricingSchemeDay".
 *
 * The followings are the available columns in table 'PricingSchemeDay':
 * @property string $SchemeDayID
 * @property string $DayName
 * @property integer $ParkInHour
 * @property string $PricingSchemeID
 * @property string $VehicleID
 */
class PricingSchemeDay extends MyActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		if(Yii::app()->user->getState('database') != ''){
			return Yii::app()->user->getState('database').'.PricingSchemeDay';
		}
		else{
			return 'PricingSchemeDay';
		}
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('VehicleID', 'required'),
			array('ParkInHour', 'numerical', 'integerOnly'=>true),
			array('SchemeDayID, DayName, PricingSchemeID, VehicleID', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('SchemeDayID, DayName, ParkInHour, PricingSchemeID, VehicleID', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'SchemeDayID' => 'Scheme Day',
			'DayName' => 'Day Name',
			'ParkInHour' => 'Park In Hour',
			'PricingSchemeID' => 'Pricing Scheme',
			'VehicleID' => 'Vehicle',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('SchemeDayID',$this->SchemeDayID,true);
		$criteria->compare('DayName',$this->DayName,true);
		$criteria->compare('ParkInHour',$this->ParkInHour);
		$criteria->compare('PricingSchemeID',$this->PricingSchemeID,true);
		$criteria->compare('VehicleID',$this->VehicleID,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PricingSchemeDay the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
