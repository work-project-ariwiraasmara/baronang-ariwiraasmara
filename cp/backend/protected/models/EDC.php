<?php

/**
 * This is the model class for table "EDC".
 *
 * The followings are the available columns in table 'EDC':
 * @property string $EDCID
 * @property string $CompanyID
 * @property string $SN
 * @property string $Name
 * @property integer $Status
 * @property double $Balance
 * @property string $Pin
 * @property string $DeviceID
 * @property string $LastLogin
 * @property string $CreatedDate
 * @property integer $CreatedBy
 * @property string $ModifiedDate
 * @property integer $ModifiedBy
 */
class EDC extends MyActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'EDC';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('EDCID, CompanyID, SN, Name, Pin, DeviceID, LastLogin, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy', 'required'),
			array('Status, CreatedBy, ModifiedBy', 'numerical', 'integerOnly'=>true),
			array('Balance', 'numerical'),
			array('EDCID, CompanyID, SN', 'length', 'max'=>20),
			array('Name, Pin, DeviceID', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('EDCID, CompanyID, SN, Name, Status, Balance, Pin, DeviceID, LastLogin, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'EDCID' => 'Edcid',
			'CompanyID' => 'Company',
			'SN' => 'Sn',
			'Name' => 'Name',
			'Status' => 'Status',
			'Balance' => 'Balance',
			'Pin' => 'Pin',
			'DeviceID' => 'Device',
			'LastLogin' => 'Last Login',
			'CreatedDate' => 'Created Date',
			'CreatedBy' => 'Created By',
			'ModifiedDate' => 'Modified Date',
			'ModifiedBy' => 'Modified By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('EDCID',$this->EDCID,true);
		$criteria->compare('CompanyID',$this->CompanyID,true);
		$criteria->compare('SN',$this->SN,true);
		$criteria->compare('Name',$this->Name,true);
		$criteria->compare('Status',$this->Status);
		$criteria->compare('Balance',$this->Balance);
		$criteria->compare('Pin',$this->Pin,true);
		$criteria->compare('DeviceID',$this->DeviceID,true);
		$criteria->compare('LastLogin',$this->LastLogin,true);
		$criteria->compare('CreatedDate',$this->CreatedDate,true);
		$criteria->compare('CreatedBy',$this->CreatedBy);
		$criteria->compare('ModifiedDate',$this->ModifiedDate,true);
		$criteria->compare('ModifiedBy',$this->ModifiedBy);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return EDC the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
