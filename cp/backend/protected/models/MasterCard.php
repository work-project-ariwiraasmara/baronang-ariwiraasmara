<?php

/**
 * This is the model class for table "mastercard".
 *
 * The followings are the available columns in table 'mastercard':
 * @property string $CardNo
 * @property string $RFIDNo
 * @property string $Barcode
 * @property string $DateCreate
 * @property integer $Status
 * @property string $Background
 * @property string $CardID
 */
class MasterCard extends MyActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		if(Yii::app()->user->getState('database') != ''){
			return Yii::app()->user->getState('database').'.MasterCard';
		}
		else{
			return 'MasterCard';
		}
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Status', 'numerical', 'integerOnly'=>true),
			array('CardNo, RFIDNo, Barcode, Background, CardID', 'length', 'max'=>255),
			array('DateCreate', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('CardNo, RFIDNo, Barcode, DateCreate, Status, Background, CardID', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'CardNo' => 'Card No',
			'RFIDNo' => 'Rfidno',
			'Barcode' => 'Barcode',
			'DateCreate' => 'Date Create',
			'Status' => 'Status',
			'Background' => 'Background',
			'CardID' => 'Card',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('CardNo',$this->CardNo,true);
		$criteria->compare('RFIDNo',$this->RFIDNo,true);
		$criteria->compare('Barcode',$this->Barcode,true);
		$criteria->compare('DateCreate',$this->DateCreate,true);
		$criteria->compare('Status',$this->Status);
		$criteria->compare('Background',$this->Background,true);
		$criteria->compare('CardID',$this->CardID,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MasterCard the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
