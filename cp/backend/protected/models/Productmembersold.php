<?php

/**
 * This is the model class for table "productmembersold".
 *
 * The followings are the available columns in table 'productmembersold':
 * @property string $ProductID
 * @property string $MemberID
 * @property string $CardNo
 * @property string $BuyDate
 * @property integer $ID
 */
class Productmembersold extends MyActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		if(Yii::app()->user->getState('database') != ''){
			return Yii::app()->user->getState('database').'.Productmembersold';
		}
		else{
			return 'Productmembersold';
		}
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ProductID, MemberID, CardNo', 'length', 'max'=>255),
			array('BuyDate', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ProductID, MemberID, CardNo, BuyDate, ID', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ProductID' => 'Product',
			'MemberID' => 'Member',
			'CardNo' => 'Card No',
			'BuyDate' => 'Buy Date',
			'ID' => 'ID',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ProductID',$this->ProductID,true);
		$criteria->compare('MemberID',$this->MemberID,true);
		$criteria->compare('CardNo',$this->CardNo,true);
		$criteria->compare('BuyDate',$this->BuyDate,true);
		$criteria->compare('ID',$this->ID);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Productmembersold the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
