<?php

/**
 * This is the model class for table "locationparkingproducttopup".
 *
 * The followings are the available columns in table 'locationparkingproducttopup':
 * @property string $ProductID
 * @property string $Nama
 * @property string $Deskripsi
 * @property integer $Status
 * @property integer $JumlahPoinAdd
 * @property integer $JumlahPoinBonus
 * @property double $Harga
 */
class Locationparkingproducttopup extends MyActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		if(Yii::app()->user->getState('database') != ''){
			return Yii::app()->user->getState('database').'.Locationparkingproducttopup';
		}
		else{
			return 'Locationparkingproducttopup';
		}
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ProductID', 'required'),
			array('Status, JumlahPoinAdd, JumlahPoinBonus', 'numerical', 'integerOnly'=>true),
			array('Harga', 'numerical'),
			array('ProductID, Nama', 'length', 'max'=>255),
			array('Deskripsi', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ProductID, Nama, Deskripsi, Status, JumlahPoinAdd, JumlahPoinBonus, Harga', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ProductID' => 'Product',
			'Nama' => 'Nama',
			'Deskripsi' => 'Deskripsi',
			'Status' => 'Status',
			'JumlahPoinAdd' => 'Jumlah Poin Add',
			'JumlahPoinBonus' => 'Jumlah Poin Bonus',
			'Harga' => 'Harga',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ProductID',$this->ProductID,true);
		$criteria->compare('Nama',$this->Nama,true);
		$criteria->compare('Deskripsi',$this->Deskripsi,true);
		$criteria->compare('Status',$this->Status);
		$criteria->compare('JumlahPoinAdd',$this->JumlahPoinAdd);
		$criteria->compare('JumlahPoinBonus',$this->JumlahPoinBonus);
		$criteria->compare('Harga',$this->Harga);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Locationparkingproducttopup the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
