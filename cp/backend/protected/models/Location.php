<?php

/**
 * This is the model class for table "location".
 *
 * The followings are the available columns in table 'location':
 * @property string $LocationID
 * @property string $ReferenceID
 * @property string $Nama
 * @property string $Alamat
 * @property string $Telepon
 * @property integer $Status
 * @property string $Lon
 * @property string $Lat
 * @property string $Deskripsi
 * @property string $Gambar
 * @property string $Code
 * @property double $Balance
 * @property integer $GP
 * @property integer $isOnlyMember
 * @property integer $isFree
 * @property string $OvoLocationID
 * @property string $OvoCompanyCode
 */
class Location extends MyActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		if(Yii::app()->user->getState('database') != ''){
			return Yii::app()->user->getState('database').'.Location';
		}
		else{
			return 'Location';
		}
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('GP, OvoLocationID, OvoCompanyCode', 'required'),
			array('Status, GP, isOnlyMember, isFree', 'numerical', 'integerOnly'=>true),
			array('Balance', 'numerical'),
			array('LocationID, ReferenceID, Nama, Alamat, Telepon, Lon, Lat, Deskripsi, Gambar', 'length', 'max'=>255),
			array('Code', 'length', 'max'=>3),
			array('OvoLocationID, OvoCompanyCode', 'length', 'max'=>15),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('LocationID, ReferenceID, Nama, Alamat, Telepon, Status, Lon, Lat, Deskripsi, Gambar, Code, Balance, GP, isOnlyMember, isFree, OvoLocationID, OvoCompanyCode', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'LocationID' => 'Location',
			'ReferenceID' => 'Reference',
			'Nama' => 'Nama',
			'Alamat' => 'Alamat',
			'Telepon' => 'Telepon',
			'Status' => 'Status',
			'Lon' => 'Lon',
			'Lat' => 'Lat',
			'Deskripsi' => 'Deskripsi',
			'Gambar' => 'Gambar',
			'Code' => 'Code',
			'Balance' => 'Balance',
			'GP' => 'Gp',
			'isOnlyMember' => 'Is Only Member',
			'isFree' => 'Is Free',
			'OvoLocationID' => 'Ovo Location',
			'OvoCompanyCode' => 'Ovo Company Code',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('LocationID',$this->LocationID,true);
		$criteria->compare('ReferenceID',$this->ReferenceID,true);
		$criteria->compare('Nama',$this->Nama,true);
		$criteria->compare('Alamat',$this->Alamat,true);
		$criteria->compare('Telepon',$this->Telepon,true);
		$criteria->compare('Status',$this->Status);
		$criteria->compare('Lon',$this->Lon,true);
		$criteria->compare('Lat',$this->Lat,true);
		$criteria->compare('Deskripsi',$this->Deskripsi,true);
		$criteria->compare('Gambar',$this->Gambar,true);
		$criteria->compare('Code',$this->Code,true);
		$criteria->compare('Balance',$this->Balance);
		$criteria->compare('GP',$this->GP);
		$criteria->compare('isOnlyMember',$this->isOnlyMember);
		$criteria->compare('isFree',$this->isFree);
		$criteria->compare('OvoLocationID',$this->OvoLocationID,true);
		$criteria->compare('OvoCompanyCode',$this->OvoCompanyCode,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Location the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
