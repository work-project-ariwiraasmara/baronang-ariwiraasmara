<?php

/**
 * This is the model class for table "vehicletype".
 *
 * The followings are the available columns in table 'vehicletype':
 * @property string $VehicleID
 * @property string $Name
 * @property integer $Status
 * @property integer $isVisible
 * @property string $Code
 */
class VehicleType extends MyActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		if(Yii::app()->user->getState('database') != ''){
			return Yii::app()->user->getState('database').'.VehicleType';
		}
		else{
			return 'VehicleType';
		}
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Status, isVisible', 'numerical', 'integerOnly'=>true),
			array('VehicleID, Name, Code', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('VehicleID, Name, Status, isVisible, Code', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'VehicleID' => 'Vehicle',
			'Name' => 'Name',
			'Status' => 'Status',
			'isVisible' => 'Is Visible',
			'Code' => 'Code',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('VehicleID',$this->VehicleID,true);
		$criteria->compare('Name',$this->Name,true);
		$criteria->compare('Status',$this->Status);
		$criteria->compare('isVisible',$this->isVisible);
		$criteria->compare('Code',$this->Code,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return VehicleType the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
