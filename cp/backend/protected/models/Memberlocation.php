<?php

/**
 * This is the model class for table "memberlocation".
 *
 * The followings are the available columns in table 'memberlocation':
 * @property integer $ID
 * @property string $MemberID
 * @property string $LocationID
 * @property integer $Status
 * @property string $DateRegister
 * @property string $ValidDate
 * @property string $VehicleID
 * @property string $CardNo
 */
class Memberlocation extends MyActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		if(Yii::app()->user->getState('database') != ''){
			return Yii::app()->user->getState('database').'.MemberLocation';
		}
		else{
			return 'MemberLocation';
		}
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Status', 'numerical', 'integerOnly'=>true),
			array('MemberID, LocationID, VehicleID, CardNo', 'length', 'max'=>255),
			array('DateRegister, ValidDate', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID, MemberID, LocationID, Status, DateRegister, ValidDate, VehicleID, CardNo', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'MemberID' => 'Member',
			'LocationID' => 'Location',
			'Status' => 'Status',
			'DateRegister' => 'Date Register',
			'ValidDate' => 'Valid Date',
			'VehicleID' => 'Vehicle',
			'CardNo' => 'Card No',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('MemberID',$this->MemberID,true);
		$criteria->compare('LocationID',$this->LocationID,true);
		$criteria->compare('Status',$this->Status);
		$criteria->compare('DateRegister',$this->DateRegister,true);
		$criteria->compare('ValidDate',$this->ValidDate,true);
		$criteria->compare('VehicleID',$this->VehicleID,true);
		$criteria->compare('CardNo',$this->CardNo,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Memberlocation the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
