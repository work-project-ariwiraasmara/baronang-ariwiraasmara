<?php

/**
 * This is the model class for table "memberlist".
 *
 * The followings are the available columns in table 'memberlist':
 * @property string $MemberID
 * @property string $Name
 * @property string $Addr
 * @property string $Phone
 * @property string $Email
 * @property integer $StatusMember
 * @property string $KTP
 * @property string $NIP
 * @property string $Reference
 */
class MemberList extends MyActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		if(Yii::app()->user->getState('database') != ''){
			return Yii::app()->user->getState('database').'.MemberList';
		}
		else{
			return 'MemberList';
		}
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('StatusMember', 'numerical', 'integerOnly'=>true),
			array('MemberID, Name, Addr, Phone, Email, Reference', 'length', 'max'=>255),
			array('KTP', 'length', 'max'=>30),
			array('NIP', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('MemberID, Name, Addr, Phone, Email, StatusMember, KTP, NIP, Reference', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'MemberID' => 'Member',
			'Name' => 'Name',
			'Addr' => 'Addr',
			'Phone' => 'Phone',
			'Email' => 'Email',
			'StatusMember' => 'Status Member',
			'KTP' => 'Ktp',
			'NIP' => 'Nip',
			'Reference' => 'Reference',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('MemberID',$this->MemberID,true);
		$criteria->compare('Name',$this->Name,true);
		$criteria->compare('Addr',$this->Addr,true);
		$criteria->compare('Phone',$this->Phone,true);
		$criteria->compare('Email',$this->Email,true);
		$criteria->compare('StatusMember',$this->StatusMember);
		$criteria->compare('KTP',$this->KTP,true);
		$criteria->compare('NIP',$this->NIP,true);
		$criteria->compare('Reference',$this->Reference,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MemberList the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
