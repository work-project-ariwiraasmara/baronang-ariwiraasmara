<?php

/**
 * This is the model class for table "locationparkingproduct".
 *
 * The followings are the available columns in table 'locationparkingproduct':
 * @property string $LocationID
 * @property string $ProductID
 * @property string $Nama
 * @property string $Deskripsi
 * @property integer $Status
 * @property integer $JumlahPoinDeduct
 * @property integer $Jumlah
 * @property double $Harga
 * @property string $Gambar
 * @property string $VehicleID
 * @property integer $Tipe
 * @property string $Keterangan
 */
class Locationparkingproduct extends MyActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		if(Yii::app()->user->getState('database') != ''){
			return Yii::app()->user->getState('database').'.Locationparkingproduct';
		}
		else{
			return 'Locationparkingproduct';
		}
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('LocationID, ProductID', 'required'),
			array('Status, JumlahPoinDeduct, Jumlah, Tipe', 'numerical', 'integerOnly'=>true),
			array('Harga', 'numerical'),
			array('LocationID, ProductID, Nama, Gambar, VehicleID, Keterangan', 'length', 'max'=>255),
			array('Deskripsi', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('LocationID, ProductID, Nama, Deskripsi, Status, JumlahPoinDeduct, Jumlah, Harga, Gambar, VehicleID, Tipe, Keterangan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'LocationID' => 'Location',
			'ProductID' => 'Product',
			'Nama' => 'Nama',
			'Deskripsi' => 'Deskripsi',
			'Status' => 'Status',
			'JumlahPoinDeduct' => 'Jumlah Poin Deduct',
			'Jumlah' => 'Jumlah',
			'Harga' => 'Harga',
			'Gambar' => 'Gambar',
			'VehicleID' => 'Vehicle',
			'Tipe' => 'Tipe',
			'Keterangan' => 'Keterangan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('LocationID',$this->LocationID,true);
		$criteria->compare('ProductID',$this->ProductID,true);
		$criteria->compare('Nama',$this->Nama,true);
		$criteria->compare('Deskripsi',$this->Deskripsi,true);
		$criteria->compare('Status',$this->Status);
		$criteria->compare('JumlahPoinDeduct',$this->JumlahPoinDeduct);
		$criteria->compare('Jumlah',$this->Jumlah);
		$criteria->compare('Harga',$this->Harga);
		$criteria->compare('Gambar',$this->Gambar,true);
		$criteria->compare('VehicleID',$this->VehicleID,true);
		$criteria->compare('Tipe',$this->Tipe);
		$criteria->compare('Keterangan',$this->Keterangan,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Locationparkingproduct the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
