<?php

/**
 * This is the model class for table "locationparkingproductdiscount".
 *
 * The followings are the available columns in table 'locationparkingproductdiscount':
 * @property string $EndDate
 * @property string $ProductID
 * @property string $StartDate
 * @property string $CreatedDate
 * @property double $Amount
 * @property integer $Status
 */
class Locationparkingproductdiscount extends MyActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		if(Yii::app()->user->getState('database') != ''){
			return Yii::app()->user->getState('database').'.Locationparkingproductdiscount';
		}
		else{
			return 'Locationparkingproductdiscount';
		}
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Status', 'numerical', 'integerOnly'=>true),
			array('Amount', 'numerical'),
			array('ProductID', 'length', 'max'=>255),
			array('EndDate, StartDate, CreatedDate', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('EndDate, ProductID, StartDate, CreatedDate, Amount, Status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'EndDate' => 'End Date',
			'ProductID' => 'Product',
			'StartDate' => 'Start Date',
			'CreatedDate' => 'Created Date',
			'Amount' => 'Amount',
			'Status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('EndDate',$this->EndDate,true);
		$criteria->compare('ProductID',$this->ProductID,true);
		$criteria->compare('StartDate',$this->StartDate,true);
		$criteria->compare('CreatedDate',$this->CreatedDate,true);
		$criteria->compare('Amount',$this->Amount);
		$criteria->compare('Status',$this->Status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Locationparkingproductdiscount the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
