<?php

/**
 * This is the model class for table "users".
 *
 * The followings are the available columns in table 'users':
 * @property string $UserID
 * @property string $CompanyID
 * @property string $Username
 * @property string $FirstName
 * @property string $LastName
 * @property string $Phone
 * @property string $Email
 * @property string $Address
 * @property string $Password
 * @property string $PIN
 * @property integer $Status
 * @property string $DeviceID
 * @property string $LastLogin
 * @property integer $SessionKey
 * @property string $SessionExpired
 * @property string $CreatedBy
 * @property string $CreatedDate
 * @property string $ModifiedBy
 * @property string $ModifiedDate
 */
class Users extends MyActiveRecord
{

	// status
	const STATUS_NOTACTIVE = 0; //not active
	const STATUS_ACTIVE = 1; //active
	const STATUS_BANNED = 9; //banned
	const STATUS_LOCKED = 2; //locked

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
			return 'Users';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('CompanyID, Username, FirstName, LastName, Phone, Email, Address, Password', 'required'),
			array('Status', 'numerical', 'integerOnly'=>true),
			array('CompanyID, Username, Phone, CreatedBy', 'length', 'max'=>20),
			array('FirstName, LastName, Password', 'length', 'max'=>255),
			array('Email', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('CompanyID, Username, FirstName, LastName, Phone, Email, Address, Password, Status, LastLogin, SessionKey, SessionExpired, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'userCreate' => array(self::BELONGS_TO, 'Users', 'CreatedBy'),
			'userModify' => array(self::BELONGS_TO, 'Users', 'ModifiedBy'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'CompanyID' => 'CompanyID',
			'Username' => 'Username',
			'Firstname' => 'Firstname',
			'Lastname' => 'Lastname',
			'Phone' => 'Phone',
			'Email' => 'Email',
			'Address' => 'Address',
			'Password' => 'Password',
			'Status' => 'Status',
			'LastLogin' => 'Lastlogin',
			'SessionKey' => 'Sessionkey',
			'SessionExpired' => 'Sessionexpired',
			'CreatedBy' => 'CreatedBy',
			'CreatedDate' => 'CreatedDate',
			'ModifiedBy' => 'ModifiedBy',
			'ModifiedDate' => 'ModifiedDate',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('CompanyID',$this->CompanyID,true);
		$criteria->compare('Username',$this->Username,true);
		$criteria->compare('FirstName',$this->FirstName,true);
		$criteria->compare('LastName',$this->LastName,true);
		$criteria->compare('Phone',$this->Phone,true);
		$criteria->compare('Email',$this->Email,true);
		$criteria->compare('Address',$this->Address,true);
		$criteria->compare('Password',$this->Password,true);
		$criteria->compare('Status',$this->Status);
		$criteria->compare('LastLogin',$this->LastLogin,true);
		$criteria->compare('SessionKey',$this->SessionKey);
		$criteria->compare('SessionExpired',$this->SessionExpired,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Users the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
