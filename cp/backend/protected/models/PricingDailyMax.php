<?php

/**
 * This is the model class for table "PricingDailyMax".
 *
 * The followings are the available columns in table 'PricingDailyMax':
 * @property string $MaxID
 * @property string $DayName
 * @property integer $MaxPrice
 * @property string $VehicleID
 */
class PricingDailyMax extends MyActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
			if(Yii::app()->user->getState('database') != ''){
				return Yii::app()->user->getState('database').'.PricingDailyMax';
			}
			else{
				return 'PricingDailyMax';
			}
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('MaxID, DayName, MaxPrice, VehicleID', 'required'),
			array('MaxPrice', 'numerical', 'integerOnly'=>true),
			array('MaxID, DayName, VehicleID', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('MaxID, DayName, MaxPrice, VehicleID', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'MaxID' => 'Max',
			'DayName' => 'Day Name',
			'MaxPrice' => 'Max Price',
			'VehicleID' => 'Vehicle',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('MaxID',$this->MaxID,true);
		$criteria->compare('DayName',$this->DayName,true);
		$criteria->compare('MaxPrice',$this->MaxPrice);
		$criteria->compare('VehicleID',$this->VehicleID,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PricingDailyMax the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
