<?php

class AdminForm extends CFormModel
{
	public $cid;
	public $username;
	public $password;
	public $rememberMe;

	private $_identity;

	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return array(
			// username and password are required
			array('cid, username, password', 'required'),
			// rememberMe needs to be a boolean
			array('rememberMe', 'boolean'),
			// password needs to be authenticated
			array('password', 'authenticate'),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'rememberMe'=>'Remember me next time',
		);
	}

	/**
	 * Authenticates the password.
	 * This is the 'authenticate' validator as declared in rules().
	 */
	public function authenticate($attribute,$params)
	{
		if(!$this->hasErrors())
		{
				$this->_identity=new AdminIdentity($this->cid,$this->username,$this->password);
				if(!$this->_identity->authenticate()){
						$this->addError('password','Incorrect username or password.');
				}
		}
	}

	/**
	 * Logs in the user using the given username and password in the model.
	 * @return boolean whether login is successful
	 */
	public function login()
	{
		if($this->_identity===null)
		{
			$this->_identity=new AdminIdentity($this->cid,$this->username,$this->password);
			$this->_identity->authenticate();
		}

		if($this->_identity->errorCode===AdminIdentity::ERROR_NONE)
		{
				$duration = $this->rememberMe ? 60*30 : 60*60; // 60 minutes
				Yii::app()->user->login($this->_identity,$duration);

				$session_key = md5(time());
        $session_expired = $duration + time();

        // update last_login & status_login
        $user = Users::model()->findByAttributes(array('Username'=>$this->username, 'CompanyID'=>$this->cid));
				if($user != null){
						$user->scenario = 'login';
	          $user->LastLogin = DateHelper::now();
	          $user->SessionKey = $session_key;
	          $user->SessionExpired = date('Y-m-d H:i:s', $session_expired);
	          $user->save();

						$koneksi = Koneksi::model()->findByAttributes(array('CompanyID'=>$this->cid, 'Status'=>'1'));
						if($koneksi != null){
								Yii::app()->user->setState('cid', $this->cid);
								Yii::app()->user->setState('database', $koneksi->DB);
						}
				}

				Yii::app()->user->setState('session_key', $session_key);
        Yii::app()->user->setState('session_expired', $session_expired);

				return true;
		}
		else
			return false;
	}
}
