<?php

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 * */
class AdminController extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/main';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();

	/**
	 * Initialize
	 */
	public function init()
	{
		parent::init();

		// authorize user login
		if (!$this->isAuthorized()) {
			$this->redirect(array('/login'));
			Yii::app()->end();
		}
	}

	/**
	 * @return bool
	 */
	public function isAuthorized()
	{
			if ($this->id != 'login' && $this->id != 'logout' && $this->id != 'redirect' && $this->id != 'error') {
					$user = MyHelper::getLogin();
          if ($user != null && $user->Status != Users::STATUS_LOCKED && $user->Status != Users::STATUS_BANNED) {
              // check session key
              if (Yii::app()->user->getState('session_key') != $user->SessionKey) {
                  $this->redirect(array('/redirect/multipleLogin'));
                  Yii::app()->end();
              }

              // force logout if time has expired
              if(Yii::app()->user->getState('session_expired') < time()){
                  $this->redirect(array('/logout'));
                  Yii::app()->end();
              }

              return true;
          }
    	}
			else
			{
	        return true;
			}

			// destroy session
			Yii::app()->user->logout();
			return false;
	}
}
