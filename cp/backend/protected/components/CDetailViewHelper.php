<?php

class CDetailViewHelper
{
    public static function getStatus($model)
    {
        return array(
            'label'=>'Status',
            'type'=>'raw',
            'value'=>EnumStatus::getLabel($model->status),
        );
    }

    public static function getCreatedBy($model)
    {
    	return self::getUser('CreatedBy', 'userCreate', $model);
    }

    public static function getModifiedBy($model)
    {
    	return self::getUser('ModifiedBy', 'userModify', $model);
    }

    public static function getCreatedDate($model)
    {
    	return self::getDate('CreatedDate', $model);
    }

    public static function getModifiedDate($model)
    {
    	return self::getDate('ModifiedDate', $model);
    }

    public static function getUser($attr_name, $relation, $model)
    {
    	$value = null;
    	if ($model->$attr_name === '0') {
    		$value = "System";
    	} else if ($model->$relation != null) {
    		if ($model->$relation->userLogin != null) {
    			$value = $model->$relation->userLogin->username;
    		}
    	}

        return array(
            'name'=>$attr_name,
            'type'=>'raw',
            'value'=>$value
        );
    }

    public static function getDate($attr_name, $model)
    {
    	if($model->$attr_name == NULL){
            $new_date = NULL;
        }else{
            $time = strtotime($model->$attr_name);
            $new_date = date('d-M-Y H:i:s', $time);
        }

        return array(
            'name'=>$attr_name,
            'type'=>'raw',
            'value'=>$new_date,
        );
    }

    public static function getLink($label, $url=array())
    {
    	return array(
			'type'=>'raw',
			'value'=>CHtml::link($label, $url, array(
    			'class'=>'detail-view-link',
    		)),
		);
    }
}

?>
