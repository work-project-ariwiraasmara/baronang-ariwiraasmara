<?php

class AdminIdentity extends CUserIdentity
{
    public $cid;
    public $username;
    public $password;
    private $_id;

    /**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
      $user = Users::model()->findByAttributes(array('Username'=>$this->username, 'CompanyID'=>$this->cid));
  		if($user === null) {
  			  $this->errorCode=self::ERROR_USERNAME_INVALID;
      }
      else {
          $company = Company::model()->findByAttributes(array('CompanyID'=>$this->cid, 'Status'=>'1'));
          if($company === null){
              $this->errorCode=self::ERROR_CID_INVALID;
          }
          else{
              if($user->Password != md5($this->password)) {
                  $this->errorCode=self::ERROR_PASSWORD_INVALID;
              }
              else {
                  $this->errorCode=self::ERROR_NONE;
                  $this->_id = $user->UserID;
              }
          }
      }

  		return !$this->errorCode;
	}

    public function getId()
    {
        return $this->_id;
    }
}
