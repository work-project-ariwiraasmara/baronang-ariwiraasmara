<?php

class PricingSchemeDayController extends AdminController
{
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionAdd()
	{
		$model=new PricingSchemeDay;
		$modelWeek = PricingDailyMax::model()->findAll();

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['PricingSchemeDay']))
		{
			$model->attributes=$_POST['PricingSchemeDay'];

			if($model->save()){
				$this->redirect(array('view','id'=>$model->SchemeDayID));
            }
		}

		$this->render('add',array(
			'model'=>$model,
			'modelWeek'=>$modelWeek,
		));
	}

	public function actionChooseTime()
	{
		if (Yii::app()->request->isAjaxRequest) {
			$json = array(
					'success'=>false,
					'content'=>'',
					'message'=>'',
			);

			if(isset($_POST['days'])){
					$content = $this->renderPartial('_formChooseTime', array(
							'days'=>$_POST['days'],
					), true);
					$json['content'] = $content;
					$json['success'] = true;
			}
			else{
					$json['message'] = 'Invalid require data';
			}

			echo CJSON::encode($json);
			Yii::app()->end();
		}
	}

	public function actionSelectScheme()
	{
		if (Yii::app()->request->isAjaxRequest) {
			$json = array(
					'success'=>false,
					'content'=>'',
					'message'=>'',
			);

			if(isset($_POST['days']) and isset($_POST['times'])){
					$model=new PricingSchemeDay;

					$content = $this->renderPartial('_formSelectScheme', array(
							'model'=>$model,
							'days'=>$_POST['days'],
							'times'=>$_POST['times'],
					), true);
					$json['content'] = $content;
					$json['success'] = true;
			}
			else{
					$json['message'] = 'Invalid require data';
			}

			echo CJSON::encode($json);
			Yii::app()->end();
		}
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['PricingSchemeDay']))
		{
			$model->attributes=$_POST['PricingSchemeDay'];

			if($model->save()){
				$this->redirect(array('view','id'=>$model->SchemeDayID));
            }
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Deletes a list of model.
	 */
	public function actionDeleteSelected()
	{
		if(Yii::app()->request->isPostRequest && isset($_POST['ids']))
		{
			// delete
			foreach ($_POST['ids'] as $id) {
				$this->loadModel($id)->delete();
			}
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new PricingSchemeDay('search');
		$model->unsetAttributes();  // clear any default values

		if(isset($_GET['PricingSchemeDay'])){
			$model->attributes=$_GET['PricingSchemeDay'];
		}

		// array data
		$event = array();

		$arr = array(
			'title'=>'Meeting',
			'start'=>'2019-06-12T10:30:00',
			'url'=>'http://google.com/',
			'end'=>'2019-06-12T12:30:00'
		);

		array_push($event, $arr);

		/*foreach ($giroPenjualan as $key => $gj) {
				foreach($gj->pembayarans as $data){
						$arrGj = array(
								'title'=>$data->penjualan->pelanggan->nama,
								'start'=>$data->pembayaranGiro->tanggal_giro,
								'description'=>'Total : '.number_format($data->pembayaranGiro->jumlah_giro).'<br />'.
										'Bank : '.$data->pembayaranGiro->bank->nama. '<br />'.
										'Cabang : '.$data->pembayaranGiro->bank->cabang
						);

						array_push($eventGJ['events'], $arrGj);
				}
		}*/

		$result_json = json_encode($event);

		$this->render('index',array(
			'model'=>$model,
			'data'=>$result_json,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=PricingSchemeDay::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='pricing-scheme-day-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
