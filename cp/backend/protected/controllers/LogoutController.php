<?php

class LogoutController extends AdminController
{
    /**
     * Clear user sessions and redirect to login page
     */
    public function actionIndex()
    {
        // change status_login
        $user = MyHelper::getLogin();
	    if ($user != null) {
	        $user->scenario = 'logout';
	        $user->save();
	    }

        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }
}