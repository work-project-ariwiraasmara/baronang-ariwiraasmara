<?php

class RedirectController extends AdminController
{
	public $layout = '//layouts/redirect';
	public $time = 5;
	public $url = '';

	public function actionMultipleLogin()
	{
		$this->time = 3;
		$this->url = $this->createUrl('/logout');
		$this->render('index');
	}
	
	
}