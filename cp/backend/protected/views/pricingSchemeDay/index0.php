<div>
	<h1 class="left">Manage Pricing Scheme Days</h1>
	<div class="form-button-container">

  </div>
</div>
<div class="clear"></div>
<hr />

<div class="alert alert-info fade in radius-bordered alert-shadowed">
    <button class="close" data-dismiss="alert">×</button>
    <strong>Info!</strong> Drag or click column bottom
</div>

<?php echo $data; ?>

<div id="wrapper">
  <div id="calendar"></div>
</div>

<script>
function formatTime(date) {
     var d = new Date(date),
         hour = d.getHours(),
         minute = d.getMinutes(),
         second = d.getSeconds();

     return [hour, minute, second].join(':');
 }

function formatDate(date) {
     var d = new Date(date),
         month = '' + (d.getMonth() + 1),
         day = '' + d.getDate(),
         year = d.getFullYear();

     if (month.length < 2) month = '0' + month;
     if (day.length < 2) day = '0' + day;

     return [year, month, day].join('-');
 }

  document.addEventListener('DOMContentLoaded', function() {
    var calendarEl = document.getElementById('calendar');

    var calendar = new FullCalendar.Calendar(calendarEl, {
      plugins: [ 'interaction', 'dayGrid', 'timeGrid' ],
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'dayGridMonth,timeGridWeek,timeGridDay'
      },
      defaultDate: '<?php echo date('Y-m-d'); ?>',
      selectable: true,
      select: function(arg) {
        alert(formatTime(arg.start));
        alert(arg.end);

        var title = prompt('Masukan Event:');
        if (title) {
          calendar.addEvent({
            title: title,
            start: arg.start,
            end: arg.end,
            allDay: arg.allDay
          })
        }
        calendar.unselect()
      },
      eventLimit: "more", // allow "more" link when too many events
      events: <?php echo $data ?>
    });

    calendar.render();
  });

</script>
