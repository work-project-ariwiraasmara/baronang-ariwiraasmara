<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title" id="myLargeModalLabel">Choose Time</h4>
</div>
<div class="modal-body">
    <h4><?php echo $days; ?></h4>

    <div class="row">
      <div class="col-xs-12 col-md-12">
        <table class="table table-bordered table-hover">
            <tbody>
                <?php for($a=0;$a<24;$a++){ ?>
                <tr class="times">
                    <td class="time<?php echo $a; ?>"><?php echo $a.':00'; ?></td>
                </tr>
                <script type="text/javascript">
                    $('.time<?php echo $a; ?>').click(function(){
                        $('.time<?php echo $a; ?>').addClass('success');
                        $('.btn-next').removeClass('hide');
                        $('.btn-ulang').removeClass('hide');
                    });
                </script>
              <?php } ?>
            </tbody>
        </table>
      </div>
    </div>

</div>
<div class="modal-footer">
    <span class="loading"></span>
    <button type="button" class="btn btn-primary btn-next hide" data-toggle="modal" data-target=".modal-selectScheme">Next</button>
    <button type="button" class="btn btn-default btn-ulang hide">Reset</button>
    <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
</div>
<script type="text/javascript">
    $('.btn-next').click(function(){
        var str = '';

        $('.times > .success').each(function(){
          str += $(this).text() + ",";
        })

        $.ajax({
            type: 'post',
            url: '<?php echo $this->createUrl('selectScheme'); ?>',
            dataType: 'json',
            data: {days: '<?php echo $days; ?>', times: str},
            beforeSend: function(){
                $('.loading').html('<i class="fa fa-rotate-right fa-spin">');
            },
            success: function(responseJSON) {
                if(responseJSON.success){
                    $('.content-chooseTime').html(responseJSON.content);
                }
                else{
                    $('.modal-chooseTime').modal('hide');
                    alert(responseJSON.message);
                }
            },
            error: function(a,b,c){
                $('.modal-chooseTime').modal('hide');
                alert('Kesalahan : '+c);
            },
            complete: function(){
                $('.loading').html('');
            }
        });
    });

    $('.btn-ulang').click(function(){
        $('.times > .success').removeClass('success');
        $('.btn-next').addClass('hide');
        $('.btn-ulang').addClass('hide');
    });
</script>
