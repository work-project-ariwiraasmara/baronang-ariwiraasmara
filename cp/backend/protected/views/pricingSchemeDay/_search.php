<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

    <div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->label($model,'SchemeDayID'); ?>
        </div>
        <div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'SchemeDayID',array('size'=>20,'maxlength'=>20)); ?>
                </div>
            </div>

        </div>
    </div>

    <div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->label($model,'DayName'); ?>
        </div>
        <div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'DayName',array('size'=>20,'maxlength'=>20)); ?>
                </div>
            </div>

        </div>
    </div>

    <div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->label($model,'ParkInHour'); ?>
        </div>
        <div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'ParkInHour'); ?>
                </div>
            </div>

        </div>
    </div>

    <div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->label($model,'PricingSchemeID'); ?>
        </div>
        <div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'PricingSchemeID',array('size'=>20,'maxlength'=>20)); ?>
                </div>
            </div>

        </div>
    </div>

    <div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->label($model,'VehicleID'); ?>
        </div>
        <div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'VehicleID',array('size'=>20,'maxlength'=>20)); ?>
                </div>
            </div>

        </div>
    </div>

	<div class="row buttons">
        <div class="col-xs-12">
            <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> Search</button>    	</div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->