<?php $form=$this->beginWidget('CActiveForm', array(
  'id'=>'pricing-scheme-day-form',
  'enableAjaxValidation'=>false,
)); ?>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title" id="myLargeModalLabel">Select Scheme</h4>
</div>
<div class="modal-body">
  <h4><?php echo $days; ?></h4>
  <br>
  <h4><?php echo $times; ?></h4>
  <div class="form">
  	<?php echo $form->errorSummary($model); ?>

    <div class="form-group">
  		<div class="row">
  			<?php echo $form->labelEx($model,'PricingSchemeID', array('class'=>'col-sm-2 control-label no-padding-right')); ?>
  			<div class="col-sm-3">
  				<?php echo $form->dropDownList($model,'PricingSchemeID',
  					CHtml::listData(PricingScheme::model()->findAll(array('order'=>'PricingSchemeID ASC')), 'PricingSchemeID', 'PricingSchemeID'),
  					array('class'=>'form-control', 'empty'=>'-- Choose Scheme --')); ?>
  				<?php echo $form->error($model,'PricingSchemeID'); ?>
  			</div>
  			<span><?php echo CHtml::link('Add New Scheme', array('pricingScheme/add')) ?></span>
  		</div>
  	</div>

    <div class="form-group">
      <div class="row">
        <?php echo $form->labelEx($model,'VehicleID', array('class'=>'col-sm-2 control-label no-padding-right')); ?>
        <div class="col-sm-3">
          <?php echo $form->dropDownList($model,'VehicleID',
            CHtml::listData(VehicleType::model()->findAll(array('order'=>'Name ASC')), 'VehicleID', 'Name'),
            array('class'=>'form-control', 'empty'=>'-- Choose Vehicle --')); ?>
          <?php echo $form->error($model,'VehicleID'); ?>
        </div>
        <span><?php echo CHtml::link('Add New Vehicle', array('vehicleType/add')) ?></span>
      </div>
    </div>
  </dv>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-primary">Submit</button>
    <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
</div>

<?php $this->endWidget() ?>
