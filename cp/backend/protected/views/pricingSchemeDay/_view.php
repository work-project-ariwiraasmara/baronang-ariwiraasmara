<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('SchemeDayID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->SchemeDayID), array('view', 'id'=>$data->SchemeDayID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('DayName')); ?>:</b>
	<?php echo CHtml::encode($data->DayName); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ParkInHour')); ?>:</b>
	<?php echo CHtml::encode($data->ParkInHour); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('PricingSchemeID')); ?>:</b>
	<?php echo CHtml::encode($data->PricingSchemeID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('VehicleID')); ?>:</b>
	<?php echo CHtml::encode($data->VehicleID); ?>
	<br />


</div>