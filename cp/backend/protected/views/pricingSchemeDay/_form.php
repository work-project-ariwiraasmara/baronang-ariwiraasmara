<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'pricing-scheme-day-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<div class="col-xs-12 col-md-6">
        <div class="well with-header with-footer">
            <div class="header">
							Choose days bottom
							<span class="loading"></span>
						</div>
            <table class="table table-bordered table-hover">
                <tbody>
									<?php
									$no = 1;
									foreach($modelWeek as $mw){ ?>
                    <tr class="daily">
                        <td class="day<?php echo $no; ?>"><?php echo $mw['DayName']; ?></td>
                    </tr>
										<script type="text/javascript">
												$('.day<?php echo $no; ?>').click(function(){
														$('.day<?php echo $no; ?>').addClass('success');
														$('.btn-choose').removeClass('hide');
														$('.btn-reset').removeClass('hide');
												});
										</script>
									<?php $no++; } ?>
                </tbody>
            </table>
            <div class="footer">
                <button type="button" class="btn btn-primary btn-choose hide" data-toggle="modal" data-target=".modal-chooseTime">Choose Time</button>
                <button type="button" class="btn btn-default btn-reset hide">Reset</button>
								<script type="text/javascript">
										$('.btn-choose').click(function(){
												var str = '';

												$('.daily > .success').each(function(){
												  str += $(this).text() + ",";
												})

												$.ajax({
														type: 'post',
														url: '<?php echo $this->createUrl('chooseTime'); ?>',
														dataType: 'json',
														data: {days: str},
														beforeSend: function(){
																$('.loading').html('<i class="fa fa-rotate-right fa-spin">');
														},
														success: function(responseJSON) {
																if(responseJSON.success){
																		$('.content-chooseTime').html(responseJSON.content);
																}
																else{
																		$('.modal-chooseTime').modal('hide');
																		alert(responseJSON.message);
																}
														},
														error: function(a,b,c){
																$('.modal-chooseTime').modal('hide');
												    		alert('Kesalahan : '+c);
												    },
														complete: function(){
																$('.loading').html('');
														}
												});
										});

										$('.btn-reset').click(function(){
												$('.daily > .success').removeClass('success');
												$('.btn-choose').addClass('hide');
												$('.btn-reset').addClass('hide');
										});
								</script>
            </div>
        </div>

    </div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<div class="modal fade modal-chooseTime" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content content-chooseTime">

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
