<?php if(Yii::app()->user->getState('user_id') == null): ?>
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'login-form',
        'enableAjaxValidation' => false,
    ));
    ?>

    <?php if(Yii::app()->user->hasFlash('forgetPassword')): ?>
        <div class="logobox">
            <?php echo Yii::app()->user->getFlash('forgetPassword'); ?>
        </div>
    <?php endif; ?>

    <div class="loginbox bg-white" style="background : #FFF;">
        <br><br><br>
        <div class="loginbox-title">SIGN IN</div>
        <div class="loginbox-or">
            <div class="or-line"></div>
        </div>
        <div class="loginbox-textbox">
            <?php echo $form->textField($model, 'cid',array('class'=>'form-control','placeholder'=>'Company ID')); ?>
            <?php echo $form->error($model, 'cid'); ?>
        </div>
        <div class="loginbox-textbox">
            <?php echo $form->textField($model, 'username',array('class'=>'form-control','placeholder'=>'Username/Email')); ?>
            <?php echo $form->error($model, 'username'); ?>
        </div>
        <div class="loginbox-textbox">
            <?php echo $form->passwordField($model, 'password',array('class'=>'form-control','placeholder'=>'Password')); ?>
            <?php echo $form->error($model, 'password'); ?>
        </div>
        <div class="loginbox-forgot">
            <a href="<?php echo $this->createUrl('forgotpassword'); ?>">Forgot Password?</a>
        </div>
        <div class="loginbox-submit">
            <input type="submit" class="btn btn-primary btn-block" value="Login">
        </div>
        <div class="loginbox-or">
            <div class="or-line"></div>
        </div>
        <br>
    </div>

    <?php $this->endWidget(); ?>
<?php endif; ?>
