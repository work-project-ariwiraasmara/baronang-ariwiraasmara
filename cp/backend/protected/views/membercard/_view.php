<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('CardNo')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->CardNo), array('view', 'id'=>$data->CardNo)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('MemberID')); ?>:</b>
	<?php echo CHtml::encode($data->MemberID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Point')); ?>:</b>
	<?php echo CHtml::encode($data->Point); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Status')); ?>:</b>
	<?php echo CHtml::encode($data->Status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('isLink')); ?>:</b>
	<?php echo CHtml::encode($data->isLink); ?>
	<br />


</div>