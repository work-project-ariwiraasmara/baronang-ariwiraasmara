<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('MemberID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->MemberID), array('view', 'id'=>$data->MemberID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Name')); ?>:</b>
	<?php echo CHtml::encode($data->Name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Addr')); ?>:</b>
	<?php echo CHtml::encode($data->Addr); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Phone')); ?>:</b>
	<?php echo CHtml::encode($data->Phone); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Email')); ?>:</b>
	<?php echo CHtml::encode($data->Email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('StatusMember')); ?>:</b>
	<?php echo CHtml::encode($data->StatusMember); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('KTP')); ?>:</b>
	<?php echo CHtml::encode($data->KTP); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('NIP')); ?>:</b>
	<?php echo CHtml::encode($data->NIP); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Reference')); ?>:</b>
	<?php echo CHtml::encode($data->Reference); ?>
	<br />

	*/ ?>

</div>