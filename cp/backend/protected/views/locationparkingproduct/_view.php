<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ProductID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ProductID), array('view', 'id'=>$data->ProductID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('LocationID')); ?>:</b>
	<?php echo CHtml::encode($data->LocationID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Nama')); ?>:</b>
	<?php echo CHtml::encode($data->Nama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Deskripsi')); ?>:</b>
	<?php echo CHtml::encode($data->Deskripsi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Status')); ?>:</b>
	<?php echo CHtml::encode($data->Status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('JumlahPoinDeduct')); ?>:</b>
	<?php echo CHtml::encode($data->JumlahPoinDeduct); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Jumlah')); ?>:</b>
	<?php echo CHtml::encode($data->Jumlah); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('Harga')); ?>:</b>
	<?php echo CHtml::encode($data->Harga); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Gambar')); ?>:</b>
	<?php echo CHtml::encode($data->Gambar); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('VehicleID')); ?>:</b>
	<?php echo CHtml::encode($data->VehicleID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Tipe')); ?>:</b>
	<?php echo CHtml::encode($data->Tipe); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Keterangan')); ?>:</b>
	<?php echo CHtml::encode($data->Keterangan); ?>
	<br />

	*/ ?>

</div>