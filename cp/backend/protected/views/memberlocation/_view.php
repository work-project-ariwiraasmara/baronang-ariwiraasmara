<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ID), array('view', 'id'=>$data->ID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('MemberID')); ?>:</b>
	<?php echo CHtml::encode($data->MemberID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('LocationID')); ?>:</b>
	<?php echo CHtml::encode($data->LocationID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Status')); ?>:</b>
	<?php echo CHtml::encode($data->Status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('DateRegister')); ?>:</b>
	<?php echo CHtml::encode($data->DateRegister); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ValidDate')); ?>:</b>
	<?php echo CHtml::encode($data->ValidDate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('VehicleID')); ?>:</b>
	<?php echo CHtml::encode($data->VehicleID); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('CardNo')); ?>:</b>
	<?php echo CHtml::encode($data->CardNo); ?>
	<br />

	*/ ?>

</div>