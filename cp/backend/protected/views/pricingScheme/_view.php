<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('PricingSchemeID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->PricingSchemeID), array('view', 'id'=>$data->PricingSchemeID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Max')); ?>:</b>
	<?php echo CHtml::encode($data->Max); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('GPIn')); ?>:</b>
	<?php echo CHtml::encode($data->GPIn); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('GPOut')); ?>:</b>
	<?php echo CHtml::encode($data->GPOut); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ParkInterval')); ?>:</b>
	<?php echo CHtml::encode($data->ParkInterval); ?>
	<br />


</div>