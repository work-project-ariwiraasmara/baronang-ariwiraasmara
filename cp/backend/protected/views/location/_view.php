<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('LocationID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->LocationID), array('view', 'id'=>$data->LocationID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ReferenceID')); ?>:</b>
	<?php echo CHtml::encode($data->ReferenceID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Nama')); ?>:</b>
	<?php echo CHtml::encode($data->Nama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Alamat')); ?>:</b>
	<?php echo CHtml::encode($data->Alamat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Telepon')); ?>:</b>
	<?php echo CHtml::encode($data->Telepon); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Status')); ?>:</b>
	<?php echo CHtml::encode($data->Status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Lon')); ?>:</b>
	<?php echo CHtml::encode($data->Lon); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('Lat')); ?>:</b>
	<?php echo CHtml::encode($data->Lat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Deskripsi')); ?>:</b>
	<?php echo CHtml::encode($data->Deskripsi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Gambar')); ?>:</b>
	<?php echo CHtml::encode($data->Gambar); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Code')); ?>:</b>
	<?php echo CHtml::encode($data->Code); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Balance')); ?>:</b>
	<?php echo CHtml::encode($data->Balance); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('GP')); ?>:</b>
	<?php echo CHtml::encode($data->GP); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('isOnlyMember')); ?>:</b>
	<?php echo CHtml::encode($data->isOnlyMember); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('isFree')); ?>:</b>
	<?php echo CHtml::encode($data->isFree); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('OvoLocationID')); ?>:</b>
	<?php echo CHtml::encode($data->OvoLocationID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('OvoCompanyCode')); ?>:</b>
	<?php echo CHtml::encode($data->OvoCompanyCode); ?>
	<br />

	*/ ?>

</div>