<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ID), array('view', 'id'=>$data->ID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ProductID')); ?>:</b>
	<?php echo CHtml::encode($data->ProductID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('MemberID')); ?>:</b>
	<?php echo CHtml::encode($data->MemberID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('CardNo')); ?>:</b>
	<?php echo CHtml::encode($data->CardNo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('BuyDate')); ?>:</b>
	<?php echo CHtml::encode($data->BuyDate); ?>
	<br />


</div>