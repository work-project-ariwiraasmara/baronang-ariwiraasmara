<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ProductID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ProductID), array('view', 'id'=>$data->ProductID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Nama')); ?>:</b>
	<?php echo CHtml::encode($data->Nama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Deskripsi')); ?>:</b>
	<?php echo CHtml::encode($data->Deskripsi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Status')); ?>:</b>
	<?php echo CHtml::encode($data->Status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('JumlahPoinAdd')); ?>:</b>
	<?php echo CHtml::encode($data->JumlahPoinAdd); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('JumlahPoinBonus')); ?>:</b>
	<?php echo CHtml::encode($data->JumlahPoinBonus); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Harga')); ?>:</b>
	<?php echo CHtml::encode($data->Harga); ?>
	<br />


</div>