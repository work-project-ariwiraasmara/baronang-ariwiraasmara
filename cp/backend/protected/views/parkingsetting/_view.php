<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ID), array('view', 'id'=>$data->ID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('PointConversion')); ?>:</b>
	<?php echo CHtml::encode($data->PointConversion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ValueConversion')); ?>:</b>
	<?php echo CHtml::encode($data->ValueConversion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('MaxTopUpEDC')); ?>:</b>
	<?php echo CHtml::encode($data->MaxTopUpEDC); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('MaxPointUser')); ?>:</b>
	<?php echo CHtml::encode($data->MaxPointUser); ?>
	<br />


</div>