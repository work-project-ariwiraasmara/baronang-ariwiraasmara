<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'master-card-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'CardNo'); ?>
        </div>
		<div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'CardNo',array('size'=>60,'maxlength'=>255)); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'CardNo'); ?>
                </div>
            </div>

		</div>
	</div>

	<div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'RFIDNo'); ?>
        </div>
		<div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'RFIDNo',array('size'=>60,'maxlength'=>255)); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'RFIDNo'); ?>
                </div>
            </div>

		</div>
	</div>

	<div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'Barcode'); ?>
        </div>
		<div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'Barcode',array('size'=>60,'maxlength'=>255)); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'Barcode'); ?>
                </div>
            </div>

		</div>
	</div>

	<div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'DateCreate'); ?>
        </div>
		<div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'DateCreate'); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'DateCreate'); ?>
                </div>
            </div>

		</div>
	</div>

	<div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'Status'); ?>
        </div>
		<div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'Status'); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'Status'); ?>
                </div>
            </div>

		</div>
	</div>

	<div class="row form-row form-group">
        <div class="col-xs-2">
            <?php echo $form->labelEx($model,'Background'); ?>
        </div>
		<div class="col-xs-10">
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->textField($model,'Background',array('size'=>60,'maxlength'=>255)); ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $form->error($model,'Background'); ?>
                </div>
            </div>

		</div>
	</div>

	<div class="row buttons">
        <div class="col-xs-12">
            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Save</button>        </div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->