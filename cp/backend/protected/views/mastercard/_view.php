<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('CardID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->CardID), array('view', 'id'=>$data->CardID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('CardNo')); ?>:</b>
	<?php echo CHtml::encode($data->CardNo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('RFIDNo')); ?>:</b>
	<?php echo CHtml::encode($data->RFIDNo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Barcode')); ?>:</b>
	<?php echo CHtml::encode($data->Barcode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('DateCreate')); ?>:</b>
	<?php echo CHtml::encode($data->DateCreate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Status')); ?>:</b>
	<?php echo CHtml::encode($data->Status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Background')); ?>:</b>
	<?php echo CHtml::encode($data->Background); ?>
	<br />


</div>