
<div>
	<h1 class="left">View MasterCard</h1>	
	<div class="form-button-container">
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('index'); ?>">List</a>
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('add'); ?>">Add</a>
		<a class="form-button btn btn-primary" href="<?php echo $this->createUrl('update', array('id'=>$model->CardID)); ?>">Update</a>
		<?php echo CHtml::linkButton('Delete',  
				array(
					'class' => 'form-button btn btn-primary',
					'submit' => array('delete','id'=>$model->CardID),
					'confirm' => 'Are you sure you want to delete this item?',					
				)
			);
		?>	</div>
</div>
<div class="clear"></div>
<hr />

<?php $this->widget('zii.widgets.CDetailView', array(
    'htmlOptions'=>array(
        'class'=>'detail-view table table-striped table-bordered table-hover'
    ),
    'cssFile' => Yii::app()->theme->baseUrl.'/css/detailView/styles.css',
	'data'=>$model,
	'attributes'=>array(
		'CardNo',
		'RFIDNo',
		'Barcode',
		'DateCreate',
		'Status',
		'Background',
		'CardID',
		//CDetailViewHelper::getCreatedBy($model),
		//CDetailViewHelper::getCreatedDate($model),
		//CDetailViewHelper::getModifiedBy($model),
		//CDetailViewHelper::getModifiedDate($model),
	),
)); ?>
