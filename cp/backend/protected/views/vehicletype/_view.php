<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('VehicleID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->VehicleID), array('view', 'id'=>$data->VehicleID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Name')); ?>:</b>
	<?php echo CHtml::encode($data->Name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Status')); ?>:</b>
	<?php echo CHtml::encode($data->Status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('isVisible')); ?>:</b>
	<?php echo CHtml::encode($data->isVisible); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Code')); ?>:</b>
	<?php echo CHtml::encode($data->Code); ?>
	<br />


</div>