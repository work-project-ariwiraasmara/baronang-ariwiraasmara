<?php

class MyHelper
{
	/**
	 * @var User|null
	 */
	private static $_user;

	/**
	 * Get error notification from flash message
	 */
	public static function errorFlashNotification()
	{
		return NotificationHelper::error(FlashMessageHelper::getError());
	}

	/**
	 * Get success notification from flash message
	 */
	public static function successFlashNotification()
	{
		return NotificationHelper::success(FlashMessageHelper::getSuccess());
	}

	/**
	 * Create a Date in the view
	 * @param mixed $model
	 * @param string $field
	 * @param null|string $format
	 * @return array
	 */
	public static function detailViewDate($model, $field, $format=null)
	{
		return array(
			'name'=>$field,
			'value'=>DateHelper::formatDate($model->$field, $format),
		);
	}

	/**
	 * Create a Date Time in the view
	 *
	 * @param mixed $model
	 * @param string $field
	 * @param null|string $format
	 * @return array
	 */
	public static function detailViewDateTime($model, $field, $format=null)
	{
		return array(
			'name'=>$field,
			'value'=>DateHelper::formatDateTime($model->$field, $format),
		);
	}

	/**
	 * Create Username in the view
	 *
	 * @param mixed $model
	 * @param string $field
	 * @return array
	 */
	public static function detailViewUsername($model, $field)
	{
		$user = Member::model()->findByPk($model->$field);

		return array(
			'name'=>$field,
			'value'=>$user !== null ? $user->username : null,
		);
	}

	/**
	 * @return User|null
	 */
	public static function getUser()
	{
		if (self::$_user === null) {
			if (!Yii::app()->user->isGuest) {
				self::$_user = Member::model()->findByPk(Yii::app()->user->id);
			}
		}

		return self::$_user;
	}

    public static function getConsigneeDetail($id)
    {
				$data = '';
				$model = Consignee::model()->findByPk($id);
				if($model != null){
						$data = $model->Name.' '.$model->Address.' '.$model->PostCode;
						if($model->city != null){
								$city = $model->city;
								if($city->state != null){
										$state = $city->state;
										if($state->country != null){
												$country = $state->country;

												$data = $model->Name.' '.$model->Address.' '.$model->PostCode.' '.$city->CityName.' '.$state->StateName.' '.$country->CountryName;
										}
								}
						}
				}

        return $data;
    }

		public static function getCityDetail($id)
    {
				$data = '';
				$model = City::model()->findByPk($id);
				if($model != null){
						$data = $model->CityName;
						if($model->state != null){
								$state = $model->state;
								if($state->country != null){
										$country = $state->country;

										$data = $model->CityName.', '.$state->StateName.' - '.$country->CountryName;
								}
						}
				}

        return $data;
    }

		public static function getCityCountry($id)
		{
				$data = '';
				$model = City::model()->findByPk($id);
				if($model != null){
						$data = $model->CityName;
						if($model->state != null){
								$state = $model->state;
								if($state->country != null){
										$country = $state->country;

										$data = $country->CountryName;
								}
						}
				}

				return $data;
		}

    public static function getLogin()
    {
        if (self::$_user === null) {
            if (!Yii::app()->user->isGuest) {
                self::$_user = Users::model()->findByPk(Yii::app()->user->id);
            }
        }

        return self::$_user;
    }

    /**
	 * @param $context
	 * @param $message
	 * @param array $params
	 * @return string
	 */
	public static function t($context, $message, $params=array())
	{
		$message = Yii::t($context, $message);
		$newParams = array();
		foreach ($params as $i=>$param) {
			$newParams['{'.$i.'}'] = $param;
		}
		return strtr($message, $newParams);
	}

	/**
	 * @param string|integer $value1
	 * @param integer|null $value2
	 * @return mixed
	 */
	public static function getNominalOrPercentage($value1, $value2=0)
	{
		$isPercentage = false;
		if (strpos($value1, '%') === strlen($value1) - 1) {
			$isPercentage = true;
		}
		$value1 = str_replace('%', '', $value1);

		if ($isPercentage) {
			return ($value1 * $value2) / 100;
		} else {
			return $value1;
		}
	}
}
