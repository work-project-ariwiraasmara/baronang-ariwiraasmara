<?php

class EmailHelper{

    public static function sendApprovalFactory($id){
        $order = ListOrder::model()->findByPk($id);
        if($order != null){
            $toko = Toko::model()->findByPk($order->kodeToko);
            if($toko != null){
                $model = Member::model()->findByPk($toko->kodeMember);
                if($model != null){
                    $message = new YiiMailMessage();
                    //this points to the file test.php inside the view path
                    $message->view = "approvalFactory";

                    $params = array(
                      'model'=>$model,
                      'toko'=>$toko,
                      'order'=>$order,
                    );

                    $message->subject = 'Order Information';
                    $message->setBody($params, 'text/html');
                    $message->addTo($model->email);
                    $message->from = 'noreply@p5trade.com';
                    Yii::app()->mail->send($message);

                    //email buyer
                    $member = Member::model()->findByPk($order->kodeMember);
                    if($member != null){
                        $message = new YiiMailMessage();
                        //this points to the file test.php inside the view path
                        $message->view = "approvalFactory";

                        $params = array(
                          'model'=>$member,
                          'toko'=>$toko,
                          'order'=>$order,
                        );

                        $message->subject = 'Order Information';
                        $message->setBody($params, 'text/html');
                        $message->addTo($member->email);
                        $message->from = 'noreply@p5trade.com';
                        Yii::app()->mail->send($message);
                    }

                    return true;
                }
                else{
                    return false;
                }
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }
    }

    public static function sendRejectFactory($id){
        $order = ListOrder::model()->findByPk($id);
        if($order != null){
            $toko = Toko::model()->findByPk($order->kodeToko);
            if($toko != null){
                $model = Member::model()->findByPk($toko->kodeMember);
                if($model != null){
                    $message = new YiiMailMessage();
                    //this points to the file test.php inside the view path
                    $message->view = "rejectFactory";

                    $params = array(
                      'model'=>$model,
                      'toko'=>$toko,
                      'order'=>$order,
                    );

                    $message->subject = 'Order Information';
                    $message->setBody($params, 'text/html');
                    $message->addTo($model->email);
                    $message->from = 'noreply@p5trade.com';
                    Yii::app()->mail->send($message);

                    //email buyer
                    $member = Member::model()->findByPk($order->kodeMember);
                    if($member != null){
                        $message = new YiiMailMessage();
                        //this points to the file test.php inside the view path
                        $message->view = "approvalFactory";

                        $params = array(
                          'model'=>$member,
                          'toko'=>$toko,
                          'order'=>$order,
                        );

                        $message->subject = 'Order Information';
                        $message->setBody($params, 'text/html');
                        $message->addTo($member->email);
                        $message->from = 'noreply@p5trade.com';
                        Yii::app()->mail->send($message);
                    }

                    return true;
                }
                else{
                    return false;
                }
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }
    }

    public static function sendCheckout($id){
        $order = ListOrder::model()->findByPk($id);
        if($order != null){
            $toko = Toko::model()->findByPk($order->kodeToko);
            if($toko != null){
                $model = Member::model()->findByPk($toko->kodeMember);
                if($model != null){
                    $message = new YiiMailMessage();
                    //this points to the file test.php inside the view path
                    $message->view = "checkout";

                    $params = array(
                      'model'=>$model,
                      'toko'=>$toko,
                      'order'=>$order,
                    );

                    $message->subject = 'Order Information';
                    $message->setBody($params, 'text/html');
                    $message->addTo($model->email);
                    $message->from = 'noreply@p5trade.com';
                    Yii::app()->mail->send($message);

                    return true;
                }
                else{
                    return false;
                }
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }
    }

    public static function sendCancel($id){
        $order = ListOrder::model()->findByPk($id);
        if($order != null){
            $toko = Toko::model()->findByPk($order->kodeToko);
            if($toko != null){
                $model = Member::model()->findByPk($toko->kodeMember);
                if($model != null){
                    $message = new YiiMailMessage();
                    //this points to the file test.php inside the view path
                    $message->view = "cancel";

                    $params = array(
                      'model'=>$model,
                      'toko'=>$toko,
                      'order'=>$order,
                    );

                    $message->subject = 'Order Information';
                    $message->setBody($params, 'text/html');
                    $message->addTo($model->email);
                    $message->from = 'noreply@p5trade.com';
                    Yii::app()->mail->send($message);

                    return true;
                }
                else{
                    return false;
                }
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }
    }

    public static function sendProspect($id){
        $order = ListOrder::model()->findByPk($id);
        if($order != null){
            $toko = Toko::model()->findByPk($order->kodeToko);
            if($toko != null){
                $model = Member::model()->findByPk($toko->kodeMember);
                if($model != null){
                    $message = new YiiMailMessage();
                    //this points to the file test.php inside the view path
                    $message->view = "prospect";

                    $params = array(
                      'model'=>$model,
                      'toko'=>$toko,
                      'order'=>$order,
                    );

                    $message->subject = 'Order Information';
                    $message->setBody($params, 'text/html');
                    $message->addTo($model->email);
                    $message->from = 'noreply@p5trade.com';
                    Yii::app()->mail->send($message);

                    return true;
                }
                else{
                    return false;
                }
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }
    }

    public static function sendRegisterStore($id){
        $model = Toko::model()->findByPk($id);
        if($model != null){
            $message = new YiiMailMessage();
            //this points to the file test.php inside the view path
            $message->view = "registerStore";

            $params = array(
              'model'=>$model,
            );

            $message->subject = 'Mail Confirmation';
            $message->setBody($params, 'text/html');
            $message->addTo($model->email);
            $message->from = 'noreply@p5trade.com';
            Yii::app()->mail->send($message);

            return true;
        }
        else{
            return false;
        }
    }

    public static function sendRegister($id){
        $model = Register::model()->findByPk($id);
        if($model != null){
            $message = new YiiMailMessage();
            //this points to the file test.php inside the view path
            $message->view = "register";

            $params  = array(
              'model'=>$model,
            );

            $message->subject = 'Mail Confirmation';
            $message->setBody($params, 'text/html');
            $message->addTo($model->email);
            $message->from = 'noreply@p5trade.com';
            Yii::app()->mail->send($message);

            return true;
        }
        else{
            return false;
        }
    }

    public static function sendToFactory($orderID){
        $model = ListOrder::model()->findByPk($orderID);
        if($model != null){
            $sql = "select* from KoneksiIndo.dbo.ListKoneksi where kodeToko = '".$model->kodeToko."'";
            // execute the sql command
            $conn =  Yii::app()->db->createCommand($sql)->queryAll();
            if($conn != null){
                $s = "select* from ".$conn[0]['db'].".dbo.Seller where sellerID = '".$model->IDFactory."'";
                // execute the sql command
                $c =  Yii::app()->db->createCommand($s)->queryAll();
                if($c != null){
                    $sellerName = $c[0]['sellerName'];
                    if($c[0]['email'] != ''){
                        $message = new YiiMailMessage();
                        //this points to the file test.php inside the view path
                        $message->view = "sendFactory";

                        $params = array(
                            'model'=>$model,
                            'name'=>$sellerName,
                        );
                        $message->subject    = 'Product Request Confirmation';
                        $message->setBody($params, 'text/html');
                        $message->addTo($c[0]['email']);
                        $message->from = 'noreply@p5trade.com';
                        Yii::app()->mail->send($message);

                        return true;
                    }
                    else{
                        return false;
                    }
                }
                else{
                    return false;
                }
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }
    }
}

?>
