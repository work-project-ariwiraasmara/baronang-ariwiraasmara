<?php

class MyException extends CException
{
	/**
	 * @var string
	 */
	public $translationContext = 'exception';

	/**
	 * @param string $message
	 * @param integer $code
	 * @param CException $previous
	 */
	public function __construct($message="", $code=0, $previous=null)
	{
		$message = Yii::t($this->translationContext, $message);
		parent::__construct($message, $code, $previous);
	}
}