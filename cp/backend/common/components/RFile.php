<?php

class RFile
{
	public static function incrementFile($file)
	{
		$newFile = $file;
		
		$fileParts = pathinfo($file);
		$fileName = $fileParts['filename'];
		$fileExt = $fileParts['extension'];
		
		$i = 0;
		while(file_exists($newFile)) {
			$i++;
			$newFile = $fileParts['dirname']. DIRECTORY_SEPARATOR . $fileName. '__' .$i. '.' .$fileExt;			
		}
		
		return $newFile;
	}
	
	public static function getFileInfo($file)
	{
		return pathinfo($file);
	}
	
	public static function getFilename($file)
	{
		$fileInfo = self::getFileInfo($file);
		return $fileInfo['filename'];
	}
	
	public static function getExtension($file)
	{
		$fileInfo = self::getFileInfo($file);
		return $fileInfo['extension'];
	}
	
	public static function getBasename($file)
	{
		$fileInfo = self::getFileInfo($file);
		return $fileInfo['basename'];
	}
	
	public static function getDirname($file)
	{
		$fileInfo = self::getFileInfo($file);
		return $fileInfo['dirname'];
	}
	
	public static function deleteUnexcludedFile($path, $excludedFile=array())
	{
		if (@$handle = opendir($path)) {
		    while (false !== ($file = readdir($handle))) {   	
		    	if (is_file($path.$file)) {
		    		$fileParts = pathinfo($file);
			    	if (!in_array($file, $excludedFile)) {
			    		unlink($path . $file);
			    	}
		    	}
		    }
		    
		    closedir($handle);
		}
	}
}