<?php

class RImage
{
	const RESIZE_FIT = 0;
	const RESIZE_EXACT = 1;
	
	private $_file;
	private $_image;
	private $imageType;
	
	public $width;
	public $height;
	public $ext;
	
	public function __construct($file)
	{
		// check if file is exists
		if (file_exists($file)) 
		{
			if (self::isImage($file)) 
			{
				$this->_file = $file;
				
				$image_info = getimagesize($file);
				$this->imageType = $image_info[2];
				
				if ( $this->imageType == IMAGETYPE_JPEG ) {		 
					$this->_image = imagecreatefromjpeg($file);
				} elseif( $this->imageType == IMAGETYPE_GIF ) {	 
					$this->_image = imagecreatefromgif($file);
				} elseif( $this->imageType == IMAGETYPE_PNG ) {	
					$this->_image = imagecreatefrompng($file);
				}
			}
		}
	}
	
	private static function isImage($file)
	{
		$fileParts = pathinfo($file);
		$fileExt = strtolower($fileParts['extension']);
		
		if (in_array($fileExt, array('jpg','jpeg','gif','png')))
			return true;
		else
			return false;
	}
	
	/*
	 * mode: fit | exact
	 */
	function resize($width, $height, $mode=self::RESIZE_EXACT)
	{
		$curWidth = $this->getWidth();
		$curHeight = $this->getHeight();
		
		$widthRatio = $curWidth / $width;
		$heightRatio = $curHeight / $height;
		
		if ($mode == 'fit') {
			$divider = 1;
			if ($widthRatio >= $heightRatio)
				$divider = $widthRatio;
			else
				$divider = $heightRatio;
			
			$width = floor($curWidth / $divider + 0.5);
			$height = floor($curHeight / $divider + 0.5);
		}
		
		$new_image = imagecreatetruecolor($width, $height);
		imagecopyresampled($new_image, $this->_image, 0, 0, 0, 0, $width, $height, $this->getWidth(), $this->getHeight());
		$this->_image = $new_image;
	}
	
	function save($filename, $incrementFile=true, $image_type=IMAGETYPE_JPEG, $compression=100, $permissions=null) 
	{
		if ($incrementFile) {
			$filename = $this->incrementFile($filename);
		} else {
			if (@file_exists($filename) && is_file($filename)) {
				@unlink($filename);
			}
		}
				
		if( $image_type == IMAGETYPE_JPEG ) {
			imagejpeg($this->_image,$filename,$compression);
		} elseif( $image_type == IMAGETYPE_GIF ) { 
			imagegif($this->_image,$filename);
		} elseif( $image_type == IMAGETYPE_PNG ) {
			imagepng($this->_image,$filename);
		} else {
			return false;
		}
		
		if( $permissions != null) {
			chmod($filename,$permissions);
		}
		
		return true;
	}
	
	public function incrementFile($file)
	{
		$newFile = RFile::incrementFile($file);
		$this->_file = $newFile;
		
		return $newFile;
	}
	
	function createThumbnail($filename, $width, $height, $mode=self::RESIZE_EXACT)
	{
		$newFile = $filename;
		
		if ($this->save($filename))
		{
			$image = new RImage($filename);
			$image->resize($width, $height, $mode);
			$image->save($image->getFile(), false);
		}
		
		return $newFile;
	}
	
	function output($image_type=IMAGETYPE_JPEG) 
	{
		if( $image_type == IMAGETYPE_JPEG ) {
			imagejpeg($this->_image);
		} elseif( $image_type == IMAGETYPE_GIF ) {
			imagegif($this->_image);
		} elseif( $image_type == IMAGETYPE_PNG ) {
			imagepng($this->_image);
		}
	}
	
	function getWidth() 
	{
		return imagesx($this->_image);
	}
	
	function getHeight() 
	{
		return imagesy($this->_image);
	}
	
	function resizeToHeight($height) 
	{
		$ratio = $height / $this->getHeight();
		$width = $this->getWidth() * $ratio;
		$this->resize($width,$height);
	}
	
	function resizeToWidth($width) {
		$ratio = $width / $this->getWidth();
		$height = $this->getheight() * $ratio;
		$this->resize($width,$height);
	}
 
	function scale($scale) {
		$width = $this->getWidth() * $scale/100;
		$height = $this->getheight() * $scale/100;
		$this->resize($width,$height);
	}
	
	function getFile()
	{
		return $this->_file;
	}
}