<?php

class EnumJenisKelamin extends MyEnum
{
    const MAN= 1;
    const WOMAN = 0;

    public static function getList()
    {
        return array(
            self::MAN => 'Man',
            self::WOMAN => 'Woman',
        );
    }

    public static function getLabel($id)
    {
        $list = static::getList();
        if (isset($list[$id])) {
            return $list[$id];
        }

        return '-';
    }
}