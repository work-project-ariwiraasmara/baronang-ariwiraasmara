<?php

class EnumStatusProduct extends MyEnum
{
    const READY = 0;
    const PROD = 1;
    const CUSTOM = 2;

    public static function getList()
    {
        return array(
            self::READY => 'Ready',
            self::PROD => 'Production',
            self::CUSTOM => 'Custom',
        );
    }

    public static function getLabel($id)
    {
        $list = static::getList();
        if (isset($list[$id])) {
            return $list[$id];
        }

        return '-';
    }
}