<?php

class EnumBulan extends MyEnum
{
    const JANUARI = 'Januari';
    const FEBRUARI = 'Februari';
    const MARET = 'Maret';
    const APRIL = 'April';
    const MEI = 'Mei';
    const JUNI = 'Juni';
    const JULI = 'Juli';
    const AGUSTUS = 'Agustus';
    const SEPTEMBER = 'September';
    const OKTOBER = 'Oktober';
    const NOVEMBER = 'November';
    const DESEMBER = 'Desember';
}