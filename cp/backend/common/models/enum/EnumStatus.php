<?php

class EnumStatus extends MyEnum
{
    const ACTIVE= 1;
    const NON_ACTIVE = 0;

    public static function getList()
    {
        return array(
            self::ACTIVE => 'Active',
            self::NON_ACTIVE => 'Non Active',
        );
    }

    public static function getLabel($id)
    {
        $list = static::getList();
        if (isset($list[$id])) {
            return $list[$id];
        }

        return '-';
    }
}