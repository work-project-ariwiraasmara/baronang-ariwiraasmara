<?php

class EnumConsNotif extends MyEnum
{
    const CONSIGNE = 0;
    const NOTIFY = 1;

    public static function getList()
    {
        return array(
            self::CONSIGNE => 'Consigne',
            self::NOTIFY => 'Notify Party',
        );
    }

    public static function getLabel($id)
    {
        $list = static::getList();
        if (isset($list[$id])) {
            return $list[$id];
        }

        return '-';
    }

}