<?php

class LookupReward extends Lookup
{
	public function init()
	{
		parent::init();		
		$this->initData();
		$this->title = 'Lookup Reward';				
	}
	
	public function initData()
	{
		$model = new Reward('search');
		$model->unsetAttributes(); // clear any default values
		if(isset($_GET['Reward']))
			$model->attributes=$_GET['Reward'];
		
		$this->defaultGridViewConfig = array(
			'id'=>'reward-grid',
			'cssFile' => $this->cssFile,
			'dataProvider'=>$model->searchLookup(),
			'filter'=>$model,
			'columns'=>array(
                            'name',
                            'point',
                            array(
                                'name'=>'image',
                                'type'=>'raw',
                                'value'=> '"<img src=".Reward::getImageUrl().$data->image." height=\"50\" />"',
                                'filter'=>false,
                                'sortable'=>false,
                            ),
			),
		);
	}
}