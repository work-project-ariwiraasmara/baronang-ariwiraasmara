<?php

Class Calendar extends CWidget{
    
    public $assetsPath;
    public $id='calendar';
    public $height = 100;
    public $timeFormat = 'yyyy-MM-dd';
    public $dayClick = 'function(date, allDay, jsEvent, view) {},';
    
    public function init(){
        if($this->assetsPath == null)
            $this->assetsPath = Yii::app()->getAssetManager()->publish(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'assets');
    }

    public function addConfig($configs)
    {
            foreach ($configs as $key=>$config) {
                    $this->_gridview_config[$key] = $config;
            }
    }

    protected function registerClientScript()
    {
            $cs = Yii::app()->clientScript;
            $cs->registerCssFile($this->assetsPath .'/fullcalendar/fullcalendar.css');
            $cs->registerCssFile($this->assetsPath .'/fullcalendar/fullcalendar.print.css');
            $cs->registerScriptFile($this->assetsPath . '/fullcalendar/fullcalendar.min.js');
            $cs->registerScriptFile($this->assetsPath .'/fullcalendar/gcal.js');

            $originalScriptId = $scriptId = 'timePicker';
            $num = 0;
            while($cs->isScriptRegistered($scriptId)) {
                    $num++;
                    $scriptId = $originalScriptId . $num;
            }

            $cs->registerScript($scriptId, "
                    $(document).ready(function() {

                    $('#".$this->id."').fullCalendar({
                        height: ".$this->height.",
                        timeFormat: '".$this->timeFormat."',
                        header: {
                            left:   'prev',
                            center: 'title',
                            right:  'next'
                        },
                        dayClick: ".$this->dayClick."
                    })

                });
            ");
    }

    public function run()
    {
            $this->registerClientScript();
    }
}
?>
