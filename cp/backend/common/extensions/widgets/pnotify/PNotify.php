<?php

Class PNotify extends CWidget{
    
    public $assetsPath;
    public $title='';
    public $type = '';
    public $text = '';
    
    public function init(){
        if($this->assetsPath == null)
            $this->assetsPath = Yii::app()->getAssetManager()->publish(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'assets');
    }

    public function addConfig($configs)
    {
            foreach ($configs as $key=>$config) {
                    $this->_gridview_config[$key] = $config;
            }
    }

    protected function registerClientScript()
    {
            $cs = Yii::app()->clientScript;
            $cs->registerCssFile($this->assetsPath .'/css/jquery.pnotify.default.css');
            $cs->registerCssFile($this->assetsPath .'/css/jquery.pnotify.default.icons.css');
            $cs->registerScriptFile($this->assetsPath .'/js/jquery.pnotify.min.js');
            

            $originalScriptId = $scriptId = 'pnotify';
            $num = 0;
            while($cs->isScriptRegistered($scriptId)) {
                    $num++;
                    $scriptId = $originalScriptId . $num;
            }
            $cs->registerScript($scriptId, "$.pnotify.defaults.styling = 'jqueryui';");
            
            if($this->text != ''){
                $cs->registerScript($scriptId, "
                        $.pnotify({
                            title: '".$this->title."',
                            text: '".$this->text."',
                            type: '".$this->type."'
                        });
                ");
            }
    }   

    public function run()
    {
            $this->registerClientScript();
    }
}
?>
