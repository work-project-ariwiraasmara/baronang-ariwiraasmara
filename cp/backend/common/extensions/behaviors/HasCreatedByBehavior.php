<?php

class HasCreatedByBehavior extends CActiveRecordBehavior
{
    public function attach($owner)
    {
        parent::attach($owner);
        $this->owner->getMetaData()->addRelation('userCreate', array(CActiveRecord::BELONGS_TO, 'Users', 'CreatedBy'));
    }

    public function beforeValidate($event)
    {
        if ($this->owner->isNewRecord) {
	        if (isset(Yii::app()->user->isMemberShipLogin)) {
		        $this->owner->createdBy = 0;
	        }
            else if (Yii::app()->user->isGuest){
                $this->owner->createdBy = 0;
            }
            else {
	            if($this->owner->createdBy===NULL || $this->owner->createdBy=== '')
		            $this->owner->createdBy = Yii::app()->user->id;
	        }
        }

        return parent::beforeValidate($event);
    }
}
