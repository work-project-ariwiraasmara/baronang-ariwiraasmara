<?php

class HasModifiedDateBehavior extends CActiveRecordBehavior
{
    public function beforeValidate($event)
    {
        if (!$this->owner->isNewRecord) {
            $this->owner->modifiedDate = DateHelper::now();
        }

        return parent::beforeValidate($event);
    }
}
