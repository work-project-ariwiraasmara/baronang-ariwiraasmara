<?php

class HasModifiedByBehavior extends CActiveRecordBehavior
{
    public function attach($owner)
    {
        parent::attach($owner);
        $this->owner->getMetaData()->addRelation('userModify', array(CActiveRecord::BELONGS_TO, 'Users', 'modifiedBy'));
    }

    public function beforeValidate($event)
    {
        if (!$this->owner->isNewRecord) {
	        if (isset(Yii::app()->user->isMemberShipLogin)) {
		        $this->owner->modifiedBy = 0;
	        }
            else if (Yii::app()->user->isGuest){
                $this->owner->modifiedBy = 0;
            }
            else if ($this->owner->modifiedBy === null || $this->owner->modifiedBy === '') {
                $this->owner->modifiedBy = Yii::app()->user->id;
	        }
        }

        return parent::beforeValidate($event);
    }
}
