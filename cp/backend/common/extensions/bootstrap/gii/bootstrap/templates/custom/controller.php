<?php
$hasCreatedBy = false;
foreach($this->tableSchema->columns as $column) {
	if ($column->name == 'created_by') {
		$hasCreatedBy = true;
	}
}
?>

<?php echo "<?php\n"; ?>

class <?php echo $this->controllerClass; ?> extends <?php echo $this->baseControllerClass."\n"; ?>
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model = new <?php echo $this->modelClass; ?>('search');
		$model->unsetAttributes();  // clear any default values

		if (MyHelper::hasAccess('<?php echo $this->modelClass; ?>Own')) {
			$model->created_by = Yii::app()->user->id;
		}

		if(isset($_GET['<?php echo $this->modelClass; ?>'])) {
			$model->attributes = $_GET['<?php echo $this->modelClass; ?>'];
		}

		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model = new <?php echo $this->modelClass; ?>();

		if(isset($_POST['<?php echo $this->modelClass; ?>'])) {
			$model->attributes = $_POST['<?php echo $this->modelClass; ?>'];
			if($model->save()) {
				MyHelper::logUserAction('Create <?php echo $this->modelClass; ?>', '<?php echo $this->modelClass; ?>', $model-><?php echo $this->tableSchema->primaryKey; ?>);
				$this->redirect(array('view','id'=>$model-><?php echo $this->tableSchema->primaryKey; ?>));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);
		$this->authorizeOptionalAccesses(array('<?php echo $this->modelClass; ?>Own'), array('model'=>$model));

		if(isset($_POST['<?php echo $this->modelClass; ?>'])) {
			$model->attributes = $_POST['<?php echo $this->modelClass; ?>'];
			if($model->save()) {
				MyHelper::logUserAction('Update <?php echo $this->modelClass; ?>', '<?php echo $this->modelClass; ?>', $model-><?php echo $this->tableSchema->primaryKey; ?>);
				$this->redirect(array('view','id'=>$model-><?php echo $this->tableSchema->primaryKey; ?>));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$model = $this->loadModel($id);
		$this->authorizeOptionalAccesses(array('<?php echo $this->modelClass; ?>Own'), array('model'=>$model));

		$this->render('view',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$model = $this->loadModel($id);
		$this->authorizeOptionalAccesses(array('<?php echo $this->modelClass; ?>Own'), array('model'=>$model));

		<?php
			if(isset($this->tableSchema->columns['is_deleted'])) {
				echo '$model->remove()->save();';
			} else {
				echo '$model->delete();';
			}
		?>


		MyHelper::logUserAction('Delete <?php echo $this->modelClass; ?>', '<?php echo $this->modelClass; ?>', $model-><?php echo $this->tableSchema->primaryKey; ?>);

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax'])) {
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
		}
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return <?php echo $this->modelClass; ?> the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model = <?php echo $this->modelClass; ?>::model()->findByPk($id);
		if($model === null) {
			throw new CHttpException(404,'The requested page does not exist.');
		}
		return $model;
	}
}
