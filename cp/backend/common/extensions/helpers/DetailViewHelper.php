<?php

class DetailViewHelper
{
    /**
     * Create a Date in the view
     * @param mixed $model
     * @param string $field
     * @param null|string $format
     * @return array
     */
    public static function formatDate($model, $field, $format=null)
    {
        return array(
            'name'=>$field,
            'value'=>DateHelper::formatDate($model->$field, $format),
        );
    }

    /**
     * Create a Date Time in the view
     *
     * @param mixed $model
     * @param string $field
     * @param null|string $format
     * @return array
     */
    public static function formatDateTime($model, $field, $format=null)
    {
        return array(
            'name'=>$field,
            'value'=>DateHelper::formatDateTime($model->$field, $format),
        );
    }

    /**
     * Create Username in the view
     *
     * @param mixed $model
     * @param string $field
     * @return array
     */
    public static function username($model, $field)
    {
        $user = Pengguna::model()->findByPk($model->$field);

        return array(
            'name'=>$field,
            'value'=>$user !== null ? $user->username : null,
        );
    }
}