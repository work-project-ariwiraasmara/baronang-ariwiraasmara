<?php

class CounterNumHelper
{

    /**
     * @param string $keyword
     * @return string
     */
    public static function getFormat($keyword) {
				// sql query for calling the procedure
				$sql = "select* from ".$_SESSION['db'].".dbo.CounterNum where keyword = 'format_".$keyword."'";
				// execute the sql command
				$model =  Yii::app()->db->createCommand($sql)->queryAll();
        if ($model == null) {
            throw new CDbException('The format to generate ID is not found.');
        }

        return $model[0]['value'];
    }

    /**
     * @param string $keyword
     * @throws CDbException
     * @return int
     */
    public static function getCounter($keyword) {
        $counter = 0;
				// sql query for calling the procedure
				$sql = "select* from ".$_SESSION['db'].".dbo.CounterNum where keyword = 'counter_".$keyword."'";
				// execute the sql command
				$model =  Yii::app()->db->createCommand($sql)->queryAll();
        if ($model == null) {
						throw new CDbException('The counter to generate ID is not found.');
        } else {
            $counter = $model[0]['value'];
        }

        return (int)$counter;
    }

    /**
     * @param string $keyword
     * @throws CDbException
     * @return boolean
     */
    public static function restartCounter($keyword) {
        $result = true;
				// sql query for calling the procedure
				$sql = "select* from ".$_SESSION['db'].".dbo.CounterNum where keyword = 'counter_".$keyword."'";
				// execute the sql command
				$model =  Yii::app()->db->createCommand($sql)->queryAll();
        if ($model == null) {
            $result = false;
        } else {
						$sql = "update ".$_SESSION['db'].".dbo.CounterNum set value = 0 where keyword = 'counter_".$keyword."'";
						Yii::app()->db->createCommand($sql)->execute();
        }

        return $result;
    }

    /**
     * @param $keyword
     * @throws CDbException
     */
    public static function addCounter($keyword) {
				// sql query for calling the procedure
				$sql = "select* from ".$_SESSION['db'].".dbo.CounterNum where keyword = 'counter_".$keyword."'";
				// execute the sql command
				$model =  Yii::app()->db->createCommand($sql)->queryAll();
        if ($model == null) {
						$sql = "insert into ".$_SESSION['db'].".dbo.CounterNum values('counter_".$keyword."','0')";
						Yii::app()->db->createCommand($sql)->execute();
        }

				$no = $model[0]['value']+1;
				$sql = "update ".$_SESSION['db'].".dbo.CounterNum set value = '".$no."' where keyword = 'counter_".$keyword."'";
				$exec = Yii::app()->db->createCommand($sql);
				if(!$exec->execute()){
						throw new CDbException('Failed to save counter.');
				}
    }

}
