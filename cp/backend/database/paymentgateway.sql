-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Jun 25, 2019 at 10:18 AM
-- Server version: 5.6.38
-- PHP Version: 7.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `paymentgateway`
--

-- --------------------------------------------------------

--
-- Table structure for table `Company`
--

CREATE TABLE `Company` (
  `CompanyID` varchar(20) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `Status` int(11) NOT NULL DEFAULT '0',
  `CountryID` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Company`
--

INSERT INTO `Company` (`CompanyID`, `Name`, `Status`, `CountryID`) VALUES
('BAP', 'Development', 1, 'INA');

-- --------------------------------------------------------

--
-- Table structure for table `Country`
--

CREATE TABLE `Country` (
  `CountryID` varchar(5) NOT NULL,
  `Name` varchar(30) NOT NULL,
  `Status` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Country`
--

INSERT INTO `Country` (`CountryID`, `Name`, `Status`) VALUES
('INA', 'Indonesia', 1);

-- --------------------------------------------------------

--
-- Table structure for table `EDC`
--

CREATE TABLE `EDC` (
  `EDCID` varchar(20) NOT NULL,
  `CompanyID` varchar(20) NOT NULL,
  `SN` varchar(20) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `Status` int(11) NOT NULL DEFAULT '0',
  `Balance` double NOT NULL DEFAULT '0',
  `Pin` varchar(50) NOT NULL,
  `DeviceID` varchar(50) NOT NULL,
  `LastLogin` datetime NOT NULL,
  `CreatedDate` datetime NOT NULL,
  `CreatedBy` int(11) NOT NULL,
  `ModifiedDate` datetime NOT NULL,
  `ModifiedBy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `EDC`
--

INSERT INTO `EDC` (`EDCID`, `CompanyID`, `SN`, `Name`, `Status`, `Balance`, `Pin`, `DeviceID`, `LastLogin`, `CreatedDate`, `CreatedBy`, `ModifiedDate`, `ModifiedBy`) VALUES
('1234567890123', 'BAP', '1234567890123', 'EDC Admin', 1, 0, 'e10adc3949ba59abbe56e057f20f883e', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `Koneksi`
--

CREATE TABLE `Koneksi` (
  `ID` int(11) NOT NULL,
  `CompanyID` varchar(5) NOT NULL,
  `IP` varchar(15) NOT NULL,
  `DB` varchar(20) NOT NULL,
  `Status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Koneksi`
--

INSERT INTO `Koneksi` (`ID`, `CompanyID`, `IP`, `DB`, `Status`) VALUES
(1, 'BAP', '', 'cloudparking', 1);

-- --------------------------------------------------------

--
-- Table structure for table `Users`
--

CREATE TABLE `Users` (
  `UserID` varchar(20) NOT NULL,
  `CompanyID` varchar(20) NOT NULL,
  `Username` varchar(20) NOT NULL,
  `FirstName` varchar(255) NOT NULL,
  `LastName` varchar(255) NOT NULL,
  `Phone` varchar(20) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `Address` text NOT NULL,
  `Password` varchar(255) NOT NULL,
  `PIN` varchar(50) NOT NULL,
  `Status` int(11) NOT NULL,
  `DeviceID` varchar(50) NOT NULL,
  `LastLogin` datetime NOT NULL,
  `SessionKey` varchar(255) NOT NULL,
  `SessionExpired` datetime NOT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedDate` datetime NOT NULL,
  `ModifiedBy` varchar(50) NOT NULL,
  `ModifiedDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Users`
--

INSERT INTO `Users` (`UserID`, `CompanyID`, `Username`, `FirstName`, `LastName`, `Phone`, `Email`, `Address`, `Password`, `PIN`, `Status`, `DeviceID`, `LastLogin`, `SessionKey`, `SessionExpired`, `CreatedBy`, `CreatedDate`, `ModifiedBy`, `ModifiedDate`) VALUES
('USR001', 'BAP', 'admin', 'Administrator', '1', '081291500104', 'rachman.latif@gmail.com', 'Jakarta', '21232f297a57a5a743894a0e4a801fc3', 'e10adc3949ba59abbe56e057f20f883e', 1, '', '2019-06-25 15:12:56', '1d7ba2f6b04b7b15b1ebc45ebc0909f8', '2019-06-25 16:12:56', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Company`
--
ALTER TABLE `Company`
  ADD PRIMARY KEY (`CompanyID`),
  ADD KEY `CountryID` (`CountryID`);

--
-- Indexes for table `Country`
--
ALTER TABLE `Country`
  ADD PRIMARY KEY (`CountryID`);

--
-- Indexes for table `EDC`
--
ALTER TABLE `EDC`
  ADD PRIMARY KEY (`EDCID`),
  ADD UNIQUE KEY `SN` (`SN`),
  ADD KEY `SN_2` (`SN`),
  ADD KEY `CompanyID` (`CompanyID`),
  ADD KEY `SN_3` (`SN`),
  ADD KEY `DeviceID` (`DeviceID`);

--
-- Indexes for table `Koneksi`
--
ALTER TABLE `Koneksi`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `CompanyCode` (`CompanyID`);

--
-- Indexes for table `Users`
--
ALTER TABLE `Users`
  ADD PRIMARY KEY (`UserID`),
  ADD KEY `phone` (`Phone`),
  ADD KEY `email` (`Email`),
  ADD KEY `CompanyID` (`CompanyID`),
  ADD KEY `Username` (`Username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Koneksi`
--
ALTER TABLE `Koneksi`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
