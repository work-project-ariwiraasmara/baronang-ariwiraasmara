-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Jun 25, 2019 at 10:18 AM
-- Server version: 5.6.38
-- PHP Version: 7.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cloudparking`
--

-- --------------------------------------------------------

--
-- Table structure for table `Location`
--

CREATE TABLE `Location` (
  `LocationID` varchar(255) NOT NULL DEFAULT '',
  `ReferenceID` varchar(255) DEFAULT NULL,
  `Nama` varchar(255) DEFAULT NULL,
  `Alamat` varchar(255) DEFAULT NULL,
  `Telepon` varchar(255) DEFAULT NULL,
  `Status` int(11) DEFAULT NULL,
  `Lon` varchar(255) DEFAULT NULL,
  `Lat` varchar(255) DEFAULT NULL,
  `Deskripsi` varchar(255) DEFAULT NULL,
  `Gambar` varchar(255) DEFAULT NULL,
  `Code` varchar(3) DEFAULT NULL,
  `Balance` float DEFAULT NULL,
  `GP` int(11) NOT NULL,
  `isOnlyMember` int(11) DEFAULT NULL,
  `isFree` int(11) DEFAULT '0',
  `OvoLocationID` varchar(15) NOT NULL,
  `OvoCompanyCode` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Location`
--

INSERT INTO `Location` (`LocationID`, `ReferenceID`, `Nama`, `Alamat`, `Telepon`, `Status`, `Lon`, `Lat`, `Deskripsi`, `Gambar`, `Code`, `Balance`, `GP`, `isOnlyMember`, `isFree`, `OvoLocationID`, `OvoCompanyCode`) VALUES
('LOC001', NULL, 'WTC Matahari Serpong', 'Serpong Tangerang', NULL, 1, NULL, NULL, NULL, NULL, '001', 0, 30, 0, 0, 'BRG', '45');

-- --------------------------------------------------------

--
-- Table structure for table `LocationGate`
--

CREATE TABLE `LocationGate` (
  `LocationGateID` varchar(255) NOT NULL,
  `LocationID` varchar(255) DEFAULT NULL,
  `Status` int(11) DEFAULT NULL,
  `Code` varchar(255) DEFAULT NULL,
  `Note` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `LocationGate`
--

INSERT INTO `LocationGate` (`LocationGateID`, `LocationID`, `Status`, `Code`, `Note`) VALUES
('LG0001', 'LOC001', 1, 'PM1', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `LocationGateDetail`
--

CREATE TABLE `LocationGateDetail` (
  `LocationGateDetailID` int(11) NOT NULL,
  `LocationGateID` varchar(255) DEFAULT NULL,
  `VehicleID` varchar(255) DEFAULT NULL,
  `Status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `LocationGateDetail`
--

INSERT INTO `LocationGateDetail` (`LocationGateDetailID`, `LocationGateID`, `VehicleID`, `Status`) VALUES
(1, 'LG0001', 'VHC002', 1);

-- --------------------------------------------------------

--
-- Table structure for table `LocationParkingProduct`
--

CREATE TABLE `LocationParkingProduct` (
  `LocationID` varchar(255) NOT NULL,
  `ProductID` varchar(255) NOT NULL,
  `Nama` varchar(255) DEFAULT NULL,
  `Deskripsi` text,
  `Status` int(11) DEFAULT NULL,
  `JumlahPoinDeduct` int(11) DEFAULT NULL,
  `Jumlah` int(11) DEFAULT NULL,
  `Harga` float DEFAULT NULL,
  `Gambar` varchar(255) DEFAULT NULL,
  `VehicleID` varchar(255) DEFAULT NULL,
  `Tipe` int(11) DEFAULT NULL,
  `Keterangan` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `LocationParkingProductDiscount`
--

CREATE TABLE `LocationParkingProductDiscount` (
  `EndDate` datetime DEFAULT NULL,
  `ProductID` varchar(255) DEFAULT NULL,
  `StartDate` datetime DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `Amount` float DEFAULT NULL,
  `Status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `LocationParkingProductTopUp`
--

CREATE TABLE `LocationParkingProductTopUp` (
  `ProductID` varchar(255) NOT NULL,
  `Nama` varchar(255) DEFAULT NULL,
  `Deskripsi` text,
  `Status` int(11) DEFAULT NULL,
  `JumlahPoinAdd` int(11) DEFAULT NULL,
  `JumlahPoinBonus` int(11) DEFAULT NULL,
  `Harga` float(20,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `LocationQuota`
--

CREATE TABLE `LocationQuota` (
  `LocationID` varchar(255) DEFAULT NULL,
  `VehicleID` varchar(255) DEFAULT NULL,
  `Quota` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `MasterCard`
--

CREATE TABLE `MasterCard` (
  `CardNo` varchar(255) DEFAULT NULL,
  `RFIDNo` varchar(255) DEFAULT NULL,
  `Barcode` varchar(255) DEFAULT NULL,
  `DateCreate` datetime DEFAULT NULL,
  `Status` int(11) DEFAULT NULL,
  `Background` varchar(255) DEFAULT NULL,
  `CardID` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `MasterCard`
--

INSERT INTO `MasterCard` (`CardNo`, `RFIDNo`, `Barcode`, `DateCreate`, `Status`, `Background`, `CardID`) VALUES
('1000100010001004', NULL, '1000100010001004', NULL, 1, NULL, '1000100010001004'),
('1000100010001008', NULL, '1000100010001008', NULL, 1, NULL, '1000100010001008'),
('1000100010001009', NULL, '1000100010001009', NULL, 1, NULL, '1000100010001009'),
('1000100010001016', NULL, '1000100010001016', NULL, 1, NULL, '1000100010001016');

-- --------------------------------------------------------

--
-- Table structure for table `MemberCard`
--

CREATE TABLE `MemberCard` (
  `MemberID` varchar(255) DEFAULT NULL,
  `CardNo` varchar(255) NOT NULL DEFAULT '',
  `Point` int(11) DEFAULT NULL,
  `Status` int(11) DEFAULT NULL,
  `isLink` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `MemberCard`
--

INSERT INTO `MemberCard` (`MemberID`, `CardNo`, `Point`, `Status`, `isLink`) VALUES
(NULL, '1000100010001004', 0, 1, 0),
(NULL, '1000100010001008', 0, 1, 0),
(NULL, '1000100010001009', 0, 1, 0),
(NULL, '1000100010001016', 1000, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `MemberData`
--

CREATE TABLE `MemberData` (
  `Name` varchar(255) DEFAULT NULL,
  `PlateNumber` varchar(255) DEFAULT NULL,
  `MemberCard` varchar(255) DEFAULT NULL,
  `RFID` varchar(255) DEFAULT NULL,
  `KTP` varchar(255) DEFAULT NULL,
  `VehicleType` varchar(255) DEFAULT NULL,
  `Note` varchar(255) DEFAULT NULL,
  `CardNo` varchar(255) NOT NULL DEFAULT '',
  `LocationID` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `MemberList`
--

CREATE TABLE `MemberList` (
  `MemberID` varchar(255) NOT NULL DEFAULT '',
  `Name` varchar(255) DEFAULT NULL,
  `Addr` varchar(255) DEFAULT NULL,
  `Phone` varchar(255) DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `StatusMember` int(11) DEFAULT NULL,
  `KTP` varchar(30) DEFAULT NULL,
  `NIP` varchar(50) DEFAULT NULL,
  `Reference` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `MemberLocation`
--

CREATE TABLE `MemberLocation` (
  `ID` int(11) NOT NULL,
  `MemberID` varchar(255) DEFAULT NULL,
  `LocationID` varchar(255) DEFAULT NULL,
  `Status` int(11) DEFAULT NULL,
  `DateRegister` datetime DEFAULT NULL,
  `ValidDate` date DEFAULT NULL,
  `VehicleID` varchar(255) DEFAULT NULL,
  `CardNo` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `MemberLocationMonth`
--

CREATE TABLE `MemberLocationMonth` (
  `CardNo` varchar(255) DEFAULT NULL,
  `Bulan` int(11) DEFAULT NULL,
  `Status` int(11) DEFAULT NULL,
  `LocationID` varchar(255) DEFAULT NULL,
  `VehicleID` varchar(255) DEFAULT NULL,
  `ID` int(11) NOT NULL,
  `Year` varchar(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ParkingList`
--

CREATE TABLE `ParkingList` (
  `TransID` int(11) NOT NULL,
  `GateIn` varchar(255) DEFAULT NULL,
  `CardNo` varchar(255) DEFAULT NULL,
  `TimeIn` datetime DEFAULT NULL,
  `TimeOut` datetime DEFAULT NULL,
  `GateOut` varchar(255) DEFAULT NULL,
  `Duration` int(11) DEFAULT NULL,
  `TotalDeduct` int(11) DEFAULT NULL,
  `Status` int(11) DEFAULT '0',
  `isMember` int(11) DEFAULT '0',
  `isFree` int(11) NOT NULL DEFAULT '0',
  `Code` varchar(255) DEFAULT NULL,
  `GP` int(11) NOT NULL DEFAULT '0',
  `GracePeriod` datetime DEFAULT NULL,
  `VehicleType` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ParkingList`
--

INSERT INTO `ParkingList` (`TransID`, `GateIn`, `CardNo`, `TimeIn`, `TimeOut`, `GateOut`, `Duration`, `TotalDeduct`, `Status`, `isMember`, `isFree`, `Code`, `GP`, `GracePeriod`, `VehicleType`) VALUES
(10, 'LG0001', 'S1A001SPBRG45', '2019-05-14 10:59:55', NULL, NULL, NULL, NULL, 2, 0, 0, 'S1A001SPBRG45', 30, '0000-00-00 00:00:00', 'C'),
(11, 'LG0001', 'RQH001SPBRG45', '2019-05-28 09:55:27', NULL, NULL, NULL, NULL, 0, 0, 0, 'RQH001SPBRG45', 30, '0000-00-00 00:00:00', 'C');

-- --------------------------------------------------------

--
-- Table structure for table `ParkingListPayment`
--

CREATE TABLE `ParkingListPayment` (
  `PaymentID` int(11) NOT NULL DEFAULT '0',
  `TransID` varchar(255) DEFAULT NULL,
  `ReferenceID` varchar(255) NOT NULL,
  `Method` varchar(15) NOT NULL,
  `PaidDate` datetime DEFAULT NULL,
  `PaidAmount` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ParkingSetting`
--

CREATE TABLE `ParkingSetting` (
  `PointConversion` int(11) DEFAULT NULL,
  `ValueConversion` float DEFAULT NULL,
  `MaxTopUpEDC` float DEFAULT NULL,
  `MaxPointUser` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ParkingSetting`
--

INSERT INTO `ParkingSetting` (`PointConversion`, `ValueConversion`, `MaxTopUpEDC`, `MaxPointUser`) VALUES
(1, 1000, 5000000, 1000);

-- --------------------------------------------------------

--
-- Table structure for table `ParkingTrans`
--

CREATE TABLE `ParkingTrans` (
  `TransactionNumber` varchar(255) NOT NULL DEFAULT '',
  `CardNo` varchar(255) DEFAULT NULL,
  `TimeStam` datetime DEFAULT NULL,
  `TransactionTypeID` varchar(255) DEFAULT NULL,
  `Descriptions` varchar(255) DEFAULT NULL,
  `Note` varchar(255) DEFAULT NULL,
  `Debet` float DEFAULT NULL,
  `Kredit` float DEFAULT NULL,
  `UserID` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `PricingDailyMax`
--

CREATE TABLE `PricingDailyMax` (
  `MaxID` varchar(20) NOT NULL,
  `DayName` varchar(20) NOT NULL,
  `MaxPrice` int(20) NOT NULL,
  `VehicleID` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `PricingDailyMax`
--

INSERT INTO `PricingDailyMax` (`MaxID`, `DayName`, `MaxPrice`, `VehicleID`) VALUES
('max001', 'Monday', 30000, 'C'),
('max002', 'Tuesday', 30000, 'C'),
('max003', 'Wednesday', 30000, 'C'),
('max004', 'Thursday', 30000, 'C'),
('max005', 'Friday', 30000, 'C'),
('max006', 'Saturday', 20000, 'C'),
('max007', 'Sunday', 20000, 'C'),
('max008', 'Public Holiday', 20000, 'C');

-- --------------------------------------------------------

--
-- Table structure for table `PricingScheme`
--

CREATE TABLE `PricingScheme` (
  `PricingSchemeID` varchar(20) NOT NULL,
  `Max` int(20) NOT NULL,
  `GPIn` int(10) NOT NULL,
  `GPOut` int(10) NOT NULL,
  `ParkInterval` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `PricingScheme`
--

INSERT INTO `PricingScheme` (`PricingSchemeID`, `Max`, `GPIn`, `GPOut`, `ParkInterval`) VALUES
('scheme01', 5000, 15, 30, 30),
('scheme02', 0, 15, 30, 60);

-- --------------------------------------------------------

--
-- Table structure for table `PricingSchemeDay`
--

CREATE TABLE `PricingSchemeDay` (
  `SchemeDayID` varchar(20) NOT NULL DEFAULT '',
  `DayName` varchar(20) DEFAULT NULL,
  `ParkInHour` int(11) DEFAULT NULL,
  `PricingSchemeID` varchar(20) DEFAULT NULL,
  `VehicleID` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `PricingSchemeDay`
--

INSERT INTO `PricingSchemeDay` (`SchemeDayID`, `DayName`, `ParkInHour`, `PricingSchemeID`, `VehicleID`) VALUES
('fri01', 'Friday', 0, 'scheme01', 'C'),
('fri02', 'Friday', 1, 'scheme01', 'C'),
('fri03', 'Friday', 2, 'scheme01', 'C'),
('fri04', 'Friday', 3, 'scheme01', 'C'),
('fri05', 'Friday', 4, 'scheme01', 'C'),
('fri06', 'Friday', 5, 'scheme01', 'C'),
('fri07', 'Friday', 6, 'scheme02', 'C'),
('fri08', 'Friday', 7, 'scheme02', 'C'),
('fri09', 'Friday', 8, 'scheme02', 'C'),
('fri10', 'Friday', 9, 'scheme02', 'C'),
('fri11', 'Friday', 10, 'scheme02', 'C'),
('fri12', 'Friday', 11, 'scheme02', 'C'),
('fri13', 'Friday', 12, 'scheme02', 'C'),
('fri14', 'Friday', 13, 'scheme02', 'C'),
('fri15', 'Friday', 14, 'scheme02', 'C'),
('fri16', 'Friday', 15, 'scheme02', 'C'),
('fri17', 'Friday', 16, 'scheme02', 'C'),
('fri18', 'Friday', 17, 'scheme02', 'C'),
('fri19', 'Friday', 18, 'scheme02', 'C'),
('fri20', 'Friday', 19, 'scheme02', 'C'),
('fri21', 'Friday', 20, 'scheme02', 'C'),
('fri22', 'Friday', 21, 'scheme01', 'C'),
('fri23', 'Friday', 22, 'scheme01', 'C'),
('fri24', 'Friday', 23, 'scheme01', 'C'),
('mon01', 'Monday', 0, 'scheme01', 'C'),
('mon02', 'Monday', 1, 'scheme01', 'C'),
('mon03', 'Monday', 2, 'scheme01', 'C'),
('mon04', 'Monday', 3, 'scheme01', 'C'),
('mon05', 'Monday', 4, 'scheme01', 'C'),
('mon06', 'Monday', 5, 'scheme01', 'C'),
('mon07', 'Monday', 6, 'scheme02', 'C'),
('mon08', 'Monday', 7, 'scheme02', 'C'),
('mon09', 'Monday', 8, 'scheme02', 'C'),
('mon10', 'Monday', 9, 'scheme02', 'C'),
('mon11', 'Monday', 10, 'scheme02', 'C'),
('mon12', 'Monday', 11, 'scheme02', 'C'),
('mon13', 'Monday', 12, 'scheme02', 'C'),
('mon14', 'Monday', 13, 'scheme02', 'C'),
('mon15', 'Monday', 14, 'scheme02', 'C'),
('mon16', 'Monday', 15, 'scheme02', 'C'),
('mon17', 'Monday', 16, 'scheme02', 'C'),
('mon18', 'Monday', 17, 'scheme02', 'C'),
('mon19', 'Monday', 18, 'scheme02', 'C'),
('mon20', 'Monday', 19, 'scheme02', 'C'),
('mon21', 'Monday', 20, 'scheme02', 'C'),
('mon22', 'Monday', 21, 'scheme01', 'C'),
('mon23', 'Monday', 22, 'scheme01', 'C'),
('mon24', 'Monday', 23, 'scheme01', 'C'),
('phd01', 'PublicHoliday', 0, 'scheme01', 'C'),
('phd02', 'PublicHoliday', 1, 'scheme01', 'C'),
('phd03', 'PublicHoliday', 2, 'scheme01', 'C'),
('phd04', 'PublicHoliday', 3, 'scheme01', 'C'),
('phd05', 'PublicHoliday', 4, 'scheme01', 'C'),
('phd06', 'PublicHoliday', 5, 'scheme01', 'C'),
('phd07', 'PublicHoliday', 6, 'scheme01', 'C'),
('phd08', 'PublicHoliday', 7, 'scheme01', 'C'),
('phd09', 'PublicHoliday', 8, 'scheme01', 'C'),
('phd10', 'PublicHoliday', 9, 'scheme01', 'C'),
('phd11', 'PublicHoliday', 10, 'scheme01', 'C'),
('phd12', 'PublicHoliday', 11, 'scheme01', 'C'),
('phd13', 'PublicHoliday', 12, 'scheme01', 'C'),
('phd14', 'PublicHoliday', 13, 'scheme02', 'C'),
('phd15', 'PublicHoliday', 14, 'scheme02', 'C'),
('phd16', 'PublicHoliday', 15, 'scheme02', 'C'),
('phd17', 'PublicHoliday', 16, 'scheme02', 'C'),
('phd18', 'PublicHoliday', 17, 'scheme02', 'C'),
('phd19', 'PublicHoliday', 18, 'scheme02', 'C'),
('phd20', 'PublicHoliday', 19, 'scheme02', 'C'),
('phd21', 'PublicHoliday', 20, 'scheme02', 'C'),
('phd22', 'PublicHoliday', 21, 'scheme01', 'C'),
('phd23', 'PublicHoliday', 22, 'scheme01', 'C'),
('phd24', 'PublicHoliday', 23, 'scheme01', 'C'),
('sat01', 'Saturday', 0, 'scheme01', 'C'),
('sat02', 'Saturday', 1, 'scheme01', 'C'),
('sat03', 'Saturday', 2, 'scheme01', 'C'),
('sat04', 'Saturday', 3, 'scheme01', 'C'),
('sat05', 'Saturday', 4, 'scheme01', 'C'),
('sat06', 'Saturday', 5, 'scheme01', 'C'),
('sat07', 'Saturday', 6, 'scheme01', 'C'),
('sat08', 'Saturday', 7, 'scheme01', 'C'),
('sat09', 'Saturday', 8, 'scheme01', 'C'),
('sat10', 'Saturday', 9, 'scheme01', 'C'),
('sat11', 'Saturday', 10, 'scheme01', 'C'),
('sat12', 'Saturday', 11, 'scheme01', 'C'),
('sat13', 'Saturday', 12, 'scheme01', 'C'),
('sat14', 'Saturday', 13, 'scheme02', 'C'),
('sat15', 'Saturday', 14, 'scheme02', 'C'),
('sat16', 'Saturday', 15, 'scheme02', 'C'),
('sat17', 'Saturday', 16, 'scheme02', 'C'),
('sat18', 'Saturday', 17, 'scheme02', 'C'),
('sat19', 'Saturday', 18, 'scheme02', 'C'),
('sat20', 'Saturday', 19, 'scheme02', 'C'),
('sat21', 'Saturday', 20, 'scheme02', 'C'),
('sat22', 'Saturday', 21, 'scheme01', 'C'),
('sat23', 'Saturday', 22, 'scheme01', 'C'),
('sat24', 'Saturday', 23, 'scheme01', 'C'),
('sun01', 'Sunday', 0, 'scheme01', 'C'),
('sun02', 'Sunday', 1, 'scheme01', 'C'),
('sun03', 'Sunday', 2, 'scheme01', 'C'),
('sun04', 'Sunday', 3, 'scheme01', 'C'),
('sun05', 'Sunday', 4, 'scheme01', 'C'),
('sun06', 'Sunday', 5, 'scheme01', 'C'),
('sun07', 'Sunday', 6, 'scheme01', 'C'),
('sun08', 'Sunday', 7, 'scheme01', 'C'),
('sun09', 'Sunday', 8, 'scheme01', 'C'),
('sun10', 'Sunday', 9, 'scheme01', 'C'),
('sun11', 'Sunday', 10, 'scheme01', 'C'),
('sun12', 'Sunday', 11, 'scheme01', 'C'),
('sun13', 'Sunday', 12, 'scheme01', 'C'),
('sun14', 'Sunday', 13, 'scheme02', 'C'),
('sun15', 'Sunday', 14, 'scheme02', 'C'),
('sun16', 'Sunday', 15, 'scheme02', 'C'),
('sun17', 'Sunday', 16, 'scheme02', 'C'),
('sun18', 'Sunday', 17, 'scheme02', 'C'),
('sun19', 'Sunday', 18, 'scheme02', 'C'),
('sun20', 'Sunday', 19, 'scheme02', 'C'),
('sun21', 'Sunday', 20, 'scheme02', 'C'),
('sun22', 'Sunday', 21, 'scheme01', 'C'),
('sun23', 'Sunday', 22, 'scheme01', 'C'),
('sun24', 'Sunday', 23, 'scheme01', 'C'),
('thu01', 'Thursday', 0, 'scheme01', 'C'),
('thu02', 'Thursday', 1, 'scheme01', 'C'),
('thu03', 'Thursday', 2, 'scheme01', 'C'),
('thu04', 'Thursday', 3, 'scheme01', 'C'),
('thu05', 'Thursday', 4, 'scheme01', 'C'),
('thu06', 'Thursday', 5, 'scheme01', 'C'),
('thu07', 'Thursday', 6, 'scheme02', 'C'),
('thu08', 'Thursday', 7, 'scheme02', 'C'),
('thu09', 'Thursday', 8, 'scheme02', 'C'),
('thu10', 'Thursday', 9, 'scheme02', 'C'),
('thu11', 'Thursday', 10, 'scheme01', 'C'),
('thu12', 'Thursday', 11, 'scheme02', 'C'),
('thu13', 'Thursday', 12, 'scheme02', 'C'),
('thu14', 'Thursday', 13, 'scheme02', 'C'),
('thu15', 'Thursday', 14, 'scheme02', 'C'),
('thu16', 'Thursday', 15, 'scheme02', 'C'),
('thu17', 'Thursday', 16, 'scheme02', 'C'),
('thu18', 'Thursday', 17, 'scheme02', 'C'),
('thu19', 'Thursday', 18, 'scheme02', 'C'),
('thu20', 'Thursday', 19, 'scheme02', 'C'),
('thu21', 'Thursday', 20, 'scheme02', 'C'),
('thu22', 'Thursday', 21, 'scheme01', 'C'),
('thu23', 'Thursday', 22, 'scheme01', 'C'),
('thu24', 'Thursday', 23, 'scheme01', 'C'),
('tue01', 'Tuesday', 0, 'scheme01', 'C'),
('tue02', 'Tuesday', 1, 'scheme01', 'C'),
('tue03', 'Tuesday', 2, 'scheme01', 'C'),
('tue04', 'Tuesday', 3, 'scheme01', 'C'),
('tue05', 'Tuesday', 4, 'scheme01', 'C'),
('tue06', 'Tuesday', 5, 'scheme01', 'C'),
('tue07', 'Tuesday', 6, 'scheme02', 'C'),
('tue08', 'Tuesday', 7, 'scheme02', 'C'),
('tue09', 'Tuesday', 8, 'scheme02', 'C'),
('tue10', 'Tuesday', 9, 'scheme02', 'C'),
('tue11', 'Tuesday', 10, 'scheme02', 'C'),
('tue12', 'Tuesday', 11, 'scheme02', 'C'),
('tue13', 'Tuesday', 12, 'scheme02', 'C'),
('tue14', 'Tuesday', 13, 'scheme02', 'C'),
('tue15', 'Tuesday', 14, 'scheme02', 'C'),
('tue16', 'Tuesday', 15, 'scheme02', 'C'),
('tue17', 'Tuesday', 16, 'scheme02', 'C'),
('tue18', 'Tuesday', 17, 'scheme02', 'C'),
('tue19', 'Tuesday', 18, 'scheme02', 'C'),
('tue20', 'Tuesday', 19, 'scheme02', 'C'),
('tue21', 'Tuesday', 20, 'scheme02', 'C'),
('tue22', 'Tuesday', 21, 'scheme01', 'C'),
('tue23', 'Tuesday', 22, 'scheme01', 'C'),
('tue24', 'Tuesday', 23, 'scheme01', 'C'),
('wed01', 'Wednesday', 0, 'scheme01', 'C'),
('wed02', 'Wednesday', 1, 'scheme01', 'C'),
('wed03', 'Wednesday', 2, 'scheme01', 'C'),
('wed04', 'Wednesday', 3, 'scheme01', 'C'),
('wed05', 'Wednesday', 4, 'scheme01', 'C'),
('wed06', 'Wednesday', 5, 'scheme01', 'C'),
('wed07', 'Wednesday', 6, 'scheme02', 'C'),
('wed08', 'Wednesday', 7, 'scheme02', 'C'),
('wed09', 'Wednesday', 8, 'scheme02', 'C'),
('wed10', 'Wednesday', 9, 'scheme02', 'C'),
('wed11', 'Wednesday', 10, 'scheme02', 'C'),
('wed12', 'Wednesday', 11, 'scheme02', 'C'),
('wed13', 'Wednesday', 12, 'scheme02', 'C'),
('wed14', 'Wednesday', 13, 'scheme02', 'C'),
('wed15', 'Wednesday', 14, 'scheme02', 'C'),
('wed16', 'Wednesday', 15, 'scheme02', 'C'),
('wed17', 'Wednesday', 16, 'scheme02', 'C'),
('wed18', 'Wednesday', 17, 'scheme02', 'C'),
('wed19', 'Wednesday', 18, 'scheme02', 'C'),
('wed20', 'Wednesday', 19, 'scheme02', 'C'),
('wed21', 'Wednesday', 20, 'scheme02', 'C'),
('wed22', 'Wednesday', 21, 'scheme01', 'C'),
('wed23', 'Wednesday', 22, 'scheme01', 'C'),
('wed24', 'Wednesday', 23, 'scheme01', 'C');

-- --------------------------------------------------------

--
-- Table structure for table `PricingSchemeDetail`
--

CREATE TABLE `PricingSchemeDetail` (
  `SchemeDetailID` varchar(50) NOT NULL,
  `PricingSchemeID` varchar(50) NOT NULL,
  `IntervalNum` int(10) NOT NULL,
  `Price` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `PricingSchemeDetail`
--

INSERT INTO `PricingSchemeDetail` (`SchemeDetailID`, `PricingSchemeID`, `IntervalNum`, `Price`) VALUES
('sch01-001', 'scheme01', 1, 1000),
('sch01-002', 'scheme01', 2, 1000),
('sch01-003', 'scheme01', 3, 1000),
('sch01-004', 'scheme01', 4, 1000),
('sch02-001', 'scheme02', 1, 1000),
('sch02-002', 'scheme02', 2, 2000),
('sch02-003', 'scheme02', 3, 3000),
('sch02-004', 'scheme02', 4, 4000);

-- --------------------------------------------------------

--
-- Table structure for table `ProductMemberSold`
--

CREATE TABLE `ProductMemberSold` (
  `ProductID` varchar(255) DEFAULT NULL,
  `MemberID` varchar(255) DEFAULT NULL,
  `CardNo` varchar(255) DEFAULT NULL,
  `BuyDate` datetime DEFAULT NULL,
  `ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Profil`
--

CREATE TABLE `Profil` (
  `Code` varchar(255) NOT NULL DEFAULT '',
  `Email` varchar(255) DEFAULT NULL,
  `Nama` varchar(255) DEFAULT NULL,
  `NickName` varchar(255) DEFAULT NULL,
  `Alamat` varchar(255) DEFAULT NULL,
  `ProvinsiID` varchar(255) DEFAULT NULL,
  `KotaID` varchar(255) DEFAULT NULL,
  `Logo` varchar(255) DEFAULT NULL,
  `Telp` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Profil`
--

INSERT INTO `Profil` (`Code`, `Email`, `Nama`, `NickName`, `Alamat`, `ProvinsiID`, `KotaID`, `Logo`, `Telp`) VALUES
('SP', 'rachman.latif@gmail.com', 'Sky Parking', 'Sky', 'Tangerang', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `TransList`
--

CREATE TABLE `TransList` (
  `TransNo` varchar(255) NOT NULL DEFAULT '',
  `AccountDebet` varchar(255) DEFAULT NULL,
  `AccountKredit` varchar(255) DEFAULT NULL,
  `Debet` varchar(255) DEFAULT NULL,
  `Kredit` varchar(255) DEFAULT NULL,
  `Amount` float DEFAULT NULL,
  `Date` datetime DEFAULT NULL,
  `TransactionTypeCode` varchar(255) DEFAULT NULL,
  `RefNumber` varchar(255) DEFAULT NULL,
  `Note` text,
  `Message` text,
  `Status` int(11) DEFAULT NULL,
  `UserID` varchar(255) DEFAULT NULL,
  `ApprovedBy` varchar(255) DEFAULT NULL,
  `TransID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `VehicleType`
--

CREATE TABLE `VehicleType` (
  `VehicleID` varchar(255) NOT NULL DEFAULT '',
  `Name` varchar(255) DEFAULT NULL,
  `Status` int(11) DEFAULT NULL,
  `isVisible` int(11) DEFAULT NULL,
  `Code` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `VehicleType`
--

INSERT INTO `VehicleType` (`VehicleID`, `Name`, `Status`, `isVisible`, `Code`) VALUES
('VHC001', 'Motor', 1, 1, 'C'),
('VHC002', 'Mobil', 1, 1, 'B'),
('VHC003', 'Truck', 1, 0, 'T'),
('VHC004', 'VIP Mobil', 1, 1, 'V'),
('VHC005', 'Valet', 1, 0, 'P'),
('VHC006', 'VIP Motor', 1, 1, 'Z');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Location`
--
ALTER TABLE `Location`
  ADD PRIMARY KEY (`LocationID`),
  ADD KEY `Code` (`Code`);

--
-- Indexes for table `LocationGate`
--
ALTER TABLE `LocationGate`
  ADD PRIMARY KEY (`LocationGateID`),
  ADD UNIQUE KEY `LocationID` (`LocationID`);

--
-- Indexes for table `LocationGateDetail`
--
ALTER TABLE `LocationGateDetail`
  ADD PRIMARY KEY (`LocationGateDetailID`),
  ADD KEY `LocationGateID` (`LocationGateID`),
  ADD KEY `VehicleID` (`VehicleID`);

--
-- Indexes for table `LocationParkingProduct`
--
ALTER TABLE `LocationParkingProduct`
  ADD PRIMARY KEY (`ProductID`);

--
-- Indexes for table `LocationParkingProductTopUp`
--
ALTER TABLE `LocationParkingProductTopUp`
  ADD PRIMARY KEY (`ProductID`);

--
-- Indexes for table `MasterCard`
--
ALTER TABLE `MasterCard`
  ADD PRIMARY KEY (`CardID`),
  ADD KEY `CardNo` (`CardNo`),
  ADD KEY `Barcode` (`Barcode`),
  ADD KEY `RFIDNo` (`RFIDNo`);

--
-- Indexes for table `MemberCard`
--
ALTER TABLE `MemberCard`
  ADD PRIMARY KEY (`CardNo`),
  ADD KEY `MemberID` (`MemberID`);

--
-- Indexes for table `MemberData`
--
ALTER TABLE `MemberData`
  ADD PRIMARY KEY (`CardNo`),
  ADD KEY `LocationID` (`LocationID`),
  ADD KEY `PlateNumber` (`PlateNumber`),
  ADD KEY `KTP` (`KTP`);

--
-- Indexes for table `MemberList`
--
ALTER TABLE `MemberList`
  ADD PRIMARY KEY (`MemberID`),
  ADD KEY `KTP` (`KTP`),
  ADD KEY `Email` (`Email`),
  ADD KEY `Phone` (`Phone`),
  ADD KEY `NIP` (`NIP`);

--
-- Indexes for table `MemberLocation`
--
ALTER TABLE `MemberLocation`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `MemberID` (`MemberID`),
  ADD KEY `LocationID` (`LocationID`),
  ADD KEY `VehicleID` (`VehicleID`),
  ADD KEY `CardNo` (`CardNo`);

--
-- Indexes for table `MemberLocationMonth`
--
ALTER TABLE `MemberLocationMonth`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `CardNo` (`CardNo`),
  ADD KEY `LocationID` (`LocationID`),
  ADD KEY `VehicleID` (`VehicleID`);

--
-- Indexes for table `ParkingList`
--
ALTER TABLE `ParkingList`
  ADD PRIMARY KEY (`TransID`),
  ADD KEY `CardNo` (`CardNo`),
  ADD KEY `LocationIn` (`GateIn`),
  ADD KEY `LocationOut` (`GateOut`),
  ADD KEY `Code` (`Code`),
  ADD KEY `VehicleType` (`VehicleType`);

--
-- Indexes for table `ParkingListPayment`
--
ALTER TABLE `ParkingListPayment`
  ADD PRIMARY KEY (`PaymentID`),
  ADD KEY `TransID` (`TransID`);

--
-- Indexes for table `ParkingTrans`
--
ALTER TABLE `ParkingTrans`
  ADD PRIMARY KEY (`TransactionNumber`),
  ADD KEY `CardNo` (`CardNo`),
  ADD KEY `TransactionTypeID` (`TransactionTypeID`);

--
-- Indexes for table `PricingDailyMax`
--
ALTER TABLE `PricingDailyMax`
  ADD PRIMARY KEY (`MaxID`),
  ADD KEY `DayName` (`DayName`);

--
-- Indexes for table `PricingScheme`
--
ALTER TABLE `PricingScheme`
  ADD PRIMARY KEY (`PricingSchemeID`);

--
-- Indexes for table `PricingSchemeDay`
--
ALTER TABLE `PricingSchemeDay`
  ADD PRIMARY KEY (`SchemeDayID`),
  ADD KEY `DayName` (`DayName`),
  ADD KEY `PricingSchemeID` (`PricingSchemeID`),
  ADD KEY `VehicleID` (`VehicleID`);

--
-- Indexes for table `PricingSchemeDetail`
--
ALTER TABLE `PricingSchemeDetail`
  ADD PRIMARY KEY (`SchemeDetailID`),
  ADD KEY `PricingSchemeID` (`PricingSchemeID`);

--
-- Indexes for table `ProductMemberSold`
--
ALTER TABLE `ProductMemberSold`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `CardNo` (`CardNo`),
  ADD KEY `MemberID` (`MemberID`);

--
-- Indexes for table `Profil`
--
ALTER TABLE `Profil`
  ADD PRIMARY KEY (`Code`),
  ADD KEY `ProvinsiID` (`ProvinsiID`),
  ADD KEY `KotaID` (`KotaID`),
  ADD KEY `Email` (`Email`),
  ADD KEY `Telp` (`Telp`);

--
-- Indexes for table `TransList`
--
ALTER TABLE `TransList`
  ADD PRIMARY KEY (`TransID`),
  ADD KEY `TransactionTypeCode` (`TransactionTypeCode`),
  ADD KEY `TransNo` (`TransNo`),
  ADD KEY `UserID` (`UserID`),
  ADD KEY `AccountDebet` (`AccountDebet`),
  ADD KEY `AccountKredit` (`AccountKredit`);

--
-- Indexes for table `VehicleType`
--
ALTER TABLE `VehicleType`
  ADD PRIMARY KEY (`VehicleID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `LocationGateDetail`
--
ALTER TABLE `LocationGateDetail`
  MODIFY `LocationGateDetailID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `MemberLocation`
--
ALTER TABLE `MemberLocation`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ParkingList`
--
ALTER TABLE `ParkingList`
  MODIFY `TransID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `ProductMemberSold`
--
ALTER TABLE `ProductMemberSold`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `TransList`
--
ALTER TABLE `TransList`
  MODIFY `TransID` int(11) NOT NULL AUTO_INCREMENT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
