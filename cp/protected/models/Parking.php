<?php

class Parking extends CFormModel
{

    public function getTransNo($cid){
        $response = array(
            'status'=>TRUE,
            'no'=>'',
        );

        $query = "select * from Koneksi where CompanyID = '".$cid."' and Status = 1";
        $conn =  Yii::app()->db->createCommand($query)->queryAll();
        if($conn != null){
            $database = $conn[0]['DB'];

            $init = 'TL'.$cid.date('ymd');

            $a = "select TransNo from ".$database.".TransList where TransNo like '%$init%' order by TransNo Desc limit 1";
            $b =  Yii::app()->db->createCommand($a)->queryAll();
            if($b == null){
                $response['no'] = 'TL'.$cid.date('ymd').'000001';
            }
            else{
                $transno = $b[0]['TransNo'];

                $a = substr($transno,10,6);
                $b = $a+1;
                if($b < 10){
                    $no = '00000'.$b;
                }
                else if($b < 100){
                    $no = '0000'.$b;
                }
                else if($b < 1000){
                    $no = '000'.$b;
                }
                else if($b < 10000){
                    $no = '00'.$b;
                }
                else if($b < 100000){
                    $no = '0'.$b;
                }
                else{
                    $no = $b;
                }

                $response['no'] = 'TL'.$cid.date('ymd').$no;
            }

            $aa = "select TransNo from ".$database.".TransList where TransNo = '".$response['no']."'";
            $bb =  Yii::app()->db->createCommand($aa)->queryAll();
            if($bb != null){
                $response['status'] = FALSE;
            }

            return $response;
        }
    }

    public function getTransID($cid){
        $response = array(
            'status'=>TRUE,
            'no'=>'',
        );

        $query = "select * from Koneksi where CompanyID = '".$cid."' and Status = 1";
        $conn =  Yii::app()->db->createCommand($query)->queryAll();
        if($conn != null){
            $database = $conn[0]['DB'];

            $init = 'PL'.$cid.date('ymd');

            $a = "select TransID from ".$database.".ParkingList where TransID like '%$init%' order by TransID Desc limit 1";
            $b =  Yii::app()->db->createCommand($a)->queryAll();
            if($b == null){
                $response['no'] = 'PL'.$cid.date('ymd').'000001';
            }
            else{
                $transid = $b[0]['TransID'];

                $a = substr($transid,10,6);
                $b = $a+1;
                if($b < 10){
                    $no = '00000'.$b;
                }
                else if($b < 100){
                    $no = '0000'.$b;
                }
                else if($b < 1000){
                    $no = '000'.$b;
                }
                else if($b < 10000){
                    $no = '00'.$b;
                }
                else if($b < 100000){
                    $no = '0'.$b;
                }
                else{
                    $no = $b;
                }

                $response['no'] = 'PL'.$cid.date('ymd').$no;
            }

            $aa = "select TransID from ".$database.".ParkingList where TransID = '".$response['no']."'";
            $bb =  Yii::app()->db->createCommand($aa)->queryAll();
            if($bb != null){
                $response['status'] = FALSE;
            }

            return $response;
        }
    }

    public function getInquiry($ticket){
        $response = array(
            'status'=>FALSE,
            'message'=>'',
            'statusCode'=>'',
        );

        $cid = substr($ticket,-7,2);

        $query = "select * from Koneksi where CompanyID = '".$cid."' and Status = 1";
        $conn =  Yii::app()->db->createCommand($query)->queryAll();
        if($conn != null){
            $database = $conn[0]['DB'];

            //parking list
            $p = "select *, DATE(timein) as TanggalMasuk, TIME(timein) as JamMasuk from ".$database.".ParkingList where CardNo='".$ticket."' and Status in (0,1,2)";
            $l =  Yii::app()->db->createCommand($p)->queryAll();
            if($l != null){
                $jammasukawal = $l[0]['JamMasuk'];
                $jamkeluarfinal = $l[0]['TimeOut'];
                $gracePeriod = $l[0]['GracePeriodOut'];

                if($gracePeriod != '' or $gracePeriod != null){
                    if($gracePeriod < date('Y-m-d H:i:s')){
                        $jammasukawal = date('H:i:s', strtotime($gracePeriod));
                    }
                }

                $tanggalmasuk = $l[0]['TanggalMasuk'];
                $tanggalkeluar = date('Y-m-d', strtotime('+ 1 day'));
                $jeniskendaraan = $l[0]['VehicleType'];
                $transid = $l[0]['TransID'];
                $gp = $l[0]['GPOut'];
                $dur = $l[0]['Duration'];
                $status = $l[0]['Status'];
                $ti = $l[0]['TimeIn'];
                $gi = $l[0]['GateIn'];
                $isFree = $l[0]['isFree'];

                $period = new DatePeriod(
                     new DateTime(".$tanggalmasuk."),
                     new DateInterval('P1D'),
                     new DateTime(".$tanggalkeluar.")
                );

                $totalbayar = 0;
                $lasttime = array();
                foreach ($period as $key => $value) {
                    $tanggal = $value->format('Y-m-d');
                    $day = date('l', strtotime($tanggal));

                    if($tanggal < date('Y-m-d')){
                        $jammasuk = $tanggal.' '.$jammasukawal;
                        $jamkeluar = $tanggal.' 23:59:59';
                    }
                    else if($tanggal == date('Y-m-d') and $lasttime != null){
                        $jammasuk = $tanggal.' '.date('H:i:s', strtotime($lasttime[0]));
                        $jamkeluar = $tanggal.' '.date('H:i:s');
                    }
                    else{
                        $jammasuk = $tanggal.' '.$jammasukawal;
                        $jamkeluar = $tanggal.' '.date('H:i:s');
                    }

                    $hitung = false;
                    if($gracePeriod != '' or $gracePeriod != null){
                        if($gracePeriod < date('Y-m-d H:i:s')){
                            $hitung = true;
                        }
                    }

                    if($status == '0'){
                        $hitung = true;
                    }

                    $arr = array();
                    if($hitung){
                        while($jammasuk < $jamkeluar){
                            $t = "select * from ".$database.".PricingSchemeDay where VehicleID = '".$jeniskendaraan."' and DayName='".$day."' and ParkInHour='".date('H', strtotime($jammasuk))."'";
                            $h =  Yii::app()->db->createCommand($t)->queryAll();
                            if($h != null){
                                $interval = 0;
                                $k = "select * from ".$database.".PricingScheme where PricingSchemeID='".$h[0]['PricingSchemeID']."'";
                                $ll =  Yii::app()->db->createCommand($k)->queryAll();
                                if($ll != null){
                                    $interval = $ll[0]['ParkInterval'];
                                }

                                array_push($arr, $ll[0]['PricingSchemeID']);

                                //echo 'Scheme '.$h[0]['PricingSchemeID'].' - '.$jammasuk.' interval '.$interval.'<br>';
                                $jammasuk = date('Y-m-d H:i:s', strtotime($jammasuk.'+ '.$interval.' minutes'));
                            }
                        }
                    }

                    array_push($lasttime, $jammasuk);

                    $result = array();
                    $prev_value = array('value' => null, 'count' => null);
                    foreach ($arr as $val) {
                        if ($prev_value['value'] != $val) {
                            unset($prev_value);
                            $prev_value = array('value' => $val, 'count' => 0);
                            $result[] =& $prev_value;
                        }

                        $prev_value['count']++;
                    }

                    //Detail Interval Harga
                    $biayaparkir = 0;
                    foreach($result as $row){
                        $count = $row['count'];
                        $value = $row['value'];

                        $maxprice = 0;
                        $maxinterval = 0;
                        $msql = "select Price, IntervalNum from ".$database.".PricingSchemeDetail where PricingSchemeID='".$value."' and IntervalNum = (select max(IntervalNum) from ".$database.".PricingSchemeDetail where PricingSchemeID='".$value."')";
                        $mrow =  Yii::app()->db->createCommand($msql)->queryAll();
                        if($mrow != null){
                            $maxprice = $mrow[0]['Price'];
                            $maxinterval = $mrow[0]['IntervalNum'];
                        }

                        for($a=1;$a<=$count;$a++){
                            $int = $a;
                            if($a > $maxinterval){
                                $int = $maxinterval;
                            }

                            $mmm = "select* from ".$database.".PricingSchemeDetail where PricingSchemeID='".$value."' and IntervalNum = '".$int."'";
                            $nnn =  Yii::app()->db->createCommand($mmm)->queryAll();
                            if($nnn != null){
                                //echo 'Interval ke: '.$a.' - '.$nnn[0]['Price'].'<br>';
                                $biayaparkir+=$nnn[0]['Price'];
                            }
                        }
                    }

                    //max daily
                    $maxdaily = 0;
                    $uuu = "select* from ".$database.".PricingDailyMax where DayName='".$day."' and VehicleID = '".$jeniskendaraan."'";
                    $iii =  Yii::app()->db->createCommand($uuu)->queryAll();
                    if($iii != null){
                       if($biayaparkir > $iii[0]['MaxPrice']){
                         $maxdaily = $iii[0]['MaxPrice'];
                       }
                       else{
                         $maxdaily = $biayaparkir;
                       }
                    }

                    $totalbayar+=$maxdaily;
                    //echo 'Total Biaya Parkir hari '.$day.' adalah: '.$maxdaily.'<br>';
                }

                $locationName = '';
                $v = "select a.Nama from ".$database.".Location a inner join ".$database.".LocationGate b on a.LocationID = b.LocationID where b.LocationGateID='".$gi."'";
                $w =  Yii::app()->db->createCommand($v)->queryAll();
                if($w != null){
                    $locationName = $w[0]['Nama'];
                }

                $vehicleName = '';
                $q = "select Name from ".$database.".VehicleType where Code='".$jeniskendaraan."'";
                $z =  Yii::app()->db->createCommand($q)->queryAll();
                if($z != null){
                    $vehicleName = $z[0]['Name'];
                }

                //cek bayar
                $sudahbayar = 0;
                $u = "select IFNULL(SUM(PaidAmount),0) as TotalBayar from ".$database.".ParkingListPayment where TransID='".$transid."'";
                $p =  Yii::app()->db->createCommand($u)->queryAll();
                if($p != null){
                    $sudahbayar = $p[0]['TotalBayar'];
                }

                $finalamount = $totalbayar;

                $response['status'] = TRUE;
                $response['statusCode'] = '000';
                $response['ticketID'] = $ticket;
                $response['timeIn'] = $ti;

                $duration = 0;
                $x = "select TIMESTAMPDIFF(MINUTE,'".$ti."',NOW()) as Duration";
                $y =  Yii::app()->db->createCommand($x)->queryAll();
                if($y != null){
                    $duration = $y[0]['Duration'];
                }

                $response['duration'] = $duration;

                if($status == 0 || $gracePeriod < date('Y-m-d H:i:s') || $finalamount > 0){
                    $response['timeOut'] = $jamkeluar;
                    $response['message'] = 'VALID';
                    $response['paymentStatus'] = 'UNPAID';
                    $response['gracePeriod'] = '';
                }

                if($status == 1){
                    $response['timeOut'] = $jamkeluar;
                    $response['message'] = 'VALID';
                    if($finalamount > 0){
                        $response['paymentStatus'] = 'UNPAID';
                    }
                    else{
                        $response['paymentStatus'] = 'PAID';
                    }
                    $response['gracePeriod'] = $gracePeriod;
                }

                if($status == 2){
                    $response['duration'] = $dur;
                    $response['timeOut'] = $jamkeluarfinal;
                    $response['message'] = 'INVALID';
                    $response['paymentStatus'] = 'FINISH';
                    $response['gracePeriod'] = $gracePeriod;
                }

                $response['amount'] = $finalamount;
                $response['amountPaid'] = $sudahbayar;

                $response['gp'] = $gp;
                $response['isFree'] = $isFree;
                if($isFree == 1){
                  $response['amount'] = 0;
                  $response['paymentStatus'] = 'FREE';
                }

                $lastpaid = '';
                $vv = "select PaidDate from ".$database.".ParkingListPayment where TransID='".$transid."' order by PaidDate desc limit 1";
                $ww =  Yii::app()->db->createCommand($vv)->queryAll();
                if($ww != null){
                    $response['lastPaid'] = $ww[0]['PaidDate'];
                }

                $response['location'] = $locationName;
                $response['vehicleType'] = $vehicleName;
                $response['cid'] = $cid;
            }
            else{
              $response['status'] = TRUE;
              $response['statusCode'] = '001';
              $response['message'] = 'QR Code not found';
            }
        }
        else{
            $response['status'] = TRUE;
            $response['statusCode'] = '202';
            $response['message'] = 'Not found';
        }

        return $response;
    }

    public function decrypt($barcode){
        $keyorigin = "iMijz5XlckKwsQqcHedfLbf66muVMzfI6rl9KBxTScu8BbjoJU3y2t9rFmSHcdfMhQrakszK7F1cHMbQ9w72hQ==";
        $key = $this->getKey($keyorigin);

        $card_id = str_replace(' ','+',$barcode);

        if(strlen($card_id) >= 108){
            $c = base64_decode($card_id);
            $ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
            $iv = substr($c, 0, $ivlen);
            $hmac = substr($c, $ivlen, $sha2len=32);
            $ciphertext_raw = substr($c, $ivlen+$sha2len);
            $original_plaintext = openssl_decrypt($ciphertext_raw, $cipher, $key, $options=OPENSSL_RAW_DATA, $iv);
            $calcmac = hash_hmac('sha256', $ciphertext_raw, $key, $as_binary=true);
            if (hash_equals($hmac, $calcmac))
            {
                return $original_plaintext;
            }
            else{
                return $card_id;
            }
        }
        else{
            return $card_id;
        }
    }

    public function getKey($key){
        $c = base64_decode($key);
        $ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
        $iv = substr($c, 0, $ivlen);
        $hmac = substr($c, $ivlen, $sha2len=32);
        $ciphertext_raw = substr($c, $ivlen+$sha2len);
        $original_plaintext = openssl_decrypt($ciphertext_raw, $cipher, 'c8f6dae75c5e2010d46924649e0c0357', $options=OPENSSL_RAW_DATA, $iv);
        $calcmac = hash_hmac('sha256', $ciphertext_raw, 'c8f6dae75c5e2010d46924649e0c0357', $as_binary=true);
        if (hash_equals($hmac, $calcmac))
        {
            return $original_plaintext;
        }
        else{
            return $key;
        }
    }
}
