<?php

Yii::setPathOfAlias('root',dirname(dirname(dirname(__FILE__))));
Yii::setPathOfAlias('admin',Yii::getPathOfAlias('root.admin'));
Yii::setPathOfAlias('common',Yii::getPathOfAlias('root.common'));
Yii::setPathOfAlias('web',Yii::getPathOfAlias('root'));
Yii::setPathOfAlias('modules',Yii::getPathOfAlias('root.protected.modules'));
Yii::setPathOfAlias('MyForm',Yii::getPathOfAlias('common.extensions.widgets.MyForm'));
Yii::setPathOfAlias('ext',Yii::getPathOfAlias('root.common.extensions'));

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
    'name'=>'My Web Application',
    'sourceLanguage'=>'en',
    'language'=>'id',

    // autoloading model and component classes
    'import'=>array(
        'common.models.*',
        'common.models.enum.*',
        'common.models.forms.*',
        'common.components.*',
        'common.components.yii-mail.*',
        'common.extensions.actions.*',
        'common.extensions.behaviors.*',
        'common.extensions.controllers.*',
        'common.extensions.exceptions.*',
        'common.extensions.filters.*',
        'common.extensions.helpers.*',
        'common.extensions.modules.auditTrail.models.*',
	    'common.extensions.runactions.components.*',
        'common.extensions.validators.*',
        'common.extensions.widgets.*',
        'common.extensions.widgets.yii.*',
        'common.extensions.widgets.Lookup.*',
        'common.services.*',
        'common.mappers.*',
    ),

    'aliases'=>array(
	    'admin'=>'root.admin',
	    'common'=>'root.common',
	    'web'=>'root',
	    'modules'=>'root.protected.modules',
	    'bootstrap'=>'common.extensions.bootstrap',
	    'myForm'=>'common.extensions.widgets.MyForm',
        'xupload'=>'common.extensions.widgets.xupload',
    ),

    // application components
    'components'=>array(
        'root'=>array(
            'class'=>'Root',
        ),
        'mail' => array(
            'class' => 'common.components.yii-mail.YiiMail',
  	        //'swiftMailerClass' => 'root.vendors.swiftMailer',
  	        'swiftMailerClass' => 'common.components.yii-mail.vendors.swiftMailer',
  	        'transportType' => 'smtp',
  	        'transportOptions' => array(
      		      'host' => 'smtp.gmail.com',
      				  'username' => 'konfirmasi@baronang.com',
      				  'password' => 'Baronang-7000',//change password sebelumnya systemct123
      				  'port' => 465,
                //'SMTPAuth'  => true,
    				    //'encryption' => 'ssl',
	              //'timeout'=>'',
	              //'extensionHandlers'=>''
	           ),
  	        'viewPath' => 'application.views.mail',
  	        'logging' => true,
  	        'dryRun' => false
        ),
        'log'=>array(
            'class'=>'CLogRouter',
            'routes'=>array(
                array(
                    'class'=>'CFileLogRoute',
                    'levels'=>'error',
                    'enabled'=>true,
                    'logFile'=>'error.log.'.date('Ymd'),
                ),
                array(
                    'class'=>'CFileLogRoute',
                    'levels'=>'profile',
                    'enabled'=>true,
                    'logFile'=>'profile.log.'.date('Ymd'),
                ),
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'warning',
					'enabled'=>true,
					'logFile'=>'warning.log.'.date('Ymd'),
				),
                array(
                    'class'=>'CWebLogRoute',
                    'enabled'=>false,
                ),
            ),
        ),
    ),

    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params'=>array(
        // this is used in contact page
        //'adminEmail'=>'webmaster@example.com',
        'log'=>true,
        'imageExtension'=>array('jpg','jpeg','gif','png'),
        'fileExtension'=>array('pdf','doc','docx','rar','zip','txt'),
    ),
);
