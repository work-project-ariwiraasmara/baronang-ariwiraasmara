<?php
return array(

	// application components
	'components'=>array(
        /*'db'=>array(
    				'connectionString' => 'mysql:host=35.240.183.132;dbname=paymentgateway',
    				'emulatePrepare' => true,
    				'username' => 'dev',
    				'password' => 'C0b@t3b@k',
    				'charset' => 'utf8',
    		),*/

				'db'=>array(
						'connectionString' => 'mysql:host=localhost;dbname=paymentgateway',
						'emulatePrepare' => true,
						'username' => 'root',
						'password' => 'C0b@t3b@k',
						'charset' => 'utf8',
				),
    ),
);
