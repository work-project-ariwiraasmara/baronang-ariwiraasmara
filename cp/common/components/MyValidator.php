<?php

abstract class MyValidator extends CValidator
{
	/**
	 * @var string
	 */
	public $translationContext = 'validation';

	protected function addError($object, $attribute, $message, $params=array())
	{
		$message = Yii::t($this->translationContext, $message);
		parent::addError($object, $attribute, $message, $params);
	}
}