<?php

class DuplicateDataValidator extends MyValidator
{
	/**
	 * Validates the attribute of the object.
	 * If there is any error, the error message is added to the object.
	 * @param CModel $object the object being validated
	 * @param string $attribute the attribute being validated
	 */
	protected function validateAttribute($object, $attribute)
	{
		$count = $object->count("$attribute=?", array($object->$attribute));
		if($count > 0) {
			$this->addError($object, $attribute, Yii::t('validation', '{attribute} is already exist.'));
		}
	}
}