<?php

class HasCreatedDateBehavior extends CActiveRecordBehavior
{
    public function beforeValidate($event)
    {
        if ($this->owner->isNewRecord) {
            $this->owner->tanggal_dibuat = DateHelper::now();
        }

        return parent::beforeValidate($event);
    }
}