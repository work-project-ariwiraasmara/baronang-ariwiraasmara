<?php

class DateTimePicker extends CWidget
{
	public $id='dateTimePicker';
	public $dateFormat = 'yy-mm-dd';
    public $timeFormat = 'hh:mm:ss';
    public $showSecond = false;
    public $changeMonth = true;
    public $changeYear = true;
    public $baseScriptUrl;
    public $theme = 'smoothness';
    public $jqueryUi = true;
    public $addOn = FALSE;

    public function init()
    {
        if ($this->baseScriptUrl === null)
            $this->baseScriptUrl = Yii::app()->getAssetManager()->publish(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'assets');
    }
		
	public function addConfig($configs)
	{
		foreach ($configs as $key=>$config) {
			$this->_gridview_config[$key] = $config;
		}
	}
	
	protected function registerClientScript()
	{
        $cs = Yii::app()->clientScript;
        $cs->registerCssFile($this->baseScriptUrl .'/anytime.5.0.3.css');
        $cs->registerScriptFile($this->baseScriptUrl . '/anytime.5.0.3.js');

		$originalScriptId = $scriptId = 'dateTimePicker';
		$num = 0;
		while($cs->isScriptRegistered($scriptId)) {
			$num++;
			$scriptId = $originalScriptId . $num;
		}
        $showSecond = $this->showSecond ? 'true' : 'false';
		
		$cs->registerScript($scriptId, "
		    $('#".$this->id."').AnyTime_picker({
		        format: '%Y-%m-%d %H:%i'
		    });
		");
	}

	public function run()
	{
		$this->registerClientScript();
	}
}