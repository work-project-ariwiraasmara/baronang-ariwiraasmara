<?php

Yii::import('zii.widgets.CDetailView');

class RDetailView extends CDetailView
{
	/**
	 * Initializes the detail view.
	 */
	public function init()
	{
        if($this->baseScriptUrl===null)
            $this->baseScriptUrl=Yii::app()->getAssetManager()->publish(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'assets').'/detailview';

        parent::init();
	}
}
