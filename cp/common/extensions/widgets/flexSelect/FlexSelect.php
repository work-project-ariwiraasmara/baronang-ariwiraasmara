<?php

Class FlexSelect extends CWidget{
    
    public $assetsPath;
    public $id='FlexSelect';
    
    public function init(){
        if($this->assetsPath == null)
            $this->assetsPath = Yii::app()->getAssetManager()->publish(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'assets');
    }

    public function addConfig($configs)
    {
            foreach ($configs as $key=>$config) {
                    $this->_gridview_config[$key] = $config;
            }
    }

    protected function registerClientScript()
    {
            $cs = Yii::app()->clientScript;
            $cs->registerCssFile($this->assetsPath .'/css/flexselect.css');
            $cs->registerScriptFile($this->assetsPath .'/js/liquidmetal.js');
            $cs->registerScriptFile($this->assetsPath .'/js/jquery.flexselect.js');
            

            $originalScriptId = $scriptId = 'flexSelect';
            $num = 0;
            while($cs->isScriptRegistered($scriptId)) {
                    $num++;
                    $scriptId = $originalScriptId . $num;
            }

            $cs->registerScript($scriptId, "
                $('#".$this->id."').flexselect();
            ");
    }   

    public function run()
    {
            $this->registerClientScript();
    }
}
?>
