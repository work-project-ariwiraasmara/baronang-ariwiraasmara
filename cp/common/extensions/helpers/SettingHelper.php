<?php

class SettingHelper
{
    /**
     * @param string $id
     * @return string
     */
    public static function getValue($id)
	{
		$setting = Setting::model()->findByPk($id);
		if ($setting !== null)
			return $setting->value;

		return '';
	}

    /**
     * @param string $id
     * @param array $settings
     * @return null|Setting
     */
    public static function findSettingById($id, $settings)
    {
        foreach ($settings as $setting) {
            if ($setting->id == $id) {
                return $setting;
            }
        }

        return null;
    }
}