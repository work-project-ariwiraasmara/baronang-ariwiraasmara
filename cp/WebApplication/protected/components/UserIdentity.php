<?php

class UserIdentity extends CUserIdentity
{
    private $_id;

	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
        $user = Pengguna::model()->find('t.username=? AND t.status<> ? AND t.status<>?', array(
	        $this->username,
	        Pengguna::STATUS_LOCKED,
	        Pengguna::STATUS_BANNED,
        ));

		if($user === null) {
			$this->errorCode=self::ERROR_USERNAME_INVALID;
        } else {
            if($user->password != Pengguna::hashPassword($this->password)) {
                $this->errorCode=self::ERROR_PASSWORD_INVALID;
            } else {
                $this->errorCode=self::ERROR_NONE;
                $this->_id = $user->id_pengguna;
            }
        }

		return !$this->errorCode;
	}

    public function getId()
    {
        return $this->_id;
    }
}