<?php

class ParkingController extends Controller {

    public function getDateAll(){
        $t = microtime(true);
        $micro = sprintf("%06d",($t - floor($t)) * 1000000);
        $d = new DateTime( date('Y-m-d H:i:s.'.$micro, $t) );

        return $d->format("Y-m-d H:i:s.u");
    }

    public function actionInquiry(){
        $response = array(
            'ticket' => '',
            'ticketstatus' => '',
            'intime' => '',
            'duration' => null,
            'tarif' => '',
            'vehicletype' => '',
            'outtime' => '',
            'graceperiod' => null,
            'location' => '',
            'paymentstatus' => '',
        );

        $param = implode(',', $_REQUEST);

        //save log api
        $id = '';
        $qwe = "insert into LogHitApi(ActionFunction, DateHit, Param, Status) values('".Yii::app()->controller->action->id."','".$this->getDateAll()."','".$param."',0)";
        $asd =  Yii::app()->db->createCommand($qwe);
        if($asd->execute()){
            $id = Yii::app()->db->getLastInsertID();
        }

        if(isset($_REQUEST['ticket'])){
            $ticket = $_REQUEST['ticket'];

            $model = new Parking();
            $parking = $model->getInquiry($ticket);
            if($parking != null){
                $statusCode = $parking['statusCode'];
                if($statusCode == '000'){
                    $status = $parking['status'];
                    $isFree = $parking['isFree'];
                    $intime = $parking['timeIn'];
                    $duration = $parking['duration'];
                    $tarif = $parking['amount'];
                    $vehicletype = $parking['vehicleType'];
                    $gp = $parking['gp'];
                    $vehiclename = $parking['vehicleType'];
                    $locationname = $parking['location'];
                    $paymentStatus = $parking['paymentStatus'];
                    $message = $parking['message'];

                    $response['ticket'] = $ticket;
                    $response['ticketstatus'] = $message;

                    if($paymentStatus <> 'FINISH'){
                        $response['intime'] = $intime;
                        $response['duration'] = $duration;
                        $response['tarif'] = $tarif;
                        $response['vehicletype'] = $vehiclename;
                        $response['outtime'] = '';
                        $response['graceperiod'] = $gp;
                        $response['location'] = $locationname;
                        $response['paymentstatus'] = $paymentStatus;
                    }
                }
                else{
                    $response['ticket'] = $ticket;
                    $response['ticketstatus'] = 'INVALID';
                }
            }
            else{
                $response['ticket'] = $ticket;
                $response['ticketstatus'] = 'INVALID';
            }
        }
        else{
            $response['statusCode'] = '404';
            $response['message'] = 'Invalid require data';
        }

        if($id != ''){
            $msg = json_encode($response);
            //save log api
            $qwe = "update LogHitApi set Status = 1, DateProses = '".$this->getDateAll()."',Message = '".$msg."' where LogHitApiID = '".$id."'";
            $asd =  Yii::app()->db->createCommand($qwe);
            $asd->execute();
        }

        echo json_encode($response);
    }

    public function actionPayment(){
        $response = array(
            'ticket' => '',
            'ticketstatus' => '',
            'intime' => '',
            'duration' => null,
            'tarif' => '',
            'vehicletype' => '',
            'outtime' => '',
            'graceperiod' => null,
            'location' => '',
            'paymentstatus' => '',
            'paymentreferenceid' => '',
            'transactionstatus' => '',
        );

        $postdata = file_get_contents("php://input");
        $data = json_decode($postdata, true);

        $param = implode(',',$data);

        //save log api
        $id = '';
        $qwe = "insert into LogHitApi(ActionFunction, DateHit, Param, Status) values('".Yii::app()->controller->action->id."','".$this->getDateAll()."','".$param."',0)";
        $asd =  Yii::app()->db->createCommand($qwe);
        if($asd->execute()){
            $id = Yii::app()->db->getLastInsertID();
        }

        $ticket = $data['ticket'];
        $tarif = $data['tarif'];
        $paymentreferenceid = $data['paymentreferenceid'];

        if(isset($ticket) and isset($tarif) and isset($paymentreferenceid)){

            $model = new Parking();
            $parking = $model->getInquiry($ticket);
            if($parking['status']){
                $statusCode = $parking['statusCode'];
                $status = $parking['status'];
                $isFree = $parking['isFree'];
                $timeIn = $parking['timeIn'];
                $duration = $parking['duration'];
                $amount = $parking['amount'];
                $vehicletype = $parking['vehicleType'];
                $gp = $parking['gracePeriod'];
                $vehiclename = $parking['vehicleType'];
                $locationname = $parking['location'];
                $paymentStatus = $parking['paymentStatus'];
                $message = $parking['message'];

                $tanggal = date('Y-m-d H:i:s');

                $cid = substr($ticket,-7,2);

                $query = "select * from Koneksi where CompanyID = '".$cid."' and Status = 1";
                $conn =  Yii::app()->db->createCommand($query)->queryAll();
                if($conn != null){
                    $database = $conn[0]['DB'];

                    //get Interval
                    $aa = "select abs(datediff(TimeIn, Now())+1) as Intv, GateIn from ".$database.".ParkingList where CardNo='".$ticket."'";
                    $bb =  Yii::app()->db->createCommand($aa)->queryAll();
                    if($bb != null){
                        $calc = date('Y-m-d H:i:s', strtotime($timeIn.' +'.$bb[0]['Intv'].' hour'));
                    }

                    //cek masuk
                    $a = "select * from ".$database.".ParkingList where CardNo='".$ticket."' and Status in (0,1)";
                    $b =  Yii::app()->db->createCommand($a)->queryAll();
                    if($b != null){
                        $gateid = $b[0]['GateIn'];

                        $gp = 30;
                        $er = "select b.GPOut from ".$database.".LocationGate a inner join ".$database.".Location b on a.LocationID = b.LocationID where a.LocationGateID = '".$gateid."'";
                        $df =  Yii::app()->db->createCommand($er)->queryAll();
                        if($df != null){
                            $gp = $df[0]['GPOut'];
                        }
                        $gpout = date('Y-m-d H:i:s', strtotime($tanggal.'+ '.$gp.' minutes'));

                        if($amount == $tarif and $tarif > 0){
                            if($paymentStatus == 'UNPAID'){
                                $transno = $model->getTransNo($cid);
                                if($transno['status']){
                                    $transId = $b[0]['TransID'];

                                    $hh = "update ".$database.".ParkingList set GPOut = '".$gp."', GracePeriodOut = '".$gpout."', Duration = '".$duration."', Status = 1, TotalPaid=TotalPaid+".$amount." where CardNo = '".$ticket."'";
                                    $ii =  Yii::app()->db->createCommand($hh);
                                    $ii->execute();

                                    $kk = "insert into ".$database.".ParkingListPayment(TransID, Method, PaidDate, PaidAmount)values('".$transId."','OVO','".$tanggal."','".$amount."')";
                                    $ll =  Yii::app()->db->createCommand($kk);
                                    $ll->execute();

                                    $ss = "insert into ".$database.".TransList(TransNo, TransID, Debet, Kredit, Amount, TransDate, TransactionType, Note, CreatedBy, RefNumber)values('".$transno['no']."','".$transId."','','".$ticket."','".$amount."','".$tanggal."','PAY','Pembayaran parkir','OVO','".$paymentreferenceid."')";
                                    $xx =  Yii::app()->db->createCommand($ss);
                                    if($xx->execute()){
                                        $response['paymentstatus'] = 'PAID';
                                    }
                                    else{
                                        $response['transactionstatus'] = 3;
                                    }
                                }
                                else{
                                    $response['transactionstatus'] = 3;
                                }
                            }
                            else{
                                $response['paymentstatus'] = $paymentStatus;
                            }

                            if($isFree == 1){
                                $response['transactionstatus'] = 2;
                            }

                            $response['transactionstatus'] = 0;
                            $response['ticket'] = $ticket;
                            $response['ticketstatus'] = $message;
                            $response['intime'] = $timeIn;
                            $response['duration'] = $duration;
                            $response['tarif'] = $tarif;
                            $response['vehicletype'] = $vehiclename;
                            $response['outtime'] = $gpout;
                            $response['graceperiod'] = $gp;
                            $response['location'] = $locationname;
                            $response['paymentreferenceid'] = $paymentreferenceid;
                        }
                        else{
                            $response['transactionstatus'] = 1;

                            if($paymentStatus == 'PAID'){
                                $response['paymentstatus'] = 'PAID';
                                $response['transactionstatus'] = 2;
                            }

                            $response['ticket'] = $ticket;
                            $response['ticketstatus'] = $message;
                            $response['intime'] = $timeIn;
                            $response['duration'] = $duration;
                            $response['tarif'] = $tarif;
                            $response['vehicletype'] = $vehiclename;
                            $response['graceperiod'] = $gp;
                            $response['location'] = $locationname;
                            $response['paymentreferenceid'] = $paymentreferenceid;
                        }
                    }
                    else{
                        $response['ticket'] = $ticket;
                        $response['ticketstatus'] = 'INVALID';
                        $response['paymentreferenceid'] = $paymentreferenceid;
                        $response['transactionstatus'] = 2;
                    }
                }
                else{
                    $response['statusCode'] = '500';
                    $response['message'] = 'Failed connection';
                }
            }
            else{
                $response['ticket'] = $ticket;
                $response['ticketstatus'] = 'INVALID';
                $response['paymentreferenceid'] = $paymentreferenceid;
                $response['transactionstatus'] = 2;
            }
        }
        else{
            $response['statusCode'] = '404';
            $response['message'] = 'Invalid require data';
        }

        if($id != ''){
            $msg = json_encode($response);
            //save log api
            $qwe = "update LogHitApi set Status = 1, DateProses = '".$this->getDateAll()."',Message = '".$msg."' where LogHitApiID = '".$id."'";
            $asd =  Yii::app()->db->createCommand($qwe);
            $asd->execute();
        }

        echo json_encode($response);
    }

}
