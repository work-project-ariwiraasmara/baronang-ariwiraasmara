<p>
  Hi! <?php echo $nama; ?>,<br><br>
  Terima kasih telah bergabung dengan Baronang App.<br>
  Kami perlu memastikan bahwa email anda valid. Harap mengkonfirmasi email anda dengan mengklik link dibawah ini.<br>
  <a href="<?php echo $link; ?>">Konfirmasi</a>
  <br>
  <br>
  <br>
  <br>
  Terima kasih.<br>
  Salam,<br>
  <br>
  <br>
  Baronang";
</p>
