<?php

Yii::import('zii.widgets.grid.CGridView');
Yii::import('zii.widgets.grid.CDataColumn');
Yii::import('zii.widgets.grid.CLinkColumn');
Yii::import('zii.widgets.grid.CButtonColumn');
Yii::import('zii.widgets.grid.CCheckBoxColumn');

class RGridView extends CGridView
{
    public $pagerCssFile;

	/**
	 * Initializes the grid view.
	 */
	public function init()
	{
		if($this->baseScriptUrl===null)
            $this->baseScriptUrl=Yii::app()->getAssetManager()->publish(dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'assets').'/gridview';

        if ($this->pagerCssFile === null) {
            if (!isset($this->pager['cssFile'])) {
                $this->pager['cssFile'] = $this->baseScriptUrl .'/pager.css';
            }
        }

        parent::init();
	}
}