<?php

class LookupBranch extends Lookup
{
	public function init()
	{
		parent::init();		
		$this->initData();
		$this->title = 'Lookup SBU';
	}
	
	public function initData()
	{
		$model = new Branch('search');
		$model->unsetAttributes(); // clear any default values
		if(isset($_GET['Branch']))
			$model->attributes=$_GET['Branch'];
		
		$this->defaultGridViewConfig = array(
			'id'=>'branch-grid',
			'cssFile' => $this->cssFile,
			'dataProvider'=>$model->searchLookup(),
			'filter'=>$model,
			'columns'=>array(
				'name',
			),
		);
	}
}