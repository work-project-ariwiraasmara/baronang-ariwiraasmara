<?php

class NotificationType
{
    const INFORMATION = 'information';
    const ATTENTION = 'attention';
    const SUCCESS = 'success';
    const ERROR = 'error';
}

class NotificationHelper
{
    /**
     * Create notification
     *
     * @param string $message
     * @param null|string $type
     * @return string
     */
    public static function create($message, $type=null)
    {
        if ($type === null) {
            $type = NotificationType::INFORMATION;
        }

        if ($message != '')
            return '<div class="notification '. $type .'">'. $message .'</div>';

        return '';
    }

    /**
     * Create information notification
     *
     * @param string $message
     * @return string
     */
    public static function information($message)
    {
        return self::create($message, NotificationType::INFORMATION);
    }

    /**
     * Create attention notification
     *
     * @param string $message
     * @return string
     */
    public static function attention($message)
    {
        return self::create($message, NotificationType::ATTENTION);
    }

    /**
     * Create success notification
     *
     * @param string $message
     * @return string
     */
    public static function success($message)
    {
        return self::create($message, NotificationType::SUCCESS);
    }

    /**
     * Create error notification
     *
     * @param string $message
     * @return string
     */
    public static function error($message)
    {
        return self::create($message, NotificationType::ERROR);
    }

    /**
     * Create form error summary notification
     *
     * @param CActiveForm $form
     * @param mixed $model
     * @return mixed
     */
    public static function formErrorSummary($form, $model)
    {
        return $form->errorSummary($model, '', '', array(
            'class'=>'notification error'
        ));
    }
}