<?php

class EnumBehavior extends CBehavior
{
	/**
	 * @param null $prefix
	 * @param string $translationContext
	 * @return array
	 */
	public function getEnum($prefix=null, $translationContext='string')
	{
		$class = $this->owner;
		$reflection = new ReflectionClass( get_class( $class ) );

		$list = array();
		$constants = $reflection->getConstants();

		foreach ($constants as $enum=>$number) {
			if ($prefix === null || ($prefix !== null && strpos(strtolower($enum), strtolower($prefix)) === 0)) {
				$enumLabel = $this->getEnumLabel($number, $prefix, $translationContext);
				$list[$number] = $enumLabel;
			}
		}

		return $list;
	}

	/**
	 * @param $enumNumber
	 * @param null $prefix
	 * @param string $translationContext
	 * @return string
	 */
	public function getEnumLabel($enumNumber, $prefix=null, $translationContext='string')
	{
		$label = "";
		$class = $this->owner;
		$reflection = new ReflectionClass( get_class( $class ) );
		$constants = $reflection->getConstants();

		foreach ($constants as $enum=>$number) {
			if ($number == $enumNumber) {
				if ($prefix === null || ($prefix !== null && strpos(strtolower($enum), strtolower($prefix)) === 0)) {
					$label = strtolower($enum);

					if ($prefix !== null) {
						$prefix = strtolower($prefix);
						$label = str_replace($prefix, "", $label);
					}
					$label = str_replace("_", " ", $label);
					$label = ucwords($label);
					$label = Yii::t($translationContext, $label);

					break;
				}
			}
		}

		return $label;
	}
}