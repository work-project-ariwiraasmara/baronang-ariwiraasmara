<h1>Update <?php echo $this->class2name($this->modelClass) . " <?php echo \$model->{$this->tableSchema->primaryKey}; ?>"; ?></h1>
<hr />

<?php echo "<?php\n"; ?>$this->widget('ext.widgets.form.FormLinkButtons', array(
	'linkButtons'=>array(
		array('label'=>'List', 'url'=>array('index'), 'access'=>array('<?php echo $this->modelClass; ?>Index')),
		array('label'=>'Create', 'url'=>array('create'), 'access'=>array('<?php echo $this->modelClass; ?>Create')),
		array('label'=>'View','url'=>array('view','id'=>$model-><?php echo $this->tableSchema->primaryKey; ?>), 'access'=>array('<?php echo $this->modelClass; ?>View'), 'optionalAccess'=>array('<?php echo $this->modelClass; ?>Own')),
	),
)); ?>

<?php echo "<?php echo \$this->renderPartial('_form',array(
	'model'=>\$model,
)); ?>"; ?>