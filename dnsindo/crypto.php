<?php
class FunctionAdmin {

    public $str;
    public function setStr($str) {
        try {
            $this->str = $str;
        }
        catch(Exception $e) {
            return "Error Function setStr() : ".$e;
        }
    }

    public function getStr() {
        try {
            return $this->str;
        }
        catch(Exception $e) {
            return "Error Function getStr() : ".$e;
        }
    }


    // SET TITLE PAGE
    public $titlepage;
    public function setTitle($title) {
        try {
            $this->titlepage = $title;
        }
        catch(Exception $e) {
            return "Error Function setTitle() : ".$e;
        }
    }

    public function getTitle() {
        try {
            return $this->titlepage;
        }
        catch(Exception $e) {
            return "Error Function getTitle() : ".$e;
        }
        
    }

    public function setDocTitle() { 
        try { ?>
            <script type="text/javascript">
                document.title = "<?php echo $this->getTitle(); ?>";
                $('#title').html("<?php echo $this->getTitle(); ?>")
            </script>
            <?php
        }
        catch(Exception $e) {
            return "Error Function setDocTitle() : ".$e;
        }
    }


    // GET AND POST PARAMETER
    public function POST($str, $state=0) {
        try {
            if($state > 1) { // Dengan Function Enlink Dan Denval
                return $this->Denval(@$_POST[$this->Enlink($str)]);
            }
            else if($state == 1) { // Dengan Function Enlink 
                return @$_POST[$this->Enlink($str)];
            }
            else {
                return @$_POST[$str];
            }
        }
        catch(Exception $e) {
            return "function POST() Error: ".$e;
        }
    }
    
    public function GET($str, $state=0) {
        try {
            if($state > 1) { // Dengan Function Enlink Dan Denval
                return $this->Denval(@$_GET[$this->Enlink($str)]);
            }
            else if($state == 1) { // Dengan Function Enlink 
                return @$_GET[$this->Enlink($str)];
            }
            else {
                return @$_GET[$str];
            }
        }
        catch(Exception $e) {
            return "function GET() Error: ".$e;
        }
    }
    
    public function FILE($str, $tipe, $state=0) {
        try {
            if($state > 0) { // Dengan Function Enlink
                return @$_FILES[$this->Enlink($str)][$tipe];
            }
            else {
                return @$_FILES[$str][$tipe];
            }
        }
        catch(Exception $e) {
            return "function FILE() Error: ".$e;
        }
    }


    // ENCRYPT AND DECRYPT
	public function encrypt($code, $key){
        $k = $this->ks($key);

        $plaintext = $code;
        $ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
        $iv = openssl_random_pseudo_bytes($ivlen);
        $ciphertext_raw = openssl_encrypt($plaintext, $cipher, $k, $options=OPENSSL_RAW_DATA, $iv);
        $hmac = hash_hmac('sha256', $ciphertext_raw, $k, $as_binary=true);
        $ciphertext = base64_encode( $iv.$hmac.$ciphertext_raw );

        return $ciphertext;
    }

    public function decrypt($code, $key){
        $k = $this->ks($key);

        $c = base64_decode($code);
        $ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
        $iv = substr($c, 0, $ivlen);
        $hmac = substr($c, $ivlen, $sha2len=32);
        $ciphertext_raw = substr($c, $ivlen+$sha2len);
        $original_plaintext = openssl_decrypt($ciphertext_raw, $cipher, $k, $options=OPENSSL_RAW_DATA, $iv);
        $calcmac = hash_hmac('sha256', $ciphertext_raw, $k, $as_binary=true);
        if (hash_equals($hmac, $calcmac))
        {
            return $original_plaintext;
        }
    }

    private function ks($code){
        $c = base64_decode($code);
        $ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
        $iv = substr($c, 0, $ivlen);
        $hmac = substr($c, $ivlen, $sha2len=32);
        $ciphertext_raw = substr($c, $ivlen+$sha2len);
        $original_plaintext = openssl_decrypt($ciphertext_raw, $cipher, 'c8f6dae75c5e2010d46924649e0c0357', $options=OPENSSL_RAW_DATA, $iv);
        $calcmac = hash_hmac('sha256', $ciphertext_raw, 'c8f6dae75c5e2010d46924649e0c0357', $as_binary=true);
        if (hash_equals($hmac, $calcmac))
        {
            return $original_plaintext;
        }
    }


    // SET AND GET CONNECTION
    public function getS() {
        return "gLoigQHLPfbCjsiKd3l7tWj65oeJGtMXoH3zYuEyJbMXVXEKfkASh3ETAuXrP8S8Q8jS7fLVOIzi5uMF/+tpUA==";
    }

    public function getU() {
        return "ewWLXmSRBo/YPD5SBSlp1O9zfsac2Oe51vIg5zTWpWNN4nddSivKrqlSRID8s+6dbp7b8cYYz8angqTGTsErxQ==";
    }

    public function getP() {
        return "rBrRaqHNOQ5iL6fPt+wWlVhJgB/nODHVKmSyeG0+RdXLYaBg0opffLJM4sOz+nBHJ9AHO/QGhdG30PYr1gIe+w==";
    }

    public function getD() {
        return "78gc+nBiWQ5iaDSQrEmLjF2SsQI3z6xU03Q+DAgQw4IT/PwDosfsTjOC41oW+iF5fQRtvPXHJP3F6Ye0+2U8mg==";
    }

    public function getK() {
        return "iMijz5XlckKwsQqcHedfLbf66muVMzfI6rl9KBxTScu8BbjoJU3y2t9rFmSHcdfMhQrakszK7F1cHMbQ9w72hQ==";
    }

    public $koneksi;
    function setConnection() {
        try {
            $this->koneksi = mysqli_connect($this->decrypt($this->getS(), $this->getK()), 
                                            $this->decrypt($this->getU(), $this->getK()), 
                                            $this->decrypt($this->getP(), $this->getK()), 
                                            $this->decrypt($this->getD(), $this->getK())); 
        }
        catch(Exception $e) {
            return "Error Function setConnection() : ".$e;
        }
    }

    public function getConnection() {
        try {
            return $this->koneksi;
        }
        catch(Exception $e) {
            return "Error Function getConnection() : ".$e;
        }
    }
    
    public function ENID($str) {
        try {
            return bin2hex(crc32(sha1(md5(crypt($str,'$6$rounds=999999999'), TRUE), TRUE)));
        }
        catch(Exception $e) {
            return "Error Function ENID() : ".$e;
        }
    }

    public function Enlink($str) {
        try {
            return bin2hex(base64_encode(crc32(sha1( md5($str), TRUE ))));
        }
        catch(Exception $e) {
            return "Error Function Enlink() : ".$e;
        }
    }

    public function Enval($str) {
        try {
            return bin2hex(base64_encode( $str ));
        }
        catch(Exception $e) {
            return "Error Function Enval() : ".$e;
        }   
    }

    public function Denval($str) {
        try {
            return base64_decode(hex2bin( $str ));
        }   
        catch(Exception $e) {
            return "Error Function Denval() : ".$e;
        }
    }

    public function setIDParam($id, $val) {
        try {
            return $this->Enlink($id).'='.$this->Enval($val);
        }
        catch(Exception $e) {
            return "Error Function setIDParam() : ".$e;
        }
    }

    public function getIDParam($id) {
        try {
            return $this->Denval(@$_GET[$this->Enlink($id)]);
        }
        catch(Exception $e) {
            return "Error Function getIDParam() : ".$e;
        }
    }

    public function ESC($str) {
        try {
            return htmlspecialchars(htmlentities(addslashes($str)));
        }
        catch(Exception $e) {
            return "Error Function ESC() : ".$e;
        }
    }

    public function REST($str) {
        try {
            return mysqli_real_escape_string($this->getConnection(), $this->ESC($str));
        }
        catch(Exception $e) {
            return "Error Function REST() : ".$e;
        }
    }


    // READABLE FORMAT TEXT
    public function READABLE($str) {
        try {
            return html_entity_decode(htmlspecialchars_decode($str));
        }
        catch(Exception $e) {
            return "Error Function READABLE() : ".$e;
        }
    }

    public function FormatRupiah($str) {
        try {
            return number_format($str,2,',','.');
        }
        catch(Exception $e) {
            return "Error Function FormatRupiah() : ".$e;
        }
    }

    public function FormatNumber($str) {
        try {
            return number_format($str,0,',','.');
        }
        catch(Exception $e) {
            return "Error Function FormatNumber() : ".$e;
        }
    }

    public function Rupiah($str) {
        try {
            return 'Rp. '.number_format($str,2,',','.');
        }
        catch(Exception $e) {
            return "Error Function Rupiah() : ".$e;
        }
    }


    // SESSION AND COOKIE
    public function setOneSession($a, $b, $type=0) {
        try {
            if( $type > 1 ) {
                $_SESSION[$this->ENID($a)] = $this->Enval($b);
            }
            else if( $type == 1 ) {
                $_SESSION[$this->ENID($a)] = $b;
            }
            else {
                $_SESSION[$a] = $b;
            }
        }
        catch(Exception $e) {
            return "Error Function setOneSession() : ".$e;
        }
    }
    
    public function setSession($array, $type=0) {
        try {
            if( $type > 1 ) {
                foreach($array as $arr => $val) {
                    $_SESSION[$this->ENID($arr)] = $this->Enval($val);
                }
            }
            else if( $type == 1 ) {
                foreach($array as $arr => $val) {
                    $_SESSION[$this->ENID($arr)] = $val;
                }
            }
            else {
                foreach($array as $arr => $val) {
                    $_SESSION[$arr] = $val;
                }
            }

            /*
            $_SESSION[$this->ENID('userid')]    = $this->Enval($a);
            $_SESSION[$this->ENID('cmpid')]     = $this->Enval($b);
            $_SESSION[$this->ENID('username')]  = $this->Enval($c);
            $_SESSION[$this->ENID('email')]     = $this->Enval($d);
            $_SESSION[$this->ENID('pin')]       = $this->Enval($e);
            */
        }
        catch(Exception $e) {
            return "Error Function setSession() : ".$e;
        }
    }

    public function setOneCookie($name, $val, $hari=1, $type=0, $jam=24, $menit=60, $detik=60) {
        try {
            if($type > 1) {
                setcookie($this->ENID($name), $this->Enval($val), time() + ($time * $jam * $menit * $detik), "/");
            }
            else if($type == 1) {
                setcookie($this->ENID($name), $val, time() + ($time * $jam * 60 * 60), "/");
            }
            else {
                setcookie($name, $val, time() + ($time * $jam * 60 * 60), "/");
            }
        }
        catch(Exception $e) {
            return "Error Function setOneCookie() : ".$e;
        }
    }

    public function setCookie($array, $hari=1, $jam=24, $menit=60, $detik=60) {
        try {
            foreach($array as $arr => $val) {
                setcookie($this->ENID($arr), $this->Enval($val), time() + ($hari * $jam * $menit * $detik), "/"); // 86400 = 1 day
            }
        }
        catch(Exception $e) {
            return "Error Function setCookie() : ".$e;
        }
    }

    public function getSession($str, $type=null) {
        try {
            if($type > 1) {
                return $this->Denval($_SESSION[$this->ENID($str)]);
            }
            else if($type == 1) {
                return $_SESSION[$this->ENID($str)];
            }
            else {
                return $_SESSION[$str];
            }
        }
        catch(Exception $e) {
            return "Error Function getSession() : ".$e;
        }
    }

    public function getCookie($str) {
        try {
            return $_COOKIE[$this->ENID($str)];
        }
        catch(Exception $e) {
            return "Error Function getCookie() : ".$e;
        }
    }

    public function getValookie($str) {
        try {
            return $this->Denval($this->getCookie($str));
        }
        catch(Exception $e) {
            return "Error Function getValookie() : ".$e;
        }
    }

    
    public function logoutSystem($array) {
        try {
            foreach($array as $arr) {
                setcookie($this->ENID($arr), null, time() - (365 * 24 * 60 * 60), "/");
            }
            session_destroy();
            /*
            setcookie('login', null, time() - (365 * 24 * 60 * 60), "/");
            setcookie($this->ENID('userid'),    null, time() - (365 * 24 * 60 * 60), "/");
            setcookie($this->ENID('cmpid'),     null, time() - (365 * 24 * 60 * 60), "/");
            setcookie($this->ENID('username'),  null, time() - (365 * 24 * 60 * 60), "/");
            setcookie($this->ENID('email'),     null, time() - (365 * 24 * 60 * 60), "/");
            setcookie($this->ENID('pin'),       null, time() - (365 * 24 * 60 * 60), "/");
            setcookie($this->ENID('koneksi'),   null, time() - (365 * 24 * 60 * 60), "/");
            setcookie($this->ENID('firstname'), null, time() - (365 * 24 * 60 * 60), "/");
            setcookie($this->ENID('lastname'),  null, time() - (365 * 24 * 60 * 60), "/");
            */
        }
        catch(Exception $e) {
            return "Error Function logoutSystem() : ".$e;
        }
    }

    public function paginate_one($reload, $page, $tpages, $adjacents) {
        $firstlabel = "<b>1</b>";
        $prevlabel  = "<b>Prev</b>";
        $nextlabel  = "<b>Next</b>";
        $lastlabel  = "<b>$tpages</b>";
        
        $out = "<div class=\"m-t-10 m-b-10 center backpaging\"><ul class=\"ownpaging\">\n";
        
        // first
        if($page>1) {
            $out.= "<li><a href=\"" . $reload . "\">" . $firstlabel . "</a></li>\n";
        }
        else {
            $out.= "<li class=\"disabledd\"><a href=\"#\">" . $firstlabel . "</a></li>\n";
        }
        
        // previous
        if($page==1) {
            $out.= "<li class=\"disabledd\"><a href=\"#\">" . $prevlabel . "</a></li>\n";
        }
        elseif($page==2) {
            $out.= "<li><a href=\"" . $reload . "\">" . $prevlabel . "</a></li>\n";
        }
        else {
            $out.= "<li><a href=\"" . $reload . "&amp;hlm=" . ($page-1) . "\">" . $prevlabel . "</a></li>\n";
        }
        
        // current
        $out.= "<li class=\"active\"><a href=\"#\" class=\"active\">Page " . $page . " of " . $tpages . "</a></li>\n";
        
        // next
        if($page<$tpages) {
            $out.= "<li><a href=\"" . $reload . "&amp;hlm=" .($page+1) . "\">" . $nextlabel . "</a></li>\n";
        }
        else {
            $out.= "<li class=\"disabledd\"><a href=\"#\">" . $nextlabel . "</a></li>\n";
        }
        
        // last
        if($page<$tpages) {
            $out.= "<li><a href=\"" . $reload . "&amp;hlm=" . $tpages . "\">" . $lastlabel . "</a></li>\n";
        }
        else {
            $out.= "<li class=\"disabledd\"><a href=\"#\">" . $lastlabel . "</a></li>\n";
        }
        
        $out.= "</ul></div>";
        
        return $out;
    }


    // GET SYSTEM TIME
    public function getHari($str) {
        try {
            return date('l', strtotime($str));
        }
        catch(Exception $e) {
            return "function Hari() Error: ".$e;
        }
    }
    
    public function getTanggal($str, $pemisah, $format, $jam) {
        try {
            if($format == 'dmy') {
                if($jam == 'Yes') {
                    return date('d'.$pemisah.'m'.$pemisah.'Y H:i:s', strtotime($str)); 
                }
                else {
                    return date('d'.$pemisah.'m'.$pemisah.'Y', strtotime($str)); 
                }
            }
            else if($format == 'ymd') {
                if($jam == 'Yes') {
                    return date('Y'.$pemisah.'m'.$pemisah.'d H:i:s', strtotime($str)); 
                }
                else {
                    return date('Y'.$pemisah.'m'.$pemisah.'d', strtotime($str));
                }
            }
            else {
                return date('d'.$pemisah.'m'.$pemisah.'Y'); 
            }
        }
        catch(Exception $e) {
            return "function getBulan() Error: ".$e;
        }
    }
    
    public function getTanggalAkhir($str) {
        try {
            return date('t', strtotime($str));
        }
        catch(Exception $e) {
            return "function getTanggalAkhir() Error: ".$e;
        }
    }
    
    public function getBulan($str, $bulan, $tahun) {
        try {
            if($bulan == 'Angka') {
                if($tahun == 'Yes') {
                    return date('m Y', strtotime($str));
                }
                else {
                    return date('m', strtotime($str));
                }
            }
            else if($bulan == 'Nama') {
                if($tahun == 'Yes') {
                    return date('F Y', strtotime($str));
                }
                else {
                    return date('F', strtotime($str));
                }
            }
            else {
                return date('F Y');
            }
        }
        catch(Exception $e) {
            return "function getBulan() Error: ".$e;
        }
    }
    
    
    // FORM FILL
    public function inputField($type, $id, $name, $text, $field_class, $text_class, $val=null, $required=null, $readonly=null) {
        try { ?>
            <label for="<?php echo $this->Enlink($id); ?>" class="<?php echo $text_class; ?>"><?php echo $text; ?></label>
            <input type="<?php echo $type; ?>" name="<?php echo $this->Enlink($name); ?>" id="<?php echo $this->Enlink($id); ?>" class="<?php echo $field_class; ?>" placeholder="<?php echo $text; ?>" value="<?php echo $val; ?>" <?php echo $required.' '.$readonly; ?> >
            <?php
            return null;
        }
        catch(Exception $e) {
            return "function inputField() Error: ".$e;
        }
    }
    
    public function textField($name, $id, $text, $field_class, $text_class=null, $val=null, $col=null, $row=null, $required=null, $readonly=null) {
        try { ?>
            <label for="<?php echo $this->Enlink($id); ?>"><?php echo $text; ?></label>
            <textarea name="<?php echo $this->Enlink($name); ?>" id="<?php echo $this->Enlink($id); ?>" class="<?php echo $field_class; ?>" rows="<?php echo $row; ?>" cols="<?php echo $col; ?>" placeholder="<?php echo $text; ?>" <?php echo $required.' '.$readonly; ?>>
                <?php 
                if( $val != '' || !empty($val) || !is_null($val) ) {
                    echo $val; 
                }
                ?>
            </textarea>
            <?php
            return null;
        }
        catch(Exception $e) {
            return "function textField() Error: ".$e;
        }
    }
    
    public function selectField($name, $id, $text, $class_select, $class_text, $arrname, $arrval, $setval=null, $required=null, $readonly=null) {
        try { ?>
            <span class="<?php echo $class_text; ?>"><?php echo $text; ?></span><br>
            <select name="<?php echo $this->Enlink($name); ?>" id="<?php echo $this->Enlink($id); ?>" class="<?php echo $class_select; ?>" <?php echo $required.' '.$readonly; ?>>
                <?php
                $x = 0;
                foreach($arrname as $val) { ?>
                    <option value="<?php echo $this->Enval($val); ?>" <?php if($val == $setval) { echo 'selected'; } ?> ><?php echo $arrval[$x]; ?></option>
                    <?php
                    $x++;
                }
                ?>
            </select>
            <?php
            return null;
        }
        catch(Exception $e) {
            return "function selectField() Error: ".$e;
        }
    }
    
    public function dtField($type, $name, $id, $text, $class_input, $class_text, $val=null, $required=null, $readonly=null) {
        try { ?>
            <span class="<?php echo $class_text; ?>"><?php echo $text; ?></span>
            <input type="<?php echo $type; ?>" name="<?php echo $this->Enlink($name); ?>" id="<?php echo $this->Enlink($id); ?>" class="<?php echo $class_input; ?>" value="<?php echo $val; ?>" <?php echo $required.' '.$readonly; ?>>
            <?php
            return null;
        }
        catch(Exception $e) {
            return "function radioField() Error: ".$e;
        }
    }
    
    public function radioField($name, $id, $text, $class_text, $val1=null, $val2=null, $required=null, $readonly=null) {
        try { ?>
            <input type="radio" name="<?php echo $this->Enlink($name); ?>" id="<?php echo $this->Enlink($id); ?>" value="<?php echo $this->Enval($val1); ?>" <?php if( !empty($val1) || !is_null($val1) || $val1 != '' || !empty($val2) || !is_null($val2) || $val2 != '' ) { if( $val1 == $val2 ) { echo 'checked'; } } ?> <?php echo $required.' '.$readonly; ?> /> 
            <label for="<?php echo $this->Enlink($id); ?>" class="<?php echo $class_text; ?>"><?php echo $text; ?></label>
            <?php
            return null;
        }
        catch(Exception $e) {
            return "function radioField() Error: ".$e;
        }
    }
    
    public function checkField($name, $id, $text, $class_text, $val1=null, $val2=null, $required=null, $readonly=null) {
        try { ?>
            <input type="checkbox" name="<?php echo $this->Enlink($name); ?>" id="<?php echo $this->Enlink($id); ?>" value="<?php echo $this->Enval($val); ?>" <?php if( !empty($val1) || !is_null($val1) || $val1 != '' || !empty($val2) || !is_null($val2) || $val2 != '' ) { if( $val1 == $val2 ) { echo 'checked'; } } ?> <?php echo $required.' '.$readonly; ?> /> 
            <label for="<?php echo $this->Enlink($id); ?>" class="<?php echo $class_text; ?>"><?php echo $text; ?></label>
            <?php
            return null;
        }
        catch(Exception $e) {
            return "function checkField() Error: ".$e;
        }
    }
    
    public function fileField($name, $id, $text, $field_class, $text_class) {
        try { ?>
            <span class="<?php echo $text_class; ?>"><?php echo $text; ?></span><br>
            <input type="file" name="<?php echo $this->Enlink($name); ?>" id="<?php echo $this->Enlink($id); ?>" class="<?php echo $field_class; ?>" />
            <?php
            return null;
        }
        catch(Exception $e) {
            return "function fileField() Error: ".$e;
        }
    }
    
    public function buttonField($type, $name, $id, $class, $text) {
        try { ?>
            <button type="<?php echo $type; ?>" name="<?php echo $this->Enlink($name); ?>" id="<?php echo $this->Enlink($id); ?>" class="<?php echo $class; ?>"><?php echo $text; ?></button>
            <?php
            return null;
        }
        catch(Exception $e) {
            return "function fileField() Error: ".$e;
        }
    }

    public function linkTo($link, $text, $class=null, $id=null) {
        try { ?>
            <a href="<?php echo $link; ?>" class="<?php echo $class; ?>" id="<?php echo $this->Enlink($id); ?>"><?php echo $text; ?></a>
            <?php
        }
        catch(Exception $e) {
            return "function linkTo() Error: ".$e;
        }
    }
    

    // NOTIFICATION
    public function Notrifger($id, $idtrigger, $txt) {
        try { ?>
            <a href="<?php echo '#'.$this->Enlink($idtrigger); ?>" id="<?php echo $this->Enlink($id); ?>" class="modal-trigger hide"><?php echo 'to-'.$txt; ?></a>
            <?php
        }
        catch(Exception $e) {
            echo "function Notrifger() Error: ".$e;
        }
    }
                
    public function Notif($id, $pesan, $tipe, $link='#') {
        try { 
            if($tipe == 'notif') { $warna = '#5f5;'; $txt = 'txt-black'; } 
            else if($tipe == 'warn') { $warna = '#f00;'; $txt = 'txt-white'; } 
            else if($tipe == 'info') { $warna = '#55f;'; $txt = 'txt-white'; }
            else { $warna = '#ccc;'; $txt = 'txt-white'; }
            ?>
            <div class="modal borad-20 <?php echo $txt; ?>" id="<?php echo $this->Enlink($id); ?>" style="<?php echo 'background: '.$warna; ?>">
                <div class="modal-content choose-date">
                    <a href="<?php echo $link; ?>" class="close-notification no-smoothState modal-close"><i class="ion-android-close"></i></a>
                    <h1 class="uppercase <?php echo $txt; ?> bold italic center"><?php echo $pesan; ?></h1>
                </div>
            </div>
            <?php
        }
        catch(Exception $e) {
            echo "function Notif() Error: ".$e;
        }
    }

    public function notifSnackBar($id, $function, $text) { 
        try { ?>
            <button class="hide" onclick="<?php echo $function; ?>" id="<?php echo $this->Enlink($id); ?>">Show Snackbar</button>
            <div id="snackbar"><?php echo $text; ?></div>
            <?php
        }
        catch(Exception $e) {
            echo "function notifSnackBar() Error: ".$e;
        }
    }


    // OTHER
    public function randNumber($length) {
        try {
            $seed = str_split('0123456789'); // and any other characters
            shuffle($seed); // probably optional since array_is randomized; this may be redundant
            $rand = '';
            foreach (array_rand($seed, $length) as $k) {
                $rand .= $seed[$k];
            }
            return $rand;
        }
        catch(Exception $e) {
            echo "function randNumber() Error: ".$e;
        }
    }


    public function genPass($length) {
        try {
            $seed = str_split('abcdefghijklmnopqrstuvwxyz'.
                              'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.
                              '0123456789'); // and any other characters
            shuffle($seed); // probably optional since array_is randomized; this may be redundant
            $rand = '';
            foreach (array_rand($seed, $length) as $k) {
                $rand .= $seed[$k];
            }
            return $rand;
        }
        catch(Exception $e) {
            echo "function genPass() Error: ".$e;
        }
    }


    public function randChar($length) {
        try {
            $seed = str_split('abcdefghijklmnopqrstuvwxyz'.
                              'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.
                              '0123456789`~!@#$%^&*()-_=+[{]}\|;:,<.>/?/'); // and any other characters
            shuffle($seed); // probably optional since array_is randomized; this may be redundant
            $rand = '';
            foreach (array_rand($seed, $length) as $k) {
                $rand .= $seed[$k];
            }
            return $rand;
        }
        catch(Exception $e) {
            echo "function randChar() Error: ".$e;
        }
    }


    // QR AND BARCODE
    function QRCode($path, $kode, $cp, $matrixPointSize) {
        //QR code

        $PNG_TEMP_DIR = dirname(__FILE__).DIRECTORY_SEPARATOR.'qrcard'.DIRECTORY_SEPARATOR;
        //$PNG_WEB_DIR = 'qrcard/'.$memberid.'/';

        $errorCorrectionLevel = 'H';
        //$matrixPointSize = 500;

        //$kode = '1000100010001004';
        $filename = $path.$kode.'.png';
        if (!file_exists($filename)){
            QRcode::png($cp, $filename, $errorCorrectionLevel, $matrixPointSize, 2);
        }

        $generator = new Picqer\Barcode\BarcodeGeneratorPNG();
        $resource = 'data:image/png;base64,'.base64_encode($generator->getBarcode($kode, $generator::TYPE_CODE_128));
    }

    function QRCipher($cardno) {
        $ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
        $iv = openssl_random_pseudo_bytes($ivlen);
        $ciphertext_raw = openssl_encrypt($cardno, $cipher, '70007000', $options=OPENSSL_RAW_DATA, $iv);
        $hmac = hash_hmac('sha256', $ciphertext_raw, '70007000', $as_binary=true);
        $ciphertext = base64_encode( $iv.$hmac.$ciphertext_raw );
        return $ciphertext;
    }
}
?>