<?php // STATUS CONFIRMATION ?>
<div id="menu-share" 
    class="menu menu-box-bottom menu-box-detached rounded-m" 
    data-menu-load="menu-share.php"
    data-menu-height="420" 
    data-menu-effect="menu-over">
</div>    
                    
<div id="menu-highlights" 
    class="menu menu-box-bottom menu-box-detached rounded-m" 
    data-menu-load="menu-colors.php"
    data-menu-height="510" 
    data-menu-effect="menu-over">        
</div>

<div id="menu-main"
    class="menu menu-box-right menu-box-detached rounded-m"
    data-menu-width="260"
    data-menu-load="menu-main.php"
    data-menu-active="nav-welcome"
    data-menu-effect="menu-over">  
</div>

<?php // PIN CONFIRMATION ?>
<div id="<?php echo $fun->Enlink('confirm-pin'); ?>" class="menu menu-box-modal rounded-m" 
    data-menu-height="270" 
    data-menu-width="300">
    <div class="mr-3 ml-3 mt-3">
        <h1 class="font-700 mb-0 center">Pin Confirmation</h1>
        
        <p class="font-11 mt-3 mb-0 justify">
            <span id="<?php echo $fun->Enlink('confirm-pin-msg'); ?>"></span>
        </p>

        <div class="input-style has-icon input-style-1 input-required">
            <i class="input-icon fa fa-lock font-11"></i>
            <span>PIN</span>
            <em>(required)</em>
            <input type="password" name="<?php echo $fun->Enlink('cfmpin'); ?>" id="<?php echo $fun->Enlink('cfmpin'); ?>" placeholder="PIN">
        </div> 

        <input type="text" name="<?php echo $fun->Enlink('idttrigger'); ?>" id="<?php echo $fun->Enlink('idttrigger'); ?>" class="hide">

        <button type="button" id="<?php echo $fun->Enlink('btn_pinconfirm'); ?>" class="btn width-100 btn-m shadow-l rounded-s text-uppercase font-900 bg-highlight mt-4">OK</button>
    </div>
</div>     