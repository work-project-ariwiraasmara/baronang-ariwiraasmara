<?php
require('util/koneksi.php');
$fun->setTitle_CP('Baronang');
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title></title>

		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
		<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
		
		<link rel="stylesheet" type="text/css" href="styles/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="styles/style.css">
		<link rel="stylesheet" type="text/css" href="styles/custom_style.css">
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900|Roboto:300,300i,400,400i,500,500i,700,700i,900,900i&amp;display=swap">
		<link rel="stylesheet" type="text/css" href="fonts/css/fontawesome-all.min.css">    
		<link rel="manifest" href="_manifest.json" data-pwa-version="set_in_manifest_and_pwa_js">
		<link rel="apple-touch-icon" sizes="180x180" href="app/icons/icon-192x192.png">
		
		<!-- Icons -->
		<link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" media="all" rel="stylesheet" type="text/css">

		<link href='images/icon/Logo-fish-icon.png' type='image/x-icon'/>
		<link href='images/icon/Logo-fish-icon.png' rel='shortcut icon'/>

		<script type="text/javascript" src="scripts/jquery-2.1.0.min.js"></script>
		
		<!-- Scan QR -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="https://rawgit.com/schmich/instascan-builds/master/instascan.min.js"></script>
		
		<script src="https://unpkg.com/html5-qrcode/minified/html5-qrcode.min.js"></script> 
		<!-- <script type="text/javascript" src="scripts/qrscan.js"></script>
		<script src="https://unpkg.com/html5-qrcode/html5-qrcode.js"></script>-->
		
		<!-- One Signal -->
		<!--<script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script>
		<script>
			window.OneSignal = window.OneSignal || [];
			OneSignal.push(function() {
				OneSignal.init({
					appId: "319f15cc-ac9c-4b2e-a5f3-1a4bb2af2172",
				});
			});
		</script>
		<script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script>
<script>
   var OneSignal = window.OneSignal || [];
    var initConfig = {
        appId: "319f15cc-ac9c-4b2e-a5f3-1a4bb2af2172",
        notifyButton: {
            enable: true
        },
    };
    OneSignal.push(function () {
        OneSignal.SERVICE_WORKER_PARAM = { scope: '/baronangapp/' };
        OneSignal.SERVICE_WORKER_PATH = 'baronangapp/OneSignalSDKWorker.js'
        OneSignal.SERVICE_WORKER_UPDATER_PATH = 'baronangapp/OneSignalSDKUpdaterWorker.js'
        OneSignal.init(initConfig);
    });
</script>-->
		
	</head>

	<body class="theme-light" data-highlight="blue2">

		<div id="preloader"><div class="spinner-border color-highlight" role="status"></div></div>
		
		<div id="page">
	
			<?php
			$pg = $fun->GET('pg');
			$sb = $fun->GET('sb');

			// header and footer bar go here
			?>
			<div class="header header-fixed header-auto-show header-logo-app">
			    <span class="header-title header-subtitle left" id="title"></span>

			    <?php
			    if(isset($_COOKIE[$fun->ENID('login')])) { ?>
			    	<a href="#" data-menu="menu-main" class="header-icon header-icon-1"><i class="fas fa-bars"></i></a>
			    	<?php
			    }
			    ?>

				<a href="#" data-toggle-theme class="header-icon header-icon-2 show-on-theme-dark"><i class="fas fa-sun"></i></a>
        		<a href="#" data-toggle-theme class="header-icon header-icon-2 show-on-theme-light"><i class="fas fa-moon"></i></a>
        		<a href="#" data-menu="menu-highlights" class="header-icon header-icon-3"><i class="fas fa-brush"></i></a>
			</div>
			
			<?php 
			if( isset($_GET[$fun->Enlink('status')]) ) { ?>
	    		<div class="ml-3 mr-3 alert alert-small rounded-s shadow-xl bg-blue2-dark" role="alert" data-dismiss="alert">
		            <span><i class="fa fa-check"></i></span>
		            <strong><?php  echo $fun->getIDParam('status'); ?></strong>
		            <button type="button" class="close color-white opacity-60 font-16" aria-label="Close">&times;</button>
		        </div>   
	    		<?php
    		}

			if(isset($_COOKIE[$fun->ENID('login')])) { ?>
				<div id="footer-bar" class="footer-bar-5">
					<?php /*
				    <a href="#index-components.html">
				    	<i data-feather="heart" data-feather-line="1" data-feather-size="21" data-feather-color="red2-dark" data-feather-bg="red2-fade-light"></i><span>Features</span>
				    </a>
			     	*/?>
					<a href="?">
						<i data-feather="home" data-feather-line="1" data-feather-size="21" data-feather-color="blue2-dark" data-feather-bg="blue2-fade-light"></i><span>Home</span>
					</a>
					
					<div class="input-style input-style-1 input-required bottom-20">
						<input type="hidden" class="generate-qr-input" value="<?php echo $fun->getValookie('id'); ?>">
					</div>
					<a href="#" data-menu="qrcode" class="generate-qr-button">
						<i data-feather="image" data-feather-line="1" data-feather-size="21" data-feather-color="green1-dark" data-feather-bg="green1-fade-light"></i>
						<span>QR Code</span>
					</a>
					
					<a href="#" data-menu="<?php echo $fun->Enlink('scanqr'); ?>">
						<i data-feather="camera" data-feather-line="1" data-feather-size="21" data-feather-color="green1-dark" data-feather-bg="green1-fade-light"></i><span>Scan</span>
					</a>
					
					<a href="?pg=shop">
						<i data-feather="shopping-cart" data-feather-line="1" data-feather-size="21" data-feather-color="green1-dark" data-feather-bg="green1-fade-light"></i><span>My Cart</span>
					</a>
			    
			        <!--<a href="#" data-toast="notification-3">
						<i data-feather="mail" data-feather-line="1" data-feather-size="21" data-feather-color="blue2-dark" data-feather-bg="blue2-fade-light"></i>
						<span>Send Notification</span>
					</a> 
    
			    	<a href="logout.php">
				    	<i data-feather="file" data-feather-line="1" data-feather-size="21" data-feather-color="brown1-dark" data-feather-bg="brown1-fade-light"></i><span>Logout</span>
					</a>-->
			        <?php
			    //}
			        /*
			        <a href="#index-settings.html">
			        	<i data-feather="settings" data-feather-line="1" data-feather-size="21" data-feather-color="gray2-dark" data-feather-bg="gray2-fade-light"></i><span>Settings</span>
			        </a>
			    */ ?>
				</div>
			
				<?php

				// WEB CONTENT
				//if(isset($_COOKIE[$fun->ENID('login')])) {
				// AFTER LOGIN { 
				if(strtolower($pg) == '' || strtolower($pg) == 'home' || strtolower($pg) == 'beranda' || strtolower($pg) == 'index') {
					require('page/02usermainpage.php');
					$fun->setTitle('Home');
					$fun->setDocTitle();
				}
				
				else if(strtolower($pg) == 'skymanagement')  {
					require('page/02bskymanagement.php');
					$fun->setTitle('Sky Parking');
					$fun->setDocTitle();
				}
								
				else if(strtolower($pg) == 'baronang')  {
					require('page/02dbaronang.php');
					$fun->setTitle('Baronang');
					$fun->setDocTitle();
				}
								
				else if(strtolower($pg) == 'cardmanagement')  {
					require('page/02ccardmanagement.php');
					$fun->setTitle('My Cards');
					$fun->setDocTitle();
				}			
								
				else if(strtolower($pg) == 'userprofile' || strtolower($pg) == 'userprofil')  {
					require('page/21memberprofile.php');
					$fun->setTitle('My Profile');
					$fun->setDocTitle();
				}

				else if(strtolower($pg) == 'location' || strtolower($pg) == 'lokasi') {
					if(strtolower($sb) == 'list' || strtolower($sb) == 'index' || strtolower($sb) == '' || is_null($sb) || empty($sb) ) {
						require('page/location/list.php');
						$fun->setTitle('Location');
						$fun->setDocTitle();
					}
					else if(strtolower($sb) == 'add') {
						require('page/location/add.php');
						$fun->setTitle('Add Location');
						$fun->setDocTitle();
					}
					else if(strtolower($sb) == 'edit') {
						require('page/location/edit.php');
						$fun->setTitle('Edit Location');
						$fun->setDocTitle();
					}

					else {
						require('404.php');
					}
				}

				else if(strtolower($pg) == 'product' || strtolower($pg) == 'produk') {
					if(strtolower($sb) == 'list' || strtolower($sb) == 'index' || strtolower($sb) == '' || is_null($sb) || empty($sb) ) {
						require('page/product/list.php');
						$fun->setTitle('Product');
						$fun->setDocTitle();
					}
					else if(strtolower($sb) == 'approval') {
						require('page/product/approval.php');
						$fun->setTitle('Product Approval');
						$fun->setDocTitle();
					}
					else if(strtolower($sb) == 'add') {
						require('page/product/add.php');
						$fun->setTitle('Add Product');
						$fun->setDocTitle();
					}
					else if(strtolower($sb) == 'edit') {
						require('page/product/edit.php');
						$fun->setTitle('Edit Product');
						$fun->setDocTitle();
					}

					else {
						require('404.php');
					}
				}

				else if(strtolower($pg) == 'generalproduct' || strtolower($pg) == 'generalproduk') {
					if(strtolower($sb) == 'list' || strtolower($sb) == 'index' || strtolower($sb) == '' || is_null($sb) || empty($sb) ) {
						require('page/generalproduct/list.php');
						$fun->setTitle('Product');
						$fun->setDocTitle();
					}
					else if(strtolower($sb) == 'approval') {
						require('page/generalproduct/approval.php');
						$fun->setTitle('Product Approval');
						$fun->setDocTitle();
					}
					else if(strtolower($sb) == 'add') {
						require('page/generalproduct/add.php');
						$fun->setTitle('Add Product');
						$fun->setDocTitle();
					}
					else if(strtolower($sb) == 'edit') {
						require('page/generalproduct/edit.php');
						$fun->setTitle('Edit Product');
						$fun->setDocTitle();
					}

					else {
						require('404.php');
					}
				}

				else if(strtolower($pg) == 'compliment') {
					if(strtolower($sb) == 'list' || strtolower($sb) == 'index' || strtolower($sb) == '' || is_null($sb) || empty($sb) ) {
						require('page/compliment/list.php');
						$fun->setTitle('Complimentary Product');
						$fun->setDocTitle();
					}
					else if(strtolower($sb) == 'add') {
						require('page/compliment/add.php');
						$fun->setTitle('Add Complimentary Product');
						$fun->setDocTitle();
					}
					else if(strtolower($sb) == 'edit') {
						require('page/compliment/edit.php');
						$fun->setTitle('Edit Complimentary Product');
						$fun->setDocTitle();
					}

					else {
						require('404.php');
					}
				}

				else if(strtolower($pg) == 'tenant') {
					if(strtolower($sb) == 'list' || strtolower($sb) == 'index' || strtolower($sb) == '' || is_null($sb) || empty($sb) ) {
						require('page/tenant/list.php');
						$fun->setTitle('Tenant');
						$fun->setDocTitle();
					}
					else if(strtolower($sb) == 'approval') {
						require('page/tenant/approval.php');
						$fun->setTitle('Tenant Approval');
						$fun->setDocTitle();
					}
					else if(strtolower($sb) == 'add') {
						require('page/tenant/add.php');
						$fun->setTitle('Add Tenant');
						$fun->setDocTitle();
					}
					else if(strtolower($sb) == 'edit') {
						require('page/tenant/edit.php');
						$fun->setTitle('Edit Tenant');
						$fun->setDocTitle();
					}

					else {
						require('404.php');
					}
				}
								
				else if(strtolower($pg) == 'report') {
					if(strtolower($sb) == 'list' || strtolower($sb) == 'index' || strtolower($sb) == '' || is_null($sb) || empty($sb) ) {
						require('page/report/list.php');
						$fun->setTitle('Reports');
						$fun->setDocTitle();
					}
					else if(strtolower($sb) == 'financial') {
						require('page/report/financialreport.php');
						$fun->setTitle('Financial Report');
						$fun->setDocTitle();
					}
					else if(strtolower($sb) == 'sales') {
						require('page/report/sales.php');
						$fun->setTitle('Membership Sales');
						$fun->setDocTitle();
					}
					else if(strtolower($sb) == 'edit') {
						require('page/report/edit.php');
						$fun->setTitle('Edit Tenant');
						$fun->setDocTitle();
					}

					else {
						require('404.php');
					}
				}

				else if(strtolower($pg) == 'company' || strtolower($pg) == 'perusahaan') {
					if(strtolower($sb) == 'list' || strtolower($sb) == 'index' || strtolower($sb) == '' || is_null($sb) || empty($sb) ) {
						//header('location: ?#'.$fun->Enlink('baronang') );
						require('page/company/list.php');
						$fun->setTitle('Company');
						$fun->setDocTitle();
					}
					else if(strtolower($sb) == 'add') {
						require('page/company/add.php');
						$fun->setTitle('Add Company');
						$fun->setDocTitle();
					}
					else if(strtolower($sb) == 'edit') {
						require('page/company/edit.php');
						$fun->setTitle('Edit Company');
						$fun->setDocTitle();
					}

					else {
						require('404.php');
					}
				}

				else if(strtolower($pg) == 'profil' || strtolower($pg) == 'profile') {
					if(strtolower($sb) == 'list' || strtolower($sb) == 'index' || strtolower($sb) == '' || is_null($sb) || empty($sb) ) {
						require('page/21memberprofile.php');
					}

					else {
						require('404.php');
					}
				}

				else if(strtolower($pg) == 'lang' || strtolower($pg) == 'language' || strtolower($pg) == 'bahasa' || strtolower($pg) == 'bhs') {
					if(strtolower($sb) == 'list' || strtolower($sb) == 'index' || strtolower($sb) == '' || is_null($sb) || empty($sb) ) {
						require('page/bahasa/list.php');
						$fun->setTitle('Language');
						$fun->setDocTitle();
					}
					else if(strtolower($sb) == 'add') {
						require('page/bahasa/add.php');
						$fun->setTitle('Add Language');
						$fun->setDocTitle();
					}
					else if(strtolower($sb) == 'edit') {
						require('page/bahasa/edit.php');
						$fun->setTitle('Edit Language');
						$fun->setDocTitle();
					}

					else {
						require('404.php');
					}
				}

				else if(strtolower($pg) == 'card' || strtolower($pg) == 'kartu') {
					if(strtolower($sb) == 'productsowned' || strtolower($sb) == 'products' || strtolower($sb) == 'products_owned') {
						require('page/card/productsowned.php');
						$fun->setTitle('Products Owned');
						$fun->setDocTitle();
					}
									
					else if(strtolower($sb) == 'list' || strtolower($sb) == 'index' || strtolower($sb) == 'activity' || strtolower($sb) == 'cardactivity' || strtolower($sb) == 'card_activity' || strtolower($sb) == '' || is_null($sb) || empty($sb) ) {
						require('page/card/activity.php');
						$fun->setTitle('Card\'s Activity');
						$fun->setDocTitle();
					}

					else if(strtolower($sb) == 'financial' || strtolower($sb) == 'history' || strtolower($sb) == 'financialhistory' || strtolower($sb) == 'financial_history') {
						require('page/card/financialhistory.php');
						$fun->setTitle('Financial History');
						$fun->setDocTitle();
					}

					else if(strtolower($sb) == 'purchase' || strtolower($sb) == 'product' || strtolower($sb) == 'purchaseproduct' || strtolower($sb) == 'purchase_product') {
						require('page/card/product.php');
						$fun->setTitle('Card\'s Products');
						$fun->setDocTitle();
					}

					else if(strtolower($sb) == 'topup' || strtolower($sb) == 'top_up') {
						require('page/card/topup.php');
						$fun->setTitle('Top Up');
						$fun->setDocTitle();
					}

					else {
						require('404.php');
					}
				}

				else if(strtolower($pg) == 'pin' || strtolower($pg) == 'pinconfirm' || strtolower($pg) == 'pin_confirm' || strtolower($pg) == 'pin_confirmation' || strtolower($pg) == 'pinconfirmation') {
					require('page/01cpinconfirmation.php');
					$fun->setTitle('Top Up');
					$fun->setDocTitle();
				}

				else if(strtolower($pg) == 'shop' || strtolower($pg) == 'belanja' || strtolower($pg) == 'shopping') {
					if(strtolower($sb) == 'list' || strtolower($sb) == 'index' || strtolower($sb) == '' || is_null($sb) || empty($sb) ) {
						require('page/shop/list.php');
						$fun->setTitle('Shopping Cart');
						$fun->setDocTitle();
					}
					else if(strtolower($sb) == 'payment' || strtolower($sb) == 'method_payment' || strtolower($sb) == 'methodpayment' || strtolower($sb) == 'pembayaran' || strtolower($sb) == 'metode_pembayaran' || strtolower($sb) == 'metodepembayaran' ) {
						require('page/shop/payment.php');
						$fun->setTitle('Payment');
						$fun->setDocTitle();
					}
					else if(strtolower($sb) == 'invoice' || strtolower($sb) == 'strukbelanja' || strtolower($sb) == 'struk') {
						require('page/shop/invoice.php');
						$fun->setTitle('Invoice');
						$fun->setDocTitle();
					}

					else {
						require('404.php');
					}
				}

				else {
					require('404.php');
				}
				// }
			}
			else {
				// BEFORE LOGIN { 
				if(strtolower($pg) == '' || strtolower($pg) == 'home' || strtolower($pg) == 'beranda' || strtolower($pg) == 'login' || strtolower($pg) == 'signin' || strtolower($pg) == 'masuk') {
					require('page/01baronang.php');
					$fun->setTitle('Sign In');
					$fun->setDocTitle();
				}
				
				else if(strtolower($pg) == 'forgotpass' || strtolower($pg) == 'forgotpassword' || strtolower($pg) == 'forgot' ||
					strtolower($pg) == 'changepass' || strtolower($pg) == 'changepassword' || strtolower($pg) == 'change' || 
					strtolower($pg) == 'gantipass'  || strtolower($pg) == 'gantipassword'  || strtolower($pg) == 'ganti'  ||
					strtolower($pg) == 'lupapass'   || strtolower($pg) == 'lupapassword'   || strtolower($pg) == 'lupa') {
					require('page/11changepasswordenteremail.php');
					$fun->setTitle('Forgot Password');
					$fun->setDocTitle();
				}

				else if(strtolower($pg) == 'updatepass' || strtolower($pg) == 'updatepassword' || strtolower($pg) == 'ubahpass' || strtolower($pg) == 'ubahpassword') {
					require('page/12changepasswordnewpassword.php');
					$fun->setTitle('Update Password');
					$fun->setDocTitle();
				}

				else if(strtolower($pg) == 'register' || strtolower($pg) == 'daftar') {
					require('page/13memberregister.php');
					$fun->setTitle('Register');
					$fun->setDocTitle();
				}

				else if(strtolower($pg) == 'aktivasi' || strtolower($pg) == 'activation' || strtolower($pg) == 'aktif' || strtolower($pg) == 'activ' || strtolower($pg) == 'activate' || strtolower($pg) == 'verfikasi' || strtolower($pg) == 'verification') {
					require('process/13bconfirmregister.php');
					$fun->setTitle('Register');
					$fun->setDocTitle();
				}

				else {
					require('404.php');
					$fun->setTitle('Cloud Parking - Error: 404!');
					$fun->setDocTitle();
				}
				// }
			}

			require('index_menu_modal_popup.php');
			?>
				
			<!---------------------------------------------------------------------------------------------->
		    <!---------------------------------------------------------------------------------------------->
		    <!-- Place all Action Elements and Menus Outside of the page-content class. Starting from here-->
		    <!---------------------------------------------------------------------------------------------->
		    <!---------------------------------------------------------------------------------------------->
		    
		    <div id="<?php echo $fun->Enlink('kamera'); ?>" class="menu menu-box-modal rounded-m">
		         <input type="file" class="upload-file bg-highlight shadow-s rounded-s" accept="image/*">
		         
		    </div>
		    
		    <div id="<?php echo $fun->Enlink('scanqr'); ?>" class="menu menu-box-modal rounded-m">
				<!--<video id="preview"></video>-->
				<div id="qr-reader" style="width:350px"></div>
				<div id="qr-reader-results"></div>
			</div>
		    
		    <div id="qrcode" 
				class="menu menu-box-bottom menu-box-detached rounded-m"
				data-menu-height="400" 
		        data-menu-width="300">
		        <h1 class="text-center mt-3 text-uppercase font-700">QR Code</h1>
		        <p class="boxed-text-l">
		             Scan this QR Code at Parking Gate
		        </p>
		        <div class="generate-qr-result"></div>
		    </div>
		     
		    <!---------------->
		    <!---------------->
		    <!--Menu Success-->
		    <!---------------->
		    <!---------------->
		    <div id="menu-success-1" class="menu menu-box-bottom menu-box-detached rounded-m" 
		         data-menu-height="305" 
		         data-menu-effect="menu-over">
		        <h1 class="text-center mt-4"><i class="fa fa-3x fa-check-circle color-green1-dark"></i></h1>
		        <h1 class="text-center mt-3 text-uppercase font-700">All's Good</h1>
		        <p class="boxed-text-l">
		             You can continue with your previous actions.<br> Easy to attach these to success calls.
		        </p>
		        <a href="#" class="close-menu btn btn-m btn-center-m button-s shadow-l rounded-s text-uppercase font-900 bg-green1-light">Great</a>
		    </div>       
		    
		    <div id="menu-success-2" class="menu menu-box-bottom menu-box-detached bg-green1-dark rounded-m" 
		         data-menu-height="305" 
		         data-menu-effect="menu-over">
		        <h1 class="text-center mt-4"><i class="fa fa-3x fa-check-circle color-white shadow-xl rounded-circle"></i></h1>
		        <h1 class="text-center mt-3 text-uppercase font-700 color-white">All's Good</h1>
		        <p class="boxed-text-l color-white opacity-70">
		             You can continue with your previous actions.<br> Easy to attach these to success calls.
		        </p>
		        <a href="#" class="close-menu btn btn-m btn-center-m button-s shadow-l rounded-s text-uppercase font-900 bg-white">Great</a>
		    </div>   
		    
		    <!-- Notification-->
		    <div id="notification-1" data-dismiss="notification-1" data-delay="3000" data-autohide="true" class="notification notification-ios bg-dark1-dark">
		        <span class="notification-icon">
		            <i class="fa fa-bell"></i>
		            <em>Enabled</em>
		            <i data-dismiss="notification-1" class="fa fa-times-circle"></i>
		        </span>
		        <h1 class="font-15 color-white mb-n3">All Good</h1>
		        <p class="pt-2">
		            I'm a notification. I show at the top or bottom of the page.
		        </p>
		    </div>  

		    <div id="notification-2" data-dismiss="notification-2" data-delay="3000" data-autohide="true" class="notification notification-android bg-dark1-dark">
		        <i class="fa fa-check bg-green1-dark"></i>
		        <h1 class="font-15 color-white">All Good</h1>
		        <strong>19:24</strong>
		        <p class="pb-0">
		            I'm a notification. I show at the top of the page. Awesome stuff here!
		        </p>
		        <div class="notification-buttons">
		            <div class="row mb-0">
		                <div class="col-6">
		                    <a href="#"><i class="fa fa-info-circle"></i>Find out More</a>
		                </div>
		                <div class="col-6">
		                    <a href="#"><i class="fa fa-download"></i>Download</a>
		                </div>
		            </div>
		        </div>
		    </div>    

		    <div id="notification-3" data-dismiss="notification-3" data-delay="3000" data-autohide="true"  class="notification notification-material bg-dark1-dark">
		        <span class="notification-icon">
		            <i class="fa fa-comment color-green1-light"></i>
		            <em class="color-green1-light">Hangouts</em>
		            <strong>Messenger</strong>
		        </span>
		        <img src="images/pictures/0t.jpg">
		        <h1 class="font-15 color-white">John Doe</h1>
		        <p class="pb-1">
		            I'm a notification. I show at the top of the page.
		        </p>
		    </div>    
		    
		    
			<!-- Be sure this is on your main visiting page, for example, the index.html page-->
		    <!-- Install Prompt for Android -->
		    <div id="menu-install-pwa-android" class="menu menu-box-bottom menu-box-detached rounded-l"
		         data-menu-height="350" 
		        data-menu-effect="menu-parallax">
		        <div class="boxed-text-l mt-4">
		            <img class="rounded-l mb-3" src="images/icon/Logo-fish-icon.png" alt="img" width="90">
		            <h4 class="mt-3">Baronang on your Home Screen</h4>
		            <p>
		                Install Baronang on your home screen, and access it just like a regular app. It really is that simple!
		            </p>
		            <a href="#" class="pwa-install btn btn-s rounded-s shadow-l text-uppercase font-900 bg-highlight mb-2">Add to Home Screen</a><br>
		            <a href="#" class="pwa-dismiss close-menu color-gray2-light text-uppercase font-900 opacity-60 font-10">Maybe later</a>
		            <div class="clear"></div>
		        </div>
		    </div>   

		    <!-- Install instructions for iOS -->
		    <div id="menu-install-pwa-ios" 
		        class="menu menu-box-bottom menu-box-detached rounded-l"
		         data-menu-height="320" 
		        data-menu-effect="menu-parallax">
		        <div class="boxed-text-xl mt-4">
		            <img class="rounded-l mb-3" src="images/icon/Logo-fish-icon.png" alt="img" width="90">
		            <h4 class="mt-3">Baronang on your Home Screen</h4>
		            <p class="mb-0 pb-3">
		                Install Baronang on your home screen, and access it just like a regular app.  Open your Safari menu and tap "Add to Home Screen".
		            </p>
		            <div class="clear"></div>
		            <a href="#" class="pwa-dismiss close-menu color-highlight font-800 opacity-80 text-center text-uppercase">Maybe later</a><br>
		            <i class="fa-ios-arrow fa fa-caret-down font-40"></i>
		        </div>
		    </div>
    
    	</div>
    	<!-- end of page -->

	    <script type="text/javascript">
	    	var cardid_tenantmanagement = 0;
			var cardid_shareaccess = 0;

			$("<?php echo '#'.$fun->Enlink('btnformlink_ok'); ?>").click(function() {
				//alert(cardid_tenantmanagement);
				var email = $("<?php echo '#'.$fun->Enlink('emaillinkcard'); ?>").val();
							
				$.ajax({
			        url : "process/ajax/ajax_link_cardemail.php",
			        type : 'POST',
			        data: { 
			            id: cardid_tenantmanagement,
			            email: email 
			        },
			        success : function(data) {
			            cardid_tenantmanagement = 0;
			            $("<?php echo '#'.$fun->Enlink('emaillinkcard'); ?>").val('');
			            //console.log(data);
			            window.location.href = "<?php echo '?'.$fun->setIDParam('status', 'Successful Link Card!'); ?>";
			        },
			        error : function(){
			            alert('Terjadi kesalahan dalam jaringan!\nGagal Link Kartu!\nSilahkan cek koneksi jaringan dan coba lagi!');
			            return false;
			        }
			    });
			});

			$("<?php echo '#'.$fun->Enlink('btnformshareaccess_ok'); ?>").click(function(){
				var email = $("<?php echo '#'.$fun->Enlink('emailshareaccess'); ?>").val();

				$.ajax({
			        url : "process/ajax/ajax_link_shareaccess_email.php",
			        type : 'POST',
			        data: { 
			            id: cardid_shareaccess,
			            email: email 
			        },
			        success : function(data) {
			        	cardid_shareaccess = 0;
			            $("<?php echo '#'.$fun->Enlink('emailshareaccess'); ?>").val('');
			            //console.log(data);
			            window.location.href = "<?php echo '?'.$fun->setIDParam('status', 'Successful Share Access!'); ?>";
			        },
			        error : function(){
			            alert('Terjadi kesalahan dalam jaringan!\nGagal Link Kartu!\nSilahkan cek koneksi jaringan dan coba lagi!');
			            return false;
			        }
			    });
			});

	    	$("<?php echo '#'.$fun->Enlink('btn_pinconfirm') ?>").click(function(){
				var id  = $("<?php echo '#'.$fun->Enlink('idttrigger') ?>").val();
				var pin = $("<?php echo '#'.$fun->Enlink('cfmpin') ?>").val();

				if(pin == '' || pin == null) {
					$("<?php echo '#'.$fun->Enlink('pinres') ?>").html('PIN can\'t be empty!');
				}
				else {

					$.ajax({
						url : "process/ajax/ajax_checkpin.php",
						type : 'POST',
						data: { 
							pin: pin
						},
						success : function(data) {
							if(data == 'pin1') {
								$(document).ready(function() {
									$("#"+id).click();
								});
							}
							else {
								$("<?php echo '#'.$fun->Enlink('pinres') ?>").html('PIN is invalid!');
							}

						},
						error : function(){
							alert('Terjadi kesalahan dalam jaringan!\nTidak dapat mengambil data Product!\nSilahkan cek koneksi jaringan dan coba lagi!');
							return false;
						}
					});

				}

			});
	    	
	    	//instascan	
	      	/*let scanner = new Instascan.Scanner({ video: document.getElementById('preview') });
	      	scanner.addListener('scan', function (content) {
	        	alert(content);
	        	scanner.stop(cameras[1]);
	      	});
	      	Instascan.Camera.getCameras().then(function (cameras) {
	        	if (cameras.length > 0) {
	          		scanner.start(cameras[1]);
	        	} 
	        	else {
	          		console.error('No cameras found.');
	        	}
	      	}).catch(function (e) {
	        	console.error(e);
	      	}); */
	    
	    	//  
	    	var resultContainer = document.getElementById('qr-reader-results');
			var lastResult, countResults = 0;

			function onScanSuccess(qrCodeMessage) {
				if (qrCodeMessage !== lastResult) {
					++countResults;
					lastResult = qrCodeMessage;
					resultContainer.innerHTML 
						+= `<div>[${countResults}] - ${qrCodeMessage}</div>`;
				}
			}

			var html5QrcodeScanner = new Html5QrcodeScanner("qr-reader", { fps: 10, qrbox: 250 });
			html5QrcodeScanner.render(onScanSuccess);

			//
			$("<?php echo '#'.$fun->Enlink('prov'); ?>").change(function() {
				var id = $("<?php echo '#'.$fun->Enlink('prov'); ?> :selected").val();
				//alert(id);

				$.ajax({
			    	url : "process/ajax/ajax_kabkot.php",
			    	type : 'POST',
			    	data: { 
			           	id: id
			        },
			        success : function(data) {
			           	$("<?php echo '#'.$fun->Enlink('kabkot'); ?>").html(data);
			        },
			        error : function(){
			            alert('Terjadi kesalahan dalam jaringan!\nTidak dapat mengambil data Kabupaten / Kota!\nSilahkan cek koneksi jaringan dan coba lagi!');
			         	return false;
			        }
			    });
			});
	    </script>
	    
	    
    <script type="application/javascript" src="https://sdki.truepush.com/sdk/v2.0.2/app.js" async></script>
    <script>
    var truepush = window.truepush || [];
            
    truepush.push(function(){
        truepush.Init({
            id: "5f0b391d9416c78083dbc6a4"
        },function(error){
          if(error) console.error(error);
        })
    })
    </script>
	    
	    <script type="text/javascript" src="scripts/jquery.js"></script>
		<script type="text/javascript" src="scripts/bootstrap.min.js"></script>
		<script type="text/javascript" src="scripts/custom.js"></script>
	</body>
</html>
