
	<div class="page-content">

		<div class="page-title page-title-small">
	        <h2 id="title"><a href="?" data-back-button><i class="fa fa-arrow-left"></i></a>Shopping Cart</h2>
	        <a href="#" data-menu="menu-main" class="bg-fade-gray1-dark shadow-xl preload-img" data-src="images/avatars/5s.png"></a>
	    </div>

	    <div class="card header-card shape-rounded" data-card-height="150">
	    	<div class="card-overlay bg-highlight opacity-95"></div>
	    	<div class="card-overlay dark-mode-tint"></div>
	    	<div class="card-bg preload-img" data-src="images/pictures/20s.jpg"></div>
		</div>

		<div class="mt-30">
			<form action="?pg=shop&sb=payment" method="post">
				<div id="<?php echo $fun->Enlink('product-shop'); ?>">
					<?php
					$card = "";
					$btn_purchase = 0;
					$scart = "SELECT * from 23eproductshop where status='2' and memberid='".$fun->getValookie('id')."' order by datecreated asc";
					$rcart = $fun->checkRowData($scart);

					if($rcart > 0) {
						$total = 0;
						$no = 1;
						$dcart = $fun->getData($scart, 2);
						foreach ($dcart as $d) {
							$card = $d['cardid'];

							if($d['qty'] == '' || empty($d['qty'])) {
								$qty = 1;
							}
							else {
								$qty = $d['qty'];
							}

							$s2 = "SELECT 	a.masterproductid as masterproductid,
											a.productdescription as productdescription,
											a.vehicletype as vehicletype,
											a.validity_days as validity_days,
											a.unit as unit,
											a.productprice as productprice,
											b.locationname as locationname  
									from 22amasterproduct as a 
									inner join 21masterlocation as b 
										on a.locationid = b.masterlocationid
									where a.masterproductid='".$d['productid']."'";
							$d2 = $fun->getData($s2);

							$subtotal = (int)$d2[0]['productprice'] * (int)$qty;
							?>
							<?php //SHOP ID  ?><input type="text" name="<?php echo $fun->Enlink('id').'['.$no.']'; ?>" id="<?php echo $fun->Enlink('id').'-'.$no; ?>" value="<?php echo $fun->Enval($d['productshopid']); ?>" class="hide" readonly>
							<input type="text" name="<?php echo $fun->Enlink('price').'['.$no.']'; ?>" id="<?php echo $fun->Enlink('price').'-'.$no; ?>" value="<?php echo $d2[0]['productprice']; ?>" class="hide" readonly>
							<input type="text" name="<?php echo $fun->Enlink('subtotal').'['.$no.']'; ?>" id="<?php echo $fun->Enlink('subtotal-'.$no); ?>" value="<?php echo $subtotal; ?>" class="hide" readonly>
							
							<div class="card card-style mb-3 " data-card-height="340">
					            <div class="pl-3 ml-1 mt-4">
					            	
									<p class="">
										<span class="bold"><?php echo $d2[0]['productdescription']; ?></span><br><br>
										<span class="bold">Location</span> : <span><?php echo $d2[0]['locationname']; ?></span><br/>
										<span class="bold">Vehicle Type</span> : <span><?php echo $d2[0]['vehicletype']; ?></span><br/>
										<span class="bold">Valid Day(s)</span> : <span><?php echo $d2[0]['validity_days'].' '.$d2[0]['unit']; ?></span><br/>
										<span class="bold">Start At</span> : <span><?php echo date('d F Y', strtotime($d['datestart'])); ?></span><br/>
										<span class="bold">Price</span> : <span>Rp. <?php echo number_format($d2[0]['productprice'],0,',','.'); ?></span><br/>
										<span class="bold">Sub Total</span> : <span>Rp.</span> <span id="<?php echo $fun->Enlink('subtotal_label-'.$no); ?>"><?php echo number_format($subtotal,0,',','.'); ?></p></span>
									</p>

									<div class="row">
										<div class="col-6">
											<input type='button' class='qtyminus' id="<?php echo $fun->Enlink('qtyminus-'.$no); ?>" value='-' field="quantity" />
										    
											<span id="<?php echo $fun->Enlink('qtylabel-'.$no); ?>"><?php echo $qty; ?></span>
											<?php //QUANTITY ?><input type='number' name='<?php echo $fun->Enlink('qty').'['.$no.']'; ?>' id="<?php echo $fun->Enlink('qty-'.$no); ?>" value='<?php echo $qty; ?>' class='hide'>
												    
											<input type='button' class='qtyplus' id="<?php echo $fun->Enlink('qtyplus-'.$no); ?>" value='+' field="quantity" />
										</div>

										<div class="col-6">
											<div class="right" style="margin-top: 2px; margin-right: 15px;">
						                    	<input type="checkbox" name="<?php echo $fun->Enlink('cbproduct').'['.$no.']'; ?>" id="<?php echo $fun->Enlink('cbproduct-'.$no); ?>" value="<?php echo $fun->Enval(1); ?>" checked>
						                    	<label for="<?php echo $fun->Enlink('cbproduct-'.$no); ?>"></label>
						                 	</div>
										</div>
									</div>

					            </div>
				        	</div>

							<script type="text/javascript">
								$("<?php echo '#'.$fun->Enlink('qtylabel-'.$no); ?>").click(function(){
									//$("<?php echo '#'.$fun->Enlink('qty-'.$no); ?>").removeClass("hide");
									//$("<?php echo '#'.$fun->Enlink('qtylabel-'.$no); ?>").addClass("hide");
								});

								$("<?php echo '#'.$fun->Enlink('cbproduct-'.$no); ?>").click(function(){
									var sbt = $("<?php echo '#'.$fun->Enlink('subtotal-'.$no); ?>").val();
									
									if( $("<?php echo '#'.$fun->Enlink('cbproduct-'.$no); ?>").is(':checked') ) {
										$("<?php echo '#'.$fun->Enlink('cbproduct-'.$no); ?>").val("<?php echo $fun->Enval(1); ?>");
										total = parseInt(total) + parseInt(sbt);
									}
									else {
										$("<?php echo '#'.$fun->Enlink('cbproduct-'.$no); ?>").val("<?php echo $fun->Enval(2); ?>");
										total = parseInt(total) - parseInt(sbt);
									}

									var ftotal = new Number(total.toFixed(0)).toLocaleString("id-ID");
									$("<?php echo '#'.$fun->Enlink('total'); ?>").val(total);
									$("<?php echo '#'.$fun->Enlink('total-label'); ?>").html(ftotal);
								});

								$("<?php echo '#'.$fun->Enlink('qtyminus-'.$no); ?>").click(function(){
									var prc = $("<?php echo '#'.$fun->Enlink('price').'-'.$no; ?>").val();
									var qty = $("<?php echo '#'.$fun->Enlink('qty-'.$no); ?>").val();
									var sbt = $("<?php echo '#'.$fun->Enlink('subtotal-'.$no); ?>").val();
									var res = parseInt(qty) - 1;
									if(res < 1) {
										//res = 1;
										var id = $("<?php echo '#'.$fun->Enlink('id').'-'.$no; ?>").val();
										$.ajax({
									    	url : "process/ajax/ajax_delete_product_shop.php",
									    	type : 'POST',
									    	data: { 
									        	productid: id
									    	},
									    	success : function(data) {
									    		//console.log(data);
									    		window.location.reload();
									    	},
									    	error : function(){
									        	alert('Terjadi kesalahan dalam jaringan!\nTidak dapat mengambil data Product!\nSilahkan cek koneksi jaringan dan coba lagi!');
									        	return false;
									    	}
									    });
									}
									else {
										sbt = parseInt(sbt) - parseInt(prc);
										total = parseInt(total) - parseInt(prc);
									}

									var ftotal = new Number(total.toFixed(0)).toLocaleString("id-ID");
									var fsbt = new Number(sbt.toFixed(0)).toLocaleString("id-ID");
									$("<?php echo '#'.$fun->Enlink('subtotal-'.$no); ?>").val(sbt);
									$("<?php echo '#'.$fun->Enlink('subtotal_label-'.$no); ?>").html(fsbt);
									$("<?php echo '#'.$fun->Enlink('total'); ?>").val(total);
									$("<?php echo '#'.$fun->Enlink('total-label'); ?>").html(ftotal);
									$("<?php echo '#'.$fun->Enlink('qty-'.$no); ?>").val(res);
									$("<?php echo '#'.$fun->Enlink('qtylabel-'.$no); ?>").html(res);
								});

								$("<?php echo '#'.$fun->Enlink('qtyplus-'.$no); ?>").click(function(){
									var prc = $("<?php echo '#'.$fun->Enlink('price').'-'.$no; ?>").val();
									var qty = $("<?php echo '#'.$fun->Enlink('qty-'.$no); ?>").val();
									var sbt = $("<?php echo '#'.$fun->Enlink('subtotal-'.$no); ?>").val();
									var res = parseInt(qty) + 1;
									total = parseInt(total) + parseInt(prc);
									sbt = parseInt(sbt) + parseInt(prc);

									var ftotal = new Number(total.toFixed(0)).toLocaleString("id-ID");
									var fsbt = new Number(sbt.toFixed(0)).toLocaleString("id-ID");
									$("<?php echo '#'.$fun->Enlink('subtotal-'.$no); ?>").val(sbt);
									$("<?php echo '#'.$fun->Enlink('subtotal_label-'.$no); ?>").html(fsbt);
									$("<?php echo '#'.$fun->Enlink('total'); ?>").val(total);
									$("<?php echo '#'.$fun->Enlink('total-label'); ?>").html(ftotal);
									$("<?php echo '#'.$fun->Enlink('qty-'.$no); ?>").val(res);
									$("<?php echo '#'.$fun->Enlink('qtylabel-'.$no); ?>").html(res);
								});
							</script>
							<?php
							$btn_purchase++; $no++;
							$total = $total + $subtotal;
						}
					}
					else { ?>
						<div class="card card-style mb-3 d-flex" data-card-height="70">
				            <div class="d-flex center">
				            	<div class="pl-3 ml-1">
					                <h4 class="font-600 pt-4">You're cart is empty</h4>
					            </div>
				            </div>
			        	</div>
						<?php
					}

					if($btn_purchase > 0) { ?>
						<div class="card card-style mb-3" data-card-height="140">
				            <div class="mt-3" style="margin-left: 15px; margin-right: 15px;">
				            	<div class="row justify-content-center">
									<div class="col-6 bold right">
										<span>Total : Rp.</span>
									</div>

									<div class="col-6">
										<span class="bold" id="<?php echo $fun->Enlink('total-label'); ?>">
											<?php
											echo number_format($total,0,',','.');
											?>
										</span>
									</div>
								</div>
				            </div>

				            <div style="margin-left: 15px; margin-right: 15px;">
				            	<button type="submit" name="confirm" id="confirm" class="btn btn-m btn-center-xl bg-highlight txt-white rounded-sm text-uppercase font-900 center width-100">Purchase</button>
				            </div>
			        	</div>
						<?php
						// CARD ID
						$fun->inputField('text', 'card', 'card', '', 'validate hide', 'txt-black', $fun->Enval($card), 'required', 'readonly');

						// TOTAl BELANJA
						$fun->inputField('number', 'total', 'total', '', 'validate hide', 'txt-black', $total, 'required', 'readonly');
					}
					?>
				</div>
			</form>
		 <div class="divider divider-margins"></div>
		<!-- footer and footer card-->
    	<div class="footer" data-menu-load="footer.php"></div>  
	</div>

<script type="text/javascript">
	$('#toBack').click(function() {
		window.location.href = "?";
	});

	<?php
	if($rcart > 0) { ?>
		var total = "<?php echo $total; ?>";
		<?php
	}
	?>
</script>
