	<div class="page-content">

		<div class="page-title page-title-small">
	        <h2 id="title"><a href="?" data-back-button><i class="fa fa-arrow-left"></i></a>Payment</h2>
	        <a href="#" data-menu="menu-main" class="bg-fade-gray1-dark shadow-xl preload-img" data-src="images/avatars/5s.png"></a>
	    </div>

	    <div class="card header-card shape-rounded" data-card-height="150">
	    	<div class="card-overlay bg-highlight opacity-95"></div>
	    	<div class="card-overlay dark-mode-tint"></div>
	    	<div class="card-bg preload-img" data-src="images/pictures/20s.jpg"></div>
		</div>

			<form action="process/42dproductshop.php" method="post">

				<?php
				$total = @$_POST[$fun->Enlink('total')] / 1000;
				$smcp = "SELECT mastercardid, points from 23amastercard where cardtype='SkyCard' and memberid='".$fun->getValookie('id')."'";
				$dmcp = $fun->getData($smcp);

				if($dmcp[0]['points'] <= $total) {
					if($dmcp[0]['points'] != 0 || $dmcp[0]['points'] != '' || empty($dmcp[0]['points'])) {
						$totalva = (int)@$_POST[$fun->Enlink('total')] - ((int)$dmcp[0]['points'] * 1000);
					}
					else {
						$totalva = (int)@$_POST[$fun->Enlink('total')] - ((int)$dmcp[0]['points'] * 1000);
					}
				}
				else {
					$totalva = (int)@$_POST[$fun->Enlink('total')];
				}


				$rbsky_chk = "";
				if($dmcp[0]['points'] != 0 || $dmcp[0]['points'] != '' || !empty($dmcp[0]['points']) ) { $rbsky_chk = "checked"; }
				else { $rbsky_chk = ""; }

				$rbvabca_chk = "";
				if($dmcp[0]['points'] <= $total) { $rbvabca_chk = "checked"; }
				else { $rbvabca_chk = ""; }


				$paymentto_val = "";
				if( ($rbsky_chk == "checked") && ($rbvabca_chk == "checked") ) {
					$paymentto_val = "skypoint-vabca";
				}
				else { 
					if($rbsky_chk == "checked") {
						$paymentto_val = "skypoint";
					}
					else {
						if($rbvabca_chk == "checked") {
							$paymentto_val = "vabca";
						}
						else {
							$paymentto_val = ""; 
						}
					}
					
				}
				
				$resskypoint = (int)$dmcp[0]['points'] - (int)$total;
				?>
				<div class="card card-style mb-3 d-flex" data-card-height="95">
		            <div class="d-flex">
		            	<div class="ml-3">
			                <h4 class="font-600 pt-4">
			                	<span class="bold">Total Shopping : </span><br>
			                	Rp. <?php echo $fun->FormatNumber(@$_POST[$fun->Enlink('total')]); ?>
			            	</h4>
			            </div>
		            </div>
	        	</div>

	        	<div class="card card-style mb-3" data-card-height="80">
					<div class="mt-3 ml-3">
						<div class="fac fac-checkbox fac-blue"><span></span>
		                    <input type="checkbox" name="<?php echo $fun->Enlink('payment'); ?>" id="<?php echo $fun->Enlink('skypoint') ?>" value="skypoint" <?php echo $rbsky_chk; ?> >
		                    <label for="<?php echo $fun->Enlink('skypoint') ?>" class="">Sky Point</label>
		                </div>

		                <div class="ml-4 bold">
							<span><?php echo $fun->FormatNumber($dmcp[0]['points']).' Points'; ?></span>
						</div>
					</div>
				</div>

				<div class="card card-style mb-3" data-card-height="85">
					<div class="mt-3 ml-3">
						<div class="fac fac-checkbox fac-blue"><span></span>
			                <input type="checkbox" name="<?php echo $fun->Enlink('payment'); ?>" id="<?php echo $fun->Enlink('vabca'); ?>" value="vabca" <?php echo $rbvabca_chk; ?> >
							<label for="<?php echo $fun->Enlink('vabca'); ?>">Virtual Account BCA</label>
						</div>

						<div class="bold ml-4">
							<span id="<?php echo $fun->Enlink('vaamount'); ?>"><?php echo 'Rp. '.$fun->FormatNumber($totalva); ?></span>
						</div>
					</div>
				</div>

				<div style="margin-left: 15px; margin-right: 15px;">
				    <button type="button" name="<?php echo $fun->Enlink('toConfirmPay'); ?>" id="<?php echo $fun->Enlink('toConfirmPay'); ?>" class="btn btn-m btn-center-xl bg-highlight txt-white rounded-sm text-uppercase font-900 width-100 mt-3">Pay</button>
					<a href="#" data-menu="<?php echo $fun->Enlink('confirm-pin'); ?>" name="<?php echo $fun->Enlink('toPay'); ?>" id="<?php echo $fun->Enlink('toPay'); ?>" class="hide">toPay</a>
				</div>

				<div class="hide">
					<?php //METODE PEMBAYARAN ?>
					<input type="text" name="<?php echo $fun->Enlink('paymentto'); ?>" id="<?php echo $fun->Enlink('paymentto'); ?>" value="<?php echo $paymentto_val; ?>">

					<?php //CARD ID ?>
					<input type="text" name="<?php echo $fun->Enlink('card'); ?>" id="<?php echo $fun->Enlink('card'); ?>" value="<?php echo @$_POST[$fun->Enlink('card')]; ?>">

					<?php //TOTAL BELANJA ?>
					<input type="text" name="<?php echo $fun->Enlink('total'); ?>" id="<?php echo $fun->Enlink('total'); ?>" value="<?php echo $fun->Enval(@$_POST[$fun->Enlink('total')]); ?>">

					<input type="text" name="<?php echo $fun->Enlink('pointtxt'); ?>" id="<?php echo $fun->Enlink('pointtxt'); ?>" value="<?php echo $dmcp[0]['points']; ?>">
				
					<button type="submit" name="<?php echo $fun->Enlink('submit_payment_confirm'); ?>" id="<?php echo $fun->Enlink('submit_payment_confirm'); ?>" class="">to Process</button>
				</div>

				<?php
				$no = 1;
				foreach(@$_POST[$fun->Enlink('id')] as $key) { 
					$cb = $fun->Denval(@$_POST[$fun->Enlink('cbproduct')][$no]);
					if($cb == 1) { ?>
						<?php //SHOP ID & SHOP QUANTITY ?>
						<input type="text" name="<?php echo $fun->Enlink('id').'['.$no.']';  ?>" id="<?php echo $fun->Enlink('id-'.$no);  ?>" value="<?php echo '--[ID]--'.$key.'--[QTY]--'.$fun->Enval(@$_POST[$fun->Enlink('qty')][$no]); ?>" class="hide"><br/>
						<?php
					}
					$no++;
				}
				?>
			</form>

	 <div class="divider divider-margins"></div>
		<!-- footer and footer card-->
    	<div class="footer" data-menu-load="footer.php"></div>  
	</div>


<?php
$fun->linkTo('#'.$fun->Enlink('notenough_skypoint_warning'), 'to Warning not enough skypoint', 'hide modal-trigger', 'toWarningSkypoint');
?>

<div class="menu menu-box-modal rounded-m" id="<?php echo $fun->Enlink('notenough_skypoint_warning'); ?>">
	<div class="mr-3 ml-3 mt-3">
		<p class="bold italic"><span>Your Skypoint is not enough for the transaction!</span></p>
		<p><span>Your current Skypoint : <?php echo $fun->FormatNumber($dmcp[0]['points']); ?> points</span></p>
		<p><span>Your total purchase : Rp. <?php echo $fun->FormatNumber(@$_POST[$fun->Enlink('total')]); ?></span></p>
		<p><span>Please choose another payment method</p>
		
		<button type="button" name="<?php echo $fun->Enlink('confirm_warning_skypoint'); ?>" id="<?php echo $fun->Enlink('confirm_warning_skypoint'); ?>" class="btn btn-m btn-center-xl bg-highlight txt-white rounded-sm text-uppercase font-900 width-100 mt-3">OK</button>
	</div>
</div>

<script type="text/javascript">
	var pay = "";
	var method = "";
	var skypoint = 0;
	var vabca = 0;
	var msg_payment = "";
	var msg_payment_amount = "";

	$('#toBack').click(function() {
		window.location.href = "?pg=shop";
	});

	//
	if( $("<?php echo '#'.$fun->Enlink('vabca'); ?>").is(':checked') ) {
		if( $("<?php echo '#'.$fun->Enlink('skypoint'); ?>").is(':checked') ) {
			msg_payment = "Skypoint and BCA Virtual Account";
			msg_payment_amount = "<?php echo $fun->FormatNumber($dmcp[0]['points']).' Points and Rp. '.$fun->FormatNumber($totalva); ?>";
		}
		else {
			msg_payment = "BCA Virtual Account";
			msg_payment_amount = "<?php echo 'Rp. '.$fun->FormatNumber($totalva); ?>";
		}
	}
	else {
		if( $("<?php echo '#'.$fun->Enlink('skypoint'); ?>").is(':checked') ) {
			msg_payment = "Skypoint";
			msg_payment_amount = "<?php echo $fun->FormatNumber($dmcp[0]['points']).' Points'; ?>";
		}
		else {
			msg_payment = "Nothing";
			msg_payment_amount = "";
		}
	}

	//
	$("<?php echo '#'.$fun->Enlink('skypoint'); ?>").click(function(){
		var total   = 0;
		var amount  = "<?php echo @$_POST[$fun->Enlink('total')]; ?>";
		var point   = $("<?php echo '#'.$fun->Enlink('pointtxt'); ?>").val();
		var payment = "";
		var ftotal  = 0;

		if( $("<?php echo '#'.$fun->Enlink('vabca'); ?>").is(':checked') ) {
			pay = $("<?php echo '#'.$fun->Enlink('vabca'); ?>").val();
			method = "vabca";
			vabca = 1;

			if( $("<?php echo '#'.$fun->Enlink('skypoint'); ?>").is(':checked') ) {
				skypoint = 1;
				total = parseInt(amount) - (parseInt(point) * 1000);
				if(total < 0) {
					total = 0;
				}

				ftotal = new Number(total.toFixed(0)).toLocaleString("id-ID");
				payment = "skypoint-vabca";
				msg_payment = "Skypoint and BCA Virtual Account";
				msg_payment_amount = "<?php echo $fun->FormatNumber($dmcp[0]['points']).' Points and Rp. '; ?>" + ftotal;
			}
			else {
				skypoint = 0;
				total = parseInt(amount);
				ftotal = new Number(total.toFixed(0)).toLocaleString("id-ID");

				payment = "vabca";
				msg_payment = "BCA Virtual Account";
				msg_payment_amount = "Rp. " + ftotal;
			}
		}
		else {
			pay = "";
			method = "";
			vabca = 0;

			total = parseInt(amount);
			ftotal = new Number(total.toFixed(0)).toLocaleString("id-ID");

			if( $("<?php echo '#'.$fun->Enlink('skypoint'); ?>").is(':checked') ) {
				payment = "skypoint";
				msg_payment = "Skypoint";
				msg_payment_amount = "<?php echo $fun->FormatNumber($dmcp[0]['points']).' Points'; ?>";
			}
			else {
				payment = "";
				msg_payment = "Nothing";
				msg_payment_amount = "";
			}
		}

		//console.log(payment);
		ftotal = new Number(total.toFixed(0)).toLocaleString("id-ID");
		$("<?php echo '#'.$fun->Enlink('vaamount'); ?>").html("Rp. " + ftotal);
		$("<?php echo '#'.$fun->Enlink('paymentto'); ?>").val(payment);
	});

	$("<?php echo '#'.$fun->Enlink('vabca'); ?>").click(function(){
		var total   = 0;
		var amount  = "<?php echo @$_POST[$fun->Enlink('total')]; ?>";
		var point   = $("<?php echo '#'.$fun->Enlink('pointtxt'); ?>").val();
		var payment = "";
		var ftotal;

		if( $("<?php echo '#'.$fun->Enlink('vabca'); ?>").is(':checked') ) {
			pay = $("<?php echo '#'.$fun->Enlink('vabca'); ?>").val();
			method = "vabca";
			vabca = 1;

			if( $("<?php echo '#'.$fun->Enlink('skypoint'); ?>").is(':checked') ) {
				skypoint = 1;
				total = parseInt(amount) - (parseInt(point) * 1000);
				if(total < 0) {
					total = 0;
				}

				ftotal = new Number(total.toFixed(0)).toLocaleString("id-ID");
				payment = "skypoint-vabca";
				msg_payment = "Skypoint and BCA Virtual Account";
				msg_payment_amount = "<?php echo $fun->FormatNumber($dmcp[0]['points']).' Points and Rp. '; ?>" + ftotal;
			}
			else {
				skypoint = 0;
				total = parseInt(amount);
				ftotal = new Number(total.toFixed(0)).toLocaleString("id-ID");

				payment = "vabca";
				msg_payment = "BCA Virtual Account";
				msg_payment_amount = "Rp. " + ftotal;
			}
		}
		else {
			pay = "";
			method = "";
			vabca = 0;

			total = parseInt(amount);
			ftotal = new Number(total.toFixed(0)).toLocaleString("id-ID");

			if( $("<?php echo '#'.$fun->Enlink('skypoint'); ?>").is(':checked') ) {
				payment = "skypoint";
				msg_payment = "Skypoint";
				msg_payment_amount = "<?php echo $fun->FormatNumber($dmcp[0]['points']).' Points'; ?>";
			}
			else {
				payment = "";
				msg_payment = "Nothing";
				msg_payment_amount = "";
			}
		}

		//console.log(payment);
		ftotal = new Number(total.toFixed(0)).toLocaleString("id-ID");
		$("<?php echo '#'.$fun->Enlink('vaamount'); ?>").html("Rp. " + ftotal);
		$("<?php echo '#'.$fun->Enlink('paymentto'); ?>").val(payment);
	});


	// 
	function toConfirmPayment(pay, msg) {
		if( pay == '' || pay == null ) {
				alert('Method Payment can\'t be empty!');
		}
		else {
			$(document).ready(function() {
				//$("<?php echo '#'.$fun->Enlink('paymentto'); ?>").val(method);
				$("<?php echo '#'.$fun->Enlink('confirm-pin-msg'); ?>").html(msg);
				$("<?php echo '#'.$fun->Enlink('toConfirmPIN'); ?>").click();
				$("<?php echo '#'.$fun->Enlink('idttrigger'); ?>").val("<?php echo $fun->Enlink('submit_payment_confirm'); ?>");
			});
				
		}
	}

	function toWarningNotEnoughSkyPoint() {
		$(document).ready(function() {
			$("<?php echo '#'.$fun->Enlink('toWarningSkypoint'); ?>").click();
		});
	}

	$("<?php echo '#'.$fun->Enlink('toConfirmPay'); ?>").click(function(){
		var msg = "You will make a payment using " + msg_payment + " " + msg_payment_amount;
		var total_purchase = parseInt("<?php echo $total; ?>");
		var skypoint_purchase = parseInt("<?php echo $dmcp[0]['points']; ?>");
		console.log("skypoint_purchase : " + skypoint_purchase);
		console.log("total_purchase : " + total_purchase);

		$(document).ready(function() {
			$("<?php echo '#'.$fun->Enlink('toPay'); ?>").click()
		});
		
		if( $("<?php echo '#'.$fun->Enlink('skypoint'); ?>").is(':checked') ) {
			if( $("<?php echo '#'.$fun->Enlink('vabca'); ?>").is(':checked') ) {
				pay = "skypoint-vabca";
				toConfirmPayment(pay, msg);
			}
			else {
				pay = "skypoint";

				if(skypoint_purchase >=  total_purchase) {
					toConfirmPayment(pay, msg);
				}
				else {
					toWarningNotEnoughSkyPoint();
				}

			}
		}
		else {
			if( $("<?php echo '#'.$fun->Enlink('vabca'); ?>").is(':checked') ) {
				pay = "vabca";
				toConfirmPayment(pay, msg);
			}
			else {
				pay = "";
				toConfirmPayment(pay, msg);
			}
		}

	});
</script>
