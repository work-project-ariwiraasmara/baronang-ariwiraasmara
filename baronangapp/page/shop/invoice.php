	<div class="page-content">
		<?php
		if(isset($_GET[$fun->Enlink('detail')])) {
			$linkback = "?pg=shop&sb=invoice";
		}
		else {
			$linkback = "?";
		}
    	?>

		<div class="page-title page-title-small">
	        <h2 id="title"><a href="<?php echo $linkback; ?>" data-back-button><i class="fa fa-arrow-left"></i></a>Invoice</h2>
	    	<a href="#" data-menu="menu-main" class="bg-fade-gray1-dark shadow-xl preload-img" data-src="images/avatars/5s.png"></a>
	    </div>

	    <div class="card header-card shape-rounded" data-card-height="150">
	    	<div class="card-overlay bg-highlight opacity-95"></div>
	    	<div class="card-overlay dark-mode-tint"></div>
	    	<div class="card-bg preload-img" data-src="images/pictures/20s.jpg"></div>
		</div>

			<?php
			if(isset($_GET[$fun->Enlink('detail')])) { 
				$smember = "SELECT * from 22bmastermember where mastermemberid='".$fun->getValookie('id')."'";
				$qmember = mysqli_query($fun->getConnection(), $smember) or die(mysqli_error($fun->getConnection()));
				$dmember = mysqli_fetch_array($qmember);

				$sinvoice = "SELECT * from 23fproductinvoice where invoiceid='".$fun->getIDParam('detail')."' and memberid='".$fun->getValookie('id')."' order by datecreate desc";
				$qinvoice = mysqli_query($fun->getConnection(), $sinvoice) or die(mysqli_error($fun->getConnection()));
				$dinvoice = mysqli_fetch_array($qinvoice);

				$smcp = "SELECT mastercardid, points from 23amastercard where cardtype='SkyCard' and memberid='".$fun->getValookie('id')."'";
				$qmcp = mysqli_query($fun->getConnection(), $smcp) or die(mysqli_error($fun->getConnection()));
				$dmcp = mysqli_fetch_array($qmcp);

				if($dinvoice['method_payment'] == 'skypoint') { 
					$cardtext = "Card Number";
					$cardnumber = $dmcp['mastercardid'];
					$mp = 'Sky Point'; 
				}
				else { 
					$cardtext = "VA BCA";
					$cardnumber = "5278".$dmember['memberphone'];
					$mp = 'VA BCA'; 
				}

				if($dinvoice['status'] == '2') {
					$action = "process/42econfirmationpaymentproductshopnonskypoint.php?".$fun->setIDParam('ID', $fun->getIDParam('detail'));
					$method = "post"; 
				}
				else {
					$action = "return false;";
					$method = "return false;"; 
				}

				$amount = (int)$dinvoice['total'] - (int)$dinvoice['total_paid'];
				?>
				<form action="<?php echo $action; ?>" method="<?php echo $method; ?>">
					<div class="card card-style">
						<div class="mt-4 ml-4">
							<h5 class="font-16 font-600"><?php echo $fun->getIDParam('detail'); ?></h5>
							<p>
								<span class="bold">Total Purchase</span> : <span>Rp. <?php echo $fun->FormatRupiah($dinvoice['total']); ?></span><br/>

								<?php
								if($dinvoice['status'] == '2') { 
									$intdatenow = strtotime(date('Y-m-d H:i:s'));

									if($dinvoice['dateinvalid'] == '' || empty($dinvoice['dateinvalid'])) { 
										$dateinvalid = date('Y-m-d H:i:s', strtotime($dinvoice['datecreate'] . ' +3 hours'));
										$intdatevalid = strtotime($dinvoice['datecreate'] . ' +3 hours');
									}
									else {
										$dateinvalid = $dinvoice['dateinvalid'];
										$intdatevalid =  strtotime($dinvoice['dateinvalid']);
									}
									?>
									<span class="bold">Date Invalid</span> : <span><?php echo $dateinvalid; ?></span><br/>
									<?php
								}
								?>

								<span class="bold">Date Shop</span> : <span><?php echo $dinvoice['datecreate']; ?></span><br/>
								<span class="bold">Method Payment</span> : <span><?php echo $mp; ?></span><br/>
								<span class="bold"><?php echo $cardtext; ?></span> : <span><?php echo $cardnumber; ?></span><br/>
								<span class="bold">Total Paid</span> : <span>Rp. <?php echo $fun->FormatRupiah($dinvoice['total_paid']); ?></span><br/>
								<span class="bold">Amount</span> : <span>Rp. <?php echo $fun->FormatRupiah($amount); ?></span>
							</p>
						</div>
					</div>

					<div class="divider divider-margins"></div>
					
					<div class="card card-style mt-2" data-card-height="40">
						<div class="mt-2 ml-4 bold">
							<span>Products :</span>
						</div>
					</div>

					<?php
					$s2 = "SELECT * from 23eproductshop where invoiceid='".$fun->getIDParam('detail')."' and status='1' order by datecreated desc";
					$q2 = mysqli_query($fun->getConnection(), $s2) or die(mysqli_error($fun->getConnection()));
					while($d2 = mysqli_fetch_array($q2)) { 

						$s3 = "SELECT * from 22amasterproduct where masterproductid='".$d2['productid']."'";
						$q3 = mysqli_query($fun->getConnection(), $s3) or die(mysqli_error($fun->getConnection()));
						$d3 = mysqli_fetch_array($q3);

						$subtotal = (int)$d2['qty'] * (int)$d3['productprice'];

						?>
						<div class="card card-style mt-2" data-card-height="220">
							<div class="ml-4 mt-3">
								<p>
									<span class="bold"><?php echo $d3['productdescription']; ?></span><br/><br/>
									<span class="bold"><?php echo $d3['vehicletype']; ?></span><br/>
									<span class="bold"><?php echo $d3['validity_days'].' '.$d3['unit']; ?></span><br/>
									<span><?php echo 'Rp. '.$fun->FormatRupiah($d3['productprice']); ?></span> <span class="bold"><?php echo ' x'.$d2['qty']; ?></span><br/>
									<span class="bold">Subtotal</span> : <span>Rp. <?php echo $fun->FormatRupiah($subtotal); ?></span><br/>
								</p>
							</div>
						</div>
						<div class="divider divider-margins"></div>
						<?php
					}
					?>

				</form>

				<?php
				if($dinvoice['status'] == '2') { 
					if($intdatevalid > $intdatenow) { ?>
						<div class="card card-style" data-card-height="105">
							<div class="mt-3 center">
								<span class="bold" id="timer_countdown" style="font-size: 20px;">Time Countdown</span>
								
								<div style="margin-left: 15px; margin-right: 15px;">
									<button type="submit" name="confirm" id="confirm" class="btn btn-m btn-center-xl bg-highlight txt-white rounded-sm text-uppercase font-900 center width-100">Confirm Payment</button>
								</div>
							</div>
						</div>
						<?php
					}
					else { ?>
						<div class="card card-style" data-card-height="50">
							<div class="mt-3 center">
								<div class="mb-4">
									<h5 class="font-16 font-600">This Transaction Has Expired!</h5>
								</div>
							</div>
						</div>
						<?php
					}
				}

			}
			else {
				$sinvoice = "SELECT * from 23fproductinvoice where memberid='".$fun->getValookie('id')."' order by datecreate desc";
				$qinvoice = mysqli_query($fun->getConnection(), $sinvoice) or die(mysqli_error($fun->getConnection()));
				$rinvoice = mysqli_num_rows($qinvoice);

				if($rinvoice > 0) {
					$intdatenow = strtotime(date('Y-m-d H:i:s'));
					while($dinvoice = mysqli_fetch_array($qinvoice)) { 
						$expired = 0;

						if($dinvoice['dateinvalid'] == '' || empty($dinvoice['dateinvalid'])) {
							$intdatevalid = strtotime($dinvoice['datecreate'] . ' +3 hours');
						}
						else {
							$intdatevalid =  strtotime($dinvoice['dateinvalid']);
						}

						if($dinvoice['status'] == '2') { 
							$class 	= "bold italic";

							if($intdatevalid > $intdatenow) {
								$status = 'waiting for payment..'; 
							}
							else {
								$status = 'expired'; 
							}
						}
						else { 
							$class 	= "bold";
							$status = 'paid'; 
						}
						?>
						<div class="mt-30 card card-style" data-card-height="85">
							<a href="<?php echo '?pg=shop&sb=invoice&'.$fun->setIDParam('detail', $dinvoice['invoiceid']); ?>" class="">
								<div class="mt-3 pl-3">
									<h5 class="font-16 font-600"><?php echo $dinvoice['invoiceid']; ?></h5>
									<p class="<?php echo $class; ?>">
										<span><?php echo $status; ?></span>
									</p>
								</div>
							</a>
						</div>
						<?php
					}
				}
				else { ?>
					<div class="mt-30 card card-style">
						<h5 class="font-16 font-600">There are no transaction!</h5>
					</div>
					
					<?php
				}
			}
			?>
		

	 	<div class="divider divider-margins"></div>
		
    	<!-- footer and footer card-->
    	<div class="footer" data-menu-load="footer.php"></div>

	</div>

<script type="text/javascript">
	$('#toBack').click(function() {

		<?php
		if(isset($_GET[$fun->Enlink('detail')])) { ?>
			window.location.href = "?pg=shop&sb=invoice";
			<?php
		}
		else { ?>
			window.location.href = "?";
			<?php
		}
		?>

	});

	<?php
	if($dinvoice['status'] == '2') { ?>
		var countDownDate = new Date("<?php echo date('M d, Y H:i:s', strtotime($dateinvalid)); ?>").getTime();
		console.log(countDownDate);

		// Update the count down every 1 second
		var x = setInterval(function() {

			// Get today's date and time
		  	var now = new Date().getTime();
		    
		  	// Find the distance between now and the count down date
		  	var distance = countDownDate - now;
		    
		  	// Time calculations for days, hours, minutes and seconds
		  	var days = Math.floor(distance / (1000 * 60 * 60 * 24));
		  	var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
		  	var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
		  	var seconds = Math.floor((distance % (1000 * 60)) / 1000);
		    
		  	// Output the result in an element with id="demo"
		  	document.getElementById("timer_countdown").innerHTML = days + "d " + hours + "h " + minutes + "m " + seconds + "s ";
		    
		  	// If the count down is over, write some text 
		  	if (distance < 0) {
		    	clearInterval(x);
		    	document.getElementById("timer_countdown").innerHTML = "EXPIRED";
		  	}
		}, 1000);
		<?php
	}
	?>
</script>
