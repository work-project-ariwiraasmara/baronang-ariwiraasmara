	<div class="page-content">

		<div class="page-title page-title-small">
	        <h2 id="title"><a href="?" data-back-button><i class="fa fa-arrow-left"></i></a>Product</h2>
	        <a href="#" data-menu="menu-main" class="bg-fade-gray1-dark shadow-xl preload-img" data-src="images/avatars/5s.png"></a>
	    </div>

	    <div class="card header-card shape-rounded" data-card-height="150">
	    	<div class="card-overlay bg-highlight opacity-95"></div>
	    	<div class="card-overlay dark-mode-tint"></div>
	    	<div class="card-bg preload-img" data-src="images/pictures/20s.jpg"></div>
		</div>

		<?php
		if( isset($_GET[$fun->Enlink('lokasi')]) ) {
			$sloc = "SELECT a.locationname as locationname, b.nama_prov as nama_prov, c.nama_kabkot as nama_kabkot
					from 21masterlocation as a 
					inner join 11amasterprovince as b 
						on a.locationprovince = b.id_prov
					inner join 11bmastercity as c
						on a.locationcity = c.id_kabkot
					where a.active = '1' and a.masterlocationid='".$fun->getIDParam('lokasi')."'";
			$dloc = $fun->getData($sloc, 2);
			?>
			<div class="card card-style">
		    	<p class="content txt-black">
		    		<span class="bold">Location :</span><br/><span><?php echo $dloc[0]['locationname']; ?></span><br>
		    	</p>
		    </div>

			<div class="card card-style">
            	<div class="content">
					<?php
					$s1 = "SELECT * from 22amasterproduct where active='1' and productprice > 0 and locationid='".$fun->getIDParam('lokasi')."' order by productdescription ASC";
					$d1 = $fun->getData($s1, 2);
					foreach($d1 as $d1) { 
						if($d1['vehicletype'] == 'Small Car') {
							$src = "images/icon/car.png";
							$icon = "fa fa-car fa-3x";
							$img = "background-image: url('images/icon/car.png'), linear-gradient(#ffffff, #ffffff); background-repeat: no-repeat; background-size: contain;";
						}
						else if($d1['vehicletype'] == 'Big Car') {
							$src = "images/icon/truck.png";
							$icon = "fa fa-truck fa-3x";
							$img = "background-image: url('images/icon/truck.png'), linear-gradient(#ffffff, #ffffff); background-repeat: no-repeat; background-size: contain;";
						}
						else {
							$src = "images/icon/motor.png";
							$icon = "fa fa-motorcycle fa-3x";
							$img = "background-image: url('images/icon/motor.png'), linear-gradient(#ffffff, #ffffff); background-repeat: no-repeat; background-size: contain;";
						}
						?>
						<a href="#" data-menu="<?php echo $fun->Enlink($d1['masterproductid']); ?>">
							<div class="d-flex justify-content-between">
								<div class="pl-3 ml-1 align-self-center">
									<p class="font-600 mb-0 bold"><?php echo $d1['additionaldesc'].'-'.$d1['productdescription']; ?></p>
				                    <p class="color-highlight mt-n1 font-11 bold">
				                        <?php echo 'Rp. '.$fun->FormatNumber($d1['productprice']).' / '.$d1['tipe'].' '.$d1['unit']; ?>
				                    </p>
								</div>
								<div class="pr-3 align-self-center right">
				                	<i class="<?php echo $icon; ?>"></i>
				                </div>
							</div>
						</a>
						<?php
					}
					?>
				</div>
			</div>
			<?php
		}
		else { ?>
			<div class="card card-style">
            	<div class="content">
					<?php
					$ano = 1;
					$s1 = "SELECT distinct 	a.id_prov as id_prov, a.nama_prov as nama_prov
							from 11amasterprovince as a 
							inner join 21masterlocation as b 
								on a.id_prov = b.locationprovince
							order by nama_prov ASC";
					$d1 = $fun->getData($s1, 2);
					foreach($d1 as $d1) { ?>
						<div class="accordion" id="<?php echo 'accordion-'.$ano; ?>">
							<div class="mb-0">

								<button class="btn accordion-btn border-0"  data-toggle="collapse" data-target="<?php echo '#collapse-'.$ano; ?>">
			                        <?php echo $d1['nama_prov']; ?>
			                        <i class="fa fa-chevron-down font-10 accordion-icon"></i>
			                    </button>

			                    <div id="<?php echo 'collapse-'.$ano; ?>" class="collapse"  data-parent="<?php echo '#accordion-'.$ano; ?>">
									<?php
									$s2 = "SELECT * from 21masterlocation where active = '1' and locationprovince='".$d1['id_prov']."' order by locationname ASC";
									$r2 = $fun->checkRowData($s2);
									if($r2 > 0) {
										$d2 = $fun->getData($s2, 2);
										foreach($d2 as $d2) { ?>
											<div class="pt-1 pb-2 pl-3 pr-3">
												<a href="<?php echo '?pg=card&sb=product&'.$fun->setIDParam('cardid', $fun->getIDParam('cardid')).'&'.$fun->setIDParam('lokasi', $d2['masterlocationid']); ?>">
													<?php echo $d2['locationname']; ?>
												</a>
											</div>
											<?php
										}
									}
									else { ?>
										<div class="pt-1 pb-2 pl-3 pr-3">[Data Not Available]</div>
										<?php
									}
									?>
								</div>

							</div>
						</div>
						<?php
						$ano++;
					}
					?>
				</div>
			</div>
			<?php
		}
		?>

		<div class="divider mt-4 mb-3"></div>

		<!-- footer and footer card-->
    	<div class="footer" data-menu-load="footer.php"></div>  
	</div>

<?php
if( isset($_GET[$fun->Enlink('lokasi')]) ) {
	$no = 1;
	$s1 = "SELECT * from 22amasterproduct where active='1' and productprice > 0 and locationid='".$fun->getIDParam('lokasi')."' order by productdescription ASC";
	$d1 = $fun->getData($s1, 2);
	foreach($d1 as $d1) { ?>
		<div id="<?php echo $fun->Enlink($d1['masterproductid']); ?>" 
             class="menu menu-box-modal menu-box-detached rounded-m" 
             data-menu-height="530" 
             data-menu-width="300" 
             data-menu-effect="menu-over">

            <h1 class="font-700 mt-3 ml-3"><?php echo $d1['additionaldesc'].'-'.$d1['productdescription']; ?></h1>

			<p class="boxed-text-xl justify mt-3">
				<span class="bold">Type :</span> <span><?php echo $d1['vehicletype']; ?></span><br/>
				<span class="bold">Quantity :</span> <span><?php echo $fun->FormatNumber($d1['validity_days']).'&nbsp;'.$d1['unit']; ?></span><br/>
				<span class="bold">Price : Rp.</span> <span><?php echo $fun->FormatRupiah($d1['productprice']); ?></span>
				
				<form action="<?php echo 'process/72addcardproduct.php?'.$fun->setIDParam('cardid', $fun->getIDParam('cardid')).'&'.$fun->setIDParam('company', '71').'&'.$fun->setIDParam('lokasi', $fun->getIDParam('lokasi')).'&'.$fun->setIDParam('productid', $d1['masterproductid']); ?>" method="post">
					<div class="mt-3" style="margin-left: 15px; margin-right: 15px;">
						<span class="bold">Quantity :</span>
						<input type='button' class='qtyminus' id="<?php echo $fun->Enlink('qtyminus-'.$no); ?>" value='-' field="quantity" />
								    
						<span id="<?php echo $fun->Enlink('qtylabel-'.$no); ?>">1</span>
						<input type='text' name='<?php echo $fun->Enlink('qty'); ?>' value='1' id="<?php echo $fun->Enlink('qty-'.$no); ?>" class='hide' />
								    
						<input type='button' class='qtyplus' id="<?php echo $fun->Enlink('qtyplus-'.$no); ?>" value='+' field="quantity" />
					
						<div class="divider mt-4 mb-3"></div>

						<span>Start At :</span>
						<input type="date" name="<?php echo $fun->Enlink('startat'); ?>" id="<?php echo $fun->Enlink('startat-'.$no); ?>">
					</div>

					<input type="text" name="<?php echo $fun->Enlink('productid'); ?>" id="<?php echo $fun->Enlink('productid-'.$no); ?>" value="<?php echo $fun->Enval($d1['masterproductid']); ?>" class="hide">
					<div style="margin-left: 15px; margin-right: 15px;">
						<button type="button" name="<?php echo $fun->Enlink('btn_cart-'.$no); ?>" 			 id="<?php echo $fun->Enlink('btn_cart-'.$no); ?>" 				class="btn btn-m btn-center-xl bg-highlight txt-white rounded-sm text-uppercase font-900 width-100 mt-3 mb-3">Add to Cart</button>
						<button type="button" name="<?php echo $fun->Enlink('btn_product_confirm-'.$no); ?>" id="<?php echo $fun->Enlink('btn_product_confirm-'.$no); ?>" 	class="btn btn-m btn-center-xl bg-highlight txt-white rounded-sm text-uppercase font-900 width-100 mt-3 mb-3">Purchase</button>
						<button type="submit" name="<?php echo $fun->Enlink('confirm-'.$no); ?>" 			 id="<?php echo $fun->Enlink('confirm-'.$no); ?>" 				class="hide">to Process</button>
					</div>
				</form>
			</p>
		</div>

		<script type="text/javascript">
			$("<?php echo '#'.$fun->Enlink('btn_product_confirm-'.$no); ?>").click(function(){
				var date = $("<?php echo '#'.$fun->Enlink('startat-'.$no); ?>").val();
				
				if(date == '' || date == null) {
					alert('Date can\'t be empty!');
				}
				else {
					$(document).ready(function() {
						$("<?php echo '#'.$fun->Enlink('toConfirmPIN'); ?>").click();
						$("<?php echo '#'.$fun->Enlink('idttrigger'); ?>").val("<?php echo $fun->Enlink('confirm-'.$no); ?>");
					});
				}
				
			});

			$("<?php echo '#'.$fun->Enlink('btn_cart-'.$no); ?>").click(function(){
				var product = $("<?php echo '#'.$fun->Enlink('productid-'.$no); ?>").val();
				var card 	= "<?php echo $fun->Enval($fun->getIDParam('cardid')); ?>";
				var qty 	= $("<?php echo '#'.$fun->Enlink('qty-'.$no); ?>").val();
				var date 	= $("<?php echo '#'.$fun->Enlink('startat-'.$no); ?>").val();

				$.ajax({
			    	url : "process/ajax/ajax_add_product_shop.php",
			    	type : 'POST',
			    	data: { 
			        	productid: product,
			        	qty: qty,
			        	date: date,
			        	cardid: card
			    	},
			    	success : function(data) {
			    		//console.log(data);
			    		window.location.href = "<?php echo '?pg=card&sb=product&'.$fun->setIDParam('cardid', $fun->getIDParam('cardid')).'&'.$fun->setIDParam('status', 'The product has been added to the basket'); ?>"
			    	},
			    	error : function(){
			        	alert('Terjadi kesalahan dalam jaringan!\nTidak dapat mengambil data Product!\nSilahkan cek koneksi jaringan dan coba lagi!');
			        	return false;
			    	}
			    });
			});


			$("<?php echo '#'.$fun->Enlink('qtyminus-'.$no); ?>").click(function(){
				var prc = $("<?php echo '#'.$fun->Enlink('price').'-'.$no; ?>").val();
				var qty = $("<?php echo '#'.$fun->Enlink('qty-'.$no); ?>").val();
				var res = parseInt(qty) - 1;
				if(res < 1) {
					res = 1;
				}

				$("<?php echo '#'.$fun->Enlink('qty-'.$no); ?>").val(res);
				$("<?php echo '#'.$fun->Enlink('qtylabel-'.$no); ?>").html(res);
			});

			$("<?php echo '#'.$fun->Enlink('qtyplus-'.$no); ?>").click(function(){
				var prc = $("<?php echo '#'.$fun->Enlink('price').'-'.$no; ?>").val();
				var qty = $("<?php echo '#'.$fun->Enlink('qty-'.$no); ?>").val();
				var res = parseInt(qty) + 1;

				$("<?php echo '#'.$fun->Enlink('qty-'.$no); ?>").val(res);
				$("<?php echo '#'.$fun->Enlink('qtylabel-'.$no); ?>").html(res);
			});
		</script>
		<?php
		$no++;
	}
}
?>
