	<div class="page-content">

		<div class="page-title page-title-small">
	        <h2 id="title"><a href="?" data-back-button><i class="fa fa-arrow-left"></i></a>Card Acitivty</h2>
	        <a href="#" data-menu="menu-main" class="bg-fade-gray1-dark shadow-xl preload-img" data-src="images/avatars/5s.png"></a>
	    </div>

	    <div class="card header-card shape-rounded" data-card-height="150">
	    	<div class="card-overlay bg-highlight opacity-95"></div>
	    	<div class="card-overlay dark-mode-tint"></div>
	    	<div class="card-bg preload-img" data-src="images/pictures/20s.jpg"></div>
		</div>

		<div class="card card-style">
	    	<p class="content txt-black">
	    		<span class="bold">Card Number :</span><br/><span><?php echo $fun->getIDParam('cardid'); ?></span><br>
	    	</p>
	    </div>

		<div class="card card-style">
            <div class="content">
                <div class="list-group list-custom-small">
					<?php
					$sact = "SELECT a.activitylogid as activitylogid,
									a.cardid as cardid, a.datecreated as datecreated,
									a.activitydescription as activitydescription,
									a.debet as debet, a.credit as credit,
									a.contraaccount as contraaccount,
									b.locationname as locationname
							from 31cmastercardactivity as a 
							inner join 21masterlocation as b 
								on a.locationid = b.masterlocationid
							where activitycode like '8%' and activitymemberid='".$fun->getValookie('id')."' and cardid='".$fun->getIDParam('cardid')."' order by datecreated desc limit 10";
					$ract = $fun->checkRowData($sact);
					if($ract > 0) {
						$dact = $fun->getData($sact, 2);
						?>
						<div class="row">
							<?php
							foreach($dact as $dact) { 
								if($dact['debet'] == '' || empty($dact['debet'])) {
									$dk = 'CR';
								}

								if($dact['credit'] == '' || empty($dact['credit'])) {
									$dk = 'DB';
								}
								?>
								<div class="col-6">
									<?php echo date('d F Y', strtotime($dact['datecreated'])) ?>
								</div>
										
								<div class="col-6 right">
									<?php echo ''; ?>
								</div>

								<div class="col-6">
									<?php echo $dact['activitydescription']; ?>
								</div>
										
								<div class="col-6 right">
									<?php echo $dact['locationname']; ?>
								</div>
								<?php
							}
							?>
						</div>
						<?php
					}
					else { ?>
						<h2 class="bold italic center">Data is Empty</h2>
						<?php
					}
					?>
				</div>
			</div>
		</div>

		<div class="divider mt-4 mb-3"></div>

		<!-- footer and footer card-->
    	<div class="footer" data-menu-load="footer.php"></div>  
	</div>