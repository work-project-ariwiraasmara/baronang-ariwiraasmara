	<div class="page-content">

		<div class="page-title page-title-small">
	        <h2 id="title"><a href="?" data-back-button><i class="fa fa-arrow-left"></i></a>Financial History</h2>
	        <a href="#" data-menu="menu-main" class="bg-fade-gray1-dark shadow-xl preload-img" data-src="images/avatars/5s.png"></a>
	    </div>

	    <div class="card header-card shape-rounded" data-card-height="150">
	    	<div class="card-overlay bg-highlight opacity-95"></div>
	    	<div class="card-overlay dark-mode-tint"></div>
	    	<div class="card-bg preload-img" data-src="images/pictures/20s.jpg"></div>
		</div>

		<div class="card card-style">
	    	<p class="content txt-black">
	    		<span class="bold">Card Number :</span><br/><span><?php echo $fun->getIDParam('cardid'); ?></span><br>
	    	</p>
	    </div>

	    <div class="card card-style">
            <div class="content">

				<div class="table-responsive">
					<table class="table table-borderless rounded-sm shadow-l" style="overflow: hidden;">
						<tbody>
							<?php
							$sfh = "SELECT * from 31cmastercardactivity 
									where 	activitycode not like '8%' and 
											activitymemberid='".$fun->getValookie('id')."' 
											and cardid='".$fun->getIDParam('cardid')."' order by datecreated desc limit 10";
							//echo $sfh;
							$qfh = mysqli_query($fun->getConnection(), $sfh) or die(mysqli_error($fun->getConnection()));
							$rfh = mysqli_num_rows($qfh);
							if($rfh > 0) {
								while($dfh = mysqli_fetch_array($qfh)) { 
									if($dfh['debet'] == 0 || empty($dfh['debet'])) {
										$dk = 'CR';
										$nominal = $dfh['credit'];
										$dp = 'Points CR';
										$point = $dfh['pointcredit'];
									}

									if($dfh['credit'] == 0 || empty($dfh['credit'])) {
										$dk = 'DB';
										$nominal = $dfh['debet'];
										$dp = 'Points DB';
										$point = $dfh['pointdebet'];
									}
									
									$s2 = "SELECT * from 22amasterproduct where masterproductid='".$dfh['contraaccount']."'";
									$q2 = mysqli_query($fun->getConnection(), $s2) or die(mysqli_error($fun->getConnection()));
									$d2 =  mysqli_fetch_array($q2);

									?>
									<tr>
										<td scope="row">
											<span>
											<?php 
											echo date('d F Y', strtotime($dfh['datecreated'])).'<br/>';
											echo $dfh['activitydescription'];
											?>
											</span>
										</td>

										<td scope="row">
											<span class="bold underline right"><?php echo $dp; ?></span><br/>
											<span class="right"><?php echo $fun->FormatNumber($point); ?></span>
										</td>

										<td scope="row" class="right">
											<span class="bold underline"><?php echo $dk; ?></span><br/>
											<span><?php echo $fun->FormatRupiah($nominal); ?></span>
										</td>
									</tr>

									<tr style="border-bottom: 1px solid #999;">
										<td scope="row" colspan="3"><span><?php echo $d2['productdescription']; ?></span></td>
									</tr>
									<?php
								}
							}
							else { ?>
								<tr>
									<td scope="row"><h2 class="bold italic center">Data is Empty</h2></td> 
								</tr>
								<?php
							}
							?>
						</tbody>
					</table>
				</div>

			</div>
		</div>

		<div class="divider mt-4 mb-3"></div>

		<!-- footer and footer card-->
    	<div class="footer" data-menu-load="footer.php"></div>  
	</div>