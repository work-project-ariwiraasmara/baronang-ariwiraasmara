	<div class="page-content">

		<div class="page-title page-title-small">
	        <h2 id="title"><a href="?" data-back-button><i class="fa fa-arrow-left"></i></a>Product Owned</h2>
	        <a href="#" data-menu="menu-main" class="bg-fade-gray1-dark shadow-xl preload-img" data-src="images/avatars/5s.png"></a>
	    </div>

	    <div class="card header-card shape-rounded" data-card-height="150">
	    	<div class="card-overlay bg-highlight opacity-95"></div>
	    	<div class="card-overlay dark-mode-tint"></div>
	    	<div class="card-bg preload-img" data-src="images/pictures/20s.jpg"></div>
		</div>

		<div class="card card-style">
            <div class="content">
            	<div class="list-group list-custom-small">
					<?php
					$like = '7102'.date('y').'100';
					$lokasi = $fun->getIDParam('lokasi');

					$sloc = "SELECT locationname from 21masterlocation where masterlocationid='$lokasi'";
					$dloc = $fun->getData($sloc);

					if($lokasi == '' || empty($lokasi)) {
						$stopup = "SELECT * from 22amasterproduct where masterproductid like '$like%' and active = '1'";
					}
					else {
						$stopup = "SELECT * from 22amasterproduct where masterproductid like '$like%' and locationid='$lokasi' and active = '1'";
					}

					$rtopup = $fun->checkRowData($stopup);
					if($rtopup > 0) {
						$dtopup = $fun->getData($stopup, 2);
						foreach($dtopup as $dtopup) { ?>
							<a href="#" data-menu="<?php echo $fun->Enlink($dtopup['masterproductid']); ?>">
								<?php echo $dtopup['productdescription']; ?>
							</a>
							<?php
						}
					}
					else { ?>
						<h2 class="bold italic center">Data is Empty</h2> 
						<?php
					}
					?>
				</div>
			</div>
		</div>

		<div class="divider mt-4 mb-3"></div>

		<!-- footer and footer card-->
    	<div class="footer" data-menu-load="footer.php"></div>  
	</div>

<?php
$no = 1;
foreach($dtopup as $dtopup) { ?>
	<div id="<?php echo $fun->Enlink($dtopup['masterproductid']); ?>" 
             class="menu menu-box-modal menu-box-detached rounded-m" 
             data-menu-height="400" 
             data-menu-width="300" 
             data-menu-effect="menu-over">

		<p class="boxed-text-xl justify mt-3">
			<span class="bold">Product Name :</span> <span><?php echo $dtopup['productdescription']; ?></span>
			<span class="bold">Get Point(s) :</span> <span><?php echo $fun->FormatNumber($dtopup['validity_days']); ?></span>
			<span class="bold">Price : Rp.</span> <span><?php echo $fun->FormatRupiah($dtopup['productprice']); ?></span>
			
			<form action="<?php echo 'process/71cardtopup.php?'.$fun->setIDParam('cardid', $fun->getIDParam('cardid')).'&'.$fun->setIDParam('company', '71').'&'.$fun->setIDParam('productid', $dtopup['masterproductid']).'&'.$fun->setIDParam('lokasi', $lokasi); ?>" method="post">
				<input type="number" name="<?php echo $fun->Enlink('amount'); ?>" id="<?php echo $fun->Enlink('amount-'.$no); ?>" value="<?php echo $dtopup['validity_days']; ?>" required>
				
				<button type="button" name="<?php echo $fun->Enlink('btn_topup_confirm-'.$no); ?>" id="<?php echo $fun->Enlink('btn_topup_confirm-'.$no); ?>" 		class="btn btn-m btn-center-xl bg-highlight txt-white rounded-sm text-uppercase font-900 width-100 mt-3 mb-3">Top Up</button>
				<button type="submit" name="<?php echo $fun->Enlink('submit_topup_confirm-'.$no); ?>" id="<?php echo $fun->Enlink('submit_topup_confirm-'.$no); ?>" class="hide">to Process</button>
			</form>
		</p>
	</div>

	<script type="text/javascript">
		$("<?php echo '#'.$fun->Enlink('btn_topup_confirm-'.$no); ?>").click(function(){
			var amount = $("<?php echo '#'.$fun->Enlink('amount-'.$no); ?>").val();
			if(amount == '' || amount == null) {
				alert('Amount can\'t be empty!');
			}
			else {
				$(document).ready(function() {
					$("<?php echo '#'.$fun->Enlink('toConfirmPIN'); ?>").click();
					$("<?php echo '#'.$fun->Enlink('idttrigger'); ?>").val("<?php echo $fun->Enlink('submit_topup_confirm-'.$no); ?>");
				});
			}
		});
	</script>
	<?php
	$no++;
}
?>