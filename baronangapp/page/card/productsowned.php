	<div class="page-content">

		<div class="page-title page-title-small">
	        <h2 id="title"><a href="?" data-back-button><i class="fa fa-arrow-left"></i></a>Product Owned</h2>
	        <a href="#" data-menu="menu-main" class="bg-fade-gray1-dark shadow-xl preload-img" data-src="images/avatars/5s.png"></a>
	    </div>

	    <div class="card header-card shape-rounded" data-card-height="150">
	    	<div class="card-overlay bg-highlight opacity-95"></div>
	    	<div class="card-overlay dark-mode-tint"></div>
	    	<div class="card-bg preload-img" data-src="images/pictures/20s.jpg"></div>
		</div>

		<div class="card card-style">
		    <p class="content txt-black">
		    	<span class="bold">Card Number :</span><br/><span><?php echo $fun->getIDParam('cardid'); ?></span><br>
		    </p>
		</div>

		<div class="card card-style">
            <div class="content">
            	<div class="list-group list-custom-small">
					<?php
					$date = date('Y-m-d');
					$sact = "SELECT a.productowned as productowned,
									a.cardid as cardid, a.startdate as startdate,
									a.expirationdate as expirationdate,
									b.productdescription as productdescription, 
									b.additionaldesc as additionaldesc,
									b.vehicletype as vehicletype,
									b.validity_days as qty,
									b.unit as unit,
									c.locationname as locationname
							from 31amastercardproductowned as a 
							inner join 22amasterproduct as b 
								on a.productowned = b.masterproductid
							inner join 21masterlocation as c 
								on b.locationid = c.masterlocationid
							where cardid='".$fun->getIDParam('cardid')."' and a.expirationdate > '".$date."'
							order by a.expirationdate";
					$ract = $fun->checkRowData($sact);
					if($ract > 0) { ?>
						<div class="row">
							<div class="col-4">
								<span class="bold">Location</span>
							</div>

							<div class="col-4">
								<span class="bold">Vehicle Type</span>
							</div>

							<div class="col-4 right">
								<span class="bold underline"><?php echo 'Expiration Date'; ?></span>
							</div>
						
							<?php
							$dact = $fun->getData($sact, 2);
							foreach($dact as $dact) { ?>
								<div class="col-4">
									<?php echo $dact['locationname']; ?>
								</div>

								<div class="col-4">
									<?php echo $dact['vehicletype']; ?>
								</div>
										
								<div class="col-4 right">
									<?php echo date('d F Y', strtotime($dact['expirationdate'])) ?>
								</div>
								<?php
							}
							?>
						</div>
						<?php
					}
					else { ?>
						<h2 class="bold italic center">Data is Empty</h2> 
						<?php
					}
					?>
				</div>
			</div>
		</div>

		<div class="divider mt-4 mb-3"></div>

		<!-- footer and footer card-->
    	<div class="footer" data-menu-load="footer.php"></div>  
	</div>
