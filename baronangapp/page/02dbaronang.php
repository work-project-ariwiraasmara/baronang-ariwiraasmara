   <div class="page-content">
        
        <div class="page-title page-title-small">
            <h2>Baronang Management</h2>
            <a href="#" data-menu="menu-main" class="bg-fade-gray1-dark shadow-xl preload-img" data-src="images/icon/Logo-fish-icon.png"></a>
        </div>

        <div class="card header-card shape-rounded" data-card-height="95">
            <div class="card-overlay bg-highlight opacity-95"></div>
            <div class="card-overlay dark-mode-tint"></div>
            <div class="card-bg preload-img" data-src="images/pictures/20s.jpg"></div>
        </div>
        
        <!--<div class="card card-style">
            <div class="content">
                <h4 class="font-600">Let's get Started</h4>
                <p>
                    Select the page you want to see first. You can always come back to this menu later on.
                </p>
            </div>
        </div>-->
        
        <div class="row text-center mb-3">
            <a href="?pg=company" class="col-6 pr-2">
                <div class="card card-style mr-0  pt-4 mb-3">
                    <h1 class="center-text pt-4">
                        <i class="fa fa-building fa-3x"></i>
                    </h1>
                    <h4 class="color-theme font-600">Company</h4>
                    <p class="mt-n2 font-11 color-highlight">
                        Manage Company Accounts
                    </p>
                    <p class="font-10 opacity-30 mb-1">Tap to View</p>
                </div>
            </a>
            
            <!--<a href="index-media.html" class="col-6 pl-2">
                <div class="card card-style ml-0 mb-3 pt-4">
                    <h1 class="center-text pt-4">
                        <i data-feather="image" 
                           data-feather-line="1" 
                           data-feather-size="50" 
                           data-feather-color="orange-dark" 
                           data-feather-bg="orange-fade-light">
                        </i>
                    </h1>
                    <h4 class="color-theme font-600">Tenant Approval</h4>
                    <p class="mt-n2 font-11 color-highlight">
                        Modal Menus & Actions
                    </p>
                    <p class="font-10 opacity-30 mb-1">Tap to View</p>
                </div>
            </a>-->
            
        </div>
        
        <div class="divider divider-margins"></div>

        <div class="content mb-2">
            <h5 class="float-left font-16 font-500 bold">Reports</h5>
            <a class="float-right font-12 color-highlight mt-n1" href="#">View All</a>
            <div class="clearfix"></div>
        </div>
        
        <div class="double-slider text-center owl-carousel owl-no-dots mb-4">
            <?php
            $sloc = "SELECT * from 21masterlocation where locationname <> 'Personal' and locationname <> 'Sky Parking'";
            $dloc = $fun->getData($sloc, 2);
            foreach ($dloc as $k) { 
                if($k['image'] == '' || empty($k['image'])) {
                    $img = "background-image: url('images/bg1.jpg'), linear-gradient(#0a3177, #081055);";
                }
                else {
                    $img = "background-image: url('images/company/".$k['image']."'), linear-gradient(#0a3177, #081055)";
                }
                ?>
                <a href="#" class="item">
                    <div data-card-height="100" class="card rounded-m shadow-l">
						<div class="card-center text-center mb-4">
							<h5 class="font-16 mb-9"><?php echo $k['locationname']; ?></h5>
                        </div>
                    </div>
                </a>
                <?php
            }
            ?>
        </div>    
        
        
        <!-- footer and footer card-->
        <div class="footer" data-menu-load="footer.php"></div>  
    </div>    
    <!-- end of page content-->
   
