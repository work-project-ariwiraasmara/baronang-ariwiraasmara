<?php
$s1 = "SELECT distinct 	a.masterlocationid as masterlocationid,
						a.locationname as locationname
		from 21masterlocation as a 
		inner join 23dmastertenant as b
			on a.masterlocationid = b.locationid
		where b.memberid='".$fun->getValookie('id')."'";
$d1 = $fun->getData($s1, 2);
foreach($d1 as $d1) {
	$tenantname = ''; $deactivated = 0;

	// KARTU 7100
	$scard1 = "SELECT 	a.mastertenantid as mastertenantid,
						a.tenantname as tenantname,
						b.mastercardid as mastercardid
				from 23dmastertenant as a 
				inner join 23amastercard as b 
					on a.mastertenantid = b.memberid
				where b.mastercardid like '7100%' and a.memberid='".$fun->getValookie('id')."' and a.locationid='".$d1['masterlocationid']."'";
	//echo $s2.'<br><br>';
	$rcard1 = $fun->checkRowData($scard1);
	if($rcard1 > 0) {
		$dcard1 = $fun->getData($scard1, 2);
		foreach($dcard1 as $dcard1) { 

			$s2 = "SELECT * from 31bmastercarduser where activationstatus='1' and cardid='".$dcard1['mastercardid']."'";
			$r2 = $fun->checkRowData($s2);

			if($r2 > 0) {
				$d2 = $fun->getData($s2);

				$s3 = "SELECT membername from 22bmastermember where mastermemberid='".$d2[0]['useridactivated']."'";
				$d3 = $fun->getData($s3);

				$tenantname = $d3[0]['membername'];
			}
			else {
				$tenantname = $dcard1['tenantname'];
			}

			$svhl = "SELECT b.vehicletype as vehicle, b.productimage as productimage
						from 31amastercardproductowned as a 
						inner join 22amasterproduct as b 
							on a.productowned = b.masterproductid
						where a.cardid='".$dcard1['mastercardid']."'";
			$dvhl = $fun->getData($svhl, 2);

			$locid = substr($dcard1['mastercardid'],0,9).'0000000';
			$sloc = "SELECT * from 21masterlocation where masterlocationid = '".$locid."'";
			$dloc = $fun->getData($sloc, 2);
			?>
			<div id="<?php echo $fun->Enlink($dcard1['mastercardid']); ?>" 
         		class="menu menu-box-modal menu-box-detached rounded-m" 
         		data-menu-height="320" 
         		data-menu-width="300" 
         		data-menu-effect="menu-over">

				<div class="card_tenant_container txt-white" style="padding: 10px;">
					<div class="card_sky_tenant" style="">
						<img src="images/logo skyparking 1.png" style="width: 35px !important; height: 35px !important;">
					</div>

					<div class="card_company_tenant">
						<?php
						if($dloc[0]['image'] == '' || empty($dloc[0]['image'])) {
							echo $dloc['locationname'];
						}
						else { ?>
							<img src="<?php echo 'images/location/'.$dloc[0]['image']; ?>" style="width: 35px !important; height: 35px !important;">
							<?php
						}
						?>
					</div>

					<img src="images/2.png" id="imgcard" style="width: 100% !important;">

					<div class="card_identity_tenant" style="font-size: 9px;">
						<?php
						echo $tenantname.'<br>';
						echo $dcard1['mastercardid'];
						?>
					</div>

					<div class="card_logo_tenant" style="margin-bottom: 1px;">
						<?php
						if($dvhl[0]['productimage'] == '' || empty($dvhl[0]['productimage'])) {?>
							<div style="margin-bottom: 10px;"><?php echo $dvhl['vehicle']; ?></div>
							<?php
						}
						else { ?>
							<div style="margin-bottom: 10px;">
								<img src="<?php echo 'images/icon/'.$dvhl[0]['productimage']; ?>" style="width: 40px !important; height: 20px !important;">
							</div>
							<?php
						}
						?>

						<img src="images/icon/Logo-whitefish-icon.png" style="width: 40px !important; height: 20px !important;">
					</div>
				</div>

				<input type="text" class="hide" name="<?php echo $fun->Enlink('link-'.$dcard1['mastercardid']); ?>" id="<?php echo $fun->Enlink('link-'.$dcard1['mastercardid']); ?>" value="<?php echo $fun->Enval($dcard1['mastercardid']); ?>">	
				<div style="margin-left: 10px; margin-right: 10px;">
					<?php
					if($deactivated > 0) { ?>
						<button type="button" class="btn btn-m btn-center-xl bg-highlight txt-white rounded-sm text-uppercase font-900 width-100 mt-3 mb-3" id="<?php echo $fun->Enlink('btndislink-'.$dcard1['mastercardid']); ?>">Deactivate User</button>
						<?php
					}
					else { ?>
						<button type="button" data-menu="<?php echo $fun->Enlink('formlinkcard'); ?>" class="btn btn-m btn-center-xl bg-highlight txt-white rounded-sm text-uppercase font-900 width-100 mt-3 mb-3" id="<?php echo $fun->Enlink('btnlink-'.$dcard1['mastercardid']); ?>">Assign Card User</button>
						<?php
					}
					?>

					<a href="#" class="btn btn-m btn-center-xl bg-highlight txt-white rounded-sm text-uppercase font-900 width-100 mt-3 mb-3">Financial History</a>
				</div>
			</div>

			<script type="text/javascript">
				<?php
				if($deactivated > 0) { ?>
					$("<?php echo '#'.$fun->Enlink('btndislink-'.$dcard1['mastercardid']); ?>").click(function() {
						var cardid = $("<?php echo '#'.$fun->Enlink('link-'.$dcard1['mastercardid']); ?>").val();

						$.ajax({
			                url : "process/ajax/ajax_unlink_cardtenant.php",
			                type : 'POST',
			                data: { 
			                	id: cardid
			                },
			                success : function(data) {
			                	window.location.href = "<?php echo '?pg=cardmanagement&'.$fun->setIDParam('status', 'Successful Unlink Card Tenant'); ?>";
			                },
			                error : function(){
			                    alert('Terjadi kesalahan dalam jaringan!\nTidak dapat mengambil data Product!\nSilahkan cek koneksi jaringan dan coba lagi!');
			                    return false;
			                }
			            });

					});
					<?php
				}
				else { ?>
					$("<?php echo '#'.$fun->Enlink('btnlink-'.$dcard1['mastercardid']); ?>").click(function() {
						cardid_tenantmanagement = $("<?php echo '#'.$fun->Enlink('link-'.$dcard1['mastercardid']); ?>").val();
						$("<?php echo '#'.$fun->Enlink('toLinkCard'); ?>").click();
					});
					<?php
				}
				?>
			</script>
			<?php
		}
	}


	// KARTU 7101
	$scard2 = "SELECT 	a.mastertenantid as mastertenantid,
						a.tenantname as tenantname,
						b.mastercardid as mastercardid
				from 23dmastertenant as a 
				inner join 23amastercard as b 
					on a.mastertenantid = b.memberid
				where b.mastercardid like '7101%' and a.memberid='".$fun->getValookie('id')."' and a.locationid='".$d1['masterlocationid']."'";
	//echo $s2.'<br><br>';
	$rcard2 = $fun->checkRowData($scard2);
	if($rcard2 > 0) {
		$dcard2 = $fun->getData($scard2, 2);
		foreach($dcard2 as $dcard2) { 

			$s2 = "SELECT * from 31bmastercarduser where activationstatus='1' and cardid='".$dcard2['mastercardid']."'";
			$r2 = $fun->checkRowData($s2);
			if($r2 > 0) {
				$d2 = $fun->getData($s2);

				$s3 = "SELECT membername from 22bmastermember where mastermemberid='".$d2['useridactivated']."'";
				$d3 = $fun->getData($s3);

				$tenantname = $d3[0]['membername'];
				$deactivated = 1;
			}
			else {
				$tenantname = $dcard2['tenantname'];
				$deactivated = 0;
			}

			$svhl = "SELECT b.vehicletype as vehicle, b.productimage as productimage
						from 31amastercardproductowned as a 
						inner join 22amasterproduct as b 
							on a.productowned = b.masterproductid
						where a.cardid='".$dcard2['mastercardid']."'";
			$dvhl = $fun->getData($svhl);

			$locid = substr($dcard2['mastercardid'],0,9).'0000000';
			$sloc = "SELECT * from 21masterlocation where masterlocationid = '".$locid."'";
			$dloc = $fun->getData($sloc);
			?>
			<div id="<?php echo $fun->Enlink($dcard2['mastercardid']); ?>" 
         		class="menu menu-box-modal menu-box-detached rounded-m" 
         		data-menu-height="320" 
         		data-menu-width="300" 
         		data-menu-effect="menu-over">

				<div class="card_tenant_container txt-white" style="padding: 10px;">
					<div class="card_sky_tenant">
						<img src="images/logo skyparking 1.png" style="width: 35px !important; height: 35px !important;">
					</div>

					<div class="card_company_tenant">
						<?php
						if($dloc[0]['image'] == '' || empty($dloc[0]['image'])) {
							echo $dloc[0]['locationname'];
						}
						else { ?>
							<img src="<?php echo 'images/location/'.$dloc[0]['image']; ?>" style="width: 35px !important; height: 35px !important;">
							<?php
						}
						?>
					</div>

					<img src="images/3.png" id="imgcard" style="width: 100% !important;">

					<div class="card_identity_tenant" style="font-size: 9px;">
						<?php
						echo $tenantname.'<br>';
						echo $dcard2['mastercardid'];
						?>
					</div>

					<div class="card_logo_tenant" style="margin-bottom: 1px;">
						<?php
						if($dvhl[0]['productimage'] == '' || empty($dvhl[0]['productimage'])) {?>
							<div style="margin-bottom: 10px;"><?php echo $dvhl[0]['vehicle']; ?></div>
							<?php
						}
						else { ?>
							<div style="margin-bottom: 10px;">
								<img src="<?php echo 'images/icon/'.$dvhl[0]['productimage']; ?>" style="width: 40px !important; height: 20px !important;">
							</div>
							<?php
						}
						?>

						<img src="images/icon/Logo-whitefish-icon.png" style="width: 40px !important; height: 20px !important;">
					</div>
				</div>

				<input type="text" class="hide" name="<?php echo $fun->Enlink('link-'.$dcard2['mastercardid']); ?>" id="<?php echo $fun->Enlink('link-'.$dcard2['mastercardid']); ?>" value="<?php echo $fun->Enval($dcard2['mastercardid']); ?>">
				<div style="margin-left: 10px; margin-right: 10px;">
					<?php
					if($deactivated > 0) { ?>
						<button type="button" class="btn btn-m btn-center-xl bg-highlight txt-white rounded-sm text-uppercase font-900 width-100 mt-3 mb-3" id="<?php echo $fun->Enlink('btndislink-'.$dcard2['mastercardid']); ?>">Deactivate User</button>
						<?php
					}
					else { ?>
						<button type="button" data-menu="<?php echo $fun->Enlink('formlinkcard'); ?>" class="btn btn-m btn-center-xl bg-highlight txt-white rounded-sm text-uppercase font-900 width-100 mt-3 mb-3" id="<?php echo $fun->Enlink('btnlink-'.$dcard2['mastercardid']); ?>">Assign Card User</button>
						<?php
					}
					?>

					<a href="#" class="btn btn-m btn-center-xl bg-highlight txt-white rounded-sm text-uppercase font-900 width-100 mt-3 mb-3">Financial History</a>
				</div>
			</div>

			<script type="text/javascript">
				<?php
				if($deactivated > 0) { ?>
					$("<?php echo '#'.$fun->Enlink('btndislink-'.$dcard2['mastercardid']); ?>").click(function() {
						var cardid = $("<?php echo '#'.$fun->Enlink('link-'.$dcard2['mastercardid']); ?>").val();

						$.ajax({
			                url : "process/ajax/ajax_unlink_cardtenant.php",
			                type : 'POST',
			                data: { 
			                	id: cardid
			                },
			                success : function(data) {
			                	window.location.href = "<?php echo '?pg=cardmanagement&'.$fun->setIDParam('status', 'Successful Deactivate User'); ?>";
			                },
			                error : function(){
			                    alert('Terjadi kesalahan dalam jaringan!\nTidak dapat mengambil data Product!\nSilahkan cek koneksi jaringan dan coba lagi!');
			                    return false;
			                }
			            });

					});
					<?php
				}
				else { ?>
					$("<?php echo '#'.$fun->Enlink('btnlink-'.$dcard2['mastercardid']); ?>").click(function() {
						cardid_tenantmanagement = $("<?php echo '#'.$fun->Enlink('link-'.$dcard2['mastercardid']); ?>").val();
						//console.log(cardid_tenantmanagement);
						//$("<?php echo '#'.$fun->Enlink('toLinkCard'); ?>").click();
					});
					<?php
				}
				?>
			</script>
			<?php
		}
	}


	// KARTU 7104
	$scard3 = "SELECT 	a.mastertenantid as mastertenantid,
						a.tenantname as tenantname,
						b.mastercardid as mastercardid
				from 23dmastertenant as a 
				inner join 23amastercard as b 
					on a.mastertenantid = b.memberid
				where b.mastercardid like '7104%' and a.memberid='".$fun->getValookie('id')."' and a.locationid='".$d1['masterlocationid']."'";
	//echo $s2.'<br><br>';
	$rcard3 = $fun->checkRowData($scard3);
	if($rcard3 > 0) {
		$dcard3 = $fun->getData($scard3, 2);
		foreach($dcard3 as $dcard3) { 

			$s2 = "SELECT * from 31bmastercarduser where activationstatus='1' and cardid='".$dcard3['mastercardid']."'";
			$r2 = $fun->checkRowData($s2);
			if($r2 > 0) {
				$d2 = $fun->getData($s2);

				$s3 = "SELECT membername from 22bmastermember where mastermemberid='".$d2['useridactivated']."'";
				$d3 = $fun->getData($s3);

				$tenantname = $d3[0]['membername'];
			}
			else {
				$tenantname = $dcard3['tenantname'];
			}

			$svhl = "SELECT b.vehicletype as vehicle, b.productimage as productimage
						from 31amastercardproductowned as a 
						inner join 22amasterproduct as b 
							on a.productowned = b.masterproductid
						where a.cardid='".$dcard3['mastercardid']."'";
			$dvhl = $fun->getData($svhl);

			$locid = '7101'.substr($dcard3['mastercardid'],4,5).'0000000';
			$sloc = "SELECT * from 21masterlocation where masterlocationid = '".$locid."'";
			$dloc = $fun->getData($sloc);
			?>
			<div id="<?php echo $fun->Enlink($dcard3['mastercardid']); ?>" 
         		class="menu menu-box-modal menu-box-detached rounded-m" 
         		data-menu-height="320" 
         		data-menu-width="300" 
         		data-menu-effect="menu-over">

				<div class="card_tenant_container txt-white" style="padding: 10px;">
					<div class="card_sky_tenant">
						<img src="images/logo skyparking 1.png" style="width: 35px !important; height: 35px !important;">
					</div>

					<div class="card_company_tenant">
						<?php
						if($dloc[0]['image'] == '' || empty($dloc[0]['image'])) {
							echo $dloc[0]['locationname'];
						}
						else { ?>
							<img src="<?php echo 'images/location/'.$dloc[0]['image']; ?>" style="width: 35px !important; height: 35px !important;">
							<?php
						}
						?>
					</div>

					<img src="images/4.png" id="imgcard" style="width: 100% !important;">

					<div class="card_identity_tenant" style="font-size: 9px;">
						<?php
						echo $tenantname.'<br>';
						echo $dcard3['mastercardid'];
						?>
					</div>

					<div class="card_logo_tenant " style="margin-bottom: 1px;">
						<?php
						if($dvhl[0]['productimage'] == '' || empty($dvhl[0]['productimage'])) {?>
							<div style="margin-bottom: 10px;"><?php echo $dvhl[0]['vehicle']; ?></div>
							<?php
						}
						else { ?>
							<div style="margin-bottom: 10px;">
								<img src="<?php echo 'images/icon/'.$dvhl[0]['productimage']; ?>" style="width: 40px !important; height: 20px !important;">
							</div>
							<?php
						}
						?>

						<img src="images/icon/Logo-whitefish-icon.png" style="width: 40px !important; height: 20px !important;">
					</div>
				</div>

				<input type="hidden" name="<?php echo $fun->Enlink('link-'.$dcard3['mastercardid']); ?>" id="<?php echo $fun->Enlink('link-'.$dcard3['mastercardid']); ?>" value="<?php echo $fun->Enval($dcard3['mastercardid']); ?>">
				<div style="margin-left: 10px; margin-right: 10px;">
					<?php
					if($deactivated > 0) { ?>
						<button type="button" class="btn btn-m btn-center-xl bg-highlight txt-white rounded-sm text-uppercase font-900 width-100 mt-3 mb-3" id="<?php echo $fun->Enlink('btndislink-'.$dcard2['mastercardid']); ?>">Deactivate User</button>
						<?php
					}
					else { ?>
						<button type="button" data-menu="<?php echo $fun->Enlink('formlinkcard'); ?>" class="btn btn-m btn-center-xl bg-highlight txt-white rounded-sm text-uppercase font-900 width-100 mt-3 mb-3" id="<?php echo $fun->Enlink('btnlink-'.$dcard2['mastercardid']); ?>">Assign Card User</button>
						<?php
					}
					?>
					
					<a href="#" class="btn btn-m btn-center-xl bg-highlight txt-white rounded-sm text-uppercase font-900 width-100 mt-3 mb-3">Financial History</a>
				</div>
			</div>

			<script type="text/javascript">
				<?php
				if($deactivated > 0) { ?>
					$("<?php echo '#'.$fun->Enlink('btndislink-'.$dcard3['mastercardid']); ?>").click(function() {
						var cardid = $("<?php echo '#'.$fun->Enlink('link-'.$dcard3['mastercardid']); ?>").val();

						$.ajax({
			                url : "process/ajax/ajax_unlink_cardtenant.php",
			                type : 'POST',
			                data: { 
			                	id: cardid
			                },
			                success : function(data) {
			                	window.location.href = "<?php echo '?pg=cardmanagement&'.$fun->setIDParam('status', 'Successful Unlink Card Tenant'); ?>";
			                },
			                error : function(){
			                    alert('Terjadi kesalahan dalam jaringan!\nTidak dapat mengambil data Product!\nSilahkan cek koneksi jaringan dan coba lagi!');
			                    return false;
			                }
			            });

					});
					<?php
				}
				else { ?>
					$("<?php echo '#'.$fun->Enlink('btnlink-'.$dcard3['mastercardid']); ?>").click(function() {
						cardid_tenantmanagement = $("<?php echo '#'.$fun->Enlink('link-'.$dcard3['mastercardid']); ?>").val();
						//console.log(cardid_tenantmanagement);
						//$("<?php echo '#'.$fun->Enlink('toLinkCard'); ?>").click();
					});
					<?php
				}
				?>
			</script>
			<?php
		}
	}

}
?>
<a href="#" data-menu="<?php echo $fun->Enlink('formlinkcard'); ?>" class="hide" id="<?php echo $fun->Enlink('toLinkCard'); ?>">toLinkCard</a>

<div id="<?php echo $fun->Enlink('formlinkcard'); ?>" 
    class="menu menu-box-modal menu-box-detached rounded-m" 
    data-menu-height="320" 
    data-menu-width="300" 
    data-menu-effect="menu-over">

	<h1 class="uppercase txt-black mt-3 ml-3">Assign Card User</h1>
	<p class="boxed-text-xl justify">
		<div class="input-style input-style-1 input-required" style="margin-left: 10px; margin-right: 10px;">
			<span>Enter User Email</span>
			<em>(required)</em>
			<input type="email" name="<?php echo $fun->Enlink('emaillinkcard'); ?>" id="<?php echo $fun->Enlink('emaillinkcard'); ?>" placeholder="Enter User Email">
		</div>
	</p>

	<div style="margin-left: 10px; margin-right: 10px;">
		<button type="button" class="btn btn-m btn-center-xl bg-highlight txt-white rounded-sm text-uppercase font-900 width-100 mt-3 mb-3" id="<?php echo $fun->Enlink('btnformlink_ok'); ?>">Assign User</button>
	</div>
</div>
