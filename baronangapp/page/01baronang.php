
	<div class="page-content">
		<div class="page-title page-title-small">
			<h1 class="txt-white center" id="title">Sign In</h1>
		</div>

		<div class="card header-card shape-rounded" data-card-height="150">
			<div class="card-overlay bg-highlight opacity-95"></div>
			<div class="card-overlay dark-mode-tint"></div>
			<div class="card-bg preload-img" data-src="images/pictures/20s.jpg"></div>
		</div>
				        
		<div class="card card-style">
			<div class="content mt-5 mb-0">
				<form action="process/01checkemailpassword.php" method="post">
					<div class="input-style has-icon input-style-1 input-required pb-1">
						<i class="input-icon fa fa-user color-theme"></i>
						<span>Email</span>
						<em>(required)</em>
						<input type="email" name="<?php echo $fun->Enlink('email'); ?>" id="<?php echo $fun->Enlink('email'); ?>" placeholder="Email">
					</div> 

					<div class="input-style has-icon input-style-1 input-required pb-1">
						<i class="input-icon fa fa-lock color-theme"></i>
						<span>Password</span>
						<em>(required)</em>
						<input type="password" name="<?php echo $fun->Enlink('pass'); ?>" id="<?php echo $fun->Enlink('pass'); ?>" placeholder="Password">
					</div>

					<button type="submit" name="confirm" class="btn btn-m mt-2 mb-4 btn-center-xl bg-highlight txt-white rounded-sm text-uppercase font-900 center width-100">Sign In</button>
				
					<div class="divider mt-4 mb-3"></div>

					<div class="d-flex">
	                    <div class="w-50 font-11 pb-2 color-theme opacity-60 pb-3 text-left">
							<a href="?pg=register" class="color-theme">Create Account</a>
						</div>
	                    <div class="w-50 font-11 pb-2 color-theme opacity-60 pb-3 text-right">
							<a href="?pg=forgotpass" class="color-theme">Forgot Password</a>
						</div>
	                </div>
				</form>
			</div>

		</div>

	</div>

