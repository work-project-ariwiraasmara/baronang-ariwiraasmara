<?php
$s1 = "SELECT locationname from 21masterlocation where masterlocationid='".$fun->getIDParam('lokasi')."'";
$d1 = $fun->getData($s1);
?>
	<div class="page-content">

		<div class="page-title page-title-small">
	        <h2 id="title"><a href="?" data-back-button><i class="fa fa-arrow-left"></i></a>Add Product</h2>
	        <a href="#" data-menu="menu-main" class="bg-fade-gray1-dark shadow-xl preload-img" data-src="images/avatars/5s.png"></a>
	    </div>

	    <div class="card header-card shape-rounded" data-card-height="150">
	    	<div class="card-overlay bg-highlight opacity-95"></div>
	    	<div class="card-overlay dark-mode-tint"></div>
	    	<div class="card-bg preload-img" data-src="images/pictures/20s.jpg"></div>
		</div>

		<form action="process/42aaddproduct.php" method="post">
			<div class="card card-style">
            	<div class="content mb-0">
					<input type="text" name="<?php echo $fun->Enlink('lokasi'); ?>" id="<?php echo $fun->Enlink('lokasi'); ?>" class="hide" value="<?php echo $fun->getIDParam('lokasi'); ?>">
					<h3 class="bolder"><?php echo 'Location : '.$d1[0]['locationname']; ?></h3>

					<div class="divider mt-4 mb-3"></div>
					
					<div class="input-style input-style-1 input-required">
						<span>Product Category :</span>
						<em><i class="fa fa-angle-down"></i></em>
						<select name="<?php echo $fun->Enlink('catprod'); ?>" id="<?php echo $fun->Enlink('catprod'); ?>" required>
							<?php
							$scatprod = "SELECT * from 22dproductcategory where categorytype = '100'";
							$dcatprod = $fun->getData($scatprod, 2);
							foreach($dcatprod as $dt) { ?>
								<option value="<?php echo $fun->Enval($dt['productcategoryid']) ?>"><?php echo $dt['productcategoryname']; ?></option>
								<?php
							}
							?>
						</select>
					</div>
					
					<div class="input-style input-style-1 input-required">
						<span>Product Type : </span>
						<em><i class="fa fa-angle-down"></i></em>
						<select name="<?php echo $fun->Enlink('tiprod'); ?>" id="<?php echo $fun->Enlink('tiprod'); ?>" required>
							<option value="">Choose category first..</option>
						</select>
					</div>

					<div class="row mt-3">
						<div class="col-6">
							<div class="input-style input-style-1 input-required">
								<span>Quantity</span>
								<input type="number" name="<?php echo $fun->Enlink('qty'); ?>" id="<?php echo $fun->Enlink('qty'); ?>" placeholder="Quantity">
							</div>
						</div>

						<div class="col-6">
							<div class="input-style input-style-1 input-required">
								<span>Unit :</span>
								<em><i class="fa fa-angle-down"></i></em>
								<select class="browser-default" name="<?php echo $fun->Enlink('prodit'); ?>" id="<?php echo $fun->Enlink('prodit'); ?>" required>
									<option value="">Choose category first..</option>
								</select>
							</div>
						</div>
					</div>
					
					<div class="input-style input-style-1 input-required">
						<span>Price</span>
						<input type="number" name="<?php echo $fun->Enlink('harga'); ?>" id="<?php echo $fun->Enlink('harga'); ?>" placeholder="Price">
					</div>
					
					<div class="input-style input-style-1 input-required">
						<span>Quota</span>
						<input type="number" name="<?php echo $fun->Enlink('quota'); ?>" id="<?php echo $fun->Enlink('quota'); ?>" placeholder="Quota">
					</div>
					
					<div class="input-style input-style-1 input-required">
						<span>Additional Description</span>
						<input type="text" name="<?php echo $fun->Enlink('deskripsi'); ?>" id="<?php echo $fun->Enlink('deskripsi'); ?>" placeholder="Additional Description" required>
					</div>

					<input type="text" name="<?php echo $fun->Enlink('process'); ?>" id="<?php echo $fun->Enlink('process'); ?>" value="product" class="hide">
					<button type="submit" name="confirm" id="confirm" class="btn btn-m btn-center-xl bg-highlight txt-white rounded-sm text-uppercase font-900 widt-100 mt-3 mb-3">Save</button>
				</div>
			</div>
		</form>

	</div>

<script type="text/javascript">
	$("<?php echo '#'.$fun->Enlink('catprod'); ?>").click(function(){
		var countunit = $("<?php echo '#'.$fun->Enlink('prodit'); ?> option").length;
		var category = $("<?php echo '#'.$fun->Enlink('catprod'); ?> :selected").val();
		//alert(countunit);

		$.ajax({
			url : "process/ajax/ajax_category_type.php",
			type : 'POST',
			data: { 
			    id: category
			},
			success : function(data) {
				$("<?php echo '#'.$fun->Enlink('tiprod'); ?>").html(data);
			},
			error : function(){
				alert('Terjadi kesalahan dalam jaringan!\nGagal Link Kartu!\nSilahkan cek koneksi jaringan dan coba lagi!');
			    return false;
			}
		});

		$.ajax({
			url : "process/ajax/ajax_category_unit.php",
			type : 'POST',
			data: { 
			    id: category
			},
			success : function(data) {
				$("<?php echo '#'.$fun->Enlink('prodit'); ?>").html(data);
			},
			error : function(){
				alert('Terjadi kesalahan dalam jaringan!\nGagal Link Kartu!\nSilahkan cek koneksi jaringan dan coba lagi!');
			    return false;
			}
		});
	});
</script>
