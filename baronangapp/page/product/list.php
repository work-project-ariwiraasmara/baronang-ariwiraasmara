<?php
$sloc = "SELECT a.locationname as locationname, a.locationaddress as locationaddess, b.nama_prov as nama_prov, c.nama_kabkot as nama_kabkot
		from 21masterlocation as a 
		inner join 11amasterprovince as b 
			on a.locationprovince = b.id_prov
		inner join 11bmastercity as c
			on a.locationcity = c.id_kabkot
		where a.masterlocationid='".$fun->getIDParam('lokasi')."'";
$dloc = $fun->getData($sloc);

$scm = "SELECT mastercardid from 23amastercard where cardtype='SkyCard' and status='1' and memberid='".$fun->getValookie('id')."'";
$dcm = $fun->getData($scm);
?>

	<div class="page-content">

		<div class="page-title page-title-small">
	        <h2 id="title"><a href="?" data-back-button><i class="fa fa-arrow-left"></i></a>Product</h2>
	        <a href="#" data-menu="menu-main" class="bg-fade-gray1-dark shadow-xl preload-img" data-src="images/avatars/5s.png"></a>
	    </div>

	    <div class="card header-card shape-rounded" data-card-height="150">
	    	<div class="card-overlay bg-highlight opacity-95"></div>
	    	<div class="card-overlay dark-mode-tint"></div>
	    	<div class="card-bg preload-img" data-src="images/pictures/20s.jpg"></div>
		</div>

		<div class="card card-style">
	    	<p class="content txt-black">
	    		<span class="bold"><?php echo 'Location : '.$dloc[0]['locationname']; ?></span><br>
	    		<span><?php echo $dloc[0]['locationaddess'].', '.$dloc[0]['nama_kabkot'].', '.$dloc[0]['nama_prov'] ?></span>
	    	</p>
	    </div>

		<?php
			$sproduct = "SELECT * from 22amasterproduct where active='1' and productprice > 0 and locationid='".$fun->getIDParam('lokasi')."' order by productdescription ASC";
			$rproduct = $fun->checkRowData($sproduct);
			if($rproduct > 0) {
				$dproduct = $fun->getData($sproduct, 2);
				foreach($dproduct as $k) { 
					if($k['vehicletype'] == 'Small Car') {
						$src = "images/icon/car.png";
						$icon = "fa fa-car fa-3x";
						$img = "background-image: url('images/icon/car.png'), linear-gradient(#ffffff, #ffffff); background-repeat: no-repeat; background-size: contain;";
					}
					else if($k['vehicletype'] == 'Big Car') {
						$src = "images/icon/truck.png";
						$icon = "fa fa-truck fa-3x";
						$img = "background-image: url('images/icon/truck.png'), linear-gradient(#ffffff, #ffffff); background-repeat: no-repeat; background-size: contain;";
					}
					else {
						$src = "images/icon/motor.png";
						$icon = "fa fa-motorcycle fa-3x";
						$img = "background-image: url('images/icon/motor.png'), linear-gradient(#ffffff, #ffffff); background-repeat: no-repeat; background-size: contain;";
					}
					?>
					<a href="#" class="card card-style mb-3 d-flex" data-card-height="105" data-menu="<?php echo $fun->Enlink('modal-'.$k['masterproductid']) ?>">
			            <div class="d-flex justify-content-between">
			                <div class="pl-3 ml-1 align-self-center">
			                    <p class="font-600 mb-0 pt-3 bold"><?php echo $k['productdescription']; ?></p>
			                    <p class="color-highlight mt-n1 font-11 bold">
			                        <?php echo 'Rp. '.$fun->FormatNumber($k['productprice']).' / '.$k['validity_days'].' '.$k['unit']; ?>
			                    </p>
			                </div>
			                <div class="pr-3 align-self-center">
			                	<i class="<?php echo $icon; ?>"></i>
			                </div>
			            </div>
			        </a>
					<?php
				}
			}
			else { ?>
				<div class="card card-style mb-3 d-flex" data-card-height="70">
		            <div class="d-flex center">
		            	<div class="pl-3 ml-1">
			                <h4 class="font-600 pt-4">Data not available</h4>
			            </div>
		            </div>
	        	</div>
				<?php
			}
		?>

		<div class="center mt-3 mb-4 ">
	    	<a href="<?php echo '?pg=product&sb=add&'.$fun->setIDParam('lokasi', $fun->getIDParam('lokasi')); ?>" class="btn btn-m btn-center-xl bg-highlight txt-white rounded-sm text-uppercase font-900"><span class="ion-android-add font-21"></span></a>
	    </div>

	    <div class="divider mt-4 mb-3"></div>

		<!-- footer and footer card-->
    	<div class="footer" data-menu-load="footer.php"></div>  
	</div>

	<?php
	if($rproduct > 0) {
		$no = 1;
		foreach($dproduct as $k) { ?>
			<div id="<?php echo $fun->Enlink('modal-'.$k['masterproductid']); ?>" 
				 class="menu menu-box-modal rounded-m" 
				 data-menu-height="320" 
				 data-menu-width="300"
				 data-menu-effect="menu-over">

				 <h1 class="font-700 mt-3" style="margin-left: 15px; margin-right: 15px;"><?php echo $k['productdescription']; ?></h1>
		        
				 <p class="boxed-text-xl justify mt-3" style="margin-left: 15px; margin-right: 15px;">
					<span class="bold">Type :</span> <span><?php echo $k['vehicletype']; ?></span><br/>
		            <span class="bold">Validity :</span> <span><?php echo $k['validity_days'].' '.$k['unit']; ?></span><br/>
		            <span class="bold">Price :</span> <span><?php echo $fun->FormatNumber($k['productprice']); ?></span>
				</p>

				<a href="<?php echo '?pg=product&sb=edit&'.$fun->setIDParam('ID', $k['masterproductid']).'&'.$fun->setIDParam('lokasi', $fun->getIDParam('lokasi')); ?>" class="btn btn-m btn-center-xl bg-highlight txt-white rounded-sm text-uppercase font-900 center">
					<i class="ion-android-create font-21"></i>
				</a>
		    </div>
			<?php
			$no++;
		}
	}
	?>

