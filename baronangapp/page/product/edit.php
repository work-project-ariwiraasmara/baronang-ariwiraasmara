<?php
$sf1 = "SELECT * from 22amasterproduct where masterproductid='".$fun->getIDParam('ID')."'";
$df1 = $fun->getData($sf1);

$s1 = "SELECT * from 21masterlocation where masterlocationid='".$fun->getIDParam('lokasi')."'";
$d1 = $fun->getData($s1);
?>

	<div class="page-content">

		<div class="page-title page-title-small">
	        <h2 id="title"><a href="?" data-back-button><i class="fa fa-arrow-left"></i></a>Edit Product</h2>
	        <a href="#" data-menu="menu-main" class="bg-fade-gray1-dark shadow-xl preload-img" data-src="images/avatars/5s.png"></a>
	    </div>

	    <div class="card header-card shape-rounded" data-card-height="150">
	    	<div class="card-overlay bg-highlight opacity-95"></div>
	    	<div class="card-overlay dark-mode-tint"></div>
	    	<div class="card-bg preload-img" data-src="images/pictures/20s.jpg"></div>
		</div>

		<form action="<?php echo 'process/42bupdateproduct.php?'.$fun->setIDParam('ID', $fun->getIDParam('ID')); ?>" method="post">
			<div class="card card-style">
		    	<p class="content txt-black">
		    		<span class="bold"><?php echo 'Location : '.$d1[0]['locationname']; ?></span><br/>
		    		<span><?php echo $df1[0]['productdescription']; ?></span>
		    		<input type="text" name="<?php echo $fun->Enlink('lokasi'); ?>" id="<?php echo $fun->Enlink('lokasi'); ?>" value="<?php echo $fun->getIDParam('lokasi'); ?>" class="hide" required>
		    	</p>
		    </div>

			<div class="card card-style">
            	<div class="content mb-0">

					<?php /*
					<div class="input-field m-t-30">
						<?php
						$fun->inputField('text', 'deskripsi', 'deskripsi', 'Deskripsi', 'validate', 'txt-black', $df1['productdescription'], 'required');
						?>
					</div>

					<div class="m-t-10">
						<?php
						$arrtipe = array('Motor', 'Mobil Kecil', 'Mobil Besar');
						$fun->selectField('tipe', 'tipe', 'Tipe', 'browser-default', 'txt-black', $arrtipe, $arrtipe, $df1['vehicletype'], 'required');
						?>
					</div>

					<div class="m-t-10">
						<span>Status</span> : <br>
						<select class="browser-default" name="<?php echo $fun->Enlink('status'); ?>" id="<?php echo $fun->Enlink('status'); ?>">
							<option value="<?php echo $fun->Enval('0'); ?>" <?php if($df1['active'] == '0') echo 'selected'; ?> >Tidak Aktif</option>
							<option value="<?php echo $fun->Enval('1'); ?>" <?php if($df1['active'] == '1') echo 'selected'; ?> >Aktif</option>
						</select>
					</div>
					*/ ?>

					<div class="input-style input-style-1 input-required">
						<span>Quantity</span>
						<input type="number" name="<?php echo $fun->Enlink('valid'); ?>" id="<?php echo $fun->Enlink('valid'); ?>" placeholder="Quantity" value="<?php echo $df1[0]['validity_days']; ?>">
					</div>

					<div class="input-style input-style-1 input-required">
						<span>Price</span>
						<input type="number" name="<?php echo $fun->Enlink('harga'); ?>" id="<?php echo $fun->Enlink('harga'); ?>" placeholder="Price" value="<?php echo $df1[0]['productprice']; ?>">
					</div>

					<button type="submit" name="confirm" id="confirm" class="btn btn-m btn-center-xl bg-highlight txt-white rounded-sm text-uppercase font-900 widt-100 mt-3 mb-3">OK</button>
				</div>
			</div>
		</form>

		<div class="divider divider-margins"></div>

	    <!-- footer and footer card-->
        <div class="footer" data-menu-load="footer.php"></div>  
	</div>