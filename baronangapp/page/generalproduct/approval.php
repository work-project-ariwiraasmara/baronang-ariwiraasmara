	<div class="page-content">

		<div class="page-title page-title-small">
	        <h2 id="title"><a href="?" data-back-button><i class="fa fa-arrow-left"></i></a>General Product Approval</h2>
	        <a href="#" data-menu="menu-main" class="bg-fade-gray1-dark shadow-xl preload-img" data-src="images/avatars/5s.png"></a>
	    </div>

	    <div class="card header-card shape-rounded" data-card-height="150">
	    	<div class="card-overlay bg-highlight opacity-95"></div>
	    	<div class="card-overlay dark-mode-tint"></div>
	    	<div class="card-bg preload-img" data-src="images/pictures/20s.jpg"></div>
		</div>

		<form action="process/42capprovalproduct.php" method="post">
			<div class="card card-style">
            	<div class="content mb-2">

					<div class="table-responsive">
						<table class="table table-borderless rounded-sm shadow-l" style="overflow: hidden;">
							<thead>
								<tr class="bg-gray1-dark text-center">
									<th scope="col" class="color-theme">Product</th>
									<th scope="col" class="color-theme">Expired</th>
									<th scope="col" class="color-theme">Price (Rp)</th>
									<th scope="col" class="color-theme">Allowance</th>
									<th scope="col" class="color-theme">Information</th>
									<th scope="col" class="color-theme">Approve?</th>
								</tr>
							</thead>

							<tbody>
								<?php
								$no = 1; 

								$sapp1 = "SELECT * from 21masterlocation order by locationname";
								$qapp1 = mysqli_query($fun->getConnection(), $sapp1) or die(mysqli_error($fun->getConnection()));
								while($dapp1 = mysqli_fetch_array($qapp1)) { 
									$title = 1;
									?>
									<tr>
										<td scope="row" class="center hide" id="<?php echo $fun->Enlink($dapp1['masterlocationid']); ?>" colspan="6"><h4 class="bold txt-black"><?php echo $dapp1['locationname']; ?></h4></td>
									</tr>

									<?php
									$sapp2 = "SELECT * from 22amasterproduct where active='2' and locationid='".$dapp1['masterlocationid']."'";
									$qapp2 = mysqli_query($fun->getConnection(), $sapp2) or die(mysqli_error($fun->getConnection()));
									$rapp2 = mysqli_num_rows($qapp2);
									
									if($rapp2 > 0) {
										while($dapp2 = mysqli_fetch_array($qapp2)) { ?>
											<tr>
												<td scope="row"><?php echo $dapp2['productdescription']; ?></td>
												<td scope="row" style="text-align: right;"><?php echo $fun->FormatNumber($dapp2['validity_days'])." ".$dapp2['unit']; ?></td>
												<td scope="row" style="text-align: right;"><?php echo $fun->FormatRupiah($dapp2['productprice']); ?></td>
												<td scope="row" style="text-align: right;"><?php echo $fun->FormatNumber($dapp2['allowance']); ?></td>
												<td scope="row">
													New
													<input type="hidden" name="<?php echo $fun->Enlink('ket').'['.$no.']'; ?>" id="<?php echo $fun->Enlink('ket').'-'.$no; ?>" value="<?php echo $fun->Enval('new'); ?>">
													<input type="hidden" name="<?php echo $fun->Enlink('productid').'['.$no.']'; ?>" id="<?php echo $fun->Enlink('productid').'-'.$no; ?>" value="<?php echo $fun->Enval($dapp2['masterproductid']); ?>">
												</td>
												<td scope="row">
													<input type="radio" name="<?php echo $fun->Enlink('approval').'['.$no.']'; ?>" id="<?php echo $fun->Enlink('approval_yes').'-'.$no; ?>" value="<?php echo $fun->Enval('1'); ?>">
													<label for="<?php echo $fun->Enlink('approval_yes').'-'.$no; ?>" class="txt-black">Ya</label>
													<br>
													<input type="radio" name="<?php echo $fun->Enlink('approval').'['.$no.']'; ?>" id="<?php echo $fun->Enlink('approval_no').'-'.$no; ?>" value="<?php echo $fun->Enval('3'); ?>">
													<label for="<?php echo $fun->Enlink('approval_no').'-'.$no; ?>" class="txt-black">Tidak</label>
												</td>
											</tr>
											<?php
											$no++;
										}
										$title = $title + 1;
									}
									else {
										$title = $title - 1;
									}

									
									$sapp3 = "SELECT distinct 	c.masterhistoryproduct as masterhistoryproduct,
															b.productdescription as productdescription, 
															c.validity_days as validity_days,
															c.price as productprice
												from 22amasterproduct as b
												inner join 22chistoryproduct as c
													on b.masterproductid = c.productid
												where c.status='2' and b.locationid='".$dapp1['masterlocationid']."'";
									$qapp3 = mysqli_query($fun->getConnection(), $sapp3) or die(mysqli_error($fun->getConnection()));
									$rapp3 = mysqli_num_rows($qapp3);

									if($rapp3 > 0) {
										while($dapp3 = mysqli_fetch_array($qapp3)) { ?>
											<tr>
												<td scope="row"><?php echo $dapp3['productdescription']; ?></td>
												<td scope="row" style="text-align: right;"><?php echo $fun->FormatNumber($dapp3['validity_days']); ?></td>
												<td scope="row" style="text-align: right;"><?php echo $fun->FormatRupiah($dapp3['productprice']); ?></td>
												<td scope="row">
													Update
													<input type="hidden" name="<?php echo $fun->Enlink('productid').'['.$no.']'; ?>" id="<?php echo $fun->Enlink('productid').'-'.$no; ?>" value="<?php echo $fun->Enval($dapp3['masterhistoryproduct']); ?>">
													<input type="hidden" name="<?php echo $fun->Enlink('ket').'['.$no.']'; ?>" id="<?php echo $fun->Enlink('ket').'-'.$no; ?>" value="<?php echo $fun->Enval('update'); ?>">
												</td>
												<td scope="row">
													<input type="radio" name="<?php echo $fun->Enlink('approval').'['.$no.']'; ?>" id="<?php echo $fun->Enlink('approval_yes').'-'.$no; ?>" value="<?php echo $fun->Enval('1'); ?>">
													<label for="<?php echo $fun->Enlink('approval_yes').'-'.$no; ?>" class="txt-black">Ya</label>
													<br>
													<input type="radio" name="<?php echo $fun->Enlink('approval').'['.$no.']'; ?>" id="<?php echo $fun->Enlink('approval_no').'-'.$no; ?>" value="<?php echo $fun->Enval('3'); ?>">
													<label for="<?php echo $fun->Enlink('approval_no').'-'.$no; ?>" class="txt-black">Tidak</label>
												</td>
											</tr>
											<?php
											$no++;
										}
										$title = $title + 1;
									}
									else {
										$title = $title - 1;
									}

									if($title > 0) { ?>
										<script type="text/javascript">
											$("<?php echo '#'.$fun->Enlink($dapp1['masterlocationid']); ?>").removeClass("hide");
											console.log("<?php echo '#'.$dapp1['masterlocationid'].': '.$title; ?>");
										</script>
										<?php
									}
								}
								?>
							</tbody>
						</table>
					</div>

					<input type="hidden" name="<?php echo $fun->Enlink('jml_no'); ?>" id="<?php echo $fun->Enlink('jml_no'); ?>" value="<?php echo $fun->Enval($no); ?>">
					<button type="submit" name="btnok" id="btnok" class="btn btn-m btn-center-xl bg-highlight txt-white rounded-sm text-uppercase font-900 width-100 mt-3 mb-3">Approve</button>
				</div>
			</div>
		</form>

		<div class="divider mt-4 mb-3"></div>

		<!-- footer and footer card-->
    	<div class="footer" data-menu-load="footer.php"></div>  
	</div>

<script type="text/javascript">
	$("<?php echo '#'.$fun->Enlink('btnlokasi'); ?>").click(function(){
		var lokasi = $("<?php echo '#'.$fun->Enlink('lokasi'); ?>").val();

		window.location.href = "<?php echo '?pg=product&sb=approval&'.$fun->Enlink('lokasi').'='; ?>" + lokasi;
	});
</script>
