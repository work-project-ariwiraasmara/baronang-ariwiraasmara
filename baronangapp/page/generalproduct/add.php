<?php
$s1 = "SELECT masterlocationid from 21masterlocation where active = '3'";
$d1 = $fun->getData($s1);
$locid = $d1[0]['masterlocationid'];
//$catetype = '7102'.date('y').'0000000000';
?>

	<div class="page-content">

		<div class="page-title page-title-small">
	        <h2 id="title"><a href="?" data-back-button><i class="fa fa-arrow-left"></i></a>Add General Product</h2>
	        <a href="#" data-menu="menu-main" class="bg-fade-gray1-dark shadow-xl preload-img" data-src="images/avatars/5s.png"></a>
	    </div>

	    <div class="card header-card shape-rounded" data-card-height="150">
	    	<div class="card-overlay bg-highlight opacity-95"></div>
	    	<div class="card-overlay dark-mode-tint"></div>
	    	<div class="card-bg preload-img" data-src="images/pictures/20s.jpg"></div>
		</div>

		<form action="process/42aaddproduct.php" method="post">
			<div class="card card-style">
            	<div class="content mb-0">

            		<input type="text" name="<?php echo $fun->Enlink('lokasi'); ?>" id="<?php echo $fun->Enlink('lokasi'); ?>" value="<?php echo $locid; ?>" class="hide" required>
            		
					<div class="input-style input-style-1 input-required">
						<span>Product Category :</span>
						<em><i class="fa fa-angle-down"></i></em>
						<select name="<?php echo $fun->Enlink('catprod'); ?>" id="<?php echo $fun->Enlink('catprod'); ?>" required>
							<?php
							$scatprod = "SELECT * from 22dproductcategory where categorytype = '000'";
							$dcatprod = $fun->getData($scatprod, 2);
							foreach($dcatprod as $dt) { ?>
								<option value="<?php echo $fun->Enval($dt['productcategoryid']); ?>"><?php echo $dt['productcategoryname']; ?></option>
								<?php
							}
							?>
						</select>
					</div>
					
					<div class="input-style input-style-1 input-required">
						<span>Product Type :</span>
						<em><i class="fa fa-angle-down"></i></em>
						<select name="<?php echo $fun->Enlink('tiprod'); ?>" id="<?php echo $fun->Enlink('tiprod'); ?>" required>
							<option value="">Choose category first..</option>
						</select>
					</div>
					
					<div class="row">
						<div class="col-6">
							<div class="input-style input-style-1 input-required">
								<span>Quantity</span>
								<input type="number" name="<?php echo $fun->Enlink('qty'); ?>" id="<?php echo $fun->Enlink('qty'); ?>" placeholder="Quantity">
							</div>
						</div>
						
						<div class="col-6">
							<div class="input-style input-style-1 input-required">
								<span>Unit :</span>
								<em><i class="fa fa-angle-down"></i></em>
								<select name="<?php echo $fun->Enlink('prodit'); ?>" id="<?php echo $fun->Enlink('prodit'); ?>" required>
									<option value="">Choose category first..</option>
								</select>
							</div>
						</div>
					</div>

					<div class="input-style input-style-1 input-required" style="margin-top: -35px;">
						<span>Price</span>
						<input type="number" name="<?php echo $fun->Enlink('harga'); ?>" id="<?php echo $fun->Enlink('harga'); ?>" placeholder="Price">
					</div>
					
					<div class="input-style input-style-1 input-required">
						<span>Quota</span>
						<input type="number" name="<?php echo $fun->Enlink('quota'); ?>" id="<?php echo $fun->Enlink('quota'); ?>" placeholder="Quota">
					</div>
					
					<div class="input-style input-style-1 input-required">
						<span>Additional Description</span>
						<em>(required)</em>
						<input type="text" name="<?php echo $fun->Enlink('deskripsi'); ?>" id="<?php echo $fun->Enlink('deskripsi'); ?>" placeholder="Additional Description">
					</div>

					<input type="text" name="<?php echo $fun->Enlink('process'); ?>" id="<?php echo $fun->Enlink('process'); ?>" value="product" class="hide">
					<button type="submit" name="confirm" id="confirm" class="btn btn-m btn-center-xl bg-highlight txt-white rounded-sm text-uppercase font-900 widt-100 mt-3 mb-3">Save</button>
				</div>
			</div>
		</form>

		<div class="divider mt-4 mb-3"></div>

		<!-- footer and footer card-->
    	<div class="footer" data-menu-load="footer.php"></div>  
	</div>

<script type="text/javascript">
	$("<?php echo '#'.$fun->Enlink('catprod'); ?>").click(function(){
		var countunit = $("<?php echo '#'.$fun->Enlink('prodit'); ?> option").length;
		var category = $("<?php echo '#'.$fun->Enlink('catprod'); ?> :selected").val();
		//alert(countunit);

		$.ajax({
			url : "process/ajax/ajax_category_type.php",
			type : 'POST',
			data: { 
			    id: category
			},
			success : function(data) {
				$("<?php echo '#'.$fun->Enlink('tiprod'); ?>").html(data);
			},
			error : function(){
				alert('Terjadi kesalahan dalam jaringan!\nGagal Link Kartu!\nSilahkan cek koneksi jaringan dan coba lagi!');
			    return false;
			}
		});

		$.ajax({
			url : "process/ajax/ajax_category_unit.php",
			type : 'POST',
			data: { 
			    id: category
			},
			success : function(data) {
				//$("<?php echo '#'.$fun->Enlink('prodit'); ?> option[value="+data+"]").prop('selected', true);
				$("<?php echo '#'.$fun->Enlink('prodit'); ?>").html(data);
			},
			error : function(){
				alert('Terjadi kesalahan dalam jaringan!\nGagal Link Kartu!\nSilahkan cek koneksi jaringan dan coba lagi!');
			    return false;
			}
		});
	});
</script>
