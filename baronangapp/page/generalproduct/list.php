<?php
$locid = '7101201000000000';

$sloc = "SELECT a.locationname as locationname, b.nama_prov as nama_prov, c.nama_kabkot as nama_kabkot
		from 21masterlocation as a 
		inner join 11amasterprovince as b 
			on a.locationprovince = b.id_prov
		inner join 11bmastercity as c
			on a.locationcity = c.id_kabkot
		where a.masterlocationid='".$locid."'";
$dloc = $fun->getData($sloc);
?>

	<div class="page-content">

		<div class="page-title page-title-small">
	        <h2 id="title"><a href="?" data-back-button><i class="fa fa-arrow-left"></i></a>General Product</h2>
	        <a href="#" data-menu="menu-main" class="bg-fade-gray1-dark shadow-xl preload-img" data-src="images/avatars/5s.png"></a>
	    </div>

	    <div class="card header-card shape-rounded" data-card-height="150">
	    	<div class="card-overlay bg-highlight opacity-95"></div>
	    	<div class="card-overlay dark-mode-tint"></div>
	    	<div class="card-bg preload-img" data-src="images/pictures/20s.jpg"></div>
		</div>
		
		<div class="card card-style">
            <div class="content">
                <div class="list-group list-custom-small">
					<?php
					$s1 = "SELECT * from 22amasterproduct where active='1' and productprice > 0 and locationid='".$locid."' order by productdescription ASC";
					$d1 = $fun->getData($s1, 2);
					foreach ($d1 as $d1) { ?>
						<a href="#" data-menu="<?php echo $fun->Enlink($d1['masterproductid']); ?>">
							<?php echo $d1['additionaldesc'].'-'.$d1['productdescription']; ?>
						</a>
						<?php
					}
					?>
				</div>
			</div>
		</div>

		<div class="center mt-3 mb-4 ">
	    	<a href="<?php echo '?pg=generalproduct&sb=add&'.$fun->setIDParam('lokasi', $locid); ?>" class="btn btn-m btn-center-xl bg-highlight txt-white rounded-sm text-uppercase font-900"><span class="ion-android-add font-21"></span></a>
	    </div>

	    <div class="divider mt-4 mb-3"></div>

		<!-- footer and footer card-->
    	<div class="footer" data-menu-load="footer.php"></div>  
	</div>

<?php
$sm1 = "SELECT 	a.masterproductid as masterproductid,
				a.additionaldesc as additionaldesc,
				a.productdescription as productdescription, 
				a.vehicletype as vehicletype,
				a.validity_days as validity_days,
				a.productprice as productprice,
				a.unit as unit,
				b.locationaddress as locationaddress,
				c.nama_kabkot as nama_kabkot,
				d.nama_prov as nama_prov
		from 22amasterproduct as a 
		inner join 21masterlocation as b 
			on a.locationid = b.masterlocationid 
		inner join 11amasterprovince as d 
			on b.locationprovince = d.id_prov  
		inner join 11bmastercity as c 
			on b.locationcity = c.id_kabkot
		where a.active='1' and a.locationid='".$locid."'";
$dm1 = $fun->getData($sm1, 2);
foreach ($dm1 as $d1) { ?>
	<div id="<?php echo $fun->Enlink($d1['masterproductid']); ?>" 
    	 class="menu menu-box-modal rounded-m" 
         data-menu-height="350" 
         data-menu-width="300" 
         data-menu-effect="menu-over">

         <h1 class="font-700 mt-3" style="margin-left: 15px; margin-right: 15px;"><?php echo $d1['additionaldesc'].'-'.$d1['productdescription']; ?></h1>

		<p class="boxed-text-xl justify mt-3" style="margin-left: 15px; margin-right: 15px;">
			<span class="bold">Type :</span> <span><?php echo $d1['vehicletype']; ?></span><br/>
			<span class="bold">Quantity :</span> <span><?php echo $fun->FormatNumber($d1['validity_days']).'&nbsp;'.$d1['unit']; ?></span><br/>
			<span class="bold">Price : Rp.</span> <span><?php echo $fun->FormatRupiah($d1['productprice']); ?></span>
		</p>

		<a href="<?php echo '?pg=generalproduct&sb=edit&'.$fun->setIDParam('ID', $d1['masterproductid']).'&'.$fun->setIDParam('lokasi', $locid); ?>" class="btn btn-m btn-center-xl bg-highlight txt-white rounded-sm text-uppercase font-900 center">
			<i class="ion-android-create font-21"></i>
		</a>
	</div>
	<?php
}
?>
