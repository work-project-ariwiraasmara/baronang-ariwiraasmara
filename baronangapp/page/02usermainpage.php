<?php
$session = $fun->getValookie('session');

if($session == 'member') {
    $smember = "SELECT * from 22bmastermember where mastermemberid='".$fun->getValookie('id')."'";
    $name = "membername";
}
else {
    $smember = "SELECT * from 23dmastertenant where mastertenantid='".$fun->getValookie('id')."'";
    $name = "tenantname";
}
$dmember = $fun->getData($smember, 1);

$sloc = "SELECT * from 21masterlocation where masterlocationid='".$dmember[0]['locationid']."'";
$rloc = $fun->checkRowData($sloc);

if($rloc > 0) { 
    $dloc = $fun->getData($sloc, 1);
    $locname = $dloc[0]['locationname'];
}
else { 
    $locname = 'null';
}

// KARTU BIRU 7000
$scard7000 = "SELECT * from 31bmastercarduser where cardid like '7000%' and  useridactivated='".$fun->getValookie('id')."' and activationstatus = '1' limit 1";
$qcard7000 = mysqli_query($fun->getConnection(), $scard7000) or die(mysqli_error($fun->getConnection()));
$rcard7000 = mysqli_num_rows($qcard7000);

if($rcard7000 > 0) { 
    $dcard7000 = $fun->getData($scard7000, 1);
    $cardid = $dcard7000[0]['cardid'];
    $spoint = "SELECT points from 23amastercard where mastercardid='".$cardid."'";
    $dpoint = $fun->getData($spoint, 1);
}
else { 
    $cardid = 0;
}

$scard7100 = "SELECT * from 23amastercard where memberid='".$fun->getValookie('id')."' and cardtype='SkyCard' and status = '1'";
$dcard7100 = $fun->getData($scard7100);
?>

    <div class="page-content" style="min-height:60vh!important">
        <div class="page-title page-title-large">
            <h1 data-username="<?php echo $fun->getValookie('name'); ?>" class="greeting-text txt-white" id="title"></h1>
            <a href="#" data-menu="menu-main" class="bg-fade-gray1-dark shadow-xl preload-img" data-src="images/avatars/5s.png"></a>
        </div>

        <div class="card header-card shape-rounded" data-card-height="150">
            <div class="card-overlay bg-highlight opacity-95"></div>
            <div class="card-overlay dark-mode-tint"></div>
            <div class="card-bg preload-img" data-src="images/pictures/20s.jpg"></div>
        </div>
            
                <div class="single-slider-boxed owl-carousel owl-no-dots">
                    <?php //KARTU 7000 [BIRU] ?>
                    <a href="#" data-menu="<?php echo $fun->Enlink('card1-'.$cardid); ?>" class="card rounded-m shadow-l">
                        <div class="card_container txt-white">
                            <div class="card_company">
                                <?php 
                                $scomp = "SELECT companyname from 12mastercompany where companyid='70' and status = '1'";
                                $dcomp = $fun->getData($scomp, 1);

                                echo strtoupper($dcomp[0]['companyname']); ?>
                            </div>

                            <img src="images/1.png" id="imgcard" style="width: 100%;">

                            <div class="card_identity">
                                <?php
                                echo $dmember[0][$name].'<br>';
                                echo $locname.'<br>';
                                if ($dpoint[0]['points'] > 0){
                                    echo $cardid.'<br>';
                                    echo $dpoint[0]['points'].' Points';
                                }
                                else {
                                    echo $cardid;
                                }
                                ?>
                            </div>

                            <div class="card_logo right">
                                <img src="images/icon/Logo-whitefish-icon.png" style="width: 40px !important; height: 20px !important;">
                            </div>
                        </div>
                    </a>
					
					 <?php
                    // KARTU 7100 [COKLAT]
                    $scard1 = "SELECT * from 31bmastercarduser where cardid like '7100%' and useridactivated='".$fun->getValookie('id')."' and activationstatus = '1'";
                    $dcard1 = $fun->getData($scard1, 2);
                    foreach ($dcard1 as $d) {

                        $s2 = "SELECT cardtype from 23amastercard where mastercardid='".$d['cardid']."'";
                        $d2 = $fun->getData($s2, 1);

                        $s3 = "SELECT * from 21masterlocation where masterlocationid='".substr($d['cardid'],0,9).'0000000'."'";
                        $d3 = $fun->getData($s3, 1);
                            
                        $s4 = "SELECT points from 23amastercard where mastercardid='".$d['cardid']."'";
                        $d4 = $fun->getData($s4, 1);
                            
                        $s5 = "SELECT companyname from 12mastercompany where companyid='71' and status = '1'";
                        $d5 = $fun->getData($s5, 1);
                        ?>
                        <a href="#" data-menu="<?php echo $fun->Enlink('card2-'.$d['cardid']); ?>" class="card rounded-m shadow-l">
                            <div class="card_container txt-white">
                                <div class="card_sky">
                                    <img src="images/logo skyparking 1.png" style="width: 45px !important; height: 45px !important;">
                                </div>

                                <div class="card_company">
                                    <?php echo strtoupper($d5[0]['companyname']); ?>
                                </div>

                                <img src="images/2.png" id="imgcard" style="width: 100%;">

                                <div class="card_identity">
                                    <?php
                                    echo $dmember[0][$name].'<br>';
                                    echo $d2[0]['cardtype'].'<br>';
                                    if ($d4[0]['points'] > 0){
                                        echo $d['cardid'].'<br>';
                                        echo $fun->FormatNumber($d4[0]['points']).' Points';
                                    }
                                    else {
                                        echo $d['cardid'];
                                    }
                                    ?>
                                </div>
                                    
                                <div class="card_logo">
                                    <img src="images/icon/Logo-whitefish-icon.png" style="width: 40px !important; height: 20px !important;">
                                </div>
                            </div>
                        </a>   
                        <?php
                    }
                    ?>
                </div>

        <div class="single-slider-boxed owl-carousel owl-no-dots">
            <?php
            // KARTU 7100 [MERAH] Card Product Owned 
            $scard2 = "SELECT * from 31bmastercarduser where cardid like '7100%' and useridactivated='".$fun->getValookie('id')."' and activationstatus = '1'";
            $dcard2 = $fun->getData($scard2, 2);
            foreach ($dcard2 as $d2) {

                $scard3 = "SELECT * from 31amastercardproductowned where cardid ='".$d2['cardid']."' and active = '1'";
                $dcard3 = $fun->getData($scard3, 2);
                foreach ($dcard3 as $d3) {
                            
                    $expdate = new datetime($d3['expirationdate']);
                    $svhl = "SELECT b.vehicletype as vehicle, b.productimage as productimage,
                                    b.locationid as locationid 
                                from 31amastercardproductowned as a 
                                inner join 22amasterproduct as b 
                                    on a.productowned = b.masterproductid
                                where a.serialnumber='".$d3['serialnumber']."'";
                    $dvhl = $fun->getData($svhl, 1);

                    $locid = $dvhl[0]['locationid'];
                    $sloc = "SELECT * from 21masterlocation where masterlocationid = '".$locid."'";
                    $dloc = $fun->getData($sloc, 1);
                    ?>
                    <a href="#" data-menu="<?php echo $fun->Enlink('card5-'.$d3['serialnumber']); ?>" class="card rounded-m shadow-l">
                        <div class="card_container txt-white">
                            <div class="card_sky">
                                <img src="images/logo skyparking 1.png" style="width: 45px !important; height: 45px !important;">
                            </div>

                            <div class="card_company">
                                <?php
                                if($dloc[0]['image'] == '' || empty($dloc[0]['image'])) {
                                    echo strtoupper($dloc[0]['locationname']);
                                }
                                else { ?>
                                    <img src="<?php echo 'images/location/'.$dloc[0]['image']; ?>" style="width: 45px !important; height: 45px !important;">
                                    <?php
                                }
                                ?>
                            </div>

                            <img src="images/5.png" id="imgcard" style="width: 100%;">
                                    
                            <div class="card_identity">
                                <?php
                                echo $dmember[0][$name].'<br>';
                                echo $d3['serialnumber'].'<br>';
                                echo date_format($expdate,'d/m/Y');
                                ?>
                            </div>

                            <div class="card_logo">
                                <?php
                                if($dvhl[0]['productimage'] == '' || empty($dvhl[0]['productimage'])) {?>
                                    <div class="bold mb-3"><?php echo $dvhl[0]['vehicle']; ?></div>
                                    <?php
                                }
                                else { ?>
                                    <div style="margin-bottom: 10px;">
                                        <img src="<?php echo 'images/icon/'.$dvhl[0]['productimage']; ?>" style="width: 40px !important; height: 25px !important;">
                                    </div>
                                    <?php
                                }
                                ?>

                                <img src="images/icon/Logo-whitefish-icon.png" style="width: 40px !important; height: 20px !important;">
                            </div>
                        </div>
                    </a>
                    <?php
                }

            }
            ?>
        </div>

        <div class="single-slider-boxed owl-carousel owl-no-dots">
            <?php // KARTU 7101 [HIJAU] 
            $card4 = "SELECT * from 31bmastercarduser where cardid like '7101%' and useridactivated='".$fun->getValookie('id')."' and activationstatus = '1'";
            $rcard4 = $fun->checkRowData($card4);
            if($rcard4 > 0) {
                $dcard4 = $fun->getData($card4, 2);
                foreach ($dcard4 as $dcard4) {
                    $s2 = "SELECT * from 31bmastercarduser where activationstatus='1' and cardid='".$dcard4['cardid']."'";
                    $r2 = $fun->checkRowData($s2);
                    if($r2 > 0) {
                        $d2 = $fun->getData($s2);

                        $s3 = "SELECT * from 22bmastermember where mastermemberid='".$d2[0]['useridactivated']."'";
                        $d3 = $fun->getData($s3);

                        $tenantname = $d3[0]['membername'];
                    }
                    else {
                        $tenantname = $dcard4['tenantname'];
                    }

                    $svhl = "SELECT b.vehicletype as vehicle, b.productimage as productimage
                                from 31amastercardproductowned as a 
                                inner join 22amasterproduct as b 
                                    on a.productowned = b.masterproductid
                                where a.cardid='".$dcard4['cardid']."'";
                    $dvhl = $fun->getData($svhl);

                    $locid = substr($dcard4['cardid'],0,9).'0000000';
                    $sloc = "SELECT * from 21masterlocation where masterlocationid = '".$locid."'";
                    $dloc = $fun->getData($sloc);
                    ?>
                    <a href="#" data-menu="<?php echo $fun->Enlink('card3-'.$dcard4['cardid']); ?>" class="card rounded-m shadow-l">
                        <div class="card_container txt-white">
                            <div class="card_sky">
                                <img src="images/logo skyparking 1.png" style="width: 45px !important; height: 45px !important;">
                            </div>

                            <div class="card_company">
                                <?php
                                if($dloc[0]['image'] == '' || empty($dloc[0]['image'])) {
                                    echo $dloc[0]['locationname'];
                                }
                                else { ?>
                                    <img src="<?php echo 'images/location/'.$dloc[0]['image']; ?>" style="width: 20px !important; height: 20px !important;">
                                    <?php
                                }
                                ?>
                            </div>

                            <img src="images/3.png" id="imgcard" style="width: 100% !important;">

                            <div class="card_identity" style="font-size: 9px;">
                                <?php
                                echo $tenantname.'<br>';
                                echo $dcard4['cardid'];
                                ?>
                            </div>

                            <div class="card_logo" style="margin-bottom: 1px;">
                                <?php
                                if($dvhl[0]['productimage'] == '' || empty($dvhl[0]['productimage'])) {?>
                                    <div style="margin-bottom: 10px;"><?php echo $dvhl[0]['vehicle']; ?></div>
                                    <?php
                                }
                                else { ?>
                                    <div style="margin-bottom: 10px;">
                                        <img src="<?php echo 'images/icon/'.$dvhl[0]['productimage']; ?>" style="width: 20px !important; height: 10px !important;">
                                    </div>
                                    <?php
                                }
                                ?>

                                <img src="images/icon/Logo-whitefish-icon.png" style="width: 20px !important; height: 10px !important;">
                            </div>
                        </div>
                    </a>
                    <?php
                }
            }
            ?>
        </div>

        <div class="divider divider-margins"></div>

        <div class="content mb-2">
            <h5 class="float-left font-16 font-500 bold">Locations</h5>
            <a class="float-right font-12 color-highlight mt-n1" href="?pg=location">View All</a>
            <div class="clearfix"></div>
        </div>

        <div class="double-slider text-center owl-carousel owl-no-dots mb-4">
            <?php
            $sloc = "SELECT * from 21masterlocation where locationname <> 'Personal' and locationname <> 'Sky Parking'";
            $dloc = $fun->getData($sloc, 2);
            foreach ($dloc as $k) { 
                if($k['image'] == '' || empty($k['image'])) {
                    $img = "background-image: url('images/bg1.jpg'), linear-gradient(#0a3177, #081055);";
                }
                else {
                    $img = "background-image: url('images/company/".$k['image']."'), linear-gradient(#0a3177, #081055)";
                }
                // <a href="#" data-menu="<?php echo $fun->Enlink('mlok-'.$k['masterlocationid']); " class="item">
                ?>
                <a href="<?php echo '?pg=card&sb=product&'.$fun->setIDParam('cardid', $dcard7100[0]['mastercardid']).'&'.$fun->setIDParam('lokasi', $k['masterlocationid']); ?>">
                    <div data-card-height="100" class="card rounded-m shadow-l">
						<div class="card-center text-center mb-4">
							<h5 class="font-16 mb-9"><?php echo $k['locationname']; ?></h5>
                        </div>
                    </div>
                </a>
                <?php
            }
            ?>
        </div>    

        <div class="divider divider-margins"></div>

        <!-- footer and footer card-->
        <div class="footer" data-menu-load="footer.php"></div>  
    </div>

    <?php
    require('page/02usermainpage_card_modal.php');

    foreach ($dloc as $k) { 
        $s2 = "SELECT a.nama_prov as nama_prov, b.nama_kabkot as nama_kabkot
                from 11amasterprovince as a 
                inner join 11bmastercity as b 
                    on a.id_prov = b.id_prov 
                where a.id_prov='".$k['locationprovince']."' and b.id_kabkot='".$k['locationcity']."'";
        $d2 = $fun->getData($s2);
        ?>
        <div id="<?php echo $fun->Enlink('mlok-'.$k['masterlocationid']); ?>" 
             class="menu menu-box-modal menu-box-detached rounded-m" 
             data-menu-height="430" 
             data-menu-width="300" 
             data-menu-effect="menu-over">

            <h1 class="text-center font-700 mt-3 pt-2"><?php echo $k['locationname']; ?></h1>
            <p class="boxed-text-xl justify mt-3">
                <span class="bold">Address : </span><br/>
                <span><?php echo $k['locationaddress'].', '.$d2[0]['nama_kabkot'].''.$d2[0]['nama_prov']; ?></span><br/>
                <br/>
                <span class="bold">Phone Number : </span>
                <span><?php echo $k['locationphone']; ?></span>
            </p>

            <div class="divider divider-margins"></div>

            <div style="margin-top: -20px; margin-bottom: 10px;">
                <a href="<?php echo '?pg=location&sb=edit&'.$fun->setIDParam('ID', $k['masterlocationid']) ?>" class="btn btn-m btn-center-xl bg-highlight txt-white rounded-sm text-uppercase font-900">
                    <i class="ion-android-create font-15"></i>
                </a>
            </div>

            <div class="divider divider-margins"></div>

            <div class="list-group list-custom-small pl-1 pr-3" style="margin-top: -30px;">

                <a href="<?php echo '?pg=product&'.$fun->setIDParam('lokasi', $k['masterlocationid']); ?>" class="pl-3">
                    <span class="ion-card"></span>
                    <span class="font-13 ml-3">Product</span>
                    <i class="fa fa-angle-right"></i>
                </a>

                <a href="<?php echo '?pg=compliment&'.$fun->setIDParam('lokasi', $k['masterlocationid']); ?>" class="pl-3">
                    <span class="ion-card"></span>
                    <span class="font-13 ml-3">Complimentary Product</span>
                    <i class="fa fa-angle-right"></i>
                </a>

                <a href="<?php echo '?pg=tenant&'.$fun->setIDParam('lokasi', $k['masterlocationid']); ?>" class="pl-3">
                    <span class="ion-android-people"></span>
                    <span class="font-13 ml-3">Tenant</span>
                    <i class="fa fa-angle-right"></i>
                </a>

            </div>
        </div>
        <?php
    }
    ?>

    
