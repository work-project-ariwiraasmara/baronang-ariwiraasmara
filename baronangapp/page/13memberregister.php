
   <div class="page-content">
        
        <div class="page-title page-title-small">
			<h1 class="txt-white center" id="title">Register</h1>
		</div>
        <div class="card header-card shape-rounded" data-card-height="150">
            <div class="card-overlay bg-highlight opacity-95"></div>
            <div class="card-overlay dark-mode-tint"></div>
            <div class="card-bg preload-img" data-src="images/pictures/20s.jpg"></div>
        </div>
        
        <div class="card card-style">
            <div class="content mb-0 mt-1">
				
                <form action="process/13asendconfirmationemail.php" method="post">
                    <p class="color-highlight font-12 text-center ">Create an Account. It's 100% free!</p>
                    
                    <div class="input-style has-icon input-style-1 input-required">
                        <i class="input-icon fa fa-at color-theme"></i>
                        <span>Email</span>
                        <em>(required)</em>
                        <input type="email" name="<?php echo $fun->Enlink('email'); ?>" id="<?php echo $fun->Enlink('email'); ?>" placeholder="Email">
                    </div> 

                    <div class="input-style has-icon input-style-1 input-required">
                        <i class="input-icon fa fa-lock color-theme"></i>
                        <span>Password</span>
                        <em>(required)</em>
                        <input type="password" name="<?php echo $fun->Enlink('pass'); ?>" id="<?php echo $fun->Enlink('pass'); ?>" placeholder="Password">
                    </div> 

                    <button type="submit" name="ok" class="btn btn-m mt-4 mb-3 btn-full rounded-sm bg-highlight text-uppercase font-900 width-100">Create account</ok>
                </form>

            </div>
        </div>

       <a href="?" class="btn btn-full btn-margins rounded-sm btn-m bg-green1-dark text-center font-900 text-uppercase">Already Registered? Sign in Here</a>

        <!-- footer and footer card-->
        <div class="footer" data-menu-load="footer.php"></div>  
    </div>    
    <!-- end of page content-->
  
