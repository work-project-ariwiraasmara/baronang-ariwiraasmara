<?php
$sf1 = "SELECT * from 22amasterproduct where masterproductid='".$fun->getIDParam('ID')."'";
$df1 = $fun->getData($qf1);

$s1 = "SELECT * from 21masterlocation where masterlocationid='".$fun->getIDParam('lokasi')."'";
$d1 = $fun->getData($s1);
?>

	<div class="page-content">

		<div class="page-title page-title-small">
	        <h2 id="title"><a href="?" data-back-button><i class="fa fa-arrow-left"></i></a>Edit Complimentary Product</h2>
	        <a href="#" data-menu="menu-main" class="bg-fade-gray1-dark shadow-xl preload-img" data-src="images/avatars/5s.png"></a>
	    </div>

	    <div class="card header-card shape-rounded" data-card-height="150">
	    	<div class="card-overlay bg-highlight opacity-95"></div>
	    	<div class="card-overlay dark-mode-tint"></div>
	    	<div class="card-bg preload-img" data-src="images/pictures/20s.jpg"></div>
		</div>

		<form action="<?php echo 'process/42bupdateproduct.php?'.$fun->setIDParam('ID', $fun->getIDParam('ID')); ?>" method="post">

			<div class="card card-style">
		    	<p class="content txt-black">
		    		<span class="bold"><?php echo 'Location : '.$d1[0]['locationname']; ?></span><br/>
		    		<input type="text" name="<?php echo $fun->Enlink('lokasi'); ?>" id="<?php echo $fun->Enlink('lokasi'); ?>" value="<?php echo $fun->getIDParam('lokasi'); ?>" class="hide" required>
		    	</p>
		    </div>

		    <div class="card card-style">
            	<div class="content mb-0">

					<div class="input-style input-style-1 input-required">
						<span>Quantity</span>
						<em>(required)</em>
						<input type="number" name="<?php echo $fun->Enlink('valid'); ?>" id="<?php echo $fun->Enlink('valid'); ?>" value="<?php echo $df1[0]['validity_days'] ?>" placeholder="Quantity" required>
					</div>

					<button type="submit" name="confirm" id="confirm" class="btn btn-m btn-center-xl bg-highlight txt-white rounded-sm text-uppercase font-900 widt-100 mt-3 mb-3">OK</button>
				</div>
			</div>
		</form>

		<div class="divider divider-margins"></div>

	    <!-- footer and footer card-->
        <div class="footer" data-menu-load="footer.php"></div>  
	</div>