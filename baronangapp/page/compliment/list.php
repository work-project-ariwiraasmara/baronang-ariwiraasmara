<?php
$sloc = "SELECT a.locationname as locationname, a.locationaddress as locationaddress, b.nama_prov as nama_prov, c.nama_kabkot as nama_kabkot
		from 21masterlocation as a 
		inner join 11amasterprovince as b 
			on a.locationprovince = b.id_prov
		inner join 11bmastercity as c
			on a.locationcity = c.id_kabkot
		where a.masterlocationid='".$fun->getIDParam('lokasi')."'";
//echo $sloc;
$dloc = $fun->getData($sloc);
?>

	<div class="page-content">

		<div class="page-title page-title-small">
	        <h2 id="title"><a href="?" data-back-button><i class="fa fa-arrow-left"></i></a>Complimentary Product</h2>
	        <a href="#" data-menu="menu-main" class="bg-fade-gray1-dark shadow-xl preload-img" data-src="images/avatars/5s.png"></a>
	    </div>

	    <div class="card header-card shape-rounded" data-card-height="150">
	    	<div class="card-overlay bg-highlight opacity-95"></div>
	    	<div class="card-overlay dark-mode-tint"></div>
	    	<div class="card-bg preload-img" data-src="images/pictures/20s.jpg"></div>
		</div>

		<div class="card card-style">
	    	<p class="content txt-black">
	    		<span class="bold"><?php echo 'Location : '.$dloc[0]['locationname']; ?></span><br>
	    		<span><?php echo $dloc[0]['locationaddress'].', '.$dloc[0]['nama_kabkot'].', '.$dloc[0]['nama_prov']; ?></span>
	    	</p>
	    </div>

		<div class="card card-style">
            <div class="content">
                <div class="list-group list-custom-small">
					<?php
					$sprod = "SELECT (substring(masterproductid, 12, 1)) as id, 
									 masterproductid, 
									 locationid, 
									 productdescription, 
									 additionaldesc 
								from 22amasterproduct 
								where active = 1 and locationid = '".$fun->getIDParam('lokasi')."' 
									having id = 0 order by productdescription ASC"; 
					$dprod = $fun->getData($sprod, 2);
					foreach ($dprod as $dt) { ?>
						<a href="#" data-menu="<?php echo $fun->Enlink('mprod-'.$dt['masterproductid']); ?>">
							<span><?php echo $dt['additionaldesc'].'-'.$dt['productdescription']; ?></span>
						</a>
						<?php
					}
					?>
				</div>
			</div>
		</div>

		<div class="center mt-3 mb-4 ">
	    	<a href="<?php echo '?pg=compliment&sb=add&'.$fun->setIDParam('lokasi', $fun->getIDParam('lokasi')); ?>" class="btn btn-m btn-center-xl bg-highlight txt-white rounded-sm text-uppercase font-900"><span class="ion-android-add font-21"></span></a>
	    </div>

	    <div class="divider mt-4 mb-3"></div>

		<!-- footer and footer card-->
    	<div class="footer" data-menu-load="footer.php"></div>  
	</div>

<?php
$smprod = 	"SELECT substring(a.masterproductid, 12, 1) as id,
					a.masterproductid as masterproductid,
					a.productdescription as productdescription, 
					a.vehicletype as vehicletype,
					a.validity_days as validity_days,
					a.productprice as productprice,
					a.unit as unit,
					b.locationaddress as locationaddress,
					c.nama_kabkot as nama_kabkot,
					d.nama_prov as nama_prov
			from 22amasterproduct as a 
			inner join 21masterlocation as b 
				on a.locationid = b.masterlocationid 
			inner join 11amasterprovince as d 
				on b.locationprovince = d.id_prov  
			inner join 11bmastercity as c 
				on b.locationcity = c.id_kabkot
			where a.active='1' and a.locationid='".$fun->getIDParam('lokasi')."' having id = 0";
$dmprod = $fun->getData($smprod, 2);
foreach($dmprod as $dt) { ?>
	<div id="<?php echo $fun->Enlink('mprod-'.$dt['masterproductid']); ?>" 
    	 class="menu menu-box-modal menu-box-detached rounded-m" 
         data-menu-height="320" 
         data-menu-width="280" 
         data-menu-effect="menu-over">

        <h1 class="font-700 mt-3 pt-2 ml-3"><?php echo $dt['productdescription']; ?></h1>

        <p class="boxed-text-xl justify ml-3">
            <span class="bold">Type :</span> <span><?php echo $dt['vehicletype'] ; ?></span><br/>
            <span class="bold">Quantity :</span> <span><?php echo $dt['validity_days'].' '.$dt['unit']; ?></span><br/>
            <span class="bold">Price : Rp.</span> <span><?php echo $fun->FormatNumber($dt['productprice']); ?></span>
        </p>

        <div class="divider divider-margins"></div>

        <a href="<?php echo '?pg=product&sb=edit&'.$fun->setIDParam('ID', $d1['masterproductid']).'&'.$fun->setIDParam('lokasi', $fun->getIDParam('lokasi')); ?>" class="btn btn-m btn-center-xl bg-highlight txt-white rounded-sm text-uppercase font-900 center">
        	<i class="ion-android-create font-18"></i>
        </a>

    </div>
	<?php
}
?>