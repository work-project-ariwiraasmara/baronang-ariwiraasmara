	<div class="page-content" style="min-height:60vh!important">
        <div class="page-title page-title-large">
            <h1 data-username="<?php echo $fun->getValookie('name'); ?>" class="greeting-text txt-white" id="title"></h1>
            <a href="#" data-menu="menu-main" class="bg-fade-gray1-dark shadow-xl preload-img" data-src="images/avatars/5s.png"></a>
        </div>

        <div class="card header-card shape-rounded" data-card-height="95">
            <div class="card-overlay bg-highlight opacity-95"></div>
            <div class="card-overlay dark-mode-tint"></div>
            <div class="card-bg preload-img" data-src="images/pictures/20s.jpg"></div>
        </div>
    
	    <?php 
	    $s1 = "SELECT distinct 	a.masterlocationid as masterlocationid,
								a.locationname as locationname
			from 21masterlocation as a 
			inner join 23dmastertenant as b
				on a.masterlocationid = b.locationid
			where b.memberid='".$fun->getValookie('id')."'";
	    $d1 = $fun->getData($s1, 2);   
		foreach($d1 as $d1) { 
			$tenantname = '';
			?>
			
			<div class="content mb-2">
	            <h5 class="float-left font-16 font-500 bold"><?php echo $d1['locationname']; ?></h5>
	            <div class="clearfix"></div>
	        </div>
	        
	        <div class="single-slider-boxed owl-carousel owl-no-dots">
				<?php
				$tohidecard1 = 0;
				// KARTU 7100
				$scard1 = "SELECT 	a.mastertenantid as mastertenantid,
									a.tenantname as tenantname,
									b.mastercardid as mastercardid
							from 23dmastertenant as a 
							inner join 23amastercard as b 
								on a.mastertenantid = b.memberid
							where b.mastercardid like '7100%' and a.memberid='".$fun->getValookie('id')."' and a.locationid='".$d1['masterlocationid']."'";
				//echo $s2.'<br><br>';
				$rcard1 = $fun->checkRowData($scard1);
				if($rcard1 > 0) { 
					$tohidecard1 = 1;
					$dcard1 = $fun->getData($scard1, 2);
					foreach($dcard1 as $dcard1) { 

						$s2 = "SELECT * from 31bmastercarduser where activationstatus='1' and cardid='".$dcard1['mastercardid']."'";
						$r2 = $fun->checkRowData($s2, 2);
						if($r2 > 0) {
							$d2 = $fun->getData($s2);

							$s3 = "SELECT membername from 22bmastermember where mastermemberid='".$d2[0]['useridactivated']."'";
							$d3 = $fun->getData($s3);

							$tenantname = $d3[0]['membername'];
						}
						else {
							$tenantname = $dcard1['tenantname'];
						}

						$svhl = "SELECT b.vehicletype as vehicle, b.productimage as productimage
								from 31amastercardproductowned as a 
								inner join 22amasterproduct as b 
									on a.productowned = b.masterproductid
								where a.cardid='".$dcard1['mastercardid']."'";
						$dvhl = $fun->getData($qvhl);

						$locid = substr($dcard1['mastercardid'],0,9).'0000000';
						$sloc = "SELECT * from 21masterlocation where masterlocationid = '".$locid."'";
						$dloc = $fun->getData($sloc);
						?>
						<div class="card rounded-m shadow-l">
							<a href="#" data-menu="<?php echo $fun->Enlink($dcard1['mastercardid']); ?>">
								<div class="card_container txt-white">
	                                <div class="card_sky">
	                                    <img src="images/logo skyparking 1.png" style="width: 45px !important; height: 45px !important;">
	                                </div>

	                                <div class="card_company">
										<?php
										if($dloc[0]['image'] == '' || empty($dloc[0]['image'])) {
											echo $dloc[0]['locationname'];
										}
										else { ?>
											<img src="<?php echo 'images/location/'.$dloc[0]['image']; ?>" style="width: 20px !important; height: 20px !important;">
											<?php
										}
										?>
									</div>

									<img src="images/2.png" id="imgcard" style="width: 100% !important;">

									<div class="card_identity_tenant" style="font-size: 9px;">
										<?php
										echo $tenantname.'<br>';
										echo $dcard1['mastercardid'];
										?>
									</div>

									<div class="card_logo" style="margin-bottom: 1px;">
										<?php
										if($dvhl[0]['productimage'] == '' || empty($dvhl[0]['productimage'])) {?>
											<div style="margin-bottom: 10px;"><?php echo $dvhl['vehicle']; ?></div>
											<?php
										}
										else { ?>
											<div style="margin-bottom: 10px;">
												<img src="<?php echo 'images/icon/'.$dvhl[0]['productimage']; ?>" style="width: 20px !important; height: 15px !important;">
											</div>
											<?php
										}
										?>

										<img src="images/icon/Logo-whitefish-icon.png" style="width: 20px !important; height: 10px !important;">
									</div>
								</div>
							</a>
						</div>
						<?php
					}
				}
				else {
					$tohidecard1 = 0;
				}

				// KARTU 7101
				$tohidecard2 = 0;
				$scard2 = "SELECT 	a.mastertenantid as mastertenantid,
									a.tenantname as tenantname,
									b.mastercardid as mastercardid
							from 23dmastertenant as a 
							inner join 23amastercard as b 
								on a.mastertenantid = b.memberid
							where b.mastercardid like '7101%' and a.memberid='".$fun->getValookie('id')."' and a.locationid='".$d1['masterlocationid']."'";
				//echo $s2.'<br><br>';
				$rcard2 = $fun->getData($scard2, 2);
				if($rcard2 > 0) {
					$tohidecard2 = 1;
					$dcard2 = $fun->getData($scard2, 2);
					foreach($dcard2 as $dcard2) { 

						$s2 = "SELECT * from 31bmastercarduser where activationstatus='1' and cardid='".$dcard2['mastercardid']."'";
						$r2 = $fun->checkRowData($s2);
						if($r2 > 0) {
							$d2 = $fun->getData($s2);

							$s3 = "SELECT * from 22bmastermember where mastermemberid='".$d2[0]['useridactivated']."'";
							$d3 = $fun->getData($s3);

							$tenantname = $d3[0]['membername'];
						}
						else {
							$tenantname = $dcard2['tenantname'];
						}

						$svhl = "SELECT b.vehicletype as vehicle, b.productimage as productimage
								from 31amastercardproductowned as a 
								inner join 22amasterproduct as b 
									on a.productowned = b.masterproductid
								where a.cardid='".$dcard2['mastercardid']."'";
						$dvhl = $fun->getData($svhl);

						$locid = substr($dcard2['mastercardid'],0,9).'0000000';
						$sloc = "SELECT * from 21masterlocation where masterlocationid = '".$locid."'";
						$dloc = $fun->getData($sloc);
						?>
						<div class="card rounded-m shadow-l">
							<a href="#" data-menu="<?php echo $fun->Enlink($dcard2['mastercardid']); ?>">
								<div class="card_container txt-white">
	                                <div class="card_sky">
	                                    <img src="images/logo skyparking 1.png" style="width: 45px !important; height: 45px !important;">
	                                </div>

	                                <div class="card_company">
										<?php
										if($dloc[0]['image'] == '' || empty($dloc[0]['image'])) {
											echo $dloc[0]['locationname'];
										}
										else { ?>
											<img src="<?php echo 'images/location/'.$dloc[0]['image']; ?>" style="width: 20px !important; height: 20px !important;">
											<?php
										}
										?>
									</div>

									<img src="images/3.png" id="imgcard" style="width: 100% !important;">

									<div class="card_identity" style="font-size: 9px;">
										<?php
										echo $tenantname.'<br>';
										echo $dcard2['mastercardid'];
										?>
									</div>

									<div class="card_logo" style="margin-bottom: 1px;">
										<?php
										if($dvhl[0]['productimage'] == '' || empty($dvhl[0]['productimage'])) {?>
											<div style="margin-bottom: 10px;"><?php echo $dvhl[0]['vehicle']; ?></div>
											<?php
										}
										else { ?>
											<div style="margin-bottom: 10px;">
												<img src="<?php echo 'images/icon/'.$dvhl[0]['productimage']; ?>" style="width: 20px !important; height: 10px !important;">
											</div>
											<?php
										}
										?>

										<img src="images/icon/Logo-whitefish-icon.png" style="width: 20px !important; height: 10px !important;">
									</div>
								</div>
							</a>
						</div>
						<?php
					}
				}
				else {
					$tohidecard2 = 0;
				}


				// KARTU 7104
				$tohidecard3 = 0;
				$scard3 = "SELECT 	a.mastertenantid as mastertenantid,
									a.tenantname as tenantname,
									b.mastercardid as mastercardid
							from 23dmastertenant as a 
							inner join 23amastercard as b 
								on a.mastertenantid = b.memberid
							where b.mastercardid like '7104%' and b.status = '1' and a.memberid='".$fun->getValookie('id')."' and a.locationid='".$d1['masterlocationid']."'";
				//echo $s2.'<br><br>';
				$rcard3 = $fun->checkRowData($scard3);
				if($rcard3 > 0) {
					$tohidecard3 = 1;
					$dcard3 = $fun->getData($scard3, 2);
					foreach($dcard3 as $dcard3) { 

						$s2 = "SELECT * from 31bmastercarduser where activationstatus='1' and cardid='".$dcard3['mastercardid']."'";
						$r2 = $fun->checkRowData($q2);
						if($r2 > 0) {
							$d2 = $fun->getData($q2);

							$s3 = "SELECT * from 22bmastermember where mastermemberid='".$d2[0]['useridactivated']."'";
							$d3 = $fun->getData($s3);

							$tenantname = $d3[0]['membername'];
						}
						else {
							$tenantname = $dcard3['tenantname'];
						}

						$svhl = "SELECT b.vehicletype as vehicle, b.productimage as productimage
								from 31amastercardproductowned as a 
								inner join 22amasterproduct as b 
									on a.productowned = b.masterproductid
								where a.cardid='".$dcard3['mastercardid']."'";
						$dvhl = $fun->getData($svhl, 2);

						$locid = '7101'.substr($dcard3['mastercardid'],4,5).'0000000';
						$sloc = "SELECT * from 21masterlocation where masterlocationid = '".$locid."'";
						$dloc = $fun->getData($sloc, 2);
						?>
						<div class="card rounded-m shadow-l">
							<a href="#" data-menu="<?php echo $fun->Enlink($dcard3['mastercardid']); ?>">
								<div class="card_container txt-white">
	                                <div class="card_sky">
	                                    <img src="images/logo skyparking 1.png" style="width: 45px !important; height: 45px !important;">
	                                </div>

	                                <div class="card_company">
										<?php
										if($dloc[0]['image'] == '' || empty($dloc[0]['image'])) {
											echo $dloc[0]['locationname'];
										}
										else { ?>
											<img src="<?php echo 'images/location/'.$dloc[0]['image']; ?>" style="width: 20px !important; height: 20px !important;">
											<?php
										}
										?>
									</div>

									<img src="images/4.png" id="imgcard" style="width: 100% !important;">

									<div class="card_identity" style="font-size: 14px;">
										<?php
										echo $tenantname.'<br>';
										echo $dcard3['mastercardid'];
										?>
									</div>

									<div class="card_logo" style="margin-bottom: 1px;">
										<?php
										if($dvhl[0]['productimage'] == '' || empty($dvhl[0]['productimage'])) {?>
											<div style="margin-bottom: 10px;"><?php echo $dvhl[0]['vehicle']; ?></div>
											<?php
										}
										else { ?>
											<div style="margin-bottom: 10px;">
												<img src="<?php echo 'images/icon/'.$dvhl[0]['productimage']; ?>" style="width: 20px !important; height: 15px !important;">
											</div>
											<?php
										}
										?>

										<img src="images/icon/Logo-whitefish-icon.png" style="width: 20px !important; height: 10px !important;">
									</div>
								</div>
							</a>
						</div>
						<?php
					}
				}
				else {
					$tohidecard3 == 0;
				}

				if( ($tohidecard1 == 0) && ($tohidecard2 == 0) && ($tohidecard3 == 0)) { ?>
					<script type="text/javascript">
						$("<?php echo '#'.$fun->Enlink('loc-'.$d1['locationname']) ?>").addClass("hide");
					</script>
					<?php
				} ?>
			</div>	
			<div class="divider divider-margins"></div>
			<?php
		}
		?>
	 
	    <!-- footer and footer card-->
        <div class="footer" data-menu-load="footer.php"></div>  
    </div> 

    <?php
    require('page/02ccardmanagement_card_modal.php');
    ?>