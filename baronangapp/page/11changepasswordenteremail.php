    <div class="page-content">
        
        <div class="page-title page-title-small">
			<h1 class="txt-white center" id="title">Forgot Password</h1>
		</div>

        <div class="card header-card shape-rounded" data-card-height="150">
            <div class="card-overlay bg-highlight opacity-95"></div>
            <div class="card-overlay dark-mode-tint"></div>
            <div class="card-bg preload-img" data-src="images/pictures/20s.jpg"></div>
        </div>
        
        <div class="card card-style">
            <div class="content mb-0">            

                <form action="process/11checkmemberemailregistered.php" method="post">
                    <h4>Recover your Account</h4>
                    <p>
                        Simply enter your email name which you registered your account under and we'll send you the password reset instructions.
                    </p>
                    
                    <div class="input-style has-icon input-style-1 input-required">
                        <i class="input-icon fa fa-at color-theme"></i>
                        <span>Email</span>
                        <em>(required)</em>
                        <input type="email" placeholder="Email">
                    </div> 

                    <button type="submit" class="btn btn-m mt-4 mb-3 btn-full rounded-sm bg-highlight text-uppercase font-900 width-100">Send Reset Instructions</button>        
                </form>

            </div>            
        </div>
        
        <a href="?" class="btn btn-full btn-margins rounded-sm btn-m bg-green1-dark text-center font-900 text-uppercase">Want to Sign In? Click here</a>

       
        <!-- footer and footer card-->
        <div class="footer" data-menu-load="footer.php"></div>  
    </div>    
    <!-- end of page content-->
    
