	<div class="page-content">

		<div class="page-title page-title-small">
	        <h2 id="title"><a href="?" data-back-button><i class="fa fa-arrow-left"></i></a>Add Company</h2>
	        <a href="#" data-menu="menu-main" class="bg-fade-gray1-dark shadow-xl preload-img" data-src="images/avatars/5s.png"></a>
	    </div>

	    <div class="card header-card shape-rounded" data-card-height="150">
	    	<div class="card-overlay bg-highlight opacity-95"></div>
	    	<div class="card-overlay dark-mode-tint"></div>
	    	<div class="card-bg preload-img" data-src="images/pictures/20s.jpg"></div>
		</div>

		<form action="process/05aaddcompany.php" method="post" enctype="multipart/form-data">
			<div class="card card-style">
            	<div class="content mb-0">
			
					<div class="input-style input-style-1 input-required">
						<span>Name</span>
						<em>(required)</em>
						<input type="text" name="<?php echo $fun->Enlink('nama'); ?>" id="<?php echo $fun->Enlink('nama'); ?>" placeholder="Name" required>
					</div>

					<div class="input-style input-style-1 input-required">
						<span>Email</span>
						<input type="text" name="<?php echo $fun->Enlink('email'); ?>" id="<?php echo $fun->Enlink('email'); ?>" placeholder="Email">
					</div>

					<div class="input-style input-style-1 input-required">
						<span>Phone Number</span>
						<input type="text" name="<?php echo $fun->Enlink('tlp'); ?>" id="<?php echo $fun->Enlink('tlp'); ?>" placeholder="Phone Number">
					</div>

					<div class="input-style input-style-1 input-required">
						<span>Label 1</span>
						<input type="text" name="<?php echo $fun->Enlink('label1'); ?>" id="<?php echo $fun->Enlink('label1'); ?>" placeholder="Label 1">
					</div>

					<div class="input-style input-style-1 input-required">
						<span>Label 2</span>
						<input type="text" name="<?php echo $fun->Enlink('label2'); ?>" id="<?php echo $fun->Enlink('label2'); ?>" placeholder="Label 2">
					</div>

					<div class="input-style input-style-1 input-required">
						<span>Label 3</span>
						<input type="text" name="<?php echo $fun->Enlink('label3'); ?>" id="<?php echo $fun->Enlink('label3'); ?>" placeholder="Label 3">
					</div>

					<div class="input-style input-style-1 input-required">
						<span>Label 4</span>
						<input type="text" name="<?php echo $fun->Enlink('label4'); ?>" id="<?php echo $fun->Enlink('label4'); ?>" placeholder="Label 4">
					</div>

					<div class="input-style input-style-1 input-required">
						<span>Label 5</span>
						<input type="text" name="<?php echo $fun->Enlink('label5'); ?>" id="<?php echo $fun->Enlink('label5'); ?>" placeholder="Label 5">
					</div>

					<div class="input-style input-style-1 input-required">
						<span>Location's Label</span>
						<input type="text" name="<?php echo $fun->Enlink('labellokasi'); ?>" id="<?php echo $fun->Enlink('labellokasi'); ?>" placeholder="Location's Label">
					</div>

					<div class="input-style input-style-1 input-required">
						<span>Tenant's Label</span>
						<em>(required)</em>
						<input type="text" name="<?php echo $fun->Enlink('labeltenant'); ?>" id="<?php echo $fun->Enlink('labeltenant'); ?>" placeholder="Tenant's Label">
					</div>

					<div class="input-style input-style-1 input-required">
						<span>Vehicle's Label</span>
						<input type="text" name="<?php echo $fun->Enlink('labelvehicle'); ?>" id="<?php echo $fun->Enlink('labelvehicle'); ?>" placeholder="Vehicle's Label">
					</div>

					<div class="">
						<span>Image</span>
						<input type="file" name="<?php echo $fun->Enlink('gambar'); ?>" id="<?php echo $fun->Enlink('gambar'); ?>">
					</div>

					<div class="row">
						<div class="col-12">
							<div class="input-style input-style-1 input-required">
								<span>Address</span>
								<em>(required)</em>
								<input type="" name="<?php echo $fun->Enlink('alamat'); ?>" id="<?php echo $fun->Enlink('alamat'); ?>" required>
							</div>
						</div>

						<div class="col-6">
							<div class="input-style input-style-1 input-required">
								<span>Province :</span>
								<em>(required)</em>
								<select name="<?php echo $fun->Enlink('prov'); ?>" id="<?php echo $fun->Enlink('prov'); ?>" required>
									<?php
									$s3 = "SELECT * from 11amasterprovince order by id_prov";
									$d3 = $fun->getData($s3, 2);
									foreach($d3 as $dt3 ) { ?>
										<option value="<?php echo $fun->Enval($dt3['id_prov']); ?>"><?php echo $dt3['nama_prov']; ?></option>
										<?php
									}
									?>
								</select>
							</div>
						</div>

						<div class="col-6">
							<div class="input-style input-style-1 input-required">
								<span>Area :</span>
								<em>(required)</em>
								<select class="browser-default txt-black" name="<?php echo $fun->Enlink('kabkot'); ?>" id="<?php echo $fun->Enlink('kabkot'); ?>" required>
									<option value="">Choose province first...</option>
								</select>
							</div>
						</div>
					</div>

					<button type="submit" name="confirm" id="confirm" class="btn btn-m btn-center-xl bg-highlight txt-white rounded-sm text-uppercase font-900 widt-100 mt-3 mb-3">Save</button>
				</div>
			</div>
		</form>

		<div class="divider divider-margins"></div>

	    <!-- footer and footer card-->
        <div class="footer" data-menu-load="footer.php"></div>  
	</div>