<?php
$scmpy = "SELECT * from 12mastercompany where companyid='".$fun->getIDParam('idcmpy')."'";
$dcmpy = $fun->getData($scmpy);

$sloccmpy = "SELECT a.nama_prov as nama_prov, b.nama_kabkot as nama_kabkot
		from 11amasterprovince as a 
		inner join 11bmastercity as b 
			on a.id_prov = b.id_prov 
		where a.id_prov='".$dcmpy[0]['province']."' and b.id_kabkot='".$dcmpy[0]['city']."'";
$dloccmpy = $fun->getData($sloccmpy);
?>

	<div class="page-content">

		<div class="page-title page-title-small">
	        <h2 id="title"><a href="?" data-back-button><i class="fa fa-arrow-left"></i></a>Edit Company</h2>
	        <a href="#" data-menu="menu-main" class="bg-fade-gray1-dark shadow-xl preload-img" data-src="images/avatars/5s.png"></a>
	    </div>

	    <div class="card header-card shape-rounded" data-card-height="150">
	    	<div class="card-overlay bg-highlight opacity-95"></div>
	    	<div class="card-overlay dark-mode-tint"></div>
	    	<div class="card-bg preload-img" data-src="images/pictures/20s.jpg"></div>
		</div>

		<form action="<?php echo 'process/05beditcompany.php?'.$fun->setIDParam('idcmpy', $fun->getIDParam('idcmpy') ); ?>" method="post" enctype="multipart/form-data">
			<div class="card card-style">
            	<div class="content mb-0">

            		<div class="input-style input-style-1 input-required">
						<span>Name</span>
						<em>(required)</em>
						<input type="text" name="<?php echo $fun->Enlink('nama'); ?>" id="<?php echo $fun->Enlink('nama'); ?>" placeholder="Name" value="<?php echo $dcmpy[0]['companyname']; ?>" required>
					</div>

					<div class="input-style input-style-1 input-required">
						<span>Email</span>
						<input type="text" name="<?php echo $fun->Enlink('email'); ?>" id="<?php echo $fun->Enlink('email'); ?>" placeholder="Email" value="<?php echo $dcmpy[0]['companyemail']; ?>">
					</div>

					<div class="input-style input-style-1 input-required">
						<span>Phone Number</span>
						<input type="text" name="<?php echo $fun->Enlink('tlp'); ?>" id="<?php echo $fun->Enlink('tlp'); ?>" placeholder="Phone Number" value="<?php echo $dcmpy[0]['companyphone']; ?>">
					</div>

					<div class="input-style input-style-1 input-required">
						<span>Label 1</span>
						<input type="text" name="<?php echo $fun->Enlink('label1'); ?>" id="<?php echo $fun->Enlink('label1'); ?>" placeholder="Label 1" value="<?php echo $dcmpy[0]['labeltier1']; ?>">
					</div>

					<div class="input-style input-style-1 input-required">
						<span>Label 2</span>
						<input type="text" name="<?php echo $fun->Enlink('label2'); ?>" id="<?php echo $fun->Enlink('label2'); ?>" placeholder="Label 2" value="<?php echo $dcmpy[0]['labeltier2']; ?>">
					</div>

					<div class="input-style input-style-1 input-required">
						<span>Label 3</span>
						<input type="text" name="<?php echo $fun->Enlink('label3'); ?>" id="<?php echo $fun->Enlink('label3'); ?>" placeholder="Label 3" value="<?php echo $dcmpy[0]['labeltier3']; ?>">
					</div>

					<div class="input-style input-style-1 input-required">
						<span>Label 4</span>
						<input type="text" name="<?php echo $fun->Enlink('label4'); ?>" id="<?php echo $fun->Enlink('label4'); ?>" placeholder="Label 4" value="<?php echo $dcmpy[0]['labeltier4']; ?>">
					</div>

					<div class="input-style input-style-1 input-required">
						<span>Label 5</span>
						<input type="text" name="<?php echo $fun->Enlink('label5'); ?>" id="<?php echo $fun->Enlink('label5'); ?>" placeholder="Label 5" value="<?php echo $dcmpy[0]['labeltier5']; ?>">
					</div>

					<div class="input-style input-style-1 input-required">
						<span>Location's Label</span>
						<input type="text" name="<?php echo $fun->Enlink('labellokasi'); ?>" id="<?php echo $fun->Enlink('labellokasi'); ?>" placeholder="Location's Label" value="<?php echo $dcmpy[0]['labellocation']; ?>">
					</div>

					<div class="input-style input-style-1 input-required">
						<span>Tenant's Label</span>
						<em>(required)</em>
						<input type="text" name="<?php echo $fun->Enlink('labeltenant'); ?>" id="<?php echo $fun->Enlink('labeltenant'); ?>" placeholder="Tenant's Label" value="<?php echo $dcmpy[0]['labeltenant']; ?>">
					</div>

					<div class="input-style input-style-1 input-required">
						<span>Vehicle's Label</span>
						<input type="text" name="<?php echo $fun->Enlink('labelvehicle'); ?>" id="<?php echo $fun->Enlink('labelvehicle'); ?>" placeholder="Vehicle's Label" value="<?php echo $dcmpy[0]['labelvehicle']; ?>">
					</div>

					<div class="">
						<span>Image</span>
						<input type="file" name="<?php echo $fun->Enlink('gambar'); ?>" id="<?php echo $fun->Enlink('gambar'); ?>">
					</div>

					<div class="row">
						<div class="col-12">
							<div class="input-style input-style-1 input-required">
								<span>Address</span>
								<em>(required)</em>
								<input type="" name="<?php echo $fun->Enlink('alamat'); ?>" id="<?php echo $fun->Enlink('alamat'); ?>" value="<?php echo $dcmpy[0]['companyaddress']; ?>" required>
							</div>
						</div>

						<div class="col-6">
							<span class="bold italic"><?php echo 'Province : '.$dloccmpy[0]['nama_prov']; ?></span> <span class="txt-link italic" id="choose_prov">[choose]</span>
							<div class="input-style input-style-1 input-required hide" id="select_prov">
								<span class="bold">Province :</span>
								<select name="<?php echo $fun->Enlink('prov'); ?>" id="<?php echo $fun->Enlink('prov'); ?>" required>
									<?php
									$sprov = "SELECT * from 11amasterprovince order by id_prov";
									$qprov = mysqli_query($fun->getConnection(), $sprov) or die(mysqli_error($fun->getConnection()));
									while($dprov = mysqli_fetch_array($qprov)) { ?>
										<option value="<?php echo $fun->Enval($dprov['id_prov']); ?>"><?php echo $dprov['nama_prov']; ?></option>
										<?php
									}
									?>
								</select>
							</div>
						</div>

						<div class="col-6">
							<span class="bold italic"><?php echo 'Area : '.$dloccmpy[0]['nama_kabkot']; ?></span> <span class="txt-link italic" id="choose_kabkot">[choose]</span>
							<div class="input-style input-style-1 input-required hide" id="select_kabkot">
								<span class="bold">Area :</span>
								<select name="<?php echo $fun->Enlink('kabkot'); ?>" id="<?php echo $fun->Enlink('kabkot'); ?>" required>
									<option value="">Choose province first...</option>
								</select>
							</div>
						</div>
					</div>

					<button type="submit" name="confirm" id="confirm" class="btn btn-m btn-center-xl bg-highlight txt-white rounded-sm text-uppercase font-900 widt-100 mt-3 mb-3">OK</button>
				</div>
			</div>
		</form>

		<div class="divider divider-margins"></div>

	    <!-- footer and footer card-->
        <div class="footer" data-menu-load="footer.php"></div>  
	</div>

<script type="text/javascript">
	$("#choose_prov").click(function(){
		if( $("#select_prov").hasClass("hide") ) {
			$("#select_prov").removeClass("hide");
		}
		else {
			$("#select_prov").addClass("hide");
		}
	});

	$("#choose_kabkot").click(function(){
		if( $("#select_kabkot").hasClass("hide") ) {
			$("#select_kabkot").removeClass("hide");
		}
		else {
			$("#select_kabkot").addClass("hide");
		}
	});
</script>