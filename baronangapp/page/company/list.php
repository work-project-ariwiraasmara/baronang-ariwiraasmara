	<div class="page-content">

		<div class="page-title page-title-small">
	        <h2 id="title"><a href="?pg=baronang" data-back-button><i class="fa fa-arrow-left"></i></a>Company</h2>
	        <a href="#" data-menu="menu-main" class="bg-fade-gray1-dark shadow-xl preload-img" data-src="images/avatars/5s.png"></a>
	    </div>

	    <div class="card header-card shape-rounded" data-card-height="150">
	    	<div class="card-overlay bg-highlight opacity-95"></div>
	    	<div class="card-overlay dark-mode-tint"></div>
	    	<div class="card-bg preload-img" data-src="images/pictures/20s.jpg"></div>
		</div>

		<div class="card card-style">
            <div class="content mt-3 mb-3">



		<?php
		$ano = 1;
		$sbprov = "SELECT distinct 	a.id_prov as id_prov, a.nama_prov as nama_prov
				from 11amasterprovince as a 
				inner join 12mastercompany as b 
					on a.id_prov = b.province
				order by nama_prov ASC";
		$rbprov = $fun->checkRowData($sbprov);

		if($rbprov > 0) { 
			$dbprov = $fun->getData($sbprov, 2);
			foreach($dbprov as $dt1) { ?>
				<div class="accordion" id="<?php echo 'accordion-'.$ano; ?>">
					<div class="mb-0">
						<button class="btn accordion-btn border-0"  data-toggle="collapse" data-target="<?php echo '#collapse-'.$ano; ?>">
		        			<?php echo $dt1['nama_prov'];  ?>
		                	<i class="fa fa-chevron-down font-10 accordion-icon"></i>
		            	</button>
						<div id="<?php echo 'collapse-'.$ano; ?>" class="collapse"  data-parent="<?php echo '#accordion-'.$ano; ?>">
							<?php
							$s2 = "SELECT * from 12mastercompany where province='".$dt1['id_prov']."' order by companyname ASC";
							$r2 = $fun->checkRowData($s2);
							if($r2 > 0) {
								$d2 = $fun->getData($s2, 2);
								foreach($d2 as $dt2) { ?>
									<div class="pt-1 pb-2 pl-3 pr-3">
										<a href="#" data-menu="<?php echo $fun->Enlink($dt2['companyid']); ?>">
											<?php echo $dt2['companyname']; ?>
										</a>
									</div>
									<?php
								}
							}
							else { ?>
								<div class="pt-1 pb-2 pl-3 pr-3">[Data not available]</div>
								<?php
							}
							?>
						</div>
					</div>
				</div>
				<?php
				$ano++;
			} ?>

			<?php
		}
		else { ?>
			<h1 class="uppercase bold center">[Data not available]</h1>
			<?php
		}
		?>

			</div>
		</div>

		<div class="center mt-3 mb-4">
	    	<a href="?pg=company&sb=add" class="btn btn-m btn-center-xl bg-highlight txt-white rounded-sm text-uppercase font-900"><span class="ion-android-add font-21"></span></a>
	    </div>

	    <div class="divider divider-margins"></div>

	    <!-- footer and footer card-->
        <div class="footer" data-menu-load="footer.php"></div>  
	</div>


<?php
$sbprov = "SELECT distinct 	b.id_prov as id_prov, c.id_kabkot as id_kabkot,
				 			b.nama_prov as nama_prov, c.nama_kabkot as nama_kabkot
				from 12mastercompany as a 
				inner join 11amasterprovince as b 
					on a.province = b.id_prov 
				inner join 11bmastercity as c 
					on a.city = c.id_kabkot
				order by nama_prov ASC";
$rbprov = $fun->checkRowData($sbprov);
if($rbprov > 0) {
	$dbprov = $fun->getData($sbprov, 2);
	foreach($dbprov as $dt1) {
		$s2 = "SELECT * from 12mastercompany where province='".$dt1['id_prov']."' order by companyname ASC";
		$r2 = $fun->checkRowData($s2);
		if($r2 > 0) {
			$d2 = $fun->getData($s2, 2);
			foreach($d2 as $dt2) { ?>
				<div id="<?php echo $fun->Enlink($dt2['companyid']); ?>" 
		             class="menu menu-box-modal menu-box-detached rounded-m" 
		             data-menu-height="380" 
		             data-menu-width="275" 
		             data-menu-effect="menu-over">

		             <h1 class="text-center font-700 mt-3 pt-2"><?php echo $dt2['companyname']; ?></h1>

		             <p class="boxed-text-xl justify mt-3">
		             	<?php
						if($dt2['companylogo'] != '' || !empty($dt2['companylogo']) ) { ?>
							<img src="<?php echo 'images/company/'.$dt2['companylogo']; ?>">
							<?php
						}
						?>

						<span class="bold">Email :</span><br/><span><?php echo $dt2['companyemail']; ?></span><br/><br/>
						<span class="bold">Phone Number :</span><br/><span><?php echo $dt2['companyphone']; ?></span><br/><br/>
						<span class="bold">Address :</span><br/><span><?php echo $dt2['companyaddress'].', '.$dt1['nama_kabkot'].', '.$dt1['nama_prov']; ?></span>
		             </p>

		             <a href="<?php echo '?pg=company&sb=edit&'.$fun->setIDParam('idcmpy', $dt2['companyid']); ?>" class="btn btn-m btn-center-xl bg-highlight txt-white rounded-sm text-uppercase font-900 mb-3">
						<i class="ion-android-create font-18"></i>
					 </a>
				</div>
				<?php
			}
		}
	}
}
?>