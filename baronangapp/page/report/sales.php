<?php
//if(isset($_GET['b'])) { $b = $funct->Denval(@$_GET['b']); } else { $b = date('m'); }
//if(isset($_GET['y'])) { $y = $funct->Denval(@$_GET['y']); } else { $y = date('Y'); }
$d = date('d');
$m = date('m');
$y = date('Y');
$numdays = date('t');
$x = 1;
?>

<div class="animated fadeinup delay-1">
	<div class="page-content">

		<h1 class="title txt-black" id="title"></h1>
		<h3 class="title txt-black"><?php echo date('F Y'); ?></h3>

		
			<div class="table-responsive m-t-30">
				
				<table>
					<thead>
						<tr>
							<th class="center">Location</th>
							<th class="center">Total Sales (Points)</th>
						</tr>
						
					</thead>

					<tbody>
						<?php
						$totalcredit = 0;
						$totaldebet = 0;
						$totalpointcr = 0;
						$totalpointdb = 0;
						
						$sapp1 = "SELECT * from 21masterlocation where active = '1' order by locationname";
						$qapp1 = mysqli_query($fun->getConnection(), $sapp1) or die(mysqli_error($fun->getConnection()));
						while($dapp1 = mysqli_fetch_array($qapp1)) {
							$spurchase = "Select sum(debet) as debet, sum(pointdebet) as pointdebet 
							from 31cmastercardactivity where locationid = '".$dapp1['masterlocationid']."' and 
							Month(datecreated) = '$m' and Year(datecreated) = '$y'";
							$qpurchase = mysqli_query($fun->getConnection(), $spurchase) or die(mysqli_error($fun->getConnection()));
							$dpurchase = mysqli_fetch_array($qpurchase);
							?>
							<tr>
								<td><?php echo $dapp1['locationname']; ?></td>
								<td style="text-align: right;"><?php echo $fun->FormatNumber($dpurchase['pointdebet']); ?></td>
								
							</tr>
						<?php 
							$totalpointdb = $totalpointdb + $dpurchase['pointdebet'];
							$totaldebet = $totaldebet + $dpurchase['debet'];
							$x++;
							}
							?>
						
					</tbody>

					<tfoot>
						<tr>
							<th style="text-align: right;">Total</th>
							<th style="text-align: right;"><?php echo $fun->FormatNumber($totalpointdb); ?></th>
							
						</tr>
					</tfoot>

				</table>

			</div>

	</div>
</div>

<script type="text/javascript">
	$('#toBack').click(function() {
		window.location.href = "<?php echo '?#'.$fun->Enlink('skymanagement'); ?>";
	});
</script>
