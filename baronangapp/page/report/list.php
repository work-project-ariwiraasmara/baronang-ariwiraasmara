<div class="animated fadeinup delay-1">
	<div class="page-content">

		<h1 class="title uppercase txt-black" id="title"></h1>
		
		<div class="m-t-30 txt-black">
			<a href="?pg=report&sb=financial" class="waves-effect waves-light">Financial Report</a>
		</div>
		
		<div class="m-t-30 txt-black">
			<a href="?pg=report&sb=sales" class="waves-effect waves-light">Membership Sales</a>
		</div>

		<div class="floating-button page-fab animated bouncein delay-3" style="z-index: 1;">
			<a class="btn-floating btn-large waves-effect waves-light primary-color btn z-depth-1" href="<?php echo '?pg=generalproduct&sb=add&'.$fun->setIDParam('lokasi', $locid); ?>">
				<i class="ion-android-add"></i>
			</a>
		</div>

	</div>
</div>

<script type="text/javascript">
	$('#toBack').click(function() {
		window.location.href = "<?php echo '?#'.$fun->Enlink('skymanagement'); ?>";
	});
</script>


