<?php
$session = $fun->getValookie('session');

if($session == 'member') {
	$s1 = "SELECT * from 22bmastermember where mastermemberid='".$fun->getValookie('id')."'";
	$name = "membername";
}
else {
	$s1 = "SELECT * from 23dmastertenant where mastertenantid='".$fun->getValookie('id')."'";
	$name = "tenantname";
}
$d1 = $fun->getData($s1, 2);

$s3 = "SELECT * from 21masterlocation where masterlocationid='".$d1[0]['locationid']."'";
$r3 = $fun->checkRowData($s3, 2);
if($r3 > 0) { 
	$d3 = $fun->getData($s3, 2); 
	$locname = $d3[0]['locationname'];
}
else { 
	$locname = 'null';
}

$s2 = "SELECT * from 31bmastercarduser where cardid like '7000%' and  useridactivated='".$fun->getValookie('id')."' and activationstatus = '1' limit 1";
$r2 = $fun->checkRowData($s2);
if($r2 > 0) { 
	$d2 = $fun->getData($s2); 
	$cardid = $d2[0]['cardid'];
	$spoint = "SELECT points from 23amastercard where mastercardid='".$cardid."'";
	$dpoint = $fun->getData($spoint, 2);
}
else { 
	$cardid = 0;
}
?>
<div id="<?php echo $fun->Enlink('card1-'.$cardid); ?>" 
     class="menu menu-box-modal menu-box-detached rounded-m" 
     data-menu-height="500" 
     data-menu-width="300" 
     data-menu-effect="menu-over">

	<div class="card_container txt-white" style="padding: 10px;">
		<div class="card_company">
			<?php 
			$scomp = "SELECT companyname from 12mastercompany where companyid='70' and status = '1'";
			$dcomp = $fun->getData($scomp);

			echo strtoupper($dcomp[0]['companyname']); ?>
		</div>

		<img src="images/1.png" id="imgcard" style="width: 100% !important;">

		<div class="card_identity">
			<?php
			echo $d1[0][$name].'<br>';
			echo $locname.'<br>';
			if ($dpoint[0]['points'] > 0){
				echo $cardid.'<br>';
				echo $dpoint[0]['points'].' Points';
			}
			else {
				echo $cardid;
			}
			?>
		</div>

		<div class="card_logo">
			<img src="images/icon/Logo-whitefish-icon.png" style="width: 40px !important; height: 20px !important;">
		</div>
	</div>

	<div style="margin-left: 10px; margin-right: 10px;">
		<a href="<?php echo '#'.$fun->setIDParam('cardid', $cardid); ?>" class="btn btn-m btn-center-xl bg-highlight txt-white rounded-sm text-uppercase font-900 width-100 mt-3 mb-3">Top Up Point</a>
		<a href="<?php echo '#'.$fun->setIDParam('cardid', $cardid); ?>" class="btn btn-m btn-center-xl bg-highlight txt-white rounded-sm text-uppercase font-900 width-100 mt-3 mb-3">Purchase Product</a>
		<a href="<?php echo '?pg=card&sb=financialhistory&'.$fun->setIDParam('cardid', $cardid); ?>" class="btn btn-m btn-center-xl bg-highlight txt-white rounded-sm text-uppercase font-900 width-100 mt-3 mb-3">Financial History</a>
		<a href="<?php echo '?pg=card&sb=activity&'.$fun->setIDParam('cardid', $cardid); ?>" class="btn btn-m btn-center-xl bg-highlight txt-white rounded-sm text-uppercase font-900 width-100 mt-3 mb-3">Card Activity</a>
		<a href="<?php echo '?pg=card&sb=productsowned&'.$fun->setIDParam('cardid', $cardid); ?>" class="btn btn-m btn-center-xl bg-highlight txt-white rounded-sm text-uppercase font-900 width-100 mt-3 mb-3">Products Owned</a>
	</div>
</div>

<?php
// KARTU 7100
$scard1 = "SELECT * from 31bmastercarduser where cardid like '7100%' and useridactivated='".$fun->getValookie('id')."'";
$dcard1 = $fun->getData($scard1, 2);
foreach($dcard1 as $dcard1) { 

	$sctype = "SELECT cardtype from 23amastercard where mastercardid='".$dcard1['cardid']."'";
	$dctype = $fun->getData($sctype);

	$sloc = "SELECT * from 21masterlocation where masterlocationid='".substr($dcard1['cardid'],0,9).'0000000'."'";
	$dloc = $fun->getData($sloc);
	
	$spoint = "SELECT points from 23amastercard where mastercardid='".$dcard1['cardid']."'";
	$dpoint = $fun->getData($spoint);
	
	$scomp = "SELECT companyname from 12mastercompany where companyid='71' and status = '1'";
	$dcomp = $fun->getData($scomp);
	
	?>
	<div id="<?php echo $fun->Enlink('card2-'.$dcard1['cardid']); ?>" 
         class="menu menu-box-modal menu-box-detached rounded-m" 
         data-menu-height="500" 
         data-menu-width="300" 
         data-menu-effect="menu-over">

		<div class="card_container txt-white" style="padding: 10px;">
			<div class="card_sky">
				<img src="images/logo skyparking 1.png" style="width: 45px !important; height: 45px !important;">
			</div>

			<div class="card_company">
				<?php echo strtoupper($dcomp[0]['companyname']); ?>
			</div>

			<img src="images/2.png" id="imgcard" style="width: 100% !important;">

			<div class="card_identity">
				<?php
				echo $d1[0][$name].'<br>';
				echo $dctype[0]['cardtype'].'<br>';
				if ($dpoint[0]['points'] > 0){
					echo $dcard1[0]['cardid'].'<br>';
					echo $fun->FormatNumber($dpoint[0]['points']).' Points';
				}
				else {
					echo $dcard1['cardid'];
				}
				?>
			</div>
					
			<div class="card_logo">
				<img src="images/icon/Logo-whitefish-icon.png" style="width: 40px !important; height: 20px !important;">
			</div>
		</div>

		<div style="margin-left: 10px; margin-right: 10px;">
			<a href="<?php echo '?pg=card&sb=topup&'.$fun->setIDParam('cardid', $dcard1['cardid']).'&'.$fun->setIDParam('lokasi', $dloc[0]['masterlocationid']); ?>" class="btn btn-m btn-center-xl bg-highlight txt-white rounded-sm text-uppercase font-900 width-100 mt-3 mb-3">Top Up Point</a>
			<a href="<?php echo '?pg=card&sb=product&'.$fun->setIDParam('cardid', $dcard1['cardid']); ?>" class="btn btn-m btn-center-xl bg-highlight txt-white rounded-sm text-uppercase font-900 width-100 mt-3 mb-3">Purchase Product</a>
			<a href="<?php echo '?pg=card&sb=financialhistory&'.$fun->setIDParam('cardid', $dcard1['cardid']); ?>" class="btn btn-m btn-center-xl bg-highlight txt-white rounded-sm text-uppercase font-900 width-100 mt-3 mb-3">Financial History</a>
			<a href="<?php echo '?pg=card&sb=activity&'.$fun->setIDParam('cardid', $dcard1['cardid']); ?>" class="btn btn-m btn-center-xl bg-highlight txt-white rounded-sm text-uppercase font-900 width-100 mt-3 mb-3">Card Activity</a>
			<a href="<?php echo '?pg=card&sb=productsowned&'.$fun->setIDParam('cardid', $dcard1['cardid']); ?>" class="btn btn-m btn-center-xl bg-highlight txt-white rounded-sm text-uppercase font-900 width-100 mt-3 mb-3">Products Owned</a>
		</div>
	</div>
	<?php
}


//7100 [MERAH] Card Product Owned 
$scard5 = "SELECT * from 31bmastercarduser where cardid like '7100%' and useridactivated='".$fun->getValookie('id')."' and activationstatus = '1'";
$dcard5 = $fun->getData($scard5, 2);
foreach($dcard5 as $dcard5) {

	$scard4 = "SELECT * from 31amastercardproductowned where cardid ='".$dcard5['cardid']."' and active = '1'";
	$dcard4 = $fun->getData($scard4, 2);
	foreach($dcard4 as $dcard4) { 

		$expdate = new datetime($dcard4['expirationdate']);
		$svhl = "SELECT b.vehicletype as vehicle, b.productimage as productimage,
						b.locationid as locationid 
						from 31amastercardproductowned as a 
						inner join 22amasterproduct as b 
							on a.productowned = b.masterproductid
						where a.serialnumber='".$dcard4['serialnumber']."'";
		$dvhl = $fun->getData($svhl);
		
		$sloc = "SELECT * from 21masterlocation where masterlocationid = '".$dvhl[0]['locationid']."'";
		$dloc = $fun->getData($sloc);
		?>
		<div id="<?php echo $fun->Enlink('card5-'.$dcard4['serialnumber']); ?>" 
         	class="menu menu-box-modal menu-box-detached rounded-m" 
         	data-menu-height="320" 
         	data-menu-width="300" 
         	data-menu-effect="menu-over">

			<div class="card_container txt-white" style="padding: 10px;">
				<div class="card_sky">
					<img src="images/logo skyparking 1.png" style="width: 45px !important; height: 45px !important;">
				</div>

				<div class="card_company">
					<?php
					if($dloc[0]['image'] == '' || empty($dloc[0]['image'])) {
						echo $dloc['locationname'];
					}
					else { ?>
						<img src="<?php echo 'images/location/'.$dloc[0]['image']; ?>" style="width: 45px !important; height: 45px !important;">
						<?php
					}
					?>
				</div>

				<img src="images/5.png" id="imgcard" style="width: 100% !important;">
	 
				<div class="card_identity">
					<?php
					echo $d1[0][$name].'<br>';
					echo $dcard4['serialnumber'].'<br>';
					echo date_format($expdate,'d/m/Y');
					?>
				</div>
						
				<div class="card_logo" style="margin-bottom: -3px;">
					<?php
					if($dvhl[0]['productimage'] == '' || empty($dvhl[0]['productimage'])) {?>
						<div class="bold m-b-10"><?php echo $dvhl[0]['vehicle']; ?></div>
						<?php
					}
					else { ?>
						<div style="margin-bottom: 10px;">
							<img src="<?php echo 'images/icon/'.$dvhl[0]['productimage']; ?>" style="width: 40px; height: 25px;">
						</div>
						<?php
					}
					?>

					<img src="images/icon/Logo-whitefish-icon.png" style="width: 40px !important; height: 20px !important;">
				</div>
			</div>

			<input type="text" name="<?php echo $fun->Enlink('share-'.$dcard4['serialnumber']); ?>" id="<?php echo $fun->Enlink('share-'.$dcard4['serialnumber']); ?>" class="hide" value="<?php echo $fun->Enval($dcard4['serialnumber']); ?>" readonly>

			<div style="margin-left: 10px; margin-right: 10px;">
				<a href="#cardactivity" class="btn btn-m btn-center-xl bg-highlight txt-white rounded-sm text-uppercase font-900 width-100 mt-3 mb-3">Card Acitivity</a>
				<button type="button" data-menu="<?php echo $fun->Enlink('formshareaccess'); ?>" class="btn btn-m btn-center-xl bg-highlight txt-white rounded-sm text-uppercase font-900 width-100 mt-3 mb-3" id="<?php echo $fun->Enlink('btnshare-'.$dcard4['serialnumber']); ?>">Share Access</button>	
			</div>
		</div>

		<script type="text/javascript">
			$("<?php echo '#'.$fun->Enlink('btnshare-'.$dcard4['serialnumber']); ?>").click(function() {
				cardid_shareaccess = $("<?php echo '#'.$fun->Enlink('share-'.$dcard4['serialnumber']); ?>").val();
				//console.log(cardid_shareaccess);
				//$("<?php echo '#'.$fun->Enlink('toShareAccess'); ?>").click();
			});
		</script>
		<?php
	}
}


// KARTU 7101
$scard2 = "SELECT * from 31bmastercarduser where cardid like '7101%' and useridactivated='".$fun->getValookie('id')."' and activationstatus = '1'";
$dcard2 = $fun->getData($scard2, 2);
foreach($dcard2 as $dcard2) { 

	$svhl = "SELECT b.vehicletype as vehicle, b.productimage as productimage
			from 31amastercardproductowned as a 
			inner join 22amasterproduct as b 
				on a.productowned = b.masterproductid
			where a.cardid='".$dcard2['cardid']."'";
	$dvhl = $fun->getData($svhl);
	
	$locid = substr($dcard2['cardid'],0,9).'0000000';
	$sloc = "SELECT * from 21masterlocation where masterlocationid = '".$locid."'";
	$dloc = $fun->getData($sloc);
	?>
	<div id="<?php echo $fun->Enlink('card3-'.$dcard2['cardid']); ?>" 
        class="menu menu-box-modal menu-box-detached rounded-m" 
        data-menu-height="320" 
        data-menu-width="300" 
        data-menu-effect="menu-over">

		<div class="card_container txt-white" style="padding: 10px;">
			<div class="card_sky">
				<img src="images/logo skyparking 1.png" style="width: 45px !important; height: 45px !important;">
			</div>

			<div class="card_company">
				<?php
				if($dloc[0]['image'] == '' || empty($dloc[0]['image'])) {
					echo $dloc[0]['locationname'];
				}
				else { ?>
					<img src="<?php echo 'images/location/'.$dloc[0]['image']; ?>" style="width: 45px !important; height: 45px !important;">
					<?php
				}
				?>
			</div>

			<img src="images/3.png" id="imgcard" style="width: 100% !important;">
 
			<div class="card_identity">
				<?php
				echo $d1[0][$name].'<br>';
				echo $dcard2['cardid'];
				?>
			</div>
					
			<div class="card_logo" style="margin-bottom: -3px;">
				<?php
				if($dvhl[0]['productimage'] == '' || empty($dvhl[0]['productimage'])) {?>
					<div class="bold m-b-10"><?php echo $dvhl[0]['vehicle']; ?></div>
					<?php
				}
				else { ?>
					<div style="margin-bottom: 10px;">
						<img src="<?php echo 'images/icon/'.$dvhl[0]['productimage']; ?>" style="width: 40px !important; height: 25px !important;">
					</div>
					<?php
				}
				?>

				<img src="images/icon/Logo-whitefish-icon.png" style="width: 40px !important; height: 20px !important;">
			</div>
		</div>

		<div style="margin-left: 10px; margin-right: 10px;">
			<a href="#cardactivity" class="btn btn-m btn-center-xl bg-highlight txt-white rounded-sm text-uppercase font-900 width-100 mt-3 mb-3">Card Activity</a>
			<a href="<?php echo '?pg=card&sb=productsowned&'.$fun->setIDParam('cardid', $dcard2['cardid']); ?>" class="btn btn-m btn-center-xl bg-highlight txt-white rounded-sm text-uppercase font-900 width-100 mt-3 mb-3">Products Owned</a>
		</div>
	</div>
	<?php
}


// KARTU 7104
$scard3 = "SELECT * from 31bmastercarduser where cardid like '7104%' and useridactivated='".$fun->getValookie('id')."' and activationstatus = '1'";
$dcard3 = $fun->getData($scard3, 2);
foreach($dcard3 as $dcard3) {

	$svhl = "SELECT b.vehicletype as vehicle, b.productimage as productimage
			from 31amastercardproductowned as a 
			inner join 22amasterproduct as b 
				on a.productowned = b.masterproductid
			where a.cardid='".$dcard3['cardid']."'";
	$dvhl = $fun->getData($svhl);
	
	$locid = '7101'.substr($dcard3['cardid'],4,5).'0000000';
	$sloc = "SELECT * from 21masterlocation where masterlocationid = '".$locid."'";
	$dloc = $fun->getData($sloc);
	?>
	<div id="<?php echo $fun->Enlink('card4-'.$dcard3['cardid']); ?>" 
        class="menu menu-box-modal menu-box-detached rounded-m" 
        data-menu-height="500" 
        data-menu-width="300" 
        data-menu-effect="menu-over">

		<div class="card_container txt-white" style="padding: 10px;">			
			<div class="card_sky">
				<img src="images/logo skyparking 1.png" style="width: 45px !important; height: 45px !important;">
			</div>

			<div class="card_company">
				<?php
				if($dloc[0]['image'] == '' || empty($dloc[0]['image'])) {
					echo $dloc[0]['locationname'];
				}
				else { ?>
					<img src="<?php echo 'images/location/'.$dloc[0]['image']; ?>" style="width: 45px !important; height: 45px !important;">
					<?php
				}
				?>
			</div>

			<img src="images/4.png" id="imgcard" style="width: 100% !important;">
 
			<div class="card_identity">
				<?php
				echo $d1[0][$name].'<br>';
				echo $dcard3['cardid'];
				?>
			</div>
					
			<div class="card_logo row" style="margin-bottom: -3px;">
				<?php
				if($dvhl[0]['productimage'] == '' || empty($dvhl[0]['productimage'])) {?>
					<div class="bold m-b-10"><?php echo $dvhl[0]['vehicle']; ?></div>
					<?php
				}
				else { ?>
					<div style="margin-bottom: 10px;">
						<img src="<?php echo 'images/icon/'.$dvhl[0]['productimage']; ?>" style="width: 40px !important; height: 25px !important;">
					</div>
					<?php
				}
				?>

				<img src="images/icon/Logo-whitefish-icon.png" style="width: 40px !important; height: 20px !important;">
			</div>
		</div>

		<div style="margin-left: 10px; margin-right: 10px;">
			<a href="#cardactivity" class="btn btn-m btn-center-xl bg-highlight txt-white rounded-sm text-uppercase font-900 width-100 mt-3 mb-3">Card Activity</a>
			<a href="<?php echo '?pg=card&sb=productsowned&'.$fun->setIDParam('cardid', $dcard3['cardid']) ?>" class="btn btn-m btn-center-xl bg-highlight txt-white rounded-sm text-uppercase font-900 width-100 mt-3 mb-3">Products Owned</a>
		</div>
	</div>
	<?php
}
?>

<a href="<?php echo '#'.$fun->Enlink('formshareaccess'); ?>" class="hide modal-trigger" id="<?php echo $fun->Enlink('toShareAccess'); ?>">toShareAccess</a>

<div id="<?php echo $fun->Enlink('formshareaccess'); ?>" 
    class="menu menu-box-modal menu-box-detached rounded-m" 
    data-menu-height="230" 
   	data-menu-width="300" 
    data-menu-effect="menu-over">

	<h1 class="text-center font-700 mt-3 pt-2">Share Access</h1>
	<p class="boxed-text-xl justify">
		<div class="input-style input-style-1 input-required" style="margin-left: 10px; margin-right: 10px;">
			<span>Enter User Email</span>
			<em>(required)</em>
			<input type="email" name="<?php echo $fun->Enlink('emailshareaccess'); ?>" id="<?php echo $fun->Enlink('emailshareaccess'); ?>" placeholder="Enter User Email">
		</div>
	</p>

	<div style="margin-left: 10px; margin-right: 10px;">
		<button type="button" class="btn btn-m btn-center-xl bg-highlight txt-white rounded-sm text-uppercase font-900 width-100 mt-3 mb-3" id="<?php echo $fun->Enlink('btnformshareaccess_ok'); ?>">Share Access</button>
	</div>
</div>
