<?php
if( isset($_GET[$fun->Enlink('ideukey')]) ) { 

	if( $fun->getIDParam('ideukey') == '' || empty($fun->getIDParam('ideukey')) || is_null($fun->getIDParam('ideukey')) ) {
		echo "Nilai Paramater ideukey kosong";
		header('location:?');
	}
	else {
		$s1 = "SELECT * from 23cOutgoingEmail where ukey='".$fun->getIDParam('ideukey')."'";
		$q1 = mysqli_query($fun->getConnection(), $s1) or die(mysqli_error($fun->getConnection()));
		$d1 = mysqli_fetch_array($q1);

		if( count($d1) > 0 ) {

			if($d1['status'] > 0) { 

				if( strtotime(date('Y-m-d H:i:s')) < strtotime($d1['datevalid']) ) { ?>
					<div class="page-content">

						<div class="page-title page-title-small">
							<h1 class="txt-white center" id="title">Change Password</h1>
						</div>

						<div class="card header-card shape-rounded" data-card-height="150">
				            <div class="card-overlay bg-highlight opacity-95"></div>
				            <div class="card-overlay dark-mode-tint"></div>
				            <div class="card-bg preload-img" data-src="images/pictures/20s.jpg"></div>
				        </div>

						<div class="card card-style">
            				<div class="content mb-0">    

								<form action="<?php echo 'process/12updatepassword.php?'.$fun->setIDParam('ideukey', $fun->getIDParam('ideukey')); ?>" method="post">
									
									<div class="input-style has-icon input-style-1 input-required">
				                        <i class="input-icon fa fa-lock color-theme"></i>
				                        <span>Password</span>
				                        <em>(required)</em>
				                        <input type="password" name="<?php echo $fun->Enlink('pass'); ?>" id="<?php echo $fun->Enlink('pass'); ?>" placeholder="Password">
				                    </div> 

				                    <div class="input-style has-icon input-style-1 input-required">
				                        <i class="input-icon fa fa-lock color-theme"></i>
				                        <span>Confirmation</span>
				                        <em>(required)</em>
				                        <input type="password" name="<?php echo $fun->Enlink('passconf'); ?>" id="<?php echo $fun->Enlink('passconf'); ?>" placeholder="Password Confirmation">
				                    </div> 

				                    <button type="submit" name="<?php echo $fun->Enlink('confirm') ?>" id="<?php echo $fun->Enlink('confirm') ?>" class="btn btn-m mt-4 mb-3 btn-full rounded-sm bg-highlight text-uppercase font-900 width-100">Change</button>
								</form>

							</div>
						</div>
						
						<!-- footer and footer card-->
        				<div class="footer" data-menu-load="footer.php"></div>  
					</div>
					<?php
				}
				else { 
					$s2 = "DELETE from 23cOutgoingEmail where ukey='".$fun->getIDParam('ideukey')."'";
					$q2 = mysqli_query($fun->getConnection(), $s2) or die(mysqli_error($fun->getConnection()));

					//$fun->setOneSession('error_content', 'Sudah Tidak Bisa Merubah Password!<br><br>Sudah Lewat Masa Tenggat!<br><br>Silahkan Buat Permintaan Baru Lagi!', 2);
					//header('location:ei.php?'.$fun->setIDParam('title', 'Error Send Request!').'&'.$fun->setIDParam('link', 'index.php'));
					header('location: ../index.php?'.$fun->setIDParam('status', 'You can\'t change your password!<br><br>The deadline has passed!<br><br>Please make a new request again!'));
				}
			}
			else {
				//echo "Paramete ideukey ada tapi status = 0, maka data di hapus";
				$s2 = "DELETE from 23cOutgoingEmail where ukey='".$fun->getIDParam('ideukey')."'";
				$q2 = mysqli_query($fun->getConnection(), $s2) or die(mysqli_error($fun->getConnection()));

				$fun->setOneSession('error_content', '', 2);
				header('location:?');
			}

		}
		else {
			//echo "Parameter ideukey ada tapi di data tidak ada";
			$fun->setOneSession('error_content', '', 2);
			header('location:?');
		}
	}

}
else {
	//echo "Tidak Ada Paramater ideukey";
	$fun->setOneSession('error_content', '', 2);
	header('location:?');
}
?>