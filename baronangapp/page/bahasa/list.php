<style type="text/css">
	#langtext {
		border: none;
		margin-top: 13px;
	}
</style>

<div class="animated fadeinup delay-1">
	<div class="page-content">

		<h1 class="title uppercase txt-black" id="title"></h1>

		<div style="width: 50%;">
			<div id="google_translate_element" ></div>
		</div>

		<?php /*
		<div class="m-t-30 bottom center" id="ytWidget" style="font-size: 10px;"></div>

		<div class="form-inputs">
			<form action="" method="post">

				<div class="input-field">
					<span>Perusahaan</span><br>
					<select class="browser-default" name="company" id="company">
						<?php
						$scp = "SELECT * from 12mastercompany";
						$qcp = mysqli_query($fun->getConnection(), $scp) or die(mysqli_error($fun->getConnection()));
						while($dcp = mysqli_fetch_array($qcp)) { ?>
							<option value="<?php echo $dcp['companyid']; ?>"><?php echo $dcp['companyname']; ?></option>
							<?php
						}
						?>
					</select>
				</div>
			
				<div class="input-field m-t-30">
					<label for="ket">Keterangan</label>
					<input type="text" name="ket" id="ket" class="vlidate">
				</div>

				<div class="input-field m-t-30">
					<span>Set Default?</span><br>
					<span class="m-r-20">
						<input type="radio" name="default" id="default-1" value="1">
						<label for="default-1" class="txt-black">Ya</label>
					</span>

					<span class="m-l-20">
						<input type="radio" name="default" id="default-0" value="0">
						<label for="default-0" class="txt-black">Tidak</label>
					</span>
				</div>

				<div class="input-field">
					<span>Pilih Bahasa</span> : <br>
					<span class="m-r-20">
						<input type="checkbox" name="lang[1]" id="lang-id" value="id">
						<label for="lang-id" class="txt-black">Indonesia</label>
					</span>

					<span class="m-l-20">
						<input type="checkbox" name="lang[2]" id="lang-en" value="en">
						<label for="lang-en" class="txt-black">English</label>
					</span>
				</div>

				<button type="submit" name="ok" id="ok" class="btn btn-large width-100 primary-color waves-effect waves-light m-t-50 borad-20">OK</button>

			</form>

			<?php
			if( isset($_POST['ok']) ) {

				$company = @$_POST['company'];
				$default = @$_POST['default'];
				$ket 	 = @$_POST['ket'];
				$lang 	 = '';

				if( isset($_POST['lang']) ) {
					foreach (@$_POST['lang'] as $val) {
						$lang .= $val.',';
					}
				}

				$stin = "INSERT into bahasa values('', '$company', '$lang', '$ket', '$default')";
				//echo $stin;
				$qtin = mysqli_query($fun->getConnection(), $stin) or die(mysqli_error($fun->getConnection()));

			}
			?>

		</div>
		*/ ?>

		<div class="m-t-30 table-responsive">

			<?php
			if(isset($_GET[$fun->Enlink('lang')])) {
				$linkform = 'process/61beditlang.php?'.$fun->setIDParam('item', 'all').'&'.$fun->setIDParam('lang', $fun->getIDParam('lang')).'&'.$fun->setIDParam('ID', $fun->getIDParam('ID'));
				$scp = "SELECT * from 12mastercompany where companyid='".$fun->getIDParam('ID')."'";
				$qcp = mysqli_query($fun->getConnection(), $scp) or die(mysqli_error($fun->getConnection()));
				$dcp = mysqli_fetch_array($qcp)
				?>
				<span class="bold"><?php echo $dcp['companyname'].' | '.strtoupper($fun->getIDParam('lang')); ?></span>
				<?php
			}
			else {
				$linkform = '';
				$scp = "SELECT * from 12mastercompany";
			}
			?>

			<form action="<?php echo $linkform; ?>" method="post">
				<table class="m-t-10">
					<thead>
						<tr>
							<?php
							if(isset($_GET[$fun->Enlink('lang')])) { ?>
								<th>Item</th>
								<th>Label</th>
								<th></th>
								<?php
							}
							else { ?>
								<th>Company</th>
								<th class="center" colspan="2">Language</th>
								<?php
							}
							?>
						</tr>
					</thead>

					<tbody>
						<?php
						if(isset($_GET[$fun->Enlink('lang')])) {
							$no = 1;
							$slang = "SELECT * from language";
							$qlang = mysqli_query($fun->getConnection(), $slang) or die(mysqli_error($fun->getConnection()));
							while($dlang = mysqli_fetch_array($qlang)) { ?>
								<tr>
									<td>
										<input type="hidden" name="<?php echo $fun->Enlink('langitem').'[]'; ?>" id="<?php echo $fun->Enlink('langitem').'-'.$no; ?>" value="<?php echo $fun->Enval($dlang['itemid']); ?>">
										<?php echo $dlang['itemname']; ?>
									</td>
									<td><input type="text" name="<?php echo $fun->Enlink('langtext').'[]'; ?>" id="langtext" value="<?php echo $dlang[ strtoupper($dcp['companyid'].$fun->getIDParam('lang')) ]; ?>"></td>
								</tr>
								<?php
								$no++;
							}
						}
						else {
							$qcp = mysqli_query($fun->getConnection(), $scp) or die(mysqli_error($fun->getConnection()));
							while($dcp = mysqli_fetch_array($qcp)) { ?>
								<tr>
									<td><?php echo $dcp['companyname']; ?></td>
									<td class="txt-link center"><a href="<?php echo '?pg=lang&'.$fun->setIDParam('lang', 'id').'&'.$fun->setIDParam('ID', $dcp['companyid']); ?>">ID</a></td>
									<td class="txt-link center"><a href="<?php echo '?pg=lang&'.$fun->setIDParam('lang', 'en').'&'.$fun->setIDParam('ID', $dcp['companyid']); ?>">EN</a></td>
								</tr>
								<?php
							}
						}
						?>
					</tbody>

					<?php
					if(isset($_GET[$fun->Enlink('lang')])) { ?>
						<tfoot>
							<tr>
								<td colspan="2"><button type="submit" name="btnok" id="btnok" class="btn btn-large width-100 primary-color waves-effect waves-light borad-20">Update</button></td>
							</tr>
						</tfoot>
						<?php
					}
					?>

				</table>
			</form>

			<?php /*
			if(isset($_GET[$fun->Enlink('lang')])) { ?>
				<div class="floating-button page-fab animated bouncein delay-3" style="z-index: 1;">
					<a class="btn-floating btn-large waves-effect waves-light primary-color btn z-depth-1" href="<?php echo '?pg=lang&sb=add&'.$fun->setIDParam('lang', $fun->getIDParam('lang')).'&'.$fun->setIDParam('ID', $fun->getIDParam('ID')); ?>">
						<i class="ion-android-add"></i>
					</a>
				</div>
				<?php
			} 
			*/ ?>

			

		</div>

	</div>
</div>

<script type="text/javascript">
	$('#toBack').click(function() {

		<?php
		if(isset($_GET[$fun->Enlink('lang')])) { ?>
			window.location.href = "?pg=lang";
			<?php
		}
		else { ?>
			window.location.href = "<?php echo '?#'.$fun->Enlink('baronang') ?>";
			<?php
		}
		?>

		
	});
</script>