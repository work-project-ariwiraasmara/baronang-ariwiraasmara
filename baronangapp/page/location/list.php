	<div class="page-content">

		<div class="page-title page-title-small">
	        <h2 id="title"><a href="?" data-back-button><i class="fa fa-arrow-left"></i></a>Location</h2>
	        <a href="#" data-menu="menu-main" class="bg-fade-gray1-dark shadow-xl preload-img" data-src="images/avatars/5s.png"></a>
	    </div>

	    <div class="card header-card shape-rounded" data-card-height="150">
	    	<div class="card-overlay bg-highlight opacity-95"></div>
	    	<div class="card-overlay dark-mode-tint"></div>
	    	<div class="card-bg preload-img" data-src="images/pictures/20s.jpg"></div>
		</div>

		<div class="card card-style">
            <div class="content mt-3 mb-3">

				<?php
				$ano = 1;
				$s1 = "SELECT distinct 	a.id_prov as id_prov, a.nama_prov as nama_prov
						from 11amasterprovince as a 
						inner join 21masterlocation as b 
							on a.id_prov = b.locationprovince
						order by nama_prov ASC";
				$dt = $fun->getData($s1, 2);
				foreach($dt as $dt) { ?>
					<div class="accordion" id="<?php echo 'accordion-'.$ano; ?>">
						<div class="mb-0">
							<button class="btn accordion-btn border-0"  data-toggle="collapse" data-target="<?php echo '#collapse-'.$ano; ?>">
		                        <?php echo $dt['nama_prov'];  ?>
		                        <i class="fa fa-chevron-down font-10 accordion-icon"></i>
		                    </button>
		                    <div id="<?php echo 'collapse-'.$ano; ?>" class="collapse"  data-parent="<?php echo '#accordion-'.$ano; ?>">
								<?php
								$s2 = "SELECT * from 21masterlocation where active = '1' and locationprovince='".$dt['id_prov']."' order by locationname ASC";
								$r2 = $fun->checkRowData($s2);
								if($r2 > 0) {
									$mno = 1;
									$dt2 = $fun->getData($s2, 2);
									foreach($dt2 as $dt2) { ?>
										<div class="pt-1 pb-2 pl-3 pr-3">
											<a href="#" data-menu="<?php echo $dt2['masterlocationid']; ?>">
												<?php echo $dt2['locationname']; ?>
											</a>
										</div>
										<?php
										$mno = 1;
									}
								}
								else { ?>
									<div class="pt-1 pb-2 pl-3 pr-3">[Data Not Available]</div>
									<?php
								}
								?>
							</div>
						</div>
					</div>
					<?php
					$ano++;
				}
				?>

			</div>
		</div>

		<div class="center mt-3 mb-4">
	    	<a href="?pg=location&sb=add" class="btn btn-m btn-center-xl bg-highlight txt-white rounded-sm text-uppercase font-900"><span class="ion-android-add font-21"></span></a>
	    </div>

	    <div class="divider divider-margins"></div>

	    <!-- footer and footer card-->
        <div class="footer" data-menu-load="footer.php"></div>  
	</div>

<?php
$mno = 1;
$sm1 = "SELECT 	a.masterlocationid as masterlocationid,
				a.locationname as locationname, 
				a.locationphone as locationphone,
				a.locationaddress as locationaddress,
				a.image as image,
				b.nama_prov as nama_prov,
				c.nama_kabkot as nama_kabkot
		from 21masterlocation as a 
		inner join 11amasterprovince as b 
			on a.locationprovince = b.id_prov 
		inner join 11bmastercity as c 
			on a.locationcity = c.id_kabkot
		where a.active='1' order by locationname ASC";
$dtm = $fun->getData($sm1, 2);
foreach($dtm as $dtm) { ?>
	<div id="<?php echo $dtm['masterlocationid']; ?>" 
             class="menu menu-box-modal menu-box-detached rounded-m" 
             data-menu-height="400" 
             data-menu-width="300" 
             data-menu-effect="menu-over">

        <h1 class="font-700 mt-3 ml-3"><?php echo $dtm['locationname']; ?></h1>

        <div class="center">
	        <?php
			if( !($dtm['image'] == '') || !empty($dtm['image']) ) { ?>
				<img src="<?php echo 'images/location/'.$dtm['image']; ?>">
				<?php
			}
			?>
		</div>

        <p class="boxed-text-xl justify mt-3" style="margin-left: 15px; margin-right: 15px;">
        	
            <span class="bold">Address : </span><br/>
            <span><?php echo $dtm['locationaddress'].', '.$dtm['nama_kabkot'].''.$dtm['nama_prov']; ?></span><br/>
            <br/>
            <span class="bold">Phone Number : </span>
            <span><?php echo $dtm['locationphone']; ?></span>
        </p>

        <a href="<?php echo '?pg=location&sb=edit&'.$fun->setIDParam('ID', $dtm['masterlocationid']) ?>" class="btn btn-m btn-center-xl bg-highlight txt-white rounded-sm text-uppercase font-900 mb-3">
			<i class="ion-android-create font-18"></i>
		</a>

		<div class="divider divider-margins"></div>

		<div class="row" style="margin-left: 5px; margin-right: 5px; margin-top: -20px;">
			<div class="col-4 center">
				<a href="<?php echo '?pg=product&'.$fun->setIDParam('lokasi', $dtm['masterlocationid']); ?>">
					<i class="ion-card"></i><br>
					Product
				</a>
			</div>

			<div class="col-4 center">
				<a href="<?php echo '?pg=compliment&'.$fun->setIDParam('lokasi', $dtm['masterlocationid']); ?>">
					<i class="ion-card"></i><br>
					Complimentary Product
				</a>
			</div>

			<div class="col-4 center">
				<a href="<?php echo '?pg=tenant&'.$fun->setIDParam('lokasi', $dtm['masterlocationid']); ?>">
					<i class="ion-android-people"></i><br>
					Tenant
				</a>
			</div>
		</div>
	</div>
	<?php
	$mno++;
}
?>
