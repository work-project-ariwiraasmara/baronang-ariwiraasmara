<?php
$sf1 = "SELECT * from 21masterlocation where masterlocationid='".$fun->getIDParam('ID')."'";
$df1 = $fun->getData($sf1);

$sf2 = "SELECT a.nama_prov as nama_prov, b.nama_kabkot as nama_kabkot
		from 11amasterprovince as a 
		inner join 11bmastercity as b 
			on a.id_prov = b.id_prov 
		where a.id_prov='".$df1[0]['locationprovince']."' and b.id_kabkot='".$df1[0]['locationcity']."'";
$df2 = $fun->getData($sf2);
?>

	<div class="page-content">

		<div class="page-title page-title-small">
	        <h2 id="title"><a href="?" data-back-button><i class="fa fa-arrow-left"></i></a>Edit Location</h2>
	        <a href="#" data-menu="menu-main" class="bg-fade-gray1-dark shadow-xl preload-img" data-src="images/avatars/5s.png"></a>
	    </div>

	    <div class="card header-card shape-rounded" data-card-height="150">
	    	<div class="card-overlay bg-highlight opacity-95"></div>
	    	<div class="card-overlay dark-mode-tint"></div>
	    	<div class="card-bg preload-img" data-src="images/pictures/20s.jpg"></div>
		</div>

		<form action="<?php echo 'process/41bupdatelocation.php?'.$fun->setIDParam('ID', $fun->getIDParam('ID')); ?>" method="post" enctype="multipart/form-data">
			<div class="card card-style">
            	<div class="content mb-0">

            		<div class="input-style input-style-1 input-required">
						<span>Name</span>
						<em>(required)</em>
						<input type="text" name="<?php echo $fun->Enlink('nama'); ?>" id="<?php echo $fun->Enlink('nama'); ?>" placeholder="Name" value="<?php echo $df1[0]['locationname']; ?>" required>
					</div>

					<div class="input-style input-style-1 input-required">
						<span>Email</span>
						<em>(required)</em>
						<input type="email" name="<?php echo $fun->Enlink('email'); ?>" id="<?php echo $fun->Enlink('email'); ?>" placeholder="Email" value="<?php echo $df1[0]['locationemail']; ?>" required>
					</div>


					<div class="input-style input-style-1 input-required">
						<span>Phone Number</span>
						<input type="number" name="<?php echo $fun->Enlink('tlp'); ?>" id="<?php echo $fun->Enlink('tlp'); ?>" placeholder="Phone Number" value="<?php echo $df1[0]['locationphone']; ?>">
					</div>

					<div class="input-style input-style-1 input-required">
						<span>Address</span>
						<input type="text" name="<?php echo $fun->Enlink('alamat'); ?>" id="<?php echo $fun->Enlink('alamat'); ?>" placeholder="Address" value="<?php echo $df1[0]['locationaddress']; ?>">
					</div>

					<div class="row">
						<div class="col-6">
							<span class="bold italic"><?php echo 'Province : '.$df2[0]['nama_prov']; ?></span> <span class="txt-link italic" id="choose_prov">[choose]</span>
							<div class="input-style input-style-1 input-required hide" id="select_prov">
								<span class="bold">Province :</span>
								<em><i class="fa fa-angle-down"></i></em>
								<select name="<?php echo $fun->Enlink('prov'); ?>" id="<?php echo $fun->Enlink('prov'); ?>" required>
									<?php
									$s3 = "SELECT * from 11amasterprovince order by id_prov";
									$d3 = $fun->getData($s3, 2);
									foreach ($d3 as $dt) { ?>
										<option value="<?php echo $fun->Enval($dt['id_prov']); ?>"><?php echo $dt['nama_prov']; ?></option>
										<?php
									}
									?>
								</select>
							</div>
						</div>

						<div class="col-6">
							<span class="bold italic"><?php echo 'Area : '.$df2[0]['nama_kabkot']; ?></span> <span class="txt-link italic" id="choose_kabkot">[choose]</span>
							<div class="input-style input-style-1 input-required hide" id="select_kabkot">
								<span class="bold">Area :</span>
								<em><i class="fa fa-angle-down"></i></em>
								<select name="<?php echo $fun->Enlink('kabkot'); ?>" id="<?php echo $fun->Enlink('kabkot'); ?>" required>
									<option value="">Choose province first...</option>
								</select>
							</div>
						</div>
					</div>

					<div style="margin-top: -25px;">
						<span>Image</span><br/>
						<input type="file" name="<?php echo $fun->Enlink('gambar'); ?>" id="<?php echo $fun->Enlink('gambar'); ?>">
					</div>

					<div class="input-style input-style-1 input-required mt-3">
						<span>Status :</span>
						<em><i class="fa fa-angle-down"></i></em>
						<select name="<?php echo $fun->Enlink('status'); ?>" id="<?php echo $fun->Enlink('status'); ?>">
							<option value="<?php echo $fun->Enval('0'); ?>" <?php if($df1[0]['active'] == '0') echo 'selected'; ?> >Not Active</option>
							<option value="<?php echo $fun->Enval('1'); ?>" <?php if($df1[0]['active'] == '1') echo 'selected'; ?> >Active</option>
						</select>
					</div>

					<button type="submit" name="confirm" id="confirm" class="btn btn-m btn-center-xl bg-highlight txt-white rounded-sm text-uppercase font-900 widt-100 mt-3 mb-3">OK</button>
				</div>
			</div>
		</form>

		<div class="divider divider-margins"></div>

		<!-- footer and footer card-->
        <div class="footer" data-menu-load="footer.php"></div>  
	</div>

<script type="text/javascript">
	$("#choose_prov").click(function(){
		if( $("#select_prov").hasClass("hide") ) {
			$("#select_prov").removeClass("hide");
		}
		else {
			$("#select_prov").addClass("hide");
		}
	});

	$("#choose_kabkot").click(function(){
		if( $("#select_kabkot").hasClass("hide") ) {
			$("#select_kabkot").removeClass("hide");
		}
		else {
			$("#select_kabkot").addClass("hide");
		}
	});
</script>