<?php
$scmpy = "SELECT * from 12mastercompany where companyid='71'";
$dcmpy = $fun->getData($scmpy);

$s1 = "SELECT locationname from 21masterlocation where masterlocationid='".$fun->getIDParam('lokasi')."'";
$d1 = $fun->getData($s1);
?>

	<div class="page-content">

		<div class="page-title page-title-small">
	        <h2 id="title"><a href="?" data-back-button><i class="fa fa-arrow-left"></i></a>Add Tenant</h2>
	        <a href="#" data-menu="menu-main" class="bg-fade-gray1-dark shadow-xl preload-img" data-src="images/avatars/5s.png"></a>
	    </div>

	    <div class="card header-card shape-rounded" data-card-height="150">
	    	<div class="card-overlay bg-highlight opacity-95"></div>
	    	<div class="card-overlay dark-mode-tint"></div>
	    	<div class="card-bg preload-img" data-src="images/pictures/20s.jpg"></div>
		</div>

		<form action="process/43aaddmember.php" method="post">
			<div class="card card-style">
            	<div class="content mb-0">

            		<input type="text" name="<?php echo $fun->Enlink('lokasi'); ?>" id="<?php echo $fun->Enlink('lokasi'); ?>" class="hide" value="<?php echo $fun->getIDParam('lokasi'); ?>">
					<h3 class="bold txt-black"><?php echo 'Location : '.$d1[0]['locationname']; ?></h3>

					<div class="input-style input-style-1 input-required">
						<span>Name</span>
						<em>(required)</em>
						<input type="text" name="<?php echo $fun->Enlink('nama'); ?>" id="<?php echo $fun->Enlink('nama'); ?>" placeholder="Name" required>
					</div>

					<?php
					if( ($dcmpy[0]['labeltier1'] != '') || !empty($dcmpy[0]['labeltier1']) ) { ?>
						<div class="input-style input-style-1 input-required">
							<span><?php echo $dcmpy[0]['labeltier1'] ?></span>
							<input type="text" name="<?php echo $fun->Enlink('tier1'); ?>" id="<?php echo $fun->Enlink('tier1'); ?>" placeholder="<?php echo $dcmpy[0]['labeltier1'] ?>">
						</div>
						<?php
					}

					if( ($dcmpy[0]['labeltier2'] != '') || !empty($dcmpy[0]['labeltier2']) ) { ?>
						<div class="input-style input-style-1 input-required">
							<span><?php echo $dcmpy[0]['labeltier2'] ?></span>
							<input type="text" name="<?php echo $fun->Enlink('tier2'); ?>" id="<?php echo $fun->Enlink('tier2'); ?>" placeholder="<?php echo $dcmpy[0]['labeltier2'] ?>">
						</div>
						<?php
					}

					if( ($dcmpy[0]['labeltier3'] != '') || !empty($dcmpy[0]['labeltier3']) ) { ?>
						<div class="input-style input-style-1 input-required">
							<span><?php echo $dcmpy[0]['labeltier3'] ?></span>
							<input type="text" name="<?php echo $fun->Enlink('tier3'); ?>" id="<?php echo $fun->Enlink('tier3'); ?>" placeholder="<?php echo $dcmpy[0]['labeltier3'] ?>">
						</div>
						<?php
					}

					if( ($dcmpy[0]['labeltier4'] != '') || !empty($dcmpy[0]['labeltier4']) ) { ?>
						<div class="input-style input-style-1 input-required">
							<span><?php echo $dcmpy[0]['labeltier4']; ?></span>
							<input type="text" name="<?php echo $fun->Enlink('tier4'); ?>" id="<?php echo $fun->Enlink('tier4'); ?>" placeholder="<?php echo $dcmpy[0]['labeltier4']; ?>">
						</div>
						<?php
					}

					if( ($dcmpy[0]['labeltier5'] != '') || !empty($dcmpy[0]['labeltier5']) ) { ?>
						<div class="input-style input-style-1 input-required">
							<span><?php echo $dcmpy[0]['labeltier5']; ?></span>
							<input type="text" name="<?php echo $fun->Enlink('tier5'); ?>" id="<?php echo $fun->Enlink('tier5'); ?>" placeholder="<?php echo $dcmpy[0]['labeltier5']; ?>">
						</div>
						<?php
					}
					?>

					<div class="input-style input-style-1 input-required">
						<span>Phone Number</span>
						<input type="number" name="<?php echo $fun->Enlink('tlp'); ?>" id="<?php echo $fun->Enlink('tlp'); ?>" placeholder="Phone Number">
					</div>

					<button type="submit" name="confirm" id="confirm" class="btn btn-m btn-center-xl bg-highlight txt-white rounded-sm text-uppercase font-900 widt-100 mt-3 mb-3">Save</button>
				</div>
			</div>
		</form>

		<div class="divider divider-margins"></div>

	    <!-- footer and footer card-->
        <div class="footer" data-menu-load="footer.php"></div>  
	</div>