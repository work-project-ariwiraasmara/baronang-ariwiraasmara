<?php
$sloc = "SELECT a.locationname as locationname, a.locationaddress as locationaddress,
				b.nama_prov as nama_prov, c.nama_kabkot as nama_kabkot
		from 21masterlocation as a 
		inner join 11amasterprovince as b 
			on a.locationprovince = b.id_prov
		inner join 11bmastercity as c
			on a.locationcity = c.id_kabkot
		where a.masterlocationid='".$fun->getIDParam('lokasi')."'";
$dloc = $fun->getData($sloc);
?>

	<div class="page-content">

		<div class="page-title page-title-small">
	        <h2 id="title"><a href="?" data-back-button><i class="fa fa-arrow-left"></i></a>Tenant</h2>
	        <a href="#" data-menu="menu-main" class="bg-fade-gray1-dark shadow-xl preload-img" data-src="images/avatars/5s.png"></a>
	    </div>

	    <div class="card header-card shape-rounded" data-card-height="150">
	    	<div class="card-overlay bg-highlight opacity-95"></div>
	    	<div class="card-overlay dark-mode-tint"></div>
	    	<div class="card-bg preload-img" data-src="images/pictures/20s.jpg"></div>
		</div>

		<div class="card card-style">
	    	<p class="content txt-black">
	    		<span class="bold"><?php echo 'Location : '.$dloc[0]['locationname']; ?></span><br>
	    		<span><?php echo $dloc[0]['locationaddress'].', '.$dloc[0]['nama_kabkot'].', '.$dloc[0]['nama_prov']; ?></span>
	    	</p>
	    </div>

		<div class="card card-style">
            <div class="content">
                <div class="list-group list-custom-small">
					<?php
					$stenant = "SELECT * from 23dmastertenant where status < 3 and locationid='".$fun->getIDParam('lokasi')."' order by mastertenantid";
					//echo $s1;
					$dtenant = $fun->getData($stenant, 2);
					foreach ($dtenant as $dt) { ?>
						<a href="#" data-menu="<?php echo $fun->Enlink($dt['mastertenantid']); ?>">
							<span><?php echo $dt['tenantname']; ?></span>
						</a>
						<?php
					}
					?>
				</div>
			</div>
		</div>

		<div class="center mt-3 mb-4">
	    	<a href="#" data-menu="<?php echo $fun->Enlink('modal_act'); ?>" class="btn btn-m btn-center-xl bg-highlight txt-white rounded-sm text-uppercase font-900"><span class="ion-android-add font-21"></span></a>
	    </div>

	    <div class="divider mt-4 mb-3"></div>

	    <!-- footer and footer card-->
        <div class="footer" data-menu-load="footer.php"></div>  
	</div>

<div id="<?php echo $fun->Enlink('modal_act'); ?>" 
    	 class="menu menu-box-modal menu-box-detached rounded-m" 
         data-menu-height="155" 
         data-menu-width="250" 
         data-menu-effect="menu-over">
    <div class="list-group list-custom-small pl-1 pr-3">

		<a href="#" data-menu="import_tenant">
			<i class="ion-ios-cloud-upload"></i> 
			<span>Import</span>
			<i class="fa fa-angle-right"></i>
		</a>
		<a href="<?php echo '#';//'?pg=tenant&sb=template&download=excel&'.$fun->setIDParam('lokasi', $fun->getIDParam('lokasi')); ?>">
			<i class="ion-ios-cloud-download"></i> 
			<span>Download Template</span>
			<i class="fa fa-angle-right"></i>
		</a>

		<a href="<?php echo '?pg=tenant&sb=add&'.$fun->setIDParam('lokasi', $fun->getIDParam('lokasi')); ?>">
			<i class="ion-android-create"></i> 
			<span>New Data</span>
			<i class="fa fa-angle-right"></i>
		</a>
	
	</div>
</div>

<div id="import_tenant" 
    	 class="menu menu-box-modal menu-box-detached rounded-m" 
         data-menu-height="160" 
         data-menu-width="250" 
         data-menu-effect="menu-over">
	<form action="<?php echo 'process/43cimportmember.php?'.$fun->setIDParam('lokasi', $fun->getIDParam('lokasi')); ?>" method="post" enctype="multipart/form-data">
		<div style="padding: 20px;">
			<span class="color-highlight">File (required)</span>
			<input type="file" name="filetenant" id="filetenant" placeholder="">
		</div> 

		<div style="margin-right: 20px; margin-left: 20px;">
			<button type="submit" name="ok" id="ok" class="btn btn-m btn-center-xl bg-highlight txt-white rounded-sm text-uppercase font-900 center width-100">Import</button>
		</div>
	</form>
</div>

<?php
$dmtenant = $fun->getData($stenant, 2);
foreach($dmtenant as $dt) { 

	$s2 = "SELECT b.nama_prov as nama_prov, c.nama_kabkot as nama_kabkot
			from 11amasterprovince as b 
			inner join 11bmastercity as c
				on b.id_prov = c.id_prov
			where b.id_prov='".$dt['province']."' and c.id_kabkot='".$dt['city']."'";
	$d2 = $fun->getData($s2);

	if($dt['tenantaddess'] == '' || empty($dt['tenantaddess']) || is_null($dt['tenantaddess'])) {
		$address = '';
	}
	else {
		$address = $dt['tenantaddess'].', '.$d2[0]['nama_kabkot'].', '.$d2[0]['nama_prov'];
	}

	?>
	<div id="<?php echo $fun->Enlink($dt['mastertenantid']); ?>" 
    	 class="menu menu-box-modal rounded-m" 
         data-menu-height="450" 
         data-menu-width="300" 
         data-menu-effect="menu-over">

        <h1 class="font-700 mt-3" style="margin-left: 15px; margin-right: 15px;"><?php echo $dt['tenantname']; ?></h1>

        <p class="boxed-text-xl justify mt-3" style="margin-left: 15px; margin-right: 15px;">
			<span class="bold">Email :</span><br/>
			<span><?php echo $dt['tenantemail']; ?></span><br/><br/>

			<span class="bold">Address :</span><br/>
			<span><?php echo $address; ?></span><br/>
			
			<span class="bold">Phone Number :</span><br/>
			<span><?php echo $dt['tenantphone']; ?></span>
		</p>

		<div class="divider mt-4 mb-3"></div>

		<a href="<?php echo '?pg=tenant&sb=edit&'.$fun->setIDParam('lokasi', $fun->getIDParam('lokasi')).'&'.$fun->setIDParam('ID', $dt['mastertenantid']); ?>" class="btn btn-m btn-center-xl bg-highlight txt-white rounded-sm text-uppercase font-900 center">
			<i class="ion-android-create font-21"></i>
		</a>
	</div>
	<?php
}
?>
