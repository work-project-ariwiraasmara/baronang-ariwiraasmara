<?php
require('plugins/TCPDF-master/tcpdf.php');
$lokasi = $fun->getIDParam('lokasi');
$jt = $fun->getIDParam('jumlah_tenant');

if( $jt > 0 ) {
	echo 'jumlah tenant : '.$jt.'<br><br><br>';

	$s1 = "SELECT * from 21masterlocation where masterlocationid='$lokasi'";
	$q1 = mysqli_query($fun->getConnection(), $s1) or die(mysqli_error($fun->getConnection()));
	$d1 = mysqli_fetch_array($q1);

	$s2 = "SELECT a.nama_prov as nama_prov, b.nama_kabkot as nama_kabkot 
			from 11amasterprovince as a 
			inner join 11bmastercity as b 
				on a.id_prov = b.id_prov 
			where a.id_prov='".$d1['locationprovince']."' and b.id_kabkot='".$d1['locationcity']."'";
	$q2 = mysqli_query($fun->getConnection(), $s2) or die(mysqli_error($fun->getConnection()));
	$d2 = mysqli_fetch_array($q2);

	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('Baronang');
	$pdf->SetTitle('Member Account '.$d1['locationname']);
	$pdf->SetSubject($d1['locationname'].', '.$d2['nama_kabkot'].', '.$d2['nama_prov']);

	$pdf->setFooterData(array(0,64,0), array(0,64,128));

	// set header and footer fonts
	//$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
	$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

	// set default monospaced font
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

	// set margins
	$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
	//$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

	// set auto page breaks
	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

	// set image scale factor
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

	// set default font subsetting mode
	$pdf->setFontSubsetting(true);

	// Set font
	// dejavusans is a UTF-8 Unicode font, if you only need to
	// print standard ASCII chars, you can use core fonts like
	// helvetica or times to reduce file size.
	$pdf->SetFont('dejavusans', '', 12, '', true);

	$pdf->AddPage();

	if($jt > 1) {
		$html = "";

		for($x = 1; $x <= $jt; $x++) {
			$html .= "#No. [".$x."]<br>";
			$html .= "Email : ".$_SESSION['email'][$x]."<br>";
			$html .= "Password : ".$_SESSION['pass'][$x]."<br>";
			$html .= "========================================<br><br>";
		}
	}
	else {
		$html = "Email : ".$fun->getSession('tenant-email', 2)."<br>Password : ".$fun->getSession('tenant-pass', 2);
	}

	// Print text using writeHTMLCell()
	$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

	$pdffilename = 'Akun Member '.$d1['locationname'];

	ob_end_clean();
	$pdf->Output($pdffilename.'.pdf', 'I');
	//header('location: ?pg=tenant&'.$fun->getIDParam('lokasi', $lokasi).'&'.$fun->setIDParam('status', 'Berhasil Tambah Tenant'));
}
else {
	echo 'jumlah tenant : 0';
	//header('location: ?pg=tenant&'.$fun->getIDParam('lokasi', $lokasi).'&'.$fun->setIDParam('status', 'Berhasil Tambah Tenant'));
}
?>