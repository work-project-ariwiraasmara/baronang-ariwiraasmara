<?php
require('../util/koneksi.php');

require('../plugins/qrcode/qrlib.php');

//barcode
include('../plugins/barcode128/BarcodeGenerator.php');
include('../plugins/barcode128/BarcodeGeneratorPNG.php');
include('../plugins/barcode128/BarcodeGeneratorSVG.php');
include('../plugins/barcode128/BarcodeGeneratorJPG.php');
include('../plugins/barcode128/BarcodeGeneratorHTML.php');

function QRCode($path, $kode, $cp, $matrixPointSize) {
	//QR code

	$errorCorrectionLevel = 'H';
	//$matrixPointSize = 500;

	//$kode = '1000100010001004';
	$filename = $path.$kode.'.png';
	if (!file_exists($filename)){
		QRcode::png($cp, $filename, $errorCorrectionLevel, $matrixPointSize, 2);
	}

	$generator = new Picqer\Barcode\BarcodeGeneratorPNG();
	$resource = 'data:image/png;base64,'.base64_encode($generator->getBarcode($kode, $generator::TYPE_CODE_128));
}

$qridlike = '7103'.date('y').'100'.date('m');
$sbqr = "SELECT qrcodelist from 41qrcodelist where qrcodelist like '$qridlike%' order by qrcodelist desc limit 1";
$qbqr = mysqli_query($fun->getConnection(), $sbqr) or die(mysqli_error($fun->getConnection()));
$rbqr = mysqli_num_rows($qbqr);
if($rbqr > 0) {
	$dbqr = mysqli_fetch_array($qbqr);
	$lastqrid = (int)substr($dbqr['qrcodelist'], -5) + 1;
}
else {
	$lastqrid = 1;
}

$jml_no = $fun->POST('jml_no', 1);
$jml_data = (int)$jml_no - 1;
echo 'Jumlah Data : '.$jml_data.'<br><br>';
for($no = 1; $no < $jml_no; $no++) {
//foreach ($variable as $key => $value) {
	// echo '========== [START#'.$no.'] ==========<br><br>';
	echo '#No. ['.$no.']<br>';
	$date = date('Y-m-d H:i:s');

	$tid 		 = $fun->Denval(@$_POST[$fun->Enlink('tid')][$no]);
	$email 		 = $fun->Denval(@$_POST[$fun->Enlink('email')][$no]);
	$pass 		 = $fun->Denval(@$_POST[$fun->Enlink('pass')][$no]);
	$enpass 	 = $fun->ENID($pass);
	
	$sloc = "SELECT locationid from 23dmastertenant where mastertenantid = '$tid'";
	$qloc = mysqli_query($fun->getConnection(), $sloc) or die(mysqli_error($fun->getConnection()));
	$dloc = mysqli_fetch_array($qloc);
	$lokasi = $dloc['locationid'];
	
	// GET ID PRODUCT & VALIDITY DAYS FOR MOTORCYCLE
	$sprodmotor = "SELECT masterproductid, validity_days from 22amasterproduct where locationid='$lokasi' and vehicletype='Motorcycle' and productprice='0'";
	$qprodmotor = mysqli_query($fun->getConnection(), $sprodmotor) or die(mysqli_error($fun->getConnection()));
	$dprodmotor = mysqli_fetch_array($qprodmotor);
	$prodmotor  = $dprodmotor['masterproductid']; 
	$validmotor = $dprodmotor['validity_days'];

	// GET SERIAL NUMBER AND LAST COUNTER FOR MOTORCYCLE
	$seriprodmotor = substr($prodmotor, 0, 14);
	$ssprodmotor = "SELECT productserialnumber from 22fproductserialnumber where productserialnumber like '$seriprodmotor%' order by productserialnumber desc limit 1";
	$qsprodmotor = mysqli_query($fun->getConnection(), $ssprodmotor) or die(mysqli_error($fun->getConnection()));
	$rsprodmotor = mysqli_num_rows($qsprodmotor);
	if($rsprodmotor > 0) {
		// 71021010411001 000000
		$dsprodmotor = mysqli_fetch_array($qsprodmotor);
		$csprodmotor = (int)substr($dsprodmotor, -6) + 1;
	}
	else {
		$csprodmotor = 1;
	}
	

	// GET ID PRODUCT & VALIDITY DAYS FOR SMALL CAR
	$sprodmobil1 = "SELECT masterproductid, validity_days from 22amasterproduct where locationid='$lokasi' and vehicletype='Small Car' and productprice='0'";
	$qprodmobil1 = mysqli_query($fun->getConnection(), $sprodmobil1) or die(mysqli_error($fun->getConnection()));
	$dprodmobil1 = mysqli_fetch_array($qprodmobil1);
	$prodmobil1  = $dprodmobil1['masterproductid']; 
	$validmobil1 = $dprodmobil1['validity_days']; 

	// GET SERIAL NUMBER AND LAST COUNTER SMALL CAR
	$seriprodmobil1 = substr($prodmobil1, 0, 14);
	$ssprodmobil1 = "SELECT productserialnumber from 22fproductserialnumber where productserialnumber like '$seriprodmobil1%' order by productserialnumber desc limit 1";
	$qsprodmobil1 = mysqli_query($fun->getConnection(), $ssprodmobil1) or die(mysqli_error($fun->getConnection()));
	$rsprodmobil1 = mysqli_num_rows($qsprodmobil1);
	if($rsprodmobil1 > 0) {
		// 71021010411001 000000
		$dsprodmobil1 = mysqli_fetch_array($qsprodmobil1);
		$csprodmobil1 = (int)substr($dsprodmobil1, -6) + 1;
	}
	else {
		$csprodmobil1 = 1;
	}


	// GET ID PRODUCT & VALIDITY DAYS FOR BIG CAR
	$sprodmobil2 = "SELECT masterproductid, validity_days from 22amasterproduct where locationid='$lokasi' and vehicletype='Big Car' and productprice='0'";
	$qprodmobil2 = mysqli_query($fun->getConnection(), $sprodmobil2) or die(mysqli_error($fun->getConnection()));
	$dprodmobil2 = mysqli_fetch_array($qprodmobil2);
	$prodmobil2  = $dprodmobil2['masterproductid']; 
	$validmobil2 = $dprodmobil2['validity_days']; 

	// GET SERIAL NUMBER AND LAST COUNTER BIG CAR
	$seriprodmobil2 = substr($prodmobil2, 0, 14);
	$ssprodmobil2 = "SELECT productserialnumber from 22fproductserialnumber where productserialnumber like '$seriprodmobil2%' order by productserialnumber desc limit 1";
	$qsprodmobil2 = mysqli_query($fun->getConnection(), $ssprodmobil2) or die(mysqli_error($fun->getConnection()));
	$rsprodmobil2 = mysqli_num_rows($qsprodmobil2);
	if($rsprodmobil2 > 0) {
		// 71021010411001 000000
		$dsprodmobil2 = mysqli_fetch_array($qsprodmobil2);
		$csprodmobil2 = (int)substr($dsprodmobil2, -6) + 1;
	}
	else {
		$csprodmobil2 = 1;
	}


	$motor  	 = @$_POST[$fun->Enlink('qtymotor')][$no];
	//$prodmotor 	 = $fun->Denval(@$_POST[$fun->Enlink('prodmotor')][$no]);
	//$validmotor	 = $fun->Denval(@$_POST[$fun->Enlink('validmotor')][$no]);

	$mobil1 	 = @$_POST[$fun->Enlink('qtymobil1')][$no];
	//$prodmobil1	 = $fun->Denval(@$_POST[$fun->Enlink('prodmobil1')][$no]);
	//$validmobil1 = $fun->Denval(@$_POST[$fun->Enlink('validmobil1')][$no]);

	$mobil2 	 = @$_POST[$fun->Enlink('qtymobil2')][$no];
	//$prodmobil2	 = $fun->Denval(@$_POST[$fun->Enlink('prodmobil2')][$no]);
	//$validmobil2 = $fun->Denval(@$_POST[$fun->Enlink('validmobil2')][$no]);

	//$nocard 	 = $fun->Denval(@$_POST[$fun->Enlink('nocard')][$no]);
	$status 	 = $fun->Denval(@$_POST[$fun->Enlink('tenant_approval')][$no]);

	if($status == 0 || empty($status)) {
		$status = 4;
	}

	
	$s1 = "UPDATE 23dmastertenant set 	tenantemail = '$email', 
										tenantpassword = '$enpass',
										allowancemotorcycle = '$motor',
										allowancecarsmall = '$mobil1',
										allowancecarbig = '$mobil2',
										status = '$status'
			where mastertenantid = '$tid'";
	$q1 =  mysqli_query($fun->getConnection(), $s1) or die(mysqli_error($fun->getConnection()));


	if($status == 2) {
		$scard = "SELECT mastercardid from 23amastercard where memberid='$tid' order by mastercardid DESC limit 1";
		$qcard = mysqli_query($fun->getConnection(), $scard) or die(mysqli_error($fun->getConnection()));
		$rcard = mysqli_num_rows($qcard);


		// GENERATE NOMOR KARTU
		if($rcard > 0) {
			$dcard = mysqli_fetch_array($qcard);
			$nocard = (int)substr($dcard['mastercardid'], -3) + 1;
		}
		else {
			$nocard = '001';
		}


		//  GET ID PRODUCT OWNED
		$s3 = "SELECT activitylogid from 31amastercardproductowned order by activitylogid DESC";
		$q3 = mysqli_query($fun->getConnection(), $s3) or die(mysqli_error($fun->getConnection()));
		$d3 = mysqli_fetch_array($q3);
		$idcpo = (int)$d3['activitylogid'] + 1;

		// echo 'Tenant ID : '.$tid.'<br>';
		echo 'Email : '.$email.'<br>';
		echo 'Pass : '.$pass.'<br>';

		//echo 'Motor : '.$motor.'<br>';
		// echo 'Produk : '.$prodmotor.'<br>';
		// echo 'Valid Days : '.$validmotor.'<br><br>';

		//echo 'Mobil Kecil : '.$mobil1.'<br>';
		// echo 'Produk : '.$prodmobil1.'<br>';
		// echo 'Valid Days : '.$validmobil1.'<br><br>';


		//echo 'Mobil Besar : '.$mobil2.'<br>';
		// echo 'Produk : '.$prodmobil2.'<br>';
		// echo 'Valid Days : '.$validmobil2.'<br><br>';

		//echo 'Status : '.$status.'<br><br>';


		// CREATE QR CODE AND INSERT INTO TABLE qrcodelist
		$qrparam = 'id='.$tid.'&email='.$email.'&pass='.$pass;
		$qrcodelist = $qridlike.str_pad($lastqrid,5,"0", STR_PAD_LEFT);
		$sibqr = "INSERT into 41qrcodelist values('$qrcodelist', '$date', '".$fun->getValookie('id')."', '$qrparam', '1', '100.php')";
		//echo $sibqr;
		$qibqr = mysqli_query($fun->getConnection(), $sibqr) or die(mysqli_error($fun->getConnection()));
		QRCode('qrtenant_'.$fun->getValookie('id').'/', $qrcodelist, $qrcodelist, 5);


		echo '<img src="qrtenant_'.$fun->getValookie('id').'/'.$qrcodelist.'.png">';
		echo '<br>';
		
		$s2 = "INSERT into 23amastercard values "; 
		$s4 = "INSERT into 31amastercardproductowned values ";
		$s5 = "INSERT into 22fproductserialnumber values ";

		// echo '======= MOTOR =======<br>';
		for($xmtr = 0; $xmtr < $motor; $xmtr++) {
			$finocard = substr($tid, 0, 13).str_pad($nocard, 3, "0", STR_PAD_LEFT);
			// echo 'No. Kartu (Motor) : '.$finocard.'<br>';

			// TAMBAH KARTU
			$s2 .= "('$finocard', '$tid', '$date', 'Motor', '0', '1'),";

			// TAMBAH SERIAL NUMBER
			$idserialmotor = $seriprodmotor.str_pad($csprodmotor, 6, "0", STR_PAD_LEFT);
			$s5 .= "('$idserialmotor', '$prodmotor', '$date', 1),";

			// TAMBAH PRODUCT OWNED
			$expvalid = date('Y-m-d H:i:s', (time() + ($validmotor * 24 * 60 * 60)) );
			$s4 .= "('$idcpo', '$date', '$finocard', '$prodmotor', '$date', '$expvalid', '$idserialmotor', 1,0,0),";

			$nocard++; $idcpo++; $csprodmotor++;
		}

		// echo '<br>';
		// echo '======= MOBILE KECIL =======<br>';
		for($xmbl1 = 0; $xmbl1 < $mobil1; $xmbl1++) {
			$finocard = substr($tid, 0, 13).str_pad($nocard, 3, "0", STR_PAD_LEFT);
			// echo 'No. Kartu (Mobil Kecil) : '.$finocard.'<br>';

			// TAMBAH KARTU
			$s2 .= "('$finocard', '$tid', '$date', 'Mobil Kecil', '0', '1'),";

			// TAMBAH SERIAL NUMBER
			$idserialmobil1 = $seriprodmobil1.str_pad($csprodmobil1, 6, "0", STR_PAD_LEFT);
			$s5 .= "('$idserialmobil1', '$prodmobil1', '$date', 1),";

			// TAMBAH PRODUCT OWNED
			$expvalid = date('Y-m-d H:i:s', (time() + ($validmobil1 * 24 * 60 * 60)) );
			$s4 .= "('$idcpo', '$date', '$finocard', '$prodmobil1', '$date', '$expvalid', '$idserialmobil1', 1,0,0),";
			
			$nocard++; $idcpo++; $csprodmobil1++;
		}

		// echo '<br>';
		// echo '======= MOBIL BESAR =======<br>';
		for($xmbl2 = 0; $xmbl2 < $mobil2; $xmbl2++) {
			$finocard = substr($tid, 0, 13).str_pad($nocard, 3, "0", STR_PAD_LEFT);
			// echo 'No. Kartu (Mobil Besar) : '.$finocard.'<br>';

			// TAMBAH KARTU
			$s2 .= "('$finocard', '$tid', '$date', 'Mobil Besar', '0', '1'),";

			// TAMBAH SERIAL NUMBER
			$idserialmobil2 = $seriprodmobil2.str_pad($csprodmobil2, 6, "0", STR_PAD_LEFT);
			$s5 .= "('$idserialmobil2', '$prodmobil2', '$date', 1),";

			// TAMBAH PRODUCT OWNED
			$expvalid = date('Y-m-d H:i:s', (time() + ($validmobil2 * 24 * 60 * 60)) );
			$s4 .= "('$idcpo', '$date', '$finocard', '$prodmobil2', '$date', '$expvalid', '$idserialmobil2', 1,0,0),";

			$nocard++; $idcpo++; $csprodmobil2++;
		}

		// echo '<br>';

		$length_s2 = (int)strlen($s2);
		$length_s4 = (int)strlen($s4);
		$length_s5 = (int)strlen($s5);

		$s2 = substr($s2, 0, $length_s2-1).';';
		echo $s2.'<br>';
		//$q2 =  mysqli_query($fun->getConnection(), $s2) or die(mysqli_error($fun->getConnection()));

		$s4 = substr($s4, 0, $length_s4-1).';';
		echo $s4.'<br><br>';
		//$q4 =  mysqli_query($fun->getConnection(), $s4) or die(mysqli_error($fun->getConnection()));

		$s5 = substr($s5, 0, $length_s5-1).';';
		echo $s5.'<br><br>';
		//$q5 =  mysqli_query($fun->getConnection(), $s5) or die(mysqli_error($fun->getConnection()));

	}
	echo '<br>';

	// echo '========== [END#'.$no.'] ==========<br><br><br><br>';
	echo '====================<br><br>';

	$lastqrid++;
}
?>

<center>
	<div style="margin-top: 100px;">
		The page will redirect within 60 seconds, or click the back button<br>
		<button id="btn_print" onclick="window.print();">Print</button>
		<a href="<?php echo '../?pg=tenant&'.$fun->setIDParam('lokasi', $fun->getIDParam('lokasi')); ?>"><button>Back</button></a>
	</div>
</center>

<script type="text/javascript">
	/*
	setTimeout(function(){
		window.location.href = "<?php echo '../?pg=tenant&'.$fun->setIDParam('lokasi', $lokasi).'&'.$fun->setIDParam('status', 'Successful add tenant!'); ?>"
	}, 60000);
	*/
</script>
