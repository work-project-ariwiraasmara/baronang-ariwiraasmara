<?php
require('../util/koneksi.php');

$amount = $fun->POST('amount', 1);

if( $amount == '' || empty($amount) ) {
	header('location: ../index.php?pg=card&sb=topup&'.$fun->setIDParam('status', 'Amount can\'t be empty!<br>Try again!'));
}
else {

	$cardid = $fun->getIDParam('cardid');
	$cid = $fun->getIDParam('company');
	$productid = $fun->getIDParam('productid');

	$s1 = "SELECT * from 12mastercompany where companyid='71'";
	$q1 = mysqli_query($fun->getConnection(), $s1) or die(mysqli_error($fun->getConnection()));
	$d1 = mysqli_fetch_array($q1);
	$cid = $d1['companyid'];

	$s2 = "SELECT * from 31ecardvanumber where cardid='$cardid'";
	$q2 = mysqli_query($fun->getConnection(), $s2) or die(mysqli_error($fun->getConnection()));
	$d2 = mysqli_fetch_array($q2);
	$van = $d2['vanumber'];

	$curl = curl_init();
	$post = "CID=$cid&CardID=$cardid&ProductID=$productid";
	curl_setopt_array($curl, array(
		CURLOPT_URL => "https://baronang.com/apibap/api/baronang/PurchasePoint?",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "POST",
		CURLOPT_POSTFIELDS => $post,
	));
	$output = curl_exec($curl);
	$err = curl_error($curl);
	curl_close($curl);
	$result = json_decode($output, true);

	if($result['statusCode'] == '101') {
		header('location: ../index.php?pg=card&sb=topup&'.$fun->setIDParam('cardid', $cardid).'&'.$fun->setIDParam('lokasi', $fun->getIDParam('lokasi')).'&'.$fun->setIDParam('status', 'Success Top Up!'));
	}
	else {
		header('location: ../index.php?pg=card&sb=topup&'.$fun->setIDParam('cardid', $cardid).'&'.$fun->setIDParam('lokasi', $fun->getIDParam('lokasi')).'&'.$fun->setIDParam('status', $result['message']));
	}

}



?>
