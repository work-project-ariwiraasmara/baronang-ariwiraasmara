<?php
require('../util/koneksi.php');

require('../plugins/qrcode/qrlib.php');

//barcode
include('../plugins/barcode128/BarcodeGenerator.php');
include('../plugins/barcode128/BarcodeGeneratorPNG.php');
include('../plugins/barcode128/BarcodeGeneratorSVG.php');
include('../plugins/barcode128/BarcodeGeneratorJPG.php');
include('../plugins/barcode128/BarcodeGeneratorHTML.php');

function QRCode($path, $kode, $cp, $matrixPointSize) {
	//QR code

	$errorCorrectionLevel = 'H';
	//$matrixPointSize = 500;

	//$kode = '1000100010001004';
	$filename = $path.$kode.'.png';
	if (!file_exists($filename)){
		QRcode::png($cp, $filename, $errorCorrectionLevel, $matrixPointSize, 2);
	}

	$generator = new Picqer\Barcode\BarcodeGeneratorPNG();
	$resource = 'data:image/png;base64,'.base64_encode($generator->getBarcode($kode, $generator::TYPE_CODE_128));
}

$qridlike = '7103'.date('y').'100'.date('m');
$sbqr = "SELECT qrcodelist from 41qrcodelist where qrcodelist like '$qridlike%' order by qrcodelist desc limit 1";
$qbqr = mysqli_query($fun->getConnection(), $sbqr) or die(mysqli_error($fun->getConnection()));
$rbqr = mysqli_num_rows($qbqr);
if($rbqr > 0) {
	$dbqr = mysqli_fetch_array($qbqr);
	$lastqrid = (int)substr($dbqr['qrcodelist'], -5) + 1;
}
else {
	$lastqrid = 1;
}

$jml_no = $fun->POST('jml_no', 1);

//  GET ID PRODUCT OWNED
$sidprodown = "SELECT activitylogid from 31amastercardproductowned order by activitylogid DESC";
$qidprodown = mysqli_query($fun->getConnection(), $sidprodown) or die(mysqli_error($fun->getConnection()));
$didprodown = mysqli_fetch_array($qidprodown);
$idcpo = (int)$didprodown['activitylogid'] + 1;

$suno = 1;
for($no = 1; $no < $jml_no; $no++) {

	$lokasi = $fun->Denval(@$_POST[$fun->Enlink('lokasi')][$no]);
	$status = $fun->Denval(@$_POST[$fun->Enlink('tenant_approval')][$no]);

	if($status == 0 || empty($status)) {
		$status = 4;
	}

	$s1 = "SELECT * from 12mastercompany where companyid='71'";
	$q1 = mysqli_query($fun->getConnection(), $s1) or die(mysqli_error($fun->getConnection()));
	$d1 = mysqli_fetch_array($q1);

	$s2 = "SELECT * from 21masterlocation where masterlocationid='$lokasi'";
	$q2 = mysqli_query($fun->getConnection(), $s2) or die(mysqli_error($fun->getConnection()));
	$d2 = mysqli_fetch_array($q2);

	

	$s3 = "SELECT * from 23dmastertenant where status='3' and locationid='$lokasi'";
	//echo $s3.'<br><br>';
	$q3 = mysqli_query($fun->getConnection(), $s3) or die(mysqli_error($fun->getConnection()));
	while($d3 = mysqli_fetch_array($q3)) {
		echo '#No. ['.$suno.']<br>';
		$date = date('Y-m-d H:i:s');

		$tid = $d3['mastertenantid'];

		if( ($d3['tier1'] == '') || empty($d3['tier1']) ) {
				$f3tier1 = '';
		}
		else {
			$f1tier1 = str_replace(' ', '', $d3['tier1']);
			$f2tier1 = str_replace('.', '', $f1tier1);
			$f3tier1 = str_replace(',', '', $f2tier1);
		}


		if( ($d3['tier2'] == '') || empty($d3['tier2']) ) {
			$f3tier2 = '';
		}
		else {
			$f1tier2 = str_replace(' ', '', $d3['tier2']);
			$f2tier2 = str_replace('.', '', $f1tier2);
			$f3tier2 = str_replace(',', '', $f2tier2);
		}


		if( ($d3['tier3'] == '') || empty($d3['tier3']) ) {
			$f3tier3 = '';
		}
		else {
			$f1tier3 = str_replace(' ', '', $d3['tier3']);
			$f2tier3 = str_replace('.', '', $f1tier3);
			$f3tier3 = str_replace(',', '', $f2tier3);
		}


		if( ($d3['tier4'] == '') || empty($d3['tier4']) ) {
			$f3tier4 = '';
		}
		else {
			$f1tier4 = str_replace(' ', '', $d3['tier4']);
			$f2tier4 = str_replace('.', '', $f1tier4);
			$f3tier4 = str_replace(',', '', $f2tier4);
		}


		if( ($d3['tier5'] == '') || empty($d3['tier5']) ) {
			$f3tier5 = '';
		}
		else {
			$f1tier5 = str_replace(' ', '', $d3['tier5']);
			$f2tier5 = str_replace('.', '', $f1tier5);
			$f3tier5 = str_replace(',', '', $f2tier5);
		}

		$email = strtolower($f3tier1).strtolower($f3tier2).strtolower($f3tier3).strtolower($f3tier4).strtolower($f3tier5).'@'.strtolower(str_replace(' ', '', $d2['locationname'])).'.'.strtolower(str_replace(' ', '', $d1['companyname'])).'.baronang.com';

		$passa = $fun->randNumber(2);
		$passb = $fun->randNumber(2);
		$password = $passa.$passb.$passa.$passb;
		$enpass = $fun->ENID($password);

		echo 'Email : '.$email.'<br>';
		echo 'Pass : '.$password.'<br><br>';
		//echo 'Status : '.$status.'<br><br>';

		$motor  = $d3['allowancemotorcycle'];
		$mobil1 = $d3['allowancecarsmall'];
		$mobil2 = $d3['allowancecarbig'];

		// GET ID PRODUCT & VALIDITY DAYS FOR MOTORCYCLE
		$sprodmotor = "SELECT masterproductid, validity_days from 22amasterproduct where locationid='$lokasi' and vehicletype='Motorcycle' and productprice='0'";
		$qprodmotor = mysqli_query($fun->getConnection(), $sprodmotor) or die(mysqli_error($fun->getConnection()));
		$dprodmotor = mysqli_fetch_array($qprodmotor);
		$prodmotor  = $dprodmotor['masterproductid']; 
		$validmotor = $dprodmotor['validity_days']; 
		
		// GET SERIAL NUMBER AND LAST COUNTER FOR MOTORCYCLE
		$seriprodmotor = substr($prodmotor, 0, 14);
		$ssprodmotor = "SELECT productserialnumber from 22fproductserialnumber where productserialnumber like '$seriprodmotor%' order by productserialnumber desc limit 1";
		$qsprodmotor = mysqli_query($fun->getConnection(), $ssprodmotor) or die(mysqli_error($fun->getConnection()));
		$rsprodmotor = mysqli_num_rows($qsprodmotor);
		if($rsprodmotor > 0) {
			// 71021010411001 000000
			$dsprodmotor = mysqli_fetch_array($qsprodmotor);
			$csprodmotor = (int)substr($dsprodmotor, -6) + 1;
		}
		else {
			$csprodmotor = 1;
		}


		// GET ID PRODUCT & VALIDITY DAYS FOR SMALL CAR
		$sprodmobil1 = "SELECT masterproductid, validity_days from 22amasterproduct where locationid='$lokasi' and vehicletype='Small Car' and productprice='0'";
		$qprodmobil1 = mysqli_query($fun->getConnection(), $sprodmobil1) or die(mysqli_error($fun->getConnection()));
		$dprodmobil1 = mysqli_fetch_array($qprodmobil1);
		$prodmobil1  = $dprodmobil1['masterproductid']; 
		$validmobil1 = $dprodmobil1['validity_days']; 

		// GET SERIAL NUMBER AND LAST COUNTER SMALL CAR
		$seriprodmobil1 = substr($prodmobil1, 0, 14);
		$ssprodmobil1 = "SELECT productserialnumber from 22fproductserialnumber where productserialnumber like '$seriprodmobil1%' order by productserialnumber desc limit 1";
		$qsprodmobil1 = mysqli_query($fun->getConnection(), $ssprodmobil1) or die(mysqli_error($fun->getConnection()));
		$rsprodmobil1 = mysqli_num_rows($qsprodmobil1);
		if($rsprodmobil1 > 0) {
			// 71021010411001 000000
			$dsprodmobil1 = mysqli_fetch_array($qsprodmobil1);
			$csprodmobil1 = (int)substr($dsprodmobil1, -6) + 1;
		}
		else {
			$csprodmobil1 = 1;
		}


		// GET ID PRODUCT & VALIDITY DAYS FOR BIG CAR
		$sprodmobil2 = "SELECT masterproductid, validity_days from 22amasterproduct where locationid='$lokasi' and vehicletype='Big Car' and productprice='0'";
		$qprodmobil2 = mysqli_query($fun->getConnection(), $sprodmobil2) or die(mysqli_error($fun->getConnection()));
		$dprodmobil2 = mysqli_fetch_array($qprodmobil2);
		$prodmobil2  = $dprodmobil2['masterproductid']; 
		$validmobil2 = $dprodmobil2['validity_days']; 

		// GET SERIAL NUMBER AND LAST COUNTER BIG CAR
		$seriprodmobil2 = substr($prodmobil2, 0, 14);
		$ssprodmobil2 = "SELECT productserialnumber from 22fproductserialnumber where productserialnumber like '$seriprodmobil2%' order by productserialnumber desc limit 1";
		$qsprodmobil2 = mysqli_query($fun->getConnection(), $ssprodmobil2) or die(mysqli_error($fun->getConnection()));
		$rsprodmobil2 = mysqli_num_rows($qsprodmobil2);
		if($rsprodmobil2 > 0) {
			// 71021010411001 000000
			$dsprodmobil2 = mysqli_fetch_array($qsprodmobil2);
			$csprodmobil2 = (int)substr($dsprodmobil2, -6) + 1;
		}
		else {
			$csprodmobil2 = 1;
		}

		
		$s4 = "UPDATE 23dmastertenant set tenantemail='$email', tenantpassword='$enpass', status='$status' where mastertenantid='$tid'";
		//echo $s4.'<br><br>';
		$q4 = mysqli_query($fun->getConnection(), $s4) or die(mysqli_error($fun->getConnection()));


		$scard = "SELECT mastercardid from 23amastercard where memberid='$tid' order by mastercardid DESC limit 1";
		$qcard = mysqli_query($fun->getConnection(), $scard) or die(mysqli_error($fun->getConnection()));
		$rcard = mysqli_num_rows($qcard);

		// GENERATE NOMOR KARTU
		if($rcard > 0) {
			$dcard = mysqli_fetch_array($qcard);
			$nocard = (int)substr($dcard['mastercardid'], -3) + 1;
		}
		else {
			$nocard = '001';
		}

		$StartDate = $date;
		if($status == 2) { 
			// CREATE QR CODE AND INSERT INTO TABLE qrcodelist
			$qrparam = 'id='.$tid.'&email='.$email.'&pass='.$password;
			$qrcodelist = $qridlike.str_pad($lastqrid,5,"0", STR_PAD_LEFT);
			$sibqr = "INSERT into 41qrcodelist values('$qrcodelist', '$date', '".$fun->getValookie('id')."', '$qrparam', '1', '100.php')";
			//echo $sibqr;
			$qibqr = mysqli_query($fun->getConnection(), $sibqr) or die(mysqli_error($fun->getConnection()));
			QRCode('qrtenant_'.$fun->getValookie('id').'/', $qrcodelist, $qrcodelist, 5);


			echo '<img src="qrtenant_'.$fun->getValookie('id').'/'.$qrcodelist.'.png">';
			echo '<br>';


			$s4 = "INSERT into 23amastercard values "; 
			$s5 = "INSERT into 31amastercardproductowned values ";
			$s6 = "INSERT into 22fproductserialnumber values ";

			// echo '======= MOTOR =======<br>';
			for($xmtr = 0; $xmtr < $motor; $xmtr++) {
				$finocard = substr($tid, 0, 13).str_pad($nocard, 3, "0", STR_PAD_LEFT);
				// echo 'No. Kartu (Motor) : '.$finocard.'<br>';

				// TAMBAH KARTU
				$s4 .= "('$finocard', '$tid', '$date', 'Motor', '0', '1'),";

				// TAMBAH SERIAL NUMBER
				$idserialmotor = $seriprodmotor.str_pad($csprodmotor, 6, "0", STR_PAD_LEFT);
				$s6 .= "('$idserialmotor', '$prodmotor', '$date', 1),";

				// TAMBAH PRODUCT OWNED
				$expvalid = date('Y-m-d H:i:s', (time() + ($validmotor * 24 * 60 * 60)) );
				$s5 .= "('$idcpo', '$date', '$finocard', '$prodmotor', '$StartDate', '$expvalid', '$idserialmotor', 1,0,0),";

				$nocard++; $idcpo++; $csprodmotor++;
			}


			// echo '<br>';
			// echo '======= MOBILE KECIL =======<br>';
			for($xmbl1 = 0; $xmbl1 < $mobil1; $xmbl1++) {
				$finocard = substr($tid, 0, 13).str_pad($nocard, 3, "0", STR_PAD_LEFT);
				// echo 'No. Kartu (Mobil Kecil) : '.$finocard.'<br>';

				// TAMBAH KARTU
				$s4 .= "('$finocard', '$tid', '$date', 'Mobil Kecil', '0', '1'),";

				// TAMBAH SERIAL NUMBER
				$idserialmobil1 = $seriprodmobil1.str_pad($csprodmobil1, 6, "0", STR_PAD_LEFT);
				$s6 .= "('$idserialmobil1', '$prodmobil1', '$date', 1),";

				// TAMBAH PRODUCT OWNED
				$expvalid = date('Y-m-d H:i:s', (time() + ($validmobil1 * 24 * 60 * 60)) );
				$s5 .= "('$idcpo', '$date', '$finocard', '$prodmobil1', '$StartDate', '$expvalid', '$idserialmobil1', 1,0,0),";
				
				$nocard++; $idcpo++; $csprodmobil1++;
			}


			// echo '<br>';
			// echo '======= MOBIL BESAR =======<br>';
			for($xmbl2 = 0; $xmbl2 < $mobil2; $xmbl2++) {
				$finocard = substr($tid, 0, 13).str_pad($nocard, 3, "0", STR_PAD_LEFT);
				// echo 'No. Kartu (Mobil Besar) : '.$finocard.'<br>';

				// TAMBAH KARTU
				$s4 .= "('$finocard', '$tid', '$date', 'Mobil Besar', '0', '1'),";

				// TAMBAH SERIAL NUMBER
				$idserialmobil2 = $seriprodmobil2.str_pad($csprodmobil2, 6, "0", STR_PAD_LEFT);
				$s6 .= "('$idserialmobil2', '$prodmobil2', '$date', 1),";

				// TAMBAH PRODUCT OWNED
				$expvalid = date('Y-m-d H:i:s', (time() + ($validmobil2 * 24 * 60 * 60)) );
				$s5 .= "('$idcpo', '$date', '$finocard', '$prodmobil2', '$StartDate', '$expvalid', '$idserialmobil2', 1,0,0),";

				$nocard++; $idcpo++; $csprodmobil2++;
			}

			$length_s4 = (int)strlen($s4);
			$length_s5 = (int)strlen($s5);
			$length_s6 = (int)strlen($s6);

			$s4 = substr($s4, 0, $length_s4-1).';';
			//echo $s4.'<br><br>';
			$q4 =  mysqli_query($fun->getConnection(), $s4) or die(mysqli_error($fun->getConnection()));
			
			$s5 = substr($s5, 0, $length_s5-1).';';
			//echo $s5.'<br><br>';
			$q5 =  mysqli_query($fun->getConnection(), $s5) or die(mysqli_error($fun->getConnection()));

			$s6 = substr($s6, 0, $length_s6-1).';';
			//echo $s4.'<br><br>';
			$q6 =  mysqli_query($fun->getConnection(), $s6) or die(mysqli_error($fun->getConnection()));
			
		}

		echo '====================<br><br>';

		$suno++; $lastqrid++;
	}

}
?>

<center>
	<div style="margin-top: 100px;">
		The page will redirect within 60 seconds, or click the back button<br>
		<button id="btn_print" onclick="window.print();">Print</button>
		<a href="../?"><button>Back</button></a>
	</div>
</center>

<script type="text/javascript">
	
	setTimeout(function(){
		window.location.href = "<?php echo '../?'.$fun->setIDParam('status', 'Successful add tenant!'); ?>"
	}, 60000);
	
</script>
