<?php
require('../util/koneksi.php');

$nama 	 		= $fun->POST('nama', 1);
$email 	 		= $fun->POST('email', 1);
$tlp 	 		= $fun->POST('tlp', 1);
$label1  		= $fun->POST('label1', 1);
$label2  		= $fun->POST('label2', 1);
$label3  		= $fun->POST('label3', 1);
$label4  		= $fun->POST('label4', 1);
$label5  		= $fun->POST('label5', 1);

$labellokasi 	= $fun->POST('labellokasi', 1);
$labeltenant 	= $fun->POST('labeltenant', 1);
$labelvehicle 	= $fun->POST('labelvehicle', 1);

$alamat  		= $fun->POST('alamat', 1);
$prov 	 		= $fun->POST('prov', 2);
$kabkot  		= $fun->POST('kabkot', 2);

$img 	 		= $fun->FILE('gambar', 'name', 1);
$src_img 		= $fun->FILE('gambar', 'tmp_name', 1);

if( $nama == '' || empty($nama) ) {
	//$fun->setOneSession('error_content', 'Nama/Email Tidak Boleh Kosong!<br>Silahkan Coba Lagi!', 2);
	//header('location:../ei.php?'.$fun->setIDParam('title', 'Error Login!').'&'.$fun->setIDParam('link', 'index.php'));
	header('location: ../index.php?'.$fun->setIDParam('status', 'Name can\'t be empty!<br>Try again!'));
}
else {

	$s1 = "SELECT companyid from 12mastercompany order by companyid DESC limit 1";
	$q1 = mysqli_query($fun->getConnection(), $s1) or die(mysqli_error($fun->getConnection()));
	$d1 = mysqli_fetch_array($q1);
	$id = (int)$d1['companyid'] + 1;

	$date = date('Y-m-d H:i:s');
	$s2 = "INSERT into 12mastercompany values('$id', '$nama', '$img', '$date', '$email', '$tlp', '$alamat', '$prov', '$kabkot', '$label1', '$label2', '$label3', '$label4', '$label5', '$labellokasi', '$labeltenant', '$labelvehicle')";
	$q2 = mysqli_query($fun->getConnection(), $s2) or die(mysqli_error($fun->getConnection()));

	if($q2) {
		move_uploaded_file($src_img, '../images/company/'.str_replace(' ', '', strtolower($id.$nama)).'/'.$img);
		header('location: ../index.php?'.$fun->setIDParam('status', 'Successful add company!'));
	}
	else {
		header('location: ../index.php?'.$fun->setIDParam('status', 'Fail to add company!<br>Try again!'));
	}

}
?>