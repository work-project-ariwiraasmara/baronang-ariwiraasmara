<?php
require('../util/koneksi.php');

$process = $fun->POST('process', 1);
$lokasi  = $fun->POST('lokasi', 1);

//$tipe 	= $fun->POST('tipe', 2);
$catprod = $fun->POST('catprod', 2);

$tiprod  = $fun->POST('tiprod', 2);
$prodit  = $fun->POST('prodit', 2);

$valid 	= $fun->POST('valid', 1);

//$img 		= $fun->FILE('gambar', 'name', 1);
//$src_img 	= $fun->FILE('gambar', 'tmp_name', 1);

//$harga 	= 0;

if($process == 'product') {
	$type 		= 1;
	$jml 		= $fun->POST('qty', 1);
	$price 	 = $fun->POST('harga', 1);
	$allowance 	= 0;
	$adddesc = $fun->POST('deskripsi', 1);
	$quota = $fun->POST('quota', 1);
}
else {
	$type 		= 0;
	$jml 		= $fun->POST('qty', 1);
	$allowance 	= $fun->POST('allowance', 1);
	$price 	= 0;
	$adddesc = 'Compliment';
	$quota = 0;
}

if( $catprod == '' || empty($catprod) ) {
	//$fun->setOneSession('error_content', 'Lokasi/Deskripsi/Tipe Tidak Boleh Kosong!<br>Silahkan Coba Lagi!', 2);
	//header('location:../ei.php?'.$fun->setIDParam('title', 'Error Login!').'&'.$fun->setIDParam('link', 'index.php'));
	header('location: ../index.php?'.$fun->setIDParam('status', 'Category Product can\'t be empty!<br>Try again!'));
}
else {

	if( $img == '' || empty($img) ) {
		if($tiprod == 'Small Car') {
			$img = 'car-white.png';
		}
		else if($tiprod == 'Big Car') {
			$img = 'truck-white.png';
		}
		else {
			$img = 'motor-white.png';
		}
	}

	$s1 = "SELECT * from 21masterlocation where masterlocationid='$lokasi'";
	$q1 = mysqli_query($fun->getConnection(), $s1) or die(mysqli_error($fun->getConnection()));
	$d1 = mysqli_fetch_array($q1);


	// AMBIL DATA CATEGORY 
	//$s2 = "SELECT * from 22dproductcategory where productcategoryid='$catprod' and companyid='".$d1['companyid']."'";
	$s2 = "SELECT * from 22dproductcategory where productcategoryid='$catprod' and companyid='71'";
	$q2 = mysqli_query($fun->getConnection(), $s2) or die(mysqli_error($fun->getConnection()));
	$d2 = mysqli_fetch_array($q2);
	$kategori = $d2['productcategoryid'];
	$katename = $d2['productcategoryname'];

	// AMBIL DATA TIPE 
	$s3 = "SELECT * from 22eproducttype where producttypeid='$tiprod' and companyid='71'";
	$q3 = mysqli_query($fun->getConnection(), $s3) or die(mysqli_error($fun->getConnection()));
	$d3 = mysqli_fetch_array($q3);
	$tipe = $d3['producttypename'];


	// AMBIL DATA 
	$s4 = "SELECT * from 22cproductunit where unitid='$prodit'";
	$q4 = mysqli_query($fun->getConnection(), $s4) or die(mysqli_error($fun->getConnection()));
	$d4 = mysqli_fetch_array($q4);
	$unit = $d4['unitname'];


	// NUMBERING [1]
	$idtlokasi = '7102'.substr($lokasi, 6, 3).$d2['productcategoryid'].$type.$tiprod;

	/*
	if($tipe == 'Mobil Kecil') {
		//$idtlokasi = '7102'.substr($lokasi, 4, 3).date('y').'0112';
		$idtlokasi = '7102'.date('y').substr($lokasi, 6, 3).'0112';
	}
	else if($tipe == 'Mobil Besar') {
		//$idtlokasi = '7102'.substr($lokasi, 4, 3).date('y').'0113';
		$idtlokasi = '7102'.date('y').substr($lokasi, 6, 3).'0113';

	}
	else {
		//$idtlokasi = '7102'.substr($lokasi, 4, 3).date('y').'0111';
		$idtlokasi = '7102'.date('y').substr($lokasi, 6, 3).'0111';
	}
	*/

	//$desk = $dloc['locationname'].' Compliment '.$tipe;
	
	$s5 = "SELECT masterproductid from 22amasterproduct where masterproductid like '$idtlokasi%' order by masterproductid desc limit 1";
	$q5 = mysqli_query($fun->getConnection(), $s5) or die(mysqli_error($fun->getConnection()));
	$r5 = mysqli_num_rows($q5);
	$d5 = mysqli_fetch_array($q5);

	if($r5 > 0) {
		$prodnum = (int)substr($d5['masterproductid'],-9,3) + 1;
		$id = $idtlokasi.str_pad($prodnum, 3, '0', STR_PAD_LEFT).'000000';
	}
	else {
		$id = $idtlokasi.'001000000';
	}
	
	$s1 = "SELECT locationname from 21masterlocation where masterlocationid='".$lokasi."'";
	$q1 = mysqli_query($fun->getConnection(), $s1) or die(mysqli_error($fun->getConnection()));
	$d1 = mysqli_fetch_array($q1);
	$namaloc = $d1['locationname'];
				
	$desk = $namaloc.'-'.$katename.'-'.$tipe;

	$date = date('Y-m-d H:i:s');
	$s6 = "INSERT into 22amasterproduct values('$id', '$date', '$lokasi', '$desk', '$tipe', '$jml', '$kategori', '$tiprod', '$unit', '$price', '2', '$allowance', '$img', '$adddesc', '$quota', '0')";
	$q6 = mysqli_query($fun->getConnection(), $s6) or die(mysqli_error($fun->getConnection()));

	if($q6) {
		if($src_img != '' || !empty($src_img) ) {
			move_uploaded_file($src_img, '../images/product/'.$img);
		}

		header('location: ../index.php?pg='.$process.'&'.$fun->setIDParam('lokasi', $lokasi).'&'.$fun->setIDParam('status', 'Succesful add '.$process.'!'));
	}
	else {
		header('location: ../index.php?pg='.$process.'&'.$fun->setIDParam('lokasi', $lokasi).'&'.$fun->setIDParam('status', 'Fail to add '.$process.'!<br>Check connection!'));
	}

}
?>
