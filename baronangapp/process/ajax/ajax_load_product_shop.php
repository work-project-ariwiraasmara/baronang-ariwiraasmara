<?php
require('../../util/koneksi.php');

$scart = "SELECT * from 23eproductshop where status='2' and memberid='".$fun->getValookie('id')."' order by datecreated asc";
$qcart = mysqli_query($fun->getConnection(), $scart) or die(mysqli_error($fun->getConnection()));
$rcart = mysqli_num_rows($qcart);

if($rcart > 0) {
	$total = 0;
	$no = 1;
	while($dcart = mysqli_fetch_array($qcart)) {

		$s2 = "SELECT 	a.masterproductid as masterproductid,
						a.productdescription as productdescription,
						a.vehicletype as vehicletype,
						a.validity_days as validity_days,
						a.unit as unit,
						a.productprice as productprice,
						b.locationname as locationname  
				from 22amasterproduct as a 
				inner join 21masterlocation as b 
					on a.locationid = b.masterlocationid
				where a.masterproductid='".$dcart['productid']."'";
		$q2 = mysqli_query($fun->getConnection(), $s2) or die(mysqli_error($fun->getConnection()));
		$d2 = mysqli_fetch_array($q2);
		?>
		<input type="text" name="<?php echo $fun->Enlink('id').'['.$no.']'; ?>" id="<?php echo $fun->Enlink('id').'-'.$no; ?>" value="<?php echo $fun->Enval($dcart['productshopid']); ?>" class="hide" readonly>
		<input type="text" name="<?php echo $fun->Enlink('price').'['.$no.']'; ?>" id="<?php echo $fun->Enlink('price').'-'.$no; ?>" value="<?php echo $d2['productprice'] ?>" class="hide" readonly>
		<div class="single-news animated fadeinright delay-3 width-100 m-t-10">
			<p class="bold"><?php echo $d2['productdescription']; ?></p>
			<p><span class="bold">Location</span> : <?php echo $d2['locationname']; ?></p>
			<p><span class="bold">Vehicle Type</span> : <?php echo $d2['vehicletype']; ?></p>
			<p><span class="bold">Valid Day(s)</span> : <?php echo $d2['validity_days'].' '.$d2['unit']; ?></p>
			<p><span class="bold">Price</span> : Rp. <?php echo $fun->FormatRupiah($d2['productprice']); ?></p>
			<p class="center m-t-10">
				<input type='button' class='qtyminus' id="<?php echo $fun->Enlink('qtyminus-'.$no); ?>" value='-' field="quantity" />
								    
				<span id="<?php echo $fun->Enlink('qtylabel-'.$no); ?>">1</span>
				<input type='text' name='<?php echo $fun->Enlink('qty').'['.$no.']'; ?>' value='1' id="<?php echo $fun->Enlink('qty-'.$no); ?>" class='hide' />
								    
				<input type='button' class='qtyplus' id="<?php echo $fun->Enlink('qtyplus-'.$no); ?>" value='+' field="quantity" />
			</p>
		</div>

		<script type="text/javascript">
			$("<?php echo '#'.$fun->Enlink('qtylabel-'.$no); ?>").click(function(){
				//$("<?php echo '#'.$fun->Enlink('qty-'.$no); ?>").removeClass("hide");
				//$("<?php echo '#'.$fun->Enlink('qtylabel-'.$no); ?>").addClass("hide");
			});

			$("<?php echo '#'.$fun->Enlink('qtyminus-'.$no); ?>").click(function(){
				var prc = $("<?php echo '#'.$fun->Enlink('price').'-'.$no; ?>").val();
				var qty = $("<?php echo '#'.$fun->Enlink('qty-'.$no); ?>").val();
				var res = parseInt(qty) - 1;
				if(res < 1) {
					//res = 1;
					var id = $("<?php echo '#'.$fun->Enlink('id').'-'.$no; ?>").val();
					$.ajax({
						url : "process/ajax/ajax_delete_product_shop.php",
						type : 'POST',
						data: { 
							productid: id
						},
						success : function(data) {
							//console.log(data);
						},
						error : function(){
							alert('Terjadi kesalahan dalam jaringan!\nTidak dapat mengambil data Product!\nSilahkan cek koneksi jaringan dan coba lagi!');
							return false;
						}
					});
				}
				else {
					total = parseInt(total) - parseInt(prc);
				}

				var ftotal = new Number(total.toFixed(0)).toLocaleString("id-ID");
				$("<?php echo '#'.$fun->Enlink('total-label'); ?>").html(ftotal);
				$("<?php echo '#'.$fun->Enlink('qty-'.$no); ?>").val(res);
				$("<?php echo '#'.$fun->Enlink('qtylabel-'.$no); ?>").html(res);
			});

			$("<?php echo '#'.$fun->Enlink('qtyplus-'.$no); ?>").click(function(){
				var prc = $("<?php echo '#'.$fun->Enlink('price').'-'.$no; ?>").val();
				var qty = $("<?php echo '#'.$fun->Enlink('qty-'.$no); ?>").val();
				var res = parseInt(qty) + 1;
				total = parseInt(total) + parseInt(prc);

				var ftotal = new Number(total.toFixed(0)).toLocaleString("id-ID");
				$("<?php echo '#'.$fun->Enlink('total-label'); ?>").html(ftotal);
				$("<?php echo '#'.$fun->Enlink('qty-'.$no); ?>").val(res);
				$("<?php echo '#'.$fun->Enlink('qtylabel-'.$no); ?>").html(res);
			});
		</script>
		<?php
		$btn_purchase++; $no++;
		$total = $total + $d2['productprice'];
	}
}
else { ?>
	<div class="single-news animated fadeinright delay-3 waves-effect waves-light width-100">
		<h1 class="center bold txt-black">You're cart is empty</h1>
	</div>
	<?php
}
?>