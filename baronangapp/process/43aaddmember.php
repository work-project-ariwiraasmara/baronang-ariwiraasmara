<?php
require('../util/koneksi.php');

$lokasi 	= $fun->POST('lokasi', 1);
$nama 		= $fun->POST('nama', 1);
$tlp 		= $fun->POST('tlp', 1);
//$qtymotor 	= $fun->POST('qtymotor', 1);
//$qtymobil1 	= $fun->POST('qtymobil1', 1);
//$qtymobil2 	= $fun->POST('qtymobil2', 1);

$tier1 	= $fun->POST('tier1', 1);
$tier2 	= $fun->POST('tier2', 1);
$tier3 	= $fun->POST('tier3', 1);
$tier4 	= $fun->POST('tier4', 1);
$tier5 	= $fun->POST('tier5', 1);

if( $lokasi == '' 		|| empty($lokasi) ||
	$nama == '' 		|| empty($nama) ) {
	//$fun->setOneSession('error_content', 'Lokasi/Deskripsi/Tipe Tidak Boleh Kosong!<br>Silahkan Coba Lagi!', 2);
	//header('location:../ei.php?'.$fun->setIDParam('title', 'Error Login!').'&'.$fun->setIDParam('link', 'index.php'));
	header('location: ../index.php?'.$fun->setIDParam('status', 'Location/Name can\'t be empty!<br>Try again!'));
}
else {

	//if($qtymotor == '' || empty($qtymotor) ) $qtymotor = 0;
	//if($qtymobil1 == '' || empty($qtymobil1) ) $qtymobil1 = 0;
	//if($qtymobil2 == '' || empty($qtymobil2) ) $qtymobil2 = 0;

	// COMPANY
	$s2 = "SELECT * from 12mastercompany where companyid='71'";
	$q2 = mysqli_query($fun->getConnection(), $s2) or die(mysqli_error($fun->getConnection()));
	$d2 = mysqli_fetch_array($q2);

	$f1tier1 = str_replace(' ', '', $tier1);
	$f2tier1 = str_replace('.', '', $f1tier1);
	$f3tier1 = str_replace(',', '', $f2tier1);

	$f1tier2 = str_replace(' ', '', $tier2);
	$f2tier2 = str_replace('.', '', $f1tier2);
	$f3tier2 = str_replace(',', '', $f2tier2);

	$f1tier3 = str_replace(' ', '', $tier3);
	$f2tier3 = str_replace('.', '', $f1tier3);
	$f3tier3 = str_replace(',', '', $f2tier3);

	$f1tier4 = str_replace(' ', '', $tier4);
	$f2tier4 = str_replace('.', '', $f1tier4);
	$f3tier4 = str_replace(',', '', $f2tier4);

	$f1tier5 = str_replace(' ', '', $tier5);
	$f2tier5 = str_replace('.', '', $f1tier5);
	$f3tier5 = str_replace(',', '', $f2tier5);


	// LOKASI
	$sloc = "SELECT * from 21masterlocation where masterlocationid='$lokasi'";
	$qloc = mysqli_query($fun->getConnection(), $sloc) or die(mysqli_error($fun->getConnection()));
	$dloc = mysqli_fetch_array($qloc);

	$email = strtolower($f3tier1).strtolower($f3tier2).strtolower($f3tier3).strtolower($f3tier4).strtolower($f3tier5).'@'.strtolower(str_replace(' ', '', $dloc['locationname'])).'.'.strtolower(str_replace(' ', '', $d2['companyname'])).'.baronang.com';

	// PRODUCT
	$sprodmotor = "SELECT * from 22amasterproduct where locationid='$lokasi' and vehicletype='Motorcycle' and productprice='0' and active = '1'";
	$qprodmotor = mysqli_query($fun->getConnection(), $sprodmotor) or die(mysqli_error($fun->getConnection()));
	$dprodmotor = mysqli_fetch_array($qprodmotor);
	if ($dprodmotor != '' || !empty($dprodmotor)) {
		
		$qtymotor = $dprodmotor['allowance'];
	}
	else {
		$qtymotor = 0;
	}
		
	$sprodmobil1 = "SELECT * from 22amasterproduct where locationid='$lokasi' and vehicletype='Small Car' and productprice='0' and active = '1'";
	$qprodmobil1 = mysqli_query($fun->getConnection(), $sprodmobil1) or die(mysqli_error($fun->getConnection()));
	if ($qprodmobil1) {
		$dprodmobil1 = mysqli_fetch_array($qprodmobil1);
		$qtymobil1 = $dprodmobil1['allowance'];
	}
	else {
		$qtymobil1 = 0;
	}
		
	$sprodmobil2 = "SELECT * from 22amasterproduct where locationid='$lokasi' and vehicletype='Big Car' and productprice='0' and active = '1'";
	$qprodmobil2 = mysqli_query($fun->getConnection(), $sprodmobil2) or die(mysqli_error($fun->getConnection()));
	if ($qprodmobil2) {
		$dprodmobil2 = mysqli_fetch_array($qprodmobil2);
		$qtymobil2 = $dprodmobil2['allowance'];
	}
	else {
		$qtymobil2 = 0;
	}
		
	
	// GET LAST ID TENANT
	$s1 = "SELECT * from 23dmastertenant where mastertenantid like '".substr($dloc['masterlocationid'], 0, 9)."%' order by mastertenantid desc limit 1";
	echo $s1.'<br><br>';
	$q1 = mysqli_query($fun->getConnection(), $s1) or die(mysqli_error($fun->getConnection()));
	$r1 = mysqli_num_rows($q1);
	
	// 7101104200001000
	if($r1 > 0) {
		//echo 'last id tenant ada!<br><br>';
		$d1 = mysqli_fetch_array($q1);
		$id1 = substr($d1['mastertenantid'], -7, 4) + 1;
		$id2 = str_pad($id1, 4, "0", STR_PAD_LEFT);
		//$tid = '7101'.substr($dloc['masterlocationid'], 4, 3).date('y').$id2.'000';
		$tid = '7101'.date('y').substr($dloc['masterlocationid'], 6, 3).$id2.'000';
	}
	else {
		//echo 'last id tenant tidak ada!<br><br>';
		//$tid = '7101'.substr($dloc['masterlocationid'], 4, 3).date('y').'1001000';
		$tid = '7101'.date('y').substr($dloc['masterlocationid'], 6, 3).'1001000';
	}

	echo 'mastertenantid : '.$tid.'<br><br>';

	
	$pass = $fun->randChar('10');
	// 01010101
	$enpass = $fun->ENID($pass);
	$date = date('Y-m-d H:i:s');
	//$s3 = "INSERT into 23dmastertenant values('$tid', '', '$date', null, '$lokasi', '$email', '$enpass', '$nama', '$tlp', '', '0', '0', '$qtymobil1', '$qtymobil2', '$qtymotor', '2', '$tier1', '$tier2', '$tier3', '$tier4', '$tier5')";
	$s3 = "INSERT into 23dmastertenant values('$tid', '', '$date', null, '$lokasi', null, null, '$nama', '$tlp', '', '0', '0', '$qtymobil1', '$qtymobil2', '$qtymotor', '3', '$tier1', '$tier2', '$tier3', '$tier4', '$tier5')";
	//echo $s3.'<br>';
	$q3 = mysqli_query($fun->getConnection(), $s3) or die(mysqli_error($fun->getConnection()));

	if($q3) {

		/*
		$fun->setOneSession('tenant-email', $email, 2);
		$fun->setOneSession('tenant-pass', $pass, 2);

		$s4 = "SELECT mastercardid from 23amastercard where memberid='$tid' order by mastercardid DESC limit 1";
		//echo $s4.'<br>';
		$q4 = mysqli_query($fun->getConnection(), $s4) or die(mysqli_error($fun->getConnection()));
		$r4 = mysqli_num_rows($q4);

		if($r4 > 0) {
			$d4 = mysqli_fetch_array($q4);
			$no = str_pad( ((int)substr($d4['mastercardid'], -3) + 1), 3, "0", STR_PAD_LEFT);
		}
		else {
			$no = str_pad(1, 3, "0", STR_PAD_LEFT);
		}

		// GET LAST ID PRODUCT OWN
		$scpo = "SELECT activitylogid from 31amastercardproductowned order by activitylogid DESC";
		$qcpo = mysqli_query($fun->getConnection(), $scpo) or die(mysqli_error($fun->getConnection()));
		$dcpo = mysqli_fetch_array($qcpo);
		$idcpo = (int)$dcpo['activitylogid'] + 1;
		echo 'Last activitylogid: '.$dcpo['activitylogid'].' ==> '.$idcpo.'<br><br>';

		$sprodmotor = "SELECT masterproductid from 22amasterproduct where locationid='$lokasi' and vehicletype='Motor' and productprice='0'";
		$qprodmotor = mysqli_query($fun->getConnection(), $sprodmotor) or die(mysqli_error($fun->getConnection()));
		$dprodmotor = mysqli_fetch_array($qprodmotor);
		$prodmotor = $dprodmotor['masterproductid'];

		if( $prodmotor != '' || !empty($prodmotor) ) {
			if($qtymotor > 0) {
				for($x = 1; $x <= $qtymotor; $x++) {
					$date = date('Y-m-d H:i:s');

					$motorid = substr($tid, 0, 13).$no;
					
					$scmotor1 = "INSERT into 23amastercard values('$motorid', '$tid', '$date', 'Motor', '0')";
					echo 'Motor #'.$x.') .'.$scmotor1.'<br>';
					$qcmotor1 = mysqli_query($fun->getConnection(), $scmotor1) or die(mysqli_error($fun->getConnection()));
					
					$spmotor = "SELECT validity_days from 22amasterproduct where masterproductid='$prodmotor'";
					$qpmotor = mysqli_query($fun->getConnection(), $spmotor) or die(mysqli_error($fun->getConnection()));
					$dpmotor = mysqli_fetch_array($qpmotor);

					$expvalid = date('Y-m-d H:i:s', (time() + ($dpmotor['validity_days'] * 24 * 60 * 60)) );

					$scmotor2 = "INSERT into 31amastercardproductowned values('$idcpo', '$date', '$motorid', '$prodmotor', '$expvalid')";
					echo 'Motor #'.$x.') .'.$scmotor2.'<br><br>';
					$qcmotor2 = mysqli_query($fun->getConnection(), $scmotor2) or die(mysqli_error($fun->getConnection()));

					$no++;
					$no = str_pad($no, 3, "0", STR_PAD_LEFT);
					$idcpo++;
				}
			}
		}
		
		echo '=========================================================================<br><br>';

		$sprodmobil1 = "SELECT masterproductid from 22amasterproduct where locationid='$lokasi' and vehicletype='Mobil Kecil' and productprice='0'";
		$qprodmobil1 = mysqli_query($fun->getConnection(), $sprodmobil1) or die(mysqli_error($fun->getConnection()));
		$dprodmobil1 = mysqli_fetch_array($qprodmobil1);
		$prodmobil1 = $dprodmobil1['masterproductid'];

		if( $prodmobil1 != '' || !empty($prodmobil1) ) {
			if($qtymobil1 > 0) {
				for($x = 1; $x <= $qtymobil1; $x++) {
					$date = date('Y-m-d H:i:s');
					$mobil1id = substr($tid, 0, 13).$no;
					
					$scmobil11 = "INSERT into 23amastercard values('$mobil1id', '$tid', '$date', 'Mobil Kecil', '0')";
					echo 'Mobil 1 #'.$x.'). '.$scmobil11.'<br>';
					$qcmobil11 = mysqli_query($fun->getConnection(), $scmobil11) or die(mysqli_error($fun->getConnection()));
					
					$spmobil11 = "SELECT validity_days from 22amasterproduct where masterproductid='$prodmobil1'";
					$qpmobil11 = mysqli_query($fun->getConnection(), $spmobil11) or die(mysqli_error($fun->getConnection()));
					$dpmobil11 = mysqli_fetch_array($qpmobil11);

					$expvalid = date('Y-m-d H:i:s', (time() + ($dpmobil11['validity_days'] * 24 * 60 * 60)) );

					$scmobil12 = "INSERT into 31amastercardproductowned values('$idcpo', '$date', '$mobil1id', '$prodmobil1', '$expvalid')";
					echo 'Mobil 1 #'.$x.'). '.$scmobil12.'<br><br>';
					$qcmobil12 = mysqli_query($fun->getConnection(), $scmobil12) or die(mysqli_error($fun->getConnection()));

					$no++;
					$no = str_pad($no, 3, "0", STR_PAD_LEFT);
					$idcpo++;
				}
			}
		}
		

		echo '=========================================================================<br><br>';

		$sprodmobil2 = "SELECT masterproductid from 22amasterproduct where locationid='$lokasi' and vehicletype='Mobil Besar' and productprice='0'";
		$qprodmobil2 = mysqli_query($fun->getConnection(), $sprodmobil2) or die(mysqli_error($fun->getConnection()));
		$dprodmobil2 = mysqli_fetch_array($qprodmobil2);
		$prodmobil2 = $dprodmobil2['masterproductid'];

		if( $prodmobil2 != '' || !empty($prodmobil2) ) {
			if($qtymobil2 > 0) {
				for($x = 1; $x <= $qtymobil2; $x++) {
					$date = date('Y-m-d H:i:s');
					$mobil2id = substr($tid, 0, 13).$no;
					
					$scmobil21 = "INSERT into 23amastercard values('$mobil2id', '$tid', '$date', 'Mobil Besar', '0')";
					echo 'Mobil 2 #'.$x.'). '.$scmobil21.'<br>';
					$qcmobil21 = mysqli_query($fun->getConnection(), $scmobil21) or die(mysqli_error($fun->getConnection()));
					
					$spmobil21 = "SELECT validity_days from 22amasterproduct where masterproductid='$prodmobil2'";
					$qpmobil21 = mysqli_query($fun->getConnection(), $spmobil21) or die(mysqli_error($fun->getConnection()));
					$dpmobil21 = mysqli_fetch_array($qpmobil21);

					$expvalid = date('Y-m-d H:i:s', (time() + ($dpmobil21['validity_days'] * 24 * 60 * 60)) );

					$scmobil22 = "INSERT into 31amastercardproductowned values('$idcpo', '$date', '$mobil2id', '$prodmobil2', '$expvalid')";
					echo 'Mobil 2 #'.$x.'). '.$scmobil22.'<br><br>';
					$qcmobil22 = mysqli_query($fun->getConnection(), $scmobil22) or die(mysqli_error($fun->getConnection()));

					$no++;
					$no = str_pad($no, 3, "0", STR_PAD_LEFT);
					$idcpo++;
				}
			}
		}
		*/

		?>
		<script type="text/javascript">
			window.location.href = "<?php echo '../?pg=tenant&'.$fun->setIDParam('lokasi', $lokasi).'&'.$fun->setIDParam('status', 'Successful add tenant!'); ?>"
			//window.location.href = "<?php //echo '43dprintoutmemberpassword.php?'.$fun->setIDParam('jumlah_tenant', '1').'&'.$fun->setIDParam('lokasi', $lokasi); ?>"
		</script>
		<?php
	}
	else { ?>
		<script type="text/javascript">
			window.location.href = "<?php echo '../?pg=tenant&'.$fun->setIDParam('lokasi', $lokasi).'&'.$fun->setIDParam('status', 'Fail to add tenant<br>Check connection!'); ?>"
		</script>
		<?php
	}
	
}
?>
