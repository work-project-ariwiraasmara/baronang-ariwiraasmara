<?php
require('util/koneksi.php');

$session = $fun->getValookie('session');
//$session = "member";
$id = $fun->getValookie('field_id');

if($session == 'member') {
    $s1 = "SELECT * from 22bmastermember where mastermemberid='".$fun->getValookie('id')."'";
}
else {
    $s1 = "SELECT * from 23dmastertenant where mastertenantid='".$fun->getValookie('id')."'";
}
$d1 = $fun->getData($s1);

$s2 = "SELECT * from 23amastercard where memberid='".$fun->getValookie('id')."'";
$d2 = $fun->getData($s2);

$s3 = "SELECT * from 21masterlocation where masterlocationid='".$d1[0]['locationid']."'";
$d3 = $fun->getData($s3);
?>
<div class="menu-header">
    <a href="#" data-toggle-theme class="border-right-0"><i class="fa font-12 color-yellow1-dark fa-lightbulb"></i></a>
    <a href="#" data-menu="menu-highlights" class="border-right-0"><i class="fa font-12 color-green1-dark fa-brush"></i></a>
    <a href="#" data-menu="menu-share" class="border-right-0"><i class="fa font-12 color-red2-dark fa-share-alt"></i></a>
    <a href="#" class="border-right-0"><i class="fa font-12 color-blue2-dark fa-cog"></i></a>
    <a href="#" class="border-right-0 close-menu"><i class="fa font-12 color-red2-dark fa-times"></i></a>
</div>

<div class="menu-logo text-center">
    <a href="<?php echo '?pg=userprofile&'.$fun->setIDParam('userprofile', $fun->getValookie('id'));; ?>"><img class="rounded-circle bg-highlight" width="80" src="images/avatars/5s.png"></a>
    <h1 class="pt-3 font-800 font-28 text-uppercase"><?php echo $fun->getValookie('name'); ?></h1>
</div>

<div class="menu-items">
    <h5 class="text-uppercase opacity-20 font-12 pl-3">Menu</h5>

    <a id="nav-welcome" href="<?php echo '?'; ?>">
        <i data-feather="home" data-feather-line="1" data-feather-size="16" data-feather-color="blue2-dark" data-feather-bg="blue2-fade-dark"></i>
        <span>Home</span>
        <!--<em class="badge bg-highlight color-white">HOT</em>-->
        <i class="fa fa-circle"></i>
    </a>

    <?php
    if($session == 'member') {
        if( $d1[0]['tab3'] == '1' ) { ?>
            <a id="nav-pages" href="?pg=cardmanagement">
                <i data-feather="id-card" data-feather-line="1" data-feather-size="16" data-feather-color="blue2-dark" data-feather-bg="blue2-fade-dark"></i>
                <span>Card Management</span>
                <i class="fa fa-circle"></i>
            </a>
            <?php
        } 

        if( $d1[0]['tab2'] == '1' ) {  ?>
            <a id="nav-starters" href="?pg=skymanagement">
                <i data-feather="star" data-feather-line="1" data-feather-size="18" data-feather-color="yellow1-dark" data-feather-bg="yellow1-fade-dark"></i>
                <span>SkyParking</span>
                <i class="fa fa-circle"></i>
            </a>
            <?php
        }     

        if( $d1[0]['tab1'] == '1' ) {  ?>
            <a id="nav-pages" href="?pg=baronang">
                <i data-feather="cloud" data-feather-line="1" data-feather-size="16" data-feather-color="brown1-dark" data-feather-bg="brown1-fade-dark"></i>
                <span>Baronang</span>
                <i class="fa fa-circle"></i>
            </a>
            <?php
        }  


    }
    ?>

    <!--<a href="#" data-submenu="sub-contact">
        <i data-feather="mail" data-feather-line="1" data-feather-size="16" data-feather-color="blue2-dark" data-feather-bg="blue2-fade-dark"></i>
        <span>Contact</span>
        <strong class="badge bg-highlight color-white">1</strong>
        <i class="fa fa-circle"></i>
    </a>
    <div id="sub-contact" class="submenu">
        <a href="contact.html" id="nav-contact"><i class="fa fa-envelope color-blue2-dark font-16 opacity-30"></i><span>Email</span><i class="fa fa-circle"></i></a>
        <a href="#"><i class="fa fa-phone color-green1-dark font-16 opacity-50"></i><span>Phone</span><i class="fa fa-circle"></i></a>
        <a href="#"><i class="fab fa-whatsapp color-whatsapp font-16 opacity-30"></i><span>WhatsApp</span><i class="fa fa-circle"></i></a>
    </div>
    <a id="nav-settings" href="settings.html">
        <i data-feather="settings" data-feather-line="1" data-feather-size="16" data-feather-color="gray2-dark" data-feather-bg="gray2-fade-dark"></i>
        <span>Settings</span>
        <i class="fa fa-circle"></i>
    </a> -->

    <a href="#" data-toast="notification-3">
        <i data-feather="mail" data-feather-line="1" data-feather-size="16" data-feather-color="gray2-dark" data-feather-bg="gray2-fade-dark"></i>
        <span>Send Notification</span>
        <i class="fa fa-circle"></i>
    </a>  

    <a id="nav-pages" href="?pg=shop&sb=invoice" data-toast="notification-3">
        <i data-feather="mail" data-feather-line="1" data-feather-size="16" data-feather-color="gray2-dark" data-feather-bg="gray2-fade-dark"></i>
        <span>Invoice</span>
        <i class="fa fa-circle"></i>
    </a>  
    <a href="logout.php">
        <i data-feather="x" data-feather-line="3" data-feather-size="16" data-feather-color="red2-dark" data-feather-bg="red2-fade-dark"></i>
        <span>Sign Out</span>
        <i class="fa fa-circle"></i>
    </a>
</div>

<div class="text-center pt-2">
    <p class="mb-0 pt-3 font-10 opacity-100">Powered by <img src="images/icon/Logo-fish-icon-20x20.png"> Baronang &copy; <span id="copyright-year"><?php echo date("Y"); ?></span>.</p>
</div>
