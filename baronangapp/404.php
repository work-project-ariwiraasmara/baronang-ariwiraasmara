<div class="page-title page-title-small">
    <h2><a href="?" data-back-button><i class="fa fa-arrow-left"></i></a>Whoops!</h2>
 </div>

<div class="card header-card shape-rounded" data-card-height="150">
    <div class="card-overlay bg-highlight opacity-95"></div>
    <div class="card-overlay dark-mode-tint"></div>
    <div class="card-bg preload-img" data-src="images/pictures/20s.jpg"></div>
</div>
        
<div class="card card-style text-center">
    <div class="content pt-4 pb-4">
        <h1><i class="fa fa-exclamation-triangle color-red2-dark fa-5x"></i></h1>
        <h1 class="fa-6x pt-5 pb-2">404</h1>
        <h3 class="text-uppercase pb-3">Page not Found</h3>
    </div>
</div>