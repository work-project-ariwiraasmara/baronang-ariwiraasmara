<?php require('header_new.php');?>
<?php require('sidebar-left.php');?>

<?php
if(!isset($_SESSION['KID'])){
    echo "<script language='javascript'>document.location='close.php';</script>";
}
?>

    <div class="animated fadeinup delay-1">
        <div class="page-content">

            <h3 class="uppercase"><?php echo $lang->lang('Buka Akun Deposito', $conn); ?></h3><br>

            <div class="form-inputs">

                <form action="procauth.php" method="POST" class="form-horizontal">
                    <div class="">

                        <div class="input-field">
                            <label><?php echo $lang->lang('Produk Deposito', $conn); ?></label><br>
                            <div class="m-t-10 table-responsive">

                                    <?php
                                    $a = "exec dbo.TimeDepositTypeView '$_SESSION[KID]'";
                                    $b = sqlsrv_query($conn, $a);
                                    while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                                        ?>
                                        <!--data-toggle="modal" data-target="."-->
                                        <a class="modal-trigger" href="#bs-example-modal-<?php echo $c[0]; ?>">
                                            <input type="radio" name="acc" id="acc-<?php echo $c[0]; ?>" value="<?php echo $c[0]; ?>">
                                            <label for="acc-<?php echo $c[0]; ?>"><?php echo $c[1]; ?></label> <br>
                                        </a>
                                    <?php } ?>

                                <input type="hidden" name="kid" value="<?php echo $_SESSION['KID']; ?>" readonly>
                                <input type="hidden" name="mid" value="<?php echo $_SESSION['MemberID']; ?>" readonly>
                                <input type="hidden" name="jenis" value="11" readonly>
                            </div>
                        </div>

                        <div class="input-field">
                            <label><?php echo $lang->lang('Akun Tabungan', $conn); ?></label><br>
                            <div class="m-t-10">
                                    <?php
                                    $a = "exec [dbo].[ListRegularSavingBal] '$_SESSION[KID]','$_SESSION[MemberID]'";
                                    $b = sqlsrv_query($conn, $a);
                                    while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                                    ?>
                                    <input type="radio" name="regacc" class="minimal" id="regacc" value="<?php echo $c[2]; ?>">
                                    <label for="regacc">
                                        <b><?php echo $c[2]; ?></b> - <?php echo $c[4]; ?>
                                        <br><span class="">Rp. <?php echo number_format($c[5]); ?></span>
                                    </label> <br>
                                    <?php } ?>
                            </div>
                        </div>

                        <div class="input-field">
                            <label style="text-align: left;">ARO</label> <br>

                            <div class="m-t-10">
                                <input type="radio" name="aro" id="aroya" value="1">
                                <label for="aroya"><?php echo $lang->lang('Ya', $conn); ?></label>

                                <input type="radio" name="aro" id="arono" value="0" checked>
                                <label for="arono"><?php echo $lang->lang('Tidak', $conn); ?></label>
                            </div>
                        </div> <br>


                        <div class="input-field">
                            <label style="text-align: left;"><?php echo $lang->lang('Bunga Ditransfer Bulanan', $conn); ?></label> <br>

                            <div class="m-t-10">
                                <input type="radio" name="itm" id="itmya" value="1">
                                <label for="itmya" style="padding-right: 20px"><?php echo $lang->lang('Ya', $conn); ?></label>

                                <input type="radio" name="itm" id="itmno" value="0" checked>
                                <label for="itmno"><?php echo $lang->lang('Tidak', $conn); ?></label>
                            </div>
                        </div> <br>

                        <div class="input-field">
                            <label style="text-align: left;"><?php echo $lang->lang('Bunga Ditransfer Saat ARO', $conn); ?></label> <br>

                            <div class="m-t-10">
                                <input type="radio" name="itaa" id="itaaya" value="1">
                                <label for="itaaya" style="padding-right: 20px"><?php echo $lang->lang('Ya', $conn); ?></label>

                                <input type="radio" name="itaa" id="itaano" value="0" checked>
                                <label for="itaano"><?php echo $lang->lang('Tidak', $conn); ?></label>
                            </div>
                        </div> <br>

                        <div class="input-field">
                            <label for="amount"><?php echo $lang->lang('Jumlah', $conn); ?></label>
                            <input type="text" name="amount" class="validate price" id=amount"" value="0">
                        </div>

                        <div class="m-t-30">
                            <button type="submit" class="waves-effect waves-light btn-large primary-color width-100"><?php echo $lang->lang('Simpan', $conn); ?></button>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>

<?php
$a = "exec dbo.TimeDepositTypeView '$_SESSION[KID]'";
$b = sqlsrv_query($conn, $a);
while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
    ?>
    <div class="modal" id="bs-example-modal-<?php echo $c[0]; ?>">
        <div class="modal-content">
            <h4 class="modal-title"><?php echo $lang->lang('Deposito', $conn); ?> <?php echo $c[1]; ?></h4> <br> 
                    
            <a href="#"><?php echo $lang->lang('Suku Bunga %', $conn); ?> <span class="right"><?php echo $c[2]; ?>%</span></a> <br>
            <a href="#"><?php echo $lang->lang('Masa Kontrak', $conn); ?> <span class="right"><?php echo $c[3]; ?> <?php echo $lang->lang('Bulan', $conn); ?></span></a> <br>
            <a href="#"><?php echo $lang->lang('Jumlah Minimum', $conn); ?> <span class="right"><?php echo number_format($c[4]); ?></span></a> <br>
            <a href="#"><?php echo $lang->lang('Biaya Penalti', $conn); ?> <span class="right"><?php echo number_format($c[5]); ?></span></a> <br>
            <a href="#"><?php echo $lang->lang('Bunga Penalti %', $conn); ?> <span class="right"><?php echo $c[6]; ?></span></a>
                    
        </div>
        <div class="modal-footer">
            <button type="button" class="btn primary-color right modal-close" id="btn-<?php echo $c[0]; ?>"><?php echo $lang->lang('OK', $conn); ?></button>
        </div>
    </div>
	
	<script>
	$('#btn-<?php echo $c[0]; ?>').click(function () {
		$('#acc-<?php echo $c[0]; ?>').prop('checked', true);
	});
	</script>
<?php } ?>

<?php require('footer_new.php');?>