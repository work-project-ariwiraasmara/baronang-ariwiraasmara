<?php require('header_new.php');?>
<?php require('sidebar-left.php');?>

<?php
if(!isset($_SESSION['KID'])){
    echo "<script language='javascript'>document.location='close.php';</script>";
}
?>

    <div class="animated fadeinup delay-1">
        <div class="page-content">

            <h3 class="uppercase"><?php echo $lang->lang('Buka Akun Tabungan', $conn); ?></h3><br>

            <div class="form-inputs">
                <form action="procauth.php" method="POST" class="form-horizontal">
                    <div class="">
                        <?php echo $lang->lang('Produk Tabungan', $conn); ?>
                        <div class="m-t-10">

                                <?php
                                $a = "exec dbo.RegularSavingTypeView '$_SESSION[KID]'";
                                $b = sqlsrv_query($conn, $a);
                                while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                                    ?>
                                    <a class="modal-trigger" id="link_modal-<?php echo $c[0]; ?>" href="#bs-example-modal-<?php echo $c[0]; ?>">
                                        <input name="acc" type="radio" id="acc-<?php echo $c[0]; ?>" value="<?php echo $c[0]; ?>">
                                        <label for="acc-<?php echo $c[0]; ?>"><?php echo $c[1]; ?></label> <br>
                                    </a>
                                <?php } ?>

                            <input type="hidden" name="kid" value="<?php echo $_SESSION['KID']; ?>" readonly>
                            <input type="hidden" name="mid" value="<?php echo $_SESSION['MemberID']; ?>" readonly>
                            <input type="hidden" name="jenis" value="9" readonly>
                        </div>
                    </div>

                    <div class="input-field">
                        <?php echo $lang->lang('Setoran Awal', $conn); ?>
                        <input type="text" name="amount" id="amount" class="validate price" value="0">
                    </div>

                    <div class="m-t-30">
                        <button type="submit" class="waves-effect waves-light btn-large primary-color width-100"><?php echo $lang->lang('Simpan', $conn); ?></button>
                    </div>
                </form>


            </div>

        </div>
    </div>

    <?php
    $a = "exec dbo.RegularSavingTypeView '$_SESSION[KID]'";
    $b = sqlsrv_query($conn, $a);
    while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
        ?>
        <div class="modal" id="bs-example-modal-<?php echo $c[0]; ?>">
            <div>
                <div class="modal-content">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn primary-color right modal-close" id="btn-<?php echo $c[0]; ?>"><?php echo $lang->lang('OK', $conn); ?></button>
                </div>
            </div>
        </div>

        <script type="text/javascript">
            $('#btn-<?php echo $c[0]; ?>').click(function () {
                //$('#acc-<?php echo $c[0]; ?>').prop('checked', true);
            });

            $('#link_modal-<?php echo $c[0]; ?>').click(function(){
                $('#acc-<?php echo $c[0]; ?>').prop('checked', true);

                var acc = $("input[name='acc']:checked").val();

                $.ajax({
                    url : "ajax_getskema.php",
                    type : 'POST',
                    data: { acc: acc},
                    success : function(data) {
                        $(".modal-content").html(data);
                    },
                    error : function(){
                        System.showToast('Coba lagi');
                    }
                });
            });
        </script>

    <?php } ?>

<?php require('footer_new.php');?>