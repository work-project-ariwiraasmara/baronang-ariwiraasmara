<div id="toolbar" class="primary-color animated fadeindown">
    <a href="javascript:history.back()" class="open-left">
        <i class="ion-android-arrow-back"></i>
    </a>

    <span class="title">Baronang</span>

    <div class="open-right">
        <a href="setting.php">
            <i class="ion-gear-a"></i>
        </a>
    </div>

    <div class="open-right owntooltip">
        <a href="approval.php">
            <i class="ion-ios-bell"></i>
            <?php if($m > 0){ ?>
                <span><?php echo $m; ?></span>
            <?php } ?>
        </a>
    </div>

    <div class="open-right owntooltip">
        <?php
        $date = date('Y-m-d H:i:s');
        if(isset($_SESSION['KID'])){
            //listtrx
            $tanggal = date('Y-m-d');
            $a = "select count(*) from [dbo].[UserTokenTransaksi] where KodeBaronangPay in ('$_SESSION[UserID]','$_SESSION[MemberID]') and StatusTrx = 0 and KID = '$_SESSION[KID]'";
            $b = sqlsrv_query($conn, $a);
            $c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC);
            $j = $c[0];

            //approval
            $x = "select* from [dbo].[UserMemberKoperasi] where KID = '$_SESSION[KID]' and UserID = '$_SESSION[UserID]'";
            $y = sqlsrv_query($conn, $x);
            $z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC);
            if($z != null){
                $kode = $z[2];
            }
            else{
                $kode = $z[1];
            }
            $a = "select count(*) from [KoneksiKoperasiBaronang].[dbo].[RequestPage] where KodeUser = '$kode' and Status = '0' and TanggalExpired >= '$date' or KodeUser = '$_SESSION[UserID]' and Status = '0' and TanggalExpired >= '$date'";
            ?>
            <a href="listtrx.php">
                <i class="ion-android-apps"></i>
                <span id="txttip">
                        <?php if($j > 0){ ?>
                            <b><?php echo $j; ?></b>
                        <?php } ?>
                     </span>
            </a>
            <?php
        }
        else {
            $a = "select count(*) from [KoneksiKoperasiBaronang].[dbo].[RequestPage] where KodeUser = '$_SESSION[UserID]' and Status = '0' and TanggalExpired >= '$date' and Link in('identity_cs.php','identity_cas.php')";
        }

        $b = sqlsrv_query($conn, $a);
        $c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC);
        $m = $c[0];
        ?>
    </div>

</div>