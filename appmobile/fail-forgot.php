<?php require('header-register.php');?>

    <div class="col-sm-12">
        <div style="margin-top: 30%;margin-bottom: 30%;">
            <div class="widget-user-image">
                <img class="profile-user-img img-responsive" src="images/times.png" style="border: none;">
            </div>
            <h3 class="text-center"><?php echo $lang->lang('Permintaan Gagal', $conn); ?></h3>
            <br>
            <br>
            <br>
            <div class="text-center">
                <?php echo $lang->lang('Email', $conn); ?><?php if(isset($_GET['p'])){ echo '/Password'; } ?> <?php echo $lang->lang('Yang anda masukan tidak terdaftar', $conn); ?>
            </div>
            <br>
        </div>

        <div class="footer2">
            <div class="col-sm-12">
                <p>
                    <a href="register.php"><button type="button" class="btn btn-lg btn-primary btn-flat btn-block" style="border-radius: 30px;"><?php echo $lang->lang('Daftar', $conn); ?></button></a>
                </p>
                <p>
                    <a href="login.php"><button type="button" class="btn btn-lg btn-info btn-flat btn-block" style="border-radius: 30px;"><?php echo $lang->lang('Masuk', $conn); ?></button></a>
                </p>
            </div>
        </div>
    </div>

<?php require('footer.php');?>