<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Baronang App</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta content="yes" name="apple-mobile-web-app-capable">
    <meta content="yes" name="apple-touch-fullscreen">
    <!-- ICON -->
    <link href="static/images/Logo-fish-icon.png" rel="shortcut icon">
    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
    <!-- Icons -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" media="all" rel="stylesheet" type="text/css">
    <!-- Styles -->
    <link href="static/dist/new/css/keyframes.css" rel="stylesheet" type="text/css">
    <link href="static/dist/new/css/materialize.min.css" rel="stylesheet" type="text/css">
    <link href="static/dist/new/css/swiper.css" rel="stylesheet" type="text/css">
    <link href="static/dist/new/css/swipebox.min.css" rel="stylesheet" type="text/css">
    <link href="static/dist/new/css/style.css" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="static/bootstrap/css/bootstrap.css"/>
    <link rel="stylesheet" href="static/bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="static/bootstrap/css/bootstrap-grid.css"/>
    <link rel="stylesheet" href="static/bootstrap/css/bootstrap-grid.min.css"/>
    <link rel="stylesheet" href="static/bootstrap/css/bootstrap-reboot.css"/>
    <link rel="stylesheet" href="static/bootstrap/css/bootstrap-reboot.min.css"/>

    <!-- jQuery 2.1.4 -->
    <script src="static/plugins/jQuery/jQuery-2.1.4.min.js"></script>

    <style>
        .toBottom {
            width: 93%;
            bottom: 0;
            position: fixed;
        }
        .center {
            text-align: center !important;
        }
        .m.t-5 {
            margin-top: 5px !important;
        }
        .owntooltip #txttip {
            margin-bottom: 15px !important;
        }
        .owntooltip a, .owntooltip a:link {
            color: #c0c0ff;
        }
    </style>
</head>
<body>
<?php
ini_set('session.cookie_lifetime', 60 * 60 * 24 * 100);
ini_set('session.gc_maxlifetime', 60 * 60 * 24 * 100);

$tablet_browser = 0;
$mobile_browser = 0;

if (preg_match('/(tablet|ipad|playbook)|(android(?!.*(mobi|opera mini)))/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
    $tablet_browser++;
}

if (preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|android|iemobile)/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
    $mobile_browser++;
}

if ((strpos(strtolower($_SERVER['HTTP_ACCEPT']),'application/vnd.wap.xhtml+xml') > 0) or ((isset($_SERVER['HTTP_X_WAP_PROFILE']) or isset($_SERVER['HTTP_PROFILE'])))) {
    $mobile_browser++;
}

$mobile_ua = strtolower(substr($_SERVER['HTTP_USER_AGENT'], 0, 4));
$mobile_agents = array(
    'w3c ','acs-','alav','alca','amoi','audi','avan','benq','bird','blac',
    'blaz','brew','cell','cldc','cmd-','dang','doco','eric','hipt','inno',
    'ipaq','java','jigs','kddi','keji','leno','lg-c','lg-d','lg-g','lge-',
    'maui','maxo','midp','mits','mmef','mobi','mot-','moto','mwbp','nec-',
    'newt','noki','palm','pana','pant','phil','play','port','prox',
    'qwap','sage','sams','sany','sch-','sec-','send','seri','sgh-','shar',
    'sie-','siem','smal','smar','sony','sph-','symb','t-mo','teli','tim-',
    'tosh','tsm-','upg1','upsi','vk-v','voda','wap-','wapa','wapi','wapp',
    'wapr','webc','winw','winw','xda ','xda-');

if (in_array($mobile_ua,$mobile_agents)) {
    $mobile_browser++;
}

if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'opera mini') > 0) {
    $mobile_browser++;
    //Check for tablets on opera mini alternative headers
    $stock_ua = strtolower(isset($_SERVER['HTTP_X_OPERAMINI_PHONE_UA'])?$_SERVER['HTTP_X_OPERAMINI_PHONE_UA']:(isset($_SERVER['HTTP_DEVICE_STOCK_UA'])?$_SERVER['HTTP_DEVICE_STOCK_UA']:''));
    if (preg_match('/(tablet|ipad|playbook)|(android(?!.*mobile))/i', $stock_ua)) {
        $tablet_browser++;
    }
}

if ($tablet_browser > 0) {
    // do something for tablet devices
}
else if ($mobile_browser > 0) {
    // do something for mobile devices
}
else {
    // do something for everything else
    //echo "Directory or file not accessible.";
    //exit;
}

include "connect.inc";
include("connectinti.inc");


$tyu = "select MobileID from [dbo].[UserPaymentGateway] where email = '".$_SESSION['Email']."'";
$fgh = sqlsrv_query($conn, $tyu);
$cfd = sqlsrv_fetch_array($fgh, SQLSRV_FETCH_NUMERIC);

if($_SESSION['device'] <> $cfd[0]){
    echo "<script language='javascript'>document.location='logout.php';</script>";
}

if($_SESSION['session_expired'] < time()){
    unset($_SESSION['url']);
    echo "<script language='javascript'>Intent.openActivity('FingerprintActivity','session.php');</script>";
    //echo "<script language='javascript'>document.location='session.php';</script>";
}

if($_SESSION['session_expired'] > time()){
    $session_expired = 60*15 + time(); //15 menit

    //tambah session 15 menit karna ada aktifitas
    $aa = "update [dbo].[UserPaymentGateway] set SessionExpired='".$session_expired."' where email = '".$_SESSION['Email']."'";
    $bb = sqlsrv_query($conn, $aa);
}

if($_SESSION['time'] <= time()){
    unset($_SESSION['time']);
    unset($_SESSION['message']);
}

session_start();
?>

<div class="m-scene" id="main">
