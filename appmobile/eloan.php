<?php require('header.php');?>

<?php
if(!isset($_GET['id'])){
    echo "<script language='javascript'>document.location='balance.php';</script>";
}

$x = "exec dbo.LoanApplicationSearchByMember '$_SESSION[KID]','$_GET[id]','$_SESSION[MemberID]'";
$y = sqlsrv_query($conn, $x);
$z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC);
if($z == null){
    echo "<script language='javascript'>document.location='balance.php';</script>";
}
else if($z != null and $z[7] != 0){
    echo "<script language='javascript'>document.location='balance.php';</script>";
}
else{
    $loanappnum = $z[1];
    $requestdate = $z[2]->format('Y-m-d H:i:s');
    $expireddate = $z[8]->format('Y-m-d H:i:s');
    $amount = $z[5];
    $type = $z[4];
    $product = '';
    $a = "exec dbo.LoanTypeSearch '$_SESSION[KID]','$type'";
    $b = sqlsrv_query($conn, $a);
    $c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC);
    if($c != null){
        $product = $c[1];
    }

    ?>

    <div class="col-lg-12">
        <div class="box box-solid">
            <div class="box-header">
                <h3 class="box-title">Ubah Pengajuan Pinjaman</h3>
            </div>
            <div class="box-body">
                <form action="proceloan.php" id="sloan" method="POST" class="form-horizontal" enctype="multipart/form-data">
                    <div class="col-sm-12">
                        <input type="hidden" name="loanappnum" id="loanappnum" value="<?php echo $loanappnum; ?>" readonly>
                        <input type="hidden" name="type" id="type" value="<?php echo $z[4]; ?>" readonly>
                        <input type="hidden" name="maxpen" id="maxpen" value="<?php echo $c[14] ?>" readonly>
                        <input type="hidden" name="maxdoc" id="maxdoc" value="<?php echo $c[16] ?>" readonly>

                        <div class="form-group">
                            <label class="col-sm-3" style="text-align: left;">No. Pengajuan Pinjaman</label>
                            <div class="col-sm-9">
                                <?php echo $loanappnum; ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3" style="text-align: left;">Produk</label>
                            <div class="col-sm-9">
                                <?php echo $product; ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3" style="text-align: left;">Tanggal Pengajuan</label>
                            <div class="col-sm-9">
                                <?php echo $requestdate; ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3" style="text-align: left;">Tanggal Kadaluarsa</label>
                            <div class="col-sm-9">
                                <?php echo $expireddate; ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3" style="text-align: left;">Jumlah Pengajuan Pinjaman</label>
                            <div class="col-sm-9">
                                <?php echo number_format($amount); ?>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group">
                            <label class="col-sm-3" style="text-align: left;">Penjamin</label>
                            <div class="col-sm-9">
                                Jumlah penjamin yang dibutuhkan <?php echo $c[14]; ?>
                                <?php
                                $no = 0;
                                $x = "exec dbo.LoanApplicationPenjaminSearch '$_SESSION[KID]','$loanappnum'";
                                $y = sqlsrv_query($conn, $x);
                                while($z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC)){
                                    $q = "select * from dbo.UserMemberKoperasi where KodeMember = '$z[1]'";
                                    $w = sqlsrv_query($conn, $q);
                                    $e = sqlsrv_fetch_array($w, SQLSRV_FETCH_NUMERIC);
                                    if($e != null){
                                        $no++;
                                    }
                                }
                                ?>
                                <table id="membertable" class="table table-bordered">
                                    <tbody>
                                    <input type="hidden" id="userid" value="<?php echo $_SESSION['UserID']; ?>" readonly>
                                    <?php if($c[14] != $no){ ?>
                                        <tr>
                                            <td>
                                                <input type="number" id="uid" class="form-control" placeholder="User ID Baronang App">
                                            </td>
                                            <td>
                                                <button type="button" class="btn btn-flat btn-primary btn-member">Add</button>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                    <tfoot>
                                    <?php
                                    $no = 0;
                                    $x = "exec dbo.LoanApplicationPenjaminSearch '$_SESSION[KID]','$loanappnum'";
                                    $y = sqlsrv_query($conn, $x);
                                    while($z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC)){ ?>
                                        <tr>
                                            <td><?php echo $e[1]; ?></td>
                                            <td>
                                                <?php
                                                if($z[3] == 0){
                                                    echo 'Menunggu Persetujuan';
                                                }
                                                else if($z[3] == 1){
                                                    echo 'Disetujui';
                                                }
                                                else{
                                                    echo 'Ditolak';
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3" style="text-align: left;">Dokumen</label>
                            <div class="col-sm-9">
                                Jumlah dokumen yang dibutuhkan <?php echo $c[16]; ?>
                                <table id="doctable" class="table">
                                    <?php
                                    $n = 0;
                                    $zxc = "exec [dbo].[SettingDocLoanView] '$_SESSION[KID]','$c[0]'";
                                    $asd = sqlsrv_query($conn, $zxc);
                                    while($qwe = sqlsrv_fetch_array($asd, SQLSRV_FETCH_NUMERIC)){
                                        $x = "select * from $_SESSION[Kop].dbo.LoanDocUpload where LoanAppNum = '$c[0]' and NameDoc = '$c[1]'";
                                        $y = sqlsrv_query($conn, $x);
                                        $z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC);
                                        if($z != null){
                                        ?>
                                            <tr>
                                                <td><?php echo $z[1]; ?></td>
                                                <td><i class="fa fa-check"></i></td>
                                            </tr>
                                        <?php } else { ?>
                                            <tr>
                                                <td>
                                                    <?php echo $qwe[1]; ?>
                                                    <input type="hidden" name="name[]" value="<?php echo $qwe[1]; ?>" readonly>
                                                </td>
                                                <td><input type="file" name="filename[]" class="form-control" accept="image/*"></td>
                                            </tr>
                                        <?php } ?>
                                    <?php } ?>
                                </table>
                            </div>
                        </div>
                        <div class="box-footer">
                            <div class="row">
                                <button type="button" class="btn btn-flat btn-block btn-success btn-save">Simpan</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        var tampung = [];

        $('.btn-save').click(function(){
            var maxpen = $('#maxpen').val();
            var rows = document.getElementById("membertable").getElementsByTagName("tfoot")[0].getElementsByTagName("tr").length;

            if(rows != maxpen){
                System.showToast('Penjamin harus dilengkapi');
                return false;
            }
            else{
                $('#sloan').submit();
            }
        });

        $('.btn-member').click(function(){
            var uid = $('#uid').val();
            var userid = $('#userid').val();
            var maxpen = $('#maxpen').val();
            var loanappnum = $('#loanappnum').val();
            var rows = document.getElementById("membertable").getElementsByTagName("tfoot")[0].getElementsByTagName("tr").length;

            if(jQuery.inArray(uid, tampung) !== -1){
                System.showToast('Tidak dapat menambahkan penjamin yang sudah ada');
                return false;
            }
            else if(uid == userid){
                System.showToast('Silahkan masukan penjamin yang lain');
                return false;
            }
            else if(rows == maxpen){
                System.showToast('Melebihi batas jumlah penjamin yang dibutuhkan');
                return false;
            }
            else{
                $.ajax({
                    url : "ajax_changemember.php",
                    type : 'POST',
                    dataType : 'json',
                    data: { uid: uid, loanappnum: loanappnum},   // data POST yang akan dikirim
                    success : function(data) {
                        if(data.status == 1){
                            tampung.push(data.uid);
                            $('#uid').val('');
                            $("#membertable tfoot").append("<tr>" +
                                "<td><input type='hidden' name='uid[]' value="+ data.uid +">" + data.uid + "</td>" +
                                "<td><a class='btn btn-default btn-flat btn-sm text-red remove' onclick='$(this).parent().parent().remove();'><i class='fa fa-trash-o'></i></a></td>" +
                                "</tr>");
                        }
                        else if(data.status == 2){
                            System.showToast(data.message);
                            return false;
                        }
                        else{
                            System.showToast('Gagal');
                            return false;
                        }
                    },
                    error : function() {
                        System.showToast('Coba lagi');
                        return false;
                    }
                });
            }
        });
    </script>
<?php } ?>

<?php require('footer.php');?>