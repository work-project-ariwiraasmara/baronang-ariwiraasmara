<?php
session_start();
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Baronang App</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- ICON -->
    <link href="static/images/Logo-fish-icon.png" rel="shortcut icon">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="static/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"> -->
    <link rel="stylesheet" href="static/plugins/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Morris chart -->
    <link rel="stylesheet" href="static/plugins/morris/morris.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="static/plugins/datepicker/datepicker3.css">
    <!-- daterange picker -->
    <link rel="stylesheet" href="static/plugins/daterangepicker/daterangepicker.css">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="static/plugins/iCheck/all.css">
    <!-- Bootstrap time Picker -->
    <link rel="stylesheet" href="static/plugins/timepicker/bootstrap-timepicker.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="static/plugins/select2/select2.min.css">
    <!-- Pace style -->
    <link rel="stylesheet" href="static/plugins/pace/pace.min.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="static/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="static/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="static/dist/css/skins/_all-skins.min.css">

    <!-- jQuery 2.1.4 -->
    <script src="static/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Cropper -->
    <link rel="stylesheet" href="static/plugins/cropper/cropper.css">

    <style>
        .footer {
            position: fixed;
            left: 0;
            bottom: 45px;
            width: 100%;
        }

        .footer2 {
            position: fixed;
            left: 0;
            bottom: 0;
            width: 100%;
        }

        .card {
            -ms-transform: rotate(90deg); /* IE 9 */
            -webkit-transform: rotate(90deg); /* Safari 3-8 */
            transform: rotate(90deg);
        }
    </style>
</head>
<body class="hold-transition skin-blue layout-top-nav fixed">
<?php
ini_set('session.cookie_lifetime', 60 * 60 * 24 * 100);
ini_set('session.gc_maxlifetime', 60 * 60 * 24 * 100);

$tablet_browser = 0;
$mobile_browser = 0;

if (preg_match('/(tablet|ipad|playbook)|(android(?!.*(mobi|opera mini)))/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
    $tablet_browser++;
}

if (preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|android|iemobile)/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
    $mobile_browser++;
}

if ((strpos(strtolower($_SERVER['HTTP_ACCEPT']),'application/vnd.wap.xhtml+xml') > 0) or ((isset($_SERVER['HTTP_X_WAP_PROFILE']) or isset($_SERVER['HTTP_PROFILE'])))) {
    $mobile_browser++;
}

$mobile_ua = strtolower(substr($_SERVER['HTTP_USER_AGENT'], 0, 4));
$mobile_agents = array(
    'w3c ','acs-','alav','alca','amoi','audi','avan','benq','bird','blac',
    'blaz','brew','cell','cldc','cmd-','dang','doco','eric','hipt','inno',
    'ipaq','java','jigs','kddi','keji','leno','lg-c','lg-d','lg-g','lge-',
    'maui','maxo','midp','mits','mmef','mobi','mot-','moto','mwbp','nec-',
    'newt','noki','palm','pana','pant','phil','play','port','prox',
    'qwap','sage','sams','sany','sch-','sec-','send','seri','sgh-','shar',
    'sie-','siem','smal','smar','sony','sph-','symb','t-mo','teli','tim-',
    'tosh','tsm-','upg1','upsi','vk-v','voda','wap-','wapa','wapi','wapp',
    'wapr','webc','winw','winw','xda ','xda-');

if (in_array($mobile_ua,$mobile_agents)) {
    $mobile_browser++;
}

if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'opera mini') > 0) {
    $mobile_browser++;
    //Check for tablets on opera mini alternative headers
    $stock_ua = strtolower(isset($_SERVER['HTTP_X_OPERAMINI_PHONE_UA'])?$_SERVER['HTTP_X_OPERAMINI_PHONE_UA']:(isset($_SERVER['HTTP_DEVICE_STOCK_UA'])?$_SERVER['HTTP_DEVICE_STOCK_UA']:''));
    if (preg_match('/(tablet|ipad|playbook)|(android(?!.*mobile))/i', $stock_ua)) {
        $tablet_browser++;
    }
}

if ($tablet_browser > 0) {
    // do something for tablet devices
}
else if ($mobile_browser > 0) {
    // do something for mobile devices
}
else {
    // do something for everything else
    echo "Directory or file not accessible.";
    exit;
}

include "connect.inc";
include("connectinti.inc");

if($_SESSION['UserID'] == '' or $_SESSION['NamaUser'] == ''){
    echo "<script language='javascript'>document.location='login.php';</script>";
}

$tyu = "select MobileID from [dbo].[UserPaymentGateway] where email = '".$_SESSION['Email']."'";
$fgh = sqlsrv_query($conn, $tyu);
$cfd = sqlsrv_fetch_array($fgh, SQLSRV_FETCH_NUMERIC);

if($_SESSION['device'] <> $cfd[0]){
    echo "<script language='javascript'>document.location='logout.php';</script>";
}

if($_SESSION['session_expired'] < time()){
    unset($_SESSION['url']);
    echo "<script language='javascript'>Intent.openActivity('FingerprintActivity','session.php');</script>";
    //echo "<script language='javascript'>document.location='session.php';</script>";
}

if($_SESSION['session_expired'] > time()){
    $session_expired = 60*15 + time(); //15 menit

    //tambah session 15 menit karna ada aktifitas
    $aa = "update [dbo].[UserPaymentGateway] set SessionExpired='".$session_expired."' where email = '".$_SESSION['Email']."'";
    $bb = sqlsrv_query($conn, $aa);
}

if($_SESSION['time'] <= time()){
    unset($_SESSION['time']);
    unset($_SESSION['message']);
}

session_start();
?>

<div class="wrapper">
    <header class="main-header">
        <nav class="navbar navbar-static-top">
            <a href="#" class="pull-left" style="padding: 8px;">
                <span class="logo-mini"><img width="160" height="30" src ="images/Baronang Logo-03.png"></span>
            </a>

            <div class="navbar-custom-menu" style="margin-top: 2px;">
                <ul class="nav navbar-nav">
                    <?php
                    $date = date('Y-m-d H:i:s');
                    if(isset($_SESSION['KID'])){
                        //listtrx
                        $tanggal = date('Y-m-d');
                        $a = "select count(*) from [dbo].[UserTokenTransaksi] where KodeBaronangPay in ('$_SESSION[UserID]','$_SESSION[MemberID]') and StatusTrx = 0 and KID = '$_SESSION[KID]'";
                        $b = sqlsrv_query($conn, $a);
                        $c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC);
                        $j = $c[0];

                        //approval
                        $x = "select* from [dbo].[UserMemberKoperasi] where KID = '$_SESSION[KID]' and UserID = '$_SESSION[UserID]'";
                        $y = sqlsrv_query($conn, $x);
                        $z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC);
                        if($z != null){
                            $kode = $z[2];
                        }
                        else{
                            $kode = $z[1];
                        }
                        $a = "select count(*) from [KoneksiKoperasiBaronang].[dbo].[RequestPage] where KodeUser = '$kode' and Status = '0' and TanggalExpired >= '$date' or KodeUser = '$_SESSION[UserID]' and Status = '0' and TanggalExpired >= '$date'";
                        ?>
                        <li class="dropdown messages-menu">
                            <a href="listtrx.php">
                                <i class="fa fa-tasks"></i>
                                <?php if($j > 0){ ?>
                                    <span class="label label-success"><?php echo $j; ?></span>
                                <?php } ?>
                            </a>
                        </li>
                    <?php
                    }
                    else {
                        $a = "select count(*) from [KoneksiKoperasiBaronang].[dbo].[RequestPage] where KodeUser = '$_SESSION[UserID]' and Status = '0' and TanggalExpired >= '$date' and Link in('identity_cs.php','identity_cas.php')";
                    }

                    $b = sqlsrv_query($conn, $a);
                    $c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC);
                    $m = $c[0];
                    ?>
                    <li class="dropdown messages-menu">
                        <a href="approval.php">
                            <i class="fa fa-bell-o"></i>
                            <?php if($m > 0){ ?>
                                <span class="label label-success"><?php echo $m; ?></span>
                            <?php } ?>
                        </a>
                    </li>
                    <li class="dropdown notifications-menu">
                        <a href="setting.php">
                            <i class="fa fa-gear"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </nav>

        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <ul class="nav">
                <?php if(isset($_SESSION['KID'])){ ?>
                    <li class="text-center col-xs-3" style="padding-left: 0;padding-right: 0;">
                        <a href="profile.php">
                            <i class="fa fa-user"></i> <?php echo $lang->lang('Profil', $conn); ?>
                        </a>
                    </li>
                    <li class="text-center col-xs-3" style="padding-left: 0;padding-right: 0;">
                        <a href="wallet.php">
                            <i class="fa fa-money"></i> <?php echo $lang->lang('Dompet', $conn); ?>
                        </a>
                    </li>
                    <li class="text-center col-xs-6" style="padding-left: 0;padding-right: 0;">
                        <a href="balance.php">
                            <i class="fa fa-home"></i>
                            <?php
                            $word = explode(' ', $_SESSION['KoperasiName']);
                            echo ucwords($word[0].' '.$word[1]);
                            ?>
                        </a>
                    </li>
                <?php } else { ?>
                    <li class="text-center col-xs-6" style="padding-left: 0;padding-right: 0;">
                        <a href="profile.php">
                            <i class="fa fa-user"></i> <?php echo $lang->lang('Profil', $conn); ?>
                        </a>
                    </li>
                    <li class="text-center col-xs-6" style="padding-left: 0;padding-right: 0;">
                        <a href="wallet.php">
                            <i class="fa fa-money"></i> <?php echo $lang->lang('Dompet', $conn); ?>
                        </a>
                    </li>
                <?php } ?>
            </ul>
        </nav>
    </header>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <br>
        <!-- <div class="load"></div> -->