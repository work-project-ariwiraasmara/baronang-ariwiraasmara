<?php require('header_new.php');?>
<?php require('sidebar-left.php');?>

<div class="animated fadeinup delay-1">
    <div class="page-content">

            <div class="">
                <div class="row m-1-0">
                    <?php
                    $arr = array();
                    $date = date('Y-m-d H:i:s');
                    if(isset($_SESSION['KID'])){
                        $x = "select* from [dbo].[UserMemberKoperasi] where KID = '$_SESSION[KID]' and UserID = '$_SESSION[UserID]'";
                        $y = sqlsrv_query($conn, $x);
                        $z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC);
                        if($z != null){
                            $kode = $z[2];
                        }
                        else{
                            $kode = $z[1];
                        }

                        $a = "select* from [dbo].[RequestPage] where KodeUser = '$kode' and Status = '0' and TanggalExpired >= '$date' or KodeUser = '$_SESSION[UserID]' and Status = '0' and TanggalExpired >= '$date'";
                    }
                    else{
                        $a = "select* from [dbo].[RequestPage] where KodeUser = '$_SESSION[UserID]' and Status = '0' and TanggalExpired >= '$date' and Link in('identity_cs.php','identity_cas.php')";
                    }
                    $b = sqlsrv_query($conns, $a);
                    while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                        array_push($arr, $c[0]);
                        ?>
                        <div class="col">
                            <span class="lead"><?php echo $c[8]; ?></span>
                            <br>
                            <!-- Button trigger modal -->
                            <a href="pin.php?app=<?php echo $c[0]; ?>">
                                <button type="button" class="btn btn-primary">
                                    <i class="ion-android-done"></i>
                                    <?php echo $lang->lang('Setujui', $conn); ?>
                                </button>
                            </a>
                            <a href="app-request.php?id=<?php echo $c[0]; ?>&p=<?php echo md5('n'); ?>"><button type="button" class="btn btn-danger btn-sm"><i class="fa fa-times"></i> <?php echo $lang->lang('Tolak', $conn); ?></button></a>
                        </div>
                    <?php } ?>

                    <?php if($arr == null){ ?>
                        <div class="col" style="text-align: center;">
                            <span class="lead"><?php echo $lang->lang('Kosong', $conn); ?></span>
                        </div>
                    <?php } ?>
                </div>
            </div>

    </div>
</div>

<?php require('footer_new.php');?>