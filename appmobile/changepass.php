<?php require('header_new.php');?>
<?php require('sidebar-left.php');?>

<div class="animated fadeinup delay-1">
    <div class="page-content">

        <div class="form-inputs">
            <form action="procchange.php" method="post">
                <div class="box box-solid">
                    <h3 class="uppercase">Ubah Password</h3> <br>

                    <div class="input-field">
                        <input type="password" class="form-control" id="old" name="old">
                        <label for="old">Password Lama</label>
                    </div> <br>

                    <div class="input-field">
                        <input type="password" class="form-control" id="new" name="new">
                        <label for="new">Password Baru</label>
                    </div> <br>

                    <div class="input-field">
                        <input type="password" class="form-control" id="retype" name="retype">
                        <label for="retype">Ulangi Password Baru</label>
                    </div>

                    <div class="input-field">
                        <button type="submit" class="waves-effect waves-light btn-large primary-color width-100">Ubah</button>
                    </div>
                </div>
            </form>
        </div>

    </div>
</div>

<?php require('footer_new.php');?>