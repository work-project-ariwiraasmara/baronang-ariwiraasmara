<?php require('header_new.php');?>
<?php require('sidebar-left.php');?>

<?php
if(!isset($_SESSION['KID'])){
    echo "<script language='javascript'>document.location='close.php';</script>";
}

if(!isset($_GET['acc'])){
    echo "<script language='javascript'>document.location='balance.php';</script>";
}
else{
    $cardno = '';
    $accno = '';
    $xx = "exec dbo.ListemoneyCard '$_SESSION[KID]','$_GET[acc]'";
    $yy = sqlsrv_query($conn, $xx);
    $zz = sqlsrv_fetch_array($yy, SQLSRV_FETCH_NUMERIC);
    if($zz != null){
        $cardno = $zz[4];
        $accno = $zz[0];
    }
    else{
        echo "<script language='javascript'>document.location='balance.php';</script>";
    }
}

?>

    <div class="animated fadeinup delay-1">
        <div class="page-content">
            <h3 class="uppercase"><?php echo $lang->lang('Blokir Kartu e-Money', $conn); ?></h3> <br>

            <div class="box-body">
                <form id="procpin" action="procblock.php" method="POST" class="form-horizontal">
                    <div class="col s12">
                        <input type="hidden" name="acc" value="<?php echo $accno; ?>" readonly>
                        <input type="hidden" name="card" value="<?php echo $cardno; ?>" readonly>
                        <div class="form-group">
                            <p class="text-center">
                                Apakah anda yakin akan memblokir kartu e-money <b><i><?php echo $accno; ?></i></b> dengan nomor kartu <b><?php echo $cardno; ?></b> ?
                            </p>
                        </div>
                        <div class="box-footer">
                            <p class="text-center">Masukan PIN anda untuk mengkonfirmasi</p>
                            <div class="col s12">
                                <input id="pin" name="pin" type="password" class="validate text-center" style="font-size: 2em;background: white;" readonly>
                            </div>
                            <div class="col s4" style="padding: 20px;"><button type="button" id="n1" class="btn btn-lg btn-default" style="border-radius: 40px;">1</button></div>
                            <div class="col s4" style="padding: 20px;"><button type="button" id="n2" class="btn btn-lg btn-default" style="border-radius: 40px;">2</button></div>
                            <div class="col s4" style="padding: 20px;"><button type="button" id="n3" class="btn btn-lg btn-default" style="border-radius: 40px;">3</button></div>
                            <div class="col s4" style="padding: 20px;"><button type="button" id="n4" class="btn btn-lg btn-default" style="border-radius: 40px;">4</button></div>
                            <div class="col s4" style="padding: 20px;"><button type="button" id="n5" class="btn btn-lg btn-default" style="border-radius: 40px;">5</button></div>
                            <div class="col s4" style="padding: 20px;"><button type="button" id="n6" class="btn btn-lg btn-default" style="border-radius: 40px;">6</button></div>
                            <div class="col s4" style="padding: 20px;"><button type="button" id="n7" class="btn btn-lg btn-default" style="border-radius: 40px;">7</button></div>
                            <div class="col s4" style="padding: 20px;"><button type="button" id="n8" class="btn btn-lg btn-default" style="border-radius: 40px;">8</button></div>
                            <div class="col s4" style="padding: 20px;"><button type="button" id="n9" class="btn btn-lg btn-default" style="border-radius: 40px;">9</button></div>
                            <div class="col s4" style="padding: 20px;"></div>
                            <div class="col s4" style="padding: 20px;"><button type="button" id="n0" class="btn btn-lg btn-default" style="border-radius: 40px;">0</button></div>
                            <div class="col s4" style="padding: 20px;"><button type="button" id="nb" class="btn btn-lg btn-default" style="border-radius: 40px;"><i class="fa fa-chevron-left"></i></button></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        function cekDigit(){
            var a = $('#pin').val();
            if(a.length == 6){
                $('#procpin').submit();
            }

            if(a.length > 6){
                $('#pin').val($('#pin').val().slice(0, -1));
            }
        }

        $('#n1').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '1');

            cekDigit();
        });
        $('#n2').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '2');

            cekDigit();
        });
        $('#n3').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '3');

            cekDigit();
        });
        $('#n4').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '4');

            cekDigit();
        });
        $('#n5').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '5');

            cekDigit();
        });
        $('#n6').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '6');

            cekDigit();
        });
        $('#n7').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '7');

            cekDigit();
        });
        $('#n8').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '8');

            cekDigit();
        });
        $('#n9').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '9');

            cekDigit();
        });
        $('#n0').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '0');

            cekDigit();
        });
        $('#nb').click(function(){
            $('#pin').val($('#pin').val().slice(0, -1));
        });
    </script>
<?php require('footer_new.php');?>
