<?php require('header_new.php');?>

    <div class="col">
        <img id="imgup" src ="images/logo_vertical.png" style="display: block;margin-left: auto;margin-right: auto;width: 40%;">
        <br>
        <div class="col-xs-12">
            <form id="procpin" action="sessionpin.php" method="post">
                <h5 class="text-center" style="color: white;">
                    <?php echo $lang->lang('Masukan PIN anda', $conn); ?>
                </h5>
                <div class="col-xs-12">
                    <input id="pin" name="pin" type="password" class="form-control text-center" style="font-size: 2em;background: transparent;color: white;" readonly>
                    <span class="pull-left"><a href="forgotpin.php" style="color: white;"><?php echo $lang->lang('Lupa PIN', $conn); ?> ?</a></span>
                </div>
                <div class="col-xs-12" style="display: block;margin-left: auto;margin-right: auto;">
                    <div class="col-xs-4" style="padding: 5% 0 10% 10%;"><button type="button" id="n1" class="btn btn-lg btn-default" style="border-radius: 40px;">1</button></div>
                    <div class="col-xs-4" style="padding: 5% 0 10% 10%;"><button type="button" id="n2" class="btn btn-lg btn-default" style="border-radius: 40px;">2</button></div>
                    <div class="col-xs-4" style="padding: 5% 0 10% 10%;"><button type="button" id="n3" class="btn btn-lg btn-default" style="border-radius: 40px;">3</button></div>
                    <div class="col-xs-4" style="padding: 0 0 10% 10%;"><button type="button" id="n4" class="btn btn-lg btn-default" style="border-radius: 40px;">4</button></div>
                    <div class="col-xs-4" style="padding: 0 0 10% 10%;"><button type="button" id="n5" class="btn btn-lg btn-default" style="border-radius: 40px;">5</button></div>
                    <div class="col-xs-4" style="padding: 0 0 10% 10%;"><button type="button" id="n6" class="btn btn-lg btn-default" style="border-radius: 40px;">6</button></div>
                    <div class="col-xs-4" style="padding: 0 0 10% 10%;"><button type="button" id="n7" class="btn btn-lg btn-default" style="border-radius: 40px;">7</button></div>
                    <div class="col-xs-4" style="padding: 0 0 10% 10%;"><button type="button" id="n8" class="btn btn-lg btn-default" style="border-radius: 40px;">8</button></div>
                    <div class="col-xs-4" style="padding: 0 0 10% 10%;"><button type="button" id="n9" class="btn btn-lg btn-default" style="border-radius: 40px;">9</button></div>
                    <div class="col-xs-4" style="padding: 0 0 10% 10%;"></div>
                    <div class="col-xs-4" style="padding: 0 0 10% 10%;"><button type="button" id="n0" class="btn btn-lg btn-default" style="border-radius: 40px;">0</button></div>
                    <div class="col-xs-4" style="padding: 0 0 10% 10%;"><button type="button" id="nb" class="btn btn-lg btn-default" style="border-radius: 40px;"><i class="fa fa-chevron-left"></i></button></div>
                </div>
            </form>
        </div>
    </div>

    <script type="text/javascript">
        function cekDigit(){
            var a = $('#pin').val();
            if(a.length == 6){
                $('#procpin').submit();
            }

            if(a.length > 6){
                $('#pin').val($('#pin').val().slice(0, -1));
            }
        }

        $('#n1').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '1');

            cekDigit();
        });
        $('#n2').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '2');

            cekDigit();
        });
        $('#n3').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '3');

            cekDigit();
        });
        $('#n4').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '4');

            cekDigit();
        });
        $('#n5').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '5');

            cekDigit();
        });
        $('#n6').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '6');

            cekDigit();
        });
        $('#n7').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '7');

            cekDigit();
        });
        $('#n8').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '8');

            cekDigit();
        });
        $('#n9').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '9');

            cekDigit();
        });
        $('#n0').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '0');

            cekDigit();
        });
        $('#nb').click(function(){
            $('#pin').val($('#pin').val().slice(0, -1));
        });
    </script>

<?php require('footer_new.php');?>