<?php
session_start();
include "connect.inc";

$acc = $_POST['acc'];
$amount = $_POST['amount'];

$json = array(
    'status'=>0,
    'message'=>'',
);

//kode trans loan
$q = "exec dbo.getKodeLoanAppNumber '$_SESSION[KID]','$_SESSION[MemberID]','$acc'";
$w = sqlsrv_query($conn, $q);
$e = sqlsrv_fetch_array( $w, SQLSRV_FETCH_NUMERIC);
$code = $e[0];

$a = "exec dbo.ProsesLoadSimulasi '$_SESSION[KID]','$code','$acc','$amount'";
$b = sqlsrv_query($conn, $a);
?>

<div class="col-sm-12">
    <div class="table-responsive">
        <table class="table table-striped">
            <tr>
                <th>No.</th>
                <th>Batas Tanggal Akhir</th>
                <th>Jumlah Pinjaman</th>
                <th>Pokok</th>
                <th>Bunga</th>
                <th>Jumlah Cicilan</th>
            </tr>
            <?php
            $x = "exec dbo.SimulasiLoanSearch '$_SESSION[KID]','$code'";
            $y = sqlsrv_query($conn, $x);
            while($z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC)){
                ?>
                <tr>
                    <td><?php echo $z[2]; ?></td>
                    <td><?php echo $z[1]->format('Y-m-d'); ?></td>
                    <td><?php echo number_format($z[3], 2); ?></td>
                    <td><?php echo number_format($z[4], 2); ?></td>
                    <td><?php echo number_format($z[5], 2); ?></td>
                    <td><?php echo number_format($z[6], 2); ?></td>
                </tr>
            <?php } ?>
        </table>
    </div>
</div>