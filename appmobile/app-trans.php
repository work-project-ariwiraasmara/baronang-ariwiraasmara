<?php require('header.php');?>

    <div class="col-sm-12">
        <div class="box box-solid">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="box-body">
                <div style="text-align: center;">
                    <img width="50" height="50" src="images/loading.gif">
                </div>
            </div>
        </div>
    </div>

<?php
require_once 'lib/googleLib/GoogleAuthenticator.php';

$amount = str_replace(',','',$_POST['amount']);
if($_POST['acc'] == '' || $_POST['acctrans'] == '' || $_POST['amount'] == '' || $_POST['pin'] == '' || !is_numeric($amount)){
    echo "<script>System.showToast('Harap lengkapi masukan');document.location='tregular_saving.php';</script>";
}
else{

    $acc = $_POST['acc'];
    $acctrans = $_POST['acctrans'];
    $note = $_POST['note'];
    $pin = md5($_POST['pin']);

    //jika di add ke rek fav
    $fav = $_POST['fav'];

    $a = "select* from [dbo].[UserPaymentGateway] where KodeUser = '$_SESSION[UserID]'";
    $b = sqlsrv_query($conn, $a);
    $c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC);
    if($c != null){
        if($pin == $c[12])
        {
            $qq = "exec [dbo].[RegularSavingAccSearchTransfer] '$_SESSION[KID]', '$_POST[acctrans]'";
            $ww = sqlsrv_query($conn, $qq);
            $ee = sqlsrv_fetch_array($ww, SQLSRV_FETCH_NUMERIC);
            if($ee != null){
                $x = "exec [dbo].[RegularSavingAccSearch] '$_SESSION[KID]', '$_SESSION[MemberID]','$acc'";
                $y = sqlsrv_query($conn, $x);
                $z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC);
                if($z != null){
                    $amacc = str_replace(',','',number_format($z[5],0));

                    $q = "exec [dbo].[RegularSavingTypeSearch] '$_SESSION[KID]', '$z[3]'";
                    $w = sqlsrv_query($conn, $q);
                    $e = sqlsrv_fetch_array($w, SQLSRV_FETCH_NUMERIC);

                    $min = $e[4];
                    $max = $e[5];
                    if($amacc >= $amount){
                        if($amacc >= $min+$amount){
                            if($amacc <= $max){
                                //kurangin pengirim
                                $sql = "exec [dbo].[ProsesRegularSavingTransfer] '$_SESSION[KID]','$acc',1,'$amount','$_SESSION[UserID]','$note'";
                                $exec = sqlsrv_query($conn, $sql);
                                if($exec){
                                    //tambah penerima
                                    $sql1 = "exec [dbo].[ProsesRegularSavingTransfer] '$_SESSION[KID]','$acctrans',0,'$amount','$_SESSION[UserID]','$note'";
                                    $exec1 = sqlsrv_query($conn, $sql1);
                                    if($exec1){
                                        //add rekening favorit
                                        if(isset($fav)){
                                            $fgh = "exec [dbo].[ListRegTransferList] '$_SESSION[KID]', '$_SESSION[MemberID]','$acctrans'";
                                            $hjk = sqlsrv_query($conn, $fgh);
                                        }

                                        messageAlert('Transfer berhasil dilakukan');
                                        echo "<script language='javascript'>document.location='notif.php';</script>";
                                    }
                                    else{
                                        echo "<script language='javascript'>System.showToast('Gagal melakukan transfer');document.location='tregular_saving.php';</script>";
                                    }
                                }
                                else{
                                    echo "<script language='javascript'>System.showToast('Gagal melakukan transfer');document.location='tregular_saving.php';</script>";
                                }
                            }
                            else{
                                echo "<script language='javascript'>System.showToast('Transaksi gagal. Melebihi maksimum jumlah transfer');document.location='tregular_saving.php';</script>";
                            }
                        }
                        else{
                            echo "<script language='javascript'>System.showToast('Transaksi gagal. Minimum saldo tabungan adalah ".number_format($min)."');document.location='tregular_saving.php';</script>";
                        }
                    }
                    else{
                        echo "<script language='javascript'>System.showToast('Saldo tabungan tidak mencukupi. Transaksi tidak dapat dilakukan');document.location='tregular_saving.php';</script>";
                    }
                }
                else{
                    echo "<script>document.location='tregular_saving.php';</script>";
                }
            }
            else{
                echo "<script>System.showToast('Akun tujuan tidak ditemukan');document.location='tregular_saving.php';</script>";
            }
        }
        else
        {
            echo "<script>System.showToast('PIN tidak benar. Transaksi tidak dapat dilakukan');document.location='tregular_saving.php';</script>";
        }
    }
    else{
        echo "<script language='javascript'>document.location='login.php';</script>";
    }
}
?>

<?php require('footer.php');?>