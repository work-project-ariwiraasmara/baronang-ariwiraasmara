<?php
session_start();
include "connect.inc";

$x = "exec [dbo].[LoanTypeSearch] '$_SESSION[KID]','$_POST[acc]'";
$y = sqlsrv_query($conn, $x);
$z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC);
if($z == null){
    echo 'Loan product not found.';
}
else{
    if($z[13] == 0){
        echo 'No need guarantor';
    }
    else{
?>

    <table class="table" id="membertable">
        <thead>
        <tr>
            <td>Harus ada <?php echo $z[14] ?> penjamin</td>
            <td></td>
        </tr>
        <tr>
            <td>
                <input type="hidden" id="userid" value="<?php echo $_SESSION['UserID']; ?>" readonly>
                <input type="hidden" id="maxpen" value="<?php echo $z[14] ?>" readonly>
                <input type="number" id="uid" class="form-control" placeholder="User ID Baronang App">
            </td>
            <td>
                <button type="button" class="btn btn-flat btn-primary btn-member">Tambah</button>
            </td>
        </tr>
        </thead>
        <tbody>

        </tbody>
    </table>

<?php } } ?>

<script type="text/javascript">
    var tampung = [];

    $('.btn-member').click(function(){
        var uid = $('#uid').val();
        var userid = $('#userid').val();
        var maxpen = $('#maxpen').val();
        var rows = document.getElementById("membertable").getElementsByTagName("tbody")[0].getElementsByTagName("tr").length;

        if(jQuery.inArray(uid, tampung) !== -1){
            System.showToast('Penjamin sudah ditambahkan');
            return false;
        }
        else if(uid == userid){
            System.showToast('Silahkan masukan penjamin yang lain');
            return false;
        }
        else if(rows == maxpen){
            System.showToast('Melebihi jumlah penjamin yang dibutuhkan');
            return false;
        }
        else{
            $.ajax({
                url : "ajax_addmember.php",
                type : 'POST',
                dataType : 'json',
                data: { uid: uid},   // data POST yang akan dikirim
                success : function(data) {
                    if(data.status == 1){
                        tampung.push(data.uid);
                        $("#membertable tbody").append("<tr>" +
                            "<td><input type='hidden' name='uid[]' value="+ data.uid +">" + data.uid + "</td>" +
                            "<td><a class='btn btn-default btn-flat btn-sm text-red remove' onclick='$(this).parent().parent().remove();'><i class='fa fa-trash-o'></i></a></td>" +
                            "</tr>");
                    }
                    else if(data.status == 2){
                        System.showToast(data.message);
                        return false;
                    }
                    else{
                        System.showToast('Gagal');
                        return false;
                    }
                },
                error : function() {
                    System.showToast('Coba lagi');
                    return false;
                }
            });
        }
    });
</script>