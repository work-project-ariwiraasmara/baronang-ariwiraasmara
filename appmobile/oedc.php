<?php require('header_new.php');?>
<?php require('sidebar-left.php');?>

<?php
if(!isset($_SESSION['KID'])){
    echo "<script language='javascript'>document.location='close.php';</script>";
}
?>

    <div class="animated fadeinup delay-1">
        <div class="page-content">

            <h3 class="uppercase"><?php echo $lang->lang('Buka EDC', $conn); ?></h3> <br>

            <div class="form-inputs">
                <form action="procauth.php" method="POST" class="form-horizontal">
                    <div class="col s12">

                        <div class="">
                            <label><?php echo $lang->lang('Akun Tabungan', $conn); ?></label>
                            <div class="">
                                    <?php
                                    $a = "exec [dbo].[ListRegularSavingBal] '$_SESSION[KID]','$_SESSION[MemberID]'";
                                    $b = sqlsrv_query($conn, $a);
                                    while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                                    ?>
                                        <input type="radio" name="acc" class="minimal" id="regacc-<?php echo $c[2]; ?>" value="<?php echo $c[2]; ?>">
                                        <label for="regacc-<?php echo $c[2]; ?>"><b><?php echo $c[2]; ?></b> - <?php echo $c[4]; ?><br><span class="">Rp. <?php echo number_format($c[5]); ?></span></label>
                                        <br>
                                    <?php } ?>
                            </div>
                            <input type="hidden" name="kid" value="<?php echo $_SESSION['KID']; ?>" readonly>
                            <input type="hidden" name="mid" value="<?php echo $_SESSION['MemberID']; ?>" readonly>
                            <input type="hidden" name="amount" value="1" readonly>
                            <input type="hidden" name="jenis" value="16" readonly>
                        </div>

                        <div class="m-t-20">
                            <b><u>Info</u></b><br>
                            <div class="">
                                Akun tabungan yang dipilih akan dgunakan sebagai pendapatan setiap transaksi yang dilakukan malalui Baronang EDC
                                <br>
                                <br>
                                Download Baronang EDC di PlayStore untuk dapat menggunakan EDC
                            </div>
                        </div>

                        <div class="m-t-30">
                            <button type="submit" class="btn btn-block primary-color"><?php echo $lang->lang('Simpan', $conn); ?></button>
                        </div>

                    </div>
                </form>
            </div>

        </div>
    </div>

<?php require('footer_new.php');?>