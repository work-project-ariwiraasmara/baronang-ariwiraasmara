<?php require('header.php');?>

    <div class="col-sm-12">
        <div class="box box-solid">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="box-body">
                <div style="text-align: center;">
                    <img width="50" height="50" src="images/loading.gif">
                </div>
            </div>
        </div>
    </div>

<?php
require_once 'lib/googleLib/GoogleAuthenticator.php';

if($_POST['kid'] == '' || $_POST['mid'] == '' || $_POST['acc'] == '' || $_POST['jenis'] == ''){
    echo "<script language='javascript'>history.go(-1);</script>";
}
else{
    $kid = $_POST['kid'];
    $mid = $_POST['mid'];
    $acc = $_POST['acc'];
    $jenis = $_POST['jenis'];

    $pin = md5($_POST['pin']);

    $a = "select* from [dbo].[UserPaymentGateway] where KodeUser = '$_SESSION[UserID]'";
    $b = sqlsrv_query($conn, $a);
    $c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC);
    if($c != null){
        if ($pin == $c[12])
        {
            $x = "exec [dbo].[RegularSavingAccSearch] '$kid','$_SESSION[MemberID]','$acc'";
            $y = sqlsrv_query($conn, $x);
            $z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC);
            if($z != null){
                $param = $kid.date('ymdHisms');
                $key = getKey($param);

                $sql = "exec Gateway.[dbo].[ProsesEDCList] '$kid', '$_SESSION[UserID]','$_SESSION[MemberID]','$acc','$key'";
                $exec = sqlsrv_query($conn, $sql);
                if($exec){
                    messageAlert('Berhasil membuat EDC. Silahkan download Baronang EDC di PlayStore');
                    echo "<script language='javascript'>document.location='notif.php';</script>";
                }
                else{
                    echo "<script>System.showToast('Gagal membuka edc');history.go(-2);</script>";
                }
            }
            else{
                echo "<script>System.showToast('Akun tabungan tidak ditemukan');history.go(-2);</script>";
            }
        }
        else
        {
            echo "<script>System.showToast('PIN tidak benar. Transaksi tidak dapat dilakukan');history.go(-2);</script>";
        }
    }
    else{
        echo "<script language='javascript'>document.location='login.php';</script>";
    }
}

function getKey($param){
    $options = [
        'cost' => 11,
        'salt' => mcrypt_create_iv(22, MCRYPT_DEV_URANDOM),
    ];
    $key = password_hash($param, PASSWORD_BCRYPT, $options);

    return $key;
}

?>

<?php require('footer.php');?>