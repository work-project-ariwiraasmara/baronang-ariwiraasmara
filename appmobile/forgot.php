<?php require('header_new.php');?>

<?php
$_SESSION['url'] = 'login.php';
?>

    <div id="content" class="primary-color login">

        <div id="toolbar" class="tool-login primary-color animated fadeindown">
            <a href="javascript:history.back()" class="open-left">
                <i class="ion-android-arrow-back"></i>
            </a>
        </div>

        <div class="login-form animated fadeinup delay-2 primary-color" style="color: #fff; margin-top: 0px; margin-right: -7px; margin-left: -7px; margin-bottom: 10px;">

            <div style="position: absolute;top: 15%;">
                <center><img id="imgup" src ="images/logo_vertical.png" style="width: 40%;" align="middle"></center>
            </div>

            <div class="toBottom" style="margin-top: 10px;">
                <label><?php echo $lang->lang('Lupa Password', $conn); ?></label>

                <form action="procforgot.php" method="post" autocomplete="off">

                    <div class="input-field">
                        <label for="email">Email</label>
                        <input id="email" type="email" name="email" class="validate" value="<?php echo $_SESSION['Email']; ?>" style="background: transparent;color: white;">
                        <span class="fa fa-envelope-o form-control-feedback" aria-hidden="true" style="color: white;"></span>
                    </div>

                    <div class="input-field">
                        <?php echo $lang->lang('Harap masukan email dengan benar. Konfirmasi perubahan password akan dikirim melalui email.', $conn); ?>
                    </div>

                    <div style="margin-top: 30px;">
                        <button type="submit" class="waves-effect waves-light width-100 m-b-20 animated bouncein delay-4" style="background: #00c0ef; border-radius: 50px; color: #fff; padding: 15px; border: none;"><?php echo $lang->lang('Kirim', $conn); ?></button>
                    </div>
                    <div style="margin-top: 10px;">
                        <a href="login.php"><button type="button" class="waves-effect waves-light width-100 m-b-20 animated bouncein delay-4" style="background: #fff; border-radius: 50px; color: #000; padding: 15px; border: none;"><?php echo $lang->lang('Batal', $conn); ?></button></a>
                    </div>

                </form>
            </div>

        </div>

    </div>

    <script type="text/javascript">
        $( "#email" ).focus(function() {
            $("#imgup").hide();
        });

        $( "#email" ).blur(function() {
            $("#imgup").show();
        });
    </script>

<?php require('footer_new.php');?>