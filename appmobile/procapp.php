<?php require('header.php');?>

    <div class="col-sm-12">
        <div class="box box-solid">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="box-body">
                <div style="text-align: center;">
                    <img width="50" height="50" src="images/loading.gif">
                </div>
            </div>
        </div>
    </div>

<?php
require_once 'lib/googleLib/GoogleAuthenticator.php';

if($_POST['kid'] == '' || $_POST['mid'] == '' || $_POST['acc'] == '' || $_POST['jenis'] == '' || $_POST['amount'] == '' || $_POST['amount'] <= 0){
    echo "<script language='javascript'>history.go(-1);</script>";
}
else{

    $kid = $_POST['kid'];
    $mid = $_POST['mid'];
    $acc = $_POST['acc'];
    $amount = $_POST['amount'];
    $jenis = $_POST['jenis'];
    //$gac = $_POST['gac'];
    $pin = md5($_POST['pin']);

    $a = "select* from [dbo].[UserPaymentGateway] where KodeUser = '$_SESSION[UserID]'";
    $b = sqlsrv_query($conn, $a);
    $c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC);
    if($c != null){
        $ga = new GoogleAuthenticator();
        //$checkResult = $ga->verifyCode($c[11], $gac, 2);    // 2 = 2*30sec clock tolerance
        if ($pin == $c[12])
        {
            //$_SESSION['googleCode'] = $gac;

            $sql = "exec [dbo].[ProsesUserTokenTransaksi] '$kid', '$_SESSION[UserID]','$acc','$jenis','$amount'";
            $exec = sqlsrv_query($conn, $sql);
            if($exec){
                messageAlert('Berhasil membuat ticketing transaksi. Silahkan ke koperasi yang dituju untuk melanjutkan transaksi');
                echo "<script language='javascript'>document.location='notif.php';</script>";
            }
            else{
                echo "<script>System.showToast('Gagal membuat transaksi');history.go(-2);</script>";
            }
        }
        else
        {
            echo "<script>System.showToast('PIN tidak benar. Transaksi tidak dapat dilakukan');history.go(-2);</script>";
        }
    }
    else{
        echo "<script language='javascript'>document.location='login.php';</script>";
    }
}
?>

<?php require('footer.php');?>