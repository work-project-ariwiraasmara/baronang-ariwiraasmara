<?php require('header_new.php');?>

<?php
if($_SESSION['UserID'] != ''){
    unset($_SESSION['url']);
    echo "<script language='javascript'>Intent.openActivity('FingerprintActivity','session.php');</script>";
    //echo "<script language='javascript'>document.location='session.php';</script>";
}
else{
?>

    <div id="content" class="primary-color login">
        <div class="login-form animated delay-2 primary-color" style="color: #fff; margin-top: 0px; margin-right: -7px; margin-left: -7px; margin-bottom: 10px;">

            <div style="position: absolute;top: 10%;">
                <center><img id="imgup" src ="images/logo_vertical.png" style="width: 40%;" align="middle"></center>
            </div>

            <div class="toBottom">
                <form action="proclogin.php" method="post" autocomplete="off">
                    <div class="input-field" style="text-align: center;">
                        <?php
                        if($_SESSION['message'] != '' and $_SESSION['time'] != ''){
                            echo $_SESSION['message'];
                        }
                        ?>
                    </div>

                    <div class="input-field">
                        <i class="ion-android-contact prefix"></i>
                        <label for="email">Email</label>
                        <input type="email" name="email" id="email" class="validate" value="<?php echo $_SESSION['Email']; ?>" >
                    </div>

                    <div class="input-field">
                        <i class="ion-android-lock prefix"></i>
                        <label for="email">Password</label>
                        <input type="password" name="password" id="password" class="validate">
                    </div>

                    <div class="input-field">
                        <span class="left"><a href="forgotpin.php"><?php echo $lang->lang('Lupa PIN', $conn); ?> ?</a></span>
                        <span class="right"><a href="forgot.php"><?php echo $lang->lang('Lupa Password', $conn); ?> ?</a></span>
                    </div>

                    <button type="submit" class="waves-effect waves-light width-100 m-b-20 animated bouncein delay-4" style="margin-top: 30px; background: #00c0ef; border-radius: 50px; color: #fff; padding: 15px; border: none;"><?php echo $lang->lang('Masuk', $conn); ?></button>
                    <button type="button" id="btn-register" class="waves-effect waves-light width-100 m-b-20 animated bouncein delay-4" style="margin-top: 5px; background: #fff; border-radius: 50px; color: #000; padding: 15px; border: none;"><?php echo $lang->lang('Daftar', $conn); ?></button>
                </form>
            </div>

        </div>

    </div>

<?php } ?>

    <script type="text/javascript">
        $('#btn-register').click(function(){
            Intent.openActivity('LoginActivity','register.php');
        });

        $( "#email" ).focus(function() {
            $("#imgup").hide();
        });

        $( "#password" ).focus(function() {
            $("#imgup").hide();
        });

        $( "#email" ).blur(function() {
            $("#imgup").show();
        });

        $( "#password" ).blur(function() {
            $("#imgup").show();
        });

        $('#eye1').click(function(){
            $("#password").prop('type','text');
            $('#eye1').hide();
            $('#eye2').removeClass('hide');
        });

        $('#eye2').click(function(){
            $("#password").prop('type','password');
            $('#eye2').addClass('hide');
            $('#eye1').show();
        });
    </script>

<?php require('footer_new.php');?>