<?php
include 'connect-card.php';

if(isset($_COOKIE['LocationID']) || isset($_COOKIE['BlockID']) || isset($_COOKIE['UserID']) || isset($_COOKIE['UserName']) || isset($_COOKIE['Email'])) { ?>
<!DOCTYPE html>
<html>
	<head>
		<title>Sky Parking</title>
		<meta content="IE=edge" http-equiv="x-ua-compatible">
	    <meta content="initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" name="viewport">
	    <meta content="yes" name="apple-mobile-web-app-capable">
	    <meta content="yes" name="apple-touch-fullscreen">
		<link rel="manifest" href="manifest.json" />
	    <link href='static/images/Logo-fish-icon.png' type='image/x-icon'/>
	    <link href='static/images/Logo-fish-icon.png' rel='shortcut icon'/>
	    <!-- Fonts -->
	    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
	    <!-- Icons -->
	    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" media="all" rel="stylesheet" type="text/css">
	    <!-- Styles -->
	    <link href="css/keyframes.css" rel="stylesheet" type="text/css">
	    <link href="css/materialize.min.css" rel="stylesheet" type="text/css">
	    <link href="css/swiper.css" rel="stylesheet" type="text/css">
	    <link href="css/swipebox.min.css" rel="stylesheet" type="text/css">
	    <link href="css/style.css" rel="stylesheet" type="text/css">

	    <script type="text/javascript" src="js/jquery-2.1.0.min.js"></script>

	    <script type="text/javascript">
	    	$(function () {
				window.onload = $('#play').click();
			});
	    </script>
	</head>
	<body>

		<div class="m-scene" id="main">
			<div class="page" id="content">

				<!-- Toolbar -->
			    <div id="toolbar" class="primary-color">
			        <span class="title"> Sky Parking - Scan Card </span>

			        <div class="right" style="margin-right: 20px;">
			            <a href="#" id="exitscan" onclick="window.close();">
			                <i class="ion-android-cancel"></i>
			            </a>
			        </div>
			    </div>

			    <div class="animated delay-1">
					<div class="p-20 txt-black">
						<div class="p-20" id="form-camera">
			            	<h3 class="uppercase">Scan QR Code/Barcode</h3><br>

			            	<div class="hide">
			            		<div class="bold"><b>Pilih Kamera</b></div>
				            	<select class="browser-default txt-black" id="camera-select"></select>
				        	</div>
				            <br>
				            <div class="">
				            	<a href="#notif-success" id="toSuccess" class="hide modal-trigger">to Success</a>
				            	<a href="#notif-fail" id="toFail" class="hide modal-trigger">to Fail</a>
				                <div class="row">
				                    <div class="col s6">
				                    	<button title="Image shoot" class="hide" id="grab-img" type="hidden" data-toggle="tooltip"><span class="glyphicon glyphicon-picture"></span></button>
				                    	<button title="Decode Image" class="hide" id="decode-img" type="hidden" data-toggle="tooltip"><span class="glyphicon glyphicon-upload"></span></button>
				                        <button title="Start Scan QR Barcode" class="waves-effect waves-light btn-large primary-color width-100" id="play" type="button">Start</button>
				                    </div>
				                    <div class="col s6">
				                        <button title="Cek Kartu" id="stop" class="waves-effect waves-light btn-large primary-color width-100">Add</button>
				                    </div>
				                </div>

				                <div class="row">
				                	<div class="col s12 hide" id="panel_result">
				                		<p class="center">
				                			<h3 class="uppercase txt-black center" id="res-check"></h3>
				                		</p>	
				                	</div>

				                	<div class="col s6">
				                        <div class="thumbnail" id="result">
				                            <img id="scanned-img" src="">
				                            <div class="caption">
				                                <p class="bold center" class="hide" id="scanned-QR"></p>
				                                <input type="hidden" id="result-code">
				                            </div>
				                        </div>
				                    </div>

				                    <div class="col s6">
				                    	<canvas id="webcodecam-canvas" style="width: 100%;"></canvas>
				                        <div class="scanner-laser laser-rightBottom" style="opacity: 0.5;"></div>
				                        <div class="scanner-laser laser-rightTop" style="opacity: 0.5;"></div>
				                        <div class="scanner-laser laser-leftBottom" style="opacity: 0.5;"></div>
				                        <div class="scanner-laser laser-leftTop" style="opacity: 0.5;"></div>

				                        <div class="hide" style="width: 100%;">
				                            <label id="zoom-value" width="100">Zoom: 2</label>
				                            <input id="zoom" onchange="Page.changeZoom();" type="range" min="10" max="30" value="20">
				                            <label id="brightness-value" width="100">Brightness: 0</label>
				                            <input id="brightness" onchange="Page.changeBrightness();" type="range" min="0" max="128" value="0">
				                            <label id="contrast-value" width="100">Contrast: 0</label>
				                            <input id="contrast" onchange="Page.changeContrast();" type="range" min="0" max="64" value="0">
				                            <label id="threshold-value" width="100">Threshold: 0</label>
				                            <input id="threshold" onchange="Page.changeThreshold();" type="range" min="0" max="512" value="0">
				                            <label id="sharpness-value" width="100">Sharpness: off</label>
				                            <input id="sharpness" onchange="Page.changeSharpness();" type="checkbox">
				                            <label id="grayscale-value" width="100">grayscale: off</label>
				                            <input id="grayscale" onchange="Page.changeGrayscale();" type="checkbox">
				                            <br>
				                            <label id="flipVertical-value" width="100">Flip Vertical: off</label>
				                            <input id="flipVertical" onchange="Page.changeVertical();" type="checkbox">
				                            <label id="flipHorizontal-value" width="100">Flip Horizontal: off</label>
				                            <input id="flipHorizontal" onchange="Page.changeHorizontal();" type="checkbox">
				                        </div>

				                    </div>
				                </div>
				            </div>
			        	</div>
	        		</div>
	        	</div>

	        	<div class="modal borad-20" id="notif-success">
			       	<div class="modal-content choose-date txt-white" style="background: #49f;">
					    <h1 class="uppercase txt-white center">
					    	<a href="sky_card.php" class="modal-close"><i class="ion-android-done txt-white"></i></a><br>
					    	Berhasil menambah kartu!
						</h1>
					</div>
			    </div>

			    <div class="modal borad-20" id="notif-fail">
			       	<div class="modal-content choose-date txt-white" style="background: #f00;">
					    <h1 class="uppercase txt-white center">
					    	<a href="#" class="modal-close"><i class="ion-android-cancel txt-white"></i></a><br>
					    	Data kartu sudah ada!<br><br>Tidak bisa menambah data dengan kartu ini!
						</h1>
					</div>
			    </div>
	        	
	        	<script type="text/javascript" src="lib/webcodecam_js/qrcodelib.js"></script>
				<script type="text/javascript" src="lib/webcodecam_js/webcodecamjs.js"></script>
				<script type="text/javascript" src="lib/webcodecam_js/main.js"></script>

				<script type="text/javascript">
					$('#play').click(function(){
						if($('#webcodecam-canvas').hasClass('hide')) {
							//document.getElementById("webcodecam-canvas").getContext("2d").drawImage("", 10, 10);
							$('#webcodecam-canvas').removeClass('hide');
							$('#panel_result').addClass('hide');
						}
					})

					$('#stop').click(function(){
						var locid 	= "<?php echo @$_COOKIE['LocationID']; ?>";
						var blokid 	= "<?php echo @$_COOKIE['BlockID']; ?>";
						var userid 	= "<?php echo @$_COOKIE['UserID']; ?>";
						var code 	= $('#scanned-QR').text().substr(9);
						var link 	= "link";
						//alert(code);
						$('#webcodecam-canvas').addClass('hide');

						$.ajax({
							url : "ajax_ceklinkcard.php",
							type : 'POST',
							data : {
								locid : locid,
								blokid : blokid,
								userid : userid,
								link : link,
								qr : code
							},
							success : function(data) {
								$('#panel_result').removeClass('hide');
								//$('#res-check').html(data);
								
								if(data == 'Success') {
									$(function () {
								        window.onload = $('#toSuccess').click();
								    });
								    window.setTimeout(function () {
								        window.location.href="sky_card.php?";
								    }, 3000);
								}
								else {
									$(function () {
								        window.onload = $('#toFail').click();
								    });
								}
	
							},
							error : function() {
								$('#panel_result').removeClass('hide');
								$('#res-check').html('Terjadi kesalahan dalam cek data QR!');
							}
						});
					});
				</script>
		<?php require('blank-footer.php'); ?>
	</body>
</html>
<?php
}
?>