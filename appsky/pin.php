<?php
require('blank-header.php');
?>

	<div class="animated delay-1">
		<div class="form-inputs txt-white">

			<div class="center">Masukan PIN Anda</div>

			<div class="bottom center">
				<div class="m-b-20 p-50">
					Untuk alasan keamanan, silahkan masukan kembali PIN Anda <br> <br>

					<a href="forgotpin.php" class="bold">Lupa PIN?</a>
				</div>
				
				<form action="" method="post">
					
					<input type="text" name="txt_pin" id="txt_pin" class="validate center" style="font-size: 30px; border: none;" readonly>

					<div class="row p-10">
						<div class="col s4 center">
							<button type="button" name="btnpin_1" id="btnpin_1" class="txt-white width-100 waves-effect waves-light" style="border: none; font-size: 30px; background: transparent;">1</button>
						</div>

						<div class="col s4 center">
							<button type="button" name="btnpin_2" id="btnpin_2" class="txt-white width-100 waves-effect waves-light" style="border: none; font-size: 30px; background: transparent;">2</button>
						</div>

						<div class="col s4 center">
							<button type="button" name="btnpin_3" id="btnpin_3" class="txt-white width-100 waves-effect waves-light" style="border: none; font-size: 30px; background: transparent;">3</button>
						</div>

						<div class="col s4 center">
							<button type="button" name="btnpin_4" id="btnpin_4" class="txt-white width-100 waves-effect waves-light" style="border: none; font-size: 30px; background: transparent;">4</button>
						</div>

						<div class="col s4 center">
							<button type="button" name="btnpin_5" id="btnpin_5" class="txt-white width-100 waves-effect waves-light" style="border: none; font-size: 30px; background: transparent;">5</button>
						</div>

						<div class="col s4 center">
							<button type="button" name="btnpin_6" id="btnpin_6" class="txt-white width-100 waves-effect waves-light" style="border: none; font-size: 30px; background: transparent;">6</button>
						</div>

						<div class="col s4 center">
							<button type="button" name="btnpin_7" id="btnpin_7" class="txt-white width-100 waves-effect waves-light" style="border: none; font-size: 30px; background: transparent;">7</button>
						</div>

						<div class="col s4 center">
							<button type="button" name="btnpin_8" id="btnpin_8" class="txt-white width-100 waves-effect waves-light" style="border: none; font-size: 30px; background: transparent;">8</button>
						</div>

						<div class="col s4 center">
							<button type="button" name="btnpin_9" id="btnpin_9" class="txt-white width-100 waves-effect waves-light" style="border: none; font-size: 30px; background: transparent;">9</button>
						</div>

						<div class="col s4 center">
							<div style="background: transparent; color: transparent;">-</div>
						</div>

						<div class="col s4 center">
							<button type="button" name="btnpin_0" id="btnpin_0" class="txt-white width-100 waves-effect waves-light" style="border: none; font-size: 30px; background: transparent;">0</button>
						</div>

						<div class="col s4 center">
							<button type="button" name="btnpin_del" id="btnpin_del" class="txt-white width-100 waves-effect waves-light" style="border: none; font-size: 30px; background: transparent;">DEL</button>
						</div>


					</div>

				</form>

				<button type="button" name="logout" id="logout" class="btn btn-large width-100 waves-effect waves-light m-t-10 primary-color">Ganti ke akun lain</button>	
			</div>

		</div>
	</div>

<?php
require('blank-footer.php');
?>