<?php
require('blank-header.php');
?>

	<div class="animated delay-1">
		<div class="form-inputs txt-white">


			<div class="bottom center">
				<div class="m-b-20 p-50">
					
					<div id="accrek" class="bold" style="font-size: 23px;">Saat Belum Bisa Menampilkan Informasi</div>
					<div id="saldo" class="bold m-t-10" style="font-size: 20px;">0 point</div>

				</div>

				<button type="button" name="back" id="back" class="btn btn-large width-100 waves-effect waves-light m-t-10 primary-color" onclick="window.location.href = 'index.php'">Kembali</button>	
			</div>

		</div>
	</div>

<?php
require('blank-footer.php');
?>