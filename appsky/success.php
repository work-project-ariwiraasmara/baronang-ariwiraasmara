<?php
require('header-success.php');
?>

	<div class="primary-color p-20" style="width: 100%; height: 100%;">
		
		<div class="center txt-white">
			<?php
			if(@$_GET['status'] == '1') { ?>
				<i class="ion-android-cancel" style="color: #f00; font-size: 150px;"></i>
				<?php
			}
			else { ?>
				<i class="ion-android-checkmark-circle" style="font-size: 150px;"></i>
				<?php
			}
			?>
			
		</div>

		<div class="center txt-white" style="font-size: 20px; margin-top: 50px;">
			<?php
			if(@$_GET['status'] == '1') { ?>
				<div class=""><?php echo @$_GET['message']; ?></div>
				<?php
			}
			else { ?>
				<div class="bold italic" style="padding: 5px; background: #fff; color: #f00;">Transaksi Berhasil!</div>
				<?php
			}
			?>
			
		</div>

		<div class="bottom p-20">
			<a href="dashboard.php" class="btn btn-large width-100 waves-effect waves-light" style="background: #fff; color: #00f; border-radius: 30px;">
				<div class="m-t-10">
					Kembali
				</div>
			</a>
		</div>

	</div>

<?php
require('footer.php');
?>