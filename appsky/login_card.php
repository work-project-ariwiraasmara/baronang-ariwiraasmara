<body class="primary-color">
    <div class="m-scene" id="main"> <!-- Main Container -->
        <!-- Page Content -->
        <div id="content">
        <?php
        //error_reporting(0);
        //session_start();
        //include 'lang.lib.php';
        ?>

		<div class="animated delay-1">
			<div class="form-inputs txt-white">

				<div class="m-t-10 top">
					<center>
						<img src="card/logo skyparking 1.jpeg" style="width: 30%; height: 30%;">
					</center>
				</div>

				<div class="bottom p-20">
					<form action="" method="post">

						<div class="input-field">
							<label for="username" class="txt-white">Username</label>
							<input type="text" name="username" id="username" class="validate">
						</div>

						<div class="input-field">
							<label for="password" class="txt-white">Password</label>
							<input type="password" name="password" id="password" class="validate">
						</div>

						<a href="forgotpass.php" class="txt-white right m-t-10 m-b-10">Lupa Password?</a>

						<button type="submit" name="login" id="login" class="btn btn-large width-100 waves-effect waves-light m-t-10" style="background: #fff; color: #00f; border: 1px solid #fff; border-radius: 100px;">Login</button>
					
						<div class="txt-white italic center m-t-10">Powered By Baronang</div>
					</form>
				</div>

				<a href="#notif-warning" id="toWarning" class="hide modal-trigger">Warning!</a>

				<?php
				if(isset($_POST['login'])) {
					$user = @$_POST['username'];
					$pass = sha1( md5( crypt(@$_POST['password'], '$6$rounds=999999999') ) );

					$sql1 = "SELECT LocationID, BlockID, UserID, UserName, Email from [dbo].[LocationCardLogin] where UserName='$user' and Password='$pass'";
					//echo $sql1;
					$qry1 = sqlsrv_query($conn, $sql1);
					$data = sqlsrv_fetch_array($qry1);
					//echo count($data[0]);
					if( count($data[0]) > 0 ) {

						setcookie('LocationID', $data[0], 		time() + (1 * 24 * 60 * 60), "/"); // 60 * 60 * 24 * 1 = 1 day
						setcookie('BlockID', 	$data[1], 		time() + (1 * 24 * 60 * 60), "/"); // 60 * 60 * 24 * 1 = 1 day
						setcookie('UserID', 	$data[2], 		time() + (1 * 24 * 60 * 60), "/"); // 60 * 60 * 24 * 1 = 1 day
						setcookie('UserName', 	$data[3], 		time() + (1 * 24 * 60 * 60), "/"); // 60 * 60 * 24 * 1 = 1 day
						setcookie('Email', 		$data[4], 		time() + (1 * 24 * 60 * 60), "/"); // 60 * 60 * 24 * 1 = 1 day
						setcookie('Device', 	DeviceType(), 	time() + (1 * 24 * 60 * 60), "/"); // 60 * 60 * 24 * 1 = 1 day
						header('location: ?');
					}
					else { ?>
						<script type="text/javascript">
							//alert('Username / Password Salah! Silahkan Coba Lagi!');
							$(function () {
						        window.onload = $('#toWarning').click();
						    });
						</script>
						<?php
					}
				}
				?>

			</div>
		</div>

		<div class="modal borad-20" id="notif-warning">
	       	<div class="modal-content choose-date txt-white" style="background: #f00;">
			    <h1 class="uppercase txt-white center">
			    	<a href="#" class="modal-close"><i class="ion-android-cancel txt-white"></i></a><br>
			    	Username / Password Salah!<br>
			    	Silahkan Coba Lagi!
				</h1>
			</div>
	    </div>

<?php
require('blank-footer.php');
?>