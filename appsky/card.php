<?php
require('header.php');
require('sidebar-left.php');

$sqlp = "SELECT NamaUser from [PaymentGateway].[dbo].[UserPaymentGateway] where KodeUser='$UserID'";
$queryp = sqlsrv_query($conn, $sqlp);
$profil = sqlsrv_fetch_array($queryp);

$sql = "SELECT * from [PaymentGateway].[dbo].[UserMemberKoperasi] where KID='700097'";
$query = sqlsrv_query($conn, $sql);
$data = sqlsrv_fetch_array($query);
?>

	<!-- Toolbar -->
    <div id="toolbar" class="primary-color">
        <div class="open-left" id="open-left" data-activates="slide-out-left">
            <i class="ion-android-menu"></i>
        </div>

        <span class="title">My Card</span>

        <div class="right" id="right" style="margin-right: 20px;">
            <a href="#bellnotif" class="modal-trigger">
                <i class="ion-android-notifications"></i>
            </a>
        </div>
    </div>

    <div class="animated delay-1">
    	<div class="p-20 txt-black">
    		<img src="card/kartu.jpg" class="img-responsive">

	    	<div class="txt-white" style="margin-left: 5%; margin-top: -17%">
	    		<span><?php echo $data[1]; ?></span><br>
	    		<span><?php echo $profil[0]; ?></span>
	    	</div>	
    	</div>

    	<div class="p-20 m-t-15p">
    		<div class="row">
    			<div class="col s8">
	    			<div>Sudah punya kartu?</div>
	    			<div style="color: #555;">Link kartu Anda untuk dapat melihat history</div> 
	    		</div>
	    			
	    		<div class="col s4">
	    			<button id="ocam" class="right btn primary-color waves-effect waves-light" style="border-radius: 50px;" onclick="window.open('kamera.php')">Link Card</button>
	    		</div>
    		</div>
    	</div>

        

    	<div class="m-t-30 p-20" style="margin-top: 0px;">
    		<?php
    		$sql2 = "SELECT * from [dbo].[MemberCard] where MemberID='".$profil[0]."' and Status='1' and isLink='1'";
            //echo $sql2;
    		//$sql2 = "SELECT * from [dbo].[MemberCard] where MemberID='0006' and Status='1' and isLink='1'";
			$query2 = sqlsrv_query($conn, $sql2);
			while( $data2 = sqlsrv_fetch_array($query2) ) { ?>
				<div style="margin-top: 50px;">
					<img src="card/logo skyparking 2.jpeg" style="height: 40px; width: 40px; margin-top: 5%; margin-left: 5%; position: absolute;">

					<a href=""><img src="card/card2.png" class="img-responsive"></a>

					<div class="txt-white" style="margin-left: 5%; margin-top: -10%">
						<?php echo $data2[1]; ?>
					</div>
				</div>
				
				<?php
			}
    		?>
    	</div>
    </div>

    <div class="modal bottom-sheet" id="bellnotif">
        <div class="choose-date p-20" style="text-align: center;">
            <h2 class="uppercase txt-black bold">Fitur ini belum difungsikan!</h2>

            <button class="modal-close btn btn-large primary-color waves-effect waves-light m-t-30 width-100">OK</button>
        </div>
    </div>


<?php
require('footer.php');
?>