<?php
require('header-success.php');
?>

	<div class="animated delay-1">
		<div class="form-inputs txt-white">

			<div class="m-t-10 top">
				<center>
					<img src="card/logo skyparking 1.jpeg" style="width: 30%; height: 30%;">
				</center>
			</div>
			
			<div class="bottom p-20">
				<form action="" method="post">
					<div class="txt-white center m-t-10 m-b-10">Anda Anda yakin akan keluar?</div>

					<button type="submit" name="logout" id="logout" class="btn btn-large width-100 waves-effect waves-light m-t-10" style="background: #fff; color: #00f; border: 1px solid #fff; border-radius: 100px;">logout</button>
					
					<a href="javascript:history.back()" name="cancel" id="cancel" class="btn btn-large width-100 waves-effect waves-light m-t-20" style="background: transparent; color: #fff; border: 1px solid #fff; border-radius: 100px;">batal</a>
				</form>
			</div>

		</div>
	</div>

	<?php 
	$logout = @$_POST['logout'];
	if(isset($logout)) {
		unset($_SESSION['UserID']);
		unset($_SESSION['Name']);
		unset($_SESSION['MemberID']);
		unset($_SESSION['KID']);

		session_destroy();
		?>
		<script type="text/javascript">
			document.cookie = "UserID=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/appsky;";
			document.cookie = "Name=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/appsky;";
			document.cookie = "MemberID=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/appsky;";
			document.cookie = "KID=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/appsky;";

			document.cookie = "UserID=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/skyparking;";
			document.cookie = "Name=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/skyparking;";
			document.cookie = "MemberID=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/skyparking;";
			document.cookie = "KID=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/skyparking;";

			window.location.href = "index.php";
		</script>
		<?php

		//echo "<script language='javascript'>document.location='index.php';</script>";
		exit;
		
	}  
	?>

<?php
require('footer.php');
?>