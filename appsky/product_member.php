<?php
require('header.php');
require('sidebar-left.php');

$sqlloc = "SELECT * from [dbo].[LocationMerchant] where LocationID='".@$_GET['loc']."'";
$queryloc = sqlsrv_query($conn, $sqlloc);
$dloc = sqlsrv_fetch_array($queryloc);

function getBetween($content,$start,$end){
    $r = explode($start, $content);
    if (isset($r[1])){
        $r = explode($end, $r[1]);
        return $r[0];
    }
    return '';
}
?>

	<!-- Toolbar -->
    <div id="toolbar" class="primary-color">
        <a href="javascript:history.back()" class="open-left">
            <i class="ion-android-arrow-back"></i>
        </a>

        <span class="title"> Membership Product </span>

        <div class="right" id="right" style="margin-right: 20px;">
            <a href="#bellnotif" class="modal-trigger">
                <i class="ion-android-notifications"></i>
            </a>
        </div>
    </div>

	<div class="animated delay-1">
		<div class="p-20">

			<div style="margin-bottom: 33px;">
				<p>
					Pilih produk member yang anda inginkan di <b><?php echo $dloc[2]; ?></b>
				</p>

				<ul class="tabs">
                	<li class="tab"><a class="active " href="#bulanan">Bulanan</a></li>
                	<li class="tab"><a class="" href="#harian">Harian</a></li>
                	<li class="tab"><a class="" href="#slotparkir">Slot Parkir</a></li>
                </ul>

                <div id="bulanan">
                	<?php
					$sql = "SELECT * from [dbo].[LocationParkingProduct] where LocationID='".@$_GET['loc']."' order by Nama ASC";
					$query = sqlsrv_query($conn, $sql);
					while($data = sqlsrv_fetch_array($query)) { 

						$sv = "SELECT Name from [dbo].[VehicleType] where VehicleID='".$data[9]."'";
						$qv = sqlsrv_query($conn, $sv);
						$dv = sqlsrv_fetch_array($qv);

						$sq = "SELECT Quota from [dbo].[LocationQuota] where VehicleID='".$data[9]."'";
						$qq = sqlsrv_query($conn, $sq);
						$dq = sqlsrv_fetch_array($qq);

						?>
						<a href="product_detail.php?prod=<?php echo $data[1]; ?>" class="modal-trigger">
							<div class="single-news animated fadeinright delay-3 txt-black">
								<h4 class="right txt-black"><?php echo $dv[0]; ?></h4><br>

								<div class="txt-black bold"><?php echo $data[2]; ?></div>
								<div><?php echo $data[3]; ?></div><br>

								<div class="left">Masa Berlaku</div>
								<div class="right">Slot Member Tersisa</div><br>

								<div class="left bold"><?php echo substr($data[3], 12); ?></div>
								<a href="#prodslot-<?php echo $dv[0]; ?>" class="right bold modal-trigger"><div style="color: #00f;"><?php echo $dq[0]; ?></div></a><br>

								<div class="m-t-10">
									Jumlah point yang digunakan <b><?php echo $data[5] ;?></b>
								</div>
							</div>
						</a>
						<?php
					}
					?>
                </div>

                <div id="harian">
                	<h1 class="center uppercase bold txt-black m-t-30">Saat ini data belum ada</h1>
                </div>

                <div id="slotparkir">
                	<?php
                	$slq = "SELECT VehicleID, Quota from [dbo].[LocationQuota] where LocationID='".@$_GET['loc']."'";
                	$qlq = sqlsrv_query($conn, $slq);
                	while($dlq = sqlsrv_fetch_array($qlq)) {
                		$svh = "SELECT Name from [dbo].[VehicleType] where VehicleID='".$dlq[0]."'";
                		$qvh = sqlsrv_query($conn, $svh);
                		$dvh = sqlsrv_fetch_array($qvh);
                		?>
                		<div class="single-news animated fadeinright delay-3 txt-black">
                			<p>
                				Jenis Kendaraan : <span class="bold"><?php echo $dvh[0]; ?></span><br>
                				Quota : <span class="bold"><?php echo $dlq[1]; ?> </span>
                			</p>
                		</div>
                		<?php
                	}
                	?>
                </div>
				
			</div>

			<?php /*
			<div class="bottom-fixed" style="background: #fff; border-top: 1px solid #aaa;">
				<div class="row center m-t-10">
					<div class="col s4">
						<a href="#barcode" class="modal-trigger">
							Bulanan
						</a>
					</div>

					<div class="col s4">
						<a href="#menu" class="modal-trigger">
							Harian
						</a>
					</div>

					<div class="col s4">
						<a href="#menu" class="modal-trigger">
							Slot Parkir
						</a>
					</div>
				</div>
			</div>
			*/ ?>

		</div>
	</div>

	<?php
	$sql = "SELECT * from [dbo].[LocationParkingProduct] where LocationID='".@$_GET['loc']."' order by Nama ASC";
	$query = sqlsrv_query($conn, $sql);
	while($data = sqlsrv_fetch_array($query)) {
		$sv = "SELECT Name from [dbo].[VehicleType] where VehicleID='".$data[9]."'";
		$qv = sqlsrv_query($conn, $sv);
		$dv = sqlsrv_fetch_array($qv);

		$sq = "SELECT Quota from [dbo].[LocationQuota] where VehicleID='".$data[9]."'";
		$qq = sqlsrv_query($conn, $sq);
		$dq = sqlsrv_fetch_array($qq);

		?>
		<div class="modal bottom-sheet" id="prodslot-<?php echo $dv[0]; ?>">
			<div class="p-20 txt-black m-b-30">
				<div class="center">
					<p>Slot member tersisa</p>
				</div>

				<p>
					<div class="left bold">
						<?php echo $dv[0]; ?>
					</div>

					<div class="right">
						<?php echo $dq[0]; ?> Tersisa
					</div>
				</p>
				
			</div>
		</div>
		<?php
	}
	?>

	<div class="modal bottom-sheet" id="bellnotif">
    	<div class="choose-date p-20" style="text-align: center;">
	        <h2 class="uppercase txt-black bold">Fitur ini belum difungsikan!</h2>

	        <button class="modal-close btn btn-large primary-color waves-effect waves-light m-t-30 width-100">OK</button>
	    </div>
	</div>

<?php
require('footer.php');
?>
