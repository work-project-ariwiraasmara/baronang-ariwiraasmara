<!-- Left Sidebar -->
<ul id="slide-out-left" class="side-nav collapsible txt-black">
    <li>
        <div class="sidenav-header primary-color">
            <?php
            $Logo = 'static/images/yourlogohere.png';
            ?>

            <div class="nav-avatar">
                <img class="circle avatar" src="<?php echo $Logo; ?>" alt="">
                <div class="avatar-body">
                    <b style="color: white;"><?php echo ucfirst($_COOKIE['Name']); ?></b>
                    <p>Online</p>
                </div>
            </div>

        </div>
    </li>
    <li><a href="dashboard.php" class="no-child"><i class="ion-android-home"></i> Home</a></li>
    <li><a href="card.php" class="no-child"><i class="ion-cash"></i> My Card</a></li>
    <li><a href="member.php" class="no-child"><i class="ion-android-bookmark"></i> Member</a></li>
    <li><a href="profile.php" class="no-child"><i class="ion-android-person"></i> Profil</a></li>
    <li><a href="merchant.php" class="no-child"><i class="ion-android-people"></i> Merchant</a></li>
    <li><a href="logout.php" class="no-child"><i class="ion-log-out"></i> Keluar</a></li>
</ul>
<!-- End of Sidebars -->

<!-- Page Content -->
<div id="content" class="page">
