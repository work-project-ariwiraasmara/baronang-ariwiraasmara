<?php
require('header.php');
require('sidebar-left.php');

$sql = "SELECT * from [PaymentGateway].[dbo].[UserPaymentGateway] where KodeUser='$UserID'";
$query = sqlsrv_query($conn, $sql);
$data = sqlsrv_fetch_array($query);

require('qrcode/qrlib.php');

//barcode
include('barcode128/BarcodeGenerator.php');
include('barcode128/BarcodeGeneratorPNG.php');
include('barcode128/BarcodeGeneratorSVG.php');
include('barcode128/BarcodeGeneratorJPG.php');
include('barcode128/BarcodeGeneratorHTML.php');

function QRCode($kode, $cp, $matrixPointSize) {
	//QR code

	$PNG_TEMP_DIR = dirname(__FILE__).DIRECTORY_SEPARATOR.'qruser'.DIRECTORY_SEPARATOR;
	$PNG_WEB_DIR = 'qruser/';

	$errorCorrectionLevel = 'H';
	//$matrixPointSize = 500;

	//$kode = '1000100010001004';
	$filename = $PNG_WEB_DIR.$kode.'.png';
	if (!file_exists($filename)){
		QRcode::png($cp, $filename, $errorCorrectionLevel, $matrixPointSize, 2);
	}

	$generator = new Picqer\Barcode\BarcodeGeneratorPNG();
	$resource = 'data:image/png;base64,'.base64_encode($generator->getBarcode($kode, $generator::TYPE_CODE_128));
}

?>

	<!-- Toolbar -->
    <div id="toolbar" class="primary-color">
        <div class="open-left" id="open-left" data-activates="slide-out-left">
            <i class="ion-android-menu"></i>
        </div>

        <span class="title"> Sky Parking </span>

        <div class="right" id="right" style="margin-right: 20px;">
            <a href="#bellnotif" class="modal-trigger">
                <i class="ion-android-notifications"></i>
            </a>
        </div>
    </div>

	<div class="animated delay-1">
		<div class="primary-color p-20 txt-white">
			
			<div class="m-l-r-10">
				<h3 class="txt-white" id="user"><?php echo $data[1]; ?></h3>
				<div class="italic"><?php echo $data[2]; ?></div>
			</div>
			
			<div class="row m-t-10 m-l-r-10 borad-30" style="border: 2px solid #fff; height: 50px;">
				<div class="col s12" style="font-size: 20px; margin-top: 7px; margin-left: 5px;">
					<span id="useraccrek"><?php echo $data[0]; ?></span>
				</div>
			</div>

			<div class="row m-l-r-10">
				<div class="col s6">
					<?php
					if(!file_exists('qruser/'.$data[0].'.png')) {
						QRCode($data[0],$data[0],5);
					}
					?>
				</div>
				<div class="col s6 center borad-30" style="background: #fff; padding: 5px; margin-top: -20px;">
					<a href="#showqr" class="modal-trigger"><div style="color: #000;">Show QR Code</div></a>
				</div>	
			</div>



			<div class="m-t-30 m-l-r-10 row" style="margin-bottom: 0px;">
				<div class="col s4">
					<div style="height: 20px; width: 20px; background: #009900; display: inline-block;"></div>
					<div style="display: inline-block; margin-left: 5px;">In</div>
				</div>

				<div class="col s4">
					<div style="height: 20px; width: 20px; background: #ffffff; display: inline-block;"></div>
					<div style="display: inline-block; margin-left: 5px;">Not Used</div>
				</div>

				<div class="col s4">
					<div style="height: 20px; width: 20px; background: #ff0000; display: inline-block;"></div>
					<div style="display: inline-block; margin-left: 5px;">Blocked</div>
				</div>
			</div>

		</div>

		<div class="">
			<?php
			$sql2 = "SELECT * from [dbo].[MemberCard] where MemberID='$MemberID' and Status='1' and isLink='1'";
			//echo $sql2;
			//$sql2 = "SELECT * from [dbo].[MemberCard] where MemberID='0006' and Status='1' and isLink='1'";
			$query2 = sqlsrv_query($conn, $sql2);
			//echo count($query2);
			while( $data2 = sqlsrv_fetch_array($query2) ) { ?>
				<a href="card_detail.php?card=<?php echo $data2[1]; ?>" class="bold" id="accrek">
					<div class="single-news animated fadeinright delay-3">
						<?php echo $data2[1]; ?>
						<span class="right"><?php echo number_format($data2[2],0,',','.'); ?> Point</span>
					</div>
				</a>
				<?php
			}
			//echo $UserID; 
			?>

		</div>		
	</div>

	<div class="modal bottom-sheet" id="showqr">
		<div class="modal-content center">
			<center>
				<img src="<?php echo 'qruser/'.$data[0].'.png'; ?>" style="height: 150px; width: 150px;" />
			</center>

			<div class="m-t-10" style="font-size: 20px;"><?php echo $data[0]; ?></div> 
		</div>
	</div>

	<div class="modal bottom-sheet" id="bellnotif">
	    <div class="choose-date p-20" style="text-align: center;">
	        <h2 class="uppercase txt-black bold">Fitur ini belum difungsikan!</h2>

	        <button class="modal-close btn btn-large primary-color waves-effect waves-light m-t-30 width-100">OK</button>
	    </div>
	</div>

<?php
require('footer.php');
?>