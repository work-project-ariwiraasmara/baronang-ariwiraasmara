<?php
session_start();
require("lib/PHPMailer_5.2.0/class.phpmailer.php");

include("connect.php");

$uid = $_POST['uid'];
$name = $_POST['name'];
$email = $_POST['email'];
$telp = $_POST['telp'];
$address = $_POST['address'];
$province = $_POST['province'];
$city = $_POST['city'];

if($uid == "" || $name == "" || $email == "" || $telp == "" || $address == "" || $province == "" || $city == ""){
    messageAlert('Harap isi seluruh kolom','info');
    header('Location: register.php');
}
else{
    $a = "select* from KoneksiKoperasiBaronang.[dbo].[UserRegister] where UserIDBaronang = '$uid' or emailKoperasi = '$email'";
    $b = sqlsrv_query($conn, $a);
    $c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC);

    $aa = "select* from PaymentGateway.[dbo].[UserPaymentGateway] where KodeUser = '$uid'";
    $bb = sqlsrv_query($conn, $aa);
    $cc = sqlsrv_fetch_array($bb, SQLSRV_FETCH_NUMERIC);

    if($c != null){
        messageAlert('Tidak dapat membuat koperasi baru. User ID Baronang App / Email sudah terdaftar.','warning');
        header('Location: register.php');
    }
    else if($cc == null){
        messageAlert('Tidak dapat melakukan pendaftaran koperasi. User ID Baronang App tidak terdaftar. Harap melakukan pendaftaran melalui Baronang App','warning');
        header('Location: register.php');
    }
    else{
        $ppsql = "exec KoneksiKoperasiBaronang.[dbo].[ProsesRegister] '$uid', '$email','$telp','$name','$address','$province','$city'";
        $ppstmt = sqlsrv_query($conn, $ppsql);

        //send email
        //send ke user
        sendEmail($cc[2], $cc[11], $cc[1]);

        if($cc[2] != $email){
            //send ke koperasi
            sendEmail($email, $cc[11], $name);
        }

        if($ppstmt){
            messageAlert('Berhasil melakukan pendaftaran koperasi. Harap periksa email anda untuk melakukan konfirmasi.','success');
            header('Location: register.php');
        }
        else{
            messageAlert('Gagal membuat koperasi','danger');
            header('Location: register.php');
        }
    }
}

function sendEmail($email,$kode,$nama){
    $root = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/';
    $link = $root."koperasi/confregister.php?u=".$kode;

    $subject = "Email Verifikasi Koperasi";
    $message =
        "Hi! ".$nama.",<br><br>
            Terima kasih telah bergabung dengan Baronang.<br>
            Kami perlu memastikan bahwa email anda valid. Harap mengkonfirmasi email anda dengan mengklik link dibawah ini.<br>
            <a href=".$link.">Konfirmasi</a>
            <br>
            <br>
            <br>
            <br>
            Terima kasih.<br>
            Salam,<br>
            <br>
            <br>
            Baronang";

            $curl = curl_init();
            $post = "toaddr=$email&subject=$subject&message=$message";
            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://interzircon.com/phpmail.php",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => $post,
            ));
            $output = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
            $result = json_decode($output, true);
}
?>
