    </div> <!-- End of Page Content -->
</div> <!-- End of Page Container -->

<div id="modal3" class="modal bottom-sheet">
    <div class="modal-content">

    </div>
</div>

<!-- Small modal -->
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btn-exec-app">Save changes</button>
            </div>
        </div>
    </div>
</div>

<button type="button" class="btn btn-primary hide" id="modal" data-toggle="modal" data-target="#myModal"></button>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?php echo lang('Detail'); ?></h4>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang('Tutup'); ?></button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModalChat" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="box box-warning direct-chat direct-chat-warning">
                <div class="box-header with-border">
                    <h3 class="box-title"><?php echo lang('Chat Langsung'); ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body-chat">

                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="input-group">
                        <input type="text" id="message" placeholder="Type Message ..." class="form-control">
                        <span class="input-group-btn">
                            <button type="button" class="btn btn-warning btn-flat btn-send"><?php echo lang('Kirim'); ?></button>
                            <button type="button" class="btn btn-default btn-flat" data-dismiss="modal"><?php echo lang('Tutup'); ?></button>
                        </span>
                    </div>
                </div>
                <!-- /.box-footer-->
            </div>
        </div>
    </div>
</div>

<script src="js/site.js"></script>
<script src="js/jquery.swipebox.min.js"></script>
<script src="js/materialize.min.js"></script>
<script src="js/swiper.min.js"></script>
<script src="js/jquery.mixitup.min.js"></script>
<script src="js/masonry.min.js"></script>
<script src="js/chart.min.js"></script>
<script src="js/functions.js"></script>

<!-- Bootstrap 3.3.5 -->
<script src="static/bootstrap/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="static/plugins/select2/select2.full.min.js"></script>
<!-- date-range-picker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src="static/plugins/daterangepicker/daterangepicker.js"></script>
<!-- bootstrap date picker -->
<script src="static/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- bootstrap color picker -->
<script src="static/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="static/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- QR Barcode Reader -->
<script type="text/javascript" src="static/plugins/webCodeCamJs/js/qrcodelib.js"></script>
<script type="text/javascript" src="static/plugins/webCodeCamJs/js/webcodecamjquery.js"></script>
<script type="text/javascript" src="static/plugins/webCodeCamJs/js/mainjquery.js"></script>

<!-- Page script -->
<?php //include "js-footer.php"; ?>

<script type="text/javascript">
    var interval = 1000; //1detik

    function doAjax(){
        $.ajax({
            url : "ajax_ceksesi.php",
            type : 'POST',
            dataType : 'json',
            data: { },
            success : function(data) {
                if(data.status == 1){
                    window.location.href = 'buka_sesi.php';
                }

                if(data.status == 2){
                    window.location.href = 'tutup_sesi_kasir.php';
                }

                if(data.status == 3){
                    window.location.href = 'tutup_sesi_teller.php';
                }
            },
            complete: function (data) {
                // Schedule the next
                setTimeout(doAjax, interval);
            }
        });
    }

    setTimeout(doAjax, interval);
</script>

<script type="text/javascript">
    <?php echo $jsArrayPositionLimit; ?>
    function changeValuePositionLimit(id){
        document.getElementById('belanja').value = prdName[id].belanja;
        document.getElementById('pinjam').value = prdName[id].pinjam;
    }

    $(document).ready(function(){
        $('#csoaregsavmember').change(function(){    // KETIKA ISI DARI FIEL 'member' BERUBAH MAKA ......
            var memberfromfield = $('#csoaregsavmember').val();  // AMBIL isi dari fiel member masukkan variabel 'memberfromfield'
            $.ajax({        // Memulai ajax
                method: "POST",
                url: "ajax_csoaregsav.php",    // file PHP yang akan merespon ajax
                data: { member: memberfromfield}   // data POST yang akan dikirim
            })
                .done(function( hasilajax ) {   // KETIKA PROSES Ajax Request Selesai
                    $('#nama').val(hasilajax);  // Isikan hasil dari ajak ke field 'nama'
                });
        });
    });
</script>
</body>
</html>