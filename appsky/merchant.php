<?php
require('header.php');
require('sidebar-left.php');

require('qrcode/qrlib.php');

//barcode
include('barcode128/BarcodeGenerator.php');
include('barcode128/BarcodeGeneratorPNG.php');
include('barcode128/BarcodeGeneratorSVG.php');
include('barcode128/BarcodeGeneratorJPG.php');
include('barcode128/BarcodeGeneratorHTML.php');

$sql1 = "SELECT * from [PaymentGateway].[dbo].[UserMemberKoperasi] where KID='700097' and UserID='$UserID'";
$query1 = sqlsrv_query($conn, $sql1);
$data1 = sqlsrv_fetch_array($query1);

function QRCode($kode, $cp, $matrixPointSize) {
	//QR code

	$PNG_TEMP_DIR = dirname(__FILE__).DIRECTORY_SEPARATOR.'qredc'.DIRECTORY_SEPARATOR;
	$PNG_WEB_DIR = 'qredc/';

	$errorCorrectionLevel = 'H';
	//$matrixPointSize = 500;

	//$kode = '1000100010001004';
	$filename = $PNG_WEB_DIR.$kode.'.png';
	if (!file_exists($filename)){
		QRcode::png($cp, $filename, $errorCorrectionLevel, $matrixPointSize, 2);
	}

	$generator = new Picqer\Barcode\BarcodeGeneratorPNG();
	$resource = 'data:image/png;base64,'.base64_encode($generator->getBarcode($kode, $generator::TYPE_CODE_128));
}
?>

	<!-- Toolbar -->
    <div id="toolbar" class="primary-color">
        <div class="open-left" id="open-left" data-activates="slide-out-left">
            <i class="ion-android-menu"></i>
        </div>

        <span class="title"> Merchant </span>

        <div class="right" id="right" style="margin-right: 20px;">
            <a href="#bellnotif" class="modal-trigger">
                <i class="ion-android-notifications"></i>
            </a>
        </div>
    </div>

	<div class="animated delay-1">
		<div class="p-20">
			
			<div class="bold" style="font-size: 20px;">Daftar EDC</div>

			<div class="italic m-t-20" style="font-size: 20px;">
				Klik untuk membuka QR Code
			</div>

			<div class="m-t-30">
				<?php
				$sedc = "SELECT SerialNumber from [Gateway].[dbo].[EDCList] where KID='$KID' and UserIDBaronang='$UserID' and MemberID='".$data1[2]."' order by SerialNumber ASC";
				$qedc = sqlsrv_query($conn, $sedc);
				while($dedc = sqlsrv_fetch_array($qedc)) { 
					if(!file_exists('qredc/'.$dedc[0].'.png')) { QRCode($dedc[0],$dedc[0],5); }
					?>
					<div class="single-news animated fadeinright delay-3">
						<a href="#modal-qr<?php echo $dedc[0]; ?>" class="modal-trigger"><?php echo $dedc[0]; ?></a>
					</div>
					<?php
				}
				?>
			</div>

		</div>
	</div>

	<div class="modal bottom-sheet" id="bellnotif">
    	<div class="choose-date p-20" style="text-align: center;">
	        <h2 class="uppercase txt-black bold">Fitur ini belum difungsikan!</h2>

	        <button class="modal-close btn btn-large primary-color waves-effect waves-light m-t-30 width-100">OK</button>
	    </div>
	</div>

	<?php
	$sedc = "SELECT SerialNumber from [Gateway].[dbo].[EDCList] where KID='$KID' and UserIDBaronang='$UserID' and MemberID='".$data1[2]."' order by SerialNumber ASC";
	$qedc = sqlsrv_query($conn, $sedc);
	while($dedc = sqlsrv_fetch_array($qedc)) { ?>
		<div class="modal bottom-sheet" id="modal-qr<?php echo $dedc[0]; ?>">
			<div class="modal-content center">
				<center>
					<img src="<?php echo 'qredc/'.$dedc[0].'.png' ?>" style="height: 150px; width: 150px;"/>
				</center>
				
				
				<div class="m-t-10" style="font-size: 20px;"><?php echo $dedc[0]; ?></div> 
			</div>
		</div>
		<?php
	}
	?>

<?php
require('footer.php');
?>