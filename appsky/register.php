<?php
require('header-register.php');
?>

	<div id="toolbar" class="primary-color">
        <a href="javascript:history.back()" class="open-left">
            <i class="ion-android-arrow-back"></i>
        </a>

    	<span class="title txt-white"><b>Register</b></span>
    </div>

    <div class="animated delay-1 txt-black">
    	<div class="form-inputs" style="margin-top: 60px;">
    		<form action="procregister2.php" method="post">
    			
	    		<div class="input-field">
	    			<input type="text" name="ktp" id="ktp" class="validate">
	    			<label for="ktp">KTP</label>
	    		</div>

	    		<div class="input-field">
	    			<input type="text" name="name" id="name" class="validate">
	    			<label for="nama">Nama</label>
	    		</div>

	    		<div class="input-field">
	    			<input type="email" name="email" id="email" class="validate">
	    			<label for="email">Email</label>
	    		</div>

	    		<div class="input-field">
	    			<input type="tel" name="telp" id="telp" class="validate">
	    			<label for="telp">Telepon</label>
	    		</div>

	    		<div class="input-field">
	    			<input type="text" name="address" id="address" class="validate">
	    			<label for="address">Alamat</label>
	    		</div>


	    		<div class="bold m-t-30">Keamanan Akun</div>

	    		<div class="input-field">
	    			<input type="password" name="pass" id="pass" class="validate">
	    			<label for="pass">Password</label>
	    		</div>

	    		<div class="input-field">
	    			<input type="password" name="passval" id="passval" class="validate">
	    			<label for="passval">Ulangi Password</label>
	    		</div>

	    		<div class="m-t-30">PIN terdiri dari 6 angka</div>

	    		<div class="input-field">
	    			<input type="password" name="pin" id="pin" class="validate" pattern="[0-9]*" inputmode="numeric">
	    			<label for="pin">PIN</label>
	    		</div>

	    		<div class="input-field">
	    			<input type="password" name="pinval" id="pinval" class="validate" pattern="[0-9]*" inputmode="numeric">
	    			<label for="pinval">Ulangi PIN</label>
	    		</div>

	    		<div class="input-field p-10 uppercase" id="konf"></div>
	    		<div class="input-field p-10 uppercase" id="pinkonf"></div>

	    		<button type="submit" name="ok" id="ok" class="btn btn-large primary-color width-100 waves-effect waves-light borad-50 m-t-30">daftar</button>

	    		<div class="center m-t-20">
	    			Dengan mendaftar, saya menyetujui<br>
	    			<a href="">Syarat</a> serta <a href="">Ketentuan</a> <br><br>

	    			<div class="italic">Powered By Baronang</div>  
	    		</div>

    		</form>

    	</div>
    </div>

    <script type="text/javascript">
    	var angka = ['1','2','3','4','5','6','7','8','9','0'];
    	$('#pin').keyup(function(){
            /*
    		var pin = $('#pin').val();
    		var pinval = $('#pinval').val();

    		if(pin == pinval) {
    			$('#konf').html('PIN Sesuai!');
    			$('#konf').css('background-color','#0f0');
    			$('#konf').css('color','white');
    		}
    		else {
    			$('#konf').html('PIN Tidak Sesuai!');
    			$('#konf').css('background-color','#f00');
    			$('#konf').css('color','white');
    		}

    		if(pin.length != 6) {
    			$('#konf').html('Panjang PIN Harus 6!');
    			$('#konf').css('background-color','#f00');
    			$('#konf').css('color','white');
    		}

            if(pin == '') {
                $('#konf').html('');
                $('#konf').css('background-color','none');
                $('#konf').css('color','none');
            }
            */
    	});

    	$('#pinval').keyup(function(){
    		var pin = $('#pin').val();
    		var pinval = $('#pinval').val();

    		if(pin == pinval) {
    			$('#konf').html('PIN Sesuai!');
    			$('#konf').css('background-color','#0f0');
    			$('#konf').css('color','white');
    		}
    		else {
    			$('#konf').html('PIN Tidak Sesuai!');
    			$('#konf').css('background-color','#f00');
    			$('#konf').css('color','white');
    		}

    		if(pin.length != 6) {
    			$('#konf').html('Panjang PIN Harus 6!');
    			$('#konf').css('background-color','#0f0');
    			$('#konf').css('color','white');
    		}

            if(pinval == '') {
                $('#konf').html('');
                $('#konf').css('background-color','none');
                $('#konf').css('color','none');
            }
    	});

    	$('#pass').keyup(function(){
    		var pass = $('#pass').val();
    		var passval = $('#passval').val();

            /*
    		if(pass == passval) {
    			$('#konf').html('Password Sesuai!');
    			$('#konf').css('background-color','#0f0');
    			$('#konf').css('color','white');
    		}
    		else {
    			$('#konf').html('Password Tidak Sesuai!');
    			$('#konf').css('background-color','#f00');
    			$('#konf').css('color','white');
    		}

            if(pass == '') {
                $('#konf').html('');
                $('#konf').css('background-color','none');
                $('#konf').css('color','none');
            }
            */
    	});

    	$('#passval').keyup(function(){
    		var pass = $('#pass').val();
    		var passval = $('#passval').val();

    		if(pass == passval) {
    			$('#konf').html('Password Sesuai!');
    			$('#konf').css('background-color','#0f0');
    			$('#konf').css('color','white');
    		}
    		else {
    			$('#konf').html('Password Tidak Sesuai!');
    			$('#konf').css('background-color','#f00');
    			$('#konf').css('color','white');
    		}

            if(passval == '') {
                $('#konf').html('');
                $('#konf').css('background-color','none');
                $('#konf').css('color','none');
            }
    	});
    </script>

<?php
require('blank-footer.php');
?>