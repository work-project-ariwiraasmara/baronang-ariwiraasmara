<?php
require('blank-header.php');
?>

	<div class="animated delay-1">
		<div class="form-inputs txt-white">

			<div class="m-t-10 top">
				<center>
					<img src="card/logo skyparking 1.jpeg" style="width: 30%; height: 30%;">
				</center>
			</div>
			
			<div class="bottom p-20">
				<form action="cek_login2.php" method="post">

					<div class="input-field">
						<input type="text" name="name" id="name" class="validate">
						<label for="name" class="txt-white">Email</label>
					</div>

					<div class="input-field">
						<input type="password" name="password" id="password" class="validate">
						<label for="password" class="txt-white">Password</label>
					</div>

					<a href="forgotpass.php" class="txt-white right m-t-10 m-b-10">Lupa Password?</a>

					<button type="submit" name="login" id="login" class="btn btn-large width-100 waves-effect waves-light m-t-10" style="background: #fff; color: #00f; border: 1px solid #fff; border-radius: 100px;">Login</button>
				
					<div class="txt-white italic center m-t-10">Powered By Baronang</div>
				</form>
			</div>

		</div>
	</div>

<?php
require('blank-footer.php');
?>