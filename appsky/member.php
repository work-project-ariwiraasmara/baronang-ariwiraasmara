<?php
require('header.php');
require('sidebar-left.php');
?>

	<!-- Toolbar -->
    <div id="toolbar" class="primary-color">
        <div class="open-left" id="open-left" data-activates="slide-out-left">
            <i class="ion-android-menu"></i>
        </div>

        <span class="title"> Membership </span>

        <div class="right" id="right" style="margin-right: 20px;">
            <a href="#bellnotif" class="modal-trigger">
                <i class="ion-android-notifications"></i>
            </a>
        </div>
    </div>

	<div class="animated delay-1">
		<div class="p-20">
			
			<div style="margin-bottom: 70px;">
				Pilih lokasi yang Anda inginkan

				<div class="m-t-10">
					<a href="#pembelian" class="left grey-blue borad-20 modal-trigger" style="padding-top: 10px; padding-bottom: 10px; padding-right: 20px; padding-left: 20px;">Cara Pembelian</a>

					<a href="#topup" class="right grey-blue borad-20 modal-trigger" style="padding-top: 10px; padding-bottom: 10px; padding-right: 20px; padding-left: 20px;">Cara Top Up</a>
				</div>
			</div>

			<?php
			$sql = "SELECT * from [dbo].[LocationMerchant] order by Nama ASC";
			$query = sqlsrv_query($conn, $sql);
			while($data = sqlsrv_fetch_array($query)) { 
				if(is_null($data[3]) || empty($data[3])) $alamat = '-';
				else $alamat = $data[3];

				if(is_null($data[4]) || empty($data[4])) $telp = '-';
				else $telp = $data[4];

				?>
				<a href="product_member.php?loc=<?php echo $data[0]; ?>" class="modal-trigger">
					<div class="single-news animated fadeinright delay-3 m-t-30 txt-black">
						<?php //<img src="img/<?php echo $data[9];"><br> ?>

						<div class="txt-black bold" style="font-size: 18px;"><?php echo $data[2]; ?></div>
						<div><?php echo $alamat; ?></div>

						<div class="right"><?php echo $telp; ?></div><br>
					</div>
				</a>
				<?php
			}
			?>

		</div>
	</div>

	<div class="modal bottom-sheet" id="showqr">
		<div class="modal-content center">
			<center>
				<img src="qr7000971812000011.png" style="height: 100px; width: 100px;" />
			</center>
			
			<div class="m-t-10" style="font-size: 20px;">7000971812000011</div> 
		</div>
	</div>

	<div id="pembelian" class="modal bottom-sheet txt-black" style="overflow-y: scroll;">
		<div class="modal-content">
			
			<div class="center bold" style="color: #00f;">Cara Melakukan Pembelian Produk Member</div><br>
			<p>1. Pastikan saldo kartu Anda mencukupi</p>
			<p>2. Pilih lokasi yang Anda akan daftar sebagai member</p>
			<p>3. Pilih produk member yang tersedia</p>
			<p>4. Pilih kartu yang Anda ingin daftarkan</p>
			<p>5. Pilih bulan yang Anda ingin terdaftar sebagai member</p>
			<p>6. Pembelian selesai dan Anda telah terdaftar sebagai member</p>

		</div>
	</div>

	<div id="topup" class="modal bottom-sheet txt-black" style="overflow-y: scroll;">
		<div class="modal-content">

			<div class="center bold" style="color: #00f;">Cara Melakukan Top Up Poin</div><br>
			<p>1. Kunjungi PPS terdekat untuk melakukan top up</p>
			<p>2. Pembelian poin dilakukan oleh petugas PPS</p>
			<p>3. Pastikan kartu yang anda akan top up sudah benar</p>

		</div>
	</div>

	<div class="modal bottom-sheet" id="bellnotif">
    	<div class="choose-date p-20" style="text-align: center;">
	        <h2 class="uppercase txt-black bold">Fitur ini belum difungsikan!</h2>

	        <button class="modal-close btn btn-large primary-color waves-effect waves-light m-t-30 width-100">OK</button>
	    </div>
	</div>

<?php
require('footer.php');
?>