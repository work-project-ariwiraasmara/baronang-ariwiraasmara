<?php
session_start();
include("conf/conf.inc");
include 'lang.lib.php';
include("lib/encrDecr/eds.php");

$model = new Encr();
$u = $model->decrs($uid, $key);
$p = $model->decrs($pwd, $key);
$d = $model->decrs($databaseName, $key);
$s = $model->decrs($serverName, $key);

$connectionInfo = array( "UID"=>$u,
                         "PWD"=>$p,
                         "Database"=>$d);

/* Connect using SQL Server Authentication. */  
$conn = sqlsrv_connect( $s, $connectionInfo);

function messageAlert($message, $type){
    $_SESSION['error-message'] = $message;
    $_SESSION['error-type'] = $type;
    $_SESSION['error-time'] = time()+5;
}

if(!isset($_COOKIE['Name'])) { header('location: login.php'); }
?>