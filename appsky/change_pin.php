<?php
require('header.php');
require('sidebar-left.php');
?>

	<!-- Toolbar -->
    <div id="toolbar" class="primary-color">
        <a href="javascript:history.back()" class="open-left">
            <i class="ion-android-arrow-back"></i>
        </a>

        <span class="title">Ubah PIN</span>

        <div class="right" id="right" style="margin-right: 20px;">
            <a href="#bellnotif" class="modal-trigger">
                <i class="ion-android-notifications"></i>
            </a>
        </div>
    </div>

    <div class="animated delay-1">
    	<div class="p-20">
    		
    		<div class="form-inputs">
    			<form action="" method="post">		
    				<div class="input-field">
	    				<input type="number" name="pin" id="pin" class="validate">
	    				<label for="pass">PIN Baru Anda</label>
	    			</div>

	    			<div class="input-field">
	    				<input type="number" name="pinval" id="pinval" class="validate">
	    				<label for="pass">Ulangi PIN</label>
	    			</div>

	    			<div class="input-field p-10 uppercase" id="konf"></div>

	    			<button type="submit" name="ok" id="ok" class="btn btn-large primary-color waves-effect waves-light width-100 m-t-30">Simpan</button>
    			</form>
	    			
    			<?php
	    		$ok = @$_POST['ok'];
	    		if(isset($ok)) {
	    			$pin = md5(@$_POST['pin']);

	    			if($pin == '' || empty($pin) || is_null($pin)) { ?>
	    				<script type="text/javascript">
	    					alert('PIN Tidak Boleh Kosong!');
	    				</script>
	    				<?php
	    			}
	    			else {
	    				$sql = "UPDATE [PaymentGateway].[dbo].[UserPaymentGateway] set Pin='$pin' where KodeUser='$KID'";
		    			$query = sqlsrv_query($conn, $sql);
		    			header('location: profil.php');
	    			}
	    		}
	    		?>
    		</div>

    	</div>
    </div>

    <div class="modal bottom-sheet" id="bellnotif">
        <div class="choose-date p-20" style="text-align: center;">
            <h2 class="uppercase txt-black bold">Fitur ini belum difungsikan!</h2>

            <button class="modal-close btn btn-large primary-color waves-effect waves-light m-t-30 width-100">OK</button>
        </div>
    </div>

    <script type="text/javascript">
    	var angka = ['1','2','3','4','5','6','7','8','9','0'];
    	$('#pin').keyup(function(){
    		var pin = $('#pin').val();
    		var pinval = $('#pinval').val();

    		if(pin.length != 6) {
    			$('#konf').html('Panjang PIN Harus 6!');
    			$('#konf').css('background-color','#f00');
    			$('#konf').css('color','white');
    		}
    		else {
    			$('#konf').html('');
                $('#konf').css('background-color','none');
                $('#konf').css('color','none');
    		}

    		/*
    		if(pin == pinval) {
    			$('#konf').html('PIN Sesuai!');
    			$('#konf').css('background-color','#0f0');
    			$('#konf').css('color','white');
    		}
    		else {
    			$('#konf').html('PIN Tidak Sesuai!');
    			$('#konf').css('background-color','#f00');
    			$('#konf').css('color','white');
    		}

            if(pin == '') {
                $('#konf').html('');
                $('#konf').css('background-color','none');
                $('#konf').css('color','none');
            }
            */
    	});

    	$('#pinval').keyup(function(){
    		var pin = $('#pin').val();
    		var pinval = $('#pinval').val();

    		if(pin == pinval) {
    			$('#konf').html('PIN Sesuai!');
    			$('#konf').css('background-color','#0f0');
    			$('#konf').css('color','white');
    		}
    		else {
    			$('#konf').html('PIN Tidak Sesuai!');
    			$('#konf').css('background-color','#f00');
    			$('#konf').css('color','white');
    		}
    	});
    </script>

<?php
require('footer.php');
?>