<?php
require('header.php');
require('sidebar-left.php');

$sql = "SELECT * from [PaymentGateway].[dbo].[UserPaymentGateway] where KodeUser='$UserID'";
$query = sqlsrv_query($conn, $sql);
$data = sqlsrv_fetch_array($query);

?>

	<!-- Toolbar -->
    <div id="toolbar" class="primary-color">
        <div class="open-left" id="open-left" data-activates="slide-out-left">
            <i class="ion-android-menu"></i>
        </div>

        <span class="title"> Profile </span>

        <div class="open-right" id="open-right">
            <a href="">
                <i class="ion-gear-a"></i>
            </a>
        </div>

        <div class="open-right" id="open-right">
            <a href="">
                <i class="ion-android-notifications"></i>
            </a>
        </div>
    </div>

	<div class="animated delay-1">
		<div class="primary-color p-20 txt-white">
			
			<div class="row">
				
				<div class="col s8">
					<h3 class="txt-white" id="user"><?php echo $data[1]; ?></h3>
					<div class="italic"><?php echo $data[2]; ?></div>
				</div>

				<div class="col s4">
					<a href="logout.php" class="right"><div style="border: 1px solid #fff; border-radius: 30px; padding-top: 10px; padding-bottom: 10px; padding-left: 40px; padding-right: 40px;">Keluar</div></a>
				</div>

			</div>

			<div class="row m-t-30" style="margin-bottom: 0px;">
				<div class="col s6">
					<a href="change_password.php" class="left"><div style="background: #fff; color: #000; border: 1px solid #fff; border-radius: 30px; padding-top: 10px; padding-bottom: 10px; padding-left: 30px; padding-right: 30px;">Ubah Password</div></a>
				</div>

				<div class="col s6">
					<a href="change_pin.php" class="right"><div style="background: #fff; color: #000; border: 1px solid #fff; border-radius: 30px; padding-top: 10px; padding-bottom: 10px; padding-left: 30px; padding-right: 30px;">Ubah Pin</div></a>
				</div>
			</div>
		
		</div>

		<div class="form-inputs txt-black">

			<div class="input-field">
				<input type="text" name="name" id="name" class="validate" value="<?php echo $data[1]; ?>" readonly>
				<label for="name">Nama</label>
			</div>

			<div class="input-field">
				<input type="text" name="ktp" id="ktp" class="validate" value="<?php echo $data[9]; ?>" readonly>
				<label for="ktp">KTP</label>
			</div>

			<div class="input-field">
				<input type="text" name="email" id="email" class="validate" value="<?php echo $data[2]; ?>" readonly>
				<label for="email">Email</label>
			</div>

			<div class="input-field">
				<input type="text" name="telp" id="telp" class="validate" value="<?php echo $data[3]; ?>" readonly>
				<label for="telp">Telepon</label>
			</div>

			<div class="input-field">
				<input type="text" name="alamat" id="alamat" class="validate" value="<?php echo $data[4]; ?>" readonly>
				<label for="alamat">Alamat</label>
			</div>

		</div>		
	</div>


<?php
require('footer.php');
?>