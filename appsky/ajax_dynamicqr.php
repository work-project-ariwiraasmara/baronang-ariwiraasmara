<?php
include 'connect-card.php';
require('qrcode/qrlib.php');

//barcode
include('barcode128/BarcodeGenerator.php');
include('barcode128/BarcodeGeneratorPNG.php');
include('barcode128/BarcodeGeneratorSVG.php');
include('barcode128/BarcodeGeneratorJPG.php');
include('barcode128/BarcodeGeneratorHTML.php');

function QRCode($kode, $cp, $matrixPointSize) {
	//QR code

	$PNG_TEMP_DIR = dirname(__FILE__).DIRECTORY_SEPARATOR.'qrcard'.DIRECTORY_SEPARATOR;
	$PNG_WEB_DIR = 'qrcard/'.@$_COOKIE['UserName'].'/';

	$errorCorrectionLevel = 'H';
	//$matrixPointSize = 500;

	//$kode = '1000100010001004';
	$filename = $PNG_WEB_DIR.$kode.'.png';
	if (!file_exists($filename)){
		QRcode::png($cp, $filename, $errorCorrectionLevel, $matrixPointSize, 2);
	}
	else {
		file_put_contents($filename, QRcode::png($cp, $filename, $errorCorrectionLevel, $matrixPointSize, 2));
	}

	$generator = new Picqer\Barcode\BarcodeGeneratorPNG();
	$resource = 'data:image/png;base64,'.base64_encode($generator->getBarcode($kode, $generator::TYPE_CODE_128));
}

$cardno = $_POST['cardno'];
unlink('qrcard/'.@$_COOKIE['UserName'].'/'.$cardno.'.png');
$json = array(
    'status' =>0,
    'img'	 =>'',
    'sql1'	 =>'',
    'resql1' =>'',
    'sql2'	 =>'',
    'tnow'	 =>'',
);

$ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
$iv = openssl_random_pseudo_bytes($ivlen);
$ciphertext_raw = openssl_encrypt($cardno, $cipher, '70007000', $options=OPENSSL_RAW_DATA, $iv);
$hmac = hash_hmac('sha256', $ciphertext_raw, '70007000', $as_binary=true);
$ciphertext = base64_encode( $iv.$hmac.$ciphertext_raw );

$s1 = "SELECT * from [dbo].[DynamicQR] where CardNo='$cardno'";
$q1 = sqlsrv_query($conn, $s1);
$d1 = sqlsrv_fetch_array($q1);
$json['sql1'] = $s1;
$json['resql1'] = count($d1);

$s2 = '';
$json['tnow'] = date('Y-m-d H:i:s', time());
$time = date('Y-m-d H:i:s', (time() + (5 * 60)));

if(count($d1) > 0) {
	$s2 = "UPDATE [dbo].[DynamicQR] set Barcode='$ciphertext', ValidTime='$time' where CardNo='$cardno'";
}
else {
	$s2 = "INSERT into [dbo].[DynamicQR] values('$cardno', '$ciphertext', '$time', '1')";
}

$q2 = sqlsrv_query($conn, $s2);
$json['sql2'] = $s2;

/*
if(!file_exists('qrcard/'.@$_COOKIE['UserName'].'/'.$cardno.'.png')) {
	QRCode($cardno,$ciphertext,5);
}
else {
	QRCode($cardno,$ciphertext,5);
}
*/

QRCode($cardno,$ciphertext,5);
$json['status'] = 1;
$json['img'] = 'qrcard/'.@$_COOKIE['UserName'].'/'.$cardno.'.png';
echo json_encode($json);
//echo 'qrcard/'.@$_COOKIE['UserName'].'/'.$cardno.'.png';
//echo 'data[2] > '.$cardno.'; ciphertext : '.$ciphertext.';\n'.$s2;
?>