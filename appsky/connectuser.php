<?php
include("conf/confuser.inc");
include_once("lib/encrDecr/eds.php");

$model = new Encr();
$u = $model->decrs($uiduser, $key);
$p = $model->decrs($pwduser, $key);
$d = $model->decrs($databaseNameuser, $key);
$s = $model->decrs($serverNameuser, $key);

$connectionInfouser = array( "UID"=>$u,
                         "PWD"=>$p,
                         "Database"=>$d);

/* Connect using SQL Server Authentication. */  
$connuser = sqlsrv_connect($s, $connectionInfouser);

?>