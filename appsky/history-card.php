<?php
require('qrcode/qrlib.php');

//barcode
include('barcode128/BarcodeGenerator.php');
include('barcode128/BarcodeGeneratorPNG.php');
include('barcode128/BarcodeGeneratorSVG.php');
include('barcode128/BarcodeGeneratorJPG.php');
include('barcode128/BarcodeGeneratorHTML.php');

function QRCode($kode, $cp, $matrixPointSize) {
	//QR code

	$PNG_TEMP_DIR = dirname(__FILE__).DIRECTORY_SEPARATOR.'qrcard'.DIRECTORY_SEPARATOR;
	$PNG_WEB_DIR = 'qrcard/';

	$errorCorrectionLevel = 'H';
	//$matrixPointSize = 500;

	//$kode = '1000100010001004';
	$filename = $PNG_WEB_DIR.$kode.'.png';
	if (!file_exists($filename)){
		QRcode::png($cp, $filename, $errorCorrectionLevel, $matrixPointSize, 2);
	}

	$generator = new Picqer\Barcode\BarcodeGeneratorPNG();
	$resource = 'data:image/png;base64,'.base64_encode($generator->getBarcode($kode, $generator::TYPE_CODE_128));
}

$sql1 = "SELECT LocationID, BlockID, BlockName, BlockDetail, CompanyName, Quota, QuotaLeft, Email from [dbo].[LocationDetail] where Status='1' and BlockID='".@$_COOKIE['BlockID']."'";
//echo $sql1;
$qry1 = sqlsrv_query($conn, $sql1);
$data1 = sqlsrv_fetch_array($qry1)
?>

<script type="text/javascript">
	document.title = "<?php echo 'Sky Parking - '.$data1[3].' - '.$data1[4]; ?>";
</script>

<body class="">
    <div class="m-scene" id="main"> <!-- Main Container -->
        <!-- Page Content -->
        <div class="page" id="content">

        	<!-- Toolbar -->
		    <div id="toolbar" class="primary-color">
		        <div class="open-left" data-activates="slide-out-left">
		        	<a href="sky_card.php?" class="waves-effect waves-light">
		        		<i class="ion-android-arrow-back"></i>
		        	</a>
		        </div>

		        <span class="title"> <?php echo $data1[3]; ?> </span>

		        <div class="right" style="margin-right: 20px;">
		            <a href="logout_card.php" class="waves-effect waves-light">
		                <i class="ion-android-exit"></i>
		            </a>
		        </div>
		    </div>

		    <div class="animated delay-1">
				<div class="primary-color p-20 txt-white">
					
					<div class="m-l-r-10">
						<h3 class="txt-white bold" id="user">
							<?php echo $data1[4]; ?>	
						</h3>

						<h3 class="txt-white bold italic underline" id="">
							<a href="<?php echo '#notif-unlink-'.@$_GET['history'] ?>" class="modal-trigger" id=""><?php echo @$_GET['history']; ?></a>
						</h3>

						<h3 class="txt-white bold">
							<?php echo $data1[7]; ?>	
						</h3>
					</div>

					<a href="#notif-unlink-success" id="toUnlinkSuccess" class="hide modal-trigger">to Success</a>
				</div>

				<div class="txt-black">
					<?php
					$sql3 = "SELECT TOP 10 *, convert(varchar(255), TimeStam, 20) from [dbo].[ParkingTrans] where AccNo='".@$_GET['history']."' order by TimeStam DESC";
					$query3 = sqlsrv_query($conn, $sql3);
					while($data3 = sqlsrv_fetch_array($query3)) {
						if( is_null($data3[6]) || empty($data3[6]) || $data3[6] == '0' ) { 
							$ket = 'CR';
							$nominal = $data3[7];
						}
						else { 
							$ket = 'DB'; 
							$nominal = $data3[6];
						}

						if( is_null($data3[9]) || empty($data3[9]) || $data3[9] == '' ) {
							$tgl = '0000-00-00 00:00:00';
						}
						else {
							$tgl = $data3[9];
						}
						?>
						<div class="single-news animated fadeinright delay-3 txt-black">
							
							<div>
								<div class="left">
									<?php echo $data3[0]; ?>
								</div>

								<div class="right">
									<?php echo $ket; ?>
								</div>
							</div> <br>

							<div class="m-t-10">
								<div class="left">
									<?php echo $tgl; ?>
								</div>

								<div class="right">
									<?php echo number_format($nominal,0,',','.'); ?> Point
								</div>
							</div> <br>

							<div class="m-t-10">
								<?php echo $data3[5]; ?>
							</div>

						</div>
						
						<?php
					}
					?>
				</div>

			</div>

			<div class="modal borad-20" id="<?php echo 'notif-unlink-'.@$_GET['history']; ?>">
				<div class="modal-content choose-date txt-black">
					<h1 class="uppercase txt-black center">
						<?php echo 'Anda yakin ingin unlink kartu ini ['.@$_GET['history'].'] ?'; ?>
					</h1>
					<div class="row">
						<div class="col s6 center">
							<button id="unlink-yes" class="btn primary-color btn-large borad-20 waves-light waves-light modal-close">Yes</button>
						</div>
						<div class="col s6 center">
							<button id="unlink-no" class="btn primary-color btn-large borad-20 waves-light waves-light modal-close">No</a>
						</div>
					</div>
				</div>
			</div>

			<div class="modal borad-20" id="notif-unlink-success">
			   	<div class="modal-content choose-date txt-white" style="background: #49f;">
					<h1 class="uppercase txt-white center">
						<a href="?" class="modal-close"><i class="ion-android-done txt-white"></i></a><br>
						Berhasil unlink kartu!
					</h1>
				</div>
			</div>

			<script type="text/javascript">
				$('#unlink-yes').click(function(){
					var locid 	= "<?php echo @$_COOKIE['LocationID']; ?>";
					var blokid 	= "<?php echo @$_COOKIE['BlockID']; ?>";
					var userid 	= "<?php echo @$_COOKIE['UserID']; ?>";
					var code 	= "<?php echo @$_GET['history']; ?>";
					var link 	= "unlink";

					//var cf = confirm("Anda yakin ingin Unlink Data Kartu ini?");
					//if(cf == true) {
						$.ajax({
							url : "ajax_ceklinkcard.php",
							type : 'POST',
							data : {
								locid : locid,
								blokid : blokid,
								userid : userid,
								link : link,
								qr : code
							},
							success : function(data) {
								//alert(data);
								$(function () {
									window.onload = $('#toUnlinkSuccess').click();
								});
								window.setTimeout(function () {
									window.location.href="sky_card.php?";
								}, 3000);
							},
							error : function() {
								alert(data);
								window.location.href = "Terjadi kesalahan dalam unlink card!";
							}
						});
					//}
				});
			</script>

<?php
require('blank-footer.php');
?>