<?php
require('header.php');

if(isset($_GET['id'])){
    $id = $_GET['id'];
    header('location: linkcard.php?id=$id');
}
?>
	<script type="text/javascript">
		$(function () {
			window.onload = $('#play').click();
		});
	</script>

    <form action="linkcard.php" method="post">
        <?php
        $sml = "SELECT * from [PaymentGateway].[dbo].[UserMemberKoperasi] where KID='700097' and UserID='$UserID'";
        $qml = sqlsrv_query($conn, $sml);
        $dml = sqlsrv_fetch_array($qml);
        $mid = $dml[2];
        ?>
        <select class="browser-default hide" id="camera-select"></select>
        <button title="Start Scan QR Barcode" class="hide waves-effect waves-light btn-large primary-color width-100" id="play" type="button"></button>
            
        <input type="hidden" name="kid" id="kid" value="<?php echo $KID; ?>">
        <input type="hidden" name="uid" id="uid" value="<?php echo $UserID; ?>">
        <input type="hidden" name="mid" id="mid" value="<?php echo $mid; ?>">
        <input type="hidden" class="validate" name="code" id="result-code" value="<?php echo $id; ?>" >
        <div class="" id="form-camera">
            <div class="">
                <div class="">
                    <div class="well">
                        <canvas id="webcodecam-canvas" style="width: 100%;"></canvas>
                        <div class="scanner-laser laser-rightBottom" style="opacity: 0.5;"></div>
                        <div class="scanner-laser laser-rightTop" style="opacity: 0.5;"></div>
                        <div class="scanner-laser laser-leftBottom" style="opacity: 0.5;"></div>
                        <div class="scanner-laser laser-leftTop" style="opacity: 0.5;"></div>
                    </div>
                </div>
                <div class="">
                    <div class="thumbnail" id="result">
                        <img id="scanned-img" style="width: 100%;" src="">
                        <div class="caption">
                            <p class="text-center" id="scanned-QR"></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="center">
            <button type="submit" class="btn btn-large primary-color waves-effect waves-light" style="width: 50%;">ok</button>
        </div>
    </form>

    <script type="text/javascript">
        $('#result-code').keyup(function(){
            var id = $('#result-code').val();

            if(id.length == 15){
                window.location.href = 'linkcard.php?id=' + id;
            }
        });
    </script>

        <?php /* if($_SESSION['device'] == 0){ ?>
            <script type="text/javascript">
                $('#ocam').click(function(){
                    Intent.openActivity("QRActivity","card.php");
                });
            </script>
        <?php } ?>

        <?php if($_SESSION['device'] == 1){ ?>
            <script type="text/javascript">
                $('#ocam').click(function(){
                    $('#result-code').val();
                    $('#form-camera').removeClass('hide');
                });

                $('#stop').click(function(){
                    $('#form-camera').addClass('hide');
                });
            </script>
        <?php } */ ?>
<?php
require('footer.php');
?>