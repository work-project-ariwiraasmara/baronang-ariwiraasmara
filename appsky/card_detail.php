<?php
require('header.php');

$sql = "SELECT * from [PaymentGateway].[dbo].[UserPaymentGateway] where KodeUser='$UserID'";
$query = sqlsrv_query($conn, $sql);
$data = sqlsrv_fetch_array($query);

$card = @$_GET['card'];
$sql2 = "SELECT * from [dbo].[MasterCard] where CardNo='$card'";
$query2 = sqlsrv_query($conn, $sql2);
$data2 = sqlsrv_fetch_array($query2);

require('qrcode/qrlib.php');

//barcode
include('barcode128/BarcodeGenerator.php');
include('barcode128/BarcodeGeneratorPNG.php');
include('barcode128/BarcodeGeneratorSVG.php');
include('barcode128/BarcodeGeneratorJPG.php');
include('barcode128/BarcodeGeneratorHTML.php');

function QRCode($kode, $cp, $matrixPointSize) {
	//QR code

	$PNG_TEMP_DIR = dirname(__FILE__).DIRECTORY_SEPARATOR.'qrcard'.DIRECTORY_SEPARATOR;
	$PNG_WEB_DIR = 'qrcard/';

	$errorCorrectionLevel = 'H';
	//$matrixPointSize = 500;

	//$kode = '1000100010001004';
	$filename = $PNG_WEB_DIR.$kode.'.png';
	if (!file_exists($filename)){
		QRcode::png($cp, $filename, $errorCorrectionLevel, $matrixPointSize, 2);
	}

	$generator = new Picqer\Barcode\BarcodeGeneratorPNG();
	$resource = 'data:image/png;base64,'.base64_encode($generator->getBarcode($kode, $generator::TYPE_CODE_128));
}

if(!file_exists('qrcard/'.$card.'.png')) {
	QRCode($card,$card,5);
}
					
?>

	<!-- Toolbar -->
    <div id="toolbar" class="primary-color">
        <a href="javascript:history.back()" class="open-left">
            <i class="ion-android-arrow-back"></i>
        </a>

        <span class="title">Sky Parking</span>

        <div class="right" id="right" style="margin-right: 20px;">
            <a href="#bellnotif" class="modal-trigger">
                <i class="ion-android-notifications"></i>
            </a>
        </div>
    </div>

    <div class="animated delay-1">
    	
    	<div class="primary-color p-20 txt-white" style="height: 120px;">
			<div class="m-l-r-10">
				<div class="left bold">
					<?php echo $card; ?>
				</div>

				<div class="right">
					<?php echo number_format($data2[0],0,',','.'); ?> Point
				</div>
			</div>

			<div class="m-l-r-10 m-t-50">
				<div class="left">
					History Card
				</div>

				<a href="#membership" class="modal-trigger right" style="background: #fff; padding-top: 5px; padding-bottom: 5px; padding-right: 10px; padding-left: 10px; border-radius: 50px;"><div style="color: #000;">Lokasi member</div></a>
				
			</div>
		</div>

		<div class="m-t-10" style="margin-bottom: 89px;">
			<?php
			$sql3 = "SELECT *, convert(varchar(255), TimeStam, 20) from [dbo].[ParkingTrans] where AccNo='$card' order by TimeStam ASC";
			$query3 = sqlsrv_query($conn, $sql3);
			while($data3 = sqlsrv_fetch_array($query3)) { 

				if( is_null($data3[6]) || empty($data3[6]) || $data3[6] == '0' ) { 
					$ket = 'CR';
					$nominal = $data3[7];
				}
				else { 
					$ket = 'DB'; 
					$nominal = $data3[6];
				}

				if( is_null($data3[9]) || empty($data3[9]) || $data3[9] == '' ) {
					$tgl = '0000-00-00 00:00:00';
				}
				else {
					$tgl = $data3[9];
				}

				?>
				<div class="single-news animated fadeinright delay-3 txt-black">
					
					<div>
						<div class="left">
							<?php echo $data3[0]; ?>
						</div>

						<div class="right">
							<?php echo $ket; ?>
						</div>
					</div> <br>

					<div class="m-t-10">
						<div class="left">
							<?php echo $tgl; ?>
						</div>

						<div class="right">
							<?php echo number_format($nominal,0,',','.'); ?> Point
						</div>
					</div> <br>

					<div class="m-t-10">
						<?php echo $data3[5]; ?>
					</div>

				</div>
				
				<?php
			}
			?>
		</div>

		<div class="bottom-fixed" style="background: #fff; border-top: 1px solid #aaa;">
			<div class="row center m-t-10">
				<div class="col s6">
					<a href="#barcode" class="modal-trigger">
						<i class="ion-qr-scanner"></i><br>
						QR Code
					</a>
				</div>

				<div class="col s6">
					<a href="#menu" class="modal-trigger">
						<i class="ion-android-menu"></i><br>
						Menu
					</a>
				</div>
			</div>
		</div>

    </div>

    <div class="modal borad-20" id="barcode">
    	<div class="p-20 center">
    		
    		<center>
    			<img src="<?php echo 'qrcard/'.$card.'.png' ?>" style="height: 150px; width: 150px;" />
    		</center>
    		
    		<p class="m-t-30"><?php echo $card; ?></p>
    		<p class="m-t-30">Anda dapat menggunakan QR Code ini di Gate<br>Jaga kerahasiaan QR Code ini</p>

    	</div>
    </div>

    <div class="modal" id="menu" style="overflow: scroll; border-radius: 20px;">
    	<div class="modal-content choose-date center">

    		<p style="color: #00f;">Pilih Menu</p>


    		<a href="#transferpoint" class="modal-trigger btn width-100 waves-effect waves-light m-t-10" style="background: #fff; height: 50px; border-radius: 30px;"> <div style="margin-top: 8px; color: #00f;">Transfer Point</div> </a>    		
    		<a href="#bayar" class="modal-trigger btn primary-color width-100 waves-effect waves-light m-t-10" style="height: 50px; border-radius: 30px;"> <div style="margin-top: 8px; color: #fff;">Bayar</div> </a>    		
    		<a href="#blockedcard" class="btn primary-color width-100 waves-effect waves-light m-t-10 modal-trigger" style="height: 50px; border-radius: 30px;"> <div style="margin-top: 8px; color: #fff;">Blokir</div> </a>    		
    		<a href="#unlinkcard" class="btn primary-color width-100 waves-effect waves-light m-t-10 modal-trigger" style="height: 50px; border-radius: 30px;"> <div style="margin-top: 8px; color: #fff;">Unlink Card</div> </a>    		
    		<a href="#" class="btn primary-color width-100 waves-effect waves-light m-t-10 modal-close" style="height: 50px; border-radius: 30px;"> <div style="margin-top: 8px; color: #fff;">Tutup</div> </a>

    	</div>
    </div>

    <div class="modal bottom-sheet center" id="transferpoint">
    	<div class="p-20">
    		<h2 class="uppercase txt-black bold">Fitur ini belum difungsikan!</h2>

	        <button class="modal-close btn btn-large primary-color waves-effect waves-light m-t-30 width-100">OK</button>
	    </div>
    </div>

    <div class="modal bottom-sheet center" id="bayar">
    	<div class="p-20">
    		<h2 class="uppercase txt-black bold">Fitur ini belum difungsikan!</h2>

	        <button class="modal-close btn btn-large primary-color waves-effect waves-light m-t-30 width-100">OK</button>
	    </div>
    </div>

    <div class="modal bottom-sheet center" id="blockedcard" style="overflow-y: auto;">
    	<div class="p-20">
    		<form action="proc_blockedcard.php" method="post">
    			<input type="hidden" name="kid" id="kid" value="<?php echo $KID; ?>">
    			<input type="hidden" name="accno" id="accno" value="<?php echo @$_GET['card']; ?>">
	    		<p style="margin-top: -10px; color: #777;">Kartu yang sudah diblokir tidak dapat digunakan lagi</p>
	    		<p class="txt-black">Anda Anda yakin akan memblokir kartu ini?</p>
		    	
		    	<button type="submit" name="bcok" id="bcok" class="btn primary-color width-100 waves-effect waves-light" style="height: 50px; border-radius: 30px;"> <div style="margin-top: 8px;">Ya</div> </button>
		    	<a href="#" class="btn width-100 waves-effect waves-light m-t-10 modal-close" style="background: #fff; height: 50px; border-radius: 30px;"> <div style="margin-top: 8px; color: #00f;">Batal</div> </a>
    		</form>
    	</div>
    </div>

    <div class="modal bottom-sheet center" id="unlinkcard" style="overflow-y: auto;">
    	<div class="p-20">
    		<form action="proc_unlinkcard.php" method="post">
    			<input type="hidden" name="kid" id="kid" value="<?php echo $KID; ?>">
        		<input type="hidden" name="uid" id="uid" value="<?php echo $UserID; ?>">
    			<input type="hidden" name="accno" id="accno" value="<?php echo @$_GET['card']; ?>">
    			<input type="hidden" name="mid" id="mid" value="<?php echo $data2[0]; ?>">

	    		<p class="txt-black">Anda Anda yakin akan melepaskan link kartu ini?</p>
		    	<p style="margin-top: -10px; color: #777;">Kartu ini akan Aktif dan tetap Bisa digunakan. Anda bisa melakukan link kembali nanti</p>

		    	<button type="submit" name="bcok" id="bcok" class="btn primary-color width-100 waves-effect waves-light" style="height: 50px; border-radius: 30px;"> <div style="margin-top: 8px;">Ya</div> </button>
		    	<a href="#" class="btn width-100 waves-effect waves-light m-t-10 modal-close" style="background: #fff; height: 50px; border-radius: 30px;"> <div style="margin-top: 8px; color: #00f;">Batal</div> </a>
    		</form>
    	</div>
    </div>

    <div class="modal bottom-sheet" id="membership" style="overflow-y: auto;">
    	<div class="p-20" >
    		<p class="center txt-black">Kartu ini terdaftar sebagai member di lokasi</p>
    		<p class="center" style="color: #777; margin-top: -10px;">Pastikan Anda menggunakan kartu yang benar</p>

    		<?php
    		$sql1 = "SELECT [CardNo], [LocationID], [VehicleID] from [dbo].[MemberLocation] where CardNo='$card'";
    		//$sql1 = "SELECT [LocationID], [VehicleID], Convert(varchar(255), DateRegister, 20), Convert(varchar(255), ValidDate, 21) from [dbo].[MemberLocation] where CardNo='1000100010001001'";
    		$query1 = sqlsrv_query($conn, $sql1);
    		$no = 0;
    		while($data1 = sqlsrv_fetch_array($query1)) {
    			$sql2 = "SELECT * From [dbo].[LocationMerchant] where LocationID='".$data1[1]."'";
    			$query2 = sqlsrv_query($conn, $sql2);
    			$data2 = sqlsrv_fetch_array($query2);
    			?>
    			<a href="<?php echo '#LOC'.$no; ?>" class="modal-trigger m-t-30 btn btn-large primary-color width-100 waves-light waves-effect" style="border-radius: 30px;">
    				<div class="m-t-10">
    					<?php echo $data2[2]; ?>
    				</div> 
    			</a>
    			<?php
    			$no++;
    		}
    		?>
    	
    	</div>
    </div>

    <?php
    $sql1 = "SELECT [CardNo], [LocationID], [VehicleID] from [dbo].[MemberLocation] where CardNo='$card'";
    //$sql1 = "SELECT [LocationID], [VehicleID], Convert(varchar(255), DateRegister, 20), Convert(varchar(255), ValidDate, 21) from [dbo].[MemberLocation] where CardNo='1000100010001001'";
    $query1 = sqlsrv_query($conn, $sql1);
    $no = 0;
    while($data1 = sqlsrv_fetch_array($query1)) { ?>
		<div class="modal txt-black" id="<?php echo 'LOC'.$no; ?>" style="overflow-y: scroll;">
			<div class="p-20">

				<div class="row p-20">
					<?php  
					for($b = 1; $b < 13; $b++) { 
						$sqlmlm = "SELECT Bulan FROM [dbo].[MemberLocationMonth] where CardNo='$card' and LocationID='".$data1[1]."' and VehicleID='".$data1[2]."' and Status='1' order by Bulan ASC";
						$qmlm = sqlsrv_query($conn, $sqlmlm);
						$dateb = date('Y').'-'.$b.'-01';						
						?>
						<div class="col s6 center m-t-10" style="<?php while($dmlm = sqlsrv_fetch_array($qmlm)) { if($dmlm[0] == $b) { echo 'background: #0a0;'; } } ?> height: 50px; padding: 13px; margin-left: 5px; margin-right: 5px;">
							<?php echo date('F', strtotime($dateb)); ?>
						</div>
						<?php
					}
					?>
				</div>

				<div class="row p-20" style="margin-bottom: 0px;">
					<div class="col s6">
						<div style="height: 20px; width: 20px; background: transparent; display: inline-block;"></div> 
						<div style="margin-left: : 5px; display: inline-block;">Not Member</div> 
					</div>

					<div class="col s6">
						<div style="height: 20px; width: 20px; background: #0a0; display: inline-block;"></div> 
						<div style="margin-left: : 5px; display: inline-block;">In Member</div> 
					</div>
				</div>

			</div>
		</div>
	    <?php
	    $no++;
	}
    ?>

    <div class="modal bottom-sheet" id="bellnotif">
    	<div class="choose-date p-20" style="text-align: center;">
	        <h2 class="uppercase txt-black bold">Fitur ini belum difungsikan!</h2>

	        <button class="modal-close btn btn-large primary-color waves-effect waves-light m-t-30 width-100">OK</button>
	    </div>
	</div>

<?php
require('footer.php');
?>