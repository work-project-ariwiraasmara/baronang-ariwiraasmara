<?php
require('header.php');
require('sidebar-left.php');
?>

	<!-- Toolbar -->
    <div id="toolbar" class="primary-color">
        <a href="javascript:history.back()" class="open-left">
            <i class="ion-android-arrow-back"></i>
        </a>

        <span class="title">Ubah Password</span>

        <div class="right" id="right" style="margin-right: 20px;">
            <a href="#bellnotif" class="modal-trigger">
                <i class="ion-android-notifications"></i>
            </a>
        </div>
    </div>

    <div class="animated delay-1">
    	<div class="p-20">
    		
    		<div class="form-inputs">
    			<form action="" method="post">		
    				<div class="input-field">
	    				<input type="password" name="pass" id="pass" class="validate">
	    				<label for="pass">Password Baru Anda</label>
	    			</div>

	    			<div class="input-field">
	    				<input type="password" name="passval" id="passval" class="validate">
	    				<label for="pass">Ulangi Password</label>
	    			</div>

	    			<div class="input-field p-10 uppercase" id="konf"></div>

	    			<button type="submit" name="ok" id="ok" class="btn btn-large primary-color waves-effect waves-light width-100 m-t-30">Simpan</button>
	    		</form>

	    		<?php
	    		$ok = @$_POST['ok'];
	    		if(isset($ok)) {
	    			$pass = md5(@$_POST['pass']);

	    			if($pass == '' || empty($pass) || is_null($pass)) { ?>
	    				<script type="text/javascript">
	    					alert('Password Tidak Boleh Kosong!');
	    				</script>
	    				<?php
	    			}
	    			else {
	    				$sql = "UPDATE [PaymentGateway].[dbo].[UserPaymentGateway] set Pass='$pass' where KodeUser='$KID'";
		    			$query = sqlsrv_query($conn, $sql);
		    			header('location: profil.php');
	    			}
	    		}
	    		?>
    		</div>

    	</div>
    </div>

    <div class="modal bottom-sheet" id="bellnotif">
        <div class="choose-date p-20" style="text-align: center;">
            <h2 class="uppercase txt-black bold">Fitur ini belum difungsikan!</h2>

            <button class="modal-close btn btn-large primary-color waves-effect waves-light m-t-30 width-100">OK</button>
        </div>
    </div>

    <script type="text/javascript">
    	var angka = ['1','2','3','4','5','6','7','8','9','0'];

    	$('#pass').keyup(function(){
    		/*
    		var pass = $('#pass').val();
    		var passval = $('#passval').val();

    		if(pass == passval) {
    			$('#konf').html('Password Sesuai!');
    			$('#konf').css('background-color','#0f0');
    			$('#konf').css('color','white');
    		}
    		else {
    			$('#konf').html('Password Tidak Sesuai!');
    			$('#konf').css('background-color','#f00');
    			$('#konf').css('color','white');
    		}

            if(pass == '') {
                $('#konf').html('');
                $('#konf').css('background-color','none');
                $('#konf').css('color','none');
            }
            */
    	});

    	$('#passval').keyup(function(){
    		var pass = $('#pass').val();
    		var passval = $('#passval').val();

    		if(pass == passval) {
    			$('#konf').html('Password Sesuai!');
    			$('#konf').css('background-color','#0f0');
    			$('#konf').css('color','white');
    		}
    		else {
    			$('#konf').html('Password Tidak Sesuai!');
    			$('#konf').css('background-color','#f00');
    			$('#konf').css('color','white');
    		}

            if(passval == '') {
                $('#konf').html('');
                $('#konf').css('background-color','none');
                $('#konf').css('color','none');
            }
    	});
    </script>

<?php
require('footer.php');
?>