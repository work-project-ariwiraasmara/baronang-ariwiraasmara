<?php 
require('header.php');
require('sidebar-left.php');
?>

	<!-- Toolbar -->
    <div id="toolbar" class="primary-color">
        <a href="javascript:history.back()" class="open-left">
            <i class="ion-android-arrow-back"></i>
        </a>

        <span class="title">Detail Product</span>

        <div class="right" id="right" style="margin-right: 20px;">
            <a href="#bellnotif" class="modal-trigger">
                <i class="ion-android-notifications"></i>
            </a>
        </div>
    </div>

    <div class="animated delay-1">
		<div class="p-20">
		
			<h4 class="center txt-black">Bulanan</h4><br>

			<?php
			$sql = "SELECT * from [dbo].[LocationParkingProduct] where ProductID='".@$_GET['prod']."' order by Nama ASC";
			$query = sqlsrv_query($conn, $sql);
			$data = sqlsrv_fetch_array($query);

			$sv = "SELECT Name from [dbo].[VehicleType] where VehicleID='".$data[9]."'";
			$qv = sqlsrv_query($conn, $sv);
			$dv = sqlsrv_fetch_array($qv);
			?>

			<p>
				<h4 class="bold"><?php echo $data[2]; ?></h4>
				<?php echo $data[3]; ?>
				<br>
				<br>
				Jenis kendaraan : <?php echo $dv[0]; ?> <br>
				Masa berlaku : <?php echo substr($data[3], 12); ?><br>
				<br>
				Jumlah Poin yang digunakan<br>
				<h4 class="bold"><?php echo $data[5]; ?> Poin</h4><br>
			
				<div class="center">
					Pilih kartu yang akan digunakan
				</div>

				<?php
				$sml = "SELECT * from [PaymentGateway].[dbo].[UserMemberKoperasi] where KID='700097' and UserID='$UserID'";
				$qml = sqlsrv_query($conn, $sml);
				$dml = sqlsrv_fetch_array($qml);
				$mid = $dml[2];
				
				$smc = "SELECT MemberID, CardNo from [dbo].[MemberCard] where MemberID='".$dml[2]."' and Status='1' and isLink='1'";
				$qmc = sqlsrv_query($conn, $smc);
				while($dmc = sqlsrv_fetch_array($qmc)) { ?>
					<div class="single-news animated fadeinright delay-3 txt-black">
						<a href="#modal-<?php echo $dmc[1]; ?>" class="modal-trigger"><div style="color: #00f"><?php echo $dmc[1]; ?></div></a> 
					</div>
					<?php
				}
				?>
			</p>
			

		</div>
	</div>

	<?php
	$sml = "SELECT * from [PaymentGateway].[dbo].[UserMemberKoperasi] where KID='700097' and UserID='$UserID'";
	$qml = sqlsrv_query($conn, $sml);
	$dml = sqlsrv_fetch_array($qml);

	$smc = "SELECT MemberID, CardNo from [dbo].[MemberCard] where MemberID='".$dml[2]."' and Status='1' and isLink='1'";
	$qmc = sqlsrv_query($conn, $smc);
	while($dmc = sqlsrv_fetch_array($qmc)) { ?>
		<div class="modal bottom-sheet txt-black" id="modal-<?php echo $dmc[1]; ?>" style="overflow-y: auto;">
			<div class="p-20">
				<form action="proc_product_detail.php" method="post">
					<p>Masa aktif kartu akan bertambah <?php echo substr($data[3], 12); ?></p>
					
					<input type="text" readonly name="kid" id="kid" value="<?php echo $KID; ?>">
        			<input type="text" readonly name="uid" id="uid" value="<?php echo $UserID; ?>">
        			<input type="text" readonly name="prodid" id="prodid" value="<?php echo @$_GET['prod']; ?>">
					<input type="text" readonly name="mid" id="mid" value="<?php echo $dmc[0]; ?>">
					<input type="text" readonly name="cid" id="cid" value="<?php echo $dmc[1]; ?>">
					<div class="input-field">
						<p>Pilih bulan mulai</p>
						<select class="browser-default" name="chbul" id="chbul">
							<?php
							$dbn = date('m');
							echo $dbn; 
							for($x = 1; $x < 13; $x++) { 
								$xdbn = date('Y').'-'.$x.'-1';
								?>
								<option value="<?php echo $x; ?>" <?php if( $dbn == date('m', strtotime($xdbn)) ) echo 'selected'; ?> ><?php echo date('F', strtotime($xdbn)); ?></option>
								<?php
							}
							?>
						</select>
					</div>
					
					<p class="center m-t-30">
						Apakah anda yakin akan membeli produk<br>
						<span style="color: #00f;"><?php echo $data[2]; ?></span>
						<br>
						menggunakan kartu<br>
						<span style="color: #00f;"><?php echo $dmc[0]; ?></span>
					</p>

					<button name="ok" id="ok" class="btn btn-large width-100 waves-effect waves-light primary-color m-t-30" style="color: #fff; border-radius: 30px;">beli</button>
					<a href="#" class="btn btn-large width-100 waves-effect waves-light modal-close m-t-10" style="background: #fff; color: #00f; border-radius: 30px;"><div class="m-t-10">batal</div></a>
				</form>
			</div>
		</div>
		<?php
	}
	?>

	<div class="modal bottom-sheet" id="bellnotif">
    	<div class="choose-date p-20" style="text-align: center;">
	        <h2 class="uppercase txt-black bold">Fitur ini belum difungsikan!</h2>

	        <button class="modal-close btn btn-large primary-color waves-effect waves-light m-t-30 width-100">OK</button>
	    </div>
	</div>
	
<?php
require('footer.php');
?>