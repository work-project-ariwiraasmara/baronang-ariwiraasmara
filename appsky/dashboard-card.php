<?php
require('qrcode/qrlib.php');

//barcode
include('barcode128/BarcodeGenerator.php');
include('barcode128/BarcodeGeneratorPNG.php');
include('barcode128/BarcodeGeneratorSVG.php');
include('barcode128/BarcodeGeneratorJPG.php');
include('barcode128/BarcodeGeneratorHTML.php');

function QRCode($kode, $cp, $matrixPointSize) {
	//QR code

	$PNG_TEMP_DIR = dirname(__FILE__).DIRECTORY_SEPARATOR.'qrcard'.DIRECTORY_SEPARATOR;
	$PNG_WEB_DIR = 'qrcard/';

	$errorCorrectionLevel = 'H';
	//$matrixPointSize = 500;

	//$kode = '1000100010001004';
	$filename = $PNG_WEB_DIR.$kode.'.png';
	if (!file_exists($filename)){
		QRcode::png($cp, $filename, $errorCorrectionLevel, $matrixPointSize, 2);
	}

	$generator = new Picqer\Barcode\BarcodeGeneratorPNG();
	$resource = 'data:image/png;base64,'.base64_encode($generator->getBarcode($kode, $generator::TYPE_CODE_128));
}

$sql1 = "SELECT LocationID, BlockID, BlockName, BlockDetail, CompanyName, Quota, QuotaLeft, Email, Status from [dbo].[LocationDetail] where Status='1' and BlockID='".@$_COOKIE['BlockID']."'";
//echo $sql1;
$qry1 = sqlsrv_query($conn, $sql1);
$data1 = sqlsrv_fetch_array($qry1)
?>

<script type="text/javascript">
	document.title = "<?php echo 'Sky Parking - '.$data1[3].' - '.$data1[4]; ?>";
</script>

<body class="">
    <div class="m-scene" id="main"> <!-- Main Container -->
        <!-- Page Content -->
        <div class="page" id="content">
	        <?php
	        //error_reporting(0);
	        //session_start();
	        //include 'lang.lib.php';
	        ?>

			<!-- Toolbar -->
		    <div id="toolbar" class="primary-color">
		    	<?php
		    	/*
		        <div class="open-left" id="open-left" data-activates="slide-out-left">
		            <i class="ion-android-menu"></i>
		        </div>
		        */ 
		        ?>

		        <span class="title"> <?php echo $data1[3]; ?> </span>

		        <div class="right" style="margin-right: 20px;">
		            <a href="logout_card.php">
		                <i class="ion-android-exit"></i>
		            </a>
		        </div>
		    </div>

			<div class="animated delay-1">
				<div class="primary-color p-20 txt-white">
					
					<div class="m-l-r-10">
						<h3 class="txt-white bold italic" id="user">
							<?php 
							echo $data1[4].'<br>'.$data1[7]; 
							?>	
						</h3>
						<div class="italic"><?php echo $data1['7']; ?></div>
					</div>
				</div>
				
				<div class="txt-black">
					<a href="#notif-success" id="toSuccess" class="hide modal-trigger">to Success</a>
					<a href="#notif-unlink-success" id="toUnlinkSuccess" class="hide modal-trigger">to Success</a>
					<?php
					//$sql22 = "SELECT Count(*) from [dbo].[LocationCard] where BlockID='".@$_COOKIE['BlockID']."'";
					//$qry22 = sqlsrv_query($conn, $sql22);
					//$data22 = sqlsrv_fetch_array($qry22);
					
					//echo 'Jumlah Baris 1 : '.$data1[5].' == Jumlah Baris 2 : '.$data22[0].'<br>';
					//if($data1[5] == $data22[0]) {
						$no=1;

						$sql2 = "SELECT LocationID, BlockID, CardNo, RegisterDate, RegisterBy, Status from [dbo].[LocationCard] where Status='1' and BlockID='".@$_COOKIE['BlockID']."'";
						$qry2 = sqlsrv_query($conn, $sql2);
						while($data2 = sqlsrv_fetch_array($qry2)) { 

							$smc = "SELECT Barcode from [dbo].[MasterCard] where CardNo='".$data2[2]."'";
							$qmc = sqlsrv_query($conn, $smc);
							$dmc = sqlsrv_fetch_array($qmc);

							if(!file_exists('qrcard/'.$data2[2].'.png')) {
								QRCode($data2[2],$dmc[0],5);
							}
							?>
							<div class="animated fadeinright delay-3" style="margin-top: 10px; border-bottom: 1px solid #ccc;">
								<div class="row">
									<div class="col s10 m-t-10 txt-link">
										<a href="<?php echo '?detail=1&history='.$data2[2]; ?>">
											<?php echo $data2[2]; ?>
										</a>
									</div>

									<div class="col s2 m-t-10">
										<a href="<?php echo '#viewqr'.$no; ?>" class="modal-trigger right">
											<img src="qr code square.png" style="width: 25px; height: 25px;">
										</a>
										<?php //echo 'Quota : xxx'; <i class="ion-qr-scanner right"></i> ?>
									</div>
								</div>
							</div>
							<?php
							$no++;
						}

						for($ql = 0; $ql < $data1[6]; $ql++) { ?>
							<div class="animated fadeinright delay-3 center" style="margin-top: 10px; border-bottom: 1px solid #ccc;">
								<a href="#addcard" class="btn btn-large primary-color waves-effect waves-light borad-20 modal-trigger" style="margin-bottom: 10px;">
									<i class="ion-android-add txt-white"></i>
								</a> 
							</div>
							<?php
						}
					//} 
					?>

					<?php /*
					<div class="m-t-30 m-b-30 center">
						<a href="#" class="btn btn-large primary-color waves-effect waves-light borad-20">
							<i class="ion-android-add txt-white"></i>
						</a> 
					</div>

					<table>
						<?php
						for($i = 101; $i < 196; $i++) {
							echo '<tr><td>'.sha1( md5( crypt('030'.$i, '$6$rounds=999999999') ) ).'</td></tr>';
						}
							?>
					</table>
					*/ 
					?>
				</div>

				

			</div>

			<div class="modal borad-20" id="notif-success">
			   	<div class="modal-content choose-date txt-white" style="background: #49f;">
					<h1 class="uppercase txt-white center">
						<a href="?" class="modal-close"><i class="ion-android-done txt-white"></i></a><br>
						Berhasil menambah kartu!
					</h1>
				</div>
			</div>

			<div class="modal borad-20" id="notif-unlink-success">
			   	<div class="modal-content choose-date txt-white" style="background: #49f;">
					<h1 class="uppercase txt-white center">
						<a href="?" class="modal-close"><i class="ion-android-done txt-white"></i></a><br>
						Berhasil unlink kartu!
					</h1>
				</div>
			</div>
			
			<?php
			//$sql22 = "SELECT Count(*) from [dbo].[LocationCard] where BlockID='".@$_COOKIE['BlockID']."'";
			//$qry22 = sqlsrv_query($conn, $sql22);
			//$data22 = sqlsrv_fetch_array($qry22);
					
			$sql2 = "SELECT LocationID, BlockID, CardNo, RegisterDate, RegisterBy, Status from [dbo].[LocationCard] where Status='1' and BlockID='".@$_COOKIE['BlockID']."'";
			$qry2 = sqlsrv_query($conn, $sql2);

			//if($data1[5] == $data22[0]) {
				$no=1;
				while($data2 = sqlsrv_fetch_array($qry2)) { ?>
					<div class="modal borad-20" id="<?php echo 'viewqr'.$no; ?>">
						<div class="modal-content choose-date txt-black center">
							<p>
								<a href="<?php echo '?detail=1&history='.$data2[2]; ?>">
									<img src="<?php echo 'qrcard/'.$data2[2].'.png'; ?>" />
								</a>
							</p>
							<p>
								<?php echo $data2[2]; ?>
							</p>
							<p class="txt-white">
								<a href="<?php echo '#notif-unlink-'.$no; ?>" id="<?php echo 'unlink'.$no; ?>" class="btn primary-color waves-effect waves-light center m-t-10 borad-20 modal-close modal-trigger">Unlink</a>
							</p>
						</div>
					</div>

					<div class="modal borad-20" id="<?php echo 'notif-unlink-'.$no; ?>">
					   	<div class="modal-content choose-date txt-black">
							<h1 class="uppercase txt-black center">
								<?php echo 'Anda yakin ingin unlink kartu ini ['.$data2[2].'] ?'; ?>
							</h1>
							<div class="row">
								<div class="col s6 center">
									<button id="<?php echo 'unlink-yes-'.$no; ?>" class="btn primary-color btn-large borad-20 waves-light waves-light modal-close">Yes</button>
								</div>
								<div class="col s6 center">
									<button id="<?php echo 'unlink-no-'.$no; ?>" class="btn primary-color btn-large borad-20 waves-light waves-light modal-close">No</a>
								</div>
							</div>
						</div>
					</div>

						<script type="text/javascript">
							$('<?php echo '#unlink-yes-'.$no; ?>').click(function(){
								var locid 	= "<?php echo @$_COOKIE['LocationID']; ?>";
								var blokid 	= "<?php echo @$_COOKIE['BlockID']; ?>";
								var userid 	= "<?php echo @$_COOKIE['UserID']; ?>";
								var code 	= "<?php echo $data2[2]; ?>";
								var link 	= "unlink";

								//alert(link + " " + code);

								//var cf = confirm("Anda yakin ingin Unlink Data Kartu ini?");
								//if(cf == true) {
									$.ajax({
										url : "ajax_ceklinkcard.php",
										type : 'POST',
										data : {
											locid : locid,
											blokid : blokid,
											userid : userid,
											link : link,
											qr : code
										},
										success : function(data) {
											$(function () {
										        window.onload = $('#toUnlinkSuccess').click();
										    });
										    window.setTimeout(function () {
										        window.location.href="sky_card.php?";
										    }, 3000);
										},
										error : function() {
											alert(data);
											window.location.href = "Terjadi kesalahan dalam unlink card!";
										}
									});
								//}
										
							});
						
						</script>
					<?php
					$no++;
				}
			//}
			?>

			<div class="modal borad-20" id="addcard">
				<div class="modal-content choose-date txt-black">
					<p class=""><h1 class="txt-black uppercase bold center">Add Card</h1></p>
					<form method="post" action="">
						<p class="txt-white">
							<a href="proc_linkcard.php" target="_blank" id="linkcard" class="btn btn-large primary-color waves-effect waves-light width-100 txt-white borad-20">Link Card</a>
						</p>

						<p>
							<button type="submit" name="createcard" class="btn btn-large primary-color waves-effect waves-light width-100 borad-20 modal-close">Create Card</button>
						</p>
					</form>
				</div>
			</div>

			<?php
			if(isset($_POST['createcard'])) {
				$locid 	= @$_COOKIE['LocationID'];
				$blokid = @$_COOKIE['BlockID'];
				$reg 	= date('Y-m-d ');

				// GENERATE BARCODE
				$date = date('Y-m-d H:i:s');
				$aa = "select dbo.getKodeCreateCard()";
				$bb = sqlsrv_query($conn, $aa);
				$cc = sqlsrv_fetch_array($bb, SQLSRV_FETCH_NUMERIC);
				if($cc != null){
					$kode =	$cc[0];

					$ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
					$iv = openssl_random_pseudo_bytes($ivlen);
					$ciphertext_raw = openssl_encrypt($kode, $cipher, '70007000', $options=OPENSSL_RAW_DATA, $iv);
					$hmac = hash_hmac('sha256', $ciphertext_raw, '70007000', $as_binary=true);
					$ciphertext = base64_encode( $iv.$hmac.$ciphertext_raw );
				}

				// INSERT INTO LOCATION CARD
				$s1 = "INSERT into [dbo].[LocationCard] values('$locid', '$blokid', '$kode', 'CMP', '$reg', '".@$_COOKIE['UserID']."', '1')";
				$q1 = sqlsrv_query($conn, $s1);
				if($q1) {
					// INSERT INTO MASTER CARD
					$s2 = "INSERT into [dbo].[MasterCard](CardNo, Barcode, DateCreate, Status, CardID) values('$kode','$ciphertext','$date','1', '$kode')";
					//echo $s2.'<br>';
					$q2 = sqlsrv_query($conn, $s2);

					if($q2) { 
						// UPDATE QUOTA LEFT
						$ql = (int)$data1[6] - 1;
						$s3 = "UPDATE [dbo].[LocationDetail] SET QuotaLeft='$ql' where BlockID='".@$_COOKIE['BlockID']."'"; 
						$q3 = sqlsrv_query($conn, $s3);

						if($q3) { ?>
							<script type="text/javascript">
								//alert('Berhasil Tambah dan Buat Kartu Baru!');
								$(function () {
							        window.onload = $('#toSuccess').click();
							    });
							    window.setTimeout(function () {
							        window.location.href="sky_card.php?";
							    }, 3000);
							</script>
							<?php 
						}
					}
				}
				
			}
			?>

<?php
require('blank-footer.php');
?>
