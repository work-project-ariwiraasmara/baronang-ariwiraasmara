<?php require('header.php');?>

        <div class="col-lg-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Account Number Transfer</h3>
                </div>
                <div class="box-body">
            <?php
            if($_POST['acc'] == ''){
                echo "<script language='javascript'>history.go(-1);</script>";
            }
            else{

                $acc = $_POST['acc'];

                $x = "exec dbo.ListRegTransferList '$_SESSION[KID]','$_SESSION[MemberID]','$acc'";
                $y = sqlsrv_query($conn, $x);
                $z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC);

                $a = "exec [dbo].[RegularSavingAccSearchTransfer] '$_SESSION[KID]','$acc'";
                $b = sqlsrv_query($conn, $a);
                $c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC);
                if($c != null){
                    ?>
                    <form action="app-save.php" method="POST" class="form-horizontal">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="col-sm-3" style="text-align: left;">Account Number Transfer</label>
                                <div class="col-sm-9">
                                    <table class="table table-bordered">
                                        <tr>
                                            <td><?php echo $c[2]; ?></td>
                                            <td><?php echo $c[7]; ?></td>
                                            <td>
                                                <input type="radio" name="acc" class="minimal" value="<?php echo $c[2]; ?>">
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="box-footer">
                                <div class="row">
                                    <button type="button" class="btn btn-flat btn-block btn-success" data-toggle="modal" data-target="#myModal">Save</button>
                                </div>
                            </div>

                            <!-- Modal -->
                            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog modal-sm" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Security Authentication</h4>
                                        </div>
                                        <div class="modal-body">
                                            <input type="password" name="pin" class="form-control" placeholder="Input PIN">
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-success btn-sm btn-block btn-flat"><i class="fa fa-check"></i> Submit</button>
                                            <button type="button" class="btn btn-default btn-sm btn-block btn-flat" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </form>
                <?php }
                else if($z != null){
                    echo "<span class='lead'>Account number already exist.</span>";
                }
                else{
                    echo "<span class='lead'>Account number not found. Please try again.</span>";
                    }
                }
                ?>
                </div>
            </div>
        </div>

<?php require('footer.php');?>