<?php require('header.php');?>

<?php
unset($_SESSION['acc']);

$_SESSION['url'] = 'balance.php';
if($_SESSION['message'] != '' and $_SESSION['time'] != ''){ ?>
    <div class="col-sm-12">
        <div class="box box-solid">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="box-body">
                <div style="margin-top: 30%;margin-bottom: 30%;">
                    <div class="widget-user-image">
                        <img class="profile-user-img img-responsive" src="images/check-list.png" style="border: none;">
                    </div>
                    <h3 class="text-center"><?php echo $lang->lang('Transaksi Berhasil', $conn); ?></h3>
                    <br>
                    <br>
                    <br>
                    <div class="text-center">
                        <?php echo $_SESSION['message']; ?>
                    </div>
                    <br>
                </div>
            </div>
        </div>
    </div>
<?php } ?>

<?php require('footer.php');?>
