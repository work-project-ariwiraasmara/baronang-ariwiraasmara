<?php require('header.php');?>

<?php
if(!isset($_SESSION['KID'])){
    echo "<script language='javascript'>document.location='close.php';</script>";
}

if(!isset($_GET['acc'])){
    echo "<script language='javascript'>document.location='balance.php';</script>";
}
else{
    $cardno = '';
    $accno = '';
    $xx = "exec dbo.ListemoneyCard '$_SESSION[KID]','$_GET[acc]'";
    $yy = sqlsrv_query($conn, $xx);
    $zz = sqlsrv_fetch_array($yy, SQLSRV_FETCH_NUMERIC);
    if($zz != null){
        $cardno = $zz[4];
        $accno = $zz[0];
    }
    else{
        echo "<script language='javascript'>document.location='balance.php';</script>";
    }
}

?>

    <div class="col-lg-12">
        <div class="box box-solid">
            <div class="box-header">
                <h3 class="box-title"><?php echo $lang->lang('Blokir Kartu e-Money', $conn); ?></h3>
            </div>
            <div class="box-body">
                <form id="procpin" action="procblock.php" method="POST" class="form-horizontal">
                    <div class="col-sm-12">
                        <input type="hidden" name="acc" value="<?php echo $_GET['acc']; ?>" readonly>
                        <input type="hidden" name="kid" value="<?php echo $_SESSION['KID']; ?>" readonly>
                        <input type="hidden" name="mid" value="<?php echo $_SESSION['MemberID']; ?>" readonly>
                        <input type="hidden" name="jenis" value="18" readonly>
                        <input type="hidden" name="amount" value="1" readonly>
                        <div class="form-group">
                            <p class="text-center">
                                Apakah anda yakin akan memblokir kartu e-money <b><i><?php echo $accno; ?></i></b> dengan nomor kartu <b><?php echo $cardno; ?></b> ?
                            </p>
                        </div>
                        <div class="box-footer">
                            <p class="text-center">Masukan PIN anda untuk mengkonfirmasi</p>
                            <div class="col-xs-12">
                                <input id="pin" name="pin" type="password" class="form-control text-center" style="font-size: 2em;background: white;" readonly>
                            </div>
                            <div class="col-xs-4" style="padding: 20px;"><button type="button" id="n1" class="btn btn-lg btn-default" style="border-radius: 40px;">1</button></div>
                            <div class="col-xs-4" style="padding: 20px;"><button type="button" id="n2" class="btn btn-lg btn-default" style="border-radius: 40px;">2</button></div>
                            <div class="col-xs-4" style="padding: 20px;"><button type="button" id="n3" class="btn btn-lg btn-default" style="border-radius: 40px;">3</button></div>
                            <div class="col-xs-4" style="padding: 20px;"><button type="button" id="n4" class="btn btn-lg btn-default" style="border-radius: 40px;">4</button></div>
                            <div class="col-xs-4" style="padding: 20px;"><button type="button" id="n5" class="btn btn-lg btn-default" style="border-radius: 40px;">5</button></div>
                            <div class="col-xs-4" style="padding: 20px;"><button type="button" id="n6" class="btn btn-lg btn-default" style="border-radius: 40px;">6</button></div>
                            <div class="col-xs-4" style="padding: 20px;"><button type="button" id="n7" class="btn btn-lg btn-default" style="border-radius: 40px;">7</button></div>
                            <div class="col-xs-4" style="padding: 20px;"><button type="button" id="n8" class="btn btn-lg btn-default" style="border-radius: 40px;">8</button></div>
                            <div class="col-xs-4" style="padding: 20px;"><button type="button" id="n9" class="btn btn-lg btn-default" style="border-radius: 40px;">9</button></div>
                            <div class="col-xs-4" style="padding: 20px;"></div>
                            <div class="col-xs-4" style="padding: 20px;"><button type="button" id="n0" class="btn btn-lg btn-default" style="border-radius: 40px;">0</button></div>
                            <div class="col-xs-4" style="padding: 20px;"><button type="button" id="nb" class="btn btn-lg btn-default" style="border-radius: 40px;"><i class="fa fa-chevron-left"></i></button></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        function cekDigit(){
            var a = $('#pin').val();
            if(a.length == 6){
                $('#procpin').submit();
            }

            if(a.length > 6){
                $('#pin').val($('#pin').val().slice(0, -1));
            }
        }

        $('#n1').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '1');

            cekDigit();
        });
        $('#n2').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '2');

            cekDigit();
        });
        $('#n3').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '3');

            cekDigit();
        });
        $('#n4').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '4');

            cekDigit();
        });
        $('#n5').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '5');

            cekDigit();
        });
        $('#n6').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '6');

            cekDigit();
        });
        $('#n7').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '7');

            cekDigit();
        });
        $('#n8').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '8');

            cekDigit();
        });
        $('#n9').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '9');

            cekDigit();
        });
        $('#n0').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '0');

            cekDigit();
        });
        $('#nb').click(function(){
            $('#pin').val($('#pin').val().slice(0, -1));
        });
    </script>
<?php require('footer.php');?>
