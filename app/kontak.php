<?php require('header.php');?>

<div class="col-sm-12">
    <div class="box box-solid">
        <div class="box-body">
            <ul class="list-group list-group-unbordered">
                <h4 class="text-bold" style="color: #0a3177">Kontak Kami</h4>
                <li class="list-group-item">
                    <b>PT. Baronang Asia Pasifik</b><br>
                    <br>
                    Plaza Sentra Indoraya Blok A No.3, Bojong Jaya, Karawaci, Tangerang City, Banten<br>
                    <br>
                    <i class="fa fa-phone"></i> (021) 5533507<br>
                    <i class="fa fa-envelope-o"></i> info@baronang.com<br>
                </li>
                <li class="list-group-item">
                    <iframe class="img-responsive" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.4782907393383!2d106.61866431476895!3d-6.2004571955112935!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x3feea099f68ce9ab!2sPT.+Baronang+Asia+Pasifik!5e0!3m2!1sen!2suk!4v1522141418467" frameborder="0" style="border:0" allowfullscreen></iframe>
                </li>
            </ul>
        </div>
    </div>
</div>

<?php require('footer.php');?>

