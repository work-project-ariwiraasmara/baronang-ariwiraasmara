<?php require('header.php');?>

    <div class="col-sm-12">
        <div class="box box-solid">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="box-body">
                <div style="text-align: center;">
                    <img width="50" height="50" src="images/loading.gif">
                </div>
            </div>
        </div>
    </div>

<?php
require_once 'lib/googleLib/GoogleAuthenticator.php';

if($_POST['kid'] == '' || $_POST['mid'] == '' || $_POST['acc'] == '' || $_POST['jenis'] == '' || $_POST['amount'] == '' || $_POST['amount'] <= 0){
    echo "<script language='javascript'>history.go(-1);</script>";
}
else{

    $kid = $_POST['kid'];
    $mid = $_POST['mid'];
    $acc = $_POST['acc'];
    $amount = $_POST['amount'];
    $jenis = $_POST['jenis'];

    $regacc = $_POST['regacc'];
    $aro = $_POST['aro'];
    $itm = $_POST['itm'];
    $itaa = $_POST['itaa'];
    //$gac = $_POST['gac'];
    $pin = md5($_POST['pin']);

    $a = "select* from [dbo].[UserPaymentGateway] where KodeUser = '$_SESSION[UserID]'";
    $b = sqlsrv_query($conn, $a);
    $c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC);
    if($c != null){
        $ga = new GoogleAuthenticator();
        //$checkResult = $ga->verifyCode($c[11], $gac, 2);    // 2 = 2*30sec clock tolerance
        if ($pin == $c[12])
        {
            //$_SESSION['googleCode'] = $gac;

            $x = "exec [dbo].[RegularSavingAccSearch] '$kid','$_SESSION[MemberID]','$regacc'";
            $y = sqlsrv_query($conn, $x);
            $z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC);
            if($z != null){
                $q = "exec [dbo].[RegularSavingTypeSearch] '$kid','$z[3]'";
                $w = sqlsrv_query($conn, $q);
                $e = sqlsrv_fetch_array($w, SQLSRV_FETCH_NUMERIC);

                $balance = $z[5] - $e[4];
                if($balance >= $amount){
                    $p = "exec [dbo].[TimeDepositTypeSearch] '$kid','$acc'";
                    $l = sqlsrv_query($conn, $p);
                    $m = sqlsrv_fetch_array($l, SQLSRV_FETCH_NUMERIC);
                    if($m != null){
                        if($m[4] <= $amount){
                            $sql = "exec [dbo].[ProsesTimeDepositOpen] '$kid', '$_SESSION[MemberID]','$acc','$aro','$itm','$itaa','$amount','1','$regacc','$_SESSION[UserID]','','$regacc'";
                            $exec = sqlsrv_query($conn, $sql);
                            if($exec){
                                messageAlert('Berhasil membuka deposito');
                                echo "<script language='javascript'>document.location='notif.php';</script>";
                            }
                            else{
                                echo "<script>System.showToast('Gagal membuka akun');history.go(-2);</script>";
                            }
                        }
                        else{
                            $min = number_format($m[4]);
                            echo "<script>System.showToast('Minimum jumlah deposito adalah ".$min."');history.go(-2);</script>";
                        }
                    }
                    else{
                        echo "<script>System.showToast('Gagal melakukan transaksi');history.go(-2);</script>";
                    }
                }
                else{
                    echo "<script>System.showToast('Saldo tabungan tidak mencukupi');history.go(-2);</script>";
                }
            }
            else{
                echo "<script>System.showToast('Akun tabungan tidak ditemukan');history.go(-2);</script>";
            }
        }
        else
        {
            echo "<script>System.showToast('PIN tidak benar. Transaksi tidak dapat dilakukan');history.go(-2);</script>";
        }
    }
    else{
        echo "<script language='javascript'>document.location='login.php';</script>";
    }
}
?>

<?php require('footer.php');?>
