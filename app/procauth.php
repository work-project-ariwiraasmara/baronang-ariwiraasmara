<?php require('header.php');?>

<?php
session_start();
require_once 'lib/googleLib/GoogleAuthenticator.php';

if($_POST['kid'] == '' || $_POST['mid'] == '' || $_POST['acc'] == '' || $_POST['jenis'] == '' || $_POST['amount'] == '' || $_POST['amount'] <= 0){
    echo "<script language='javascript'>System.showToast('Harap input dengan benar');history.go(-1);</script>";
}
else{

    $kid = $_POST['kid'];
    $mid = $_POST['mid'];
    $acc = $_POST['acc'];
    $amount = $_POST['amount'];
    $jenis = $_POST['jenis'];

    $qrCodeUrl = '';
    $a = "select* from [dbo].[UserPaymentGateway] where KodeUser = '$_SESSION[UserID]'";
    $b = sqlsrv_query($conn, $a);
    $c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC);
    if($c != null){
        $xx = "select* from [dbo].[ListKoperasiView] where UserID = '$_SESSION[UserID]' and KodeMember = '$mid'";
        $yy = sqlsrv_query($conn, $xx);
        $zz = sqlsrv_fetch_array($yy, SQLSRV_FETCH_NUMERIC);

        $ga = new GoogleAuthenticator();
        $qrCodeUrl = $ga->getQRCodeGoogleUrl($c[2], $c[11], 'Baronang : '.$zz[1]);
    }
    else{
        echo "<script language='javascript'>document.location='login.php';</script>";
    }

    if($jenis == 9){
        //buka tabungan
        $link = 'procreg.php';
    }
    else if($jenis == 11){
        //buka deposito
        $link = 'proctime.php';
    }
    else if($jenis == 10){
        //buka pinjaman
        $link = 'procloan.php';
    }
    else if($jenis == 2){
        //penarikan
        $link = 'procwreg.php';
    }
    else if($jenis == 13){
        //buka emoney
        $link = 'procemoney.php';
    }
    else if($jenis == 14){
        //topup
        $link = 'topemoney2.php';
    }
    else if($jenis == 15){
        //transfer tabungan
        $link = 'app-trans.php';
    }
    else if($jenis == 16){
        //buka edc
        $link = 'procedc.php';
    }
    else if($jenis == 17){
        //bayar billing
        $link = 'procbill.php';
    }
    else{
        $link = 'procapp.php';
    }
    ?>

    <div class="col-lg-12">
        <div class="box box-solid">
            <form id="procpin" action="<?php echo $link; ?>" method="post">
                <div class="box-header text-center">
                    <h3 class="box-title">Masukan PIN anda</h3>
                </div>
                <div class="box-body text-center">
                    <input type="hidden" name="kid" value="<?php echo $kid; ?>" readonly>
                    <input type="hidden" name="mid" value="<?php echo $mid; ?>" readonly>
                    <input type="hidden" name="acc" value="<?php echo $acc; ?>" readonly>
                    <input type="hidden" name="amount" value="<?php echo $amount; ?>" readonly>
                    <input type="hidden" name="jenis" value="<?php echo $jenis; ?>" readonly>

                    <?php if(isset($_POST['regacc'])){ ?>
                        <input type="hidden" name="regacc" value="<?php echo $_POST['regacc']; ?>" readonly>
                    <?php } ?>

                    <?php if(isset($_POST['aro'])){ ?>
                        <input type="hidden" name="aro" value="<?php echo $_POST['aro']; ?>" readonly>
                    <?php } ?>

                    <?php if(isset($_POST['itm'])){ ?>
                        <input type="hidden" name="itm" value="<?php echo $_POST['itm']; ?>" readonly>
                    <?php } ?>

                    <?php if(isset($_POST['itaa'])){ ?>
                        <input type="hidden" name="itaa" value="<?php echo $_POST['itaa']; ?>" readonly>
                    <?php } ?>

                    <?php if(isset($_POST['acctrans'])){ ?>
                        <input type="hidden" name="acctrans" value="<?php echo $_POST['acctrans']; ?>" readonly>
                    <?php } ?>

                    <?php if(isset($_POST['fav'])){ ?>
                        <input type="hidden" name="fav" value="<?php echo $_POST['fav']; ?>" readonly>
                    <?php } ?>

                    <?php if(isset($_POST['note'])){ ?>
                        <input type="hidden" name="note" value="<?php echo $_POST['note']; ?>" readonly>
                    <?php } ?>

                    <div class="col-xs-12">
                        <input id="pin" name="pin" type="password" class="form-control text-center" style="font-size: 2em;background: white;" readonly>
                    </div>
                    <div class="col-xs-4" style="padding: 20px;"><button type="button" id="n1" class="btn btn-lg btn-default" style="border-radius: 40px;">1</button></div>
                    <div class="col-xs-4" style="padding: 20px;"><button type="button" id="n2" class="btn btn-lg btn-default" style="border-radius: 40px;">2</button></div>
                    <div class="col-xs-4" style="padding: 20px;"><button type="button" id="n3" class="btn btn-lg btn-default" style="border-radius: 40px;">3</button></div>
                    <div class="col-xs-4" style="padding: 20px;"><button type="button" id="n4" class="btn btn-lg btn-default" style="border-radius: 40px;">4</button></div>
                    <div class="col-xs-4" style="padding: 20px;"><button type="button" id="n5" class="btn btn-lg btn-default" style="border-radius: 40px;">5</button></div>
                    <div class="col-xs-4" style="padding: 20px;"><button type="button" id="n6" class="btn btn-lg btn-default" style="border-radius: 40px;">6</button></div>
                    <div class="col-xs-4" style="padding: 20px;"><button type="button" id="n7" class="btn btn-lg btn-default" style="border-radius: 40px;">7</button></div>
                    <div class="col-xs-4" style="padding: 20px;"><button type="button" id="n8" class="btn btn-lg btn-default" style="border-radius: 40px;">8</button></div>
                    <div class="col-xs-4" style="padding: 20px;"><button type="button" id="n9" class="btn btn-lg btn-default" style="border-radius: 40px;">9</button></div>
                    <div class="col-xs-4" style="padding: 20px;"></div>
                    <div class="col-xs-4" style="padding: 20px;"><button type="button" id="n0" class="btn btn-lg btn-default" style="border-radius: 40px;">0</button></div>
                    <div class="col-xs-4" style="padding: 20px;"><button type="button" id="nb" class="btn btn-lg btn-default" style="border-radius: 40px;"><i class="fa fa-chevron-left"></i></button></div>
                </div>
            </form>
        </div>
    </div>

    <script type="text/javascript">
        function cekDigit(){
            var a = $('#pin').val();
            if(a.length == 6){
                $('#procpin').submit();
            }

            if(a.length > 6){
                $('#pin').val($('#pin').val().slice(0, -1));
            }
        }

        $('#n1').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '1');

            cekDigit();
        });
        $('#n2').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '2');

            cekDigit();
        });
        $('#n3').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '3');

            cekDigit();
        });
        $('#n4').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '4');

            cekDigit();
        });
        $('#n5').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '5');

            cekDigit();
        });
        $('#n6').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '6');

            cekDigit();
        });
        $('#n7').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '7');

            cekDigit();
        });
        $('#n8').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '8');

            cekDigit();
        });
        $('#n9').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '9');

            cekDigit();
        });
        $('#n0').click(function(){
            var text = $('#pin').val();
            $('#pin').val(text + '0');

            cekDigit();
        });
        $('#nb').click(function(){
            $('#pin').val($('#pin').val().slice(0, -1));
        });
    </script>
<?php } ?>

<?php require('footer.php');?>
