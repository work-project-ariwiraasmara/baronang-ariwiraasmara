<?php require('header.php');?>

<?php if(isset($_SESSION['KID'])){
    unset($_SESSION['type']);
    unset($_SESSION['acc']);

    $_SESSION['url'] = 'balance.php';

    $x = "select* from [dbo].[UserMemberKoperasi] where UserID = '$_SESSION[UserID]' and KID = '$_SESSION[KID]'";
    $y = sqlsrv_query($conn, $x);
    $z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC);
    ?>

    <div class="col-sm-12">
        <div class="box box-solid">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="box-body">
                <div class="col-xs-12 text-center">
                    <img class="img-responsive" style="border-radius: 10px;" src="<?php echo $_SESSION['CardImage']; ?>" data-toggle="modal" data-target="#myCard">
                    <?php
                    //set it to writable location, a place for temp generated PNG files
                    $PNG_TEMP_DIR = dirname(__FILE__).DIRECTORY_SEPARATOR.'temp'.DIRECTORY_SEPARATOR;
                    $PNG_WEB_DIR = 'temp/';

                    $code = $_SESSION['UserID'];
                    $errorCorrectionLevel = 'H';
                    $matrixPointSize = 5;
                    $filename = $PNG_TEMP_DIR.'qr'.md5($code.'|'.$errorCorrectionLevel.'|'.$matrixPointSize).'.png';
                    ?>
                    <img src="<?php echo $PNG_WEB_DIR.basename($filename); ?>" style="width: 15%;bottom: 10%;right: 8%;position: absolute;">
                    <span class="lead" style="position: absolute;top: 12%;right: 8%;color: <?php echo $_SESSION['CardFontColor']; ?>;">
                        <b>
                            <?php
                            $word = explode(' ', $_SESSION['NickName']);
                            echo ucwords($word[0].' '.$word[1]);
                            ?>
                        </b>
                    </span>
                    <span class="lead" style="position: absolute;bottom: 5%;left: 8%;color: <?php echo $_SESSION['CardFontColor']; ?>;"><?php echo $_SESSION['NamaUser']; ?></span>
                    <span class="lead" style="position: absolute;bottom: -4%;left: 8%;color: <?php echo $_SESSION['CardFontColor']; ?>;"><?php echo $_SESSION['KID'].' '.$_SESSION['MemberID']; ?></span>
                    <?php if($_SESSION['Logo'] != ''){ ?>
                        <img src="<?php echo '../koperasi/'.$_SESSION['Logo']; ?>" style="width: 15%;top: 5%;left: 8%;position: absolute;">
                    <?php } ?>
                </div>
            </div>
        </div>

        <span><?php echo $lang->lang('Untuk melakukan transaksi, pilih akun dibawah ini', $conn); ?></span>
        <?php if ($_SESSION['tipe'] ==0) { ?>
        <div class="box box-solid collapsed-box">
            <div class="box-header">
                <div class="pull-left">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                </div>
                <h3 class="box-title btn btn-sm" style="color: #0a3177" data-widget="collapse"><?php echo $lang->lang('Limit Belanja', $conn); ?></h3>
                <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="table">
                    <?php
                    $a = "exec [dbo].[ListLimitBelanjaBal] '$z[0]','$z[2]'";
                    $b = sqlsrv_query($conn, $a);
                    while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                        ?>
                        <tr>
                            <td><a href="report.php?type=lim&id=<?php echo $c[3]; ?>"><b><?php echo $c[3]; ?></b><span class="pull-right">Rp. <?php echo number_format($c[4]); ?></span></a></td>
                        </tr>
                    <?php } ?>
                </table>
            </div>
            <!-- /.box-body -->
        </div>

        <div class="box box-solid collapsed-box">
            <div class="box-header">
                <div class="pull-left">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                </div>
                <h3 class="box-title btn btn-sm" style="color: #0a3177" data-widget="collapse"><?php echo $lang->lang('Simpanan', $conn); ?></h3>
                <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="table">
                    <?php
                    $a = "exec [dbo].[ListBasicSavingBal] '$z[0]','$z[2]'";
                    $b = sqlsrv_query($conn, $a);
                    while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                        ?>
                        <tr>
                            <td><a href="report.php?type=bas&id=<?php echo $c[5]; ?>"><b><?php echo $c[5]; ?></b> <?php echo $c[4]; ?><span class="pull-right">Rp. <?php echo number_format($c[7]); ?></span></a></td>
                        </tr>
                    <?php } ?>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
		<?php } ?>

        <div class="box box-solid">
            <div class="box-header">
                <div class="pull-left">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                </div>
                <h3 class="box-title btn btn-sm" style="color: #0a3177" data-widget="collapse"><?php echo $lang->lang('Tabungan', $conn); ?></h3>
                <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="table">
                    <?php
                    $a = "exec [dbo].[ListRegularSavingBal] '$z[0]','$z[2]'";
                    $b = sqlsrv_query($conn, $a);
                    while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                        ?>
                        <tr>
                            <td><a href="report.php?type=reg&id=<?php echo $c[2]; ?>"><b><?php echo $c[2]; ?></b> <?php echo $c[4]; ?><span class="pull-right">Rp. <?php echo number_format($c[5]); ?></span></a></td>
                        </tr>
                    <?php } ?>
                    <tr>
                        <td>
                            <a href="oregular_saving.php" class="pull-right"><?php echo $lang->lang('Buka Akun Tabungan', $conn); ?></a>
                        </td>
                    </tr>
                </table>
            </div>
            <!-- /.box-body -->
        </div>

        <div class="box box-solid collapsed-box">
            <div class="box-header">
                <div class="pull-left">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                </div>
                <h3 class="box-title btn btn-sm" style="color: #0a3177" data-widget="collapse"><?php echo $lang->lang('Deposito', $conn); ?></h3>
                <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="table">
                    <?php
                    $a = "exec [dbo].[ListTimeDepositBal] '$z[0]','$z[2]'";
                    $b = sqlsrv_query($conn, $a);
                    while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                        ?>
                        <tr>
                            <td><a href="report.php?type=time&id=<?php echo $c[7]; ?>"><b><?php echo $c[7]; ?></b> <?php echo $c[9]; ?><span class="pull-right">Rp. <?php echo number_format($c[14]); ?></span></a></td>
                        </tr>
                    <?php } ?>
                    <tr>
                        <td>
                            <a href="otime_deposit.php" class="pull-right"><?php echo $lang->lang('Buka Akun Deposito', $conn); ?></a>
                        </td>
                    </tr>
                </table>
            </div>
            <!-- /.box-body -->
        </div>

        <div class="box box-solid collapsed-box">
            <div class="box-header">
                <div class="pull-left">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                </div>
                <h3 class="box-title btn btn-sm" style="color: #0a3177" data-widget="collapse"><?php echo $lang->lang('Pinjaman', $conn); ?></h3>
                <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="table">
                    <tr>
                        <td>
                            <a href="listloan.php"><?php echo $lang->lang('Riwayat Pengajuan Pinjaman', $conn); ?></a>
                        </td>
                    </tr>
                    <?php
                    $a = "exec dbo.LoanApplicationSearchMemberRelease '$_SESSION[KID]','$_SESSION[MemberID]'";
                    $b = sqlsrv_query($conn, $a);
                    while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                        $type = '';
                        $x = "exec dbo.LoanTypeSearch '$_SESSION[KID]','$c[4]'";
                        $y = sqlsrv_query($conn, $x);
                        $z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC);
                        if($z != null){
                            $type = $z[1];
                        }

                        //cek yang sudah bayar
                        $aa = "exec dbo.LoanPaidAll '$_SESSION[KID]','$c[1]'";
                        $bb = sqlsrv_query($conn, $aa);
                        $cc = sqlsrv_fetch_array( $bb, SQLSRV_FETCH_NUMERIC);
                        if($cc != null){
                            $sisa = $c[5] - ($cc[1]+$cc[2]);
                        }
                        else{
                            $sisa = $c[5];
                        }
                        ?>
                        <tr>
                            <td><a href="report.php?type=loan&id=<?php echo $c[1]; ?>"><b><?php echo $c[1]; ?></b> <?php echo $type; ?><span class="pull-right">Rp. <?php echo number_format($sisa); ?></span></a></td>
                        </tr>
                    <?php } ?>
                    <tr>
                        <td>
                            <a href="oloan.php" class="pull-right"><?php echo $lang->lang('Buka Pengajuan Pinjaman', $conn); ?></a>
                        </td>
                    </tr>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
        <div class="box box-solid collapsed-box">
            <div class="box-header">
                <div class="pull-left">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                </div>
                <h3 class="box-title btn btn-sm" style="color: #0a3177" data-widget="collapse"><?php echo $lang->lang('e-Money', $conn); ?></h3>
                <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="table">
                    <?php
                    $a = "exec [dbo].[ListemoneyAcc] '$_SESSION[KID]','$_SESSION[MemberID]'";
                    $b = sqlsrv_query($conn, $a);
                    while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                        ?>
                        <tr>
                            <td><a href="report.php?type=emon&id=<?php echo $c[0]; ?>"><b><?php echo $c[0]; ?></b><span class="pull-right">Rp. <?php echo number_format($c[4]); ?></span></a></td>
                        </tr>
                    <?php } ?>
                </table>
            </div>
            <!-- /.box-body -->
        </div>

        <!-- <div class="box box-solid collapsed-box">
            <div class="box-header">
                <div class="pull-left">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                </div>
                <h3 class="box-title btn btn-sm" style="color: #0a3177" data-widget="collapse"><?php echo $lang->lang('Billing', $conn); ?></h3>
            </div>
            <div class="box-body">
                <table class="table">
                    <?php
                    $amount = 0;
                    $admin = 0;
                    $denda = 0;
                    $totalBill = 0;
                    $a = "exec [dbo].[ListBillMember] '$_SESSION[KID]','$_SESSION[MemberID]'";
                    $b = sqlsrv_query($conn, $a);
                    while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                        $amount = $c[6];
                        $admin = $c[7];
                        $denda = $c[8];
                        $totalBill = $amount;
                        ?>
                        <tr>
                            <td><a href="report.php?type=bill&id=<?php echo $c[0]; ?>"><b><?php echo $c[0].' '.$c[4]; ?></b><span class="pull-right">Rp. <?php echo number_format($totalBill); ?></span></a></td>
                        </tr>
                    <?php } ?>
                </table>
            </div>
        </div> -->

        <?php if ($_SESSION['tipe'] == 1) { ?>
        <div class="box box-solid collapsed-box">
            <div class="box-header">
                <div class="pull-left">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                </div>
                <h3 class="box-title btn btn-sm" style="color: #0a3177" data-widget="collapse"><?php echo $lang->lang('EDC', $conn); ?></h3>
                <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="table">
                    <?php
                    $a = "select* from Gateway.dbo.EDCList where KID = '$_SESSION[KID]' and MemberID = '$_SESSION[MemberID]' and UserIDBaronang = '$_SESSION[UserID]'";
                    $b = sqlsrv_query($conn, $a);
                    while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                        ?>
                        <tr>
                            <td><a href="report.php?type=edc&id=<?php echo $c[0]; ?>"><b><?php echo $c[0]; ?></b></a></td>
                        </tr>
                    <?php } ?>
                    <tr>
                        <td>
                            <a href="oedc.php" class="pull-right"><?php echo $lang->lang('Buka EDC', $conn); ?></a>
                        </td>
                    </tr>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
        <?php } ?>
    </div>
<?php
}
else{
    echo "<script language='javascript'>document.location='logout.php';</script>";
}
?>

<?php require('footer.php');?>
