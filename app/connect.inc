<?php
error_reporting(0);
include("conf/conf.inc");
include("lib/encrDecr/ed.php");
include("lib/libLang/lang.php");
$lang = new LangLib();

$u = decr($uid, $key);
$p = decr($pwd, $key);
$d = decr($databaseName, $key);
$s = decr($serverName, $key);

$connectionInfo = array( "UID"=>$u,
    "PWD"=>$p,
    "Database"=>$d);

$conn = sqlsrv_connect($s, $connectionInfo);

function messageAlert($message){
    $_SESSION['message'] = $message;
    $_SESSION['time'] = time()+5;
}
?>