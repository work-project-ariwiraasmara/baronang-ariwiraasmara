<?php require('header.php');?>

    <div class="col-sm-12">
        <div class="box box-solid">
            <div class="box-body box-profile">
                <ul class="list-group list-group-unbordered">
                    <h4 class="text-bold" style="color: #0a3177">
                        <?php echo $lang->lang('Keamanan Akun', $conn); ?>
                    </h4>
                    <li class="list-group-item">
                        <a href="changepass.php">
                            <?php echo $lang->lang('Ubah Password', $conn); ?>
                        </a>
                    </li>
                    <li class="list-group-item">
                        <a href="changepin.php">
                            <?php echo $lang->lang('Ubah PIN', $conn); ?>
                        </a>
                    </li>
                    <h4 class="text-bold" style="color: #0a3177">
                        <?php echo $lang->lang('Tentang Baronang', $conn); ?>
                    </h4>
                    <li class="list-group-item">
                        <a href="#">
                            <?php echo $lang->lang('Profil', $conn); ?>
                        </a>
                    </li>
                    <li class="list-group-item">
                        <a href="#">
                            <?php echo $lang->lang('Saran', $conn); ?>
                        </a>
                    </li>
                    <li class="list-group-item">
                        <a href="kontak.php">
                            <?php echo $lang->lang('Kontak', $conn); ?>
                        </a>
                    </li>
                    <h4 class="text-bold" style="color: #0a3177">
                        <?php echo $lang->lang('Bahasa', $conn); ?>
                    </h4>
                    <li class="list-group-item">
                        <?php
                        $y = "select * from [dbo].[LanguageList]";
                        $n = sqlsrv_query($conns, $y);
                        while($m = sqlsrv_fetch_array($n, SQLSRV_FETCH_NUMERIC)){
                            $disabled = '';
                            if($_SESSION['Language'] == $m[0]){
                                $disabled = 'disabled';
                            }
                            ?>
                            <a href="setting.php?lang=<?php echo $m[0]; ?>" class="btn btn-xs btn-primary" <?php echo $disabled; ?>><?php echo $m[0]; ?></a>
                        <?php } ?>
                    </li>
                    <br>
                    <h6 class="text-black">
                        <?php echo $lang->lang('Versi', $conn); ?> 1.2.0
                    </h6>
                </ul>
            </div>
            <div class="box-footer">
                <button type="button" class="btn btn-block btn-primary" id="btn-logout">
                    <?php echo $lang->lang('Ganti akun', $conn); ?>
                </button>
            </div>
        </div>
    </div>

    <script type="text/javascript">
    $('#btn-logout').click(function(){
        Intent.openActivity('LogoutActivity','logout.php');
    });
    </script>
<?php require('footer.php');?>
