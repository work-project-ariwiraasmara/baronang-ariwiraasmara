<?php require('header-login.php');?>

<?php
if($_SESSION['UserID'] != ''){
    unset($_SESSION['url']);
    echo "<script language='javascript'>Intent.openActivity('FingerprintActivity','session.php');</script>";
    //echo "<script language='javascript'>document.location='session.php';</script>";
}
else{
?>

    <span style="text-align:center;position: absolute;top: 15%;">
        <img id="imgup" src ="images/logo_vertical.png" style="width: 40%;" align="middle">
    </span>

    <div class="footer2">
        <form action="proclogin.php" method="post" autocomplete="off">
            <div class="register-box-body" style="background: #0a3177;">
                <div class="form-group has-feedback text-center" style="color: white;">
                    <?php
                    if($_SESSION['message'] != '' and $_SESSION['time'] != ''){
                        echo $_SESSION['message'];
                    }
                    ?>
                </div>
                <div class="form-group has-feedback">
                    <div class="input-group">
                        <input type="email" name="email" id="email" class="form-control" value="<?php echo $_SESSION['Email']; ?>" placeholder="Email" style="background: transparent;color: white;">
                        <span class="input-group-btn" style="color: white;">
                            <div class="btn"><i class="fa fa-envelope-o"></i></div>
                        </span>
                    </div>
                </div>
                <div class="form-group has-feedback">
                    <div class="input-group">
                        <input type="password" name="password" id="password" class="form-control" placeholder="Password" style="background: transparent;color: white;">
                        <span class="input-group-btn" style="color: white;">
                            <div id="eye1" class="btn"><i class="fa fa-eye"></i></div>
                            <div id="eye2" class="btn hide"><i class="fa fa-eye-slash"></i></div>
                        </span>
                    </div>
                </div>
                <div class="form-group has-feedback">
                    <span class="pull-left"><a href="forgotpin.php" style="color: white;"><?php echo $lang->lang('Lupa PIN', $conn); ?> ?</a></span>
                    <span class="pull-right"><a href="forgot.php" style="color: white;"><?php echo $lang->lang('Lupa Password', $conn); ?> ?</a></span>
                    <br>
                    <br>
                </div>
                <button type="submit" class="btn btn-lg btn-info bn-flat btn-block btn-login" style="border-radius: 30px;"><?php echo $lang->lang('Masuk', $conn); ?></button>
                <br>
                <button type="button" id="btn-register" class="btn btn-lg btn-default btn-flat btn-block btn-login"  style="border-radius: 30px;"><?php echo $lang->lang('Daftar', $conn); ?></button>
            </div><!-- /.login-box-body -->
        </form>
    </div>

    <?php } ?>

    <script type="text/javascript">
        $('#btn-register').click(function(){
            Intent.openActivity('LoginActivity','register.php');
        });

        $( "#email" ).focus(function() {
            $("#imgup").hide();
        });

        $( "#password" ).focus(function() {
            $("#imgup").hide();
        });

        $( "#email" ).blur(function() {
            $("#imgup").show();
        });

        $( "#password" ).blur(function() {
            $("#imgup").show();
        });

        $('#eye1').click(function(){
            $("#password").prop('type','text');
            $('#eye1').hide();
            $('#eye2').removeClass('hide');
        });

        $('#eye2').click(function(){
            $("#password").prop('type','password');
            $('#eye2').addClass('hide');
            $('#eye1').show();
        });
    </script>

<?php require('footer-login.php');?>