<?php require('header.php');?>

<?php
if(!isset($_SESSION['KID'])){
    echo "<script language='javascript'>document.location='close.php';</script>";
}

$amount = 0;
if(isset($_GET['amount'])){
    $amount = $_GET['amount'];
}

if(!isset($_SESSION['acc'])){
    echo "<script language='javascript'>document.location='balance.php';</script>";
}
else{
    //cek tagihan
    $i = 0;
    $p = 0;
    $t = 0;
    $a = "exec dbo.Tagihan '$_SESSION[KID]','$_SESSION[MemberID]','$_SESSION[acc]'";
    $b = sqlsrv_query($conn, $a);
    while($c = sqlsrv_fetch_array( $b, SQLSRV_FETCH_NUMERIC)){
        //cek yang sudah bayar
        $ii = 0;
        $pp = 0;
        $aa = "exec dbo.LoanPaid '$_SESSION[KID]','$_SESSION[acc]','$c[0]'";
        $bb = sqlsrv_query($conn, $aa);
        $cc = sqlsrv_fetch_array( $bb, SQLSRV_FETCH_NUMERIC);
        if($cc != null){
            $ii=$cc[1];
            $pp=$cc[2];
        }

        $i+=$c[9]-$ii;
        $p+=$c[8]-$pp;
    }

    $t = $i+$p;

    $info = '';
    $disabled = '';
    $readonly = '';
    if($t <= 0){
        $disabled = 'disabled';
        $readonly = 'readonly';

        $info = 'Belum ada tagihan terbaru saat ini';
        echo "<script>System.showToast('Belum ada tagihan terbaru saat ini');</script>";
    }
}

$_SESSION['url'] = 'report.php?type=loan&id='.$_SESSION['acc'];

?>

    <div class="col-lg-12">
        <div class="box box-solid">
            <div class="box-header">
                <h3 class="box-title"><?php echo $lang->lang('Pembayaran Pinjaman', $conn); ?></h3>
            </div>
            <div class="box-body">
                <form id="dloan" action="procauth.php" method="POST" class="form-horizontal">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label class="col-sm-3" style="text-align: left;"><?php echo $lang->lang('Akun Pinjaman', $conn); ?></label>
                            <div class="col-sm-9">
                                <table class="table table-bordered table-striped">
                                    <?php
                                    $a = "exec dbo.LoanApplicationSearch '$_SESSION[KID]','$_SESSION[acc]'";
                                    $b = sqlsrv_query($conn, $a);
                                    $c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC);
                                    if($c != null){
                                        $aa = "exec dbo.LoanTypeSearch '$_SESSION[KID]','$c[4]'";
                                        $bb = sqlsrv_query($conn, $aa);
                                        $cc = sqlsrv_fetch_array($bb, SQLSRV_FETCH_NUMERIC);
                                        ?>
                                        <tr>
                                            <td>
                                                <input type="hidden" class="acc" name="acc" value="<?php echo $c[1]; ?>">
                                                <?php echo $c[1].' - '.$cc[1]; ?>
                                            </td>
                                        </tr>
                                    <?php
                                    }
                                    else{
                                        echo "<script language='javascript'>document.location='balance.php';</script>";
                                    }
                                    ?>
                                </table>
                                <span class="text-danger"><?php echo $info; ?></span>
                                <input type="hidden" name="kid" value="<?php echo $_SESSION['KID']; ?>" readonly>
                                <input type="hidden" name="mid" value="<?php echo $_SESSION['MemberID']; ?>" readonly>
                                <input type="hidden" name="jenis" value="12" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3" style="text-align: left;"><?php echo $lang->lang('Jumlah Bunga', $conn); ?></label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control price" id="interest" placeholder="" value="<?php echo $i; ?>" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3" style="text-align: left;"><?php echo $lang->lang('Jumlah Pokok', $conn); ?></label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control price" id="principal" placeholder="" value="<?php echo $p; ?>" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3" style="text-align: left;"><?php echo $lang->lang('Total', $conn); ?></label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control price" id="total" placeholder="" value="<?php echo $t; ?>" readonly>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group">
                            <label class="col-sm-3" style="text-align: left;"><?php echo $lang->lang('Jumlah Pembayaran', $conn); ?></label>
                            <div class="col-sm-9">
                                <input type="number" name="amount" id="amount" class="form-control price" placeholder="" value="<?php echo $amount; ?>" <?php echo $readonly; ?>>
                            </div>
                        </div>
                        <div class="box-footer">
                            <div class="row">
                                <button type="submit" class="btn btn-flat btn-block btn-success btn-save" <?php echo $disabled; ?>><?php echo $lang->lang('Bayar', $conn); ?></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $('.btn-save').click(function(){
            var total = $('#total').val();
            var amount = $('#amount').val();

            if(parseInt(amount) > parseInt(total)){
                System.showToast('Jumlah harus lebih kecil dari '+ total);
                return false;
            }
            else{
                $('#dloan').submit();
            }
        });

        $('.acc').click(function(){
            var acc = $("input[name='acc']:checked").val();
            $("#interest").val(0);
            $("#principal").val(0);
            $("#total").val(0);

            $.ajax({
                url : "ajax_gettagihan.php",
                type : 'POST',
                dataType: 'json',
                data: { acc: acc},
                success : function(data) {
                    if(data.status == 1){
                        $("#interest").val(data.interest);
                        $("#principal").val(data.principal);
                        $("#total").val(data.total);

                        if(data.total > 0){
                            $("#btn-save").prop('disabled', false);
                            $("#amount").prop('readonly', false);
                        }
                    }
                    else{
                        System.showToast(data.message);
                        return false;
                    }
                },
                error : function(){
                    System.showToast('Coba lagi');
                }
            });
        });
    </script>
<?php require('footer.php');?>