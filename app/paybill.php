<?php require('header.php');?>

<?php
if(!isset($_SESSION['KID'])){
    echo "<script language='javascript'>document.location='close.php';</script>";
}

if(!isset($_GET['acc']) or $_GET['acc'] == ''){
    echo "<script language='javascript'>document.location='balance.php';</script>";
}

?>

    <div class="col-lg-12">
        <div class="box box-solid">
            <div class="box-header">
                <h3 class="box-title"><?php echo $lang->lang('Bayar billing', $conn); ?></h3>
            </div>
            <div class="box-body">
                <form action="procauth.php" method="POST" class="form-horizontal">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <div class="col-sm-9">
							                  <input type="hidden" name="acc" value="<?php echo $_GET['acc']; ?>" readonly>
                                <input type="hidden" name="kid" value="<?php echo $_SESSION['KID']; ?>" readonly>
                                <input type="hidden" name="mid" value="<?php echo $_SESSION['MemberID']; ?>" readonly>
                                <input type="hidden" name="jenis" value="17" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3" style="text-align: left;"><?php echo $lang->lang('Pilih Akun Tabungan', $conn); ?></label>
                            <div class="col-sm-9">
                                <table class="table">
                                    <?php
                                    $a = "exec [dbo].[ListRegularSavingBal] '$_SESSION[KID]','$_SESSION[MemberID]'";
                                    $b = sqlsrv_query($conn, $a);
                                    while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                                    ?>
                                    <tr>
                                        <td>
                                            <input type="radio" name="regacc" class="minimal" id="regacc" value="<?php echo $c[2]; ?>">
                                            <b><?php echo $c[2]; ?></b> - <?php echo $c[4]; ?><span class="pull-right badge bg-aqua">Rp. <?php echo number_format($c[5]); ?></span>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </table>
                            </div>
                        </div>
                        <?php
                        $jumlah = 0;
                        $xx = "exec dbo.ListBillSearch '$_SESSION[KID]','$_GET[acc]'";
                        $yy = sqlsrv_query($conn, $xx);
                        $zz = sqlsrv_fetch_array($yy, SQLSRV_FETCH_NUMERIC);
                        if($zz != null){
                            $jumlah = $zz[6]; //amount+admin+denda
                        }
                        ?>
                        <div class="form-group">
                            <label class="col-sm-3" style="text-align: left;"><?php echo $lang->lang('Jumlah Tagihan', $conn); ?></label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control price" value="<?php echo number_format($jumlah); ?>" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3" style="text-align: left;"><?php echo $lang->lang('Jumlah Bayar', $conn); ?></label>
                            <div class="col-sm-9">
                                <input type="text" name="amount" class="form-control price" placeholder="" value="0">
                            </div>
                        </div>
                        <div class="box-footer">
                            <div class="row">
                                <button type="submit" class="btn btn-flat btn-block btn-success"><?php echo $lang->lang('Bayar', $conn); ?></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

<?php require('footer.php');?>
