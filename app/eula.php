<?php require('header-main.php');?>

<?php
$_SESSION['url'] = 'register.php';
?>

    <header class="main-header">
        <nav class="navbar navbar-static-top">
            <a href="#" class="pull-left" style="padding: 8px;">
                <span class="logo-mini"><img width="160" height="30" src ="images/Baronang Logo-03.png"></span>
            </a>
        </nav>
        <nav class="navbar main-footer" style="background: white;box-shadow:0 2px 0 0 rgba(0, 0, 0, 0.1);">
            <div class="row">
                <div class="col-sm-12 text-center" style="font-size: 1.5em;">
                    <a href="register.php" class="pull-left text-black">
                        <img src="static/dist/img/android-arrow-back.svg" width="30" height="30">
                    </a>
                    <?php echo $lang->lang('Syarat ketentuan', $conn); ?>
                </div>
            </div>
        </nav>
    </header>

    <div class="content-wrapper" style="background: white;">
    <br>
    <div class="col-sm-12">
    <p>
        Terima Kasih Anda telah mengunduh Baronang App. Baronang App adalah layanan aplikasi sebagaimana dimaksud dalam Syarat dan Ketentuan ini. Pada saat anda mengunduh Aplikasi, Anda mengakui dan menyetujui bahwa Anda telah membaca dengan teliti, memahami, menerima dan menyetujui seluruh Syarat dan Ketentuan yang akan berlaku sebagai perjanjian antara Anda dengan PT Baronang Asia Pasifik, beserta rekanan PT Baronang Asia Pasifik yang tergabung dalam Layanan Aplikasi ini.
        <br>
        <br>
        <b><u>MOHON ANDA MEMBACA DAN MEMAHAMI SYARAT DAN KETENTUAN KAMI DENGAN SEKSAMA SEBELUM MENGUNDUH APLIKASI ATAU MENGGUNAKAN LAYANAN KAMI UNTUK PERTAMA KALI-NYA.</u></b>
        <br>
        Layanan yang tersedia untuk Anda pada Baronang App sangat-lah beragam, sehingga kami harus mencantumkan atau memberlakukan Syarat dan Ketentuan tambahan untuk keperluan Layanan tertentu, dimana persyaratan tambahan tersebut merupakan bagian dari Layanan kami dan akan tunduk dan terikat dengan Syarat dan Ketentuan ini.
        Kami berhak untuk setiap waktu mengubah, menambah, mengurangi, mengganti, menyesuaikan, dan/atau memodifikasi Syarat dan Ketentuan (baik sebagian ataupun seluruhnya). Untuk itu Anda diwajibkan untuk dari waktu ke waktu membaca Syarat dan Ketentuan yang kami sediakan dalam website tersebut diatas atau melalui Aplikasi.
        <br>
        Syarat dan Ketentuan ini berlaku terhadap Anda maupun atas Transaksi atau atas penggunaan Layanan oleh atau melalui Akun Anda dimanapun Anda berada, baik di wilayah Republik Indonesia maupun di wilayah Negara lainnya.
        <br>
        <br>
    </p>

    <ul style="list-style-type: circle">
        <li><a href="#definisi">Definisi</a></li>
        <li><a href="#panduan">Panduan Singkat</a></li>
        <li><a href="#kewajiban">Kewajiban Pernyataan Dan Jaminan</a></li>
        <li><a href="#batas">Pembatasan Tanggung Jawab</a></li>
        <li><a href="#hak">Hak Kekayaan Intelektual</a></li>
        <li><a href="#masa">Masa Berlaku Dan Pengakhiran</a></li>
        <li><a href="#hukum">Hukum Yang Berlaku Dan Penyelesaian Perselisihan</a></li>
        <li><a href="#ketentuan">Ketentuan Lain</a></li>
    </ul>

    <section id="definisi">
        <h3>DEFINISI</h3>
        <ul style="list-style-type:disc">
            <li>“Baronang App” adalah aplikasi ini yang Anda unduh (download) sehingga Anda masuk pada Laman ini atau Syarat dan Ketentuan ini, yang saat ini dikenal dengan merek, nama, logo dan/atau tanda yang dikenal dengan “BARONANG” atau merek, nama, logo dan/atau tanda lainnya.</li>
            <li>“Akun” atau “Akun Anda” berarti identifikasi khusus yang dibuat di BARONANG berdasarkan permintaan pendaftaran Anda.</li>
            <li>“Data” berarti adalah setiap data, informasi dan/atau keterangan dalam bentuk apapun yang dari waktu ke waktu (termasuk pada saat Anda mengunduh (download) Aplikasi) Anda sampaikan kepada Kami/Penyedia Layanan atau yang Anda cantumkan atau sampaikan dalam, pada atau melalui Aplikasi.</li>
            <li>“Kami” adalah PT Baronang Asia Pasifik.</li>
            <li>“Layanan” berarti setiap layanan, program, jasa, produk, fitur, sistem, fasilitas dan/atau jasa yang disediakan dan/atau ditawarkan dalam atau melalui Aplikasi.</li>
            <li>“Layanan Pelanggan (Call Center BARONANG)” adalah fungsi customer service center untuk nasabah yang dapat dihubungi lewat panggilan telepon dan/atau email.</li>
            <li>“BARONANG” adalah sistem elektronik (platform) yang dibuat oleh PT Baronang Asia Pasifik.</li>
            <li>“Syarat dan Ketentuan” berarti Syarat dan Ketentuan ini berikut setiap perubahan, penambahan, penggantian, penyesuaian dan/atau modifikasinya yang dibuat dari waktu ke waktu.</li>
            <li>“Transaksi” berarti segala transaksi, kegiatan, aktivitas dan/atau aksi yang dilakukan dalam atau melalui Aplikasi, Akun dan/atau Security Code termasuk penggunaan Layanan atau fitur-fitur tertentu dalam Layanan atau Aplikasi.</li>
            <li>“BAP” adalah PT Baronang Asia Pasifik, suatu perseroan terbatas yang didirikan berdasarkan hukum Negara Republik Indonesia.</li>
        </ul>
    </section>

    <section id="panduan">
        <h3>PANDUAN SINGKAT</h3>
        <ul style="list-style-type: disc;">
            <li>Baronang App merupakan aplikasi perangkat lunak dimana seluruh instruksi yang Anda lakukan akan berasal dari mobile aplikasi atau online.</li>
            <li>Baronang App dapat dioperasikan melalui HP Android (OS 4.4.0 ke atas atau Android Kitkat) melalui Google Play Store.</li>
            <li>Anda akan diminta untuk melakukan otorisasi instruksi dengan menggunakan berbagai jenis informasi keamanan yang berbeda (misalnya Security Code, nama pengguna, password) jika dibutuhkan.</li>
            <li>Setelah melakukan otorisasi, BARONANG akan melaksanakan instruksi sesuai perintah Anda, Anda diwajibkan untuk memastikan tidak memberitahukan informasi keamanan Anda kepada pihak lain.</li>
            <li>BARONANG memastikan kerahasiaan dan keamanan informasi diri yang Anda berikan terjaga dengan baik. Penggunaan data Anda akan kami lakukan sesuai dengan ketentuan yang berlaku.</li>
            <li>Anda dapat menanyakan atau memberi masukkan kepada kami melalui Contact Center Layanan Pengguna BARONANG pada alamat email sebagai berikut [info@Baronang.id].</li>
        </ul>

        Baronang App menawarkan 2 (dua) jenis klasifikasi pelanggan dengan jenis layanan BARONANG atau fitur-fitur layanan yang berbeda. Klasifikasi pelanggan tersebut adalah:
        <br>
        <b>A. Baronang App</b>
        <br>
        Baronang App adalah aplikasi untuk nasabah koperasi yang memungkinkan Anda dapat menikmati fasilitas yang disiapkan.
        <br>
        <b>B. BARONANG Web</b>
        <br>
        Baronang Web adalah aplikasi untuk management koperasi untuk mempermudah penggunaan sistem koperasi.
    </section>

    <section id="kewajiban">
        <h3>KEWAJIBAN PERNYATAAN DAN JAMINAN</h3>
        <ul style="list-style-type: disc">
            <li>Anda hanya dapat mengakses atau menggunakan Aplikasi, Layanan, dan/atau Sistem (a) sesuai dengan Syarat dan Ketentuan ini, (b) untuk tujuan yang sah, dan (c) tidak digunakan untuk tujuan atau tindakan penipuan, pelanggaran hukum, kriminal maupun tindakan, aktifitas, perbuatan atau tujuan lain yang melanggar atau bertentangan dengan hukum, peraturan perundang-undangan yang berlaku maupun hak atau kepentingan pihak manapun. Anda bertanggung jawab penuh untuk memeriksa dan memastikan bahwa Anda telah mengunduh (download) software yang benar untuk perangkat Anda. Kami tidak bertanggung jawab jika Anda tidak memiliki perangkat yang kompatibel dengan Sistem atau Aplikasi atau jika Anda telah mengunduh (download) versi software yang salah untuk perangkat Anda.</li>
            <li>Anda dilarang untuk menggunakan Layanan, Aplikasi dan/atau Akun atau melakukan Transaksi: (a) untuk tujuan, kegiatan, aktifitas atau aksi yang melanggar hukum atau melanggar hak atau kepentingan (termasuk Hak Kekayaan Intelektual atau hak privasi milik pihak manapun); (b) yang memiliki materi atau unsur yang berbahaya atau yang merugikan pihak manapun; (c) yang mengandung virus software, worm, trojan horses atau kode komputer berbahaya lainnya, file, script, agen atau program; dan (d) yang mengganggu integritas atau kinerja Sistem dan/atau Aplikasi.</li>
            <li>Anda dilarang untuk melakukan tindakan apapun termasuk dalam atau melalui Aplikasi atau Akun serta dilarang untuk melakukan Transaksi yang dapat merusak atau mengganggu reputasi Kami atau Penyedia Layanan.</li>
            <li>
                Anda dengan ini secara tegas menyetujui serta menyatakan dan menjamin bahwa:
                <ul style="list-style-type: decimal">
                    <li>Anda adalah individu yang secara hukum cakap untuk melakukan tindakan hukum berdasarkan hukum negara Republik Indonesia termasuk untuk mengikatkan diri dalam Syarat dan Ketentuan ini. Jika Anda tidak memenuhi syarat ini, Kami berhak untuk sewaktu-waktu untuk men-nonaktifkan atau mengakhiri penggunaan Akun, Layanan dan/atau Aplikasi, baik untuk sementara waktu maupun untuk seterusnya;</li>
                    <li>Anda memiliki hak, wewenang dan kapasitas untuk menggunakan Layanan dan Akun serta untuk melaksanakan seluruh Syarat dan Ketentuan;</li>
                    <li>Jika Anda melakukan pendaftaran atau mengunduh Aplikasi atas nama suatu badan hukum, persekutuan perdata atau pihak lain, Anda dengan ini menyatakan dan menjamin bahwa Anda memiliki kapasitas, hak dan wewenang yang sah untuk bertindak untuk dan atas nama badan hukum, persekutuan perdata atau pihak lain tersebut termasuk tetapi tidak terbatas pada mengikat badan hukum, persekutuan perdata atau pihak lain tersebut untuk tunduk pada seluruh isi Syarat dan Ketentuan;</li>
                    <li>Anda telah membaca seluruh isi Syarat dan Ketentuan serta telah mengerti dan memahami seluruh isi Syarat dan Ketentuan ini dan karenanya mengikatkan diri secara sukarela untuk tunduk dengan serta melaksanakan seluruh Syarat dan Ketentuan ini;</li>
                    <li>Anda menyatakan dan menjamin bahwa dana yang dipergunakan dalam rangka transaksi bukan dana yang berasal dari tindak pidana yang dilarang berdasarkan peraturan perundang-undangan yang berlaku di Republik Indonesia, pembukaan rekening ini tidak dimaksudkan dan/atau ditujukan dalam rangka upaya melakukan tidak pidana pencucian uang sesuai dengan ketentuan peraturan perundang-undangan yang berlaku di Republik Indonesia, transaksi tidak dilakukan untuk maksud mengelabui, mengaburkan, atau menghindari pelaporan kepada Pusat Pelaporan Dan Analisa Transaksi Keuangan (PPATK) berdasarkan ketentuan peraturan perundang-undangan yang berlaku di Republik Indonesia, dan Anda bertanggung jawab sepenuhnya serta melepaskan BAP dari segala tuntutan, klaim, atau ganti rugi dalam bentuk apapun, apabila Anda melakukan tindak pidana pencucian uang berdasarkan ketentuan peraturan perundang-undangan yang berlaku di Republik Indonesia;</li>
                    <li>Seluruh Data baik yang telah Anda sampaikan atau cantumkan maupun yang akan Anda sampaikan atau cantumkan baik langsung maupun tidak langsung di kemudian hari atau dari waktu ke waktu adalah benar, lengkap, akurat terkini dan tidak menyesatkan serta tidak melanggar hak (termasuk tetapi tidak terbatas pada hak kekayaan intelektual) atau kepentingan pihak manapun. Penyampaian Data oleh Anda kepada Kami atau pada atau melalui Aplikasi atau Sistem tidak bertentangan dengan hukum yang berlaku serta tidak melanggar akta, perjanjian, kontrak, kesepakatan atau dokumen lain dimana Anda merupakan pihak atau dimana Anda atau aset Anda terikat;</li>
                    <li>Layanan, Aplikasi maupun Akun akan digunakan untuk kepentingan Anda sendiri atau untuk kepentingan badan hukum, badan usaha, persekutuan perdata atau pihak lain yang secara sah Anda wakili sebagaimana dimaksud nomor 3 di atas;</li>
                    <li>Anda tidak akan memberikan hak, wewenang dan/atau kuasa dalam bentuk apapun dan dalam kondisi apapun kepada orang atau pihak lain untuk menggunakan Data, Akun dan/atau Security Code, dan Anda karena alasan apapun dan dalam kondisi apapun tidak akan dan dilarang untuk mengalihkan Akun kepada orang atau pihak manapun dan dalam atau pada saat menggunakan Layanan, Aplikasi dan/atau Akun, Anda setuju untuk mematuhi dan melaksanakan seluruh ketentuan hukum dan peraturan perundang-undangan yang berlaku termasuk hukum dan peraturan perundang-undangan di negara asal Anda maupun di negara atau kota dimana Anda berada.</li>
                </ul>
            </li>
            <li>Dengan melaksanakan transaksi melalui Aplikasi Mobile BARONANG, Anda memahami bahwa seluruh komunikasi dan instruksi dari Anda yang diterima oleh Kami akan diperlakukan sebagai bukti solid meskipun tidak dibuat dalam bentuk dokumen tertulis atau diterbitkan dalam bentuk dokumen yang ditandatangani, dan, dengan demikian, Anda setuju untuk mengganti rugi dan melepaskan Kami dan rekanan-rekanan Kami dari segala kerugian, tanggung jawab, tuntutan dan pengeluaran (termasuk biaya litigasi) yang dapat muncul terkait dengan eksekusi dari instruksi Anda.</li>
        </ul>
    </section>

    <section id="batas">
        <h3>PEMBATASAN TANGGUNG JAWAB</h3>
        <ul style="list-style-type: decimal">
            <li>
                Layanan disediakan dalam kondisi “as is” atau “apa adanya”. Kami (dalam beberapa fitur) tidak memberikan pernyataan atau jaminan dalam bentuk apapun atas reliabilitas, keamanan, ketepatan waktu, kualitas, kesesuaian, ketersediaan, akurasi dan/atau kelengkapan Layanan, Aplikasi dan/atau Sistem. Kami tidak memberikan pernyataan atau jaminan dalam bentuk apapun bahwa:
                <ul style="list-style-type: lower-alpha">
                    <li>Penggunaan Layanan, Aplikasi, Akun dan/atau Sistem (atau bagian daripadanya) akan aman, tepat waktu, tidak terganggu atau bebas dari kesalahan, gangguan, virus atau hambatan lain atau komponen berbahaya lainnya;</li>
                    <li>Layanan, Aplikasi, Akun dan/atau Sistem dapat tetap beroperasi atau digunakan bersamaan dengan atau dengan kombinasi perangkat (baik hardware maupun software) atau sistem pihak lain yang tidak kami sediakan atau miliki untuk pengoperasian Aplikasi;</li>
                    <li>Penyediaan Layanan maupun Aplikasi atau kelancaran ber-Transaksi atau penggunaan Akun akan memenuhi persyaratan atau harapan Anda;</li>
                    <li>Setiap data Transaksi yang Kami simpan atau tersimpan dalam Sistem harus akurat atau terpercaya;</li>
                    <li>Kualitas Layanan atau Aplikasi akan memenuhi persyaratan atau harapan Anda dan tidak akan ada kesalahan, gangguan atau cacat dari Sistem, Layanan dan/atau Aplikasi.</li>
                </ul>
            </li>
            <li>
                Anda mengetahui dan setuju bahwa Kami berhak untuk memblokir dan/atau menutup Akun dan rekening dan/atau layanan/fasilitas apabila:
                <ul style="list-style-type: lower-alpha">
                    <li>Kami memahami dan memiliki alasan yang memadai untuk menyatakan bahwa telah terjadi atau akan terjadi manipulasi keuangan atau perbankan atau kriminal yang terkait dengan Akun atau Rekening dan/atau layanan/fasilitas Pengguna BARONANG;</li>
                    <li>Pemegang atau Nasabah memberikan data yang tidak valid/tidak lengkap kepada Kami;</li>
                    <li>Terdapat permintaan tertulis dari instansi Kepolisian, Kejaksaan, Pengadilan, Pusat Pelaporan dan Analisis Transaksi Keuangan (PPATK), Kantor Pajak atau lembaga berwenang lainnya sesuai dengan hukum dan perundangan yang berlaku atau untuk memenuhi kewajiban/utang yang belum diselesaikan oleh Pemegang atau Nasabah.</li>
                </ul>
            </li>
            <li>
                Anda dengan ini mengetahui bahwa Layanan, Aplikasi, Akun dan/atau Sistem mungkin atau dapat mengalami, terdapat atau terjadi pembatasan, keterlambatan, dan/atau masalah lain termasuk yang disebabkan karena atau sehubungan dengan
                <ul style="list-style-type: lower-alpha">
                    <li>Ketidaktersediaan atau terbatasnya jaringan (termasuk jaringan internet) dan/atau penggunaan atau</li>
                    <li>Tidak tersedianya, terganggunya, atau tidak berfungsinya fitur tertentu pada perangkat yang Anda gunakan. Kami tidak bertanggung jawab atas segala keterlambatan, terhambatnya, tidak suksesnya, terganggunya atau gagalnya suatu Transaksi yang disebabkan karena hal tersebut di atas.</li>
                </ul>
            </li>
            <li>
                Anda dengan ini mengetahui bahwa terdapat kemungkinan
                <ul style="list-style-type: lower-alpha">
                    <li>Sistem atau Aplikasi (atau bagian manapun dari Sistem atau Aplikasi) tidak stabil, terganggu, terhenti, tidak berjalan dengan baik, tidak berjalan dengan sempurna dan/atau memiliki beberapa bug, dan/atau</li>
                    <li>Layanan (atau fitur-futur atau bagian-bagian tertentu) dapat berubah, tidak tersedia, dan atas terjadinya hal tersebut Anda setuju untuk tidak mengajukan Klaim kepada Kami.</li>
                </ul>
            </li>
            <li>
                Kami dalam kondisi apapun tidak bertanggung jawab atas segala Klaim dari pihak manapun termasuk Anda maupun atas kerugian Anda serta pihak manapun yang terjadi sebagai akibat dari atau sehubungan dengan:
                <ul style="list-style-type: lower-alpha">
                    <li>Kehilangan Data;</li>
                    <li>Kehilangan pendapatan, keuntungan atau pemasukan lainnya;</li>
                    <li>Kehilangan, kerusakan atau cedera yang timbul dari, atau sehubungan dengan penggunaan Anda atas Aplikasi, Layanan dan/atau Akun atau atas ketidakmampuan atau kesalahan Anda dalam menggunakan Layanan, Aplikasi dan/atau Akun; atau</li>
                    <li>Tuntutan maupun gugatan yang dialami oleh Anda yang mungkin timbul sebagai akibat penyampaian informasi dari Anda yang tidak lengkap atau akibat tidak dilaksanakannya instruksi Anda, antara lain pembatalan, perubahan instruksi (untuk instruksi yang belum dijalankan) yang disampaikan kepada BAP, kecuali jika kerugian tersebut terjadi akibat kesalahan yang disengaja atau kelalaian oleh BAP</li>
                </ul>
            </li>
            <li>
                Anda dengan ini setuju dan mengikatkan diri untuk membebaskan Kami dari segala Klaim dalam bentuk apapun dan dimanapun dalam hal Kami tidak dapat melaksanakan, melanjutkan atau meneruskan Transaksi maupun perintah atau instruksi dari Anda melalui Aplikasi atau Akun, baik sebagian maupun seluruhnya, yang disebabkan karena kejadian-kejadian atau hal-hal di luar kekuasaan atau kemampuan Kami termasuk namun tidak terbatas pada
                <ul style="list-style-type: lower-alpha">
                    <li>Segala gangguan virus komputer,</li>
                    <li>Sistem trojan horses,</li>
                    <li>Komponen atau sistem yang dapat membahayakan serta mengganggu Aplikasi, Layanan dan/atau Akun,</li>
                    <li>Layanan atau jasa Internet Service Provider atau jasa atau layanan pihak ketiga lainnya yang tersedia dalam Layanan, dan/atau</li>
                    <li>Bencana alam, perang, huru-hara, keadaan peralatan, sistem atau transmisi yang tidak berfungsi, gangguan listrik, gangguan telekomunikasi, kebijakan pemerintah, kegagalan sistem perbankan dan/atau keuangan serta kejadian-kejadian atau sebab-sebab lain diluar kekuasaan atau kemampuan Kami.</li>
                </ul>
            </li>
            <li>
                Anda dengan ini setuju dan mengikatkan diri untuk membebaskan Kami dari setiap dan seluruh Klaim dalam bentuk apapun, dari pihak manapun dan dimanapun yang diajukan, timbul atau terjadi sehubungan dengan atau sebagai akibat dari:
                <ul style="list-style-type: lower-alpha">
                    <li>Penggunaan Data oleh Kami berdasarkan Syarat dan Ketentuan ini atau berdasarkan persetujuan, pengakuan, wewenang, kuasa dan/atau hak yang Anda berikan baik secara langsung maupun tidak langsung kepada Kami dalam Syarat dan Ketentuan ini;</li>
                    <li>
                        Pemberian Data baik secara langsung maupun tidak langsung oleh Anda kepada Kami atau dalam atau melalui Aplikasi ini yang Anda lakukan secara
                        <ul style="list-style-type: lower-roman">
                            <li>melanggar atau melawan hukum atau peraturan perundang-undangan yang berlaku,</li>
                            <li>melanggar hak (termasuk hak kekayaan intelektual) dari atau milik orang atau pihak manapun, atau</li>
                            <li>melanggar kontrak, kerjasama, kesepakatan, akta, pernyataan, penetapan, keputusan dan/atau dokumen apapun dimana Anda merupakan pihak atau dimana Anda atau aset Anda terikat;</li>
                        </ul>
                    </li>
                    <li>
                        Penggunaan Aplikasi, Akun dan/atau Layanan
                        <ul style="list-style-type: lower-roman">
                            <li>secara tidak sah,</li>
                            <li>melanggar hukum yang berlaku,</li>
                            <li>melanggar Syarat dan Ketentuan ini, dan/atau</li>
                            <li>untuk tindakan atau tujuan penipuan, kriminal, tindakan tidak sah atau tindakan pelanggaran hukum lainnya.</li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li>Anda dengan ini setuju dan mengikatkan diri untuk mengganti seluruh kerugian yang Kami alami dan mengganti seluruh biaya, ongkos, beban dan pengeluaran yang telah atau mungkin akan Kami keluarkan atau bayarkan sehubungan dengan atau sebagai akibat dari Klaim sebagaimana dimaksud butir 6, 8 dan 9 di atas (termasuk tetapi tidak terbatas pada biaya jasa hukum yang kami bayarkan atau keluarkan untuk melakukan pembelaan atau tindakan lain yang diperlukan terkait dengan Klaim tersebut).</li>
            <li>Anda dengan ini setuju bahwa Kami akan dibebaskan dari segala tuntutan, jika Kami tidak dapat melaksanakan instruksi dari Anda, baik sebagian maupun sepenuhnya yang disebabkan oleh kejadian atau sebab yang berada di luar kendali atau kemampuan Kami, meliputi tetapi tidak terbatas pada bencana alam, peperangan, kerusuhan, kondisi perangkat keras, kegagalan sistem infrastruktur elektronik atau transmisi, gangguan daya, gangguan telekomunikasi, kegagalan sistem kliring atau hal lainnya yang ditetapkan oleh Bank Indonesia atau lembaga berwenang lainnya.</li>
            <li>Setelah kejadian yang menyebabkan BAP dan/atau rekanannya tidak dapat melaksanakan instruksi dari Pemegang atau Nasabah berakhir, maka BAP dan/atau rekanannya akan melanjutkan kembali instruksi tersebut dalam kurun waktu sesuai dengan ketentuan dari Bank Indonesia dan/atau Otoritas Jasa Keuangan.</li>
        </ul>
    </section>

    <section id="hak">
        <h3>HAK KEKAYAAN INTELEKTUAL</h3>
        <ul style="list-style-type: decimal">
            <li>
                Seluruh Sistem dan Aplikasi termasuk tetapi tidak terbatas pada seluruh:
                <ul style="list-style-type: lower-alpha">
                    <li>layout, desain dan tampilan Aplikasi yang terdapat dalam atau ditampilkan pada media Aplikasi Anda;</li>
                    <li>logo, foto, gambar, nama, merek, kata, huruf-huruf, angka-angka, tulisan, dan susunan warna yang terdapat dalam Aplikasi; dan</li>
                    <li>kombinasi dari unsur-unsur sebagaimana dimaksud dalam huruf (a) dan (b), sepenuhnya merupakan Hak Kekayaan Intelektual milik Kami dan tidak ada pihak lain yang turut memiliki hak atas Aplikasi maupun atas layout, desain dan tampilan Aplikasi.</li>
                </ul>
            </li>
            <li>Anda dilarang untuk dengan cara apapun dan dalam kondisi apapun menggunakan Hak Kekayaan Intelektual milik kami sebagaimana dimaksud dalam butir 1 di atas, tanpa persetujuan tertulis terlebih dahulu dari Kami.</li>
        </ul>
    </section>

    <section id="masa">
        <h3>MASA BERLAKU DAN PENGAKHIRAN</h3>
        <ul style="list-style-type: decimal">
            <li>Perjanjian ini berlaku selama Anda belum melakukan penutupan Akun dan uang elektronik pada BARONANG Premier Anda atau Layanan BARONANG.</li>
            <li>
                Kami berhak untuk
                <ul style="list-style-type: upper-roman">
                    <li>tidak melaksanakan perintah dari Anda,</li>
                    <li>tidak melanjutkan atau tidak meneruskan atau membatasi Transaksi,</li>
                    <li>menghentikan, menangguhkan atau men-nonaktifkan penggunaan Akun, Layanan atau Aplikasi, dan/atau</li>
                    <li>
                        menghentikan atau menangguhkan keanggotaan Anda sebagai pengguna Aplikasi, dimana untuk masing-masing kondisi tersebut di atas baik untuk sementara waktu, untuk jangka waktu tertentu atau untuk seterusnya berdasarkan pertimbangan atau keputusan Kami yang Kami anggap baik, apabila terjadinya satu atau lebih kondisi di bawah ini:
                        <ul style="list-style-type: lower-alpha">
                            <li>berdasarkan penilaian, keputusan dan/atau pertimbangan Kami sendiri, Kami mengetahui atau menduga telah atau akan terjadi atau dilakukan penggunaan Akun, Security Code, Aplikasi dan/atau Layanan untuk tujuan penipuan, tujuan kriminal, aksi kejahatan dan/atau untuk tujuan, alasan, aksi atau aktifitas lainnya yang melanggar hukum atau peraturan perundang-undangan negara Republik Indonesia maupun negara lain;</li>
                            <li>
                                berdasarkan penilaian, keputusan dan/atau pertimbangan Kami sendiri
                                <ul style="list-style-type: lower-roman">
                                    <li>Kami mengetahui atau menduga terjadi Transaksi yang tidak wajar, dan/atau</li>
                                    <li>jumlah Transaksi dengan menggunakan atau melalui Akun dan/atau Security Code dalam satu hari melebihi jumlah atau tingkat kewajaran Transaksi, masing-masing sebagaimana ditentukan oleh Kami;</li>
                                    <li>berdasarkan penilaian, keputusan dan/atau pertimbangan Kami sendiri, Anda telah melanggar satu atau lebih Syarat dan Ketentuan;</li>
                                    <li>sebagian atau seluruh Data tidak benar, tidak lengkap, palsu, fiktif dan/atau menyesatkan;</li>
                                    <li>alamat email atau telepon yang Anda cantumkan atau gunakan untuk penggunaan Aplikasi atau Layanan atau untuk melakukan Transaksi telah terblokir;</li>
                                    <li>Anda belum melengkapi proses atau persyaratan sebagaimana diatur dalam ketentuan hukum dan/atau peraturan perundang-undangan yang berlaku;</li>
                                    <li>Kami sedang melakukan pembaharuan, pemeliharaan, perubahan, up-grade, penyesuaian, penggantian dan/atau tindakan lain atas Aplikasi, Layanan dan/atau Sistem (atau bagian manapun daripadanya) dan untuk itu Kami tidak berkewajiban mempertanggungjawabkannya kepada siapapun; dan/atau</li>
                                    <li>Anda meninggal dunia atau perusahaan, badan usaha, badan hukum atau persekutuan perdata yang Anda wakili dinyatakan atau dimohonkan pailit atau likuidasi atau berada dalam kondisi pailit atau likuidasi.</li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li>Anda dengan ini sepakat untuk mengenyampingkan ketentuan Pasal 1266 ayat (2) dan (3) Kitab Undang-undang Hukum Perdata sehingga Syarat dan Ketentuan, Aplikasi dan/atau Layanan dapat diakhiri (baik sebagian maupun seluruhnya, baik sementara waktu maupun seterusnya) sesuai dengan Syarat dan Ketentuan tanpa diperlukan adanya keputusan atau penetapan dari hakim pengadilan.</li>
            <li>
                Jika Akun atau Rekening telah berakhir atau ditutup atau jika Layanan atau Aplikasi sudah berakhir atau diakhiri oleh Kami:
                <ul style="list-style-type: lower-alpha">
                    <li>Jika pengakhiran Akun, Layanan atau Aplikasi disebabkan karena pelanggaran yang Anda lakukan atas satu atau lebih Syarat dan Ketentuan, maka sisa saldo pada Akun seluruhnya akan menjadi hak BAP dan Anda setuju dan mengikatkan untuk tidak meminta pengembalian, pembayaran, penggantian dan/atau kompensasi dalam bentuk apapun kepada BAP.</li>
                </ul>
            </li>
            <li>Anda sepakat dan mengikatkan diri untuk tidak melakukan tindakan apapun yang dapat membatasi, menghambat dan/atau mengurangi satu atau lebih hak dan/atau wewenang Kami berdasarkan Syarat dan Ketentuan ini maupun hukum yang berlaku.</li>
        </ul>
    </section>

    <section id="hukum">
        <h3>HUKUM YANG BERLAKU DAN PENYELESAIAN PERSELISIHAN</h3>
        <ul style="list-style-type: decimal">
            <li>Syarat dan Ketentuan ini diatur dan ditafsirkan berdasarkan hukum Negara Republik Indonesia.</li>
            <li>
                Segala perselisihan atau pertentangan yang timbul sehubungan dengan atau terkait dengan hal-hal yang diatur dalam Syarat dan Ketentuan (maupun bagian daripadanya) termasuk perselisihan yang disebabkan karena adanya atau dilakukannya perbuatan melawan hukum atau pelanggaran atas satu atau lebih Syarat dan Ketentuan ini (“Perselisihan”) wajib diselesaikan dengan cara sebagai berikut:
                <ul style="list-style-type: lower-alpha">
                    <li>Salah satu pihak baik Anda atau Kami (“Pihak Pertama”) wajib menyampaikan pemberitahuan tertulis kepada pihak lainnya (“Pihak Kedua”) atas telah terjadinya Perselisihan (“Pemberitahuan Perselisihan”). Perselisihan wajib diselesaikan secara musyawarah mufakat dalam waktu paling lambat 90 (sembilan puluh) hari kalender sejak tanggal Pemberitahuan Perselisihan Perselisihan (“Periode Penyelesaian Musyawarah”);</li>
                    <li>jika Perselisihan tidak dapat diselesaikan secara musyawarah mufakat sampai dengan berakhirnya Periode Penyelesaian Musyawarah, Pihak Pertama dan Pihak Kedua wajib untuk bersama-sama menunjuk pihak ketiga (“Mediator”) sebagai mediator untuk menyelesaikan Perselisihan dan penunjukan tersebut wajib dituangkan dalam bentuk tertulis yang ditandatangani bersama oleh Pihak Pertama dan Pihak Kedua.</li>
                </ul>
            </li>
            <li>Proses mediasi oleh Mediator khusus untuk sengketa Para Pihak dibidang Perbankan, diselesaikan penyelesaiannya oleh Para Pihak kepada LAPSPI yaitu Lembaga Alternatif Penyelesaian Sengketa Perbankan Indonesia sebagaimana diatur dalam Peraturan Otoritas Jasa Keuangan Nomor 1/POJK.07/2014 tentang Perlindungan Konsumen Sektor Jasa Keuangan, yang diundangkan tanggal 23 Januari 2014 (Lembaran Negara Republik Indonesia Tahun 2014 Nomor 12, Tambahan Lembaran Negara Republik Indonesia Nomor 5499) beserta perubahannya apabila ada.</li>
            <li>Anda wajib untuk menanggung seluruh biaya, ongkos dan pengeluaran yang telah atau mungkin Kami keluarkan atau bayarkan dalam rangka penyelesaian Perselisihan (termasuk tetapi tidak terbatas pada biaya, ongkos dan pengeluaran untuk menghadiri sidang arbitrase serta untuk mempersiapkan dan mengajukan segala pembelaan, gugatan, tuntutan, informasi, bukti, keterangan dan/atau dokumen apapun dalam rangka pelaksanaan atau selama proses sidang arbitrase) sampai dengan adanya putusan arbitrase yang final dan mengikat;</li>
            <li>Kecuali disyaratkan berdasarkan hukum yang berlaku atau diminta berdasarkan permintaan, keputusan atau penetapan resmi yang diterbitkan, dikeluarkan atau dibuat oleh pengadilan atau instansi pemerintah yang berwenang, selama proses penyelesaian Perselisihan sebagaimana diatur di atas sampai dengan adanya keputusan yang sah, final dan mengikat Pihak Pertama dan Pihak Kedua, maka Pihak Pertama dan Pihak Kedua wajib untuk merahasiakan segala informasi terkait dengan Perselisihan maupun proses penyelesaiannya dan karenanya dilarang untuk dengan cara apapun menginformasikan, memberitahukan atau mengumumkan kepada pihak manapun adanya Perselisihan tersebut maupun proses penyelesaiannya termasuk tetapi tidak terbatas melalui media massa (koran, televisi atau media lainnya) dan/atau media sosial. Jika Anda melanggar ketentuan butir nomor 5 ini, Anda dengan ini mengetahui dan setuju bahwa seluruh atau sebagian hak Anda untuk menggunakan Layanan, Aplikasi, Akun dan/atau PIN dapat sewaktu-waktu diakhiri atau dinon-aktifkan oleh Kami baik untuk sementara waktu maupun untuk seterusnya.</li>
        </ul>
    </section>

    <section id="ketentuan">
        <h3>KETENTUAN LAIN</h3>
        <ul style="list-style-type: decimal">
            <li>Untuk setiap masalah yang berkaitan dengan Transaksi, Layanan dan/atau Akun, Anda dapat meminta bantuan pada atau dari Contact Center atau Layanan Pengguna BARONANG Kami pada alamat email sebagai berikut [info@Baronang.id] dengan wajib menyertakan bukti pendukung yang dapat Kami terima untuk penyampaian pengaduan tertulis tersebut.</li>
            <li>Seluruh persetujuan, kuasa, wewenang dan/atau hak yang Anda berikan kepada Kami dalam Syarat dan Ketentuan ini tidak dapat berakhir karena alasan apapun termasuk karena alasan-alasan sebagaimana dimaksud dalam Pasal 1813, 1814, dan 1816 Kitab Undang-Undang Hukum Perdata selama Anda masih menggunakan Layanan atau Aplikasi atau masih menggunakan dan/atau memiliki Akun.</li>
            <li>Anda dengan ini membebaskan Kami dari seluruh Klaim sehubungan dengan pelaksanaan segala tindakan Kami berdasarkan kuasa, wewenang dan/atau hak yang Anda berikan kepada Kami dalam atau berdasarkan Syarat dan Ketentuan maupun pelaksanaan hak atau wewenang Kami berdasarkan Syarat dan Ketentuan.</li>
        </ul>
    </section>
    </div>

<?php require('footer-login.php');?>