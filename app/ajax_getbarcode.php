<?php
//set it to writable location, a place for temp generated PNG files
$PNG_TEMP_DIR = dirname(__FILE__).DIRECTORY_SEPARATOR.'temp'.DIRECTORY_SEPARATOR;

//html PNG location prefix
$PNG_WEB_DIR = 'temp/';

include('qrcode/qrlib.php');

//ofcourse we need rights to create temp dir
if (!file_exists($PNG_TEMP_DIR))
    mkdir($PNG_TEMP_DIR);

$code = $_POST['number'];
$errorCorrectionLevel = 'H';
$matrixPointSize = 5;
$filename = $PNG_TEMP_DIR.'qr'.md5($code.'|'.$errorCorrectionLevel.'|'.$matrixPointSize).'.png';
QRcode::png($code, $filename, $errorCorrectionLevel, $matrixPointSize, 2);
?>
<img src="<?php echo $PNG_WEB_DIR.basename($filename); ?>" data-dismiss="modal">
<br>
<?php
include('barcode128/BarcodeGenerator.php');
include('barcode128/BarcodeGeneratorPNG.php');
include('barcode128/BarcodeGeneratorSVG.php');
include('barcode128/BarcodeGeneratorJPG.php');
include('barcode128/BarcodeGeneratorHTML.php');

$generat = new Picqer\Barcode\BarcodeGeneratorPNG();
$resour = 'data:image/png;base64,'.base64_encode($generat->getBarcode($_POST['number'], $generat::TYPE_CODE_128));
?>
<img src="<?php echo $resour; ?>" style="width: 100%;height: 50px;" data-dismiss="modal">