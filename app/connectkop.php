<?php require('header.php');?>

    <div class="col-sm-12">
        <div class="box box-solid">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="box-body">
                <div style="text-align: center;">
                    <img width="50" height="50" src="images/loading.gif">
                    <br>
                    <?php echo $lang->lang('Sedang menghubungkan ke koperasi', $conn); ?>
                </div>
            </div>
        </div>
    </div>

<?php
if(isset($_GET['idkoperasiwallet'])){
    unset($_SESSION['KID']);
    unset($_SESSION['KoperasiName']);
    unset($_SESSION['NickName']);
    unset($_SESSION['Kop']);
    unset($_SESSION['MemberID']);

    $x = "select* from [dbo].[UserMemberKoperasi] where UserID = '$_SESSION[UserID]' and KID = '$_GET[idkoperasiwallet]'";
    $y = sqlsrv_query($conn, $x);
    $z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC);
    if($z != null){
        $xx = "select top 1 * from [dbo].[ListKoperasiView] where UserID = '$_SESSION[UserID]' and KID = '$_GET[idkoperasiwallet]'";
        $yy = sqlsrv_query($conn, $xx);
        $zz = sqlsrv_fetch_array($yy, SQLSRV_FETCH_NUMERIC);
        if($zz != null){
            $poi = "select* from $zz[3].[dbo].[Profil]";
            $lkj = sqlsrv_query($conn, $poi);
            $mnb = sqlsrv_fetch_array($lkj, SQLSRV_FETCH_NUMERIC);
            if($mnb != null){
                $_SESSION['KoperasiName'] = $mnb[1];
                $_SESSION['NickName'] = $mnb[2];
                $_SESSION['Logo'] = $mnb[6];
            }

            $cvb = "select* from $zz[3].[dbo].[CardDesign]";
            $dfg = sqlsrv_query($conn, $cvb);
            $ert = sqlsrv_fetch_array($dfg, SQLSRV_FETCH_NUMERIC);
            if($ert != null){
                $_SESSION['CardImage'] = '../koperasi/'.$ert[0];
                $_SESSION['CardFontColor'] = $ert[1];
            }
            else{
                $_SESSION['CardImage'] = 'card/card.png';
                $_SESSION['CardFontColor'] = '#000000';
            }

            $_SESSION['KID'] = $zz[0];
            $_SESSION['Kop'] = $zz[3];
        }
        $_SESSION['MemberID'] = $z[2];

        echo "<script language='javascript'>document.location='balance.php';</script>";
    }
    else{
        echo "<script language='javascript'>document.location='close.php';</script>";
    }
}

?>

<?php require('footer.php');?>