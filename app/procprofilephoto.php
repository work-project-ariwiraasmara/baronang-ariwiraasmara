<?php require('header.php');?>

    <div class="col-sm-12">
        <div class="box box-solid">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="box-body">
                <div style="text-align: center;">
                    <img width="50" height="50" src="images/loading.gif">
                </div>
            </div>
        </div>
    </div>

<?php
session_start(); //Do not remove this
error_reporting(0);

function resizeImage($image,$width,$height,$scale) {
    list($imagewidth, $imageheight, $imageType) = getimagesize($image);
    $imageType = image_type_to_mime_type($imageType);
    $newImageWidth = ceil($width * $scale);
    $newImageHeight = ceil($height * $scale);
    $newImage = imagecreatetruecolor($newImageWidth,$newImageHeight);
    switch($imageType) {
        case "image/gif":
            $source=imagecreatefromgif($image);
            break;
        case "image/pjpeg":
        case "image/jpeg":
        case "image/jpg":
            $source=imagecreatefromjpeg($image);
            break;
        case "image/png":
        case "image/x-png":
            $source=imagecreatefrompng($image);
            break;
    }
    imagecopyresampled($newImage,$source,0,0,0,0,$newImageWidth,$newImageHeight,$width,$height);

    switch($imageType) {
        case "image/gif":
            imagegif($newImage,$image);
            break;
        case "image/pjpeg":
        case "image/jpeg":
        case "image/jpg":
            imagejpeg($newImage,$image,90);
            break;
        case "image/png":
        case "image/x-png":
            imagepng($newImage,$image);
            break;
    }

    chmod($image, 0777);
    return $image;
}

$upload_dir = "upload_pic"; 				// The directory for the images to be saved in
$upload_path = $upload_dir."/";				// The path to where the image will be saved
$large_image_prefix = "resize_"; 			// The prefix name to large image
$profile_image_prefix = "profile_"; 			// The prefix name to large image
$large_image_name = $large_image_prefix.$_SESSION['UserID'];     // New name of the large image (append the timestamp to the filename)
$profile_image_name = $profile_image_prefix.$_SESSION['UserID'];     // New name of the large image (append the timestamp to the filename)
$max_file = "6"; 							// Maximum file size in MB
$max_width = "500";							// Max width allowed for the large image

// Only one of these image types should be allowed for upload
$allowed_image_types = array('image/pjpeg'=>"jpg",'image/jpeg'=>"jpg",'image/jpg'=>"jpg",'image/png'=>"png",'image/x-png'=>"png",'image/gif'=>"gif");
$allowed_image_ext = array_unique($allowed_image_types); // do not change this
$image_ext = "";	// initialise variable, do not change this.
foreach ($allowed_image_ext as $mime_type => $ext) {
    $image_ext.= strtoupper($ext)." ";
}

//You do not need to alter these functions
function getHeight($image) {
    $size = getimagesize($image);
    $height = $size[1];
    return $height;
}
//You do not need to alter these functions
function getWidth($image) {
    $size = getimagesize($image);
    $width = $size[0];
    return $width;
}

//Create the upload directory with the right permissions if it doesn't exist
if(!is_dir($upload_dir)){
    mkdir($upload_dir, 0777);
    chmod($upload_dir, 0777);
}

//Image Locations
$large_image_location = $upload_path.$large_image_name.$_SESSION['user_file_ext'];
$profile_image_location = $upload_path.$profile_image_name.$_SESSION['user_file_ext'];

if (isset($_POST["upload"])) {
    //Get the file information
    $userfile_name = $_FILES['image']['name'];
    $userfile_tmp = $_FILES['image']['tmp_name'];
    $userfile_size = $_FILES['image']['size'];
    $userfile_type = $_FILES['image']['type'];
    $filename = basename($_FILES['image']['name']);
    $file_ext = strtolower(substr($filename, strrpos($filename, '.') + 1));

    //Only process if the file is a JPG, PNG or GIF and below the allowed limit
    if((!empty($_FILES["image"])) && ($_FILES['image']['error'] == 0)) {

        foreach ($allowed_image_types as $mime_type => $ext) {
            //loop through the specified image types and if they match the extension then break out
            //everything is ok so go and check file size
            if($file_ext==$ext && $userfile_type==$mime_type){
                $error = "";
                break;
            }else{
                $error = "Only ".$image_ext." images accepted for upload";
            }
        }

        //check if the file size is above the allowed limit
        if ($userfile_size > ($max_file*1048576)) {
            $error.= "Images must be under ".$max_file."MB in size";
        }

        //Everything is ok, so we can upload the image.
        if (strlen($error)==0){
            if (isset($_FILES['image']['name'])){
                //Delete the image file so the user can create a new one
                if (file_exists($profile_image_location)) {
                    unlink($profile_image_location);
                }

                if (file_exists($large_image_location)) {
                    unlink($large_image_location);
                }

                //put the file ext in the session so we know what file to look for once its uploaded
                $_SESSION['user_file_ext']=".".$file_ext;

                $image_location = $upload_path.$large_image_name.$_SESSION['user_file_ext'];

                move_uploaded_file($userfile_tmp, $image_location);
                chmod($image_location, 0777);

                $width = getWidth($image_location);
                $height = getHeight($image_location);
                //Scale the image if it is greater than the width set above
                if ($width > $max_width){
                    $scale = $max_width/$width;
                    $uploaded = resizeImage($image_location,$width,$height,$scale);
                }else{
                    $scale = 1;
                    $uploaded = resizeImage($image_location,$width,$height,$scale);
                }
            }

            //Refresh the page to show the new uploaded image
            header("location: changephoto.php");
        }
        else{
            echo "<script language='javascript'>System.showToast('$error');document.location='changephoto.php';</script>";
        }

    }else{
        echo "<script language='javascript'>System.showToast('Pilih gambar untuk diupload');document.location='changephoto.php';</script>";
    }
}

if(isset($_GET['a']) == 'delete' and isset($_GET['t'])){
    //get the file locations
    $large_image_location = $upload_path.$_GET['t'];
    if (file_exists($large_image_location)) {
        unset($_SESSION['user_file_ext']);
        unlink($large_image_location);
    }

    //Refresh the page to show the new uploaded image
    header("location: changephoto.php");
}

if (isset($_POST["content"]) and file_exists($large_image_location)) {
    unlink($large_image_location);

    file_put_contents($profile_image_location, file_get_contents($_POST['content']));

    echo "<script language='javascript'>System.showToast('Berhasil mengubah foto profil');document.location='profile.php';</script>";
}
?>

<?php require('footer.php');?>