<?php
session_start();
include "connect.inc";

$a = "exec dbo.RegularSavingTypeSearch '$_SESSION[KID]','$_POST[acc]'";
$b = sqlsrv_query($conn, $a);
$c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC);

if($c[2] == 0){
    $type = 'Saldo harian';
}
else if($c[2] == 1){
    $type = 'Saldo rata-rata';
}
else if($c[2] == 2){
    $type = 'Saldo terendah';
}
else{
    $type = '';
}
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel">Tabungan - <?php echo $c[1]; ?></h4>
</div>
<div class="modal-body">
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_1" data-toggle="tab">Info</a></li>
            <li><a href="#tab_2" data-toggle="tab">Skema Bunga</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab_1">
                <ul class="nav nav-stacked">
                    <li><a href="#">Tipe Bunga <span class="pull-right"><?php echo $type; ?></span></a></li>
                    <li><a href="#">Suku Bunga % <span class="pull-right"><?php echo $c[3]; ?>%</span></a></li>
                    <li><a href="#">Saldo Minimum <span class="pull-right"><?php echo number_format($c[4]); ?></span></a></li>
                    <li><a href="#">Penarikan Maksimum <span class="pull-right"><?php echo number_format($c[5]); ?></span></a></li>
                    <li><a href="#">Biaya Admin <span class="pull-right"><?php echo number_format($c[6]); ?></span></a></li>
                    <li><a href="#">Biaya Penutupan <span class="pull-right"><?php echo number_format($c[9]); ?></span></a></li>
                </ul>
            </div>
            <!-- /.tab-pane -->
            <div class="tab-pane" id="tab_2">
                <div class="table-responsive">
                    <table class="table">
                        <?php
                        $zxc = "select * from $_SESSION[Kop].dbo.SkemaBunga where RegularSavingType = '$_POST[acc]' order by Urutan asc";
                        $asd = sqlsrv_query($conn, $zxc);
                        while($qwe = sqlsrv_fetch_array($asd, SQLSRV_FETCH_NUMERIC)){
                            ?>
                            <tr>
                                <th><?php echo number_format($qwe[2]).' - '.number_format($qwe[3]); ?></th>
                                <td><?php echo $qwe[4]; ?>%</td>
                            </tr>
                        <?php } ?>
                    </table>
                </div>
            </div>
        </div>
        <!-- /.tab-content -->
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
</div>