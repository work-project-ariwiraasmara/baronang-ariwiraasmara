<?php require('header.php');?>

<?php
if(!isset($_SESSION['KID'])){
    echo "<script language='javascript'>document.location='close.php';</script>";
}
?>
    <div class="col-lg-12">
        <div class="box box-solid">
            <div class="box-header">
                <h3 class="box-title"><?php echo $lang->lang('Buka Pengajuan Pinjaman', $conn); ?></h3>
            </div>
            <div class="box-body">
                <form action="procloan.php" id="sloan" method="POST" class="form-horizontal" enctype="multipart/form-data">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label class="col-sm-3" style="text-align: left;"><?php echo $lang->lang('Produk Pinjaman', $conn); ?></label>
                            <div class="col-sm-9">
                                <table class="table table-bordered table-striped">
                                    <?php
                                    $a = "exec dbo.LoanTypeView '$_SESSION[KID]'";
                                    $b = sqlsrv_query($conn, $a);
                                    while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                                        $itype = '';
                                        if($c[2] == '01'){
                                            $itype = 'Flat';
                                        }
                                        else if($c[2] == '02'){
                                            $itype = 'Efektif';
                                        }
                                        else if($c[2] == '03'){
                                            $itype = 'Annuitas';
                                        }

                                        $mat = '';
                                        if($c[6] == '01'){
                                            $mat = 'Bulanan';
                                        }
                                        else if($c[6] == '02'){
                                            $mat = 'Mingguan';
                                        }
                                        else if($c[6] == '03'){
                                            $mat = 'Harian';
                                        }
                                        ?>
                                        <tr>
                                            <td><input type="radio" name="acc" class="acc" value="<?php echo $c[0]; ?>" data-toggle="modal" data-target=".bs-example-modal-<?php echo $c[0]; ?>"></td>
                                            <td><?php echo $c[1]; ?></td>
                                        </tr>

                                        <div class="modal fade bs-example-modal-<?php echo $c[0]; ?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title" id="myModalLabel">Pinjaman - <?php echo $c[1]; ?></h4>
                                                    </div>
                                                    <div class="modal-body">

                                                        <ul class="nav nav-stacked">
                                                            <li><a href="#"><?php echo $lang->lang('Tipe Bunga', $conn); ?><span class="pull-right"><?php echo $itype; ?></span></a></li>
                                                            <li><a href="#"><?php echo $lang->lang('Suku Bunga %', $conn); ?><span class="pull-right"><?php echo $c[3]; ?>%</span></a></li>
                                                            <li><a href="#"><?php echo $lang->lang('Masa Kontrak', $conn); ?><span class="pull-right"><?php echo $c[7]; ?> month</span></a></li>
                                                            <li><a href="#"><?php echo $lang->lang('Jumlah Minimum', $conn); ?><span class="pull-right"><?php echo number_format($c[4]); ?></span></a></li>
                                                            <li><a href="#"><?php echo $lang->lang('Jumlah Maximum', $conn); ?><span class="pull-right"><?php echo number_format($c[5]); ?></span></a></li>
                                                            <li><a href="#"><?php echo $lang->lang('Maturity', $conn); ?> <span class="pull-right"><?php echo $mat; ?></span></a></li>
                                                            <li><a href="#"><?php echo $lang->lang('Pokok Penalti %', $conn); ?><span class="pull-right"><?php echo $c[8]; ?>%</span></a></li>
                                                            <li><a href="#"><?php echo $lang->lang('Bunga Penalti %', $conn); ?><span class="pull-right"><?php echo $c[9]; ?>%</span></a></li>
                                                        </ul>

                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $lang->lang('Tutup', $conn); ?></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </table>
                                <input type="hidden" name="kid" value="<?php echo $_SESSION['KID']; ?>" readonly>
                                <input type="hidden" name="mid" value="<?php echo $_SESSION['MemberID']; ?>" readonly>
                                <input type="hidden" name="jenis" value="10" readonly>
                                <input type="hidden" name="max" id="max" value="0" readonly>
                                <input type="hidden" id="min" value="0" readonly>

                                <input type="hidden" name="minpen" id="minpen" value="0" readonly>
                                <input type="hidden" name="mindoc" id="mindoc" value="0" readonly>

                                <input type="hidden" name="tempo" class="price" id="tempo" value="0" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3" style="text-align: left;"><?php echo $lang->lang('Jumlah Minimum', $conn); ?></label>
                            <div class="col-sm-9">
                                <div class="min"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3" style="text-align: left;"><?php echo $lang->lang('Jumlah Maksimum', $conn); ?></label>
                            <div class="col-sm-9">
                                <div class="max"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3" style="text-align: left;"><?php echo $lang->lang('Sisa Batas Pinjaman', $conn); ?></label>
                            <div class="col-sm-9">
                                <span class="last text-bold">0</span>
                                dari
                                <span class="limit">0</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3" style="text-align: left;"><?php echo $lang->lang('Lama Angsuran ', $conn); ?><span id="contracttime"></span></label>
                            <div class="col-sm-9">
                                <input type="text" name="contractperiod" id="contractperiod" class="form-control" value="0">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3" style="text-align: left;"><?php echo $lang->lang('Jumlah Pinjaman', $conn); ?></label>
                            <div class="col-sm-9">
                                <input type="number" name="amount" id="amount" class="form-control" placeholder="" value="0" required="">
                                <button type="button" class="btn btn-primary btn-flat btn-sm hide" id="btn-simulasi" data-toggle="modal" data-target=".bs-example-modal-simulasi">Lihat Simulasi</button>

                                <div class="modal fade bs-example-modal-simulasi" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="myModalLabel"><?php echo $lang->lang('Simulasi Pinjaman', $conn); ?></h4>
                                            </div>
                                            <div class="modal-body-loan">

                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $lang->lang('Tutup', $conn); ?></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3" style="text-align: left;"><?php echo $lang->lang('Akun Tabungan', $conn); ?></label>
                            <div class="col-sm-9">
                                <select name="rsacc" class="form-control">
                                    <option value=""><?php echo $lang->lang('Pilih', $conn); ?></option>
                                    <?php
                                    $a = "exec dbo.ListRegularSavingBal '$_SESSION[KID]','$_SESSION[MemberID]'";
                                    $b = sqlsrv_query($conn, $a);
                                    while($c = sqlsrv_fetch_array($b, SQLSRV_FETCH_NUMERIC)){
                                    ?>
                                        <option value="<?php echo $c[2]; ?>"><?php echo $c[2].' - '.$c[4].' , Rp. '.$c[6]; ?></option>
                                    <?php } ?>
                                </select>
                                <?php echo $lang->lang('Pinjaman akan dicairkan ke akun tabungan', $conn); ?>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group">
                            <label class="col-sm-3" style="text-align: left;"><?php echo $lang->lang('Penjamin', $conn); ?></label>
                            <div class="col-sm-9">
                                <div id="dpen">
                                    <?php echo $lang->lang('Pilih produk pinjaman', $conn); ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3" style="text-align: left;"><?php echo $lang->lang('Dokumen', $conn); ?></label>
                            <div class="col-sm-9">
                                <div id="ddoc">
                                    <?php echo $lang->lang('Pilih produk pinjaman', $conn); ?>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <div class="row">
                                <button type="submit" class="btn btn-flat btn-block btn-success btn-save"><?php echo $lang->lang('Simpan', $conn); ?></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        //var rows = document.getElementById("membertable").getElementsByTagName("tbody")[0].getElementsByTagName("tr").length;

        $('.acc').click(function(){
            var acc = $("input[name='acc']:checked").val();

            $.ajax({
                url : "ajax_doc.php",
                type : 'POST',
                data: { acc: acc},
                success : function(data) {
                    $("#ddoc").html(data);
                },
                error : function(){
                    System.showToast('Coba lagi');
                }
            });

            $.ajax({
                url : "ajax_penjamin.php",
                type : 'POST',
                data: { acc: acc},
                success : function(data) {
                    $("#dpen").html(data);
                },
                error : function(){
                    System.showToast('Coba lagi');
                }
            });

            $.ajax({
                url : "ajax_getloan.php",
                type : 'POST',
                dataType: 'json',
                data: { acc: acc},
                success : function(data) {
                    if(data.status == 1){
                        $("#btn-simulasi").removeClass('hide');

                        $("#min").val(data.min);
                        $("#max").val(data.last);
                        $(".max").html(data.maxloan);
                        $(".min").html(data.minloan);
                        $(".limit").html(data.limitf);
                        $(".last").html(data.lastf);

                        $("#minpen").val(data.minpen);
                        $("#mindoc").val(data.mindoc);
                        $("#tempo").val(data.tempo);
                        $('#contractperiod').val(data.contractperiod);
                        $('#contracttime').html('(Max '+data.contractperiod+'x)');
                    }
                    else{
                        System.showToast(data.message);
                        return false;
                    }
                },
                error : function(){
                    System.showToast('Coba lagi');
                }
            });
        });

        $('#btn-simulasi').click(function(){
            var acc = $("input[name='acc']:checked").val();
            var amount = $('#amount').val();

            $.ajax({
                url : "ajax_getsimulasi.php",
                type : 'POST',
                data: { acc: acc, amount: amount},
                success : function(data) {
                    $(".modal-body-loan").html(data);
                },
                error : function(){
                    System.showToast('Coba lagi');
                }
            });
        });
    </script>

<?php require('footer.php');?>
