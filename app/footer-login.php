    <br>
    </div>
</div>

<!-- Bootstrap 3.3.5 -->
<script src="static/bootstrap/js/bootstrap.min.js"></script>

<script type="text/javascript">
    (function(window, location) {
        history.replaceState(null, document.title, location.pathname+"#!/history");
        history.pushState(null, document.title, location.pathname);

        window.addEventListener("popstate", function() {
            if(location.hash === "#!/history") {
                history.replaceState(null, document.title, location.pathname);
                setTimeout(function(){
                    Intent.openActivity('LoginActivity', '<?php echo $_SESSION['url']; ?>');
                },0);
            }
        }, false);
    }(window, location));
</script>

</body>
</html>
