<?php require('header.php');?>

<?php
if(!isset($_SESSION['KID'])){
    echo "<script language='javascript'>document.location='close.php';</script>";
}

if(!isset($_SESSION['acc'])){
    echo "<script language='javascript'>document.location='balance.php';</script>";
}

$_SESSION['url'] = 'report.php?type=loan&id='.$_SESSION['acc'];

$tab = '';
$balance = 0;
$x = "exec dbo.LoanApplicationReleaseSearch '$_SESSION[KID]','$_GET[acc]'";
$y = sqlsrv_query($conn, $x);
$z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC);
if($z != null){
    $xx = "exec dbo.LoanTypeSearch '$_SESSION[KID]','$z[4]'";
    $yy = sqlsrv_query($conn, $xx);
    $zz = sqlsrv_fetch_array($yy, SQLSRV_FETCH_NUMERIC);
    if($zz != null){
        $tab = $zz[1];
    }

    $acc = $z[1];
    //cek yang sudah bayar
    $aa = "exec dbo.LoanPaidAll '$_SESSION[KID]','$z[1]'";
    $bb = sqlsrv_query($conn, $aa);
    $cc = sqlsrv_fetch_array( $bb, SQLSRV_FETCH_NUMERIC);
    if($cc != null){
        $sisa = $z[5] - ($cc[1]+$cc[2]);
        $balance = number_format($sisa);
    }
    else{
        $balance = number_format($z[5]);
    }
}

include('qrcode/qrlib.php');
$errorCorrectionLevel = 'H';
$matrixPointSize = 5;
$PNG_TEMP_DIR = dirname(__FILE__).DIRECTORY_SEPARATOR.'temp'.DIRECTORY_SEPARATOR;
$PNG_WEB_DIR = 'temp/';
$code = $_GET['acc'];

$filename = $PNG_WEB_DIR.'qr'.md5($code.'|'.$errorCorrectionLevel.'|'.$matrixPointSize).'.png';
if (!file_exists($filename)){
    QRcode::png($code, $filename, $errorCorrectionLevel, $matrixPointSize, 2);
}
?>

    <div class="col-sm-12">
        <div class="box box-solid">
            <div class="box-body">
                <table class="table table-bordered table-striped">
                    <tr>
                        <td><?php echo $acc; ?></td>
                        <td rowspan="3">
                            <div class="pull-right text-center">
                                <img src="<?php echo $filename; ?>" style="width: 65%;" data-toggle="modal" data-target="#myBarcode">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td><?php echo $tab; ?></td>
                    </tr>
                    <tr>
                        <td><?php echo $balance; ?></td>
                    </tr>
                </table>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="myBarcode" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document" style="margin-top: 40%;">
                <div class="modal-content">
                    <div class="modal-body">
                        <center>
                            <img src="<?php echo $filename; ?>" style="width: 60%;" data-dismiss="modal">
                        </center>
                        <table class="table text-center">
                            <tr>
                                <th>
                                    <span class="lead">
                                    <?php echo $acc; ?><br>
                                        <?php echo $tab; ?><br><br>
                                    </span>
                                    <?php echo $balance; ?>
                                </th>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="box box-solid">
            <div class="box-header">
                <h3 class="box-title"><?php echo $lang->lang('Simulasi Cicilan', $conn); ?></h3>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <tr>
                            <th>No.</th>
                            <th>Batas Tanggal Akhir</th>
                            <th>Jumlah Pinjaman</th>
                            <th>Pokok</th>
                            <th>Bunga</th>
                            <th>Jumlah Cicilan</th>
                        </tr>
                        <?php
                        $x = "exec dbo.SimulasiLoanSearch '$_SESSION[KID]','$_GET[acc]'";
                        $y = sqlsrv_query($conn, $x);
                        while($z = sqlsrv_fetch_array($y, SQLSRV_FETCH_NUMERIC)){
                            ?>
                            <tr>
                                <td><?php echo $z[2]; ?></td>
                                <td><?php echo $z[1]->format('Y-m-d'); ?></td>
                                <td><?php echo number_format($z[3], 2); ?></td>
                                <td><?php echo number_format($z[4], 2); ?></td>
                                <td><?php echo number_format($z[5], 2); ?></td>
                                <td><?php echo number_format($z[6], 2); ?></td>
                            </tr>
                        <?php } ?>
                    </table>
                </div>
            </div>
        </div>
    </div>

<?php require('footer.php');?>