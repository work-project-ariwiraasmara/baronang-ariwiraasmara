<?php require('header.php'); ?>

	<div id="content" class="page txt-black">

		<div id="toolbar" class="primary-color">
			<a href="index.php" class="open-left" id="toBack">
				<i class="ion-android-arrow-back"></i>
			</a>
			
			<span class="title uppercase txt-white" id="title"></span>
		</div>

		<div class="animated fadeinup delay-1">
			<div class="page-content">

				<form action="" method="post">
					<div class="input-field">
						<span class="bold">COA 1</span>
						<select class="browser-default txt-black" name="coa1" id="coa1" required>
							<option value="0">-Select COA 1-</option>
							<?php
							$scoa1 = "SELECT * from 11coa1 order by coa1 ASC";
							$qcoa1 = mysqli_query($koneksi, $scoa1) or die(mysqli_error($koneksi));
							$rcoa1 = mysqli_num_rows($qcoa1);
							if($rcoa1 > 0) {
								while($dcoa1 = mysqli_fetch_array($qcoa1)) { ?>
									<option value="<?php echo $dcoa1['coa1id']; ?>"><?php echo $dcoa1['coa1desc']; ?></option>
									<?php
								}
							}
							else { ?>
								<option value="" disabled>Data is Empty</option>
								<?php
							}							
							?>
						</select>
					</div>

					<div class="input-field">
						<label for="coa2" class="txt-black">COA 2</label>
						<input type="number" name="coa2" id="coa2" class="validate">
					</div>

					<div class="input-field">
						<label for="coa2desk" class="txt-black">COA 2 Description</label>
						<input type="text" name="coa2desk" id="coa2desk" class="validate">
					</div>

					<input type="text" name="numbertoajax" id="numbertoajax" value="0" class="hide">

					<input type="text" name="query" id="query" value="" class="hide" readonly>

					<div class="bold italic center" id="msgadd"></div>

					<button type="button" name="btnadd" id="btnadd" class="btn btn-large width-100 waves-effect waves-light primary-color m-t-30 borad-20">Add</button>

					<table class="table">
						<thead>
							<tr>
								<th>COA 2</th>
								<th>Description</th>
								<th></th>
							</tr>
						</thead>

						<tbody id="tobeadded">
							
						</tbody>

						<tfoot>
							<tr>
								<th colspan="2"><hr></th>
							</tr>
						</tfoot>
					</table>

					<button type="submit" name="confirm" class="btn btn-large width-100 waves-effect waves-light primary-color m-t-10 borad-20">Save</button>
				</form>

				<?php
				if(isset($_POST['confirm'])) {

					$coa1 		= @$_POST['coa1'];
					$coa2 		= @$_POST['coa2'];
					$coa2desk 	= @$_POST['coa2desk'];
					
					$qval	 	= @$_POST['query'];
					$lenqval 	= (int)strlen($qval);
					$qval 		= substr($qval, 0, $lenqval-1).');';

					$sql = "INSERT into 12coa2(coa1id, coa2, coa2desc) values $qval";
					echo $sql;
					$res = mysqli_query($koneksi, $sql) or die(mysqli_error($koneksi));
					if($res) { ?>
						<script type="text/javascript">
							window.location.href = "?";
						</script>
						<?php
					}

				}
				?>

				<div class="table-responsive m-t-30">
					<!--<div class="txt-link right">
						<a href="#" id="refreshtable">refresh</a>
					</div> -->
					
					<table class="table">
						<thead>
							<tr>
								<th class="center">COA 2</th>
								<th class="center">COA 2 Description</th>
							</tr>
						</thead>

						<tbody id="loadtable">
							<?php
							$no = 1;
							$scoa 	= "SELECT * from 12coa2 as a order by coa1id, coa2 ASC";
							$qcoa = mysqli_query($koneksi, $scoa) or die(mysqli_error($koneksi));
							while($dcoa = mysqli_fetch_array($qcoa)) { 

								$scoa1 = "SELECT * from 11coa1 where coa1id='".$dcoa['coa1id']."'";
								$qcoa1 = mysqli_query($koneksi, $scoa1) or die(mysqli_error($koneksi));
								$dcoa1 = mysqli_fetch_array($qcoa1);

								?>
								<tr>
									<td><?php echo $dcoa1['coa1'].'-'.$dcoa['coa2']; ?></td>
									<td><?php echo $dcoa['coa2desc']; ?></td>
								</tr>
								<?php
								$no++;
							}
							?>
						</tbody>

						<tfoot>
							<tr>
								<th colspan="2"><hr></th>
							</tr>
						</tfoot>
					</table>
				</div>

			</div>
		</div>

	</div>

<script type="text/javascript">
	var valtobeadd = [];

	$("#btnadd").click(function() {
		var coa 		= "coa1";
		var coa1 		= $("#coa1 :selected").val();
		var coa2 		= $("#coa2").val();
		var coa2desk 	= $("#coa2desk").val();
		var qval 		= $("#query").val();
		var number 		= $("#numbertoajax").val();

		if( (coa1 == "") || (coa2 == "") || (coa2desk == "") ) {
			alert("COA 1, COA2 or COA 2 Description can't be empty");
		}
		else {
			$.ajax({
				url : "ajax_add_coa.php",
				type : 'POST',
				data: { 
					coa1: coa1,
					coa2: coa2,
					coadesk: coa2desk,
					coa_reltype: coa,
					number: number
				},
				success : function(data) {
					if(data == "exist") {
						$("#msgadd").html("This data already exist");
					}
					else {
						var valadd = "('" + coa1 + "','" + coa2 + "','" + coa2desk + "')";
						number = number + 1;
						valtobeadd.push(valadd);
						$("#numbertoajax").val(number);
						$("#tobeadded").append(data);
						$("#query").val(valtobeadd.join());
						$("#coa2").val('');
						$("#coa2desk").val('');
						$("#msgadd").html("");
					}	
				},
				error : function(){
					alert('Terjadi kesalahan dalam jaringan!\nTidak dapat menambah data!\nSilahkan cek koneksi jaringan dan coba lagi!');
					return false;
				}
			});
		}
	});

	$("#coa1").change(function(){
		var coa1 	= $("#coa1 :selected").val();
		var coatype = "coa1";

		$.ajax({
			url : "ajax_loadtable_relasi_coa.php",
			type : 'POST',
			data: { 
				coa1: coa1,
				coa_reltype: coatype
			},
			success : function(data) {
				$("#loadtable").html(data);
			},
			error : function(){
				alert('Terjadi kesalahan dalam jaringan!\nTidak dapat menambah data!\nSilahkan cek koneksi jaringan dan coba lagi!');
				return false;
			}
		});
	});

	$("#refreshtable").click(function(){
		var coatype = "coa2";

		$.ajax({
			url : "ajax_refresh_table.php",
			type : 'POST',
			data: { 
				coa_reltype: coatype
			},
			success : function(data) {
				$("#loadtable").html(data);
			},
			error : function(){
				alert('Terjadi kesalahan dalam jaringan!\nTidak dapat menambah data!\nSilahkan cek koneksi jaringan dan coba lagi!');
				return false;
			}
		});
	});
</script>

<?php 
$fun->setTitle_CP('Baronang');
$fun->setTitle('COA 2');
$fun->setDocTitle();
require('footer.php'); 
?>
