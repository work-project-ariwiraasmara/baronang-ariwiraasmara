<?php require('header.php'); ?>

	<div id="content" class="page txt-black">

		<div id="toolbar" class="primary-color">
			<a href="index.php" class="open-left" id="toBack">
				<i class="ion-android-arrow-back"></i>
			</a>
			
			<span class="title uppercase txt-white" id="title"></span>
		</div>

		<div class="animated fadeinup delay-1">
			<div class="page-content">

				<form action="" method="post">
					<div class="input-field">
						<span class="bold">COA 1</span>
						<select class="browser-default txt-black" name="coa1" id="coa1" required>
							<option value="0">-Select Item-</option>
							<?php
							$scoa1 = "SELECT * from 11coa1 order by coa1 ASC";
							$qcoa1 = mysqli_query($koneksi, $scoa1) or die(mysqli_error($koneksi));
							$rcoa1 = mysqli_num_rows($qcoa1);
							if($rcoa1 > 0) {
								while($dcoa1 = mysqli_fetch_array($qcoa1)) { ?>
									<option value="<?php echo $dcoa1['coa1id']; ?>"><?php echo $dcoa1['coa1desc']; ?></option>
									<?php
								}
							}
							else { ?>
								<option value="" disabled>Data is Empty</option>
								<?php
							}							
							?>
						</select>
					</div>

					<div class="input-field">
						<span class="bold">COA 2</span>
						<select class="browser-default txt-black" name="coa2" id="coa2" required>
							<option value="0" disabled>Choose COA 1 First..</option>
						</select>
					</div>

					<div class="input-field">
						<label for="coa3" class="txt-black">COA 3</label>
						<input type="number" name="coa3" id="coa3" class="validate">
					</div>

					<div class="input-field">
						<label for="coa3desk" class="txt-black">COA 3 Description</label>
						<input type="text" name="coa3desk" id="coa3desk" class="validate">
					</div>

					<input type="text" name="numbertoajax" id="numbertoajax" value="0" class="hide">

					<input type="text" name="query" id="query" value="" class="hide">

					<div class="bold italic center" id="msgadd"></div>

					<button type="button" name="btnadd" id="btnadd" class="btn btn-large width-100 waves-effect waves-light primary-color m-t-30 borad-20">Add</button>

					<table class="table">
						<thead>
							<tr>
								<th>COA 3</th>
								<th>Description</th>
								<th></th>
							</tr>
						</thead>

						<tbody id="tobeadded">
							
						</tbody>

						<tfoot>
							<tr>
								<th colspan="2"><hr></th>
							</tr>
						</tfoot>
					</table>

					<button type="submit" name="confirm" class="btn btn-large width-100 waves-effect waves-light primary-color m-t-10 borad-20">Save</button>
				</form>

				<?php
				if(isset($_POST['confirm'])) {

					$coa2 		= @$_POST['coa2'];
					$coa3 		= @$_POST['coa3'];
					$coa3desk 	= @$_POST['coa3desk'];

					$qval	 	= @$_POST['query'];
					$lenqval 	= (int)strlen($qval);
					$qval 		= substr($qval, 0, $lenqval-1).');';

					$sql = "INSERT into 13coa3(coa2id, coa3, coa3desc) values $qval";
					//echo $sql;
					$res = mysqli_query($koneksi, $sql) or die(mysqli_error($koneksi));
					if($res) { ?>
						<script type="text/javascript">
							window.location.href = "?";
						</script>
						<?php
					}

				}
				?>

				<div class="table-responsive m-t-30">
					<!--<div class="txt-link right">
						<a href="#" id="refreshtable">refresh</a>
					</div>-->

					<table class="table">
						<thead>
							<tr>
								<th class="center">COA 3</th>
								<th class="center">COA 3 Description</th>
							</tr>
						</thead>

						<tbody id="loadtable">
							<?php
							$no = 1;
							$scoa 	= "SELECT * from 13coa3 order by coa2id, coa3 ASC";
							$qcoa = mysqli_query($koneksi, $scoa) or die(mysqli_error($koneksi));
							while($dcoa = mysqli_fetch_array($qcoa)) { 

								$scoa2 = "SELECT * from 12coa2 where coa2id='".$dcoa['coa2id']."'";
								$qcoa2 = mysqli_query($koneksi, $scoa2) or die(mysqli_error($koneksi));
								$dcoa2 = mysqli_fetch_array($qcoa2);

								$scoa1 = "SELECT * from 11coa1 where coa1id='".$dcoa2['coa1id']."'";
								$qcoa1 = mysqli_query($koneksi, $scoa1) or die(mysqli_error($koneksi));
								$dcoa1 = mysqli_fetch_array($qcoa1);

								?>
								<tr>
									<td><?php echo $dcoa1['coa1'].'-'.$dcoa2['coa2'].'-'.$dcoa['coa3']; ?></td>
									<td><?php echo $dcoa['coa3desc']; ?></td>
								</tr>
								<?php
								$no++;
							}
							?>
						</tbody>

						<tfoot id="tobeadded">
							<tr>
								<th colspan="2"><hr></th>
							</tr>
						</tfoot>
					</table>
				</div>

			</div>
		</div>

	</div>

<script type="text/javascript">
	var valtobeadd = new Array();

	$("#btnadd").click(function() {
		var coa 		= "coa2";
		var coa1 		= $("#coa1 :selected").val();
		var coa2 		= $("#coa2 :selected").val();
		var coa3 		= $("#coa3").val();
		var coa3desk 	= $("#coa3desk").val();
		var qval 		= $("#query").val();
		var number 		= $("#numbertoajax").val();

		if( (coa1 == "") || (coa2 == "") || (coa3 == "") || (coa3desk == "") ) {
			alert("COA 1, COA2 or COA 2 Description can't be empty");
		}
		else {
			$.ajax({
				url : "ajax_add_coa.php",
				type : 'POST',
				data: { 
					coa1: coa1,
					coa2: coa2,
					coa3: coa3,
					coadesk: coa3desk,
					coa_reltype: coa,
					number: number
				},
				success : function(data) {
					if(data == "exist") {
						$("#msgadd").html("This data already exist");
					}
					else {
						var valadd = "('" + coa2 + "','" + coa3 + "','" + coa3desk + "')";
						number = number + 1;
						valtobeadd.push(valadd);
						$("#numbertoajax").val(number);
						$("#tobeadded").append(data);
						$("#query").val(valtobeadd.join());
						$("#coa3").val('');
						$("#coa3desk").val('');
						$("#msgadd").html("");
					}
				},
				error : function(){
					alert('Terjadi kesalahan dalam jaringan!\nTidak dapat menambah data!\nSilahkan cek koneksi jaringan dan coba lagi!');
					return false;
				}
			});
		}
		
	});

	$("#coa1").change(function(){
		var coa1 	 = $("#coa1 :selected").val();
		var coatype  = "coa2";

		if(coa1 == 0) {
			$("#coa2").html("<option disabled>Choose COA 2 First..</option>");
		}
		else {
			$.ajax({
				url : "ajax_select_coa.php",
				type : 'POST',
				data: { 
					coa: coa1,
					coa_reltype: coatype
				},
				success : function(data) {
					if(data == 0) {
						$("#coa2").html("<option disabled>There is no data for this COA</option>");
					}
					else {
						$("#coa2").html(data);
					}
				},
				error : function(){
					alert('Terjadi kesalahan dalam jaringan!\nTidak dapat menambah data!\nSilahkan cek koneksi jaringan dan coba lagi!');
					return false;
				}
			});
		}
	});

	$("#coa2").change(function(){
		var coa1 	= $("#coa1 :selected").val();
		var coa2 	= $("#coa2 :selected").val();
		var coatype = "coa2";

		$.ajax({
			url : "ajax_loadtable_relasi_coa.php",
			type : 'POST',
			data: { 
				coa1: coa1,
				coa2: coa2,
				coa_reltype: coatype
			},
			success : function(data) {
				$("#loadtable").html(data);
			},
			error : function(){
				alert('Terjadi kesalahan dalam jaringan!\nTidak dapat menambah data!\nSilahkan cek koneksi jaringan dan coba lagi!');
				return false;
			}
		});
	});

	$("#refreshtable").click(function(){
		var coatype = "coa3";

		$.ajax({
			url : "ajax_refresh_table.php",
			type : 'POST',
			data: { 
				coa_reltype: coatype
			},
			success : function(data) {
				$("#loadtable").html(data);
			},
			error : function(){
				alert('Terjadi kesalahan dalam jaringan!\nTidak dapat menambah data!\nSilahkan cek koneksi jaringan dan coba lagi!');
				return false;
			}
		});
	});
</script>

<?php 
$fun->setTitle_CP('Baronang');
$fun->setTitle('COA 3');
$fun->setDocTitle();
require('footer.php'); 
?>
