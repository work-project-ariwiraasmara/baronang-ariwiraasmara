<?php
require('header.php');
if( isset($_COOKIE['login']) ) { 
?>

	<div class="page-content">

		<div class="page-title page-title-small">
	        <h2 id="title"><a href="10master_coa.php" data-back-button><i class="fa fa-arrow-left"></i></a>Form Library</h2>
	        <a href="#" data-menu="menu-main" class="bg-fade-gray1-dark shadow-xl preload-img" data-src="../images/icon/logo-skyparking-50x50.png"></a>
	    </div>

	    <div class="card header-card shape-rounded" data-card-height="95">
	        <div class="card-overlay bg-highlight opacity-95"></div>
	        <div class="card-overlay dark-mode-tint"></div>
	        <div class="card-bg preload-img" data-src="../images/pictures/20s.jpg"></div>
	    </div>

		<div class="card card-style">
	        <div class="content mt-3 mb-3">

	        	<form action="" method="post">
	        	
	        		<?php /*
	        		<div class="tab-controls tabs-round tab-animated tabs-small tabs-rounded shadow-xl" 
                     	 data-tab-items="3" 
                     	 data-tab-active="bg-highlight activated color-white">

                     	<a href="#" data-tab="tab-detail" data-tab-active >Detail</a>
                    	<a href="#" data-tab="tab-debet">Debet</a>
                    	<a href="#" data-tab="tab-credit">Credit</a>

                    </div>
                    <div class="clearfix mb-3"></div>
                    */ ?>

                    <div class="Atab-content" id="tab-detail">

                    	<div class="input-style input-style-1 input-required mb-4">
							<span class="txt-black">Form Name</span>
							<em>(required)</em>
							<input type="text" name="formname" id="formname" class="txt-black" placeholder="Form Name">
						</div>

						<div class="">
							<span class="txt-black">Location</span>
							
							<ul id="myUL">
								<?php
								$sloc = "SELECT * from 10masterlocation where status='1'";
								$qloc = mysqli_query($koneksi, $sloc) or die(mysqli_error($koneksi));
								$rloc = mysqli_num_rows($qloc);
								if($rloc > 0) {
									while($dloc = mysqli_fetch_array($qloc)) { ?>
										<li><span class="caret"><?php echo $dloc['locationname']; ?></span>
											<ul class="nested">
												<?php
												$slib = "SELECT * from 01formlibrary where locationcode='".$dloc['locationcode']."'";
												$qlib = mysqli_query($koneksi, $slib) or die(mysqli_error($koneksi));
												while($dlib = mysqli_fetch_array($qlib)) { ?>
													<li><span class=""><?php echo $dlib['formname']; ?></span></li>
													<?php
												}
												?>
												<li>
													<div class="fac fac-radio fac-blue"><span></span>
							                            <input type="radio" name="locationcode" id="<?php echo 'add-'.$dloc['locationcode']; ?>" value="0">
							                            <label for="<?php echo 'add-'.$dloc['locationcode']; ?>"><i class="ion-android-add"></i> Add Location</label>
							                        </div>
												</li>
											</ul>
										</li>

										<script type="text/javascript">
											$("<?php echo '#add-'.$dloc['locationcode']; ?>").click(function(){
												var location = "<?php echo $dloc['locationcode']; ?>";
												var name 	 = "<?php echo $dloc['locationname']; ?>";

												if( $("<?php echo '#add-'.$dloc['locationcode']; ?>").is(":checked") ) {
													var strchoosen = "The location of " + name + " has been chosen";

													$("#location").val(location);
													$("#chooselocation").html("<span>" + strchoosen + "</span>");
													$("#chooselocation").removeClass("hide");
												}
												else {
													$("#location").val("");
													$("#chooselocation").html("");
													$("#chooselocation").addClass("hide");
												}
											});
										</script>
										<?php
									}
								}
								else { ?>
									<li><span class="caret">There are no location!</span></li>
									<?php
								}
								?>
								<li id="chooselocation" class="hide bold italic"></li>
							</ul>

							<div class="input-style input-style-1 input-required mb-4">
								<input type="text" name="location" id="location" class="hide" required readonly>
							</div>

							<?php /*
							<em><i class="fa fa-angle-down"></i></em>
							<select class="txt-black" name="location" id="location" required>
								<option value="" disabled>--Choose Location--</option>
								<?php
								$sloc = "SELECT * from 10masterlocation where status='1'";
								$qloc = mysqli_query($koneksi, $sloc) or die(mysqli_error($koneksi));
								$rloc = mysqli_num_rows($qloc);
								if($rloc > 0) {
									while($dloc = mysqli_fetch_array($qloc)) { ?>
										<option value="<?php echo $dloc['locationcode']; ?>"><?php echo $dloc['locationname']; ?></option>
										<?php
									}
								}
								else { ?>
									<option value="" disabled>There are no location!</option>
									<?php
								}
								?>
							</select>
							*/ ?>
						</div>

						<div class="input-style input-style-1 input-required">
							<span class="txt-black">User Level</span>
							<em>(required)</em>
							<input type="number" name="userlevel" id="userlevel" class="txt-black" placeholder="User Level">
						</div>

                    </div>

                    <div class="Atab-content mt-5" id="tab-debet" style="border-top: 1px solid #ccc;">
                    	<div class="mb-4 bold center"><span>Choose for Debet's Account</span></div>

                    	<?php /*
                    	<span class="txt-black">COA 1 :</span>
						<div class="input-style input-style-2 input-required">
							<em><i class="fa fa-angle-down"></i></em>
							<select class="txt-black" name="coa1-debet" id="coa1-debet" required>
								<option value="0">-Select Item-</option>
								<?php
								$scoa1 = "SELECT * from 11coa1 order by coa1 ASC";
								$qcoa1 = mysqli_query($koneksi, $scoa1) or die(mysqli_error($koneksi));
								$rcoa1 = mysqli_num_rows($qcoa1);
								if($rcoa1 > 0) {
									while($dcoa1 = mysqli_fetch_array($qcoa1)) { ?>
										<option value="<?php echo $dcoa1['coa1id']; ?>"><?php echo $dcoa1['coa1desc']; ?></option>
										<?php
									}
								}
								else { ?>
									<option value="" disabled>Data is Empty</option>
									<?php
								}							
								?>
							</select>
						</div>

						<span class="txt-black">COA 2 :</span>
						<div class="input-style input-style-2 input-required">
							<em><i class="fa fa-angle-down"></i></em>
							<select class="browser-default txt-black" name="coa2-debet" id="coa2-debet" required>
								<option value="0" disabled>Choose COA 1 First..</option>
							</select>
						</div>
						*/ ?>

						<span class="txt-black">COA 3 :</span>
						<div class="input-style input-style-2 input-required">
							<em><i class="fa fa-angle-down"></i></em>
							<select class="browser-default txt-black" name="coa3-debet" id="coa3-debet" required>
								<option value="" disabled selected>-- Choose Account --</option>
								<?php
								// <option value="0" disabled>Choose COA 2 First..</option>
								$slibcoadebet = "SELECT * from 13coa3 order by coa3desc";
								$qlibcoadebet = mysqli_query($koneksi, $slibcoadebet) or die(mysqli_error($koneksi));
								while($dlibcoadebet = mysqli_fetch_array($qlibcoadebet)) { ?>
									<option value="<?php echo $dlibcoadebet['coa3id'] ?>"><?php echo $dlibcoadebet['coa3desc'] ?></option>
									<?php
								}
								?>
							</select>
						</div>

						<table class="mt-4 table table-borderless rounded-sm shadow-l" style="overflow: hidden;">
							<thead>
								<tr class="bg-gray1-dark">
									<th scope="col" class="color-theme">COA 4</th>
									<th scope="col" class="color-theme">COA 4 Description</th>
									<th scope="col" class="color-theme">
										<div class="fac fac-checkbox fac-default"><span></span>
				                            <input type="checkbox" id="selectall-debet" value="0">
				                            <label for="selectall-debet">All</label>
				                        </div>
									</th>
								</tr>
							</thead>

							<tbody id="debet-loadtable">
								<tr>
									<td colspan="3" class="center"><span>Chooser COA 1 to 3 First..</span></td>
								</tr>
								<?php
								/*
								$no = 1;
								$scoa 	= "SELECT * from 14coa4 as a order by coa3id, coa4 ASC";
								$qcoa = mysqli_query($koneksi, $scoa) or die(mysqli_error($koneksi));
								while($dcoa = mysqli_fetch_array($qcoa)) { 

									$scoa3 = "SELECT * from 13coa3 where coa3id='".$dcoa['coa3id']."'";
									$qcoa3 = mysqli_query($koneksi, $scoa3) or die(mysqli_error($koneksi));
									$dcoa3 = mysqli_fetch_array($qcoa3);

									$scoa2 = "SELECT * from 12coa2 where coa2id='".$dcoa3['coa2id']."'";
									$qcoa2 = mysqli_query($koneksi, $scoa2) or die(mysqli_error($koneksi));
									$dcoa2 = mysqli_fetch_array($qcoa2);

									$scoa1 = "SELECT * from 11coa1 where coa1id='".$dcoa2['coa1id']."'";
									$qcoa1 = mysqli_query($koneksi, $scoa1) or die(mysqli_error($koneksi));
									$dcoa1 = mysqli_fetch_array($qcoa1);

									?>
									<tr>
										<td><span><?php echo $dcoa1['coa1'].'-'.$dcoa2['coa2'].'-'.$dcoa3['coa3'].'-'.$dcoa['coa4']; ?></span></td>
										<td><span><?php echo $dcoa['coa4desc']; ?></span></td>
									</tr>
									<?php
									$no++;
								}
								*/
								?>
							</tbody>
						</table>

						<div class="input-style input-style-1 input-required">
							<input type="text" name="debetcoa-query" id="debetcoa-query" class="txt-black" readonly>
						</div>
                    </div>

                    <div class="Atab-content mt-5" id="tab-credit" style="border-top: 1px solid #ccc;">
                    	<div class="mb-4 bold center"><span>Choose for Credits's Account</span></div>

                    	<?php /*
                    	<span class="txt-black">COA 1 :</span>
						<div class="input-style input-style-2 input-required">
							<em><i class="fa fa-angle-down"></i></em>
							<select class="txt-black" name="coa1-credit" id="coa1-credit" required>
								<option value="0">-Select Item-</option>
								<?php
								$scoa1 = "SELECT * from 11coa1 order by coa1 ASC";
								$qcoa1 = mysqli_query($koneksi, $scoa1) or die(mysqli_error($koneksi));
								$rcoa1 = mysqli_num_rows($qcoa1);
								if($rcoa1 > 0) {
									while($dcoa1 = mysqli_fetch_array($qcoa1)) { ?>
										<option value="<?php echo $dcoa1['coa1id']; ?>"><?php echo $dcoa1['coa1desc']; ?></option>
										<?php
									}
								}
								else { ?>
									<option value="" disabled>Data is Empty</option>
									<?php
								}							
								?>
							</select>
						</div>

						<span class="txt-black">COA 2 :</span>
						<div class="input-style input-style-2 input-required">
							<em><i class="fa fa-angle-down"></i></em>
							<select class="browser-default txt-black" name="coa2-credit" id="coa2-credit" required>
								<option value="0" disabled>Choose COA 1 First..</option>
							</select>
						</div>
						*/ ?>

						<span class="txt-black">COA 3 :</span>
						<div class="input-style input-style-2 input-required">
							<em><i class="fa fa-angle-down"></i></em>
							<select class="browser-default txt-black" name="coa3-credit" id="coa3-credit" required>
								<option value="" disabled selected>-- Choose Account --</option>
								<?php
								// <option value="0" disabled>Choose COA 2 First..</option>
								$slibcoacredit = "SELECT * from 13coa3 order by coa3desc";
								$qlibcoacredit = mysqli_query($koneksi, $slibcoacredit) or die(mysqli_error($koneksi));
								while($dlibcoacredit = mysqli_fetch_array($qlibcoacredit)) { ?>
									<option value="<?php echo $dlibcoacredit['coa3id'] ?>"><?php echo $dlibcoacredit['coa3desc'] ?></option>
									<?php
								}
								?>
							</select>
						</div>

						<table class="mt-4 table table-borderless rounded-sm shadow-l" style="overflow: hidden;">
							<thead>
								<tr class="bg-gray1-dark">
									<th scope="col" class="color-theme">COA 4</th>
									<th scope="col" class="color-theme">COA 4 Description</th>
									<th scope="col" class="color-theme">
										<div class="fac fac-checkbox fac-default"><span></span>
				                            <input type="checkbox" id="selectall-credit" value="0">
				                            <label for="selectall-credit">All</label>
				                        </div>
									</th>
								</tr>
							</thead>

							<tbody id="credit-loadtable">
								<tr>
									<td colspan="3" class="center"><span>Chooser COA 1 to 3 First..</span></td>
								</tr>
								<?php
								/*
								$no = 1;
								$scoa 	= "SELECT * from 14coa4 as a order by coa3id, coa4 ASC";
								$qcoa = mysqli_query($koneksi, $scoa) or die(mysqli_error($koneksi));
								while($dcoa = mysqli_fetch_array($qcoa)) { 

									$scoa3 = "SELECT * from 13coa3 where coa3id='".$dcoa['coa3id']."'";
									$qcoa3 = mysqli_query($koneksi, $scoa3) or die(mysqli_error($koneksi));
									$dcoa3 = mysqli_fetch_array($qcoa3);

									$scoa2 = "SELECT * from 12coa2 where coa2id='".$dcoa3['coa2id']."'";
									$qcoa2 = mysqli_query($koneksi, $scoa2) or die(mysqli_error($koneksi));
									$dcoa2 = mysqli_fetch_array($qcoa2);

									$scoa1 = "SELECT * from 11coa1 where coa1id='".$dcoa2['coa1id']."'";
									$qcoa1 = mysqli_query($koneksi, $scoa1) or die(mysqli_error($koneksi));
									$dcoa1 = mysqli_fetch_array($qcoa1);

									?>
									<tr>
										<td><span><?php echo $dcoa1['coa1'].'-'.$dcoa2['coa2'].'-'.$dcoa3['coa3'].'-'.$dcoa['coa4']; ?></span></td>
										<td><span><?php echo $dcoa['coa4desc']; ?></span></td>
									</tr>
									<?php
									$no++;
								}
								*/
								?>
							</tbody>
						</table>

						<div class="input-style input-style-1 input-required">
							<input type="text" name="creditcoa-query" id="creditcoa-query" class="txt-black" readonly>
						</div>
                    </div>

                    <button type="submit" name="confirm" class="btn btn-m btn-full mb-2 mt-5 rounded-xl text-uppercase font-900 shadow-s bg-highlight width-100">Save</button>

	        	</form>

	        	<?php
	        	if(isset($_POST['confirm'])) {
	        		$name 		= @$_POST['formname'];
					$location 	= @$_POST['location'];
					$debet 		= str_replace(',', '', substr(@$_POST['debetcoa-query'], 1));
					$credit 	= str_replace(',', '', substr(@$_POST['creditcoa-query'], 1));
					$userlevel	= @$_POST['userlevel'];

					if( $name == '' 	 || empty($name) ||
						$location == ''  || empty($location) ||
						$debet == '' 	 || empty($debet) ||
						$credit == '' 	 || empty($credit) ||
						$userlevel == '' || empty($userlevel) ) { ?>
							<div class="center bold italic mt-5">
								Form Name / Location / Debet's Account / Credit's Account / User Level can't be empty
							</div>
						<?php
					}
					else {

						$sql = "INSERT into 01formlibrary(formname, locationcode, debet, credit, userlevel, status) values ('$name', '$location', '$debet', '$credit', '$userlevel', 1)";
						$qry = mysqli_query($koneksi, $sql) or die(mysqli_error($koneksi));
						if($qry) { ?>
							<div class="center bold italic mt-5">
								Success saving new data Form Library
							</div>
							<?php
						}

					}
	        	}
	        	?>

	        </div>
	    </div>

	</div>

	<script type="text/javascript">
		var valtobeadd_debet  = new Array();
		var valtobeadd_kredit = new Array();

		// DEBET
		/*
		$("#coa1-debet").change(function(){
			var coa1 	 = $("#coa1-debet :selected").val();
			var coatype  = "coa2";

			if(coa1 == 0) {
				$("#coa2-debet").html("<option disabled>Choose COA 1 First..</option>");
				$("#coa3-debet").html("<option disabled>Choose COA 2 First..</option>");
			}
			else {
				$.ajax({
					url : "ajax_select_coa.php",
					type : 'POST',
					data: { 
						coa: coa1,
						coa_reltype: coatype
					},
					success : function(data) {
						if(data == 0) {
							$("#coa2-debet").html("<option disabled>There is no data for this COA</option>");
							$("#coa3-debet").html("<option disabled>There is no data for this COA</option>");
						}
						else {
							$("#coa2-debet").html(data);
							$("#coa3-debet").html("<option disabled>Choose COA 2 First..</option>");
						}
					},
					error : function(){
						alert('Terjadi kesalahan dalam jaringan!\nTidak dapat menambah data!\nSilahkan cek koneksi jaringan dan coba lagi!');
						return false;
					}
				});
			}
		});

		$("#coa2-debet").change(function(){
			var coa2 	= $("#coa2-debet :selected").val();
			var coatype = "coa3";

			if(coa2 == 0) {
				$("#coa3-debet").html("<option disabled>Choose COA 2 First..</option>");
			}
			else {
				$.ajax({
					url : "ajax_select_coa.php",
					type : 'POST',
					data: { 
						coa: coa2,
						coa_reltype: coatype
					},
					success : function(data) {
						if(data == 0) {
							$("#coa3-debet").html('<option disabled>There is no data for this COA</option>');
						}
						else {
							$("#coa3-debet").html(data);
						}
					},
					error : function(){
						alert('Terjadi kesalahan dalam jaringan!\nTidak dapat menambah data!\nSilahkan cek koneksi jaringan dan coba lagi!');
						return false;
					}
				});
			}
		});
		*/

		$("#coa3-debet").change(function(){
			//var coa1 	= $("#coa1-debet :selected").val();
			//var coa2 	= $("#coa2-debet :selected").val();
			var coa3 	= $("#coa3-debet :selected").val();
			var coatype = "coa3";
			var dc 		= "debet";

			$.ajax({
				url : "ajax_loadtable_relasi_coa_formlib.php",
				type : 'POST',
				data: { 
					//coa1: coa1,
					//coa2: coa2,
					coa3: coa3,
					dc: dc,
					coa_reltype: coatype
				},
				success : function(data) {
					//console.log(data);
					$("#debet-loadtable").html(data);
				},
				error : function(){
					alert('Terjadi kesalahan dalam jaringan!\nTidak dapat menambah data!\nSilahkan cek koneksi jaringan dan coba lagi!');
					return false;
				}
			});
		});

		// CREDIT
		/*
		$("#coa1-credit").change(function(){
			var coa1 	 = $("#coa1-credit :selected").val();
			var coatype  = "coa2";

			if(coa1 == 0) {
				$("#coa2-credit").html("<option disabled>Choose COA 1 First..</option>");
				$("#coa3-credit").html("<option disabled>Choose COA 2 First..</option>");
			}
			else {
				$.ajax({
					url : "ajax_select_coa.php",
					type : 'POST',
					data: { 
						coa: coa1,
						coa_reltype: coatype
					},
					success : function(data) {
						if(data == 0) {
							$("#coa2-credit").html("<option disabled>There is no data for this COA</option>");
							$("#coa3-credit").html("<option disabled>There is no data for this COA</option>");
						}
						else {
							$("#coa2-credit").html(data);
							$("#coa3-credit").html("<option disabled>Choose COA 2 First..</option>");
						}
					},
					error : function(){
						alert('Terjadi kesalahan dalam jaringan!\nTidak dapat menambah data!\nSilahkan cek koneksi jaringan dan coba lagi!');
						return false;
					}
				});
			}
		});

		$("#coa2-credit").change(function(){
			var coa2 	= $("#coa2-credit :selected").val();
			var coatype = "coa3";

			if(coa2 == 0) {
				$("#coa3-credit").html("<option disabled>Choose COA 2 First..</option>");
			}
			else {
				$.ajax({
					url : "ajax_select_coa.php",
					type : 'POST',
					data: { 
						coa: coa2,
						coa_reltype: coatype
					},
					success : function(data) {
						if(data == 0) {
							$("#coa3-credit").html('<option disabled>There is no data for this COA</option>');
						}
						else {
							$("#coa3-credit").html(data);
						}
					},
					error : function(){
						alert('Terjadi kesalahan dalam jaringan!\nTidak dapat menambah data!\nSilahkan cek koneksi jaringan dan coba lagi!');
						return false;
					}
				});
			}
		});
		*/

		$("#coa3-credit").change(function(){
			//var coa1 	= $("#coa1-credit :selected").val();
			//var coa2 	= $("#coa2-credit :selected").val();
			var coa3 	= $("#coa3-credit :selected").val();
			var coatype = "coa3";
			var dc 		= "credit";

			$.ajax({
				url : "ajax_loadtable_relasi_coa_formlib.php",
				type : 'POST',
				data: { 
					//coa1: coa1,
					//coa2: coa2,
					coa3: coa3,
					dc: dc,
					coa_reltype: coatype
				},
				success : function(data) {
					//console.log(data);
					$("#credit-loadtable").html(data);
				},
				error : function(){
					alert('Terjadi kesalahan dalam jaringan!\nTidak dapat menambah data!\nSilahkan cek koneksi jaringan dan coba lagi!');
					return false;
				}
			});
		});

		$("#selectall-debet").click(function(){
			var coa3 = $("#coa3-debet :selected").val();
			$("input:checkbox").not(this).prop("checked", this.checked);

			var checkedValues = $('input:checkbox:checked').map(function() {
				    				return this.value;
								}).get();
			
			
			if( $("#selectall-debet").is(':checked') ) {
				valtobeadd_debet = checkedValues;
				var vallength = parseInt(valtobeadd_debet.length) - 1;
				valtobeadd_debet.splice(vallength, 1);
				valtobeadd_debet.splice(0, 1);

				$("<?php echo '#debetcoa-query'; ?>").val(valtobeadd_debet.join());
			}
			else {
				valtobeadd_debet = null;
				$("<?php echo '#debetcoa-query'; ?>").val("");
			}

			//console.log(valtobeadd_debet.join());
		});

		$("#selectall-credit").click(function(){
			var coa3 = $("#coa3-credit :selected").val();
			$("input:checkbox").not(this).prop("checked", this.checked);

			var checkedValues = $('input:checkbox:checked').map(function() {
				    				return this.value;
								}).get();
			
			if( $("#selectall-credit").is(':checked') ) {
				valtobeadd_kredit = checkedValues;
				var vallength = parseInt(valtobeadd_kredit.length);
				valtobeadd_kredit.splice(vallength, 1);
				valtobeadd_kredit.splice(0, 1);

				$("<?php echo '#creditcoa-query'; ?>").val(valtobeadd_kredit.join());
			}
			else {
				valtobeadd_kredit = null;
				$("<?php echo '#creditcoa-query'; ?>").val("");
			}

			//console.log(valtobeadd_kredit.join());
		});

		//
		var toggler = document.getElementsByClassName("caret");
		var i;

		for (i = 0; i < toggler.length; i++) {
			toggler[i].addEventListener("click", function() {
				this.parentElement.querySelector(".nested").classList.toggle("active");
		    	this.classList.toggle("caret-down");
			});
		}
	</script>

<?php 
}
require('footer.php');
$fun->setTitle('Form Library');
$fun->setTitle_CP('Baronang');
$fun->setDocTitle();
?>