<?php
require('header.php');
if( isset($_COOKIE['login']) ) {
	$sfilt = "SELECT * from 02masteruser where usercode='".@$_COOKIE['ucode']."'";
	//echo $sfilt;
	$qfilt = mysqli_query($koneksi, $sfilt) or die(mysqli_error($koneksi));
	$dfilt = mysqli_fetch_array($qfilt);
	?>

	<div class="page-content">

		<div class="page-title page-title-small">
	        <h2>Home</h2>
	        <a href="#" data-menu="menu-main" class="bg-fade-gray1-dark shadow-xl preload-img" data-src="images/icon/logo-skyparking-50x50.png"></a>
	    </div>

	    <div class="card header-card shape-rounded" data-card-height="95">
	        <div class="card-overlay bg-highlight opacity-95"></div>
	        <div class="card-overlay dark-mode-tint"></div>
	        <div class="card-bg preload-img" data-src="images/pictures/20s.jpg"></div>
	    </div>

		<div class="row text-center mb-3">

			<?php
			if( $dfilt['menu1'] == 1 ) { ?>
				<a href="page/01admin.php" class="col-6 pr-2">
		            <div class="card card-style mr-0  pt-4 mb-3">
		                <h1 class="center-text pt-4">
		                    <i class="fa fa-building fa-3x"></i>
		                </h1>
		                <h4 class="color-theme font-600">Admin</h4>
		                <p class="mt-n2 font-11 color-highlight">

		                </p>
		                <p class="font-10 opacity-30 mb-1">Tap to View</p>
		            </div>
		        </a>
				<?php
			}
				    
			if( $dfilt['menu2'] == 1 ) { ?>
				<a href="#" class="col-6 pr-2">
		            <div class="card card-style mr-0  pt-4 mb-3">
		                <h1 class="center-text pt-4">
		                    <i class="fa fa-building fa-3x"></i>
		                </h1>
		                <h4 class="color-theme font-600">Master COA</h4>
		                <p class="mt-n2 font-11 color-highlight">

		                </p>
		                <p class="font-10 opacity-30 mb-1">Tap to View</p>
		            </div>
		        </a>
				<?php
			}
				    
			if( $dfilt['menu3'] == 1 ) { ?>
				<a href="?pg=location" class="col-6 pr-2">
		            <div class="card card-style mr-0  pt-4 mb-3">
		                <h1 class="center-text pt-4">
		                    <i class="fa fa-building fa-3x"></i>
		                </h1>
		                <h4 class="color-theme font-600">Entry Journal</h4>
		                <p class="mt-n2 font-11 color-highlight">

		                </p>
		                <p class="font-10 opacity-30 mb-1">Tap to View</p>
		            </div>
		        </a>
		        <?php
			}
			?>

	        <?php
	        //if( $dfilt['menu1'] == '1' ) { require('page/01admin.php'); }
	        //if( $dfilt['menu2'] == '1' ) { require('page/10master_coa.php'); }
	        //if( $dfilt['menu3'] == '1' ) { require('page/20entry_journal.php'); }
			?>

		</div>

	</div>

<?php
}
require('footer.php');
$fun->setTitle('Home');
$fun->setTitle_CP('Baronang');
$fun->setDocTitle();
?>