<?php
require('header.php');
if( isset($_COOKIE['login']) ) { 
?>

	<div class="page-content">

		<div class="page-title page-title-small">
	        <h2><a href="../?" data-back-button><i class="fa fa-arrow-left"></i></a>Master COA</h2>
	        <a href="#" data-menu="menu-main" class="bg-fade-gray1-dark shadow-xl preload-img" data-src="../images/icon/logo-skyparking-50x50.png"></a>
	    </div>

	    <div class="card header-card shape-rounded" data-card-height="95">
	        <div class="card-overlay bg-highlight opacity-95"></div>
	        <div class="card-overlay dark-mode-tint"></div>
	        <div class="card-bg preload-img" data-src="../images/pictures/20s.jpg"></div>
	    </div>

	    <div class="card card-style">
	        <div class="content mt-3 mb-3">
	    	
	        	<div class="d-flex justify-content-between">
				    <div class="pl-3 ml-1 align-self-center">
				    	<a href="12coa2.php">COA 2</a>
				    </div>
				</div>

				<div class="d-flex justify-content-between">
				    <div class="pl-3 ml-1 align-self-center">
				    	<a href="13coa3.php">COA 3</a>
				    </div>
				</div>

				<div class="d-flex justify-content-between">
				    <div class="pl-3 ml-1 align-self-center">
				    	<a href="14coa4.php">COA 4</a>
				    </div>
				</div>

	    	</div>
	    </div>

	</div>

<?php 
}
require('footer.php');
$fun->setTitle('Master COA');
$fun->setTitle_CP('Baronang');
$fun->setDocTitle();
?>