<?php 
require('header.php');
if( isset($_COOKIE['login']) ) { 
$sform = "SELECT * from 01formlibrary where id = '".$fun->getIDParam('formid')."'";
$qform = mysqli_query($koneksi, $sform) or die(mysqli_error($koneksi));
$dform = mysqli_fetch_array($qform);
$d = $dform['debet'];
$c = $dform['credit'];	
$loc = $dform['locationcode'];	
?>

	<div class="page-content">

		<div class="page-title page-title-small">
	        <h2><a href="20entry_journal.php" data-back-button><i class="fa fa-arrow-left"></i></a><?php echo $dform['formname']; ?></h2>
	        <a href="#" data-menu="menu-main" class="bg-fade-gray1-dark shadow-xl preload-img" data-src="../images/icon/logo-skyparking-50x50.png"></a>
	    </div>

	    <div class="card header-card shape-rounded" data-card-height="95">
	        <div class="card-overlay bg-highlight opacity-95"></div>
	        <div class="card-overlay dark-mode-tint"></div>
	        <div class="card-bg preload-img" data-src="../images/pictures/20s.jpg"></div>
	    </div>

		<div class="card card-style">
	        <div class="content mt-3 mb-3">

				<form action="" method="post">

					<div class="input-style input-style-1 input-required">
						<span class="txt-black">Description</span>
						<em>(required)</em>
						<input type="text" name="desk" id="desk" class="txt-black" placeholder="Description" required>
					</div>

					<div class="row">
						<div class="col-12">
							<span class="bold">Debet</span>
						</div>

						<div class="col-12">
							<div class="input-style input-style-2 input-required">
								<select class="txt-black" name="debet" id="debet">
									<option value="">--Choose Debet COA--</option>
									<?php
									$sdebet = "SELECT * from (SELECT 
											CONCAT(d.coa1, c.coa2, b.coa3, a.coa4) AS KodeCOA,
											a.coa4desc as desk
											from accounting.14coa4 as a 
											inner join accounting.13coa3 as b on a.coa3id = b.coa3id
											inner join accounting.12coa2 as c on b.coa2id = c.coa2id
											inner join accounting.11coa1 as d on c.coa1id = d.coa1id) as e
											where e.KodeCOA regexp '$d' order by e.KodeCOA";
									//echo $sdebet;
									$qdebet = mysqli_query($koneksi, $sdebet) or die(mysqli_error($koneksi));
									while($debet = mysqli_fetch_array($qdebet)) { ?>
										<option value="<?php echo $debet['KodeCOA']; ?>"><?php echo $debet['desk']; ?></option>
										<?php
									}
									?>
								</select>
							</div>
						</div>

						<div class="col-11">
							<div class="input-style input-style-1 input-required">
								<span class="txt-black">Amount</span>
								<em>(required)</em>
								<input type="number" name="amountdebet" id="amountdebet" class="txt-black" placeholder="Amount">
							</div>
						</div>

						<div class="col-1">
							<div  class="right mt-3 mr-3">
								<span id="btnadddebet"><i class="ion-android-add"></i></span>
							</div>
						</div>
					</div>

					<div style="border-bottom: 1px solid #ccc;"></div>

					<div class="row mt-5">
						<div class="col s12">
							<span class="bold">Kredit</span>
						</div>

						<div class="col-12">
							<div class="input-style input-style-2 input-required">
								<select class="txt-black" name="kredit" id="kredit">
									<option value="">--Choose Credit COA--</option>
									<?php
									$skredit = "SELECT * from (SELECT 
											CONCAT(d.coa1, c.coa2, b.coa3, a.coa4) AS KodeCOA,
											a.coa4desc as desk
											from accounting.14coa4 as a 
											inner join accounting.13coa3 as b on a.coa3id = b.coa3id
											inner join accounting.12coa2 as c on b.coa2id = c.coa2id
											inner join accounting.11coa1 as d on c.coa1id = d.coa1id) as e
											where e.KodeCOA regexp '$c' order by e.KodeCOA";
									//echo $skredit;
									$qkredit = mysqli_query($koneksi, $skredit) or die(mysqli_error($koneksi));
									while($kredit = mysqli_fetch_array($qkredit)) { ?>
										<option value="<?php echo $kredit['KodeCOA']; ?>"><?php echo $kredit['desk']; ?></option>
										<?php	
									}
									?>
								</select>
							</div>
						</div>

						<div class="col-11">
							<div class="input-style input-style-1 input-required">
								<span class="txt-black">Amount</span>
								<em>(required)</em>
								<input type="number" name="amountkredit" id="amountkredit" class="txt-black" placeholder="Amount">
							</div>
						</div>

						<div class="col-1">
							<div  class="right mt-3 mr-3">
								<span id="btnaddkredit"><i class="ion-android-add"></i></span>
							</div>
						</div>
					</div>

					<input type="text" name="numbertoajax" id="numbertoajax" value="0" class="hide">

					<div class="input-style input-style-1 input-required">
						<input type="text" name="query" id="query" value="" class="" readonly>
					</div>
					
					<input type="number" name="totdebet"  id="totdebet" class="hide"value="0" readonly>

					<input type="number" name="totkredit" id="totkredit" class="hide"value="0" readonly>

					<div class="bold italic center" id="msgadd"></div>

					<div class="m-t-30">
						<table class="table table-borderless rounded-sm shadow-l" style="overflow: hidden;">
							<thead>
								<tr class="bg-gray1-dark">
									<th scope="col" class="color-theme">COA</th>
									<th scope="col" class="color-theme">Debet</th>
									<th scope="col" class="color-theme">Kredit</th>
									<th></th>
								</tr>
							</thead>

							<tbody id="tobeadded">
								
							</tbody>

							<tfoot>
								<tr>
									<th colspan="1"></th>
									<th style="text-align: right;" id="restotdebet">0</th>
									<th style="text-align: right;" id="restotkredit">0</th>
									<th></th>
								</tr>
							</tfoot>
						</table>
					</div>

					<button type="submit" name="confirm" id="confirm" class="btn btn-m btn-full mb-2 mt-4 rounded-xl text-uppercase font-900 shadow-s bg-highlight width-100" disabled>Confirm</button>
				</form>

				<?php
				if(isset($_POST['confirm'])) {
					$desk = @$_POST['desk'];

					if($desk == '' || empty($desk)) { ?>
						<script type="text/javascript">
							$("#msgadd").html("Description can't be empty!");
						</script>
						<?php
					}
					else {
						$qval = @$_POST['query'];
					
						$findlikeid = $loc.'-01-'.date('Y-m');
						$date = date('Y-m-d H:i:s');

						$scoa1 = "SELECT journalmastercode from 211journalmaster where journalmastercode like '$findlikeid%' order by journalmastercode desc limit 1";
						$qcoa1 = mysqli_query($koneksi, $scoa1) or die(mysqli_error($koneksi));
						$rcoa1 = mysqli_num_rows($qcoa1);
						if($rcoa1 > 0) {
							$dcoa1 = mysqli_fetch_array($qcoa1);
							$id1 = (int)substr($dcoa1['journalmastercode'], -5) + 1;
							$id2 = str_pad($id1, 5, '0', STR_PAD_LEFT);
							$masterid = $loc.'-01-'.date('Y-m').'-'.$id2;
						}
						else {
							$masterid = $loc.'-01-'.date('Y-m').'-00001';
						}

						$qvalrid = str_replace("masterid", $masterid, $qval);

						$scoa2 = "INSERT into 211journalmaster values('','$masterid', '$date', '$desk', '".@$_COOKIE['ucode']."', '$loc')";
						echo $scoa2.'<br>';
						$qcoa2 = mysqli_query($koneksi, $scoa2) or die(mysqli_error($koneksi));

						$scoa3 = "INSERT into 212journaldetail(journalmastercode, coadebet, coakredit, amountdebet, amountcredit) values $qvalrid";
						echo $scoa3.'<br>';
						$qcoa3 = mysqli_query($koneksi, $scoa3) or die(mysqli_error($koneksi));

						if($qcoa2 && $qcoa3) { ?>
							<script type="text/javascript">
								window.location.href = "?";
							</script>
							<?php
						}
					}
				}
				?>

				<div class="mt-1"><hr>
					<table class="table table-borderless rounded-sm shadow-l" style="overflow: hidden;">
						<thead>
							<tr class="bg-gray1-dark">
								<th scope="col" class="color-theme">Nama COA</th>
								<th scope="col" class="color-theme">Debet (Rp.)</th>
								<th scope="col" class="color-theme">Kredit (Rp.)</th>
							</tr>
						</thead>

						<tbody>
							<?php
							$totdebet = 0; $totkredit = 0;
							$sjdt = "SELECT * from journalaccounting";
							$qjdt = mysqli_query($koneksi, $sjdt) or die(mysqli_error($koneksi));
							while($djdt = mysqli_fetch_array($qjdt)) { ?>
								<tr>
									<td></td>
									<td style="text-align: right;"><?php $fun->FormatNumber($djdt['debet']); ?></td>
									<td style="text-align: right;"><?php $fun->FormatNumber($djdt['kredit']); ?></td>
								</tr>
								<?php
								$totdebet  = (int)$totdebet  + (int)$djdt['debet'];
								$totkredit = (int)$totkredit + (int)$djdt['kredit'];
							}
							?>
						</tbody>
					</table>
				</div>

			</div>
		</div>

	</div>

<script type="text/javascript">
	var valtobeadd = new Array();

	$("#btnaddkredit").click(function(){
		var coa 		= $("#kredit :selected").val();
		var desk 		= $("#desk").val();
		var tipe 		= "kredit";
		var amount 		= $("#amountkredit").val();
		var number 		= $("#numbertoajax").val();
		var totkredit	= $("#totkredit").val();
		var totdebet 	= $("#totdebet").val();
		var restotal 	= 0;

		if( (coa == "") || (amount == "") ) {
			$("#msgadd").html("Credit COA , Credit Amount can't be empty! Fill COA and Amount first!");
		}
		else {
			$.ajax({
				url : "ajax_form_add_coa.php",
				type : 'POST',
				data: { 
					coakredit: coa,
					coadebet: 0,
					desk: desk,
					tipe: tipe,
					amount: amount,
					number: number
				},
				success : function(data) {
					if(data == "exist") {
						$("#msgadd").html("This data already exist");
					}
					else {
						var valadd = "('masterid','0','" + coa + "','0','" + amount + "')";
						number = parseInt(number) + 1;
						valtobeadd.push(valadd);
						$("#numbertoajax").val(number);
						$("#tobeadded").append(data);
						$("#query").val(valtobeadd.join());
						$("#desk").val('');
						$("#amountkredit").val('');
						$("#msgadd").html("");

						restotal = parseInt(totkredit) + parseInt(amount);
						var fintotal = new Number(restotal.toFixed(0)).toLocaleString("id-ID");
						$("#totkredit").val(restotal);
						$("#restotkredit").html(fintotal);

						if(totdebet == restotal) {
							$("#confirm").attr("disabled", false);
						}
						else {
							$("#confirm").attr("disabled", true);
						}

						var index = valtobeadd.indexOf(valadd);

						console.log("<===== ARRAY TO BE ADDED =====>");
						console.log("Valadd :=> " + valadd);
						console.log("Index Valadd :=> " + index);
					}	
				},
				error : function(){
					alert('Terjadi kesalahan dalam jaringan!\nTidak dapat menambah data!\nSilahkan cek koneksi jaringan dan coba lagi!');
					return false;
				}
			});
		}
	});

	$("#btnadddebet").click(function(){
		var coa 		= $("#debet :selected").val();
		var desk 		= $("#desk").val();
		var tipe 		= "debet";
		var amount 		= $("#amountdebet").val();
		var number 		= $("#numbertoajax").val();
		var totkredit	= $("#totkredit").val();
		var totdebet 	= $("#totdebet").val();
		var restotal 	= 0;

		if( (coa == "") || (amount == "") ) {
			$("#msgadd").html("Credit COA , Credit Amount can't be empty! Fill COA and Amount first!");
		}
		else {
			$.ajax({
				url : "ajax_form_add_coa.php",
				type : 'POST',
				data: { 
					coadebet: coa,
					coakredit: 0,
					desk: desk,
					tipe: tipe,
					amount: amount,
					number: number
				},
				success : function(data) {
					if(data == "exist") {
						$("#msgadd").html("This data already exist");
					}
					else {
						var valadd = "('masterid','" + coa + "','0','" + amount + "','0')";
						number = parseInt(number) + 1;
						valtobeadd.push(valadd);
						$("#numbertoajax").val(number);
						$("#tobeadded").append(data);
						$("#query").val(valtobeadd.join());
						$("#desk").val('');
						$("#amountdebet").val('');
						$("#msgadd").html("");

						restotal = parseInt(totdebet) + parseInt(amount);
						var fintotal = new Number(restotal.toFixed(0)).toLocaleString("id-ID");
						$("#totdebet").val(restotal);
						$("#restotdebet").html(fintotal);

						if(totkredit == restotal) {
							$("#confirm").attr("disabled", false);
						}
						else {
							$("#confirm").attr("disabled", true);
						}

						var index = valtobeadd.indexOf(valadd);

						console.log("<===== ARRAY TO BE ADDED =====>");
						console.log("Valadd :=> " + valadd);
						console.log("Index Valadd :=> " + index);
					}	
				},
				error : function(){
					alert('Terjadi kesalahan dalam jaringan!\nTidak dapat menambah data!\nSilahkan cek koneksi jaringan dan coba lagi!');
					return false;
				}
			});
		}
	});
</script>

<?php 
}
require('footer.php');
$fun->setTitle($dform['formname']);
$fun->setTitle_CP('Baronang');
$fun->setDocTitle();
?>