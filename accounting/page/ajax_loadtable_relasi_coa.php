<?php
require('../koneksi.php');

$coa_reltype = @$_POST['coa_reltype'];
$fieldid 	 = $coa_reltype;

$coa1 		 = @$_POST['coa1'];
$coa2 		 = @$_POST['coa2'];
$coa3 		 = @$_POST['coa3'];
$coa4 		 = @$_POST['coa4'];

if($coa_reltype == 'coa3') {
	$sql = "SELECT 	a.coa4 as coa4, a.coa4desc as desk, 
					b.coa3 as coa3, c.coa2 as coa2, d.coa1 as coa1
			from 14coa4 as a 
			inner join 13coa3 as b 
				on a.coa3id = b.coa3id
			inner join 12coa2 as c
				on b.coa2id = c.coa2id
			inner join 11coa1 as d
				on c.coa1id = d.coa1id
			where d.coa1id='$coa1' and c.coa2id='$coa2' and b.coa3id='$coa3' order by d.coa1 ASC, c.coa2 ASC, b.coa3 ASC";
}
else if($coa_reltype == 'coa2') {
	$sql = "SELECT 	a.coa3 as coa3, a.coa3desc as desk, 
					b.coa2 as coa2, c.coa1 as coa1
			from 13coa3 as a 
			inner join 12coa2 as b 
				on a.coa2id = b.coa2id
			inner join 11coa1 as c
				on b.coa1id = c.coa1id
			where c.coa1id='$coa1' and b.coa2id='$coa2' order by c.coa1 ASC, b.coa2 ASC";
}
else {
	$sql = "SELECT 	a.coa2 as coa2, a.coa2desc as desk, 
					b.coa1 as coa1
			from 12coa2 as a 
			inner join 11coa1 as b 
				on a.coa1id = b.coa1id
			where b.coa1id='$coa1' order by b.coa1 ASC";
}
$no = 1;
$query = mysqli_query($koneksi, $sql) or die(mysqli_error($koneksi));
while($data = mysqli_fetch_array($query)) { 
	if($coa_reltype == 'coa3') {
		$coasid  = $data['coa1'].'-'.$data['coa2'].'-'.$data['coa3'].'-'.$data['coa4'];
	}
	else if($coa_reltype == 'coa2'){
		$coasid  = $data['coa1'].'-'.$data['coa2'].'-'.$data['coa3'];
	}
	else {
		$coasid  = $data['coa1'].'-'.$data['coa2'];
	}
	?>
	<tr>
		<td><span><?php echo $coasid; ?></span></td>
		<td><span><?php echo $data['desk']; ?></span></td>
	</tr>
	<?php
	$no++;
}
?>
