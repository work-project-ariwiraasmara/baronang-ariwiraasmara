<?php
require('header.php');
if( isset($_COOKIE['login']) ) { 
?>

	<div class="page-content">

		<div class="page-title page-title-small">
	        <h2 id="title"><a href="10master_coa.php" data-back-button><i class="fa fa-arrow-left"></i></a>COA 2</h2>
	        <a href="#" data-menu="menu-main" class="bg-fade-gray1-dark shadow-xl preload-img" data-src="../images/icon/logo-skyparking-50x50.png"></a>
	    </div>

	    <div class="card header-card shape-rounded" data-card-height="95">
	        <div class="card-overlay bg-highlight opacity-95"></div>
	        <div class="card-overlay dark-mode-tint"></div>
	        <div class="card-bg preload-img" data-src="../images/pictures/20s.jpg"></div>
	    </div>

		<div class="card card-style">
	        <div class="content mt-3 mb-3">

				<form action="" method="post">

					<span class="txt-black">COA 1 :</span>
					<div class="input-style input-style-2 input-required">
						<em><i class="fa fa-angle-down"></i></em>
						<select class="txt-black" name="coa1" id="coa1" required>
							<option value="0">-Select COA 1-</option>
							<?php
							$scoa1 = "SELECT * from 11coa1 order by coa1 ASC";
							$qcoa1 = mysqli_query($koneksi, $scoa1) or die(mysqli_error($koneksi));
							$rcoa1 = mysqli_num_rows($qcoa1);
							if($rcoa1 > 0) {
								while($dcoa1 = mysqli_fetch_array($qcoa1)) { ?>
									<option value="<?php echo $dcoa1['coa1id']; ?>"><span><?php echo $dcoa1['coa1desc']; ?></span></option>
									<?php
								}
							}
							else { ?>
								<option value="" disabled>Data is Empty</option>
								<?php
							}							
							?>
						</select>
					</div>

					<div class="input-style input-style-1 input-required">
						<span class="txt-black">COA 2</span>
						<em>(required)</em>
						<input type="number" name="coa2" id="coa2" placeholder="COA 2" class="txt-black">
					</div>

					<div class="input-style input-style-1 input-required">
						<span class="txt-black">COA 2 Description</span>
						<em>(required)</em>
						<input type="text" name="coa2desk" id="coa2desk" placeholder="COA 2 Description" class="txt-black">
					</div>

					<input type="text" name="numbertoajax" id="numbertoajax" value="0" class="hide">

					<input type="text" name="query" id="query" value="" class="hide" readonly>

					<div class="bold italic center" id="msgadd"></div>

					<button type="button" name="btnadd" id="btnadd" class="btn btn-m btn-full mb-2 mt-4 rounded-xl text-uppercase font-900 shadow-s bg-highlight width-100">Add</button>

					<table class="table table-borderless rounded-sm shadow-l" style="overflow: hidden;">
						<thead>
							<tr class="bg-gray1-dark">
								<th scope="col" class="color-theme">COA 2</th>
								<th scope="col" class="color-theme">COA 2 Description</th>
								<th></th>
							</tr>
						</thead>

						<tbody id="tobeadded">
							
						</tbody>

					</table>

					<hr>

					<button type="submit" name="confirm" class="btn btn-m btn-full mb-2 mt-5 rounded-xl text-uppercase font-900 shadow-s bg-highlight width-100">Save</button>
				</form>

				<?php
				if(isset($_POST['confirm'])) {

					$coa1 		= @$_POST['coa1'];
					$coa2 		= @$_POST['coa2'];
					$coa2desk 	= @$_POST['coa2desk'];
					
					$qval	 	= @$_POST['query'];
					$lenqval 	= (int)strlen($qval);
					$qval 		= substr($qval, 0, $lenqval-1).');';

					$sql = "INSERT into 12coa2(coa1id, coa2, coa2desc) values $qval";
					echo $sql;
					$res = mysqli_query($koneksi, $sql) or die(mysqli_error($koneksi));
					if($res) { ?>
						<script type="text/javascript">
							window.location.href = "?";
						</script>
						<?php
					}

				}
				?>

				<div class="table-responsive m-t-30">
					<!--<div class="txt-link right">
						<a href="#" id="refreshtable">refresh</a>
					</div> -->
					
					<table class="table table-borderless rounded-sm shadow-l" style="overflow: hidden;">
						<thead>
							<tr class="bg-gray1-dark">
								<th scope="col" class="color-theme">COA 2</th>
								<th scope="col" class="color-theme">COA 2 Description</th>
							</tr>
						</thead>

						<tbody id="loadtable">
							<?php
							$no = 1;
							$scoa 	= "SELECT * from 12coa2 as a order by coa1id, coa2 ASC";
							$qcoa = mysqli_query($koneksi, $scoa) or die(mysqli_error($koneksi));
							while($dcoa = mysqli_fetch_array($qcoa)) { 

								$scoa1 = "SELECT * from 11coa1 where coa1id='".$dcoa['coa1id']."'";
								$qcoa1 = mysqli_query($koneksi, $scoa1) or die(mysqli_error($koneksi));
								$dcoa1 = mysqli_fetch_array($qcoa1);

								?>
								<tr>
									<td><span><?php echo $dcoa1['coa1'].'-'.$dcoa['coa2']; ?></span></td>
									<td><span><?php echo $dcoa['coa2desc']; ?></span></td>
								</tr>
								<?php
								$no++;
							}
							?>
						</tbody>

						<tfoot>
							<tr>
								<th colspan="2"><hr></th>
							</tr>
						</tfoot>
					</table>
				</div>

			</div>
		</div>

	</div>

<script type="text/javascript">
	var valtobeadd = [];

	$("#btnadd").click(function() {
		var coa 		= "coa1";
		var coa1 		= $("#coa1 :selected").val();
		var coa2 		= $("#coa2").val();
		var coa2desk 	= $("#coa2desk").val();
		var qval 		= $("#query").val();
		var number 		= $("#numbertoajax").val();

		if( (coa1 == "") || (coa2 == "") || (coa2desk == "") ) {
			alert("COA 1, COA2 or COA 2 Description can't be empty");
		}
		else {
			$.ajax({
				url : "ajax_add_coa.php",
				type : 'POST',
				data: { 
					coa1: coa1,
					coa2: coa2,
					coadesk: coa2desk,
					coa_reltype: coa,
					number: number
				},
				success : function(data) {
					if(data == "exist") {
						$("#msgadd").html("This data already exist");
					}
					else {
						var valadd = "('" + coa1 + "','" + coa2 + "','" + coa2desk + "')";
						number = number + 1;
						valtobeadd.push(valadd);
						$("#numbertoajax").val(number);
						$("#tobeadded").append(data);
						$("#query").val(valtobeadd.join());
						$("#coa2").val('');
						$("#coa2desk").val('');
						$("#msgadd").html("");
					}	
				},
				error : function(){
					alert('Terjadi kesalahan dalam jaringan!\nTidak dapat menambah data!\nSilahkan cek koneksi jaringan dan coba lagi!');
					return false;
				}
			});
		}
	});

	$("#coa1").change(function(){
		var coa1 	= $("#coa1 :selected").val();
		var coatype = "coa1";

		$.ajax({
			url : "ajax_loadtable_relasi_coa.php",
			type : 'POST',
			data: { 
				coa1: coa1,
				coa_reltype: coatype
			},
			success : function(data) {
				$("#loadtable").html(data);
			},
			error : function(){
				alert('Terjadi kesalahan dalam jaringan!\nTidak dapat menambah data!\nSilahkan cek koneksi jaringan dan coba lagi!');
				return false;
			}
		});
	});

	$("#refreshtable").click(function(){
		var coatype = "coa2";

		$.ajax({
			url : "ajax_refresh_table.php",
			type : 'POST',
			data: { 
				coa_reltype: coatype
			},
			success : function(data) {
				$("#loadtable").html(data);
			},
			error : function(){
				alert('Terjadi kesalahan dalam jaringan!\nTidak dapat menambah data!\nSilahkan cek koneksi jaringan dan coba lagi!');
				return false;
			}
		});
	});
</script>

<?php 
}
require('footer.php');
$fun->setTitle('COA 2');
$fun->setTitle_CP('Baronang');
$fun->setDocTitle();
?>