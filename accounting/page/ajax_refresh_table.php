<?php
require('../koneksi.php');

$coa = @$_POST['coa_reltype'];

if($coa == "coa4") {
	$scoa 	= "SELECT 	a.coa4 as coa, a.coa4desc as desk,
						b.coa3 as coa3, c.coa2 as coa2, d.coa1 as coa1
				from 14coa4 as a 
				inner join 13coa3 as b 
					on a.coa3id = b.coa3
				inner join 12coa2 as c 
					on b.coa2id = c.coa2 
				inner join 11coa1 as d 
					on c.coa1id = d.coa1
				order by a.coa4 ASC";
}
else if($coa == "coa3") {
	$scoa 	= "SELECT 	a.coa3 as coa, a.coa3desc as desk,
						b.coa2 as coa2, c.coa1 as coa1
				from 13coa3 as a 
				inner join 12coa2 as b 
					on a.coa2id = b.coa2
				inner join 11coa1 as c 
					on b.coa1id = c.coa1 
				order by a.coa3 ASC";
}
else {
	$scoa 	= "SELECT 	a.coa2 as coa, a.coa2desc as desk,
						b.coa1 as coa1
				from 12coa2 as a 
				inner join 11coa1 as b 
					on a.coa1id = b.coa1
				order by a.coa2 ASC";
}
$no = 1;
$qcoa = mysqli_query($koneksi, $scoa) or die(mysqli_error($koneksi));
while($dcoa = mysqli_fetch_array($qcoa)) { 

	if($coa == "coa4") {
		$coasid  = $dcoa['coa1'].'-'.$dcoa['coa2'].'-'.$dcoa['coa3'];
	}
	else if($coa == "coa3") {
		$coasid  = $dcoa['coa1'].'-'.$dcoa['coa2'];
	}
	else {
		$coasid  = $dcoa['coa1'];
	}
	?>
	<tr>
		<td class="right"><span><?php echo $no.'). '; ?></span></td>
		<td><span><?php echo $dcoa['coa']; ?></span></td>
		<td><span><?php echo $dcoa['desk']; ?></span></td>
		<td><span><?php echo $coasid; ?></span></td>
	</tr>
	<?php
	$no++;
}
?>