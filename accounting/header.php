<?php
require('util/koneksi.php');
?>
<!DOCTYPE html>
<html>
	<head>
		<title></title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
		<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
		
		<link rel="stylesheet" type="text/css" href="styles/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="styles/style.css">
		<link rel="stylesheet" type="text/css" href="styles/custom_style.css">
		<link rel="stylesheet" type="text/css" href="fonts/css/fontawesome-all.min.css">    
		<link rel="stylesheet" href="https://fonts.googleapis.comcss?family=Poppins:300,400,500,600,700,800,900|Roboto:300,300i,400,400i,500,500i,700,700i,900,900i&amp;display=swap">
		<link rel="manifest" href="_manifest.json" data-pwa-version="set_in_manifest_and_pwa_js">
		<link rel="apple-touch-icon" sizes="180x180" href="app/icons/icon-192x192.png">
		
		<!-- Icons -->
		<link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" media="all" rel="stylesheet" type="text/css">

		<link href='images/icon/Logo-fish-icon.png' type='image/x-icon'/>
		<link href='images/icon/Logo-fish-icon.png' rel='shortcut icon'/>

		<script type="text/javascript" src="scripts/jquery-2.1.0.min.js"></script>
	</head>

	<body class="theme-light" data-highlight="blue2">
		<div id="page">

			<?php 
			if(isset($_COOKIE['login'])) { ?>
				<div class="header header-fixed header-auto-show header-logo-app">
					<span class="header-title header-subtitle left" id="title"></span>

					<a href="#" data-menu="menu-main" class="header-icon header-icon-1"><i class="fas fa-bars"></i></a>

					<a href="#" data-toggle-theme class="header-icon header-icon-2 show-on-theme-dark"><i class="fas fa-sun"></i></a>
			        <a href="#" data-toggle-theme class="header-icon header-icon-2 show-on-theme-light"><i class="fas fa-moon"></i></a>
			        <a href="#" data-menu="menu-highlights" class="header-icon header-icon-3"><i class="fas fa-brush"></i></a>
				</div>
				<?php
			}