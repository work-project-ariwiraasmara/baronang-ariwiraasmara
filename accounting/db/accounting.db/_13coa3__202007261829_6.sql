INSERT INTO accounting.`13coa3` (coa2id,coa3,coa3desc) VALUES 
(1,'01','CASH')
,(1,'02','BANK')
,(1,'03','TIME DEPOSIT')
,(1,'04','ACCOUNT RECEIVABLE')
,(1,'05','ADVANCE')
,(1,'06','DUE FROM RELATED PARTIES')
,(1,'07','INVENTORY')
,(1,'08','PREPAID EXPENSES')
,(1,'09','PREPAID TAXES')
,(1,'10','INVESTMENT')
;
INSERT INTO accounting.`13coa3` (coa2id,coa3,coa3desc) VALUES 
(2,'01','OFFICE EQUIPMENT')
,(2,'02','MACHINE')
,(2,'03','BUILDING')
,(2,'04','LAND')
,(2,'05','CONSTRUCTION IN PROGRESS')
,(2,'06','ACCUMULATED DEPRECIATION')
,(3,'01','OTHER ASSETS')
,(4,'01','ACCOUNT PAYABLE')
,(4,'02','TAX PAYABLE')
,(4,'03','BANK LOAN')
;
INSERT INTO accounting.`13coa3` (coa2id,coa3,coa3desc) VALUES 
(4,'04','ACCRUED EXPENSES')
,(4,'05','OTHER LIABILITIES')
,(4,'06','UNEARNED INCOME')
,(6,'01','Long Term Debt')
,(7,'01','Capital')
,(7,'02','Retained Earning')
,(8,'01','Sales')
,(9,'01','Other income')
,(10,'01','DIRECT COST ( LABOUR & OVERHEAD PROJECT )')
,(11,'01','RAW MATERIAL')
;
INSERT INTO accounting.`13coa3` (coa2id,coa3,coa3desc) VALUES 
(12,'01','Finished Goods')
,(13,'01','Selling Expense')
,(14,'01','GENERAL & ADMINISTRATIVE EXPENSE')
,(15,'01','Other Expenses')
;