<?php require('header.php'); ?>

	<div id="content" class="page txt-black">

		<div id="toolbar" class="primary-color">
			<a href="index.php" class="open-left" id="toBack">
				<i class="ion-android-arrow-back"></i>
			</a>
			
			<span class="title uppercase txt-white" id="title"></span>
		</div>

		<div class="animated fadeinup delay-1">
			<div class="page-content">

				<form action="" method="post">
					<div class="input-field">
						<span class="bold">COA 1</span>
						<select class="browser-default txt-black" name="coa1" id="coa1" required>
							<option value="0">-Select Item-</option>
							<?php
							$scoa1 = "SELECT * from 11coa1 order by coa1 ASC";
							$qcoa1 = mysqli_query($koneksi, $scoa1) or die(mysqli_error($koneksi));
							$rcoa1 = mysqli_num_rows($qcoa1);
							if($rcoa1 > 0) {
								while($dcoa1 = mysqli_fetch_array($qcoa1)) { ?>
									<option value="<?php echo $dcoa1['coa1id']; ?>"><?php echo $dcoa1['coa1desc']; ?></option>
									<?php
								}
							}
							else { ?>
								<option value="" disabled>Data is Empty</option>
								<?php
							}							
							?>
						</select>
					</div>

					<div class="input-field">
						<span class="bold">COA 2</span>
						<select class="browser-default txt-black" name="coa2" id="coa2" required>
							<option value="0" disabled>Choose COA 1 First..</option>
						</select>
					</div>

					<div class="input-field">
						<span class="bold">COA 3</span>
						<select class="browser-default txt-black" name="coa3" id="coa3" required>
							<option value="0" disabled>Choose COA 2 First..</option>
						</select>
					</div>

					<div class="input-field">
						<label for="coa4" class="txt-black">COA 4</label>
						<input type="number" name="coa4" id="coa4" class="validate">
					</div>

					<div class="input-field">
						<label for="coa4desk" class="txt-black">COA 4 Description</label>
						<input type="text" name="coa4desk" id="coa4desk" class="validate">
					</div>

					<input type="text" name="numbertoajax" id="numbertoajax" value="0" class="hide">

					<input type="text" name="query" id="query" value="" class="hide">

					<div class="bold italic center" id="msgadd"></div>

					<button type="button" name="btnadd" id="btnadd" class="btn btn-large width-100 waves-effect waves-light primary-color m-t-30 borad-20">Add</button>

					<table class="table">
						<thead>
							<tr>
								<th>COA 4</th>
								<th>Description</th>
								<th></th>
							</tr>
						</thead>

						<tbody id="tobeadded">
							
						</tbody>

						<tfoot>
							<tr>
								<th colspan="2"><hr></th>
							</tr>
						</tfoot>
					</table>

					<button type="submit" name="confirm" class="btn btn-large width-100 waves-effect waves-light primary-color m-t-10 borad-20">Save</button>
				</form>

				<?php
				if(isset($_POST['confirm'])) {

					$coa3 		= @$_POST['coa3'];
					$coa4 		= @$_POST['coa4'];
					$coa4desk 	= @$_POST['coa4desk'];

					$qval	 	= @$_POST['query'];
					$lenqval 	= (int)strlen($qval);
					$qval 		= substr($qval, 0, $lenqval-1).');';

					$sql = "INSERT into 14coa4(coa3id, coa4, coa4desc) values $qval";
					//echo $sql;
					$res = mysqli_query($koneksi, $sql) or die(mysqli_error($koneksi));
					if($res) { ?>
						<script type="text/javascript">
							window.location.href = "?";
						</script>
						<?php
					}

				}
				?>

				<div class="table-responsive m-t-30">
					<!--<div class="txt-link right">
						<a href="#" id="refreshtable">refresh</a>
					</div>-->

					<table class="table">
						<thead>
							<tr>
								<th class="center">COA 4</th>
								<th class="center">COA 4 Description</th>
							</tr>
						</thead>

						<tbody id="loadtable">
							<?php
							$no = 1;
							$scoa 	= "SELECT * from 14coa4 as a order by coa3id, coa4 ASC";
							$qcoa = mysqli_query($koneksi, $scoa) or die(mysqli_error($koneksi));
							while($dcoa = mysqli_fetch_array($qcoa)) { 

								$scoa3 = "SELECT * from 13coa3 where coa3id='".$dcoa['coa3id']."'";
								$qcoa3 = mysqli_query($koneksi, $scoa3) or die(mysqli_error($koneksi));
								$dcoa3 = mysqli_fetch_array($qcoa3);

								$scoa2 = "SELECT * from 12coa2 where coa2id='".$dcoa3['coa2id']."'";
								$qcoa2 = mysqli_query($koneksi, $scoa2) or die(mysqli_error($koneksi));
								$dcoa2 = mysqli_fetch_array($qcoa2);

								$scoa1 = "SELECT * from 11coa1 where coa1id='".$dcoa2['coa1id']."'";
								$qcoa1 = mysqli_query($koneksi, $scoa1) or die(mysqli_error($koneksi));
								$dcoa1 = mysqli_fetch_array($qcoa1);

								?>
								<tr>
									<td><?php echo $dcoa1['coa1'].'-'.$dcoa2['coa2'].'-'.$dcoa3['coa3'].'-'.$dcoa['coa4']; ?></td>
									<td><?php echo $dcoa['coa4desc']; ?></td>
								</tr>
								<?php
								$no++;
							}
							?>
						</tbody>

						<tfoot id="tobeadded">
							<tr>
								<th colspan="2"><hr></th>
							</tr>
						</tfoot>
					</table>
				</div>

			</div>
		</div>

	</div>

<script type="text/javascript">
	var valtobeadd = new Array();

	$("#btnadd").click(function() {
		var coa 		= "coa3";
		var coa1 		= $("#coa1 :selected").val();
		var coa2 		= $("#coa2 :selected").val();
		var coa3 		= $("#coa3 :selected").val();
		var coa4 		= $("#coa4").val();
		var coa4desk 	= $("#coa4desk").val();
		var qval 		= $("#query").val();
		var number 		= $("#numbertoajax").val();

		if( (coa1 == "") || (coa2 == "") || (coa3 == "") || (coa4 == "") || (coa4desk == "") ) {
			alert("COA 1, COA2 or COA 2 Description can't be empty");
		}
		else {
			$.ajax({
				url : "ajax_add_coa.php",
				type : 'POST',
				data: { 
					coa1: coa1,
					coa2: coa2,
					coa3: coa3,
					coa4: coa4,
					coadesk: coa4desk,
					coa_reltype: coa,
					number: number
				},
				success : function(data) {
					if(data == "exist") {
						$("#msgadd").html("This data already exist");
					}
					else {
						var valadd = "('" + coa3 + "','" + coa4 + "','" + coa4desk + "')";
						number = number + 1;
						valtobeadd.push(valadd);
						$("#numbertoajax").val(number);
						$("#tobeadded").append(data);
						$("#query").val(valtobeadd.join());
						$("#coa4").val('');
						$("#coa4desk").val('');
						$("#msgadd").html("");
					}
				},
				error : function(){
					alert('Terjadi kesalahan dalam jaringan!\nTidak dapat menambah data!\nSilahkan cek koneksi jaringan dan coba lagi!');
					return false;
				}
			});
		}

	});

	$("#coa1").change(function(){
		var coa1 	 = $("#coa1 :selected").val();
		var coatype  = "coa2";

		if(coa1 == 0) {
			$("#coa2").html("<option disabled>Choose COA 1 First..</option>");
			$("#coa3").html("<option disabled>Choose COA 2 First..</option>");
		}
		else {
			$.ajax({
				url : "ajax_select_coa.php",
				type : 'POST',
				data: { 
					coa: coa1,
					coa_reltype: coatype
				},
				success : function(data) {
					if(data == 0) {
						$("#coa2").html("<option disabled>There is no data for this COA</option>");
						$("#coa3").html("<option disabled>There is no data for this COA</option>");
					}
					else {
						$("#coa2").html(data);
						$("#coa3").html("<option disabled>Choose COA 2 First..</option>");
					}
				},
				error : function(){
					alert('Terjadi kesalahan dalam jaringan!\nTidak dapat menambah data!\nSilahkan cek koneksi jaringan dan coba lagi!');
					return false;
				}
			});
		}
	});

	$("#coa2").change(function(){
		var coa2 	= $("#coa2 :selected").val();
		var coatype = "coa3";

		if(coa2 == 0) {
			$("#coa3").html("<option disabled>Choose COA 2 First..</option>");
		}
		else {
			$.ajax({
				url : "ajax_select_coa.php",
				type : 'POST',
				data: { 
					coa: coa2,
					coa_reltype: coatype
				},
				success : function(data) {
					if(data == 0) {
						$("#coa3").html('<option disabled>There is no data for this COA</option>');
					}
					else {
						$("#coa3").html(data);
					}
				},
				error : function(){
					alert('Terjadi kesalahan dalam jaringan!\nTidak dapat menambah data!\nSilahkan cek koneksi jaringan dan coba lagi!');
					return false;
				}
			});
		}
	});

	$("#coa3").change(function(){
		var coa1 	= $("#coa1 :selected").val();
		var coa2 	= $("#coa2 :selected").val();
		var coa3 	= $("#coa3 :selected").val();
		var coatype = "coa3";

		$.ajax({
			url : "ajax_loadtable_relasi_coa.php",
			type : 'POST',
			data: { 
				coa1: coa1,
				coa2: coa2,
				coa3: coa3,
				coa_reltype: coatype
			},
			success : function(data) {
				//console.log(data);
				$("#loadtable").html(data);
			},
			error : function(){
				alert('Terjadi kesalahan dalam jaringan!\nTidak dapat menambah data!\nSilahkan cek koneksi jaringan dan coba lagi!');
				return false;
			}
		});
	});

	$("#refreshtable").click(function(){
		var coatype = "coa4";

		$.ajax({
			url : "ajax_refresh_table.php",
			type : 'POST',
			data: { 
				coa_reltype: coatype
			},
			success : function(data) {
				$("#loadtable").html(data);
			},
			error : function(){
				alert('Terjadi kesalahan dalam jaringan!\nTidak dapat menambah data!\nSilahkan cek koneksi jaringan dan coba lagi!');
				return false;
			}
		});
	});
</script>

<?php 
$fun->setTitle_CP('Baronang');
$fun->setTitle('COA 4');
$fun->setDocTitle();
require('footer.php'); 
?>
