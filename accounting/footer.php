			<?php
			//require('menu-colors.php');
			//require('menu-main.php');
			//require('menu-share.php');
			?>

			<div id="menu-share" 
		         class="menu menu-box-bottom menu-box-detached rounded-m" 
		         data-menu-load="menu-share.php"
		         data-menu-height="420" 
		         data-menu-effect="menu-over">
		    </div>    
		    
		    <div id="menu-highlights" 
		         class="menu menu-box-bottom menu-box-detached rounded-m" 
		         data-menu-load="menu-colors.php"
		         data-menu-height="510" 
		         data-menu-effect="menu-over">        
		    </div>
		    
		    <div id="menu-main"
		         class="menu menu-box-right menu-box-detached rounded-m"
		         data-menu-width="260"
		         data-menu-load="menu-main.php"
		         data-menu-active="nav-welcome"
		         data-menu-effect="menu-over">  
		    </div>

		</div>

		<script type="text/javascript" src="scripts/jquery.js"></script>
		<script type="text/javascript" src="scripts/bootstrap.min.js"></script>
		<script type="text/javascript" src="scripts/custom.js"></script>
	</body>
</html>