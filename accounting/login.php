<?php require('header.php');
if( !isset($_COOKIE['login']) ) {
?>

	<div class="page-content">
		<div class="page-title page-title-small">
			<h1 class="txt-white center" id="title">Sign In</h1>
		</div>

		<div class="card header-card shape-rounded" data-card-height="150">
			<div class="card-overlay bg-highlight opacity-95"></div>
			<div class="card-overlay dark-mode-tint"></div>
			<div class="card-bg preload-img" data-src="images/pictures/20s.jpg"></div>
		</div>
				        
		<div class="card card-style">
			<div class="content mt-5 mb-0">
				<form action="" method="post">
					<div class="input-style has-icon input-style-1 input-required pb-1">
						<i class="input-icon fa fa-user color-theme"></i>
						<span>Email</span>
						<em>(required)</em>
						<input type="email" name="email" id="email" placeholder="Email">
					</div> 

					<div class="input-style has-icon input-style-1 input-required pb-1">
						<i class="input-icon fa fa-lock color-theme"></i>
						<span>Password</span>
						<em>(required)</em>
						<input type="password" name="pass" id="pass" placeholder="Password">
					</div>

					<button type="submit" name="ok" class="btn btn-m mt-2 mb-4 btn-center-xl bg-highlight txt-white rounded-sm text-uppercase font-900 center width-100">Sign In</button>
				
				</form>

				<?php
				if(isset($_POST['ok'])) {
					$email = @$_POST['email'];
					$pass  = @$_POST['pass'];

					$sql = "SELECT * from 02masteruser where email='$email' and pass='$pass'";
					$qry = mysqli_query($koneksi, $sql) or die(mysqli_error($koneksi));
					$row = mysqli_num_rows($qry);
					if($row > 0) {
						$data = mysqli_fetch_array($qry);
						setcookie('login', '#login{1}->'.$data['nama'].':'.$data['email'], time() + (1 * 24 * 60 * 60), "/"); // 86400 = 1 day
						setcookie('ucode',  $data['usercode'], time() + (1 * 24 * 60 * 60), "/"); // 86400 = 1 day
						setcookie('email', $data['email'], time() + (1 * 24 * 60 * 60), "/"); // 86400 = 1 day
						setcookie('name', $data['nama'], time() + (1 * 24 * 60 * 60), "/"); // 86400 = 1 day
						header('location:home.php');
					}
					else { ?>
						<h3 class="txt-black italic center m-t-30">Email / Password is wrong!<br/>Try Again!</h3>
						<?php
					}
				}
				?>
			</div>

		</div>

	</div>

<?php
}
else {
	header('location:home.php');
}
require('footer.php');
$fun->setTitle('Login Accounting');
$fun->setTitle_CP('Baronang');
$fun->setDocTitle();
?>